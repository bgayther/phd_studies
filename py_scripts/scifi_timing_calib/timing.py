import ROOT
import numpy as np
import time
from scipy.stats import norm, sem
from collections import defaultdict
import matplotlib.pyplot as plt
import argparse
import logging
import os
import networkx as nx
import tabulate

startTime = time.time()

parser = argparse.ArgumentParser(description='SciFi Calibration Analysis',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of events to process; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='timing_analysis', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/offset_reco_bkg_data/large_data", help='path/to/reco_file.root (not including file though)')
parser.add_argument('-c','--correct', type=int, default=0, help='Use found offsets to correct data? (default is no correction, 1 for correction)')
args = parser.parse_args()

os.chdir(args.path)

ROOT.gStyle.SetOptStat(0)
# root_file = ROOT.TFile.Open(args.inputs[0])
# ROOT.gROOT.ProcessLine("segs->MakeClass()")
# ROOT.gROOT.ProcessLine(".L segs.C+") # make sure to do segs.MakeClass() before this if running for the first time!
events = ROOT.TChain("segs")
for file in args.inputs:
    print "Adding {} for analysis".format(file)
    events.Add(file)

# events.MakeClass()
# ROOT.gROOT.ProcessLine(".L segs.C+")
Events = events#ROOT.segs()
# Events.Init(events)

nevents = Events.GetEntries()#Events.fChain.GetEntries() 
if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.output+".root", 'RECREATE')

fb_sids = 12 # number of fb sensors
frame_ns = 50 # frame length used

h_vel = ROOT.TH1D("h_vel", "vel as fraction of c", 100, -10, 10)
h_t_diff = ROOT.TH1D("h_t_diff", ";t_diff;count", 200, -frame_ns/5, frame_ns/5)
h_t_diff_adj = ROOT.TH1D("h_t_diff_adj", ";t_diff;count", 200, -frame_ns/5, frame_ns/5)
h_charge = ROOT.TH1D("h_charge", "", 100, -10, 10)
h_t2_t1 = ROOT.TH2D("h_t2_t1", "", 60, -5, 55,  60, -5, 55)
h_t_diff_t2_sid = ROOT.TH2D("h_t_diff_t2_sid", ";t_diff;fb sensor", 200, -frame_ns, frame_ns, fb_sids, 0, fb_sids)
h_t_diff_t1_sid = ROOT.TH2D("h_t_diff_t1_sid", ";t_diff;fb sensor", 200, -frame_ns, frame_ns, fb_sids, 0, fb_sids)
h_t2_vs_sid = ROOT.TH2D("h_t2_vs_sid", "", 200, -5, 55, fb_sids, 0, fb_sids)
h_t1_vs_sid = ROOT.TH2D("h_t1_vs_sid", "", 200, -5, 55, fb_sids, 0, fb_sids)
h_dt_all = ROOT.TH2D("h_dt_all", "All;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
h_dt_unadjusted = ROOT.TH2D("h_dt_unadjusted", "Unadjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
h_dt_both_adjusted = ROOT.TH2D("h_dt_both_adjusted", "Both adjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
h_dt_one_adjusted = ROOT.TH2D("h_dt_one_adjusted", "One adjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
h_dt_all_corrected = ROOT.TH2D("h_dt_all_corrected", "Corrected;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
h_dt_all_vs_t2_sid = ROOT.TH3D("h_dt_all_vs_t2_sid", "All;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]; t2 sensor id", 100, 0, 1000, 100, -10, 10, fb_sids, 0, fb_sids)
h_calib_matrix = ROOT.TH2D("h_calib_matrix", "", fb_sids, 0, fb_sids, fb_sids, 0, fb_sids) 
h_t_diff_corrected = ROOT.TH1D("h_t_diff_corrected", ";t_diff;count", 200, -frame_ns/5, frame_ns/5)

# Offset vs no tracks used 
g_offset_trk_no_ls = []
for sid in xrange(fb_sids):
    gr = ROOT.TGraph()
    gr.SetName("offset_"+str(sid)+"_trk_no")
    gr.SetTitle("offset_"+str(sid)+"_trk_no")
    g_offset_trk_no_ls.append(gr)

# Offset moving avg vs no tracks used 
g_offset_MA_trk_no_ls = []
for sid in xrange(fb_sids):
    gr = ROOT.TGraph()
    gr.SetName("offset_"+str(sid)+"_moving_avg_trk_no")
    gr.SetTitle("offset_"+str(sid)+"_moving_avg_trk_no")
    g_offset_MA_trk_no_ls.append(gr)

# List of histos, indexed by T1 sid, each will contain the t_diff and corresponding T2 fb sid 
h_diff_T1_ls = []
for sid in xrange(fb_sids):
    h_diff_T1_ls.append(ROOT.TH2D("h_diff_sid_"+str(sid)+"_T1", "corresponding T2 fb sensors for T1 fb sensor "+str(sid)+" ;t_diff; T2 fb sensor", 200, -frame_ns, frame_ns, fb_sids, 0, fb_sids))

# List of histos, for each sensor, will contain the t_diff and corresponding T2 or T1 fb sid 
h_diff_sid_all_ls = []
for sid in xrange(fb_sids):
    h_diff_sid_all_ls.append(ROOT.TH2D("h_diff_sid_"+str(sid)+"_vs_all_sid", "", 200, -frame_ns, frame_ns, fb_sids, 0, fb_sids))

all_sensors = [sid for sid in xrange(fb_sids)]

# Index is T1_sid, and key is T2_sid, with values t_diff 
T1_T2_multimap = [defaultdict(list) for i in xrange(fb_sids)]

offsets_list = [0.0 for i in xrange(fb_sids)]
offset_data = [[] for i in xrange(fb_sids)]
offset_data_err = [[] for i in xrange(fb_sids)]

track_counter = 0
fb_resolution = 0.250 #250 ps

ctr = [0.0 for i in xrange(fb_sids)]
# mc_offsets_dict = dict([(0, 0.0), (1, 1.0), (2, 0.0), (3, 3.0), (4, 0.0), (5, 0.0), (6, 1.5), (7, 0.0), (8, 0.0), (9, 0.0), (10, 0.0), (11, 0.5)])
# mc_offsets_dict = dict([(0, 0.0), (1, 0.1), (2, 0.2), (3, 0.3), (4, 0.4), (5, 0.5), (6, 0.6), (7, 0.7), (8, 0.8), (9, 0.9), (10, 1.0), (11, 4.0)])
# mc_offsets_dict = dict([(0, 0.0), (1, 0.0), (2, 0.0), (3, 0.0), (4, 0.0), (5, 0.0), (6, 0.0), (7, 0.0), (8, 0.0), (9, 0.0), (10, 0.0), (11, 0.0)])
mc_offsets_dict = dict([(0, 0.03), (1, 0.05), (2, 0.1), (3, 0.09), (4, 0.01), (5, 0.06), (6, 0.03), (7, 0.06), (8, 0.09), (9, 0.1), (10, 0.08), (11, 0.07)])
all_offsets_close = {key:False for (key,value) in mc_offsets_dict.items()}

sid_has_been = [False for i in xrange(fb_sids)]
curAvg = [0.0 for i in xrange(fb_sids)] # Can try intialising them all to some high offset

window  = 2000 
t_diff_window_off = 40.0 
t_diff_window_ma = 10.0 
t_diff_adj_factor = 2.0 

# def selection(event):
#     if (event.n != 6 or event.mc != 1 or event.fbc_t2 == 0 or event.fbc_t1 == 0 or event.mc_pid != -11 or -1 * event.mc_p / event.r < 0): 
#         return False
#     else:
#         return True


def selection(event):
    if (event.n != 4 or event.fbc_t2 == 0 or event.fbc_t1 == 0 or event.p > 0 or event.mc_fb_crossings!=2 or event.fbc_sh1 < 0): 
        return False
    else:
        return True

Events.SetBranchStatus("*", 0)
Events.SetBranchStatus("n", 1)
Events.SetBranchStatus("mc", 1)
Events.SetBranchStatus("mc_prime", 1)
Events.SetBranchStatus("mc_pid", 1)
Events.SetBranchStatus("fbc_t1_sid", 1)
Events.SetBranchStatus("fbc_t2_sid", 1)
Events.SetBranchStatus("r", 1)
Events.SetBranchStatus("mc_p", 1)
Events.SetBranchStatus("fbc_t1", 1)
Events.SetBranchStatus("fbc_t2", 1)
Events.SetBranchStatus("s01", 1)
Events.SetBranchStatus("s20", 1)
Events.SetBranchStatus("fbc_sh1", 1)
Events.SetBranchStatus("mc_fb_crossings", 1)
Events.SetBranchStatus("p", 1)

for entry in xrange(nevents):
    Events.LoadTree(entry)
    Events.GetEntry(entry)
    # Only use L8, hits associated with tracks, require both fibre times, and use positrons 
    if selection(Events) == False: continue

    # Only use correctly assigned charges!
    # h_charge.Fill(q)

    fbc_dt = Events.fbc_t2 - Events.fbc_t1 
    path_length = Events.s01[5] - Events.s20[0] 
    # path_length = Events.fbc_sh1
    # if abs(fbc_dt) < fb_resolution: continue # Should I be doing this?

    path_length_per_c = path_length / 300.0 # units of mm/ns, ns overall
    t_diff = fbc_dt - path_length_per_c
    t1_sid = Events.fbc_t1_sid
    t2_sid = Events.fbc_t2_sid
    
    # fbc_size = Events.fbc_nh
    # lam = Events.mc_lam

    if fbc_dt != 0: 
        vel = path_length_per_c / fbc_dt
        h_vel.Fill(vel)

    """  
    #### T_DIFF_ADJ CALCULATION ####
    if ctr[t2_sid] <= window:
        t_2 = Events.fbc_t2 - offsets_list[t2_sid]
    else:
        t_2 = Events.fbc_t2 - curAvg[t2_sid] 

    if ctr[t1_sid] <= window:
        t_1 = Events.fbc_t1 - offsets_list[t1_sid]
    else:
        t_1 = Events.fbc_t1 - curAvg[t1_sid] 

    t_diff_adj = (t_2 - t_1) - path_length_per_c

    #### FACTOR TO DIVIDE T_DIFF_ADJ FOR LATER WHEN UPDATING OFFSET LIST ####
    if ctr[t2_sid] <= window + window/20.0:
        adj_t2 = t_diff_adj / t_diff_adj_factor
    else:
        adj_t2 = t_diff_adj * t_diff_adj_factor

    if ctr[t1_sid] <= window + window/20.0:
        adj_t1 = t_diff_adj / t_diff_adj_factor
    else:
        adj_t1 = t_diff_adj * t_diff_adj_factor

    #### OFFSET UPDATING ####
    passed_t2 = False
    window_t2_off = offsets_list[t2_sid] - t_diff_window_off <= abs(offsets_list[t2_sid] + adj_t2) <= offsets_list[t2_sid] + t_diff_window_off
    window_t2_ma = curAvg[t2_sid] - t_diff_window_ma <= abs(curAvg[t2_sid] + adj_t2) <= curAvg[t2_sid] + t_diff_window_ma

    if ctr[t2_sid] <= window and window_t2_off:
        passed_t2 = True
        offsets_list[t2_sid] = offsets_list[t2_sid] + adj_t2
    if ctr[t2_sid] > window and window_t2_ma:
        passed_t2 = True
        offsets_list[t2_sid] = curAvg[t2_sid] + adj_t2

    passed_t1 = False
    window_t1_off = offsets_list[t1_sid] - t_diff_window_off <= abs(offsets_list[t1_sid] - adj_t1) <= offsets_list[t1_sid] + t_diff_window_off
    window_t1_ma = curAvg[t1_sid] - t_diff_window_ma <= abs(curAvg[t1_sid] - adj_t1) <= curAvg[t1_sid] + t_diff_window_ma

    if ctr[t1_sid] <= window and window_t1_off:
        passed_t1 = True
        offsets_list[t1_sid] = offsets_list[t1_sid] - adj_t1
    if ctr[t1_sid] > window and window_t1_ma:
        passed_t1 = True
        offsets_list[t1_sid] = curAvg[t1_sid] - adj_t1


    #### MOVING AVERAGE CALCULATION ####
    if passed_t2:

        if sid_has_been[t2_sid]:
            curAvg[t2_sid] = curAvg[t2_sid] + (offsets_list[t2_sid] - curAvg[t2_sid])/(ctr[t2_sid]-window+1)

        if ctr[t2_sid] == window:
            # print t2_sid, ctr[t2_sid], track_counter, curAvg[t2_sid]
            sid_has_been[t2_sid] = True 
            curAvg[t2_sid] = np.mean(offset_data[t2_sid])
            # print curAvg[t2_sid]

        offset_data[t2_sid].append(offsets_list[t2_sid]) 
        ctr[t2_sid]+=1
    
    if passed_t1:

        if sid_has_been[t1_sid]:
            curAvg[t1_sid] = curAvg[t1_sid] + (offsets_list[t1_sid] - curAvg[t1_sid])/(ctr[t1_sid]-window+1)

        if ctr[t1_sid] == window:
            # print t1_sid, ctr[t1_sid], track_counter, curAvg[t1_sid]
            sid_has_been[t1_sid] = True 
            curAvg[t1_sid] = np.mean(offset_data[t1_sid])
            # print curAvg[t1_sid]
            
        offset_data[t1_sid].append(offsets_list[t1_sid]) 
        ctr[t1_sid]+=1
    #### END OF CALCULATION ####


    # if ctr[6] >= window-20 and ctr[6] <= window+5 and (t1_sid==6  or t2_sid==6) :
    #     print("Ctr {:.0f}, t_diff_adj {:.3f}, CMA {:.3f}, offset_update {:.3f} \n".format(ctr[6],t_diff_adj,curAvg[6]-np.mean(curAvg),offsets_list[6]-np.mean(offsets_list)))         

    h_t_diff_adj.Fill(t_diff_adj)

    gr1 = g_offset_trk_no_ls[t1_sid]
    gr1.SetPoint(gr1.GetN(), ctr[t1_sid], offsets_list[t1_sid])

    gr2 = g_offset_trk_no_ls[t2_sid]
    gr2.SetPoint(gr2.GetN(), ctr[t2_sid], offsets_list[t2_sid])

    gr_ma1 = g_offset_MA_trk_no_ls[t1_sid]
    gr_ma1.SetPoint(gr_ma1.GetN(), ctr[t1_sid], curAvg[t1_sid])

    gr_ma2 = g_offset_MA_trk_no_ls[t2_sid]
    gr_ma2.SetPoint(gr_ma2.GetN(), ctr[t2_sid], curAvg[t2_sid])

    # cli arg, use small offset method?
    # t_diff = t_diff_adj

    """
    # For each T1 sid, plot time diff vs T2's sid
    h_diff_T1_ls[t1_sid].Fill(t_diff,t2_sid)
    
    # Essentially a list of multimaps
    defDic = T1_T2_multimap[t1_sid]
    defDic[t2_sid].append(t_diff)

    h_t_diff.Fill(t_diff)
    h_t2_t1.Fill(Events.fbc_t2, Events.fbc_t1)

    h_dt_all.Fill(path_length, fbc_dt)
    h_dt_all_vs_t2_sid.Fill(path_length, fbc_dt, t2_sid)

    h_t2_vs_sid.Fill(Events.fbc_t2, t2_sid)
    h_t1_vs_sid.Fill(Events.fbc_t1, t1_sid)
    
    h_t_diff_t2_sid.Fill(t_diff, t2_sid)
    h_t_diff_t1_sid.Fill(t_diff, t1_sid)

    track_counter += 1

print "No. of tracks passing selection ", track_counter


# T_diff_adj method results.

""" 
found_mean_overall = np.mean(curAvg)
offsets_map = {}
found_offset_sensors = []
results = []
sigmas = []
for sid in all_sensors:
    rel_offset = curAvg[sid] - found_mean_overall
    expected_offset = mc_offsets_dict.get(sid) - expected_mean_overall
    offsets_map[sid] = rel_offset
    err = sem(offset_data[sid], axis=None, ddof=0)
    sigma = (rel_offset-expected_offset)/err
    sigmas.append(abs(sigma))
    # if abs(rel_offset) > 0.10:
    #     found_offset_sensors.append(sid)
    results.append([sid, rel_offset, expected_offset, err, sigma])

print(tabulate.tabulate(results, ["sensor","CMA offset","expected offset","error","sigma"], tablefmt='fancy_grid',floatfmt=".3f"))
print "Mean of found offfsets ", found_mean_overall
print "Sum of sigmas ", sum(sigmas)
print "Window size (first N tracks used) ", window 
"""
expected_mean_overall = np.array(list(mc_offsets_dict.values())).mean()
print "Mean of expected offsets ", expected_mean_overall



T1_T2_multimap = [dict(def_dict) for def_dict in T1_T2_multimap]

# Sum all sid combos and plot per sid
for t1_sid, dct in enumerate(T1_T2_multimap):
    for t2_sid, data in dct.iteritems():
        for t_diff in data:
            h_diff_sid_all_ls[t1_sid].Fill(t_diff,t2_sid)

    other_sids = [i for i in all_sensors if i is not t1_sid]
    for sid in other_sids:
        for t2_sid, data in T1_T2_multimap[sid].iteritems():
            if t2_sid == t1_sid:
                for t_diff in data:
                    h_diff_sid_all_ls[t1_sid].Fill(t_diff,sid)


# Create a directed graph, with nodes being each fb sensor id
def makeSensorGraph(T1_T2_multimap):
    diGraph = nx.DiGraph() 
    diGraph.add_nodes_from(all_sensors)
    t1_t2_pair_has_been = []
    t1_t2_pair_unique = [] # removes degenerate pairs
    h_gauss  = ROOT.TH1D("h_gauss", "Gaussian fit to data;t_diff;counts", 200, -frame_ns/5, frame_ns/5)

    for t1_sid, dct in enumerate(T1_T2_multimap):
        sum_mu = 0
        sum_std = 0
        fb_ctr = 0
        trk_ctr = 0
        data_sum = 0.0
        for t2_sid, data in dct.iteritems():
            data_sum += len(data)
        for t2_sid, data in dct.iteritems():
            if len(data)/data_sum < 0.10: continue 
            # print "T2 key", t2_sid, "percentage ", len(data)/data_sum *100
            fb_ctr += 1
            trk_ctr += len(data)

            for i in data: 
                h_gauss.Fill(i)
            
            mean_shift = h_gauss.GetMean()
            
            # Maybe use two gaussians? one for the peak and another to cover the broader tail
            width = 1.5
            mean_low = mean_shift - width
            mean_high = mean_shift + width

            gauss = ROOT.TF1("gauss", "gaus", mean_low, mean_high)

            h_gauss.Fit("gauss", "Q N E M R L")
            mu = gauss.GetParameter(1)
            std = gauss.GetParameter(2)
            sem = gauss.GetParError(1)

            # if t1_sid == 4 and t2_sid == 10:
            #     can_gauss = ROOT.TCanvas("can_gauss", "can_gauss", 800, 600)
            #     h_gauss.SetTitle("mean = "+str(mu)+" +/- "+str(sem))
            #     h_gauss.Draw("")
            #     gauss.Draw("same") 
            #     # can_gauss.SaveAs("gaussian_fit_histo.pdf")
            #     raw_input("continue")

            h_gauss.Reset()

            # Significance Z = (X - mu) / std_dev, x = 0 for this case
            Z_0 =  -mu/std
            # Std err on mean (SEM) 
            # print sem

            h_calib_matrix.Fill(t1_sid, t2_sid, Z_0)

            sum_mu += mu
            sum_std += std

            t1_t2_pair_has_been.append([t1_sid, t2_sid])

            # as [t1_sid, t2_sid] should = -[t2_sid, t1_sid] (may have to check this!)
            if [t2_sid, t1_sid] not in t1_t2_pair_has_been: 
                # Create edge from t1 to t2 sensors, with a weight attr and error attr
                diGraph.add_edge(t1_sid, t2_sid, weight=mu, error=sem) 
                t1_t2_pair_unique.append([t1_sid, t2_sid])

        if (fb_ctr == 0):
            print "No fbs found!"
            continue
        avg_mu = -sum_mu/fb_ctr
        avg_std = sum_std/fb_ctr

        # print "Avg of mu for sensor ", t1_sid, ": ", avg_mu
        # print "No. of fbs: ", fb_ctr, " and no. of tracks ", trk_ctr, "\n"
    return diGraph

def path_cost(G, path, calcErr):
    if calcErr:
        return sum([G[path[i]][path[i+1]]['error']**2.0 for i in range(len(path)-1)])**0.5
    else:
        return sum([G[path[i]][path[i+1]]['weight'] for i in range(len(path)-1)])

def find_offsets_from_graph(G, relative_sid, cutoff_g):
    offsets_map = {}
    offsets_large = []
    table = []
    all_sensors_but_source = [i for i in xrange(fb_sids) if i is not relative_sid]
    for sid in all_sensors_but_source:
        paths = []
        i = 0
        while len(paths) == 0 and i < 5:
            paths = [path for path in nx.all_simple_paths(G, source=relative_sid, target=sid, cutoff=cutoff_g+i)] 
            i+=1
        prev_weight = np.inf
        prev_error = np.inf
        min_path = []
        acc_weight = 0.0
        for path in paths:
            path_weight = path_cost(G, path, False)
            path_error = path_cost(G, path, True)
            # if sid == 5: print path, path_weight, path_error

            if len(path) >= 1: # make this a variable?
                if (path_error < prev_error):
                    prev_error = path_error
                    prev_weight = abs(path_weight)
                    acc_weight = path_weight
                    del min_path[:]
                    min_path.append(path)

        if prev_weight > 0.39:
            # print "Found one"
            offsets_large.append(sid)
        if (len(min_path) != 0):
            expected_offset = mc_offsets_dict.get(sid) - mc_offsets_dict.get(relative_sid)
            table.append([sid, acc_weight,expected_offset,prev_error,(acc_weight-expected_offset)/prev_error,min_path[0]])
            # print("In sensor {:2d}, found offset {:.3f}, (rel to sid {:2d}), with error {:.3f}, path was {} \n".format(sid, acc_weight,relative_sid,prev_error,min_path[0]))         
        else:
            print "Did not find any paths..."
        offsets_map[sid] = acc_weight

    print(tabulate.tabulate(table, ["sensor","expected offset","offset","error","sigma","path"], tablefmt='fancy_grid',floatfmt=".3f")) # can do latex for a latex table
    return offsets_map, offsets_large

diGraph = makeSensorGraph(T1_T2_multimap)
offsets_map, found_offset_sensors = find_offsets_from_graph(diGraph, 0, 4)

# Now loop over events again to correct by estimated offsets
if args.correct:
    ROOT.gROOT.SetBatch(ROOT.kTRUE)

    case3_ctr = 0
    for entry in xrange(nevents):
        Events.GetEntry(entry)
        if selection(Events) == False: continue

        t2 = Events.fbc_t2
        t1 = Events.fbc_t1
        t1_sid = Events.fbc_t1_sid
        t2_sid = Events.fbc_t2_sid

        # Should actually be altering the t_diff value...
        if t1_sid in offsets_map:
            t1 -= offsets_map.get(t1_sid)
        if t2_sid in offsets_map: 
            t2 -= offsets_map.get(t2_sid)

        if t2 < 0.0 or t1 < 0.0: continue # would require some frame fixing I think
        fbc_dt = t2 - t1 
        path_length = Events.s01[5] - Events.s20[0] 
        path_length_per_c = path_length / 300.0 # units of mm/ns, ns overall
        t_diff = fbc_dt - path_length_per_c

        h_dt_all_corrected.Fill(path_length, fbc_dt)
        h_t_diff_corrected.Fill(t_diff)

        if len(found_offset_sensors) >= 1:
            if (t1_sid and t2_sid not in found_offset_sensors):
                h_dt_unadjusted.Fill(path_length, fbc_dt)

            both = False
            if (t1_sid and t2_sid in found_offset_sensors):
                both = True
                h_dt_both_adjusted.Fill(path_length, fbc_dt)

            if t1 > t2 and both: case3_ctr += 1
            if (t1_sid or t2_sid in found_offset_sensors and not both):
                h_dt_one_adjusted.Fill(path_length, fbc_dt)

    print "Found", case3_ctr, "case 3's"

    can_dt_all = ROOT.TCanvas("can_dt_all", "can_dt_all", 1200, 600)
    can_dt_all.Divide(2)

    can_dt_all.cd(1)

    # Actually, fit straight line corresponding to speed of light, rather than fitting to data maybe?

    line_dt_all = ROOT.TF1("line_dt_all","[0]+[1]*x",90,990)
    h_dt_all.Fit("line_dt_all", "Q N")
    h_dt_all.Draw("colz")
    line_dt_all.Draw("same")

    can_dt_all.cd(2)
    line_dt_all_corrected = ROOT.TF1("line_dt_all_corrected","[0]+[1]*x",90,990)
    h_dt_all_corrected.Fit("line_dt_all_corrected", "Q N")
    h_dt_all_corrected.Draw("colz")
    line_dt_all_corrected.Draw("same")

    can_dt_all.SaveAs("dt_all_corrected_line.pdf")

    ROOT.gPad.Update()

    can_t_diff = ROOT.TCanvas("can_t_diff", "can_t_diff", 1300, 600)
    can_t_diff.Divide(2)

    can_t_diff.cd(1)
    gauss_t_diff = ROOT.TF1("gauss_t_diff", "gaus", -10,10)
    h_t_diff.Fit("gauss_t_diff", "Q N")
    h_t_diff.Draw()
    # gauss_t_diff.Draw("same")

    can_t_diff.cd(2)
    gauss_t_diff_corrected = ROOT.TF1("gauss_t_diff_corrected", "gaus", -10,10)
    h_t_diff_corrected.Fit("gauss_t_diff_corrected", "Q N")
    h_t_diff_corrected.Draw()
    # gauss_t_diff_corrected.Draw("same")

    can_t_diff.SaveAs("t_diff_corrected_gauss.pdf")

    ROOT.gPad.Update()


for gr in g_offset_trk_no_ls:
    gr.Write()
for gr in g_offset_MA_trk_no_ls:
    gr.Write()

output_file.Write()
output_file.Close()

# Add this to some .sh file which calls this script as well
# rootprint -f pdf -d histograms -D "colz"  timing_analysis.root:*

# dot_out = nx.nx_pydot.to_pydot(diGraph)
# TODO export graph to graphviz...
# edge_labels=dict([((u,v,),d['weight'])
#                  for u,v,d in diGraph.edges(data=True)])
# pos=nx.spring_layout(diGraph)
# nx.draw_networkx_edge_labels(diGraph, pos, edge_labels=edge_labels)
# nx.draw_circular(diGraph, with_labels=True)
# plt.show()
# plt.savefig('graph.pdf')


print ("Took {:.2f} s".format(time.time() - startTime))