import json
import argparse
import numpy as np
from math import atan2

parser = argparse.ArgumentParser(description='Read alignment tree from file and produce json for event display',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--inputSimSensors', type=str, required=True, help='Input sim sensors json file')
parser.add_argument('--inputRunSensors', type=str, required=True, help='Input run sensors json file')
parser.add_argument('--path', type=str, default="", help='path/to/sim_file.root (not including file)')
parser.add_argument('--outputDir', type=str, default="./", help="path/to/outputfile.json (not including filename")
args = parser.parse_args()

if (args.path != ""): 
    inputSimSensors = args.path + args.inputSimSensors
    inputRunSensors = args.path + args.inputRunSensors

inputSimSensors = args.inputSimSensors
inputRunSensors = args.inputRunSensors

alignmentRunData = {}
alignmentSimData = {}
simToRunData = {}
allSensorData = []

simFileData = []
runFileData = []

with open(inputSimSensors) as file: simFileData = json.load(file)
with open(inputRunSensors) as file: runFileData = json.load(file)

for runEntry in runFileData:
    runData = runFileData[runEntry]

    runPos = np.array([runData["v"]["x"]*1000.0, runData["v"]["y"]*1000.0, runData["v"]["z"]*1000.0])
    runRadial2 = np.sqrt(runPos[0]**2.0 + runPos[1]**2.0 + runPos[2]**2.0)
    runRadialTrans = np.sqrt(runPos[0]**2.0 + runPos[1]**2.0)

    run_dx = simEntry["drow"]["x"] / np.hypot(simEntry["drow"]["x"], simEntry["drow"]["y"])
    run_dy = simEntry["drow"]["y"] / np.hypot(simEntry["drow"]["x"], simEntry["drow"]["y"])
    run_l = simPos[0] * run_dx + simPos[1] * run_dy
    run_vx_ = simPos[0] - run_l *  run_dx
    run_vy_ = simPos[1] - run_l * run_dy
    run_r = np.hypot(run_vx_, run_vy_)
    run_phi = atan2(run_vy_, run_vx_)

    closest_simRadialPos = np.inf
    closest_simRadialTransPos = np.inf
    closest_simEntry = -1
    for simEntry in simFileData:

        simPos = np.array([simEntry["v"]["x"], simEntry["v"]["y"], simEntry["v"]["z"]])
        simRadial2 = np.sqrt(simPos[0]**2.0 + simPos[1]**2.0 + simPos[2]**2.0)
        simRadialTrans = np.sqrt(simPos[0]**2.0 + simPos[1]**2.0)
        # print(simRadial2, np.sign(simPos))
        
        sim_dx = simEntry["drow"]["x"] / np.hypot(simEntry["drow"]["x"], simEntry["drow"]["y"])
        sim_dy = simEntry["drow"]["y"] / np.hypot(simEntry["drow"]["x"], simEntry["drow"]["y"])
        sim_l = simPos[0] * sim_dx + simPos[1] * sim_dy
        sim_vx_ = simPos[0] - sim_l *  sim_dx
        sim_vy_ = simPos[1] - sim_l * sim_dy
        sim_r = np.hypot(sim_vx_, sim_vy_)
        sim_phi = atan2(sim_vy_, sim_vx_)

        if ((runRadial2 - simRadial2) < closest_simRadialPos and (runRadialTrans - simRadialTrans) < closest_simRadialTransPos \
            and (np.sign(simPos) == np.sign(runPos)).all() ):
            closest_simRadialPos = (runRadial2 - simRadial2)
            closest_simRadialTransPos = (runRadialTrans - simRadialTrans)
            closest_simEntry = simEntry

    print("RunChip {}, closest sim chip ID is {}".format(runData["intRunChip"], closest_simEntry["simChip"]))
        

for entry in runFileData:
    runChip = runFileData[entry]["intRunChip"]

    simSensorEntry = [d for d in simFileData if d["runChip"] == runFileData[entry]["intRunChip"]][0] # TODO: check if multiple...
    simChip = simSensorEntry["simChip"]
    layer = simSensorEntry["layer"]
    ladder = simSensorEntry["ladder"]
    chip = simSensorEntry["chip"]
    
    simToRunData[simChip] = {"runChip" : runChip}
    # print(entry, simSensorId)

    whichDataToUse = runFileData[entry]
    # whichDataToUse = simSensorEntry

    sensorData = {
        "simChip": simChip,
        "runChip" : runChip,
        "layer" : layer,
        "ladder" : ladder,
        "chip" : chip,
        "v": {
            "x": whichDataToUse["v"]["x"] if whichDataToUse == simSensorEntry else whichDataToUse["v"]["x"]*1000.0 ,
            "y": whichDataToUse["v"]["y"] if whichDataToUse == simSensorEntry else whichDataToUse["v"]["y"]*1000.0 ,
            "z": whichDataToUse["v"]["z"] if whichDataToUse == simSensorEntry else whichDataToUse["v"]["z"]*1000.0 
        },
        "drow": {
            "x": whichDataToUse["drow"]["x"] if whichDataToUse == simSensorEntry else whichDataToUse["drow"]["x"]*1000.0,
            "y": whichDataToUse["drow"]["y"] if whichDataToUse == simSensorEntry else whichDataToUse["drow"]["y"]*1000.0,
            "z": whichDataToUse["drow"]["z"] if whichDataToUse == simSensorEntry else whichDataToUse["drow"]["z"]*1000.0
        },
        "dcol": {
            "x": whichDataToUse["dcol"]["x"] if whichDataToUse == simSensorEntry else whichDataToUse["dcol"]["x"]*1000.0,
            "y": whichDataToUse["dcol"]["y"] if whichDataToUse == simSensorEntry else whichDataToUse["dcol"]["y"]*1000.0,
            "z": whichDataToUse["dcol"]["z"] if whichDataToUse == simSensorEntry else whichDataToUse["dcol"]["z"]*1000.0
        },
        "nrow": whichDataToUse["nrow"],
        "ncol": whichDataToUse["ncol"],
        "width": whichDataToUse["width"] if whichDataToUse == simSensorEntry else whichDataToUse["width"]*1000.0,
        "length": whichDataToUse["length"] if whichDataToUse == simSensorEntry else whichDataToUse["length"]*1000.0,
        "thickness": whichDataToUse["thickness"],
        "pixelSize": whichDataToUse["pixelSize"]
    }

    alignmentRunData[str(runChip)] = sensorData
    alignmentSimData[str(simChip)] = sensorData
    allSensorData.append(sensorData)

with open('sensors_cosRun2022.json', 'w') as outfile:
    json.dump(alignmentRunData, outfile, indent=4)

with open('sensors_sim_cosRun2022.json', 'w') as outfile:
    json.dump(alignmentSimData, outfile, indent=4)

with open('simChipToRunChip.json', 'w') as outfile:
    json.dump(simToRunData, outfile, indent=4) 

with open('sensors_CAD.json', 'w') as outfile:
    json.dump(allSensorData, outfile, indent=4) 

# TODO: this for fibres/tiles in future