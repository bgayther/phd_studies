import ROOT
import numpy as np
import argparse
import os
import tabulate
from array import array
import json

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of vertexTree to process; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='vertexTimingAnalysis', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

os.chdir(args.path)
# ROOT.gROOT.SetBatch(True)
# ROOT.gStyle.SetOptStat(0)

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

vertexTree = ROOT.TChain("vertex")

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    vertexTree.Add(file)

cfg = json.loads(str(ROOT.TFile.Open(args.inputs[0]).Get("config").GetString()))
clsMadeWithFit = True if cfg["trirec"]["fb"]["recluster_using_likelihood"].lower() == "true" else False
recluster_singleTrk_clusters = True if cfg["trirec"]["fb"]["recluster_singleTrk_clusters"].lower() == "true" else False
recluster_multiTrk_clusters = True if cfg["trirec"]["fb"]["recluster_multiTrk_clusters"].lower() == "true" else False

singleTrk_dt_min = float(cfg["trirec"]["fb"]["singleTrk_dt_min"])
singleTrk_dt_max = float(cfg["trirec"]["fb"]["singleTrk_dt_max"])
multiTrk_dt_min = float(cfg["trirec"]["fb"]["multiTrk_dt_min"])

nevents = vertexTree.GetEntries()
if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.output+".root", 'RECREATE')

min_x = -1.0
max_x = 1.0
res = 0.001
Nbins = int((max_x-min_x)/res)

# All L6/L8 tracks (recurlers)
h_tr1_vertex_time_res = ROOT.TH1D("h_tr1_vertex_time_res", "Res", Nbins, min_x, max_x)
h_tr2_vertex_time_res = ROOT.TH1D("h_tr2_vertex_time_res", "Res", Nbins, min_x, max_x)
h_tr3_vertex_time_res = ROOT.TH1D("h_tr3_vertex_time_res", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_err = ROOT.TH1D("h_combined_vertex_time_err", "Res", 100, 0, 0.400)

# tl only tracks
h_tr1_vertex_time_res_tl_only = ROOT.TH1D("h_tr1_vertex_time_res_tl_only", "Res", Nbins, min_x, max_x)
h_tr2_vertex_time_res_tl_only = ROOT.TH1D("h_tr2_vertex_time_res_tl_only", "Res", Nbins, min_x, max_x)
h_tr3_vertex_time_res_tl_only = ROOT.TH1D("h_tr3_vertex_time_res_tl_only", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_tl_only = ROOT.TH1D("h_combined_vertex_time_res_tl_only", "Res", Nbins, min_x, max_x)

# tl and fb tracks
h_tr1_vertex_time_res_tl_and_fb = ROOT.TH1D("h_tr1_vertex_time_res_tl_and_fb", "Res", Nbins, min_x, max_x)
h_tr2_vertex_time_res_tl_and_fb = ROOT.TH1D("h_tr2_vertex_time_res_tl_and_fb", "Res", Nbins, min_x, max_x)
h_tr3_vertex_time_res_tl_and_fb = ROOT.TH1D("h_tr3_vertex_time_res_tl_and_fb", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_tl_and_fb = ROOT.TH1D("h_combined_vertex_time_res_tl_and_fb", "Res", Nbins, min_x, max_x)

# fb only tracks
h_tr1_vertex_time_res_fb_only = ROOT.TH1D("h_tr1_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)
h_tr2_vertex_time_res_fb_only = ROOT.TH1D("h_tr2_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)
h_tr3_vertex_time_res_fb_only = ROOT.TH1D("h_tr3_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)

h_tr_vertex_time_res_fb_only = ROOT.TH1D("h_tr_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)
h_tr_unaffected_vertex_time_res_fb_only = ROOT.TH1D("h_tr_unaffected_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)
h_tr_affected_vertex_time_res_fb_only = ROOT.TH1D("h_tr_affected_vertex_time_res_fb_only", "Res", Nbins, min_x, max_x)

# combined times
h_combined_vertex_time_res = ROOT.TH1D("h_combined_vertex_time_res", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_all_fb_only = ROOT.TH1D("h_combined_vertex_time_res_all_fb_only", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_no_fb_only = ROOT.TH1D("h_combined_vertex_time_res_no_fb_only", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_one_fb_only = ROOT.TH1D("h_combined_vertex_time_res_one_fb_only", "Res", Nbins, min_x, max_x)
h_combined_vertex_time_res_two_fb_only = ROOT.TH1D("h_combined_vertex_time_res_two_fb_only", "Res", Nbins, min_x, max_x)

# Unaffected times
h_unaffected_combined_vertex_time_res = ROOT.TH1D("h_unaffected_combined_vertex_time_res", "Res", Nbins, min_x, max_x)
h_unaffected_combined_vertex_time_res_all_fb_only = ROOT.TH1D("h_unaffected_combined_vertex_time_res_all_fb_only", "Res", Nbins, min_x, max_x)
h_unaffected_combined_vertex_time_res_no_fb_only = ROOT.TH1D("h_unaffected_combined_vertex_time_res_no_fb_only", "Res", Nbins, min_x, max_x)
h_unaffected_combined_vertex_time_res_one_fb_only = ROOT.TH1D("h_unaffected_combined_vertex_time_res_one_fb_only", "Res", Nbins, min_x, max_x)
h_unaffected_combined_vertex_time_res_two_fb_only = ROOT.TH1D("h_unaffected_combined_vertex_time_res_two_fb_only", "Res", Nbins, min_x, max_x)

# Affected times
h_affected_combined_vertex_time_res = ROOT.TH1D("h_affected_combined_vertex_time_res", "Res", Nbins, min_x, max_x)
h_affected_combined_vertex_time_res_all_fb_only = ROOT.TH1D("h_affected_combined_vertex_time_res_all_fb_only", "Res", Nbins, min_x, max_x)
h_affected_combined_vertex_time_res_no_fb_only = ROOT.TH1D("h_affected_combined_vertex_time_res_no_fb_only", "Res", Nbins, min_x, max_x)
h_affected_combined_vertex_time_res_one_fb_only = ROOT.TH1D("h_affected_combined_vertex_time_res_one_fb_only", "Res", Nbins, min_x, max_x)
h_affected_combined_vertex_time_res_two_fb_only = ROOT.TH1D("h_affected_combined_vertex_time_res_two_fb_only", "Res", Nbins, min_x, max_x)

# TIMING Chi2
NTimingBins = 10000
XTimingAxis = 1000

h_tv_chi2 = ROOT.TH1D("h_tv_chi2","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_chi2_tl_only = ROOT.TH1D("h_tv_chi2_tl_only","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_chi2_fb_only = ROOT.TH1D("h_tv_chi2_fb_only","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_chi2_tl_and_fb = ROOT.TH1D("h_tv_chi2_tl_and_fb","Chi2", NTimingBins, 0, XTimingAxis)

h_tv_rnd_chi2 = ROOT.TH1D("h_tv_rnd_chi2","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_rnd_chi2_tl_only = ROOT.TH1D("h_tv_rnd_chi2_tl_only","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_rnd_chi2_fb_only = ROOT.TH1D("h_tv_rnd_chi2_fb_only","Chi2", NTimingBins, 0, XTimingAxis)
h_tv_rnd_chi2_tl_and_fb = ROOT.TH1D("h_tv_rnd_chi2_tl_and_fb","Chi2", NTimingBins, 0, XTimingAxis)

# BHABHA TS
h_tv_bhabha_ts = ROOT.TH1D("h_tv_bhabha_ts","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_bhabha_ts_tl_only = ROOT.TH1D("h_tv_bhabha_ts_tl_only","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_bhabha_ts_fb_only = ROOT.TH1D("h_tv_bhabha_ts_fb_only","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_bhabha_ts_tl_and_fb = ROOT.TH1D("h_tv_bhabha_ts_tl_and_fb","t_{s}", NTimingBins, 0, XTimingAxis)

h_tv_rnd_bhabha_ts = ROOT.TH1D("h_tv_rnd_bhabha_ts","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_rnd_bhabha_ts_tl_only = ROOT.TH1D("h_tv_rnd_bhabha_ts_tl_only","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_rnd_bhabha_ts_fb_only = ROOT.TH1D("h_tv_rnd_bhabha_ts_fb_only","t_{s}", NTimingBins, 0, XTimingAxis)
h_tv_rnd_bhabha_ts_tl_and_fb = ROOT.TH1D("h_tv_rnd_bhabha_ts_tl_and_fb","t_{s}", NTimingBins, 0, XTimingAxis)

h_path1 = ROOT.TH1D("h_path1",";[mm]", 1000, 0, 1000)
h_path2 = ROOT.TH1D("h_path2",";[mm]", 1000, 0, 1000)
h_path3 = ROOT.TH1D("h_path3",";[mm]", 1000, 0, 1000)

vertexTree.SetBranchStatus("*", 0)
vertexTree.SetBranchStatus("ncomb", 1)
vertexTree.SetBranchStatus("nvert", 1)
vertexTree.SetBranchStatus("pm", 1)
vertexTree.SetBranchStatus("targetdist", 1)
vertexTree.SetBranchStatus("chi2", 1)

vertexTree.SetBranchStatus("t0_1", 1)
vertexTree.SetBranchStatus("t0_2", 1)
vertexTree.SetBranchStatus("t0_3", 1)

vertexTree.SetBranchStatus("t0_fb1", 1)
vertexTree.SetBranchStatus("t0_fb2", 1)
vertexTree.SetBranchStatus("t0_fb3", 1)

vertexTree.SetBranchStatus("t0_tl1", 1)
vertexTree.SetBranchStatus("t0_tl2", 1)
vertexTree.SetBranchStatus("t0_tl3", 1)

vertexTree.SetBranchStatus("tv_mc1", 1)
vertexTree.SetBranchStatus("tv_mc2", 1)
vertexTree.SetBranchStatus("tv_mc3", 1)

vertexTree.SetBranchStatus("tv_err1", 1)
vertexTree.SetBranchStatus("tv_err2", 1)
vertexTree.SetBranchStatus("tv_err3", 1)

vertexTree.SetBranchStatus("tv_1", 1)
vertexTree.SetBranchStatus("tv_2", 1)
vertexTree.SetBranchStatus("tv_3", 1)

vertexTree.SetBranchStatus("tv_path1", 1)
vertexTree.SetBranchStatus("tv_path2", 1)
vertexTree.SetBranchStatus("tv_path3", 1)

vertexTree.SetBranchStatus("nhit1", 1)
vertexTree.SetBranchStatus("nhit2", 1)
vertexTree.SetBranchStatus("nhit3", 1)

vertexTree.SetBranchStatus("type1", 1)
vertexTree.SetBranchStatus("type2", 1)
vertexTree.SetBranchStatus("type3", 1)

vertexTree.SetBranchStatus("vx", 1)
vertexTree.SetBranchStatus("vy", 1)
vertexTree.SetBranchStatus("vz", 1)
vertexTree.SetBranchStatus("true_vx", 1)
vertexTree.SetBranchStatus("true_vy", 1)
vertexTree.SetBranchStatus("true_vz", 1)

vertexTree.SetBranchStatus("tv_chi2", 1)
vertexTree.SetBranchStatus("tv_chi2_rnd", 1)

vertexTree.SetBranchStatus("tv_chi2_bhabha", 1) # need to sqrt to get ts
vertexTree.SetBranchStatus("tv_chi2_bhabha_rnd", 1) # need to sqrt to get ts

vertexTree.SetBranchStatus("fbc_t1_madeWithFit1", 1)
vertexTree.SetBranchStatus("fbc_t2_madeWithFit1", 1)
vertexTree.SetBranchStatus("fbc_t1_madeWithFit2", 1)
vertexTree.SetBranchStatus("fbc_t2_madeWithFit2", 1)
vertexTree.SetBranchStatus("fbc_t1_madeWithFit3", 1)
vertexTree.SetBranchStatus("fbc_t2_madeWithFit3", 1)

vertexTree.SetBranchStatus("fbc_t1_ns1", 1)
vertexTree.SetBranchStatus("fbc_t2_ns1", 1)
vertexTree.SetBranchStatus("fbc_t1_ns2", 1)
vertexTree.SetBranchStatus("fbc_t2_ns2", 1)
vertexTree.SetBranchStatus("fbc_t1_ns3", 1)
vertexTree.SetBranchStatus("fbc_t2_ns3", 1)

vertexTree.SetBranchStatus("fbc_t1_dt1", 1)
vertexTree.SetBranchStatus("fbc_t2_dt1", 1)
vertexTree.SetBranchStatus("fbc_t1_dt2", 1)
vertexTree.SetBranchStatus("fbc_t2_dt2", 1)
vertexTree.SetBranchStatus("fbc_t1_dt3", 1)
vertexTree.SetBranchStatus("fbc_t2_dt3", 1)

pm_cut = 10
t_dist_cut = 1.0
chi2_cut = 35
nhit_cut = 6
frameLength = 64.0

ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
# ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
ROOT.RooMsgService.instance().setSilentMode(True)

def fitGaus(h):
    x = ROOT.RooRealVar("x", "x", min_x, max_x)
    data = ROOT.RooDataHist("data","data",x,h)

    mean1 = ROOT.RooRealVar("mean1","mean1",0, -0.2,0.2)
    sigma1 = ROOT.RooRealVar("sigma1","sigma1",0.1,0.01,1.0)
    gauss1 = ROOT.RooGaussian("g1","g",x,mean1,sigma1)

    mean2 = ROOT.RooRealVar("mean2","mean",0, min_x, max_x)
    sigma2 = ROOT.RooRealVar("sigma2","sigma",0.1,0.01,1.0)
    gauss2 = ROOT.RooGaussian("g2","g",x,mean2,sigma2)

    frac = ROOT.RooRealVar("frac","fraction",0.8,0.0,1.0)
    model = ROOT.RooAddPdf("sum","g1+g2",ROOT.RooArgList(gauss1,gauss2), ROOT.RooArgList(frac))

    model.fitTo(data)

    # frame = x.frame()
    # data.plotOn(frame)
    # model.plotOn(frame)
    # # gauss1.plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kRed))
    # # gauss2.plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kBlue))
    # model.plotOn(frame, ROOT.RooFit.Components("g1"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kRed))
    # model.plotOn(frame, ROOT.RooFit.Components("g2"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kBlue))
    # frame.SetMinimum(1e-5)
    # frame.Draw()
    # input("Press Enter to continue...")

    # Draw frame to check fit...

    coreResolutionVal = sigma1.getVal()
    if frac.getVal()<0.7 and (sigma2.getVal()<sigma1.getVal()):
        coreResolutionVal = sigma2.getVal()
    
    # print("DONE FITTING")
    # frac.Print()
    # mean1.Print()
    # sigma1.Print()
    # mean2.Print()
    # sigma2.Print()
    return float(coreResolutionVal)

def printEvent(vertex, idx):
    print ("--idx {}".format(idx))
    print ("pm {}, t_dist {}, chi2 {}, mc_vt {}, vx {}, vy {}, vz {}".format(vertex.pm[idx], vertex.targetdist[idx], vertex.chi2[idx], vertex.tv_mc1[idx], vertex.vx[idx], vertex.vy[idx], vertex.vz[idx]))
    print ("tv_1 {}, tv_1_err {}, tv_2 {}, tv_2_err {},  tv_3 {}, tv_3_err {}".format(vertex.tv_1[idx], vertex.tv_err1[idx], vertex.tv_2[idx], vertex.tv_err2[idx], vertex.tv_3[idx], vertex.tv_err3[idx]))
    print ("t0_1 {}, t0_2 {}, t0_3 {}".format(vertex.t0_1[idx], vertex.t0_2[idx], vertex.t0_3[idx]))
    print ("t0_fb1 {}, t0_fb2 {}, t0_fb3 {}".format(vertex.t0_fb1[idx], vertex.t0_fb2[idx], vertex.t0_fb3[idx]))
    print ("t0_tl1 {}, t0_tl2 {}, t0_tl3 {}".format(vertex.t0_tl1[idx], vertex.t0_tl2[idx], vertex.t0_tl3[idx]))
    print ("fbc_t1_dt1 {}, fbc_t1_dt2 {}, fbc_t1_dt3 {}".format(vertex.fbc_t1_dt1[idx], vertex.fbc_t1_dt2[idx], vertex.fbc_t1_dt3[idx]))
    print ("fbc_t2_dt1 {}, fbc_t2_dt2 {}, fbc_t2_dt3 {}".format(vertex.fbc_t2_dt1[idx], vertex.fbc_t2_dt2[idx], vertex.fbc_t2_dt3[idx]))
    print ("fbc_t1_ns1 {}, fbc_t1_ns2 {}, fbc_t1_ns3 {}".format(vertex.fbc_t1_ns1[idx], vertex.fbc_t1_ns2[idx], vertex.fbc_t1_ns3[idx]))
    print ("fbc_t2_ns1 {}, fbc_t2_ns2 {}, fbc_t2_ns3 {}".format(vertex.fbc_t2_ns1[idx], vertex.fbc_t2_ns2[idx], vertex.fbc_t2_ns3[idx]))
    print ("\n")
    # print ("tv_path1 {}, tv_path2 {}, tv_path3 {}, tv_chi2 {}, tv_bhabha_ts {}".format(vertex.tv_path1[idx], vertex.tv_path2[idx], vertex.tv_path3[idx], vertex.tv_chi2[idx], np.sqrt(vertex.tv_chi2_bhabha[idx])))

def cut(vertex, idx):
    if vertex.pm[idx] < pm_cut and vertex.targetdist[idx] < t_dist_cut and vertex.chi2[idx] < chi2_cut:
        # and vertex.tv_1[idx] > 0 and vertex.tv_2[idx] > 0 and vertex.tv_3[idx] > 0 and (vertex.tv_mc1[idx] == vertex.tv_mc2[idx] == vertex.tv_mc3[idx])
        return True
    else:
        return False

def genRandTimes(vt):
    t_rnd = [0.0]*3

    t_pair = (vt[1] + vt[2]) / 2.0
    # Change to weighted average
    t_pair_new = np.random.uniform() * frameLength

    t_rnd[0] = np.random.uniform() * frameLength # Michel positron
    t_rnd[1] = t_pair_new + (vt[1] - t_pair) # Bhabha positron t_pair_new + (t1 - t2)/2
    t_rnd[2] = t_pair_new + (vt[2] - t_pair) # Bhabha electron t_pair_new + (t2 - t1)/2

    return t_rnd

# def timingChi2(vt, w):
#     ts = 0.0
#     ws = 0.0
#     for i in range(len(vt)):
#         ts += vt[i] * w[i]
#         ws += w[i]
#     tm = ts / ws
#     chi2 = 0.0
#     for i in range(len(vt)): 
#         chi2 += pow(vt[i] - tm, 2) * w[i]
#     return chi2

# def bhabhaTs(vt, w):
#     t_bhabha = 0.0
#     t_e = 0.0
#     t_bhabha_w = 0.0
#     t_e_w = 0.0

#     if (abs(vt[0] -vt[2]) < abs(vt[1] - vt[2])):
#         t_bhabha_w = w[0] + w[2]
#         t_bhabha = (vt[0] * w[0] + vt[2] * w[2]) / t_bhabha_w
#         t_e = vt[1]
#         t_e_w = w[1]
#     else:
#         t_bhabha_w = w[1] + w[2]
#         t_bhabha = (vt[1] * w[1] + vt[2] * w[2]) / t_bhabha_w
#         t_e = vt[0]
#         t_e_w = w[0]
    
#     return np.sqrt( pow(t_e - t_bhabha, 2) / (1./t_bhabha_w + 1./t_e_w) )


# def makeGraph(gr1, gr2, title, name):
#     can = ROOT.TCanvas("can", "can", 1200, 600)
#     mg = ROOT.TMultiGraph("mg",str(title))

#     gr1.SetLineColor(4)
#     gr2.SetLineColor(6)
#     mg.Add(gr1,"PL")
#     mg.Add(gr2, "PL")
#     mg.Draw("A LP")

#     leg = ROOT.TLegend(0.15,0.65,0.35,0.85)
#     leg.AddEntry(gr1,"n_mppcs 1","l")
#     leg.AddEntry(gr2,"n_mppcs 10","l")
#     leg.SetBorderSize(0)
#     leg.SetTextFont(42)
#     leg.SetTextSize(0.035)
#     leg.Draw()

#     ROOT.gPad.Update()
#     can.SaveAs(name)
#     return


def effSupPlots(h_coinc, h_rnd, name, xtitle):
    n_bins = h_coinc.GetNbinsX()
    eff = [0.0]*n_bins
    sup = [0.0]*n_bins
    x   = [0.0]*n_bins
    x_rnd = [0.0]*n_bins

    n     = h_coinc.GetEntries()
    n_rnd = h_rnd.GetEntries()

    g_sig = ROOT.TGraph()
    g_bg = ROOT.TGraph()

    g_roc = ROOT.TGraph()

    cutoff_data = []

    for i in range(1, n_bins):
        x[i]   =  h_coinc.GetBinLowEdge(i+1)
        x_rnd[i] = h_rnd.GetBinLowEdge(i+1)

        eff[i] = 1.0 * h_coinc.Integral(0, i) / n
        sup[i] = n_rnd / 1.0 * h_rnd.Integral(0, i) 

        # g_sig.SetPoint(g_sig.GetN(), x[i], (n - 1.0 * h_coinc.Integral(i, n_bins)) / n )
        # g_bg.SetPoint(g_bg.GetN(), x_rnd[i], (n_rnd - 1.0 * h_rnd.Integral(i, n_bins)) / n_rnd )
        # g_sig.SetPoint(g_sig.GetN(), x[i], 1.0 * h_coinc.Integral(i, n_bins)/n )
        # g_bg.SetPoint(g_bg.GetN(), x_rnd[i], 1.0 * h_rnd.Integral(i, n_bins)/n_rnd )
        g_sig.SetPoint(g_sig.GetN(), x[i], h_coinc.GetBinContent(i) )
        g_bg.SetPoint(g_bg.GetN(), x_rnd[i], h_rnd.GetBinContent(i) )

        if (not np.isfinite(sup[i])): sup[i] = 1


        cutoff_data.append([x[i], 1.0 * h_coinc.Integral(0, i)/n, 1.0 * h_rnd.Integral(0, i)/n_rnd])
        
        # g_roc.SetPoint(g_sig.GetN(), 1.0 * h_rnd.Integral(0, i)/n_rnd, 1.0 * h_coinc.Integral(0, i)/n )
        g_roc.SetPoint(g_sig.GetN(), 1.0 * h_coinc.Integral(0, i)/n, 1.0 * h_rnd.Integral(0, i)/n_rnd )


    g_eff = ROOT.TGraph(n_bins, array('d', x), array('d', eff))
    g_eff.SetName("g_eff_"+name)
    g_eff.GetXaxis().SetTitle(xtitle)
    g_eff.GetYaxis().SetTitle("efficiency")
    g_eff.Write()

    g_sup = ROOT.TGraph(n_bins, array('d', x), array('d', sup))
    g_sup.SetName("g_sup_"+name)
    g_sup.GetXaxis().SetTitle(xtitle)
    g_sup.GetYaxis().SetTitle("suppression")
    g_sup.Write()

    g_eff_sup = ROOT.TGraph(n_bins, array('d', eff), array('d', sup))
    g_eff_sup.SetName("g_eff_sup_"+name)
    g_eff_sup.GetXaxis().SetTitle("efficiency")
    g_eff_sup.GetYaxis().SetTitle("suppression")
    g_eff_sup.Write()

    g_roc.SetName("g_roc_"+name)
    g_roc.SetTitle(name)
    # g_roc.GetXaxis().SetTitle("FP rate")
    # g_roc.GetYaxis().SetTitle("TP rate")
    g_roc.GetXaxis().SetTitle("TP rate")
    g_roc.GetYaxis().SetTitle("FP rate")
    g_roc.Write()

    g_sig.SetName("g_coinc_"+name)
    g_sig.GetXaxis().SetTitle(xtitle)
    g_sig.Write()

    mg = ROOT.TMultiGraph("mg",";"+xtitle)
    can = ROOT.TCanvas("can", "can", 1200, 600)

    g_sig.SetLineColor(4)
    g_bg.SetLineColor(6)
    mg.Add(g_sig,"PL")
    mg.Add(g_bg, "PL")
    mg.Draw("A LP")

    ROOT.gPad.Modified()
    mg.GetXaxis().SetLimits(0.0,10.0)

    leg = ROOT.TLegend(0.65,0.65,0.85,0.85)
    leg.AddEntry(g_sig,"coinc","l")
    leg.AddEntry(g_bg,"rnd","l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.Draw()

    ROOT.gPad.Update()
    can.SaveAs(name+"_can.png")
    return cutoff_data

total_verts = 0
skipped = 0
#### Analysis loop ####
for i in range(0, nevents):
    vertexTree.GetEntry(i)
    for j in range(len(vertexTree.tv_1)):
        # TODO: Does it make sense to require each tracks mc vertex time to be the same? If looking at effect of track PU??
        # if cut(vertexTree, j) and (vertexTree.nhit1[j] >= 6 and vertexTree.nhit2[j] >= 6 and vertexTree.nhit3[j] >= 6):
        if (vertexTree.nhit1[j] >= 6 and vertexTree.nhit2[j] >= 6 and vertexTree.nhit3[j] >= 6):

            total_verts += 1
            # Can still use the other times to get a res! - However, won't be able to do bhabha Ts calc...
            if vertexTree.tv_path1[j] == 0 or vertexTree.tv_path2[j] == 0 or vertexTree.tv_path3[j] == 0:
            # #     printEvent(vertexTree, j)
            # #     print vertexTree.true_vx[j], vertexTree.true_vy[j], vertexTree.true_vz[j]
                skipped += 1
                continue

            fbc_t1_madeWithFit = [vertexTree.fbc_t1_madeWithFit1[j], vertexTree.fbc_t1_madeWithFit2[j], vertexTree.fbc_t1_madeWithFit3[j]]
            fbc_t2_madeWithFit = [vertexTree.fbc_t2_madeWithFit1[j], vertexTree.fbc_t2_madeWithFit2[j], vertexTree.fbc_t2_madeWithFit3[j]]

            fbc_t1_ns = [vertexTree.fbc_t1_ns1[j], vertexTree.fbc_t1_ns2[j], vertexTree.fbc_t1_ns3[j]]
            fbc_t2_ns = [vertexTree.fbc_t2_ns1[j], vertexTree.fbc_t2_ns2[j], vertexTree.fbc_t2_ns3[j]]

            fbc_t1_dt = [vertexTree.fbc_t1_dt1[j], vertexTree.fbc_t1_dt2[j], vertexTree.fbc_t1_dt3[j]]
            fbc_t2_dt = [vertexTree.fbc_t2_dt1[j], vertexTree.fbc_t2_dt2[j], vertexTree.fbc_t2_dt3[j]]

            tls = [vertexTree.t0_tl1[j], vertexTree.t0_tl2[j], vertexTree.t0_tl3[j]]
            fbs = [vertexTree.t0_fb1[j], vertexTree.t0_fb2[j], vertexTree.t0_fb3[j]]

            t0 = [vertexTree.t0_1[j], vertexTree.t0_2[j], vertexTree.t0_3[j]]

            tv = [vertexTree.tv_1[j], vertexTree.tv_2[j], vertexTree.tv_3[j]]
            tv_paths = [vertexTree.tv_path1[j], vertexTree.tv_path2[j], vertexTree.tv_path3[j]]
            mc_vt = [vertexTree.tv_mc1[j], vertexTree.tv_mc2[j], vertexTree.tv_mc3[j]]

            tr1_res = vertexTree.tv_1[j] - vertexTree.tv_mc1[j]
            tr2_res = vertexTree.tv_2[j] - vertexTree.tv_mc2[j]
            tr3_res = vertexTree.tv_3[j] - vertexTree.tv_mc3[j]

            h_tr1_vertex_time_res.Fill(tr1_res)
            h_tr2_vertex_time_res.Fill(tr2_res)
            h_tr3_vertex_time_res.Fill(tr3_res)

            h_path1.Fill(vertexTree.tv_path1[j])
            h_path2.Fill(vertexTree.tv_path2[j])
            h_path3.Fill(vertexTree.tv_path3[j])

            tr1_tl_and_fb = False
            tr1_fb_only = False
            tr1_tl_only = False

            tr2_tl_and_fb = False
            tr2_fb_only = False
            tr2_tl_only = False

            tr3_tl_and_fb = False
            tr3_fb_only = False
            tr3_tl_only = False

            # TR 1
            if tls[0] > 0 and fbs[0] > 0:
                tr1_tl_and_fb = True
                h_tr1_vertex_time_res_tl_and_fb.Fill(tr1_res)

            elif tls[0] < 0 and fbs[0] > 0:
                tr1_fb_only = True
                h_tr1_vertex_time_res_fb_only.Fill(tr1_res)

            elif tls[0] > 0 and fbs[0] < 0:
                tr1_tl_only = True
                h_tr1_vertex_time_res_tl_only.Fill(tr1_res)
            
            # TR 2
            if tls[1] > 0 and fbs[1] > 0:
                tr2_tl_and_fb = True
                h_tr2_vertex_time_res_tl_and_fb.Fill(tr2_res)

            elif tls[1] < 0 and fbs[1] > 0:
                tr2_fb_only = True
                h_tr2_vertex_time_res_fb_only.Fill(tr2_res)

            elif tls[1] > 0 and fbs[1] < 0:
                tr2_tl_only = True
                h_tr2_vertex_time_res_tl_only.Fill(tr2_res)

            # TR 3
            if tls[2] > 0 and fbs[2] > 0:
                tr3_tl_and_fb = True
                h_tr3_vertex_time_res_tl_and_fb.Fill(tr3_res)

            elif tls[2] < 0 and fbs[2] > 0:
                tr3_fb_only = True
                h_tr3_vertex_time_res_fb_only.Fill(tr3_res)

            elif tls[2] > 0 and fbs[2] < 0:
                tr3_tl_only = True
                h_tr3_vertex_time_res_tl_only.Fill(tr3_res)

            for tr in range(3):
                if fbs[tr] > 0 and tls[tr] < 0:
                    tr_res = tv[tr] - mc_vt[tr]
                    h_tr_vertex_time_res_fb_only.Fill(tr_res)

                    affected = False
                    if (clsMadeWithFit):
                        if (fbc_t1_madeWithFit[tr] == 1 or fbc_t2_madeWithFit[tr] == 1): affected = True
                    else: 
                        if (fbc_t1_dt[tr]>=singleTrk_dt_min and fbc_t1_dt[tr]<=singleTrk_dt_max and fbc_t1_ns[tr]==1) or \
                            (fbc_t2_dt[tr]>=singleTrk_dt_min and fbc_t2_dt[tr]<=singleTrk_dt_max and fbc_t2_ns[tr]==1) : affected = True
                    
                    if (affected): h_tr_affected_vertex_time_res_fb_only.Fill(tr_res)
                    else: h_tr_unaffected_vertex_time_res_fb_only.Fill(tr_res)

            if (any(i<0 for i in t0)): continue
            tr1_w = 1.0 / vertexTree.tv_err1[j]**2.0
            tr2_w = 1.0 / vertexTree.tv_err2[j]**2.0
            tr3_w = 1.0 / vertexTree.tv_err3[j]**2.0
            ws = tr1_w + tr2_w + tr3_w

            combined = (vertexTree.tv_1[j] * tr1_w + vertexTree.tv_2[j] * tr2_w + vertexTree.tv_3[j] * tr3_w) / ws
            combined_err = np.sqrt((vertexTree.tv_err1[j]**2.0 + vertexTree.tv_err2[j]**2.0 + vertexTree.tv_err3[j]**2.0)/3.0)

            vertex_time_res = combined - vertexTree.tv_mc1[j]

            timing_chi2 = vertexTree.tv_chi2[j]
            # timing_chi2_rnd = vertexTree.tv_chi2_rnd[j]

            # bhabha_ts = np.sqrt(vertexTree.tv_chi2_bhabha[j])
            # bhabha_rnd_ts = np.sqrt(vertexTree.tv_chi2_bhabha_rnd[j])

            # h_tv_chi2.Fill(timing_chi2)
            # h_tv_rnd_chi2.Fill(timing_chi2_rnd)

            # if bhabha_ts > 5: 
            #     printEvent(vertexTree, j)
            #     print "combined time {}".format(combined)

            # h_tv_bhabha_ts.Fill(bhabha_ts)
            # h_tv_rnd_bhabha_ts.Fill(bhabha_rnd_ts)

            h_combined_vertex_time_res.Fill(vertex_time_res)
            h_combined_vertex_time_err.Fill(combined_err)

            affected = False
            if (clsMadeWithFit): 
                if (any(i == 1 for i in fbc_t1_madeWithFit) or any(i == 1 for i in fbc_t2_madeWithFit)): affected = True
            else:
                # any(i>=multiTrk_dt_min and fbc_t1_ns[fbc_t1_dt.index(i)] > 1 for i in fbc_t1_dt)
                # t1Cut = (fbc_t1_ns>1 and fbc_dt1>=multiTrk_dt_min) and fbc_t1_passMultiTrkCut==1
                # t2Cut = (fbc_t2_ns>1 and fbc_dt2>=multiTrk_dt_min) and fbc_t2_passMultiTrkCut==1
                if (any(i>=singleTrk_dt_min and i<=singleTrk_dt_max and fbc_t1_ns[fbc_t1_dt.index(i)] == 1 for i in fbc_t1_dt) or \
                    any(i>=singleTrk_dt_min and i<=singleTrk_dt_max and fbc_t2_ns[fbc_t2_dt.index(i)] == 1 for i in fbc_t2_dt)): affected = True


            # Look at individual tracks as well
            # time at vertex (from i.e. tr1's prediction) - mc time
            # before and after for affected tracks
            # fb only tracks / fb + tl tracks / 1/2 fbc cl's only 


            # if any(i>=singleTrk_dt_min for i in fbc_t1_dt) or any(i>=singleTrk_dt_min for i in fbc_t2_dt):
            #     printEvent(vertexTree, j)


            if (affected): h_affected_combined_vertex_time_res.Fill(vertex_time_res)
            else: h_unaffected_combined_vertex_time_res.Fill(vertex_time_res)

            # No fb only tracks
            if (not tr1_fb_only and not tr2_fb_only and not tr3_fb_only):
                h_combined_vertex_time_res_no_fb_only.Fill(vertex_time_res)
                if (affected): h_affected_combined_vertex_time_res_no_fb_only.Fill(vertex_time_res)
                else: h_unaffected_combined_vertex_time_res_no_fb_only.Fill(vertex_time_res)

            # One fb only track
            one_fb_only = (tr1_fb_only and (not tr2_fb_only and not tr3_fb_only)) or ((not tr1_fb_only and not tr2_fb_only) and tr3_fb_only) or (tr2_fb_only and (not tr3_fb_only and not tr1_fb_only))
            if (one_fb_only):
                h_combined_vertex_time_res_one_fb_only.Fill(vertex_time_res)
                if (affected): h_affected_combined_vertex_time_res_one_fb_only.Fill(vertex_time_res)
                else: h_unaffected_combined_vertex_time_res_one_fb_only.Fill(vertex_time_res)

            # Two fb only tracks
            two_fb_only = (tr1_fb_only and tr2_fb_only and not tr3_fb_only) or (tr1_fb_only and not tr2_fb_only and tr3_fb_only) or (not tr1_fb_only and tr2_fb_only and tr3_fb_only)
            if (two_fb_only):
                h_combined_vertex_time_res_two_fb_only.Fill(vertex_time_res)
                if (affected): h_affected_combined_vertex_time_res_two_fb_only.Fill(vertex_time_res)
                else: h_unaffected_combined_vertex_time_res_two_fb_only.Fill(vertex_time_res)

            # all fb only tracks
            if (tr1_fb_only and tr2_fb_only and tr3_fb_only):
                h_combined_vertex_time_res_all_fb_only.Fill(vertex_time_res)
                if (affected): h_affected_combined_vertex_time_res_all_fb_only.Fill(vertex_time_res)
                else: h_unaffected_combined_vertex_time_res_all_fb_only.Fill(vertex_time_res)


# _ = effSupPlots(h_tv_chi2, h_tv_rnd_chi2, "", "chi2")
# data = effSupPlots(h_tv_bhabha_ts, h_tv_rnd_bhabha_ts, "bhabha", "t_{s}")

# thresholds = np.array([i[0] for i in data])
# tpr = np.array([i[1] for i in data])
# fpr = np.array([i[2] for i in data])

# optimal_idx = np.argmax(tpr - fpr)
# optimal_threshold = thresholds[optimal_idx]

# print (optimal_threshold)
print ("Total vertices {}, skipped vertices {} (due to prop err)".format(total_verts, skipped))

print("ALL")
results = []
results.append(["three long tracks",    (fitGaus(h_combined_vertex_time_res) * 1e3),             h_combined_vertex_time_res.GetEntries()])
results.append(["no fb only tracks",    (fitGaus(h_combined_vertex_time_res_no_fb_only) * 1e3),  h_combined_vertex_time_res_no_fb_only.GetEntries() ])
results.append(["one fb only track",    (fitGaus(h_combined_vertex_time_res_one_fb_only) * 1e3), h_combined_vertex_time_res_one_fb_only.GetEntries()])
results.append(["two fb only tracks",   (fitGaus(h_combined_vertex_time_res_two_fb_only) * 1e3), h_combined_vertex_time_res_two_fb_only.GetEntries()])
results.append(["three fb only tracks", (fitGaus(h_combined_vertex_time_res_all_fb_only) * 1e3), h_combined_vertex_time_res_all_fb_only.GetEntries()])
print(tabulate.tabulate(results, ["Combined 3-track time","Time res at vertex [ps]","Entries in hist"], tablefmt='fancy_grid',floatfmt="4.0f"))

print("UNAFFECTED FB CL's")
results = []
results.append(["three long tracks",    (fitGaus(h_unaffected_combined_vertex_time_res) * 1e3),             h_unaffected_combined_vertex_time_res.GetEntries()])
results.append(["no fb only tracks",    (fitGaus(h_unaffected_combined_vertex_time_res_no_fb_only) * 1e3),  h_unaffected_combined_vertex_time_res_no_fb_only.GetEntries()])
results.append(["one fb only track",    (fitGaus(h_unaffected_combined_vertex_time_res_one_fb_only) * 1e3), h_unaffected_combined_vertex_time_res_one_fb_only.GetEntries()])
results.append(["two fb only tracks",   (fitGaus(h_unaffected_combined_vertex_time_res_two_fb_only) * 1e3), h_unaffected_combined_vertex_time_res_two_fb_only.GetEntries()])
results.append(["three fb only tracks", (fitGaus(h_unaffected_combined_vertex_time_res_all_fb_only) * 1e3), h_unaffected_combined_vertex_time_res_all_fb_only.GetEntries()])
print(tabulate.tabulate(results, ["Combined 3-track time","Time res at vertex [ps]","Entries in hist"], tablefmt='fancy_grid',floatfmt="4.0f"))

print("AFFECTED FB CL's")
results = []
results.append(["three long tracks",    (fitGaus(h_affected_combined_vertex_time_res) * 1e3),             h_affected_combined_vertex_time_res.GetEntries()])
results.append(["no fb only tracks",    (fitGaus(h_affected_combined_vertex_time_res_no_fb_only) * 1e3),  h_affected_combined_vertex_time_res_no_fb_only.GetEntries()])
results.append(["one fb only track",    (fitGaus(h_affected_combined_vertex_time_res_one_fb_only) * 1e3), h_affected_combined_vertex_time_res_one_fb_only.GetEntries()])
results.append(["two fb only tracks",   (fitGaus(h_affected_combined_vertex_time_res_two_fb_only) * 1e3), h_affected_combined_vertex_time_res_two_fb_only.GetEntries()])
results.append(["three fb only tracks", (fitGaus(h_affected_combined_vertex_time_res_all_fb_only) * 1e3), h_affected_combined_vertex_time_res_all_fb_only.GetEntries()])
print(tabulate.tabulate(results, ["Combined 3-track time","Time res at vertex [ps]","Entries in hist"], tablefmt='fancy_grid',floatfmt="4.0f"))

print("INDIVIDUAL TRACK VERTEX RES")
results = []
results.append(["All fb only tracks",        (fitGaus(h_tr_vertex_time_res_fb_only) * 1e3),  h_tr_vertex_time_res_fb_only.GetEntries()])
results.append(["Unaffected fb only tracks", (fitGaus(h_tr_unaffected_vertex_time_res_fb_only) * 1e3),  h_tr_unaffected_vertex_time_res_fb_only.GetEntries()])
results.append(["Affected fb only tracks",   (fitGaus(h_tr_affected_vertex_time_res_fb_only) * 1e3),  h_tr_affected_vertex_time_res_fb_only.GetEntries()])
print(tabulate.tabulate(results, ["Individual track time","Time res at vertex [ps]","Entries in hist"], tablefmt='fancy_grid',floatfmt="4.0f"))

# raw_input("Press Enter to continue...")
print ("Finished...")

output_file.Write()
output_file.Close()

