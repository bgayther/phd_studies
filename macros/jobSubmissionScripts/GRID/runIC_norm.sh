#!/bin/bash
set -x
source /mu3e/install/activate.sh
source /geant4-install/bin/geant4.sh
source /root-install/bin/thisroot.sh

INPUT_FILE="$1"
# SEED=$(printf "%06d" $2)
echo $INPUT_FILE
mu3eAnaICnorm $INPUT_FILE > IC_norm.txt
