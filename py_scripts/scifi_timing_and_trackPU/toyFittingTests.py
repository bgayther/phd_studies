import ROOT
import numpy as np
import scipy.stats as st
from scipy.optimize import curve_fit
import matplotlib
import sys
import matplotlib.pyplot as plt
from collections import defaultdict, OrderedDict
import probfit
import iminuit
from probfit.pdf import johnsonSU
from probfit.plotting import draw_pdf

# f(x) = N1*f1(x1) + N2*f2(x2) + N3*f3(x3)

#  x = mean
# (N_ unit normalised)

# For i in Ncls to fit:
#   Initialise starting positions for x_i
#   fit ULL of f(x) = sum ( N_i * f_i(x_i) )
#   record error on LL


# Test 1
# Generate dummy data from already fitted pdf
# Test accuracy against number of sampled points (shift the points by same amount)
# See if it can estimate mean (offset) correctly, tail is correct

# Test 2
# Draw n points from pdf apply offset, draw n points from another pdf and apply a different offset
# See if it can estimate mean (offset) for these two distributions correctly, tails are correct

def johnsonsu(x, a, b, loc, scale):
    Y = (x - loc)/scale
    Y2 = Y*Y
    norm_pdf = lambda x : np.exp(-(x**2.0)/2.0) / np.sqrt(2*np.pi)
    trm = norm_pdf(a + b * np.log(Y + np.sqrt(Y2+1)))
    pdf = (b/np.sqrt(Y2+1.0))*trm
    return pdf/scale

def johnsonsu_mean(a, b, loc, scale):
    m = loc - scale * np.exp(0.5 / b**2) * np.sinh(a/b)
    return m

def johnsonsu_var(a, b, loc, scale):
    t = np.exp(1.0 / b**2)
    v = 0.5*scale**2 * (t - 1.0) * (t * np.cosh(2.0*a/b) + 1.0)
    return v


# ROOT.RooMsgService.instance().setSilentMode(True)
f = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/model.root")
w = f.Get("w")
# w.Print()

model_fromFit = w.pdf("model")

a_fromFit = w.var("a")
# a.Print()
# a.setConstant(ROOT.kTRUE)

b_fromFit = w.var("b")
# b.Print()
# b.setConstant(ROOT.kTRUE)

loc_fromFit = w.var("loc")
# loc.Print()
# loc.setConstant(ROOT.kTRUE)

scale_fromFit = w.var("scale")
# scale.Print()
# scale.setConstant(ROOT.kTRUE)


x = ROOT.RooRealVar("x", "x", -5, 25)

# Will shift / change this
loc_shift = 3.0
loc = ROOT.RooRealVar("loc", "loc", loc_fromFit.getValV() + loc_shift)

a = ROOT.RooRealVar("a", "a", a_fromFit.getValV())
b = ROOT.RooRealVar("b", "b", b_fromFit.getValV())
scale = ROOT.RooRealVar("scale", "scale", scale_fromFit.getValV())

Y = ROOT.RooFormulaVar("Y","(x-loc)/scale", ROOT.RooArgList(x,loc,scale))
Y2 = ROOT.RooFormulaVar("Y2","pow(Y,2.0)", ROOT.RooArgList(Y))
gauss_input = ROOT.RooFormulaVar("gauss_input","a + b * TMath::Log(Y + sqrt(Y2+1))", ROOT.RooArgList(Y,Y2,a,b))
gauss = ROOT.RooFormulaVar("gauss","exp(-0.5*pow(gauss_input,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input))
johnsonsu_pdf = ROOT.RooFormulaVar("johnsonsu_pdf","(b / sqrt(Y2+1))*gauss", ROOT.RooArgList(Y2,b,gauss))

model = ROOT.RooGenericPdf("model","model", "johnsonsu_pdf/scale", ROOT.RooArgList(johnsonsu_pdf,scale))


# toyData = model.generate(ROOT.RooArgSet(x),10)
# model.fitTo(toyData)

#####################
# ACTUAL MODEL TO FIT
#####################

# Test 1
# loc_toFit = ROOT.RooRealVar("loc_toFit", "loc_toFit", 0.5,-5,5)

# Y_toFit = ROOT.RooFormulaVar("Y_toFit","(x-loc_toFit)/scale", ROOT.RooArgList(x,loc_toFit,scale))
# Y2_toFit = ROOT.RooFormulaVar("Y2_toFit","pow(Y_toFit,2.0)", ROOT.RooArgList(Y_toFit))
# gauss_input_toFit = ROOT.RooFormulaVar("gauss_input_toFit","a + b * TMath::Log(Y_toFit + sqrt(Y2_toFit+1))", ROOT.RooArgList(Y_toFit,Y2_toFit,a,b))
# gauss_toFit = ROOT.RooFormulaVar("gauss_toFit","exp(-0.5*pow(gauss_input_toFit,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_toFit))
# johnsonsu_pdf_toFit = ROOT.RooFormulaVar("johnsonsu_pdf_toFit","(b / sqrt(Y2_toFit+1))*gauss_toFit", ROOT.RooArgList(Y2_toFit,b,gauss_toFit))

# normalisation variable
# frac = ROOT.RooRealVar("frac", "frac", 0.5, 0.01, 10)
# model_toFit = ROOT.RooGenericPdf("model_toFit","model_toFit", "johnsonsu_pdf_toFit/scale", ROOT.RooArgList(johnsonsu_pdf_toFit,scale))
# model_toFit.fitTo(toyData)

# ROOT.RooRandom.randomGenerator().SetSeed(102)
# mcs = ROOT.RooMCStudy(model,ROOT.RooArgSet(x), ROOT.RooFit.FitModel(model_toFit))
# mcs.generateAndFit(10000,10)


# print "Shifted loc = {}, by {}, from original fitted loc of {}".format(loc.getValV(),loc_shift,loc_fromFit.getValV())
# print "New fitted loc was {}, residual difference (pred-obs) was {}".format(loc_toFit.getValV(), loc.getValV() - loc_toFit.getValV())
# loc_toFit.Print()
# frac.Print()


# frameParam = mcs.plotParam(loc_toFit, ROOT.RooFit.FrameRange(0,5), ROOT.RooFit.FrameBins(500))
# frameParam = mcs.plotPull(loc_toFit, ROOT.RooFit.FrameRange(-3,3), ROOT.RooFit.FrameBins(600))
# frameParam = mcs.plotPull(loc_toFit)
# frameParam.SetMinimum(1e-5)
# frameParam = mcs.plotError(loc_toFit)
# frameParam.Draw()

# Plot toy data and pdf in observable x
# frame = x.frame()
# toyData.plotOn(frame)
# model.plotOn(frame)
# model.plotOn(frame, ROOT.RooFit.Normalization(toyData.sumEntries(), ROOT.RooAbsReal.NumEvent))
# model_toFit.plotOn(frame)
# model_toFit.plotOn(frame, ROOT.RooFit.Normalization(toyData.sumEntries(), ROOT.RooAbsReal.NumEvent))
# frame.Draw()
# raw_input("Press Enter to continue...")




# For test 2
# Need way to intialise variables accordingly....
# Could use KDE for finding peaks? and give these as starting points for ... fit?

frame = x.frame()
ROOT.RooRandom.randomGenerator().SetSeed(10)
nhits1 = 3
nhits2 = 3
toyData1 = model.generate(ROOT.RooArgSet(x),nhits1)
toyData1.SetName("first")
print "FIRST CLUSTER LOC"
first_loc = loc.getValV()
loc.Print()
# model.plotOn(frame, ROOT.RooFit.Normalization(toyData1.sumEntries(), ROOT.RooAbsReal.NumEvent))
loc.setVal(loc.getValV() + 4.0)
print "SECOND CLUSTER LOC"
second_loc = loc.getValV()
loc.Print()
toyData2 = model.generate(ROOT.RooArgSet(x),nhits2)
toyData1.SetName("second")
# model.plotOn(frame, ROOT.RooFit.Normalization(toyData2.sumEntries(), ROOT.RooAbsReal.NumEvent))
# toyData1.plotOn(frame)
# toyData2.plotOn(frame)
# frame.Draw()

toyData1.append(toyData2)
# toyData1.Print()


variables = ROOT.RooArgList()
variables_set = ROOT.RooArgSet()
# funcs = ROOT.RooArgList()
pdfs = ROOT.RooArgList()

a = w.var("a")
a.setConstant(ROOT.kTRUE)

b = w.var("b")
b.setConstant(ROOT.kTRUE)

scale = w.var("scale")
scale.setConstant(ROOT.kTRUE)
# variables.add(a)
# variables.add(b)
# variables.add(scale)
def makeJohnsonPDFtoFit(name, loc_guess):
    # a = workspace.var("a")
    # a.setConstant(ROOT.kTRUE)

    # b = workspace.var("b")
    # b.setConstant(ROOT.kTRUE)

    # scale = workspace.var("scale")
    # scale.setConstant(ROOT.kTRUE)

    loc_toFit = ROOT.RooRealVar(name+"loc_toFit", "loc_toFit", loc_guess, -1, 25)

    Y_toFit = ROOT.RooFormulaVar(name+"Y_toFit","(@0-@1)/@2", ROOT.RooArgList(x,loc_toFit,scale))
    Y2_toFit = ROOT.RooFormulaVar(name+"Y2_toFit","pow(@0,2.0)", ROOT.RooArgList(Y_toFit))
    gauss_input_toFit = ROOT.RooFormulaVar(name+"gauss_input_toFit","@0 + @1 * TMath::Log(@2 + sqrt(@3+1))", ROOT.RooArgList(a,b,Y_toFit,Y2_toFit))
    gauss_toFit = ROOT.RooFormulaVar(name+"gauss_toFit","exp(-0.5*pow(@0,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_toFit))
    johnsonsu_pdf_toFit = ROOT.RooFormulaVar(name+"johnsonsu_pdf_toFit","(@0 / sqrt(@1+1))*@2/@3", ROOT.RooArgList(b,Y2_toFit,gauss_toFit,scale))
    model_toFit = ROOT.RooGenericPdf(name+"model_toFit",name+"model_toFit", "@0", ROOT.RooArgList(johnsonsu_pdf_toFit))

    ROOT.SetOwnership(model_toFit, False)
    ROOT.SetOwnership(johnsonsu_pdf_toFit, False)
    ROOT.SetOwnership(gauss_toFit, False)
    ROOT.SetOwnership(gauss_input_toFit, False)
    ROOT.SetOwnership(Y2_toFit, False)
    ROOT.SetOwnership(Y_toFit, False)
    ROOT.SetOwnership(loc_toFit, False)


    variables.add(loc_toFit)
    variables_set.add(loc_toFit)
    # funcs.add(Y_toFit)
    # funcs.add(Y2_toFit)
    # funcs.add(gauss_input_toFit)
    # funcs.add(gauss_toFit)
    # funcs.add(johnsonsu_pdf_toFit)
    pdfs.add(model_toFit)
    model_toFit.Print()
    # return model_toFit
    # return



# loc_toFit1 = ROOT.RooRealVar("loc_toFit1", "loc_toFit1", 2.5,-1,25)

# Y_toFit1 = ROOT.RooFormulaVar("Y_toFit1","(x-loc_toFit1)/scale", ROOT.RooArgList(x,loc_toFit1,scale))
# Y2_toFit1 = ROOT.RooFormulaVar("Y2_toFit1","pow(Y_toFit1,2.0)", ROOT.RooArgList(Y_toFit1))
# gauss_input_toFit1 = ROOT.RooFormulaVar("gauss_input_toFit1","a + b * TMath::Log(Y_toFit1 + sqrt(Y2_toFit1+1))", ROOT.RooArgList(Y_toFit1,Y2_toFit1,a,b))
# gauss_toFit1 = ROOT.RooFormulaVar("gauss_toFit1","exp(-0.5*pow(gauss_input_toFit1,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_toFit1))
# johnsonsu_pdf_toFit1 = ROOT.RooFormulaVar("johnsonsu_pdf_toFit1","(b / sqrt(Y2_toFit1+1))*gauss_toFit1/scale", ROOT.RooArgList(Y2_toFit1,b,gauss_toFit1,scale))

# model_toFit1 = ROOT.RooGenericPdf("model_toFit1","model_toFit1", "johnsonsu_pdf_toFit1/scale", ROOT.RooArgList(johnsonsu_pdf_toFit1,scale))

# loc_toFit2 = ROOT.RooRealVar("loc_toFit2", "loc_toFit2", 5.5,-1,25)

# Y_toFit2 = ROOT.RooFormulaVar("Y_toFit2","(x-loc_toFit2)/scale", ROOT.RooArgList(x,loc_toFit2,scale))
# Y2_toFit2 = ROOT.RooFormulaVar("Y2_toFit2","pow(Y_toFit2,2.0)", ROOT.RooArgList(Y_toFit2))
# gauss_input_toFit2 = ROOT.RooFormulaVar("gauss_input_toFit2","a + b * TMath::Log(Y_toFit2 + sqrt(Y2_toFit2+1))", ROOT.RooArgList(Y_toFit2,Y2_toFit2,a,b))
# gauss_toFit2 = ROOT.RooFormulaVar("gauss_toFit2","exp(-0.5*pow(gauss_input_toFit2,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_toFit2))
# johnsonsu_pdf_toFit2 = ROOT.RooFormulaVar("johnsonsu_pdf_toFit2","(b / sqrt(Y2_toFit2+1))*gauss_toFit2/scale", ROOT.RooArgList(Y2_toFit2,b,gauss_toFit2,scale))

# model_toFit2 = ROOT.RooGenericPdf("model_toFit2","model_toFit2", "johnsonsu_pdf_toFit2/scale", ROOT.RooArgList(johnsonsu_pdf_toFit2,scale))

frac = ROOT.RooRealVar("frac", "frac", 0.5, 0.0, 1)
# joint_model = ROOT.RooGenericPdf("joint_model","joint_model", "frac*johnsonsu_pdf_toFit1/scale + (1-frac)*johnsonsu_pdf_toFit2/scale", ROOT.RooArgList(frac,johnsonsu_pdf_toFit1,johnsonsu_pdf_toFit2,scale))
# joint_model = ROOT.RooAddPdf("joint_model","joint_model", ROOT.RooArgList(model_toFit1,model_toFit2), ROOT.RooArgList(frac))
makeJohnsonPDFtoFit("first", 2.5)
makeJohnsonPDFtoFit("second", 5.5)
# pdfs.at(0)
joint_model = ROOT.RooAddPdf("joint_model","joint_model", pdfs, ROOT.RooArgList(frac))

joint_model.fitTo(toyData1)
# joint_model.Print()

print "First true loc {}, second true loc {}, fitted ones below:".format(first_loc, second_loc)
# pdfs.at(0).Print()
variables.at(0).Print()
variables.at(1).Print()
# loc_toFit1.Print()
# loc_toFit2.Print()
print "Mixing frac expected {}, fitted:".format(nhits1/float(nhits2+nhits1))
frac.Print()

toyData1.plotOn(frame)
joint_model.plotOn(frame)
# joint_model.plotOn(frame, ROOT.RooFit.Components("model_toFit1"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kRed))
# johnsonsu_pdf_toFit1.plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kRed))
# johnsonsu_pdf_toFit2.plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kGreen))
frame.SetMinimum(1e-5)
frame.Draw()

raw_input("Press Enter to continue...")



