#!/bin/bash
set -e

cd /
source /usr/local/root/bin/thisroot.sh
source /usr/local/geant4/bin/geant4.sh

if [[ ! -e /usr/bin/gmake ]]; then ln -sf /usr/bin/make /usr/bin/gmake; fi
if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3.9 /usr/bin/python; fi && \
if [[ ! -e /usr/bin/python-config ]]; then ln -sf /usr/bin/python3.9-config /usr/bin/python-config; fi && \
if [[ ! -e /usr/bin/pip ]]; then ln -sf /usr/bin/pip3.9 /usr/bin/pip; fi

export PATH=/usr/local/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export MIDASSYS=$HOME/packages/midas
export MIDAS_EXPTAB=$HOME/online/exptab
export MIDAS_EXPT_NAME="Mu3e"
export PATH=$PATH:$MIDASSYS/bin
export PYTHONPATH=$PYTHONPATH:$MIDASSYS/python
export NODE_OPTIONS=--openssl-legacy-provider
export XAUTHORITY=/root/.Xauthority 
exec "$@"
