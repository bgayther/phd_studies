#ifndef UTIL_DATAFRAME_HELPERS_HPP_
#define UTIL_DATAFRAME_HELPERS_HPP_

#include <ROOT/RDataFrame.hxx>
#include <TH1D.h>
#include <TH2D.h>
#include "PlotInfo.hpp"

class DataFrameHelpers {
    public:

        using TH2D_ptr = ROOT::RDF::RResultPtr<TH2D>;
        using TH1D_ptr = ROOT::RDF::RResultPtr<TH1D>;

        static TH1D_ptr plot1D(const ROOT::RDF::RNode& RDF, const PlotInfo& plot) {
            ROOT::RDF::RNode data(RDF);
            TH1D_ptr hist;

            if (plot.cut != "") {
                hist = data.Filter(plot.cut).Histo1D({plot.name.c_str(), (plot.title + ";" + 
                                        plot.xtitle + ";" + plot.ytitle).c_str(), 
                                        plot.xbins, plot.xmin, plot.xmax}, plot.varX);
            } else {
                hist = data.Histo1D({plot.name.c_str(), (plot.title + ";" + 
                                        plot.xtitle + ";" + plot.ytitle).c_str(), 
                                        plot.xbins, plot.xmin, plot.xmax}, plot.varX);
            }

            return hist;
        }

        static void plot1D(const ROOT::RDF::RNode& RDF, const PlotInfo& plot, std::vector<TH1D_ptr>& histos) {
            histos.push_back(plot1D(RDF, plot));
        }

        static TH2D_ptr plot2D(const ROOT::RDF::RNode& RDF, const PlotInfo& plot) {
            ROOT::RDF::RNode data(RDF);
            TH2D_ptr hist;
            
            if (plot.cut != "") {
                hist = data.Filter(plot.cut).Histo2D({plot.name.c_str(), (plot.title + ";" + 
                                        plot.xtitle + ";" + plot.ytitle).c_str(), plot.xbins, plot.xmin, plot.xmax, 
                                        plot.ybins, plot.ymin, plot.ymax}, plot.varX, plot.varY);
            } else {
                hist = data.Histo2D({plot.name.c_str(), (plot.title + ";" + 
                                        plot.xtitle + ";" + plot.ytitle).c_str(), plot.xbins, plot.xmin, 
                                        plot.xmax, plot.ybins, plot.ymin, plot.ymax}, plot.varX, plot.varY);
            }

            return hist;
        }

        static void plot2D(const ROOT::RDF::RNode& RDF, const PlotInfo& plot, std::vector<TH2D_ptr>& histos) {
            histos.push_back(plot2D(RDF, plot));
        }

        // TODO:
        /*
        template<typename Lambda>
        struct DataFrameArgs {
            std::string name = "";
            // bool cut = [] () {};
            Lambda cut;
            // std::string cut = "";
            std::vector<std::string> branches;

            DataFrameCut(std::string name_, Lambda&& cut_, std::vector<std::string> branches_) {
                name = name_;
                cut = cut_;
                branches = branches_;
            };
        };


        struct mottSegsDataFrame {
            std::string tree = "segs";
            ROOT::RDataFrame segs;

            segsDataFrame(const std::vector<std::string>& files) {
                segs(tree.c_str(), files);
            };

            ROOT::RDF::RNode getFilteredDataFrame(const std::vector<DataFrameArgs>& definitions, const std::vector<DataFrameArgs>& definitions) {
                auto filteredDataFrame = std::make_unique<ROOT::RDF::RNode>(segs);
                // filteredDataFrame = segs.Define("lam010", "lam01[0]")
                //                         .Define("pCorr", corrSegsMomCharge, {"p", "mc_pid"})
                //                         .Define("p_res", p_seg_res, {"pCorr", "mc_p"})
                for (const auto& d : definitions) filteredDataFrame = 
            };

            std::map<int, ROOT::RDF::RNode> getPerTrackDataFrame() {
                std::map<int, ROOT::RDF::RNode> trackSegFilterMap;
                for (auto i : trackTypes) {
                    auto cut = [i] (const int &nhit, const float& chi2, const int& s6n, const int& s6n_0, const int& s8n, const int& seq) {
                        bool corr_nhits = (nhit==i);
                        if (!corr_nhits) return false;
                        bool cut = false;
                        if (i == 4) {
                            cut = (s6n > 0 || s8n > 0);
                        } else if (i == 6) {
                            // cut = (s8n_0 > 0 || seq > 0); // TODO: can only use for later files
                            cut = (seq > 0);
                        } else if (i == 8) {
                            cut = (s6n_0 > 0 || seq > 0);
                        }
                        return !cut;
                    };
                    trackSegFilterMap.insert(std::make_pair(i, filteredSegs.Filter(cut, {"nhit", "chi2", "s6n", "s6n_0", "s8n", "seq"})));
                }
                return trackSegFilterMap;
            };
        }
        */



        DataFrameHelpers() {};
        ~DataFrameHelpers() {};
};

#endif /* UTIL_DATAFRAME_HELPERS_HPP_ */
