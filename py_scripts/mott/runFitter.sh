#!/bin/sh
#PBS -N mottFitter
#PBS -o /unix/muons/users/bgayther/phd_studies/py_scripts/mott/logs
#PBS -e /unix/muons/users/bgayther/phd_studies/py_scripts/mott/logs
#PBS -l ncpus=16
#PBS -q medium
#PBS -m b
#PBS -M zcembga@ucl.ac.uk
set -e

source /unix/muons/users/bgayther/phd_studies/ucl_setup_on_cvmfs.sh
cd /unix/muons/users/bgayther/phd_studies/py_scripts/mott/

MICHELFILES=/unix/muons/users/bgayther/MichelDecays/batch/sim_michel*/*/results/mu3e_trirec_*_standard_muon_beam.root
DIR=/unix/muons/users/bgayther/MottScattering/batch
SIM_TAG="12MeV_mott_gen_Carbon_250keV_mom_spread"

ANA_TAG="Test"
python mottFitter.py -michelFiles $MICHELFILES \
                     -mottPosFiles ${DIR}/sim_positron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
                     -mottElecFiles ${DIR}/sim_electron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
                     -AngularCorrection \
                     -DblTurnFits \
                     -MottPeakFits \
                     -MottPeakVsLambdaFits \
                     -MottPeakVsPhiFits \
                     -MottPeakVsZPCAFits \
                     -MottPeakMCFits \
                     -ncpu 16 \
                     -nfiles 1 \
                     -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/mottFitter${ANA_TAG}
                    #  -v \
                    #  -SelectDoubleCrossing \
                    #  -SelectSingleCrossing \

# ANA_TAG="DoubleCrossing"
# python mottFitter.py -michelFiles $MICHELFILES \
#                      -mottPosFiles ${DIR}/sim_positron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -mottElecFiles ${DIR}/sim_electron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -AngularCorrection \
#                      -DblTurnFits \
#                      -MottPeakFits \
#                      -MottPeakVsLambdaFits \
#                      -MottPeakVsPhiFits \
#                      -MottPeakVsZPCAFits \
#                      -MottPeakMCFits \
#                      -ncpu 16 \
#                      -SelectDoubleCrossing \
#                      -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/mottFitter${ANA_TAG}

# ANA_TAG="SingleCrossing"
# python mottFitter.py -michelFiles $MICHELFILES \
#                      -mottPosFiles ${DIR}/sim_positron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -mottElecFiles ${DIR}/sim_electron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -AngularCorrection \
#                      -DblTurnFits \
#                      -MottPeakFits \
#                      -MottPeakVsLambdaFits \
#                      -MottPeakVsPhiFits \
#                      -MottPeakVsZPCAFits \
#                      -MottPeakMCFits \
#                      -ncpu 16 \
#                      -SelectSingleCrossing \
#                      -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/mottFitter${ANA_TAG}

# with momentum scaling
# python mottFitter.py -michelFiles $MICHELFILES \
#                      -mottPosFiles ${DIR}/sim_positron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -mottElecFiles ${DIR}/sim_electron_*_${SIM_TAG}/*/results/mu3e_trirec_*_${SIM_TAG}.root \
#                      -ncpu 10 \
#                      -MomentumScaleCorrection \
#                      -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/mottFitterScaledMomentum

# no energy correction
# REC_TAG="no_ecorr_rec"
# MICHELFILES=/unix/muons/users/bgayther/MichelDecays/batch/rec_michel_*_${REC_TAG}/*/results/mu3e_trirec_*_standard_muon_beam_${REC_TAG}.root
# python mottFitter.py -michelFiles $MICHELFILES \
#                      -mottPosFiles ${DIR}/rec_positron_*_${SIM_TAG}_${REC_TAG}/*/results/mu3e_trirec_*.root \
#                      -mottElecFiles ${DIR}/rec_electron_*_${SIM_TAG}_${REC_TAG}/*/results/mu3e_trirec_*.root \
#                      -ncpu 10 \
#                      -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/mottFitterNoECorr