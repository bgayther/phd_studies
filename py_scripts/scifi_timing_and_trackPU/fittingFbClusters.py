import ROOT
import numpy as np
import time
import scipy.stats as st
from scipy.optimize import curve_fit
from collections import defaultdict
import argparse
import logging
import os
import sys
import copy
# import tabulate
import matplotlib
import matplotlib.pyplot as plt
# from mpl_toolkits import mplot3d
# from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from fibreClasses import FbCluster, Frame, MCHit
from tqdm import tqdm
import pathlib
sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input sim file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of frames to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='fbHitTimingFit', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e/tests/data", help='path/to/sim_file.root (not including file though)')
args = parser.parse_args()

pathlib.Path(args.path).mkdir(parents=True, exist_ok=True)

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792458 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n

############## SIM RES ###############
simTree = ROOT.TChain("mu3e")
mchits = ROOT.TChain("mu3e_mchits")

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    simTree.Add(file)
    mchits.Add(file)

nevents = simTree.GetEntries()
if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.path + "/" + args.output + ".root", 'RECREATE')

simTree.SetBranchStatus("*", 0)
mchits.SetBranchStatus("*", 0)

frame = Frame(simTree, mchits)


sipmTotal = 0
Nbins = 500

# Histos
# hHitTimeZCorrected = ROOT.TH1F("hHitTimeZCorrected", "Time Resolution per hit (Z corrected);t_{hit} - t_{mc} [ns]", Nbins, 0, 15)
def getDiffIds(ids):
    ntids = len(ids)
    if ntids == 1 and 0 in ids:
        return [-1, -1]
    nhids = 0
    for tid, hid in ids.items():
        nhids_ = len(hid)
        if nhids_ > nhids:
            nhids = nhids_
    return [ntids, nhids]

# data = []
x = ROOT.RooRealVar("x","", -10, 70)
rds = ROOT.RooDataSet("rds","rds",ROOT.RooArgSet(x))

h_nhits_per_side = ROOT.TH1D("h_nhits_per_side", ";Number of hits;Counts", 9, 0.5, 9.5)

skip = args.skip
for i in tqdm(range(skip, nevents+skip)):
    simTree.GetEntry(i)

    traj_tids, _ = frame.traj.getTids()
    clusters = []
    threshold = 0

    # Loop through all sipm columns
    prev_col = -1
    for sipmno in range(frame.sipm.getN()):
        if frame.sipm.amplitude[sipmno] <= 0 or frame.sipm.getMcTime(sipmno) < 0 or frame.sipm.crosstalk[sipmno] > 0: continue
        # if frame.sipm.col[sipmno] == prev_col: continue
        sipmTotal += 1
        prev_col = frame.sipm.col[sipmno]
        # print(f"Processing sipm {sipmno}, column {frame.sipm.col[sipmno]}, mc_time {frame.sipm.getMcTime(sipmno)}, amplitude {frame.sipm.amplitude[sipmno]}")

        ids = frame.sipm.getIds(sipmno) # [tid][hids]
        [ntids, nhids] = getDiffIds(ids)
        tid = frame.sipm.getTid(sipmno)

        cluster = FbCluster(frame.sipm, sipmno, ids, 0) # Just cluster of size 1 (one hit)

        # check if any of the tid/hid pair is already present in a cluster
        # Merges hits into existing clusters or creates new ones....
        merged = 0
        # photon threshold level
        if(frame.sipm.amplitude[sipmno] > threshold):
            for tid, hids in ids.items():
                for hid in hids:
                    for cl in clusters:
                        # Should this just use track id? Or hit id as well?
                        # Or could change it to more closely resemble reconstruction
                        if tid in cl.ids and hid in cl.ids[tid] and cl.side == cluster.side and cl.ribbon == cluster.ribbon:
                            cl.add(cluster) # add hit to cluster
                            merged = 1
                            break
            if not merged:
                # Additional code - only use tids from selected traj_ids
                for tid, hids in cluster.ids.items():
                    if tid in traj_tids:
                        clusters.append(copy.deepcopy(cluster)) # create new cluster


        if (ntids==1 and nhids==1 and tid in traj_tids): 
            zPos = frame.sipm.getZPos(sipmno)
            side = frame.sipm.sipm[sipmno]%2
            # d_side = fb_len_half - pow(-1, side) * zPos
            if (side==0): d_side = fb_len_half - zPos
            else: d_side = fb_len_half + zPos
            mc_time = frame.sipm.getMcTime(sipmno)
            hit_time = frame.sipm.time[sipmno]
            # print(f"Side {side}, zPos {zPos}, d_side {d_side}, mc_time {mc_time}, hit_time {hit_time}")

            z_corr_time = hit_time - d_side/cn
            # print(zPos, sipmTotal, mc_time, hit_time, z_corr_time, frame.sipm.col[sipmno])

            # print("DEBUG MPPC {}, side {}, col {}, ampl {}, hit time {}, mc time {}, zPos {}, z_corr_time {}, crosstalk {}, TIDS/HIDS {}".format(frame.sipm.sipm[sipmno], side, frame.sipm.col[sipmno], frame.sipm.amplitude[sipmno],  hit_time, mc_time, zPos, z_corr_time, frame.sipm.crosstalk[sipmno], frame.sipm.getIds(sipmno)))
            x.setVal(z_corr_time - mc_time)
            rds.add(ROOT.RooArgSet(x))

            # if (x > 0 and x <= 15): data.append(x)
            # data.append(z_corr_time - mc_time)
            # hHitTimeZCorrected.Fill(x)

    for cl in clusters:
        ntids, nhids = cl.getPileup()
        if (ntids==1 and nhids==1): h_nhits_per_side.Fill(cl.Nhits)


# data = np.array(data)
# dh = ROOT.RooDataHist("dh", "dh", ROOT.RooArgList(x), ROOT.RooFit.Import(hHitTimeZCorrected))

# x.setBins(10000,"fft")

# dh.plotOn(frame)
frame = x.frame(ROOT.RooFit.Range(0, 9.0))
x.setRange("fitRange", 0, 9.0)
frame.SetYTitle("Events")
frame.SetXTitle("T_{Hit} - T_{MC} [ns]")
frame.SetTitle("")

a = ROOT.RooRealVar("a", "a", -1.5, -5, 5)
b = ROOT.RooRealVar("b", "b", 0.9, 0.01, 5)
loc = ROOT.RooRealVar("loc", "loc", 0.8, -5.0, 5.0)
scale = ROOT.RooRealVar("scale", "scale", 0.4, 0.01, 5.0)

# a = ROOT.RooRealVar("#gamma", "a", -1.5, -5, 5)
# b = ROOT.RooRealVar("#delta", "b", 0.9, 0.01, 5)
# loc = ROOT.RooRealVar("#mu", "loc", 0.8, -5.0, 5.0)
# scale = ROOT.RooRealVar("#lambda", "scale", 0.4, 0.01, 5.0)

model = ROOT.RooJohnson("model","model",x,loc,scale,a,b)

model.fitTo(rds, ROOT.RooFit.Range("fitRange"))

rds.plotOn(frame)
model.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.NormRange("fitRange"), ROOT.RooFit.Range("fitRange"))
model.paramOn(frame, ROOT.RooFit.Format("NEU", ROOT.RooFit.AutoPrecision(1)), ROOT.RooFit.Layout(0.6,0.83,0.85), ROOT.RooFit.ShowConstants(False))
frame.getAttText().SetTextSize(0.95*frame.GetYaxis().GetLabelSize())
can = ROOT.TCanvas("can", "")
frame.Draw()
can.SetLogy(1)
can.SaveAs(args.path+"/timeRes.pdf")
can.SetLogy(0)
can.SaveAs(args.path+"/timeRes_lin.pdf")

# rename a var to "a"


w = ROOT.RooWorkspace("w")
getattr(w,'import')(model)
w.Print()
w.writeToFile("model.root")

 
# Option 1
# Basic gauss + exp
# # Fit a Gaussian p.d.f to the data
# mean = ROOT.RooRealVar("mean", "mean", 0.1, 0.1, 5.0)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.01, 0.01, 5.0)
# gauss = ROOT.RooGaussian("gauss", "gauss", x, mean, sigma)

# c_exp = ROOT.RooRealVar("c_exp","Exp arg constant", -1/2.8) 
# expon = ROOT.RooExponential("exp","exp", x, c_exp)

# frac1 = ROOT.RooRealVar("frac1", "frac1", 0.01, 0., 0.5)
# frac2 = ROOT.RooRealVar("frac2", "frac2", 0.49, 0., 0.5)

# model = ROOT.RooAddPdf("model","model",ROOT.RooArgList(gauss, expon), ROOT.RooArgList(frac1, frac2))
# model.fitTo(dh)
# model.plotOn(frame)
# expon.plotOn(frame)
# gauss.plotOn(frame)

# Option 2
# mean = ROOT.RooRealVar("mean", "mean", 0.5, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 0.5)
# gm = ROOT.RooGaussModel("gm1", "gauss model 1", x, mean, sigma)

# tau = ROOT.RooRealVar("tau", "tau", 0.1, 3.0)
# res_model = ROOT.RooDecay("res_model", "decay", x, tau, gm, ROOT.RooDecay.SingleSided)
# res_model.fitTo(dh)
# res_model.plotOn(frame)



# Option 3
# tau = ROOT.RooRealVar("tau","tau", 2.8)
# mean = ROOT.RooRealVar("mean", "mean", 1.0, 0.5, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 0.1, 1.0)
# gaussm = ROOT.RooGaussModel("gaussm","gaussm",x,mean,sigma) 
# basis = ROOT.RooFormulaVar("basis","exp(-@0/@1)",ROOT.RooArgList(x,tau))
# decayConvGauss = gaussm.convolution(basis,x)
# decayConvGauss.plotOn(frame, ROOT.RooFit.NormRange("full"),ROOT.RooFit.Range("full"))
# normGauss = decayConvGauss.createIntegral(ROOT.RooArgSet(x), ROOT.RooArgSet(x),"full")
# normGauss.plotOn(frame)

# OPtion 4
# mean = ROOT.RooRealVar("mean", "mean", 0.1, 2.0)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 0.9)
# tau = ROOT.RooRealVar("tau", "tau", 2.8)
# rsf =  ROOT.RooRealVar("rsf", "rsf", 0.01, 5.0)
# gaussm = ROOT.RooGExpModel("gaussm","gaussm",x,mean,sigma,tau, rsf) 
# gaussm.fitTo(dh)
# gaussm.plotOn(frame)



# Option 5

# mean = ROOT.RooRealVar("mean", "mean", 0.5, 2.0)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 2.0)
# alpha = ROOT.RooRealVar("alpha", "alpha", -0.1, -50)
# n = ROOT.RooRealVar("n", "n", 1, 0, 100)
# cb = ROOT.RooCBShape("cb", "", x, mean, sigma, alpha, n)
# # cb.fitTo(dh)
# # cb.plotOn(frame)
# a = ROOT.RooRealVar("a", "a", -100.0, 100.0)
# b = ROOT.RooRealVar("b", "b", 0.1, 5.0)
# # beta = ROOT.RooRealVar("beta", "beta", 0.1, 5.0)
# third = ROOT.RooFormulaVar("third","a * pow(x, -b)", ROOT.RooArgList(x,a,b))

# right = 1.7
# boundary = ROOT.RooRealVar("boundary", "boundary", 1.7,1.5,3.0)

# model = ROOT.RooGenericPdf("model","model", "(x<=boundary)*cb + (x>boundary)*third ", ROOT.RooArgList(x,cb,third,boundary))

# model.fitTo(dh)
# model.plotOn(frame)


# Option 6

# mean = ROOT.RooRealVar("mean", "mean", 0.5, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 2.0)
# tail = ROOT.RooRealVar("tail", "tail", -0.1, -100)
# nb = ROOT.RooNovosibirsk("cb", "", x, mean, sigma, tail)
# nb.fitTo(dh)
# nb.plotOn(frame)


# Option 7

# mean = ROOT.RooRealVar("mean", "mean", 0.1, 0.1, 5.0)
# sigma1 = ROOT.RooRealVar("sigma1", "sigma", 0.01, 1.0)
# gauss1 = ROOT.RooGaussModel("gauss1", "gauss", x, mean, sigma1)

# # mean2 = ROOT.RooRealVar("mean2", "mean", 0.1, 0.1, 5.0)
# sigma2 = ROOT.RooRealVar("sigma2", "sigma", 0.1, 3.0)
# gauss2 = ROOT.RooGaussModel("gauss2", "gauss", x, mean, sigma2)
# frac_core = ROOT.RooRealVar("frac_core", "frac", 0., 1.)

# gaussModel = ROOT.RooAddModel("gaussModel","model",ROOT.RooArgList(gauss1, gauss2), ROOT.RooArgList(frac_core))

# c_exp = ROOT.RooRealVar("c_exp","Exp arg constant", 0.1, 3.0) 
# res_model = ROOT.RooDecay("res_model", "decay", x, c_exp, gaussModel, ROOT.RooDecay.SingleSided)
# res_model.fitTo(dh)
# res_model.plotOn(frame)



# OPTION 9 SCIPY
# binwidth = 0.2
# print("min {}, max {}".format(min(data),max(data)))
# bins=np.arange(min(data), max(data) + binwidth, binwidth)
# n, bins, _ = plt.hist(data, bins=1000, density=True)
# invgauss
# mu, loc, scale = st.invgauss.fit(data)
# print("mu {}, loc {}, scale {}".format(mu,loc,scale))
# data_for_pdf = np.linspace(data.min(), data.max(), 500)
# pdf_fitted = st.invgauss.pdf(data_for_pdf, mu, loc, scale)
# # plt.plot(pdf_fitted, label="invGauss")
# plt.plot(data_for_pdf, pdf_fitted)
# plt.savefig("pdf.pdf")

# params = st.johnsonsu.fit(data)
# data_for_pdf = np.linspace(data.min(), data.max(), 500)
# pdf_fitted = st.johnsonsu.pdf(data_for_pdf, *params)
# print (params)
# plt.plot(pdf_fitted, label="invGauss")
# plt.plot(data_for_pdf, pdf_fitted)
# plt.savefig("pdf.pdf")

# def func(x, params):
#     return st.johnsonsu.pdf(x, *params)

# y = func(data_for_pdf, params)
# popt, pcov = curve_fit(st.johnsonsu.pdf, data_for_pdf, y, p0)


# Option 10, iminuit/probfit
# import probfit
# import iminuit 

# Option 10
# x.setRange("peak",0,2)
# x.setRange("tail",2,15)
# x.setRange("full",0,15)

# mean = ROOT.RooRealVar("mean", "mean", 0.1, 2.5)
# shape = ROOT.RooRealVar("shape", "shape", 0.01, 10.0)

# first = ROOT.RooFormulaVar("first","sqrt(@1/(2*TMath::Pi()*pow(@0,3.0)))", ROOT.RooArgList(x,shape))
# second = ROOT.RooFormulaVar("second","-1.0*@2*pow(@0-@1,2.0)/(2*pow(@1,2.0)*@0)", ROOT.RooArgList(x,mean,shape))
# model = ROOT.RooGenericPdf("model","model", "first*exp(second)", ROOT.RooArgList(first,second))

# model.fitTo(dh)
# model.plotOn(frame)

# Option x

# mean = ROOT.RooRealVar("mean", "mean", 0.1, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 5.0)
# c_exp = ROOT.RooRealVar("c_exp","Exp arg constant", 0.01, 4.0) 
# c_exp2 = ROOT.RooRealVar("c_exp2","Exp arg constant", 0.01, 4.0) 

# first = ROOT.RooFormulaVar("first","1.0/(@0*sqrt(2.0*TMath::Pi()))", ROOT.RooArgList(sigma))
# frac = ROOT.RooFormulaVar("frac", "(@0 - @1)/@2;", ROOT.RooArgList(x,mean,sigma))
# second = ROOT.RooFormulaVar("second","-0.5 * pow(@0, 2.0)", ROOT.RooArgList(frac))

# third = ROOT.RooFormulaVar("third","-1.0*(@0/@1)", ROOT.RooArgList(x,c_exp))
# fourth = ROOT.RooFormulaVar("fourth","-1.0*(@0/@1)", ROOT.RooArgList(x,c_exp2))

# right = 2.0
# boundary = ROOT.RooRealVar("boundary", "boundary", 1.0, right)
# boundary2 = ROOT.RooRealVar("boundary2", "boundary2", right, 5.0)

# model = ROOT.RooGenericPdf("model","model", "(x<boundary)*first*exp(second) + (x>boundary)*(x<boundary2)*exp(third) + (x>=boundary2)*exp(fourth)", ROOT.RooArgList(x,first,second,third,fourth,boundary,boundary2))

# model.fitTo(dh)
# model.plotOn(frame)


# Option 11 (convert hist to pdf directly)
# Need to smooth!!
# pdf = ROOT.RooHistPdf("pdf","your pdf",x,dh,2)
# pdf.plotOn(frame)


# Option
# side = 1.7
# x.setRange("peak",0,side)
# x.setRange("tail",side,15)
# x.setRange("full",0,15)

# mean = ROOT.RooRealVar("mean", "mean", 0.1, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 5.0)

# gauss = ROOT.RooGaussian("gauss", "gauss", x, mean, sigma)
# gauss.fitTo(dh, ROOT.RooFit.Range("peak"))
# gauss.plotOn(frame, ROOT.RooFit.NormRange("peak"),ROOT.RooFit.Range("peak"))


# Option

# mean = ROOT.RooRealVar("mean", "mean", 0.1, 2.5)
# sigma = ROOT.RooRealVar("sigma", "sigma", 0.1, 5.0)

# first = ROOT.RooFormulaVar("first","1.0/(@0*sqrt(2.0*TMath::Pi()))", ROOT.RooArgList(sigma))
# frac = ROOT.RooFormulaVar("frac", "(@0 - @1)/@2;", ROOT.RooArgList(x,mean,sigma))
# second = ROOT.RooFormulaVar("second","-0.5 * pow(@0, 2.0)", ROOT.RooArgList(frac))

# a = ROOT.RooRealVar("a", "a", 0.1, 10.0)
# b = ROOT.RooRealVar("b", "b", 0.1, 5.0)
# beta = ROOT.RooRealVar("beta", "beta", 0.1, 5.0)
# third = ROOT.RooFormulaVar("third","a * pow(x, -b) * exp(-beta*x)", ROOT.RooArgList(x,a,b,beta))

# right = 1.7
# boundary = ROOT.RooRealVar("boundary", "boundary", 1.7,1.5,3.0)

# model = ROOT.RooGenericPdf("model","model", "(x<boundary)*first*exp(second) + (x>=boundary)*third ", ROOT.RooArgList(x,first,second,third,boundary))

# model.fitTo(dh)
# model.plotOn(frame)






# c = ROOT.TCanvas("", "", 800, 800)
# frame.Draw()

# mean.Print()
# sigma.Print()
# c_exp.Print()
# frac1.Print()
# frac2.Print()


# input("Press Enter to continue...")
# input("Press Enter to continue...")
print ("Finished...")


# Save h_nhits_per_side as a pdf
can = ROOT.TCanvas()
h_nhits_per_side.SetLineWidth(2)
h_nhits_per_side.Draw()
can.SetLogy()
can.SaveAs(args.path + "/nhits_per_side.pdf")


output_file.Write()
output_file.Close()