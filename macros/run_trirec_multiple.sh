#!/bin/bash
start=$(date +%s.%N)
echo "STARTING RECO"

source ../../install/activate.sh

INPUT_DIR="large_bkg_data"
OUTPUT_DIR="/unix/muons/users/bgayther/testing_code/offset_reco_bkg_data/scenario_3"
# OUTPUT_DIR="large_bkg_data/test_dir"
FILENAME_STUB="mu3e_sorted_"

for f in ${INPUT_DIR}/${FILENAME_STUB}*.root
do
    echo "OPENING $f"
    tmp=${f#${INPUT_DIR}/${FILENAME_STUB}}   # remove prefix 
    i=${tmp%_*}   # remove suffix 
    j=${i%.root}
    # echo $j
    mu3eTrirec \
    --input $f \
    --output ${OUTPUT_DIR}/run$j-reco_offset.root
done


# mu3eTrirec \
#     --input large_bkg_data/run000043-sort.root \
#     --output /unix/muons/users/bgayther/testing_code/offset_reco_bkg_data/run000043-reco_test.root
#     # --output /unix/muons/users/bgayther/testing_code/no_offset_reco_bkg_data/run000043-reco.root

end=$(date +%s.%N)
runtime=$(python -c "print(${end} - ${start})")

echo "Runtime was $runtime"