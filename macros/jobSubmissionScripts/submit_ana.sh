function submit {
    echo "\n Submitting signal ana for file(s) ${VERTEX_FILES}, run ${RUN}, sample ${SAMPLE}, nsig ${NSIGNAL}"
	qsub -q short -v VERTEX_FILES,SIM_TAG,SAMPLE,VERTEX_TAG,OUTDIR,NSIGNAL run_ana.sh
	sleep 0.1
}

rm ana_logs/*
cd ana_logs
cp ../run_ana.sh .

export OUTDIR=/unix/muons/users/bgayther/SignalDecays/batch

export SIM_TAG="sig_and_muon_beam"
export SAMPLE="signal"

export VERTEX_TAG=""
export VERTEX_FILES=""
export NSIGNAL=0

# DESIRED_RUN=101

# Standard (no scaling)
VERTEX_TAG="no_momentum_scaling"
VERTEX_FILES=${OUTDIR}/sim_${SAMPLE}_*_${SIM_TAG}/*/results/mu3e_vertex_*.root
SIGNAL_NORMS=${OUTDIR}/sim_${SAMPLE}_*_${SIM_TAG}/*/signalnorm.txt

for SIGFILE in $SIGNAL_NORMS; do
    ((NSIGNAL+=`cat ${SIGFILE}`))
done

submit

# With scaling
VERTEX_TAG="momentum_scaling"
VERTEX_FILES=${OUTDIR}/vertex_${SAMPLE}_*_${SIM_TAG}_${VERTEX_TAG}/*/results/mu3e_vertex_*.root

submit

# echo $VERTEX_FILES
# if compgen -G "$VERTEX_FILES" > /dev/null; then
#     # for FILENAME in $FILELIST; do
#     #     # echo $FILENAME
#     #     TRUNC=${FILENAME#*mu3e_trirec_*}
#     #     export RUN=${TRUNC%%_*}
#     #     export REC_FILE=${FILENAME}
#     #     submit
#     # done
#     for SIGFILE in $SIGNAL_NORMS; do
#         ((NSIGNAL+=`cat ${SIGFILE}`))
#     done
#     submit
# fi

cd ..