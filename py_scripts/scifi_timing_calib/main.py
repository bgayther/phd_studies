import ROOT
import numpy as np
import time
import argparse
import logging
import os
from helperFuns import *

startTime = time.time()

parser = argparse.ArgumentParser(description='SciFi Calibration Analysis',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of events to process; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='timing_analysis_param_scan', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/offset_reco_bkg_data", help='path/to/reco_file.root (not including file though)')
parser.add_argument('-c','--correct', type=int, default=0, help='Use found offsets to correct data? (default is no correction, 1 for correction)')
args = parser.parse_args()

os.chdir(args.path)

ROOT.gStyle.SetOptStat(0)
# root_file = ROOT.TFile.Open(args.inputs[0])
# ROOT.gROOT.ProcessLine("segs->MakeClass()")
# ROOT.gROOT.ProcessLine(".L segs.C+") # make sure to do segs->MakeClass() before this if running for the first time!
events = ROOT.TChain("segs")
for file in args.inputs:
    print "Adding {} for analysis".format(file)
    events.Add(file)

# events.MakeClass()
ROOT.gROOT.ProcessLine(".L segs.C+")
Events = ROOT.segs()
Events.Init(events)

nevents = Events.fChain.GetEntries() 

if args.nevents >= 0: nevents = args.nevents

output_file = ROOT. TFile(args.output+".root", 'RECREATE')

# could read this from a file instead...
mc_offsets_dict = dict([(0, 0.0), (1, 1.0), (2, 0.0), (3, 3.0), (4, 0.0), (5, 0.0), (6, 1.5), (7, 0.0), (8, 0.0), (9, 0.0), (10, 0.0), (11, 0.5)])
# mc_offsets_dict = dict([(0, 0.0), (1, 0.0), (2, 0.0), (3, 0.0), (4, 0.0), (5, 0.0), (6, 0.0), (7, 0.0), (8, 0.0), (9, 0.0), (10, 0.0), (11, 0.0)])

offsets_map, sigmas = findOffsets(Events, nevents, mc_offsets_dict, 2000, 40.0, 10.0, 2.0)



# For param scanning...too slow in python though...
""" tracks = countTracks(Events, nevents) 

window_min = int((tracks/6.0)*0.1)
window_max = int((tracks/6.0)*0.9)
windows = np.linspace(window_min, window_max, num=10, dtype=int)

t_diff_window_off_min = 5.0
t_diff_window_off_max = 100.0
t_diff_window_offs = np.linspace(t_diff_window_off_min, t_diff_window_off_max, num=20)

t_diff_window_ma_min = 5.0
t_diff_window_ma_max =  15.0
t_diff_window_ma = np.linspace(t_diff_window_ma_min, t_diff_window_ma_max, num=10)

t_diff_adj_factor_min = 1.0
t_diff_adj_factor_max = 10.0
t_diff_adj_factor = np.linspace(t_diff_adj_factor_min, t_diff_adj_factor_max, num=10) 

window_sigmas = []
for window in windows:
    offsets_map, sigmas = findOffsets(Events, nevents, mc_offsets_dict, window, 40.0, 10.0, 2.0)
    window_sigmas.append(np.mean(sigmas))


Do this with TGraph instead
print( "\n\n - - - MIN MEAN SIGMA FOUND {:.3f}, WINDOW SIZE {:.2f} % - - - \n\n".format(min(window_sigmas), windows[window_sigmas.index(min(window_sigmas))]*(6.0/tracks)*100.0, ))
 """

# if args.correct:
#     ROOT.gROOT.SetBatch(ROOT.kTRUE)
#     correctForOffsets(Events, nevents, offsets_map)

output_file.Write()
output_file.Close()

print ("Took {:.2f} s".format(time.time() - startTime))
