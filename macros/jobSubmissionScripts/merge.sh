DIR=/unix/muons/users/bgayther/MottScattering/batch

SAMPLES="electron positron"
# TAGS="20MeV_1mm_gold_plane_target 50MeV_1mm_gold_plane_target"
# TAGS="20MeV_1mm_lead_plane_target 50MeV_1mm_lead_plane_target"
TAGS="20MeV_5mm_lead_plane_target 50MeV_5mm_lead_plane_target"

# TAGS="20MeV_noMottBall 50MeV_noMottBall"
# TAGS="20MeV 50MeV"
# TAGS="20MeV 35MeV 50MeV"
#flex1 flex2"

for SAMPLE in $SAMPLES ; do
for TAG in $TAGS ; do
    hadd -j 8 ${DIR}/mu3e_sorted_${SAMPLE}_${TAG}.root ${DIR}/sim_${SAMPLE}_*_${TAG}/*/data/mu3e_sorted_*_${TAG}.root
    hadd -j 8 ${DIR}/mu3e_trirec_${SAMPLE}_${TAG}.root ${DIR}/sim_${SAMPLE}_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
done
done