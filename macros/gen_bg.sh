#!/bin/bash
#PBS -q mediumc7
#PBS -o /unix/muons/users/bgayther/testing_code/master/mu3e/tests/large_bkg_data
#PBS -e /unix/muons/users/bgayther/testing_code/master/mu3e/tests/large_bkg_data
#PBS -l walltime=06:30:00 
#PBS -l mem=2000mb 
#PBS -l nodes=1 
set -e

start=$(date +%s.%N)
echo "STARTING SIMS AND SORT"

if [ -z "$PBS_O_WORKDIR" ]
then
      echo "\$PBS_O_WORKDIR is NULL"
else
      echo "\$PBS_O_WORKDIR is NOT NULL"
      cd $PBS_O_WORKDIR
fi

source ../../install/activate.sh

mkdir -p data

NEV=100116
VERBOSE=0

cp digi_bg.json digi.json
# cp pipeline_bg.mac run.mac

for i in 2 3 4
do
    echo "SIMS SCRIPT, $i'TH ITERATION"
    RUN_NO=$i
    # echo '/run/verbose ' $VERBOSE  > run.mac
    # echo '/event/verbose ' $VERBOSE  > run.mac
    echo '/mu3e/run/setRunNumber ' $RUN_NO  > run.mac
    echo '/run/beamOn ' $NEV >> run.mac
    run=`printf %06i $RUN_NO`
    infile=`echo 'data/mu3e_run_'$run'.root'`
    outfile=`echo 'data/mu3e_sorted_'$run'.root'`
    mu3eSim run.mac
    mu3eSort \
        $infile \
        $outfile 

    rm run.mac
    rm $infile
    # mu3eSim pipeline_bg.mac
    # mv data/mu3e_run_000043.root data/mu3e_run_000043_$i.root
    # mu3eSort \
    #     data/mu3e_run_000043_$i.root \
    #     data/run000043-sort_$i.root
done

mv data/*.root large_bkg_data/
rm -r data/

rm digi.json

end=$(date +%s.%N)
runtime=$(python -c "print(${end} - ${start})")

echo "Runtime was $runtime"
