function submit {
    echo "\n Submitting reconstruction for file ${SIM_FILE}, run ${RUN}, sample ${SAMPLE}"
	# qsub -q medium -v SIM_FILE,REC_CONF_OPT,SIM_TAG,SAMPLE,REC_TAG,OUTDIR,RUN run_rec.sh
	sleep 0.1
}

rm rec_logs/*
cd rec_logs
cp ../run_rec.sh .

# DIR=/unix/muons/users/bgayther/MottScattering/batch
# export OUTDIR=$DIR

# export SIM_TAG="52MeV_mott_gen_Carbon_250keV_mom_spread"
# SAMPLES="positron electron"

# export REC_TAG="no_ecorr_rec"
# export REC_CONF_OPT="trirec.use_ecorr = false"

# DESIRED_RUN=101

# # ENERGIES = (12 17 22 32 42 52)

# for SAMPLE in $SAMPLES; do
#     export SAMPLE=$SAMPLE
#     FILELIST=${DIR}/sim_${SAMPLE}_${DESIRED_RUN}_*_${SIM_TAG}/*/data/mu3e_sorted_*.root
#     # echo ${#FILELIST[@]}
# 	# echo $FILELIST
#     if compgen -G "$FILELIST" > /dev/null; then
#         for FILENAME in $FILELIST; do
#             # echo $FILENAME
#             TRUNC=${FILENAME#*mu3e_sorted_*}
#             export RUN=${TRUNC%%_*}
#             export SIM_FILE=${FILENAME}
#             submit
#         done
#     fi
# done

DIR=/unix/muons/users/bgayther/TimingStudyData/batch
export OUTDIR=$DIR
export SIM_TAG="muon_beam"

# export REC_TAG="multiTrkPU"
# export REC_CONF_OPT="
# trirec {
#   fb {
#       recluster_using_likelihood = true
#       recluster_multiTrk_clusters = true
#       fit_max_trackPU_density = 3
#       assign_using_tof = true
#   } # fb
# } # trirec
# "

export REC_TAG="singleTrkPU"
export REC_CONF_OPT="
trirec {
  fb {
      recluster_using_likelihood = true
      recluster_singleTrk_clusters = true
      fit_max_trackPU_density = 3
      assign_using_tof = true
  } # fb
} # trirec
"

# export REC_TAG="defaultReco"
# export REC_CONF_OPT=""

export SAMPLE=""
DESIRED_RUN=1

FILELIST=${DIR}/sim_${DESIRED_RUN}_*_${SIM_TAG}/*/data/mu3e_sorted_*.root
# echo ${#FILELIST[@]}
# echo $FILELIST
if compgen -G "$FILELIST" > /dev/null; then
    for FILENAME in $FILELIST; do
        # echo $FILENAME
        TRUNC=${FILENAME#*mu3e_sorted_*}
        export RUN=${TRUNC%%_*}
        export SIM_FILE=${FILENAME}
        submit
    done
fi

cd ..