import ROOT
import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

inputFile = "/unix/muons/users/bgayther/MichelDecays/batch/sim_michel_50_100000_standard_muon_beam/job_5743680.farm-a00.hep.ucl.ac.uk/data/mu3e_sorted_50_standard_muon_beam.root"

sensorDict = {}
sensorTree = ROOT.TChain("alignment/sensors")
sensorTree.Add(inputFile)

for i in range(sensorTree.GetEntries()):
    sensorTree.GetEntry(i)
    sensorID = sensorTree.sensor
    sensorDict[sensorID] = {"v" : {"x" : sensorTree.vx, "y" : sensorTree.vy, "z" : sensorTree.vz},
                            "drow" : {"x" : sensorTree.rowx, "y" : sensorTree.rowy, "z" : sensorTree.rowz},
                            "dcol" : {"x" : sensorTree.colx, "y" : sensorTree.coly, "z" : sensorTree.colz},
                            "nrow" : sensorTree.nrow,
                            "ncol" : sensorTree.ncol,
                            "width" : sensorTree.width,
                            "length" : sensorTree.length,
                            "thickness" : sensorTree.thickness,
                            "pixelSize" : sensorTree.pixelSize}
# print(sensorDict)
# Save sensorDict as a json file

# import json
# with open('sensorDict.json', 'w') as fp:
    # json.dump(sensorDict, fp)

SiVars = ["Nhit", "hit_pixelid", "traj_PID", "hit_mc_i", "hit_mc_n"]
# TODO: Add fibre and tile variables
#       Would need to add alignment data for fibres and tiles
#       Then decode the fibre and tile hit information and get x,y,z positions
# FbVars = ["Nfibremppc", "fibremppc_mppc", "fibremppc_col", "fibremppc_mc_i", "fibremppc_mc_n", "fibremppc_amplitude", "fibremppc_time"]
# TlVars = ["Ntilehit", "tilehit_tile", "tilehit_time", "tilehit_edep", "tilehit_mc_i", "tilehit_mc_n"]

data = ROOT.RDataFrame("mu3e", inputFile).Range(10).AsNumpy(columns=SiVars)
"""
    Nhit: Number of pixel hits
    hit_pixelid: ID of the pixel hit (includes sensor ID (bit 16 and up) and row and column (lower 16 bits)
    hit_mc_i: Index into the mu3e_mchits tree for looking up truth information
    hit_mc_n: Number of truth information records in the mu3e_mchits tree for this hit
"""

mc_hits = ROOT.RDataFrame("mu3e_mchits", inputFile).AsNumpy(columns=["det", "tid", "pdg", "hid", "hid_g", "edep", "time"])
""" 
    det: Detector that was hit. Encoding 1) pixel, 2) fibre, 3) fibre.mppc 4) tile
    tid: Unique trajectory ID
    pdg: Particle type encoded using the PDG scheme
    hid: Hit number (of corresponding detector) along trajectory, negative if trajectory is radially inwards moving
    hid_g: Global hit id (pixel hid)
    edep: Energy deposition of the hit
    time: Time of the hit, relative to run start
"""

#  decode sensorID
def decodePixelID(sensorID):
    pix_id = (sensorID >> 16) & 0xFFFF
    col = int((pix_id >> 8) & 0xFF)
    row = int((pix_id >> 0) & 0xFF)
    return col, row

# (x,y,z) = sensor.v + sensor.drow * (0.5f + row) + sensor.dcol * (0.5f + col)
# Read in the data and calculate the (x,y,z) position of each hit in the event and save it to data as three new columns (np.array)
def calculateHitPosition(data):
    # print(data)
    x_pos = []
    y_pos = []
    z_pos = []
    for i in range(len(data["Nhit"])):
        x_pos.append([])
        y_pos.append([])
        z_pos.append([])

        for j in range(data["Nhit"][i]):
            sensorID = data["hit_pixelid"][i][j]
            # Select bits 16-31 of sensorID
            sensor = sensorDict[sensorID >> 16]
            col, row = decodePixelID(sensorID)
            # print(sensorID, sensorID>>16, col, row)
            x = sensor["v"]["x"] + sensor["drow"]["x"] * (0.5 + row) + sensor["dcol"]["x"] * (0.5 + col)
            y = sensor["v"]["y"] + sensor["drow"]["y"] * (0.5 + row) + sensor["dcol"]["y"] * (0.5 + col)
            z = sensor["v"]["z"] + sensor["drow"]["z"] * (0.5 + row) + sensor["dcol"]["z"] * (0.5 + col)
            x_pos[i].append(x)
            y_pos[i].append(y)
            z_pos[i].append(z)
    return x_pos, y_pos, z_pos

x_pos, y_pos, z_pos = calculateHitPosition(data)
data["x_pos"] = x_pos
data["y_pos"] = y_pos
data["z_pos"] = z_pos

# Calculate transverse radius of each hit
data["rtHit"] = np.sqrt(data["x_pos"]**2 + data["y_pos"]**2)

# Calculate phi of each hit
data["phiHit"] = np.arctan2(data["y_pos"], data["x_pos"])

# calculate lambda of each hit
data["lambdaHit"] = np.arctan2(data["z_pos"], data["rtHit"])

# Remove all instance of traj_PID != 13 or traj_PID != 11 (and corresponding index in mc_hits)
# def removeNonMuonElectron(data):
#     for i in range(len(data["Nhit"])):
#         for j in range(data["Nhit"][i]):
#             if data["traj_PID"][i][j] != 13 and data["traj_PID"][i][j] != 11:
#                 data["Nhit"][i] -= 1
#                 data["hit_pixelid"][i].pop(j)
#                 data["traj_PID"][i].pop(j)
#                 data["hit_mc_i"][i].pop(j)
#                 data["hit_mc_n"][i].pop(j)
#                 data["x_pos"][i].pop(j)
#                 data["y_pos"][i].pop(j)
#                 data["z_pos"][i].pop(j)
#     return data

# print(mc_hits["tid"][0])
# print(data["hit_mc_i"][0])
# print(data["hit_mc_n"][0])
# print(mc_hits["tid"][data["hit_mc_i"][0][0]])

# Assign truth ID (np array) to each hit
def assignTruthData(data, mc_hits):
    truthID = []
    truthPID = []
    for i in range(len(data["Nhit"])):
        truthID.append([])
        truthPID.append([])
        for j in range(data["Nhit"][i]):
            truthID[i].append([])
            truthPID[i].append([])
            for k in range(data["hit_mc_n"][i][j]):
                truthID[i][j].append(mc_hits["tid"][data["hit_mc_i"] + k])
                truthPID[i][j].append(mc_hits["pdg"][data["hit_mc_i"] + k])
    return truthID, truthPID

data["truthID"], data["truthPID"]  = assignTruthData(data, mc_hits)

# Plot y vs x for first event with truthIDs labelled
def plotSiHits(data, event):
    fig, ax = plt.subplots()
    for i in range(data["Nhit"][event]):
        ax.scatter(data["x_pos"][event][i], data["y_pos"][event][i], label=data["truthID"][event][i])
    ax.legend()
    plt.savefig('test.pdf')

# plotSiHits(data, 0)

# Make graph neural network
# Node is hit (x, y, z)
# edge is hit to hit (so a track/trajectory) attribute is just a weight (- sign if radially inwards moving, + sign if radially outwards moving)
# Aim is to correctly assign as many hits to the correct trajectory as possible

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_geometric.data import Data, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool
    
