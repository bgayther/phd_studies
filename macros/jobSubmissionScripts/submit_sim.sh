
function submit {
    echo "\n Submitting events: $EVENTS, sample: $SAMPLE, tag: $TAG, SIM_CONF_OPT: $SIM_CONF_OPT"
    for (( XRUN=$RUNSTART ; XRUN<($NJOB+RUNSTART) ; XRUN++ )); do
	echo "Submitting run $XRUN"
    export RUN=$XRUN
	qsub -q medium -v RUN,EVENTS,SAMPLE,TAG,SIM_CONF_OPT,REC_CONF_OPT,VERTEX_CONF_OPT -m e -M zcembga@ucl.ac.uk run_sim.sh
	sleep 0.1
    done
}

# need to export to make visible to run_sub.sh
export NJOB=1
export EVENTS=50000
# export EVENTS=2400000
# export EVENTS=100000
export SAMPLE=0
# sample: 0=signal, 1=IC, 2=positron beam, 3=electron beam
export TAG=""
export SIM_CONF_OPT=""
export REC_CONF_OPT=""
export VERTEX_CONF_OPT=""
export RUNSTART=1
export XRUN=0
# export PIPELINE=0
# 0 = full, 1 = sim only, 2 = sim + sort, 3 = sim + sort + sig norm
# 4 = sim + sort + rec, 5 = sim + sort + sig norm + rec
# export RM_SIM_FILE=0 
# 0 = keep all, 1 = rm sim (sort) file

rm logs/*
cd logs
cp ../run_sim.sh .

#########################################

#IC
# SAMPLE=1
# TAG="std"
# RUNSTART=10
# submit

# For timing study
# SAMPLE=4
# EVENTS=100000
# TAG="muon_beam"
# SIM_CONF_OPT=", \"digi.write.truth\" : 2"
# submit

# signal
# SAMPLE=0
# TAG="sig_and_muon_beam"
# SIM_CONF_OPT=""
# submit

#positrons/electrons
# SAMPLES="2 3"

# TAG="52MeV_standard_250keV_mom_spread"
# SIM_CONF_OPT=",
#      \"digi.beam.momentum.mean\" : 52,
#      \"digi.beam.momentum.spread\" : 0.250
#      "

# for SAMPLE in $SAMPLES; do 
#     submit
# done

# TAG="52MeV_standard_no_degrader_250keV_mom_spread"
# SIM_CONF_OPT=",
#      \"detector.beampipe.degraders.1.thickness\" : 1e-6,
#      \"digi.beam.momentum.mean\" : 52,
#      \"digi.beam.momentum.spread\" : 0.250
#      "

# for SAMPLE in $SAMPLES; do 
#     submit
# done

# TAG="52MeV_mott_gen_Oxygen_250keV_mom_spread"
# SIM_CONF_OPT=",
#     \"digi.beam.momentum.mean\" : 52,
#     \"digi.beam.momentum.spread\" : 0.250,
#     \"digi.beam.beam_profile_mode\" : 3,
#     \"digi.beam.mottGenerator.targetAtom\" : \"O\"
#     "

# for SAMPLE in $SAMPLES; do 
#     submit
# done

# ENERGIES=(12 17 22 32 42 52)

# for i in ${!ENERGIES[@]};
# do
#     ENERGY=${ENERGIES[$i]}
#     TAG="${ENERGY}MeV_mott_gen_Carbon_250keV_mom_spread"
#     # echo $TAG
#     SIM_CONF_OPT=",
#         \"digi.beam.momentum.mean\" : ${ENERGY},
#         \"digi.beam.momentum.spread\" : 0.250,
#         \"digi.beam.beam_profile_mode\" : 3,
#         \"digi.beam.mottGenerator.targetAtom\" : \"C\"
#         "
#     for SAMPLE in $SAMPLES; do 
#         submit
#     done
# done


# TAG="52MeV_mott_gen_Carbon_250keV_mom_spread"
# SIM_CONF_OPT=",
#     \"digi.beam.momentum.mean\" : 52,
#     \"digi.beam.momentum.spread\" : 0.250,
#     \"digi.beam.beam_profile_mode\" : 3,
#     \"digi.beam.mottGenerator.targetAtom\" : \"C\"
#     "

# for SAMPLE in $SAMPLES; do 
#     submit
# done

cd ..
