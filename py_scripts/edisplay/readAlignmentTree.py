import json
import ROOT
import argparse
import os

parser = argparse.ArgumentParser(description='Read alignment tree from file and produce json for event display',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--input', dest="inputFile", type=str, required=True, help='Input reco file names')
parser.add_argument('-hole-file','--hole-file', dest='holes', type=str, required=False, help='Input holes conf file')
parser.add_argument('--path', type=str, default="", help='path/to/sim_file.root (not including file)')
args = parser.parse_args()

holes = []
if args.holes:
    with open(args.holes) as file:
        print("Holes file {} given".format(file))
        holesData = json.load(file)
        print(holesData["holes"])
        for chip, v in holesData["holes"].items():
            holes.append(int(chip))

if (args.path != ""): os.chdir(args.path)

inFile = ROOT.TFile.Open(args.inputFile)
cfg = json.loads(str(inFile.Get("config").GetString())) # if needed
alignment = inFile.GetDirectory("alignment")

sensors = alignment.Get("sensors")

alignmentData = []
# efficiencyData = {"chipEfficiencies" : {}}

highEffLaddersL0 = [1,2,5]
highEffLaddersL1 = [2,7,8]

layer0 = [
        0, 1, 2, 62, 61, 60, 3, 4, 5, 65, 64, 63, 6, 7, 8, 68, 67, 66, 9, 10, 11, 71, 70, 69, 12, 13, 14, 74, 73, 72,
        15,	16,	17,	77,	76,	75, 18,	19,	20,	80,	79,	78, 21,	22,	23,	83,	82,	81
        ]

layer1 = [
        24,	25,	26,	86,	85,	84, 27,	28,	29,	89,	88,	87, 30,	31,	32,	92,	91,	90, 33,	34,	35,	95,	94,	93, 36,	37,	38,
        98,	97,	96, 39,	40,	41,	101, 100, 99, 42, 43, 44, 104, 103, 102, 45, 46, 47, 107, 106, 105, 48, 49, 50, 110,
        109, 108, 51, 52, 53, 113, 112, 111
        ]

def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

print("LAYER0:")
for group in chunker(layer0, 6): print(group)

print("LAYER1:")
for group in chunker(layer1, 6): print(group)

for i in sensors:
    if sensors.sensor in holes: continue

    sensorID = (sensors.sensor & 0x1F) + (sensors.sensor & 0x3E0) + (sensors.sensor & 0x7C00)
    assert(sensors.sensor == sensorID)

    layer = (sensors.sensor>>10) & 0x1F
    ladder = (sensors.sensor>>5) & 0x1F
    chip = sensors.sensor & 0x1F
    if(layer == 0): ladder = int(ladder / 8) * 4 + ladder % 8
    if(layer == 1): ladder = int(ladder / 8) * 6 + ladder % 8

    if (layer == 0): runId = layer0[(chip-1)+6*ladder]
    if (layer == 1): 
        if (ladder>=4 and ladder < 10): runId = layer1[(chip-1)+6*(ladder-1)]
        elif (ladder>=10): runId = layer1[(chip-1)+6*(ladder-2)]
        else: runId = layer1[(chip-1)+6*ladder]
    print("runId {}, sensorId {}, layer {}, ladder {}, chip {}".format(runId, sensors.sensor, layer, ladder, chip))

    sensorData = {
        "simChip": sensors.sensor,
        "runChip" : runId,
        "id" : runId,
        "layer" : layer,
        "ladder" : ladder,
        "chip" : chip,
        "v": {
            "x": sensors.vx,
            "y": sensors.vy,
            "z": sensors.vz
        },
        "drow": {
            "x": sensors.rowx,
            "y": sensors.rowy,
            "z": sensors.rowz
        },
        "dcol": {
            "x": sensors.colx,
            "y": sensors.coly,
            "z": sensors.colz
        },
        "nrow": sensors.nrow,
        "ncol": sensors.ncol,
        "width": sensors.width,
        "length": sensors.length,
        "thickness": sensors.thickness,
        "pixelSize": sensors.pixelSize
    }
    alignmentData.append(sensorData)
    # if sensors.sensor>>10 
    # if ((layer==0 and ladder in highEffLaddersL0) or (layer==1 and ladder in highEffLaddersL1)): eff = 0.999
    # else: eff = 0.8
    # efficiencyData["chipEfficiencies"][str(sensors.sensor)] = eff

# with open('chipToEffs.json', 'w') as outfile:
#     json.dump(efficiencyData,  outfile, indent=4)

# deadRunChips = []

# for data in alignmentData:
#    if (data["simSensorId"] in holesData): deadRunChips.append(data["cosRunSensorId"])

# print(deadRunChips)

with open('sensors.json', 'w') as outfile:
    json.dump(alignmentData,  outfile, indent=4)

# TODO: this for fibres/tiles in future