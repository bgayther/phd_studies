import argparse
import pathlib
import time
import ROOT
import sys
import os

from mottFitter import getConfig
import quickMottSim

sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers
from combineTH1 import overlayMultipleHistos

# ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/cpp_scripts/include/LambdaHelpers.hpp")

# TODO: separate the acceptance requiring sim files, and the acceptance requiring reco files

if __name__ == "__main__":
    startTime = time.time()

    parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-simFiles', type=str, nargs='+', required=True, help='Input sim file names')
    parser.add_argument('-recoFiles', type=str, nargs='+', required=True, help='Input reco file names')
    parser.add_argument('-ncpu', type=int, default=8, required=False, help='Number of cpu')
    parser.add_argument('-output', type=str, default='mottAcceptanceResults', required=False, help='Output file name')
    parser.add_argument('-outputGraphsDir', type=str, default='./graphs', required=True, help='output path')
    args = parser.parse_args()

    print("Starting script")

    ROOT.EnableImplicitMT(args.ncpu)
    ROOT.gROOT.SetBatch(True)
    outputGraphsDir = args.outputGraphsDir + "/" # check if / at end?
    pathlib.Path(outputGraphsDir).mkdir(parents=True, exist_ok=True)

    sim_file = args.simFiles
    reco_file = args.recoFiles
            
    # for file in reco_file:
    #     # check if file contains TTree "frames" and "mc_tracks"
    #     if not ROOT.TFile(file).Get("frames") or not ROOT.TFile(file).Get("mc_tracks"):
    #         print("File {} does not contain TTree frames or mc_tracks".format(file))
    #         reco_file.remove(file)
    
    print(reco_file)

    cfg = getConfig(sim_file[0])
    energy = float(cfg["digi"]["beam"]["momentum"]["mean"])
    min_theta = float(cfg["digi"]["beam"]["mottGenerator"]["min_theta"])
    max_theta = float(cfg["digi"]["beam"]["mottGenerator"]["max_theta"])

    min_lam = -1.5 #np.pi/2-max_theta
    max_lam = +1.5 #np.pi/2-min_theta
    lam_bins = 151 #int((max_lam - min_lam)/2.0)
    
    lam_binning = (lam_bins, min_lam, max_lam)
    phi_binning = (268, -3.35, 3.35)

    print(f"Energy={energy}, min_theta={min_theta:.2f}, max_theta={max_theta:.2f}, min_lam={min_lam:.2f}, max_lam={max_lam:.2f}")

    # selection = f"traj_PID==-11&&traj_vr<=19.0&&abs(traj_vz)<=50.0&&traj_theta>={min_theta}&&traj_theta<={max_theta}"
    # sim_df = ROOT.RDataFrame("mu3e", sim_file).Define("traj_pr", "ROOT::VecOps::hypot(traj_px, traj_py)") \
    #                                           .Define("traj_lam", "ROOT::VecOps::atan(traj_pz / traj_pr)") \
    #                                           .Define("traj_theta", "M_PI/2.0 - traj_lam") \
    #                                           .Define("traj_phi", "ROOT::VecOps::atan2(traj_py, traj_px)") \
    #                                           .Define("traj_vr", "ROOT::VecOps::hypot(traj_vx, traj_vy)") \
    #                                           .Define("selection", selection) \
    #                                           .Define("good_traj_lam", "traj_lam[selection]") \
    #                                           .Define("good_traj_theta", "traj_theta[selection]") \
    #                                           .Define("good_traj_phi", "traj_phi[selection]")


    # h_sim_lam = sim_df.Histo1D(("h_sim_theta", ";#lambda_{MC} [rad];Counts", *lam_binning), "good_traj_lam")
    # h_sim_phi = sim_df.Histo1D(("h_sim_phi", ";#phi_{MC} [rad];Counts", *phi_binning), "good_traj_phi")

    # reco_df = ROOT.RDataFrame("segs", reco_file).Filter("selectMCPosTrack(mc_pid, mc_type)").Filter("mc_vr<=19.0&&abs(mc_vz)<=50.0&&mc_prime==1")
    # mc_reco_df = ROOT.RDataFrame("segs_mc", reco_file).Filter("selectMCPosTrack(mc_pid, mc_type)").Filter("mc_vr<=19.0&&abs(mc_vz)<=50.0&&mc_prime==1")
    
    selection = "mc_vr<=19.1&&abs(mc_vz)<=50.1&&mc_prime==1&&mc_pid==-11"
    s4_plus_selection = selection + "&&nhit>=4"
    short_but_not_long_selection = selection + "&&nhit==4"
    long_selection = selection + "&&nhit>=6"
    frames_df = ROOT.RDataFrame("frames", reco_file).Define("s4_plus_selection", s4_plus_selection) \
                                                    .Define("long_selection", long_selection) \
                                                    .Define("short_but_not_long_selection", short_but_not_long_selection) \
                                                    .Define("good_short_not_long_lam", "mc_lam[short_but_not_long_selection]") \
                                                    .Define("good_short_not_long_vz", "mc_vz[short_but_not_long_selection]") \
                                                    .Define("good_short_lam", "mc_lam[s4_plus_selection]") \
                                                    .Define("good_short_vz", "mc_vz[s4_plus_selection]") \
                                                    .Define("good_long_lam", "mc_lam[long_selection]") \
                                                    .Define("good_long_vz", "mc_vz[long_selection]")

    selection = "vr<=19.1&&abs(vz)<=50.1&&pid==-11"
    mc_tracks_df = ROOT.RDataFrame("mc_tracks", reco_file).Define("selection", selection).Define("good_lam", "lam[selection]").Define("good_vz", "vz[selection]")

    h_mc_tracks_lam_vs_z = mc_tracks_df.Histo2D(("h_mc_tracks_short_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "good_vz", "good_lam")
    h_frames_short_lam_vs_z = frames_df.Histo2D(("h_frames_short_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "good_short_vz", "good_short_lam")
    h_frames_short_not_long_lam_vs_z = frames_df.Histo2D(("h_frames_short_not_long_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "good_short_not_long_vz", "good_short_not_long_lam")
    h_frames_long_lam_vs_z = frames_df.Histo2D(("h_frames_long_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "good_long_vz", "good_long_lam")

    # h_reco_S4_lam_vs_z = reco_df.Filter("nhit==4").Histo2D(("h_reco_S4_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "mc_vz", "mc_lam")
    # h_mc_reco_S4_lam_vs_z = mc_reco_df.Filter("nhit==4").Histo2D(("h_mc_reco_S4_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "mc_vz", "mc_lam")

    # h_reco_long_lam_vs_z = reco_df.Filter("nhit>=6").Histo2D(("h_reco_long_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "mc_vz", "mc_lam")
    # h_mc_reco_long_lam_vs_z = mc_reco_df.Filter("nhit>=6").Histo2D(("h_mc_reco_long_lam_vs_z", ";V_{z} [mm];#lambda_{MC} [rad]", 120, -60, 60, *lam_binning), "mc_vz", "mc_lam")

    # h_reco_S4_lam = reco_df.Filter("nhit==4").Histo1D(("h_reco_S4_lam", ";#lambda_{MC} [rad];Counts", *lam_binning), "mc_lam")
    # h_reco_L6_lam = reco_df.Filter("nhit==6").Histo1D(("h_reco_L6_lam", ";#lambda_{MC} [rad];Counts", *lam_binning), "mc_lam")
    # h_reco_L8_lam = reco_df.Filter("nhit==8").Histo1D(("h_reco_L8_lam", ";#lambda_{MC} [rad];Counts", *lam_binning), "mc_lam")

    # h_reco_S4_phi = reco_df.Filter("nhit==4").Histo1D(("h_reco_S4_phi", ";#phi_{MC} [rad];Counts", *phi_binning), "mc_phi")
    # h_reco_L6_phi = reco_df.Filter("nhit==6").Histo1D(("h_reco_L6_phi", ";#phi_{MC} [rad];Counts", *phi_binning), "mc_phi")
    # h_reco_L8_phi = reco_df.Filter("nhit==8").Histo1D(("h_reco_L8_phi", ";#phi_{MC} [rad];Counts", *phi_binning), "mc_phi")

    # h_sim_lam = h_sim_lam.GetValue()
    # h_sim_phi = h_sim_phi.GetValue()
    # h_reco_S4_lam_vs_z = h_reco_S4_lam_vs_z.GetValue()
    # h_mc_reco_S4_lam_vs_z = h_mc_reco_S4_lam_vs_z.GetValue()
    # h_reco_long_lam_vs_z = h_reco_long_lam_vs_z.GetValue()
    # h_mc_reco_long_lam_vs_z = h_mc_reco_long_lam_vs_z.GetValue()
    h_frames_short_lam_vs_z = h_frames_short_lam_vs_z.GetValue()
    h_mc_tracks_lam_vs_z = h_mc_tracks_lam_vs_z.GetValue()
    h_frames_long_lam_vs_z = h_frames_long_lam_vs_z.GetValue()
    h_frames_short_not_long_lam_vs_z = h_frames_short_not_long_lam_vs_z.GetValue()
    # h_reco_S4_lam = h_reco_S4_lam.GetValue()
    # h_reco_L6_lam = h_reco_L6_lam.GetValue()
    # h_reco_L8_lam = h_reco_L8_lam.GetValue()

    # h_reco_S4_phi = h_reco_S4_phi.GetValue()
    # h_reco_L6_phi = h_reco_L6_phi.GetValue()
    # h_reco_L8_phi = h_reco_L8_phi.GetValue()

    # NScattered = h_sim_lam.GetEntries()
    # print(f"NScattered={NScattered}")

    # c = ROOT.TCanvas()
    # c.SetLogy(True)
    # h_sim_phi.Draw()
    # c.SaveAs(outputGraphsDir+"trajPhi.pdf")

    # overlayMultipleHistos([h_sim_phi, h_reco_S4_phi, h_reco_L6_phi, h_reco_L8_phi],
    #                       ["Simulated", "S4", "L6", "L8"],
    #                       (0.20,0.70,0.35,0.85),
    #                       ";#phi_{MC} [rad]; Counts",
    #                       outputGraphsDir+"acc_phi", ymin=1.0, ymax=h_sim_phi.GetMaximum() + 1000, logy=True
    #                       )

    # overlayMultipleHistos([h_sim_lam, h_reco_S4_lam, h_reco_L6_lam, h_reco_L8_lam],
    #                       ["Simulated", "S4", "L6", "L8"],
    #                       (0.20,0.70,0.35,0.85),
    #                       ";#lambda_{MC} [rad]; Counts",
    #                       outputGraphsDir+"acc_lam", ymin=1.0, logy=True
    #                       )

    # theta_range = np.linspace(min_theta, max_theta, 100)
    # diff_cross_sec_C, scattered_mom_C = quickMottSim.mott_lab(theta_range, energy, *quickMottSim.CARBON)

    # indices = np.where(diff_cross_sec_C == inf) + np.where(diff_cross_sec_C <= 10**-10)
    # diff_cross_sec_C = np.delete(diff_cross_sec_C, indices)
    # theta_range = np.delete(theta_range, indices)

    # diff_cross_sec_C /= float(sum(diff_cross_sec_C)) # normalise
    # diff_cross_sec_C *= NScattered # normalise
    # lam_range = np.pi/2.0 - theta_range
    # # print(diff_cross_sec_C, sum(diff_cross_sec_C))
    # # print(lam_range)

    c = ROOT.TCanvas()
    # # c.SetLogy(1)

    # leg = ROOT.TLegend(0.20,0.65,0.35,0.85)

    # gr = ROOT.TGraph(len(diff_cross_sec_C), lam_range, diff_cross_sec_C)
    # gr.SetTitle("")
    # gr.GetYaxis().SetTitle("Counts")
    # gr.GetXaxis().SetTitle("#lambda_{MC} [rad]")
    # # gr.Scale(1. / NScattered)
    # gr.SetLineWidth(2)

    # h_sim_lam.SetMaximum(max(diff_cross_sec_C)+50)
    # h_sim_lam.GetXaxis().SetRangeUser(0.0, 1.4)
    # h_sim_lam.SetFillColor(ROOT.kC0)
    # h_sim_lam.SetLineWidth(2)
    # h_sim_lam.SetFillStyle(3244)

    # # h_reco_S4_lam.SetLineColor(ROOT.kC1)
    # # h_reco_S4_lam.SetFillColor(ROOT.kC1)
    # # h_reco_S4_lam.SetLineWidth(2)
    # # h_reco_S4_lam.SetFillStyle(3001)

    # leg.AddEntry(gr, "Theoretical", "L")
    # leg.AddEntry(h_sim_lam, "Simulated", "L")
    # # leg.AddEntry(h_reco_S4_lam, "Reco S4", "L")

    # h_sim_lam.Draw("HIST")
    # # h_reco_S4_lam.Draw("HIST SAME")
    # gr.Draw("L")
    # leg.Draw()

    # c.SaveAs(outputGraphsDir+"AccGraph.pdf")

    # hTheory = ROOT.TH1D("hTheory", ";#lambda_{MC} [rad];Ratio", *lam_binning)

    # for xsec, lam in zip(diff_cross_sec_C, lam_range):
    #   hTheory.Fill(lam, xsec)

    # # hRatio = ROOT.TH1D("ratio", ";#lambda_{MC} [rad];Ratio", *lam_binning)
    # # for idx, lam in enumerate(lam_range):
    # #   lam_bin = h_sim_lam.FindBin(lam)
    # #   hRatio.Fill(lam, h_sim_lam.GetBinContent(lam_bin) / diff_cross_sec_C[idx])

    # # hRatio.SetMaximum(2)
    # # hRatio.GetXaxis().SetRangeUser(0.0, 1.4)
    # # hRatio.Draw("P")

    # h3 = h_sim_lam.Clone("h3")
    # h3.GetYaxis().SetTitle("Ratio (theory/simulation)")
    # h3.Sumw2()
    # h3.Divide(hTheory)
    # h3.GetXaxis().SetRangeUser(0.0, 1.4)
    # h3.SetMaximum(2.5)
    # h3.Draw("ep")
    # c.SaveAs(outputGraphsDir+"AccRatio.pdf")

    # h_ratio_lam_vz_z = h_reco_S4_lam_vs_z.Clone()
    # h_ratio_lam_vz_z.Divide(h_mc_reco_S4_lam_vs_z)
    # h_ratio_lam_vz_z.Draw("colz")
    # h_ratio_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    # c.SaveAs(outputGraphsDir+"2d_short_ratio.pdf")

    # h_ratio_lam_vz_z = h_reco_long_lam_vs_z.Clone()
    # h_ratio_lam_vz_z.Divide(h_mc_reco_long_lam_vs_z)
    # h_ratio_lam_vz_z.Draw("colz")
    # h_ratio_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    # c.SaveAs(outputGraphsDir+"2d_long_ratio.pdf")

    # h_ratio_lam_vz_z = h_reco_long_lam_vs_z.Clone()
    # h_ratio_lam_vz_z.Divide(h_mc_reco_S4_lam_vs_z)
    # h_ratio_lam_vz_z.Draw("colz")
    # h_ratio_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    # c.SaveAs(outputGraphsDir+"2d_long_to_short_ratio.pdf")

    # h_ratio_lam_vz_z = h_reco_long_lam_vs_z.Clone()
    # h_ratio_lam_vz_z.Divide(h_mc_reco_long_lam_vs_z)
    # h_ratio_lam_vz_z.Draw("colz")
    # h_ratio_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    # c.SaveAs(outputGraphsDir+"2d_long_ratio.pdf")

    h_acc_lam_vz_z = h_frames_short_lam_vs_z.Clone()
    h_acc_lam_vz_z.Divide(h_mc_tracks_lam_vs_z)
    h_acc_lam_vz_z.Draw("colz")
    h_acc_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    c.SaveAs(outputGraphsDir+"short_acc_lam_vs_vz.pdf")

    h_acc_lam_vz_z = h_frames_long_lam_vs_z.Clone()
    h_acc_lam_vz_z.Divide(h_mc_tracks_lam_vs_z)
    h_acc_lam_vz_z.Draw("colz")
    h_acc_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    c.SaveAs(outputGraphsDir+"long_acc_lam_vs_vz.pdf")

    h_acc_lam_vz_z = h_frames_short_not_long_lam_vs_z.Clone()
    h_acc_lam_vz_z.Divide(h_mc_tracks_lam_vs_z)
    h_acc_lam_vz_z.Draw("colz")
    h_acc_lam_vz_z.GetZaxis().SetRangeUser(0., 1.)
    c.SaveAs(outputGraphsDir+"short_but_not_long_acc_lam_vs_vz.pdf")

    print(f"Script took {time.time() - startTime:.2f} s")
