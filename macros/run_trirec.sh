#!/bin/bash

source /unix/muons/users/bgayther/testing_code/master/install/activate.sh


mu3eTrirec \
    --input /unix/muons/users/bgayther/testing_code/master/mu3e/tests/large_bkg_data/run000043-sort.root \
    --output /unix/muons/users/bgayther/testing_code/offset_reco_bkg_data/run000043-reco.root
