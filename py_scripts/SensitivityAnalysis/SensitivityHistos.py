import argparse
import json
import os
import XRootD.client as xrd
import ROOT

ROOT.gInterpreter.Declare("""

    #include "ROOT/RVec.hxx"
    ROOT::VecOps::RVec<double> weightVertices(const ROOT::VecOps::RVec<int>& mc_type1, const ROOT::VecOps::RVec<int>& mc_type2, const ROOT::VecOps::RVec<int>& mc_type3, const double& event_weight, const double& type_weight) {
        auto vertex_weights = ROOT::VecOps::RVec<double>(mc_type1.size());

        // loop over mc_type1, mc_type2, mc_type3
        for (int i = 0; i < mc_type1.size(); i++) {

            if (mc_type1[i] == 91 && mc_type2[i] == 91 && mc_type3[i] == 92) {
                vertex_weights[i] = type_weight;
            } else if (type_weight == 0.0) {
                vertex_weights[i] = event_weight;
            } else {
                vertex_weights[i] = event_weight * type_weight;
            }
        }

        return vertex_weights;
    };

    """
)

class Hist1d:
    def __init__(self, name, title, nbin, xmin, xmax):
        self.name = name
        self.title = title
        self.nbin = nbin
        self.xmin = xmin
        self.xmax = xmax

    def create(self, df, var, weight):
        return df.Histo1D((self.name, self.title, self.nbin, self.xmin, self.xmax), var, weight)

class Hist2d:
    def __init__(self, name, title, nbinx, xmin, xmax, nbiny, ymin, ymax):
        self.name = name
        self.title = title
        self.nbinx = nbinx
        self.xmin = xmin
        self.xmax = xmax
        self.nbiny = nbiny
        self.ymin = ymin
        self.ymax = ymax

    def create(self, df, varx, vary, weight):
        return df.Histo2D((self.name, self.title, self.nbinx, self.xmin, self.xmax, self.nbiny, self.ymin, self.ymax), varx, vary, weight)

class HistStore:
    def __init__(self, df):
        self.df = df
        self.histos = []

    def add(self, hist, *args):
        self.histos.append(hist.create(self.df, *args))

    def index(self, name):
        for i, h in enumerate(self.histos):
            if h.GetName() == name:
                return i
        return -1

    def get(self, name):
        return self.histos[self.index(name)]

    def get_all(self):
        return self.histos

def getConfig(input):
    return json.loads(str(ROOT.TFile.Open(input).Get("config").GetString()))

def GetNGenerated(file):
    # if file path starts with root:// use XRootD to open file
    # print("Opening file: {}".format(file))
    counts = 0
    if file.startswith("root://"):
        f = xrd.File()
        status, _ = f.open(file)
        if (not status.ok):
            print("Error opening file: {}".format(file))
            return -1
        _, counts = f.read(0, 100)
        # print(status)
        f.close()
    else:
        # check if file exists
        if not os.path.isfile(file):
            print("Error opening file: {}".format(file))
            return -1
        f = open(file, 'r')
        counts = f.readline()
        f.close()
    return int(counts)

if __name__ == "__main__":
    import time
    start_time = time.time()

    parser = argparse.ArgumentParser(description='Signal analysis',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-vertexFilesIC', type=str, nargs='+', required=False, help='Input vertex file names')
    parser.add_argument('-vertexFilesSig', type=str, nargs='+', required=False, help='Input vertex file names')
    parser.add_argument('-vertexFilesBhabha', type=str, nargs='+', required=False, help='Input vertex file names')
    parser.add_argument('-genFilesSig', type=str, nargs='+', required=False, help='Input signalnorm file names')
    parser.add_argument('-output', type=str, required=True, help='Output file name')
    parser.add_argument('-ncpu', type=int, default=8, required=False, help='Number of cpu')
    args = parser.parse_args()

    print("Starting script")

    ROOT.EnableImplicitMT(args.ncpu)
    if args.ncpu == 1:
        ROOT.EnableImplicitMT(0)
    ROOT.gROOT.SetBatch(True)
    ROOT.gErrorIgnoreLevel = ROOT.kWarning

    # TODO: prepare mixed mode?
    mode = "IC" if args.vertexFilesIC else "Sig" if args.vertexFilesSig else "BhabhaMichel" if args.vertexFilesBhabha else "None"
    print("Running in mode: {}".format(mode))

    if mode == "None":
        print("No input files given. Exiting.")
        exit(0)

    NSig = 0
    if mode == "Sig":
        assert(len(args.vertexFilesSig) == len(args.genFilesSig))

        # Get number of generated events
        Sig_files_to_remove = []
        for i in range(len(args.genFilesSig)):
            NSig_curr = GetNGenerated(args.genFilesSig[i])

            if NSig_curr == -1:
                Sig_files_to_remove.append(i)
            else:
                NSig += NSig_curr

        for i in reversed(Sig_files_to_remove):
            del args.genFilesSig[i]
            del args.vertexFilesSig[i]

        print(f"Signal sample: {NSig} generated")

    desired_vars = ["pm", "chi2", "mmass", "targetdist"]

    reco_cut = "pm<=4.0 && nhit1 >= 6 && nhit2 >= 6 && nhit3 >= 6 && chi2 < 15 && (mep1 > 10 || mep1 < 5)"

    mc_tr_sel = "id1 != id2 && id1 != id3 && id2 != id3 && mid1 == mid2 && mid2 == mid3 && prime1 == 1 && prime2 == 1 && prime3 == 1"
    true_vertex_cut = "hypot(true_vx, true_vy) <= 19 && abs(true_vz) <= 50"

    IC_sel = f"{mc_tr_sel} && {true_vertex_cut} && type1==31 && type2==31 && type3==32"
    bhabha_sel = f"chi2 < 30 && nhit1 > 4 && nhit2 > 4 && nhit3 > 4 && mmass > 80 && mmass < 115 && type3 == 252 && prime1 == 1 && prime2 == 1 && prime3 == 1 && pid1 == -11 && pid2 == -11 && pid3 == 11 && id1 != id2 && id1 != id3 && id2 != id3 && (mid1 == mid3 || mid2 == mid3)"
    sig_sel = f"mc_signal==1 && {true_vertex_cut}"

    print("Loading vertex files")

    df = None
    weight_var = 0
    max_entry = 0

    # TODO: add vars before and after cuts

    if mode == "IC":
        # TODO: check if MC track id seen before
        IC_df = ROOT.RDataFrame("vertex", args.vertexFilesIC).Filter("weight!=1") \
                                                             .Define("IC_sel", f"{IC_sel} && {reco_cut}") \
                                                             .Define("IC_sel_no_cut", f"{IC_sel}") \
                                                             .Define("vertexWeight", f"weightVertices(type1, type2, type3, weight, 0.0)") \
                                                             .Define(f"{mode}_vertexWeight", "vertexWeight[IC_sel]") \
                                                             .Define(f"{mode}_vertexWeight_no_cut", "vertexWeight[IC_sel_no_cut]")

        for var in desired_vars:
            IC_df = IC_df.Define(f"{mode}_{var}", f"{var}[IC_sel]").Define(f"{mode}_{var}_no_cut", f"{var}[IC_sel_no_cut]")

        weight_var = IC_df.Sum("weight").GetValue()
        df = IC_df

    if mode == "Sig":
        sig_df = ROOT.RDataFrame("vertex", args.vertexFilesSig).Define("vertexWeight", f"weightVertices(type1, type2, type3, weight, 1.0)") \
                                                               .Define("sig_sel", f"{sig_sel} && {reco_cut}") \
                                                               .Define("sig_sel_no_cut", f"{sig_sel}") \
                                                               .Define(f"{mode}_vertexWeight", f"vertexWeight[sig_sel]") \
                                                               .Define(f"{mode}_vertexWeight_no_cut", f"vertexWeight[sig_sel_no_cut]")

        for var in desired_vars:
            sig_df = sig_df.Define(f"{mode}_{var}", f"{var}[sig_sel]").Define(f"{mode}_{var}_no_cut", f"{var}[sig_sel_no_cut]")

        weight_var = NSig
        df = sig_df

    if mode == "BhabhaMichel":
        # BhabhaMichel sample
        BhabhaMichel_df = ROOT.RDataFrame("vertex", args.vertexFilesBhabha).Filter("weight!=1") \
                                                                           .Define("bhabha_sel", f"{bhabha_sel} && {reco_cut}") \
                                                                           .Define("bhabha_sel_no_cut", f"{bhabha_sel}") \
                                                                           .Define("vertexWeight", f"weightVertices(type1, type2, type3, weight, 0.0)") \
                                                                           .Define(f"{mode}_vertexWeight", "vertexWeight[bhabha_sel]") \
                                                                           .Define(f"{mode}_vertexWeight_no_cut", "vertexWeight[bhabha_sel_no_cut]")

        for var in desired_vars:
            BhabhaMichel_df = BhabhaMichel_df.Define(f"{mode}_{var}", f"{var}[bhabha_sel]").Define(f"{mode}_{var}_no_cut", f"{var}[bhabha_sel_no_cut]")

        # max_entry = BhabhaMichel_df.Max("event")
        weight_var = BhabhaMichel_df.Count()
        
        # max_entry = max_entry.GetValue()
        # print(f"Max entry: {max_entry}")
        weight_var = weight_var.GetValue()
        df = BhabhaMichel_df

    if df is None:
        raise ValueError("No dataframe created")

    # define histograms
    hists = HistStore(df)

    # plots
    pm_binning = (75, 0, 15)
    chi2_binning = (500, 0, 100)
    mmass_binning = (75, 95, 110)
    targetdist_binning = (125, 0, 25)
    pm_mmass_binning = (60, 95, 110, 75, 0, 15)

    weight_name = f"{mode}_vertexWeight"
    no_cut_weight_name = f"{mode}_vertexWeight_no_cut"
        
    hists.add(Hist1d(f"h_pm_{mode}", ";p_{eee} [MeV/#it{c}];Counts", *pm_binning), f"{mode}_pm", weight_name)
    hists.add(Hist1d(f"h_chi2_{mode}", ";#chi^{2};Counts", *chi2_binning), f"{mode}_chi2", weight_name)
    hists.add(Hist1d(f"h_mmass_{mode}", ";m_{eee} [MeV/#it{c}^{2}];Counts", *mmass_binning), f"{mode}_mmass", weight_name)
    hists.add(Hist1d(f"h_targetdist_{mode}", ";targetdist [mm];Counts", *targetdist_binning), f"{mode}_targetdist", weight_name)
    hists.add(Hist2d(f"h_pm_mmass_{mode}", ";m_{eee} [MeV/#it{c}^{2}];p_{eee} [MeV/#it{c}]", *pm_mmass_binning), f"{mode}_mmass", f"{mode}_pm", weight_name)

    # Plots for no cut weights
    hists.add(Hist1d(f"h_pm_{mode}_no_cut", ";p_{eee} [MeV/#it{c}];Counts", *pm_binning), f"{mode}_pm_no_cut", no_cut_weight_name)
    hists.add(Hist1d(f"h_chi2_{mode}_no_cut", ";#chi^{2};Counts", *chi2_binning), f"{mode}_chi2_no_cut", no_cut_weight_name)
    hists.add(Hist1d(f"h_mmass_{mode}_no_cut", ";m_{eee} [MeV/#it{c}^{2}];Counts", *mmass_binning), f"{mode}_mmass_no_cut", no_cut_weight_name)
    hists.add(Hist1d(f"h_targetdist_{mode}_no_cut", ";targetdist [mm];Counts", *targetdist_binning), f"{mode}_targetdist_no_cut", no_cut_weight_name)
    hists.add(Hist2d(f"h_pm_mmass_{mode}_no_cut", ";m_{eee} [MeV/#it{c}^{2}];p_{eee} [MeV/#it{c}]", *pm_mmass_binning), f"{mode}_mmass_no_cut", f"{mode}_pm_no_cut", no_cut_weight_name)

    print("Registered histograms")

    # Save all plots to root file
    f = ROOT.TFile(args.output + ".root", "RECREATE")

    # Save weight_var as TParameter to root file (different name depending on mode)
    print("Saving weight_var", weight_var)
    if mode == "BhabhaMichel":
        weight_var_tp = ROOT.TParameter("int")("n_bhabha_frames_skim", weight_var)
        weight_var_tp.Write()
    elif mode == "IC":
        weight_var_tp = ROOT.TParameter("double")("IC_weight_sum", weight_var)
        weight_var_tp.Write()
    elif mode == "Sig":
        weight_var_tp = ROOT.TParameter("int")("n_signal_events", weight_var)
        weight_var_tp.Write()

    # Write out the number of files used
    files = []
    if mode == "BhabhaMichel":
        n_files_tp = ROOT.TParameter("int")("n_bhabha_files", len(args.vertexFilesBhabha))
        n_files_tp.Write()
    elif mode == "IC":
        n_files_tp = ROOT.TParameter("int")("n_IC_files", len(args.vertexFilesIC))
        n_files_tp.Write()
    elif mode == "Sig":
        n_files_tp = ROOT.TParameter("int")("n_sig_files", len(args.vertexFilesSig))
        n_files_tp.Write()

    for hist in hists.get_all():
        hist.Write()
    f.Close()

    print("Saved plots")
    print(f"Script took {time.time() - start_time:.2f} seconds to run")