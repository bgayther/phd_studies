# setup DIRAC
from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)
from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job
import sys

if len(sys.argv) == 4 or len(sys.argv) == 2:
    print ("")
else:
    print ('All information is not provided, usage:\n       python Status-All.py [jobID file] {starting job seed} {print detial (0 or 1)}')
    sys.exit(1) 

dirac = Dirac()
File = sys.argv[1]
jobIDFile = open(File, "r")
all_jobids = [jobid.strip() for jobid in jobIDFile.readlines()]
Done="Done"
Running="Running"
Failed= "Failed"
AllRunning=True
AllDone=True
Failure=False
printDetial=False
findList=False
if len(sys.argv) == 4:
    findList=True
    startSeed=sys.argv[2]
    if int(sys.argv[3]) == 1:
        print ("Printing Detial")
        printDetial=True
startID=all_jobids[0]
list1=[]
numF=0

for i in range(len(all_jobids)):
    if(printDetial):
        print (dirac.getJobStatus(all_jobids[i]))
    else:
        if(i%1000 == 0):
            print ("At Job number ", i)
    x=str(dirac.getJobStatus(all_jobids[i]))
    runID=x[23:31]
    running=x[45:52]
    failed=x[45:51]
    done=x[45:49]

    if(failed==Failed):
        numF += 1
        if(printDetial):
            print ("Failed job")
            print (dirac.getJobStatus(all_jobids[i]))
        if( runID==startID):
            r=1
        else:
            if(findList):
                k=int(runID)-int(startID)
                u=int(startSeed)+k
                list1.append(u)
        #print runID
        Failure=True
    if(Done!=done):
        AllDone=False
    if(Running!=running):
        AllRunning=False


if(Failure):
    if(findList):
        print ("List of failed JobIDs \n")
        print (list1)
    print ("Number of failed jobs,", numF)
if(AllDone):
    print ("All jobs completed")
if(AllRunning):
    print ("All jobs running")
if(not(AllRunning) and not(AllDone) and not(Failure)):
    print ("Some jobs waiting")