import numpy as np
import matplotlib.pyplot as plt

ALPHA = 7.2973525693E-3  # fine-structure constant α
HBARC = 197.3269804  # ħc [Mev fm]
E2 = HBARC * ALPHA  # e^2 [Mev fm]
AMU = 931.49410242  # atomic mass unit [MeV/c^2]
NA = 6.02214076E+23  # Avogadro constant [mol^(-1)]
MEC2 = 0.510999  # electron mass energy [MeV]
RU = 3.12 # fm FOR CARBON
CARBON = (6, 12.010736)
HYDROGEN = (1, 1.00784)
OXYGEN = (8, 15.999)

def mott_lab(theta, T, Z, A):
    """
    Mott Scattering in the lab system

    Arguments
    ---------
    theta : scattering angle in the lab system
    T     : kinetic energy of incident particle in the lab system
    Z     : atomic number; charge in units of e
    A     : mass number

    Return
    ------
    dσ/dΩ(θ) in the lab system [mb/str]
    """ 

    mc2 = A * AMU
    theta_half = theta/2.0

    mott_point_like = ( (Z * E2) / 2*T )**2.0 * (np.cos(theta_half)**2.0 / np.sin(theta_half)**4.0) * (1 / (1 + (2*T / mc2)*np.sin(theta_half)**2.0))

    T_scattered = T / (1 + (T/mc2)*(1-np.cos(theta)) )

    q = np.sqrt(2 * T_scattered * T * (1 - np.cos(theta)))
    x = (q * RU) / (HBARC * 2 * np.pi)
    nuclear_form_factor = 3 * ( (np.sin(x) - x*np.cos(x))/x**3.0 ) # FOR CARBON

    diff_cross_sec = mott_point_like * nuclear_form_factor**2.0
    return diff_cross_sec, T_scattered

if __name__ == "__main__":
    import ROOT, os, sys
    sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
    import StyleHelpers
    from combineTgraphs import overlayMultipleTGraphs

    ROOT.gROOT.SetBatch(True)

    theta_range = np.linspace(10, 170.0, 100)
    rad_theta_range = np.radians(theta_range)
    lam_range = np.pi/2. - rad_theta_range

    energies = [10., 20., 30., 40., 53.]

    graphs = {}
    offsetGraphs = {}

    def setupGraph(gr, ytitle, xtitle="Scattering Angle [deg]"):
        gr.SetTitle("")
        gr.GetXaxis().SetTitle(xtitle)
        gr.GetYaxis().SetTitle(ytitle)
        gr.SetLineWidth(2)
        gr.SetLineColor(ROOT.kC0)

    grO_scatt = None
    grH_scatt = None
    gr_xsec = None

    for energy in energies:
        diff_cross_sec_C, scattered_mom_C = mott_lab(rad_theta_range, energy, *CARBON)

        if (energy == max(energies)):
            diff_cross_sec_O, scattered_mom_O = mott_lab(rad_theta_range, energy, *OXYGEN)
            diff_cross_sec_H, scattered_mom_H = mott_lab(rad_theta_range, energy, *HYDROGEN)

            grO_scatt = ROOT.TGraph(len(scattered_mom_O), rad_theta_range, scattered_mom_O)
            grH_scatt = ROOT.TGraph(len(scattered_mom_H), rad_theta_range, scattered_mom_H)
            gr_xsec = ROOT.TGraph(len(diff_cross_sec_C), rad_theta_range, diff_cross_sec_C/float(sum(diff_cross_sec_C)))

            setupGraph(grO_scatt, ytitle="Scattered momentum [MeV/#it{c}]")
            setupGraph(grH_scatt, ytitle="Scattered momentum [MeV/#it{c}]")
            setupGraph(gr_xsec, ytitle="d#sigma/d#Omega [A.U.]")

        grC_scatt = ROOT.TGraph(len(scattered_mom_C), rad_theta_range, scattered_mom_C)
        setupGraph(grC_scatt, ytitle="Scattered momentum [MeV/#it{c}]")

        grC_offset = ROOT.TGraph(len(scattered_mom_C), rad_theta_range, scattered_mom_C-energy)
        setupGraph(grC_offset, ytitle="Scattered - incident momentum [MeV/#it{c}]")

        graphs[energy] = grC_scatt
        offsetGraphs[energy] = grC_offset

    # Just for checking
    # for energy in energies:
    #     c = ROOT.TCanvas()
    #     graphs[energy].Clone().Draw("AL")
    #     c.SaveAs(f"scattMomE{energy:.1f}.pdf")

    #     offsetGraphs[energy].Clone().Draw("AL")
    #     c.SaveAs(f"scattMomDiffE{energy:.1f}.pdf")

    energies = energies[::-1]

    overlayMultipleTGraphs([grO_scatt, graphs[max(energies)], grH_scatt],
                           ["Oxygen", "Carbon", "Hydrogen"],
                           (0.20,0.22,0.52,0.42),
                           "Incident momentum of " + str(max(energies)) + " MeV; Scattering angle #theta_{s} [rad]; Scattered momentum [MeV/#it{c}]",
                           "ScatteredMomentumVsAngleForMultipleTargets",
                           ymin=max(energies)-5.5, ymax=max(energies)+0.5, xmin=min(rad_theta_range), xmax=max(rad_theta_range),
                           legStyle="L", drawStyle="AL")

    overlayMultipleTGraphs([offsetGraphs[energy] for energy in energies],
                           [f"{energy:.0f} MeV" for energy in energies],
                           (0.20,0.22,0.40,0.46),
                           "; Scattering angle #theta_{s} [rad]; Scattered - incident momentum [MeV/#it{c}]",
                           "ScatteredMomentumDifferenceVsThetaAngle",
                           ymin=-0.5, ymax=0.1, xmin=min(rad_theta_range), xmax=max(rad_theta_range),
                           legStyle="L", ncols=1, drawStyle="AL")

    overlayMultipleTGraphs([gr_xsec],
                           [f"{max(energies):.0f} MeV"],
                           (0.7,0.7,0.85,0.75),
                           "; Scattering angle #theta_{s} [rad]; d#sigma/d#Omega [A.U.]",
                           "DifferentialCrossSectionVsThetaAngle",
                           ymin=0.0, ymax=0.1, xmin=min(rad_theta_range), xmax=max(rad_theta_range),
                           legStyle="L", ncols=1, drawStyle="AL")

    def shiftGraph(gr, shift):
        grClone = gr.Clone()
        x = grClone.GetX()
        for i in range(grClone.GetN()):
            x[i] = shift - x[i]
        return grClone
        
    # vs lambda
    overlayMultipleTGraphs([shiftGraph(offsetGraphs[energy], np.pi/2.) for energy in energies],
                           [f"{energy:.0f} MeV" for energy in energies],
                           (0.7,0.25,0.85,0.45),
                           "; Scattering angle #lambda_{s} [rad]; Scattered - incident momentum [MeV/#it{c}]",
                           "ScatteredMomentumDifferenceVsLambdaAngle",
                           ymin=-0.5, ymax=0.1, xmin=min(lam_range), xmax=max(lam_range),
                           legStyle="L", ncols=1, drawStyle="AL")

    overlayMultipleTGraphs([shiftGraph(graphs[energy], np.pi/2.) for energy in energies],
                           [f"{energy:.0f} MeV" for energy in energies],
                           (0.20,0.22,0.32,0.42),
                           "; Scattering angle #lambda_{s} [rad]; Scattered momentum [MeV/#it{c}]",
                           "ScatteredMomentumVsLambdaAngleForMultipleEnergies",
                           ymin=5, ymax=55, xmin=min(lam_range), xmax=max(lam_range),
                           legStyle="L", drawStyle="AL")
