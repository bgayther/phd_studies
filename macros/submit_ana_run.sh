#!/bin/bash
#PBS -N run_analyzer
#PBS -o /unix/muons/users/bgayther/CosmicRun2022/data/analyzerJobOutput
#PBS -e /unix/muons/users/bgayther/CosmicRun2022/data/analyzerJobOutput
#PBS -l walltime=48:00:00
#PBS -l nodes=1 
#PBS -m bea
#PBS -M zcembga@ucl.ac.uk 
set -e

start=$(date +%s.%N)
echo "STARTING ANALYZER ON RUN DATA"

WORK_DIR=/unix/muons/users/bgayther/CosmicRun2022/data
cd $WORK_DIR
# if [ -z "$PBS_O_WORKDIR" ]
# then
#       echo "\$PBS_O_WORKDIR is NULL"
# else
#       echo "\$PBS_O_WORKDIR is NOT NULL"
#       cd $PBS_O_WORKDIR
# fi

source /unix/muons/users/bgayther/setup.sh
source /unix/muons/users/bgayther/CosmicRun2022/mu3e/install/activate.sh

echo $FILE # need to pass FILE like qsub -v FILE, so do export FILE=run353.mid for example
/unix/muons/users/bgayther/CosmicRun2022/analyzer/build/analyzer/analyzer_mu3e $FILE -e100000

RUN_NO=$(echo $FILE | tr -dc '0-9')
python /unix/muons/users/bgayther/phd_studies/scripts/getEDisplayString.py \
    -i ${WORK_DIR}/reco_output/mu3e_cosmic_run_reco_${RUN_NO}.root \
    -o ${WORK_DIR}/reco_output/mu3e_cosmic_run_reco_eDisplay_${RUN_NO}.json

end=$(date +%s.%N)
runtime=$(python -c "print(${end} - ${start})")

echo "Runtime was $runtime"
