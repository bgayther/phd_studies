import ROOT

ROOT.gROOT.SetBatch(True)
ROOT.ROOT.EnableImplicitMT(8)

files = [f"root://hepgrid11.ph.liv.ac.uk//dpm/ph.liv.ac.uk/home/mu3e.org/mu3e.org/prod/build4.4.3/Bhabha/run{SEED:06d}-ana_vertexfit.root" for SEED in range(50000, 50000+100)]
# print(files)

# loop over files and get number of entries
for file in files:
    f = ROOT.TFile.Open(file)
    t = f.Get("vertex")
    print(t.GetEntries())

df = ROOT.RDataFrame("vertex", files).Filter("weight!=1")

h_weight = df.Histo1D(("h_weight", ";Weight per frame;Counts", 100, 1e-6, 1e-4), "weight")

num_frames = df.Count()
sum_weights = df.Sum("weight")
max_weight = df.Max("weight")
min_weight = df.Min("weight")

sum_weights = sum_weights.GetValue()
num_frames = num_frames.GetValue()
avg_weight = sum_weights/num_frames
min_weight = min_weight.GetValue()
max_weight = max_weight.GetValue()

# Save h_weight
c = ROOT.TCanvas()
h_weight.Draw()
c.SetLogx()
c.SetLogy()
c.SaveAs("h_weight.pdf")

framelength_ratio = 50.0/64.0
rate_ratio = 2.466e8/1.9e8

print(f"num_frames = {num_frames}, sum_weights = {sum_weights}, avg_weight = {avg_weight}, min_weight = {min_weight}, max_weight = {max_weight}")
print(f"Adjusted avg_weight = {avg_weight*framelength_ratio*rate_ratio}")