from ROOT import TCanvas, THStack, TLegend, kRed, kBlue, gPad
import StyleHelpers

def overlayMultipleHistos(histList, legList, legPosition, canTitle, saveName, colourList=[], linewidth=None, logy=False, ymin=None, ymax=None, xmin=None, xmax=None, legStyle="L", ncols=None, drawStyle="NOSTACK", openFrame=False):
    can = TCanvas()
    stack = THStack("hs", str(canTitle))
    leg = TLegend(*legPosition)

    num = len(histList)
    if (num != len(legList)): 
        print("Num histos != legs")
        return

    if not(len(colourList)): colourList = StyleHelpers.ColourList
    if (num > len(colourList)):
        print("Too many hists and not enough colours...")
        return

    if (ncols is not None): leg.SetNColumns(ncols)
    
    for i in range(num):
        hist = histList[i].Clone()
        hist.SetLineColor(colourList[i])
        hist.SetMarkerColor(colourList[i])
        if (linewidth is not None): hist.SetLineWidth(linewidth)
        stack.Add(hist)
        leg.AddEntry(hist, legList[i], legStyle)

    stack.Draw(drawStyle)
    leg.Draw()

    if (ymin is not None): stack.SetMinimum(ymin)
    if (ymax is not None): stack.SetMaximum(ymax)
    if (xmin is not None and xmax is not None): stack.GetXaxis().SetLimits(xmin, xmax)
    if (openFrame): 
        can.SetFrameLineColor(0)
        can.SetTickx(0)
        can.SetTicky(0)

    # gPad.RedrawAxis()

    gPad.SetLogy(logy)
    can.SaveAs(saveName+".pdf")
    return

def overlay_2_histos(h1, h2, name, leg1="", leg2="", title=""):
    can = TCanvas()
    hs = THStack("hs", str(title))
    hs.Add(h1)
    hs.Add(h2)
    hs.Draw("NOSTACK")

    h1.SetLineColor(kRed)
    h1.SetMarkerColor(kRed)

    h2.SetLineColor(kBlue)
    h2.SetMarkerColor(kBlue)

    leg = TLegend(0.80,0.75,0.85,0.85)
    leg.AddEntry(h1,leg1,"l")
    leg.AddEntry(h2,leg2,"l")
    leg.Draw()

    can.SaveAs(name)
    return    

if __name__ == "__main__":
    import ROOT
    ROOT.gROOT.SetBatch(True)

    # rfile = ROOT.TFile.Open("/unix/muons/users/bgayther/MottScattering/anaOutput/mottAna_52MeV_mott_gen_Carbon_250keV_mom_spread_52_MeV.root")
    # outDir = "/unix/muons/users/bgayther/MottScattering/anaOutput/"
    # overlayMultipleHistos([rfile.Get("h_p_elec"), rfile.Get("h_p_unidentified_elec"), rfile.Get("h_p_fakes_elec"), rfile.Get("h_p_photon_conv_elec"), rfile.Get("h_p_compton_elec"), rfile.Get("h_p_bhabha_elec")], ["All", "BeamGun", "Fakes", "Photon Conversion", "Compton Scattering", "Bhabha"], colourList, ";p_{Reco} [MeV/#it{c}];Counts", outDir+"elecMomentumDistributionByType")
    # overlayMultipleHistos([rfile.Get("h_p_pos"), rfile.Get("h_p_unidentified_pos"), rfile.Get("h_p_fakes_pos"), rfile.Get("h_p_photon_conv_pos")], ["All", "BeamGun", "Fakes", "Photon Conversion"], colourList, ";p_{Reco} [MeV/#it{c}];Counts", outDir+"posMomentumDistributionByType")

    rfile = ROOT.TFile.Open("/unix/muons/users/bgayther/MottScattering/mu3e/tests/results/mu3e_sig_vertex.root")
    outDir = "./"
    overlayMultipleHistos([rfile.Get("hNominalMomentumResidual"), rfile.Get("hScaledMomentumResidual")], ["Nominal", "Scaled"], (0.20,0.70,0.35,0.85), ";p_{mc} - p_{rec} [MeV/#it{c}];Counts", outDir+"ScaledAndNominalMomentumResiduals")

    # cosmic stuff
    # file = "/ben_dev/mu3e_cosmicRun/mu3e/tests/data/cosmicRecoAna.root"
    # rfile = ROOT.TFile.Open(file)

    # overlay_2_histos(rfile.Get("h_S4_mc_phi"), rfile.Get("h_S3_mc_phi"), "cosmic_reco_segs_phi.pdf", leg1="S4", leg2="S3", title="Reco segs (matched to mc tracks);#phi [rad];")
    # overlay_2_histos(rfile.Get("h_S4_mc_theta"), rfile.Get("h_S3_mc_theta"), "cosmic_reco_segs_theta.pdf", leg1="S4", leg2="S3", title="Reco segs (matched to mc tracks);#theta [rad];")
    # overlay_2_histos(rfile.Get("h_S4_zpca_z"), rfile.Get("h_S3_zpca_z"), "cosmic_reco_segs_zpca_z.pdf", leg1="S4", leg2="S3", title="Reco segs (matched to mc tracks);z0 [mm];")
    # overlay_2_histos(rfile.Get("h_S4_zpca_r"), rfile.Get("h_S3_zpca_r"), "cosmic_reco_segs_zpca_r.pdf", leg1="S4", leg2="S3", title="Reco segs (matched to mc tracks);DCA [mm];")
    # overlay_2_histos(rfile.Get("h_S4_pt"), rfile.Get("h_S3_pt"), "cosmic_reco_segs_pt.pdf", leg1="S4", leg2="S3", title="Reco segs (matched to mc tracks);pt [MeV/#it{c}];")
