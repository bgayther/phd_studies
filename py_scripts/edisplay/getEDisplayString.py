import ROOT
import json
import argparse

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--input', type=str, required=True, help='Input reco file names')
parser.add_argument('-o','--output', type=str, required=True, help='output file name')
args = parser.parse_args()

inputFile = ROOT.TFile.Open(args.input)
keys = inputFile.GetListOfKeys()
hasString = keys.FindObject("eDisplayString")

if (hasString):
    displayStr = str(ROOT.TFile.Open(args.input).Get("eDisplayString").GetString())
    displayStr = "[" + displayStr + "]"
    displayStr = json.loads(displayStr)

    with open(args.output, "w") as file:
        json.dump(displayStr, file, indent=4)
        # file.write(displayStr)

    print("Written event display json to", args.output)
else:
    print("No event display string present (no segs reconstructed)...")