import pickle
import numpy as np
import matplotlib as mpl
import mplhep as hep
import matplotlib.pyplot as plt
import pandas as pd
import xgboost as xgb
from sklearn.metrics import precision_score, recall_score, accuracy_score, f1_score, roc_curve, auc
from sklearn.metrics import precision_recall_curve, average_precision_score, roc_auc_score, confusion_matrix
import math
import argparse
from sklearn.model_selection import train_test_split
import pathlib
import time
import shap

mpl.use('Agg')

def main(bdt_IC, bdt_bhabha, X_test, y_test, w_test, optimal_thr, resultsDir, mmass_range=(101, 107.5), pm_range=(0, 5)):
    mpl.rcParams.update({"axes.grid" : False})
    start_time = time.time()
    print("Testing BDT...")
    
    # Look at shapley values for both BDTs
    explainer_IC = shap.TreeExplainer(bdt_IC)
    shap_values_IC = explainer_IC.shap_values(X_test)

    explainer_bhabha = shap.TreeExplainer(bdt_bhabha)
    shap_values_bhabha = explainer_bhabha.shap_values(X_test)

    # Plot shapley values
    shap.summary_plot(shap_values_IC, X_test, plot_type="bar", show=False)
    plt.savefig(f"{resultsDir}/test/IC_shapley_values.pdf")

    shap.summary_plot(shap_values_bhabha, X_test, plot_type="bar", show=False)
    plt.savefig(f"{resultsDir}/test/bhabha_shapley_values.pdf")

    y_pred_proba = (bdt_IC.predict_proba(X_test)[:, 1] + bdt_bhabha.predict_proba(X_test)[:, 1]) / 2.0 # combined probabilities
    y_pred = np.where(y_pred_proba > optimal_thr, 1, 0) # Predicted class

    # Scale probabilities to [-1, 1]
    # y_pred_proba = 2 * y_pred_proba - 1

    # Calculate ROC curve
    fpr, tpr, _ = roc_curve(y_test, y_pred_proba, sample_weight=w_test)
    roc_auc = auc(fpr, tpr)

    # Calculate precision-recall curve
    precision, recall, _ = precision_recall_curve(y_test, y_pred_proba, sample_weight=w_test)
    average_precision = average_precision_score(y_test, y_pred_proba, sample_weight=w_test)

    hep.style.use("LHCb2")

    # Plot ROC curve
    plt.figure()
    plt.plot(fpr, tpr, label=f"ROC curve (area = {roc_auc:.4f})")
    plt.plot([0, 1], [0, 1], "k--")
    plt.xlabel("False positive rate")
    plt.ylabel("True positive rate")
    plt.title("ROC curve")
    plt.legend(loc="best")
    plt.savefig(f"{resultsDir}/test/ROC.pdf")
    plt.close()

    # Plot precision-recall curve
    plt.figure()
    plt.plot(recall, precision, label=f"Precision-recall curve (area = {average_precision:.4f})")
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.title("Precision-recall curve")
    plt.legend(loc="best")
    plt.savefig(f"{resultsDir}/test/PR.pdf")
    plt.close()

    # Plot confusion matrix
    # plt.figure()
    # conf_matrix = confusion_matrix(y_test, y_pred, sample_weight=w_test)
    # plt.imshow(conf_matrix, interpolation="nearest", cmap=plt.cm.Blues)
    # plt.title("Confusion matrix")
    # plt.colorbar()
    # tick_marks = np.arange(2)
    # plt.xticks(tick_marks, ["Background", "Signal"], rotation=45)
    # plt.yticks(tick_marks, ["Background", "Signal"])
    # plt.tight_layout()
    # plt.ylabel("True label")
    # plt.xlabel("Predicted label")
    # plt.savefig(f"{resultsDir}/Test_ConfusionMatrix.pdf")
    # plt.close()

    # Plot BDT output
    plt.figure()
    plt.hist(y_pred_proba[y_test == 0], bins=100, range=(0, 1), label="MC background", histtype="step")
    plt.hist(y_pred_proba[y_test == 1], bins=100, range=(0, 1), label="MC signal", histtype="step")
    plt.xlabel("BDT output")
    plt.ylabel("Absolute entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/unweighted_bdt_output.pdf")
    plt.close()

    # Plot BDT output with weights
    plt.figure()
    plt.hist(y_pred_proba[y_test == 0], bins=100, range=(0, 1), label="MC background", histtype="step", weights=w_test[y_test == 0])
    plt.hist(y_pred_proba[y_test == 1], bins=100, range=(0, 1), label="MC signal", histtype="step", weights=w_test[y_test == 1])
    plt.xlabel("BDT output")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/weighted_bdt_output.pdf")
    plt.close()

    # Make mmass plot for true signal and background
    plt.figure()
    plt.hist(X_test["mmass"][y_test == 0], bins=75, range=(95, 110), label="MC Background", histtype="step", weights=w_test[y_test == 0])
    plt.hist(X_test["mmass"][y_test == 1], bins=75, range=(95, 110), label="MC Signal", histtype="step", weights=w_test[y_test == 1])
    plt.xlabel("m$_{eee}$ [MeV]")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/true_mmass.pdf")

    # signal error bars (sqrt of sum of weights per bin)
    # background error bars (sqrt of sum of weights per bin)

    # Plot using mplhep
    # def plot_sig_and_bkg(var, X_test, y_values, w_test, resultsDir, xlabel="m$_{eee}$ [MeV]", SaveName="test", xbins=75, xrange=(95, 110))):
    #     hsig, bins = np.histogram(X_test[var][y_values == 1], bins=xbins, range=xrange)
    #     hbkg, bins = np.histogram(X_test[var][y_values == 0], bins=xbins, range=xrange)
        
    #     hep.histplot((hsig, bins), label='Signal')
    #     hep.histplot((hbkg, bins), label='Background')
    #     plt.xlabel(xlabel)
    #     plt.ylabel("Weighted entries")
    #     plt.legend(loc="best")
    #     plt.yscale("log")
    #     plt.savefig(f"{resultsDir}/{SaveName}.pdf")

    # Make mmass plot for predicted signal and background
    plt.figure()
    plt.hist(X_test["mmass"][y_pred == 0], bins=75, range=(95, 110), label="Background", histtype="step", weights=w_test[y_pred == 0])
    plt.hist(X_test["mmass"][y_pred == 1], bins=75, range=(95, 110), label="Signal", histtype="step", weights=w_test[y_pred == 1])
    plt.xlabel("m$_{eee}$ [MeV]")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/bdt_pred_mmass.pdf")
    plt.close()

    # Make same plot but labelling misclassified events
    plt.figure()
    plt.hist(X_test["mmass"][y_pred == 0], bins=75, range=(95, 110), label="Background", histtype="step", weights=w_test[y_pred == 0])
    plt.hist(X_test["mmass"][y_pred == 1], bins=75, range=(95, 110), label="Signal", histtype="step", weights=w_test[y_pred == 1])
    plt.hist(X_test["mmass"][(y_pred == 0) & (y_test == 1)], bins=75, range=(95, 110), label="Misclassified signal", histtype="step", weights=w_test[(y_pred == 0) & (y_test == 1)])
    plt.hist(X_test["mmass"][(y_pred == 1) & (y_test == 0)], bins=75, range=(95, 110), label="Misclassified background", histtype="step", weights=w_test[(y_pred == 1) & (y_test == 0)])
    plt.xlabel("m$_{eee}$ [MeV]")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/bdt_pred_mmass_misclassified.pdf")

    # Compare to cut of pm<=4.0 && nhit1 >= 6 && nhit2 >= 6 && nhit3 >= 6 && chi2 < 15 && (mep1 > 10 || mep1 < 5)
    y_cut_pred = (X_test["pm"] <= 4.0) & (X_test["chi2"] < 15) & ((X_test["mep1"] > 10) | (X_test["mep1"] < 5))
    y_cut_pred = y_cut_pred.astype(int)

    # Make mmass plot for predicted signal and background with cut
    plt.figure()
    plt.hist(X_test["mmass"][y_cut_pred == 0], bins=75, range=(95, 110), label="Background", histtype="step", weights=w_test[y_cut_pred == 0])
    plt.hist(X_test["mmass"][y_cut_pred == 1], bins=75, range=(95, 110), label="Signal", histtype="step", weights=w_test[y_cut_pred == 1])
    plt.xlabel("m$_{eee}$ [MeV]")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/cut_pred_mmass.pdf")
    plt.close()

    # Make same plot but labelling misclassified events
    plt.figure()
    plt.hist(X_test["mmass"][y_cut_pred == 0], bins=75, range=(95, 110), label="Background", histtype="step", weights=w_test[y_cut_pred == 0])
    plt.hist(X_test["mmass"][y_cut_pred == 1], bins=75, range=(95, 110), label="Signal", histtype="step", weights=w_test[y_cut_pred == 1])
    plt.hist(X_test["mmass"][(y_cut_pred == 0) & (y_test == 1)], bins=75, range=(95, 110), label="Misclassified signal", histtype="step", weights=w_test[(y_cut_pred == 0) & (y_test == 1)])
    plt.hist(X_test["mmass"][(y_cut_pred == 1) & (y_test == 0)], bins=75, range=(95, 110), label="Misclassified background", histtype="step", weights=w_test[(y_cut_pred == 1) & (y_test == 0)])
    plt.xlabel("m$_{eee}$ [MeV]")
    plt.ylabel("Weighted entries")
    plt.legend(loc="best")
    plt.yscale("log")
    plt.savefig(f"{resultsDir}/test/cut_pred_mmass_misclassified.pdf")

    # TODO: calculate cut efficiency/purity/rejection for signal/background using weights!

    results_df = pd.DataFrame({"accuracy": accuracy_score(y_test, y_pred, sample_weight=w_test), 
                               "precision": precision_score(y_test, y_pred, sample_weight=w_test), 
                               "recall": recall_score(y_test, y_pred, sample_weight=w_test), 
                               "f1": f1_score(y_test, y_pred, sample_weight=w_test), 
                               "roc_auc": roc_auc, 
                               "average_precision": average_precision,
                               "cut_accuracy": accuracy_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_precision": precision_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_recall": recall_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_f1": f1_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_roc_auc": roc_auc_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_average_precision": average_precision_score(y_test, y_cut_pred, sample_weight=w_test),
                               "cut_efficiency": np.sum(y_cut_pred[y_test == 1]) / np.sum(y_test == 1),
                               "cut_efficiency_error": np.sqrt(np.sum(y_cut_pred[y_test == 1])) / np.sum(y_test == 1),
                               "cut_purity": np.sum(y_cut_pred[y_test == 1]) / np.sum(y_cut_pred == 1),
                               "cut_purity_error": np.sqrt(np.sum(y_cut_pred[y_test == 1])) / np.sum(y_cut_pred == 1),
                               "optimal_thr_bdt_efficiency": np.sum(y_pred_proba[y_test == 1] > optimal_thr) / np.sum(y_test == 1),
                               "optimal_thr_bdt_efficiency_error": np.sqrt(np.sum(y_pred_proba[y_test == 1] > optimal_thr)) / np.sum(y_test == 1),
                               "optimal_thr_bdt_purity": np.sum(y_pred_proba[y_test == 1] > optimal_thr) / np.sum(y_pred_proba > optimal_thr),
                               "optimal_thr_bdt_purity_error": np.sqrt(np.sum(y_pred_proba[y_test == 1] > optimal_thr)) / np.sum(y_pred_proba > optimal_thr)
                               }, index=[0])

    with open(f"{resultsDir}/results.txt", "w") as f:
        for col in results_df.columns:
            print(f"{col}: {results_df[col][0]:.4f}")
            f.write(f"{col}: {results_df[col][0]:.4f}\n")

    results_df.to_csv(f"{resultsDir}/results.csv", index=False)

    # Now look at metrics in search region (within mmass_range and pm_range)
    search_mask = (X_test["mmass"] > mmass_range[0]) & (X_test["mmass"] < mmass_range[1]) & (X_test["pm"] > pm_range[0]) & (X_test["pm"] < pm_range[1])
    search_y_test = y_test[search_mask]
    search_y_pred = y_pred[search_mask]
    search_y_pred_proba = y_pred_proba[search_mask]
    search_w_test = w_test[search_mask]

    # Look at cut performance in search region
    search_y_cut_pred = y_cut_pred[search_mask]

    search_results_df = pd.DataFrame({"search_accuracy": accuracy_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_precision": precision_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_recall": recall_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_f1": f1_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_roc_auc": roc_auc_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_average_precision": average_precision_score(search_y_test, search_y_pred, sample_weight=search_w_test),
                                      "search_cut_accuracy": accuracy_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_precision": precision_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_recall": recall_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_f1": f1_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_roc_auc": roc_auc_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_average_precision": average_precision_score(search_y_test, search_y_cut_pred, sample_weight=search_w_test),
                                      "search_cut_efficiency": np.sum(search_y_cut_pred[search_y_test == 1]) / np.sum(search_y_test == 1),
                                      "search_cut_efficiency_error": np.sqrt(np.sum(search_y_cut_pred[search_y_test == 1])) / np.sum(search_y_test == 1),
                                      "search_cut_purity": np.sum(search_y_cut_pred[search_y_test == 1]) / np.sum(search_y_cut_pred == 1),
                                      "search_cut_purity_error": np.sqrt(np.sum(search_y_cut_pred[search_y_test == 1])) / np.sum(search_y_cut_pred == 1),
                                      "search_optimal_thr_bdt_efficiency": np.sum(search_y_pred_proba[search_y_test == 1] > optimal_thr) / np.sum(search_y_test == 1),
                                      "search_optimal_thr_bdt_efficiency_error": np.sqrt(np.sum(search_y_pred_proba[search_y_test == 1] > optimal_thr)) / np.sum(search_y_test == 1),
                                      "search_optimal_thr_bdt_purity": np.sum(search_y_pred_proba[search_y_test == 1] > optimal_thr) / np.sum(search_y_pred_proba > optimal_thr),
                                      "search_optimal_thr_bdt_purity_error": np.sqrt(np.sum(search_y_pred_proba[search_y_test == 1] > optimal_thr)) / np.sum(search_y_pred_proba > optimal_thr)
                                      }, index=[0])

    with open(f"{resultsDir}/search_results.txt", "w") as f:
        for col in search_results_df.columns:
            print(f"{col}: {search_results_df[col][0]:.4f}")
            f.write(f"{col}: {search_results_df[col][0]:.4f}\n")

    print(f"Time taken for testing: {time.time() - start_time:.2f} s")

if __name__ == "__main__":
    from trainBDT import prepare_data, norm_data, join_dataframes, selected_vars

    parser = argparse.ArgumentParser()
    parser.add_argument("--sig_files", type=str, nargs="+", help="Signal files")
    parser.add_argument("--bhabha_files", type=str, nargs="+", help="Bhabha files")
    parser.add_argument("--IC_files", type=str, nargs="+", help="IC files")
    parser.add_argument("--weightsFile", type=str, default="./weights.pickle", help="Weights file")
    parser.add_argument("--ncpu", type=int, default=8, help="Number of CPUs to use")
    parser.add_argument("--model", type=str, default="bdt.pickle", help="BDT model (.pickle)")
    parser.add_argument("--resultsDir", type=str, default="./results", help="Input/results directory")
    args = parser.parse_args()

    # TODO
    # resultsDir = args.resultsDir

    # # Remove trailing slash
    # if resultsDir[-1] == "/":
    #     resultsDir = resultsDir[:-1]
        
    # pathlib.Path(resultsDir).mkdir(parents=True, exist_ok=True)


    # main(bdt, X_test, y_test, w_test, optimal_thr, resultsDir)
