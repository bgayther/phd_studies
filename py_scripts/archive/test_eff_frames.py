from ROOT import *
import numpy as np
import itertools

# NB This isn't working at the moment, probably due to segments (tracks) being shared 
# between multiple frames, hence duplicated segments.

DataTree = TChain("frames")
DataTree.Add("run000042-reco.root")

DataTree_mc = TChain("frames_mc")
DataTree_mc.Add("run000042-reco.root")

# https://www.physi.uni-heidelberg.de/Forschung/he/mu3e/wiki/index.php/ROOT_File_Contents
# Each entry (row) contains vector of segments (tracks) with given eventId  
frame_n, n3, n4, n6, n8 = 0.0, 0.0, 0.0, 0.0, 0.0
frame_n_mc, n3_mc, n4_mc, n6_mc, n8_mc = 0.0, 0.0, 0.0, 0.0, 0.0

for entry in xrange(DataTree.GetEntries()):
    frame_n += 1
    DataTree.GetEntry(entry)  

    mc_primes = [i for i in DataTree.mc_prime]
    nhits = [i for i in DataTree.nhit]
    chi2s = [i for i in DataTree.chi2]

    joined_lists = itertools.product(mc_primes, nhits)
    # NOTE, following line will change joined_lists iterator entirely
    # print len(list(joined_lists))

    for i in joined_lists:
        if i[0]:
            if i[1] == 4:
                n4 += 1
            if i[1] == 6:
                n6 += 1
            if i[1] == 8:
                n8 += 1               
        # print "({}, {})".format(i[0], i[1])

for entry in xrange(DataTree_mc.GetEntries()):
    frame_n_mc += 1
    DataTree_mc.GetEntry(entry)   

    mc_primes = [i for i in DataTree_mc.mc_prime]
    nhits = [i for i in DataTree_mc.nhit]
    chi2s = [i for i in DataTree_mc.chi2]

    joined_lists = itertools.product(mc_primes, nhits, chi2s)
    for i in joined_lists:
        if i[0]:
            if i[1] == 4 and i[2] < 32:
                n4_mc += 1
            if i[1] == 6 and i[2] < 48:
                n6_mc += 1
            if i[1] == 8 and i[2] < 48:
                n8_mc += 1
        # print "({}, {}, {})".format(i[0], i[1], i[2])

print "n4 = {}, n6 = {}, n8 = {}".format(n4, n6, n8)
print "n4_mc = {}, n6_mc = {}, n8_mc = {}".format(n4_mc, n6_mc, n8_mc)

# Eff = N_reconstructed / N_monte_carlo
print "Short track efficiency: ", 100 * n4 / n4_mc, "%"
print "Long track efficiency: ", 100 * n8 / n8_mc, "%"

