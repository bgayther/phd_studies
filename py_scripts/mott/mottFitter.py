import argparse
import json
import logging
import os
import sys
from collections import defaultdict
import pathlib
import numpy as np
import ROOT
import time

sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers
from combineTgraphs import overlayMultipleTGraphs
from combineTH1 import overlayMultipleHistos
from quickMottSim import AMU

ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
ROOT.RooMsgService.instance().setSilentMode(True)

ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/cpp_scripts/include/LambdaHelpers.hpp")

ncpu = 8
RooFitBatchMode = False
outputGraphsDir = ""
min_entries = 100
SaveLogPlots = False

class doubleGaussAndLandauResult():
    def __init__(self, shared_mean=None, gauss_frac=None, gauss_sigma1=None, gauss_sigma2=None, landau_sigma=None, mpv=None, status=0):
        self.shared_mean = shared_mean
        self.gauss_frac = gauss_frac
        self.gauss_sigma1 = gauss_sigma1
        self.gauss_sigma2 = gauss_sigma2
        self.landau_sigma = landau_sigma
        self.status = status
        self.mpv = mpv
        self.valid = 0
        if (shared_mean is None): self.valid = -1

    def getMinSigma(self):
        if (not self.gauss_sigma1 or not self.gauss_sigma2): return
        if (self.gauss_frac.getValV() >= 0.5): return self.gauss_sigma1
        else: return self.gauss_sigma2

class doubleSidedCrystalBallResult():
    def __init__(self, mean=None, sigma=None, alphaL=None, nL=None, alphaR=None, nR=None, status=0):
        self.mean = mean
        self.sigma = sigma
        self.alphaL = alphaL
        self.nL = nL
        self.alphaR = alphaR
        self.nR = nR
        self.status = status
        self.valid = 0
        if (mean is None): self.valid = -1

def setPointAndErr(gr, x, xerr, val, dblTurn=False, offset=None): # maybe add option to not give x err
    # Access status/valid fit status here
    if (gr is not None and val is not None):
        y = val.getValV()
        # if (y == 0.0): return
        if (y == 0.0 and x == 0.0): return
        yerr = val.getError()
        if (dblTurn):
            y /= np.sqrt(2.0)
            yerr /= np.sqrt(2.0)
        if (offset is not None): y += offset
        N = gr.GetN()
        gr.SetPoint(N, x, y)
        gr.SetPointError(N, xerr, yerr)

# def correctForAngularDependence(mean_beam_momentum, momentum, theta):
#     mc2 = 12.010736 * AMU # For Carbon!
#     p_scattered = mean_beam_momentum / (1 + (mean_beam_momentum/mc2)*(1-np.cos(theta)) )
#     diff = p_scattered - momentum
#     return momentum - diff

# TODO: avoid duplicate code in these two fittting funcs
def fitDoubleGaussAndLandau(basis, dataset, energy, shared_mean_init, binWidth, name, particle, savePlot=True, range="resolutionRange", cut_var_name=None, cut_var_val=None, cut_binWidth=None, useBinned=False) -> doubleGaussAndLandauResult:
    startTime = time.time()
    logging.info(f"Fitting double gaussian and landau at E={energy}, range={binWidth}, particle {particle}")
    dataset = dataset.GetValue()
    dataset = dataset.reduce(ROOT.RooFit.SelectVars(ROOT.RooArgSet(basis)), ROOT.RooFit.CutRange(range))
    if useBinned: dataset = ROOT.RooDataHist("dh", "", ROOT.RooArgSet(basis), dataset)

    shared_mean = ROOT.RooRealVar("shared_mean", "shared_mean", *shared_mean_init)

    gauss_sigma1 = ROOT.RooRealVar("gauss_sigma1", "gauss_sigma1", 0.4, 0.05, 2.0)
    gauss1 = ROOT.RooGaussian("gauss1", "gauss model 1", basis, shared_mean, gauss_sigma1)

    gauss_sigma2 = ROOT.RooRealVar("gauss_sigma2", "gauss_sigma2", 1.0, 0.1, 3.5)
    gauss2 = ROOT.RooGaussian("gauss2", "gauss model 2", basis, shared_mean, gauss_sigma2)

    gauss_frac = ROOT.RooRealVar("gauss_frac", "frac of gauss1", 0.6, 0, 1)
    double_gauss = ROOT.RooAddPdf("double_gauss", "sum of gauss1 and gauss2", ROOT.RooArgList(gauss1, gauss2), gauss_frac)

    # landau_mean = ROOT.RooRealVar("landau_mean", "mean landau", 1, 0, 2)
    landau_sigma = ROOT.RooRealVar("landau_sigma", "sigma landau", 0.1, 0.0, 1)
    landau = ROOT.RooLandau("landau", "landau", basis, shared_mean, landau_sigma)

    model = ROOT.RooFFTConvPdf("model", "landau (X) dbl gauss", basis, landau, double_gauss)

    cut_var_test = cut_var_val is not None and cut_binWidth is not None and cut_var_name is not None
    if (cut_var_test): logging.info(f"With {cut_var_name} cut of {cut_var_val:.2f} +/- {cut_binWidth:.2f}")

    dataSize = dataset.sumEntries() if useBinned else dataset.numEntries()
    if (dataSize == 0 or dataSize < min_entries): return doubleGaussAndLandauResult()

    logging.info(f"Fitting nentries={dataSize}, data access took {time.time() - startTime:.2f} s")
    startTime = time.time()

    rf = model.fitTo(dataset, ROOT.RooFit.Range(range), ROOT.RooFit.PrintEvalErrors(-1), ROOT.RooFit.BatchMode(RooFitBatchMode), ROOT.RooFit.Save(True)) #ROOT.RooFit.NumCPU(ncpu)

    assert(rf)
    status = rf.status()
    if (status != 0):
        logging.info(f"Fit invalid status={status}")
        # return doubleGaussAndLandauResult()

    if (logging.INFO >= logging.root.level):
        shared_mean.Print()
        # landau_mean.Print()
        gauss_frac.Print()
        gauss_sigma1.Print()
        gauss_sigma2.Print()
        landau_sigma.Print()

    if (savePlot):
        c = ROOT.TCanvas()

        title = f"{energy:.2f} \pm {binWidth:.2f} MeV, {particle}"
        plotName = name + f"_p_{energy:.2f}_binW_{binWidth:.2f}"
        if (cut_var_test): 
            plotName += f"_{cut_var_name}_{cut_var_val:.2f}_binW_{cut_binWidth:.2f}"
            title += f" ({cut_var_name} = {cut_var_val:.2f} \pm {cut_binWidth:.2f})"
        xframe = basis.frame(ROOT.RooFit.Title(title), ROOT.RooFit.Range(range), ROOT.RooFit.Name(plotName)) #ROOT.RooFit.Bins(50)
        xframe.SetTitle(title)

        # minX, maxX = ROOT.RooHelpers.getRangeOrBinningInterval(basis, range)

        dataset.plotOn(xframe, ROOT.RooFit.Name("xdata"), ROOT.RooFit.XErrorSize(0)) #ROOT.RooFit.Binning(100, minX, maxX)

        model.plotOn(xframe, ROOT.RooFit.Name("fit"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.NormRange(range))
        # model.plotOn(xframe, ROOT.RooFit.Name("Landau"), ROOT.RooFit.LineStyle(ROOT.kDotted), ROOT.RooFit.NormRange(range), ROOT.RooFit.LineColor(ROOT.kC1), ROOT.RooFit.Components(landau))
        # model.plotOn(xframe, ROOT.RooFit.Name("DblGauss"), ROOT.RooFit.LineStyle(ROOT.kDotted), ROOT.RooFit.NormRange(range), ROOT.RooFit.LineColor(ROOT.kC2), ROOT.RooFit.Components(double_gauss))

        xframe.SetMinimum(1e-5)
        xframe.Draw()

        basis.setVal(shared_mean.getValV())
        meanLine = ROOT.TLine(shared_mean.getValV(), 0.0, shared_mean.getValV(), model.getVal(basis)*dataSize*0.05)
        meanLine.SetLineStyle(ROOT.kDashed)
        meanLine.SetLineColor(ROOT.kC2)
        meanLine.SetLineWidth(2)

        # basis.setVal(landau_mean.getValV())
        # landauMeanLine = ROOT.TLine(landau_mean.getValV(), 0.0, landau_mean.getValV(), model.getVal(basis)*dataSize*0.05)
        # landauMeanLine.SetLineStyle(ROOT.kDashed)
        # landauMeanLine.SetLineColor(ROOT.kC4)
        # landauMeanLine.SetLineWidth(2)

        f = model.asTF(ROOT.RooArgList(basis))
        mpv = f.GetMaximumX()
        logging.info(f"Most probable value (mode) = {mpv}")

        basis.setVal(mpv)
        mpvLine = ROOT.TLine(mpv, 0.0, mpv, model.getVal(basis)*dataSize*0.05)
        mpvLine.SetLineStyle(ROOT.kDashed)
        mpvLine.SetLineColor(ROOT.kC3)
        mpvLine.SetLineWidth(2)

        meanLine.Draw("SAME")
        # landauMeanLine.Draw("SAME")
        mpvLine.Draw("SAME")

        legend = ROOT.TLegend(0.68, 0.70, 0.83, 0.85)
        legend.AddEntry(xframe.findObject("fit"), "Fit", "L")
        legend.AddEntry(meanLine, "Mean", "L")
        # legend.AddEntry(landauMeanLine, "Landau Mean", "L")
        legend.AddEntry(mpvLine, "MPV", "L")
        # legend.AddEntry(xframe.findObject("Landau"),"Landau", "L")
        # legend.AddEntry(xframe.findObject("DblGauss"),"Gaussians", "L")
        legend.AddEntry(xframe.findObject("xdata"), "Data", "LEP")
        legend.Draw("SAME")

        c.SaveAs(outputGraphsDir + plotName + ".pdf")

        if (SaveLogPlots):
            xframe.SetMinimum(1)
            c.SetLogy()
            c.Modified()
            c.Update()
            c.SaveAs(outputGraphsDir + plotName + "_log.pdf")

    logging.info(f"Finished fitting, took {time.time() - startTime:.2f} s")

    mpvRV = ROOT.RooRealVar("mpv", "mpv", mpv)
    mpvRV.setError(0.0)
    return doubleGaussAndLandauResult(shared_mean, gauss_frac, gauss_sigma1, gauss_sigma2, landau_sigma, mpvRV, status)

def fitCrystalBall(basis, dataset, energy, shared_mean_init, binWidth, name, particle, cut_var_name=None, cut_var_val=None, cut_binWidth=None, savePlot=True, range="resolutionRange", useBinned=False) -> doubleSidedCrystalBallResult:
    startTime = time.time()
    logging.info(f"Fitting double sided crystal ball at E={energy}, range={binWidth}, particle {particle}")
    dataset = dataset.GetValue()
    dataset = dataset.reduce(ROOT.RooFit.SelectVars(ROOT.RooArgSet(basis)), ROOT.RooFit.CutRange(range))
    if useBinned: dataset = ROOT.RooDataHist("dh", "", ROOT.RooArgSet(basis), dataset)

    mean = ROOT.RooRealVar("mean", "mean", *shared_mean_init)

    sigmaLR = ROOT.RooRealVar("sigmaLR", "", 0.4, 0.01, 2.5)

    alphaL = ROOT.RooRealVar("alphaL", "", 1, 0.0, 10.0)
    nL = ROOT.RooRealVar("nL", "", 1, 0.0, 1000)

    alphaR = ROOT.RooRealVar("alphaR", "", 1, 0.0, 15.0)
    nR = ROOT.RooRealVar("nR", "", 1, 0.0, 1000)

    model = ROOT.RooCrystalBall("cb", "", basis, mean, sigmaLR, alphaL, nL, alphaR, nR)

    cut_var_test = cut_var_val is not None and cut_binWidth is not None and cut_var_name is not None
    if (cut_var_test): logging.info(f"With {cut_var_name} cut of {cut_var_val:.2f} +/- {cut_binWidth:.2f}")

    # printData(dataset)
    # cut_dataset = dataset.reduce(ROOT.RooArgSet(basis), cut)
    dataSize = dataset.sumEntries() if useBinned else dataset.numEntries()
    if (dataSize==0 or dataSize < min_entries): return doubleSidedCrystalBallResult()

    logging.info(f"Fitting nentries={dataSize}, data access took {time.time() - startTime:.2f} s")
    startTime = time.time()

    rf = model.fitTo(dataset, ROOT.RooFit.Range(range), ROOT.RooFit.PrintEvalErrors(-1), ROOT.RooFit.BatchMode(RooFitBatchMode), ROOT.RooFit.Save(True)) #ROOT.RooFit.NumCPU(ncpu)

    assert(rf)
    status = rf.status()
    if (status != 0):
        logging.info(f"Fit invalid status={status}")
    #     return doubleSidedCrystalBallResult()

    if (logging.INFO >= logging.root.level):
        mean.Print()
        sigmaLR.Print()
        alphaL.Print()
        nL.Print()
        alphaR.Print()
        nR.Print()

    if (savePlot):
        c = ROOT.TCanvas()

        title = f"{energy:.2f} \pm {binWidth:.2f} MeV, {particle}"
        plotName = name + f"_p_{energy:.2f}_binW_{binWidth:.2f}"
        if (cut_var_test): 
            plotName += f"_{cut_var_name}_{cut_var_val:.2f}_binW_{cut_binWidth:.2f}"
            title += f" ({cut_var_name} = {cut_var_val:.2f} \pm {cut_binWidth:.2f})"
        xframe = basis.frame(ROOT.RooFit.Title(title), ROOT.RooFit.Range(range), ROOT.RooFit.Name(plotName)) #ROOT.RooFit.Bins(100)
        xframe.SetTitle(title)

        # minX, maxX = ROOT.RooHelpers.getRangeOrBinningInterval(basis, range)

        dataset.plotOn(xframe, ROOT.RooFit.Name("xdata"), ROOT.RooFit.XErrorSize(0)) #ROOT.RooFit.Binning(100, minX, maxX)

        model.plotOn(xframe, ROOT.RooFit.Name("fit"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.Range(range))

        xframe.SetMinimum(1e-5)
        xframe.Draw()

        legend = ROOT.TLegend(0.68, 0.70, 0.83, 0.85)
        legend.AddEntry(xframe.findObject("fit"),"Fit", "L")
        legend.AddEntry(xframe.findObject("xdata"),"Data", "LEP")
        legend.Draw("SAME")

        # TODO: add models values?
        c.SaveAs(outputGraphsDir + plotName + ".pdf")

        if (SaveLogPlots):
            xframe.SetMinimum(1)
            c.SetLogy()
            c.Modified()
            c.Update()
            c.SaveAs(outputGraphsDir + plotName + "_log.pdf")

    logging.info(f"Finished fitting, took {time.time() - startTime:.2f} s")
    return doubleSidedCrystalBallResult(mean, sigmaLR, alphaL, nL, alphaR, nR, status)

def fitMomentumOffsetFromTGraph(gr, name, energies, title="", ymin=None, ymax=None):
    grClone = gr.Clone()

    grClone.SetTitle(title)
    grClone.GetXaxis().SetTitle("p_{Reco} [MeV/#it{c}]")
    grClone.GetYaxis().SetTitle("Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]")
    if (ymin is not None): grClone.SetMinimum(ymin)
    if (ymax is not None): grClone.SetMaximum(ymax)

    fitFunc = ROOT.TF1("fitFunc", "pol4")
    fitFunc.SetLineColor(ROOT.kC0)
    fitFunc.SetLineStyle(ROOT.kDashed)
    fitFunc.SetRange(min(energies), max(energies))
    grClone.Fit(fitFunc, "R F M EX0", "L")

    coefs = []
    errs = []
    for par in range(fitFunc.GetNpar()):
        coefs.append(fitFunc.GetParameter(par))
        errs.append(fitFunc.GetParError(par))
    #     print(f"{title} -> Parameter {par} has value {fitFunc.GetParameter(par)} and error {fitFunc.GetParError(par)}")

    print(f"Coefficients for {title}, {coefs} with errors {errs}")

    c = ROOT.TCanvas()
    grClone.Draw("AP")
    c.SaveAs(outputGraphsDir + name + ".pdf")
    return

def printData(data):
    print("")
    data.Print()
    for i in range(min(data.numEntries(), 20)):
        print(
            "("
            + ", ".join(["{0:8.3f}".format(var.getVal()) for var in data.get(i)])
            + ", )  weight={0:10.3f}".format(data.weight())
        )

# MOVE!
def getConfig(input):
    return json.loads(str(ROOT.TFile.Open(input).Get("config").GetString()))

def getEntries(input):
    rfile = ROOT.TFile.Open(input)
    segs = rfile.Get("segs")
    return segs.GetEntries()

def makeSegsDataFrame(inputs, type, nfiles=0, energy=None, selectDoubleCrossing=False, selectSingleCrossing=False):
    logging.info("Making dataframe")
    if (nfiles != 0): inputs = inputs[0:nfiles]
    df = ROOT.RDataFrame("segs", inputs, {"deltaL8_neg_dp", "pCorr"}) \
             .Define("lam010", "lam01[0]") \
             .Define("tan010", "tan01[0]") \
             .Define("deltaL8_neg_dp", "deltaL8_neg_dp(p_next_turn, p)") \
             .Define("deltaL8_tan_diff", "dphi(tan010, tan_next_turn)") \
             .Define("deltaL8_lam_diff", "dphi(lam010, lam_next_turn)") \
             .Define("pCorr", "corrSegsMomCharge(p, mc_pid)") \
             .Define("p_res", "diff(pCorr, mc_p)") \
             .Define("lam_res", "dphi(lam010, mc_lam)") \
             .Filter("long_track_cut(nhit)") \
             .Filter("qualityCut(zpca_z, lam010)") \
             .Filter("framesCut(nhit, s6n, s6n_0, s8n, s8n_0, seq)")

    # TODO: May have to redifine p_res with whichever correction is used
    # TODO: May have to redifine pScaled with whichever correction is used
    # Option to use mc_prime?
    # .Filter("mc_prime_cut(mc_prime)") \

    if (selectDoubleCrossing and selectSingleCrossing):
        logging.error("ERROR: Cannot select both single and double crossing events")
        return None
    if (selectDoubleCrossing):
        print("Selecting double crossing")
        df = df.Filter("doubleCrossing(mc_vx, mc_vy, mc_vz, mc_lam, mc_phi)")
    if (selectSingleCrossing):
        print("Selecting single crossing")
        df = df.Filter("singleCrossing(mc_vx, mc_vy, mc_vz, mc_lam, mc_phi)")

    if (type == "MICHEL"):
        return df.Filter("filterPosTracks(p)") \
                 .Filter("selectMichelTracks(mc_type)")
    elif (type == "POSITRON"):
        return df.Filter("filterPosTracks(p)") \
                 .Filter("selectMCPosTrack(mc_pid, mc_type)") \
                 .Define("pScaled", "scaleMomentum(nhit, mc_pid, pCorr)") \
                 .Define("pCorrWithAngCorr", f"applyAngularDependence({energy}, pCorr, M_PI_2 - lam010)")
    elif (type == "ELECTRON"):
        return df.Filter("filterElecTracks(p)") \
                 .Filter("selectMCElecTrack(mc_pid, mc_type)") \
                 .Define("pScaled", "scaleMomentum(nhit, mc_pid, pCorr)") \
                 .Define("pCorrWithAngCorr", f"applyAngularDependence({energy}, pCorr, M_PI_2 - lam010)")

def processMottFiles(inputs, type, nfiles=None, selectDoubleCrossing=False, selectSingleCrossing=False):
    fileMap = defaultdict(list)
    nEntriesPerEnergy = defaultdict(int)

    for file in inputs:
        cfg = getConfig(file)
        nEntriesPerEnergy[float(cfg["digi"]["beam"]["momentum"]["mean"])] += getEntries(file)
        fileMap[float(cfg["digi"]["beam"]["momentum"]["mean"])].append(file)
        # print("Beam mean momentum {:.2f} and spread {:.2f}".format(float(cfg["digi"]["beam"]["momentum"]["mean"]), float(cfg["digi"]["beam"]["momentum"]["spread"])))
        # if (cfg.get("trirec", None).get("use_ecorr", None)): print("Reco ecorr {}".format(cfg["trirec"]["use_ecorr"]))

    dataFrameObjs = defaultdict(ROOT.RDataFrame)
    for energy, files in fileMap.items():
        dataFrameObjs[energy] = makeSegsDataFrame(files, type, nfiles=nfiles, energy=energy, selectDoubleCrossing=selectDoubleCrossing, selectSingleCrossing=selectSingleCrossing)

    return dataFrameObjs, nEntriesPerEnergy

def createVariableForDict(dct, yvar, xvar, suffix=None):
    gr = ROOT.TGraphErrors()
    if (suffix is not None): gr.SetName(yvar + "_vs_" + xvar + "_" + suffix)
    # gr.Write()

    if dct[yvar]: dct[yvar].update({xvar : gr})
    else: dct[yvar] = {xvar : gr}
    # print(f"{dct}, {yvar}, {xvar}")
    return

def createGraphDict(yvars, xvars, suffix=None):
    graphs = dict.fromkeys(yvars)

    for yvar in yvars:
        for xvar in xvars:
            createVariableForDict(graphs, yvar, xvar, suffix=suffix)

    return graphs

# Not working...
def saveGraphs(graphs):
    for yvar in graphs:
        # print(yvar, graphs[yvar])
        for xvar in graphs[yvar]:
            graphs[yvar][xvar].Write()
            # print(xvar, graphs[yvar][xvar])

def makeLinspace(min, max, steps):
    points, binWidth = np.linspace(min, max, num=steps, retstep=True)
    binWidth /= 2.0
    return points, binWidth

if __name__ == "__main__":
    start = time.time()

    parser = argparse.ArgumentParser(description='Mott analysis',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-michelFiles', type=str, nargs='+', required=True, help='Input reco file names')
    parser.add_argument('-mottPosFiles', type=str, nargs='+', required=True, help='Input reco file names')
    parser.add_argument('-mottElecFiles', type=str, nargs='+', required=True, help='Input reco file names')
    parser.add_argument('-outputGraphsDir', type=str, default='./graphs', required=True, help='output path')
    parser.add_argument('-ncpu', type=int, default=8, required=False, help='Number of cpu')
    parser.add_argument('-nfiles', type=int, default=0, required=False, help='Number of files to process')
    parser.add_argument('-output', type=str, default='mottFitterResults', required=False, help='Output file name')
    parser.add_argument('-MomentumScaleCorrection', default=False, required=False, action='store_true', help='Scale momentum')
    parser.add_argument('-AngularCorrection', default=False, required=False, action='store_true', help='Correct for angular dependence (Mott)')
    parser.add_argument('-SelectSingleCrossing', default=False, required=False, action='store_true', help='Select single crossing tracks')
    parser.add_argument('-SelectDoubleCrossing', default=False, required=False, action='store_true', help='Select double crossing tracks')
    parser.add_argument('-DblTurnFits', default=False, required=False, action='store_true', help='Double turn fits')
    parser.add_argument('-DblTurnVsLambdaFits', default=False, required=False, action='store_true', help='Double turn fits for lambda')
    parser.add_argument('-DblTurnVsPhiFits', default=False, required=False, action='store_true', help='Double turn fits for ZPCA')
    parser.add_argument('-DblTurnVsZPCAFits', default=False, required=False, action='store_true', help='Double turn fits for phi')
    parser.add_argument('-MottPeakFits', default=False, required=False, action='store_true', help='Mott peak fits')
    parser.add_argument('-MottPeakMCFits', default=False, required=False, action='store_true', help='Mott peak MC fits')
    parser.add_argument('-MottPeakVsLambdaFits', default=False, required=False, action='store_true', help='Mott peak fits for lambda')
    parser.add_argument('-MottPeakVsPhiFits', default=False, required=False, action='store_true', help='Mott peak fits for phi')
    parser.add_argument('-MottPeakVsZPCAFits', default=False, required=False, action='store_true', help='Mott peak fits for zpca')
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const", dest="loglevel", const=logging.INFO)
    args = parser.parse_args()

    print("Starting script")
    logging.basicConfig(level=args.loglevel)

    ncpu = args.ncpu
    if (args.MomentumScaleCorrection): print("Momentum scale correction turned on!")

    ROOT.EnableImplicitMT(ncpu)
    ROOT.gROOT.SetBatch(True)
    outputGraphsDir = args.outputGraphsDir + "/" # check if / at end?
    ROOT.gErrorIgnoreLevel = ROOT.kWarning

    # verbosity = ROOT.Experimental.RLogScopedVerbosity(ROOT.Detail.RDF.RDFLogChannel(), ROOT.Experimental.ELogLevel.kInfo)

    pathlib.Path(outputGraphsDir).mkdir(parents=True, exist_ok=True)
    print("Creating dataframes")

    useBinnedData = True

    michelDF = makeSegsDataFrame(args.michelFiles, "MICHEL", nfiles=args.nfiles, selectSingleCrossing=args.SelectSingleCrossing, selectDoubleCrossing=args.SelectDoubleCrossing)
    mottPosDFs, mottPosSegsPerE = processMottFiles(args.mottPosFiles, "POSITRON", nfiles=args.nfiles, selectSingleCrossing=args.SelectSingleCrossing, selectDoubleCrossing=args.SelectDoubleCrossing)
    mottElecDFs, mottElecSegsPerE = processMottFiles(args.mottElecFiles, "ELECTRON", nfiles=args.nfiles, selectSingleCrossing=args.SelectSingleCrossing, selectDoubleCrossing=args.SelectDoubleCrossing)

    print("Finished creating dataframes")

    output_file = ROOT.TFile(outputGraphsDir + args.output + ".root", 'RECREATE')
    output_file.cd()

    energies = list(mottPosDFs.keys())
    energies.sort()

    for energy in energies:
        print(f"E={energy}, number of segs for positrons={mottPosSegsPerE[energy]}, electrons={mottElecSegsPerE[energy]}")

    energies_to_delete = [10.0, 11.0, 13.0]
    for energy_to_delete in energies_to_delete:
        if (energy_to_delete in energies):
            energies.remove(energy_to_delete)

    # TODO check how many segs reconstructed to use as condition

    print(f"Energies present {energies}")

    dblTurnMomentumRV = ROOT.RooRealVar("deltaP", "-#Deltap [MeV/#it{c}]", -3, 3)
    dblTurnMomentumRV.setBins(120)
    dblTurnMomentumRV.setBins(10000, "cache")
    dblTurnMomentumRV.setRange("resolutionRange", -1.0, 2.5)

    momentumRV = ROOT.RooRealVar("p", "p_{Reco} [MeV/#it{c}]", 5, 55)
    momentumRV.setBins(1000)
    momentumRV.setBins(10000, "cache")

    MCmomentumRV = ROOT.RooRealVar("pMC", "p_{MC} [MeV/#it{c}]", 5, 55)
    MCmomentumRV.setBins(1000)
    MCmomentumRV.setBins(10000, "cache")

    truthResRV = ROOT.RooRealVar("pRes", "p_{Reco} - p_{MC} [MeV/#it{c}]", -5, 5)
    truthResRV.setBins(200)
    truthResRV.setBins(10000, "cache")
    truthResMin = -2
    truthResMax = 2
    mc_p_err = (truthResMax-truthResMin)/2.0
    truthResRV.setRange("resolutionRange", truthResMin, truthResMax)

    DblTurnArg = ROOT.RooArgSet(dblTurnMomentumRV)
    DblTurnVar = ["deltaL8_neg_dp"]

    MomentumArg = ROOT.RooArgSet(momentumRV)
    MomentumVar = ["pCorr"]
    if args.MomentumScaleCorrection:
        MomentumVar = ["pScaled"]
    elif args.AngularCorrection:
        MomentumVar = ["pCorrWithAngCorr"]

    MCResArg = ROOT.RooArgSet(truthResRV)
    MCResVar = ["p_res"]

    MCMomArg = ROOT.RooArgSet(MCmomentumRV)
    MCMomVar = ["mc_p"]

    # TODO add lam_next_turn and phi_next_turn

    dblTurnBinWidth = 1.5
    mottPeakBinWidth = 1.5
    # mottPeakBinWidth = 3.5 # no ecorr sample

    # TODO reduce this by having another dict, i.e. dbl_turn_RDS["michel"]...

    # Double turn
    michel_dbl_turn_RDS = {}
    positron_dbl_turn_RDS = {}
    electron_dbl_turn_RDS = {}

    # Dbl turn cuts
    positron_dbl_turn_RDS_for_cuts = defaultdict(lambda : defaultdict(dict))
    electron_dbl_turn_RDS_for_cuts = defaultdict(lambda : defaultdict(dict))
    michel_dbl_turn_RDS_for_cuts = defaultdict(lambda : defaultdict(dict))

    # Mott Peaks
    positron_Mott_peak_RDS = defaultdict(dict)
    electron_Mott_peak_RDS = defaultdict(dict)

    # Mott peak cuts
    positron_Mott_peak_RDS_for_cuts = defaultdict(lambda : defaultdict(lambda : defaultdict(dict)))
    electron_Mott_peak_RDS_for_cuts = defaultdict(lambda : defaultdict(lambda : defaultdict(dict)))

    # lambda ranges
    L6_lam_points, L6_lam_binWidth = makeLinspace(-0.9, 0.9, 37)
    L8_lam_points, L8_lam_binWidth = makeLinspace(-0.5, 0.5, 21)

    # phi ranges ["phi"]
    phi_points, phi_binWidth = makeLinspace(-np.pi, np.pi, 63)

    # zpca ranges ["zpca"]
    zpca_points, zpca_binWidth = makeLinspace(-55, 55, 110)

    # michelDblTurnDF = michelDF.Filter("double_turn_cut(p_next_turn)")
    # TODO: maybe just make these all TH1 histograms?, then convert to RooDataHists??
    for energy in energies:
        if (args.DblTurnFits):
            cut_str = f"min_max_p_cut(p, {energy-dblTurnBinWidth}, {energy+dblTurnBinWidth})"

            tmpMichel = michelDF.Filter("double_turn_cut(p_next_turn)").Filter(cut_str)
            tmpMottPos = mottPosDFs[energy].Filter("double_turn_cut(p_next_turn)").Filter(cut_str)
            tmpMottElec = mottElecDFs[energy].Filter("double_turn_cut(p_next_turn)").Filter(cut_str)

            michel_dbl_turn_RDS[energy] = tmpMichel.Book(ROOT.std.move(ROOT.RooDataSetHelper("michelDS_Energy="+str(energy), "", DblTurnArg)), DblTurnVar)
            positron_dbl_turn_RDS[energy] = tmpMottPos.Book(ROOT.std.move(ROOT.RooDataSetHelper("posDS_Energy="+str(energy), "", DblTurnArg)), DblTurnVar)
            electron_dbl_turn_RDS[energy] = tmpMottElec.Book(ROOT.std.move(ROOT.RooDataSetHelper("elecDS_Energy="+str(energy), "", DblTurnArg)), DblTurnVar)
        
            if (args.DblTurnVsLambdaFits):
                for lam in L8_lam_points:
                    lam_cut = f"min_max_var_cut(lam010, {lam-L8_lam_binWidth}, {lam+L8_lam_binWidth})"
                    positron_dbl_turn_RDS_for_cuts[energy]["lam"][lam] = tmpMottPos.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_posDS_Energy="+str(energy)+lam_cut, "", DblTurnArg)), DblTurnVar)
                    electron_dbl_turn_RDS_for_cuts[energy]["lam"][lam] = tmpMottElec.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_elecDS_Energy="+str(energy)+lam_cut, "", DblTurnArg)), DblTurnVar)
                    michel_dbl_turn_RDS_for_cuts[energy]["lam"][lam] = tmpMichel.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_michelDS_Energy="+str(energy)+lam_cut, "", DblTurnArg)), DblTurnVar)

            if (args.DblTurnVsPhiFits):
                for phi in phi_points:
                    phi_cut = f"min_max_var_cut(tan010, {phi-phi_binWidth}, {phi+phi_binWidth})"
                    positron_dbl_turn_RDS_for_cuts[energy]["phi"][phi] = tmpMottPos.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_posDS_Energy="+str(energy)+phi_cut, "", DblTurnArg)), DblTurnVar)
                    electron_dbl_turn_RDS_for_cuts[energy]["phi"][phi] = tmpMottElec.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_elecDS_Energy="+str(energy)+phi_cut, "", DblTurnArg)), DblTurnVar)
                    michel_dbl_turn_RDS_for_cuts[energy]["phi"][phi] = tmpMichel.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_michelDS_Energy="+str(energy)+phi_cut, "", DblTurnArg)), DblTurnVar)

            if (args.DblTurnVsZPCAFits):
                for zpca in zpca_points:
                    zpca_cut = f"min_max_var_cut(zpca_z, {zpca-zpca_binWidth}, {zpca+zpca_binWidth})"
                    positron_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca] = tmpMottPos.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_posDS_Energy="+str(energy)+zpca_cut, "", DblTurnArg)), DblTurnVar)
                    electron_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca] = tmpMottElec.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_elecDS_Energy="+str(energy)+zpca_cut, "", DblTurnArg)), DblTurnVar)
                    michel_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca] = tmpMichel.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("dbl_turn_michelDS_Energy="+str(energy)+zpca_cut, "", DblTurnArg)), DblTurnVar)

        if (args.MottPeakFits):
            cut_str = f"min_max_p_cut(p, {energy-mottPeakBinWidth}, {energy+mottPeakBinWidth})"
            tmpMottPos = mottPosDFs[energy].Filter(cut_str)
            tmpMottElec = mottElecDFs[energy].Filter(cut_str)

            tmpMottPosL6 = tmpMottPos.Filter("L6_track_cut(nhit)")
            tmpMottPosL8 = tmpMottPos.Filter("L8_track_cut(nhit)")

            tmpMottElecL6 = tmpMottElec.Filter("L6_track_cut(nhit)")
            tmpMottElecL8 = tmpMottElec.Filter("L8_track_cut(nhit)")

            positron_Mott_peak_RDS[energy]["L6"] = tmpMottPosL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_posDS_Energy="+str(energy), "", MomentumArg)), MomentumVar)
            electron_Mott_peak_RDS[energy]["L6"] = tmpMottElecL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_elecDS_Energy="+str(energy), "", MomentumArg)), MomentumVar)

            positron_Mott_peak_RDS[energy]["L8"] = tmpMottPosL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_posDS_Energy="+str(energy), "", MomentumArg)), MomentumVar)
            electron_Mott_peak_RDS[energy]["L8"] = tmpMottElecL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_elecDS_Energy="+str(energy), "", MomentumArg)), MomentumVar)

            if (args.MottPeakMCFits):
                positron_Mott_peak_RDS[energy]["MC_Res_L6"] = tmpMottPosL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Res_L6_posDS_Energy="+str(energy), "", MCResArg)), MCResVar)
                electron_Mott_peak_RDS[energy]["MC_Res_L6"] = tmpMottElecL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Res_L6_elecDS_Energy="+str(energy), "", MCResArg)), MCResVar)

                positron_Mott_peak_RDS[energy]["MC_Res_L8"] = tmpMottPosL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Res_L8_posDS_Energy="+str(energy), "", MCResArg)), MCResVar)
                electron_Mott_peak_RDS[energy]["MC_Res_L8"] = tmpMottElecL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Res_L8_elecDS_Energy="+str(energy), "", MCResArg)), MCResVar)

                positron_Mott_peak_RDS[energy]["MC_Mom_L6"] = tmpMottPosL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Mom_L6_posDS_Energy="+str(energy), "", MCMomArg)), MCMomVar)
                electron_Mott_peak_RDS[energy]["MC_Mom_L6"] = tmpMottElecL6.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Mom_L6_elecDS_Energy="+str(energy), "", MCMomArg)), MCMomVar)

                positron_Mott_peak_RDS[energy]["MC_Mom_L8"] = tmpMottPosL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Mom_L8_posDS_Energy="+str(energy), "", MCMomArg)), MCMomVar)
                electron_Mott_peak_RDS[energy]["MC_Mom_L8"] = tmpMottElecL8.Book(ROOT.std.move(ROOT.RooDataSetHelper("MC_Mom_L8_elecDS_Energy="+str(energy), "", MCMomArg)), MCMomVar)

            if (args.MottPeakVsLambdaFits):
                # Adjust momentum bin width for lambda slices?
                for lam in L6_lam_points:
                    lam_cut = f"min_max_var_cut(lam010, {lam-L6_lam_binWidth}, {lam+L6_lam_binWidth})"
                    positron_Mott_peak_RDS_for_cuts[energy]["L6"]["lam"][lam] = tmpMottPosL6.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_posDS_Energy="+str(energy)+lam_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L6"]["lam"][lam] = tmpMottElecL6.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_elecDS_Energy="+str(energy)+lam_cut, "", MomentumArg)), MomentumVar)

                for lam in L8_lam_points:
                    lam_cut = f"min_max_var_cut(lam010, {lam-L8_lam_binWidth}, {lam+L8_lam_binWidth})"
                    positron_Mott_peak_RDS_for_cuts[energy]["L8"]["lam"][lam] = tmpMottPosL8.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_posDS_Energy="+str(energy)+lam_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L8"]["lam"][lam] = tmpMottElecL8.Filter(lam_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_elecDS_Energy="+str(energy)+lam_cut, "", MomentumArg)), MomentumVar)

            if (args.MottPeakVsPhiFits):
                for phi in phi_points:
                    phi_cut = f"min_max_var_cut(tan010, {phi-phi_binWidth}, {phi+phi_binWidth})"
                    positron_Mott_peak_RDS_for_cuts[energy]["L6"]["phi"][phi] = tmpMottPosL6.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_posDS_Energy="+str(energy)+phi_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L6"]["phi"][phi] = tmpMottElecL6.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_elecDS_Energy="+str(energy)+phi_cut, "", MomentumArg)), MomentumVar)
                    positron_Mott_peak_RDS_for_cuts[energy]["L8"]["phi"][phi] = tmpMottPosL8.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_posDS_Energy="+str(energy)+phi_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L8"]["phi"][phi] = tmpMottElecL8.Filter(phi_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_elecDS_Energy="+str(energy)+phi_cut, "", MomentumArg)), MomentumVar)

            if (args.MottPeakVsZPCAFits):
                for zpca in zpca_points:
                    zpca_cut = f"min_max_var_cut(zpca_z, {zpca-zpca_binWidth}, {zpca+zpca_binWidth})"
                    positron_Mott_peak_RDS_for_cuts[energy]["L6"]["zpca"][zpca] = tmpMottPosL6.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_posDS_Energy="+str(energy)+zpca_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L6"]["zpca"][zpca] = tmpMottElecL6.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L6_elecDS_Energy="+str(energy)+zpca_cut, "", MomentumArg)), MomentumVar)
                    positron_Mott_peak_RDS_for_cuts[energy]["L8"]["zpca"][zpca] = tmpMottPosL8.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_posDS_Energy="+str(energy)+zpca_cut, "", MomentumArg)), MomentumVar)
                    electron_Mott_peak_RDS_for_cuts[energy]["L8"]["zpca"][zpca] = tmpMottElecL8.Filter(zpca_cut).Book(ROOT.std.move(ROOT.RooDataSetHelper("L8_elecDS_Energy="+str(energy)+zpca_cut, "", MomentumArg)), MomentumVar)

    # Maybe run graphs on michels, or all??
    # def unpackDefaultDict(dct):
        # list_of_lists = [list(i.values()) for i in dct.values()]
        # flattened = [val for sublist in list_of_lists for val in sublist]
        # return flattened

    # print(list(michel_dbl_turn_RDS.values()) + list(positron_dbl_turn_RDS.values()) + list(electron_dbl_turn_RDS.values()) + unpackDefaultDict(positron_Mott_peak_RDS) + unpackDefaultDict(electron_Mott_peak_RDS))
    # ROOT.RDF.RunGraphs(list(michel_dbl_turn_RDS.values()) + list(positron_dbl_turn_RDS.values()) + list(electron_dbl_turn_RDS.values()) + unpackDefaultDict(positron_Mott_peak_RDS) + unpackDefaultDict(electron_Mott_peak_RDS))

    print("Booked RDS's")

    dblTurnVars = ["DeltaL8Res", "EnergyLoss", "LandauWidth"]
    MottPeakVars = ["MottPeakL6Res", "MottPeakL8Res", "MottPeakL6_Offset", "MottPeakL8_Offset", "MottPeakL6_Mean", "MottPeakL8_Mean"]
    MottPeakMCVars = ["MC_MottPeakL6Res", "MC_MottPeakL8Res", "MC_MottPeakL6_Offset", "MC_MottPeakL8_Offset",  "MC_MottPeakL6_Mean", "MC_MottPeakL8_Mean"]
    TruthResVars = ["TruthResL6", "TruthResL8", "TruthBiasL6", "TruthBiasL8"]

    yvars = dblTurnVars + MottPeakVars + MottPeakMCVars + TruthResVars
    michelGraphs = createGraphDict(yvars, ["p"], suffix="michel")
    positronGraphs = createGraphDict(yvars, ["p"], suffix="positron")
    electronGraphs = createGraphDict(yvars, ["p"], suffix="electron")

    michelGraphsPerEnergy = {k : createGraphDict(dblTurnVars, ["lam", "phi", "zpca"], suffix=f"michel_E{k:.2f}") for k in energies}
    positronGraphsPerEnergy = {k : createGraphDict(MottPeakVars+dblTurnVars, ["lam", "phi", "zpca"], suffix=f"positron_E{k:.2f}") for k in energies}
    electronGraphsPerEnergy = {k : createGraphDict(MottPeakVars+dblTurnVars, ["lam", "phi", "zpca"], suffix=f"electron_E{k:.2f}") for k in energies}

    scaledMomentumHistos = defaultdict(lambda : defaultdict(lambda : defaultdict(dict)))
    nomialMomentumHistos = defaultdict(lambda : defaultdict(lambda : defaultdict(dict)))

    for energy in energies:
        for track, cut in zip(["L6", "L8"], ["L6_track_cut(nhit)", "L8_track_cut(nhit)"]):
            for particle, df in zip(["positron", "electron"], [mottPosDFs[energy], mottElecDFs[energy]]):
                scaledMomentumHistos[particle][str(energy)][track] = df.Filter(cut).Histo1D((f"h_{particle}_{str(energy)}_{track}_scaled_p", ";#p_{scaled} [MeV/#it{c}];Counts", 1000, 5, 55), "pScaled")
                nomialMomentumHistos[particle][str(energy)][track] = df.Filter(cut).Histo1D((f"h_{particle}_{str(energy)}_{track}_nomial_p", ";#p_{scaled} [MeV/#it{c}];Counts", 1000, 5, 55), "pCorr")

    deltaL8_shared_mean_init = (0.4, -3, 3)
    mc_shared_mean_init = (-0.1, -1, +1)

    energies.reverse()

    # TODO use in main loop like this?
    # particles = ["michel", "positron", "electron"]
    # tracks = ["L6", "L8"]

    for energy_idx, energy in enumerate(energies):
        # Double turn fits
        if (args.DblTurnFits):
            michelDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, michel_dbl_turn_RDS[energy], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_michel", "Michel Positrons", useBinned=useBinnedData)
            positronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, positron_dbl_turn_RDS[energy], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_pos", "Mott Positrons", useBinned=useBinnedData)
            electronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, electron_dbl_turn_RDS[energy], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_elec", "Mott Electrons", useBinned=useBinnedData)

            for result, graph in zip([michelDblTurnResult, positronDblTurnResult, electronDblTurnResult], [michelGraphs, positronGraphs, electronGraphs]):
                setPointAndErr(graph["DeltaL8Res"]["p"], energy, dblTurnBinWidth, result.getMinSigma(), dblTurn=True)
                setPointAndErr(graph["EnergyLoss"]["p"], energy, dblTurnBinWidth, result.mpv)
                setPointAndErr(graph["LandauWidth"]["p"], energy, dblTurnBinWidth, result.landau_sigma)

            if (args.DblTurnVsLambdaFits):
                for lam in L8_lam_points:
                    michelDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, michel_dbl_turn_RDS_for_cuts[energy]["lam"][lam], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_michel", "Michel Positrons", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L8_lam_binWidth, useBinned=useBinnedData)
                    positronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, positron_dbl_turn_RDS_for_cuts[energy]["lam"][lam], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_pos", "Mott Positrons", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L8_lam_binWidth, useBinned=useBinnedData)
                    electronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, electron_dbl_turn_RDS_for_cuts[energy]["lam"][lam], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_elec", "Mott Electrons", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L8_lam_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([michelDblTurnResult, positronDblTurnResult, electronDblTurnResult], [michelGraphsPerEnergy, positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["DeltaL8Res"]["lam"], lam, L8_lam_binWidth, result.getMinSigma(), dblTurn=True)
                        setPointAndErr(graph[energy]["EnergyLoss"]["lam"], lam, L8_lam_binWidth, result.mpv)
                        setPointAndErr(graph[energy]["LandauWidth"]["lam"], lam, L8_lam_binWidth, result.landau_sigma)

            if (args.DblTurnVsZPCAFits):
                for zpca in zpca_points:
                    michelDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, michel_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_michel", "Michel Positrons", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)
                    positronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, positron_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_pos", "Mott Positrons", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)
                    electronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, electron_dbl_turn_RDS_for_cuts[energy]["zpca"][zpca], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_elec", "Mott Electrons", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([michelDblTurnResult, positronDblTurnResult, electronDblTurnResult], [michelGraphsPerEnergy, positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["DeltaL8Res"]["zpca"], zpca, zpca_binWidth, result.getMinSigma(), dblTurn=True)
                        setPointAndErr(graph[energy]["EnergyLoss"]["zpca"], zpca, zpca_binWidth, result.mpv)
                        setPointAndErr(graph[energy]["LandauWidth"]["zpca"], zpca, zpca_binWidth, result.landau_sigma)
            
            if (args.DblTurnVsPhiFits):
                for phi in phi_points:
                    michelDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, michel_dbl_turn_RDS_for_cuts[energy]["phi"][phi], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_michel", "Michel Positrons", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)
                    positronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, positron_dbl_turn_RDS_for_cuts[energy]["phi"][phi], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_pos", "Mott Positrons", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)
                    electronDblTurnResult = fitDoubleGaussAndLandau(dblTurnMomentumRV, electron_dbl_turn_RDS_for_cuts[energy]["phi"][phi], energy, deltaL8_shared_mean_init, dblTurnBinWidth, "deltaL8_elec", "Mott Electrons", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([michelDblTurnResult, positronDblTurnResult, electronDblTurnResult], [michelGraphsPerEnergy, positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["DeltaL8Res"]["phi"], phi, phi_binWidth, result.getMinSigma(), dblTurn=True)
                        setPointAndErr(graph[energy]["EnergyLoss"]["phi"], phi, phi_binWidth, result.mpv)
                        setPointAndErr(graph[energy]["LandauWidth"]["phi"], phi, phi_binWidth, result.landau_sigma)

        # Mott peak fits
        if (args.MottPeakFits):
            momentumRV.setRange("resolutionRange", energy - mottPeakBinWidth, energy + mottPeakBinWidth)
            MCmomentumRV.setRange("resolutionRange", energy - mottPeakBinWidth, energy + mottPeakBinWidth)
            mottPeak_shared_mean_init = (energy-0.2, energy-0.5, energy+0.2)
            # mottPeak_shared_mean_init = (energy-0.5, energy-1.5, energy+0.2) # no ecorr reco sample

            positronMottPeakL6Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS[energy]["L6"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_pos", "Mott Positrons (L6 tracks)", useBinned=useBinnedData)
            electronMottPeakL6Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS[energy]["L6"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_elec", "Mott Electrons (L6 tracks)", useBinned=useBinnedData)

            positronMottPeakL8Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS[energy]["L8"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_pos", "Mott Positrons (L8 tracks)", useBinned=useBinnedData)
            electronMottPeakL8Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS[energy]["L8"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_elec", "Mott Electrons (L8 tracks)", useBinned=useBinnedData)

            for result, graph in zip([positronMottPeakL8Result, electronMottPeakL8Result], [positronGraphs, electronGraphs]):
                setPointAndErr(graph["MottPeakL8_Offset"]["p"], energy, mottPeakBinWidth, result.mean, offset=-energy)
                setPointAndErr(graph["MottPeakL8_Mean"]["p"], energy, mottPeakBinWidth, result.mean)
                setPointAndErr(graph["MottPeakL8Res"]["p"], energy, mottPeakBinWidth, result.sigma)

            for result, graph in zip([positronMottPeakL6Result, electronMottPeakL6Result], [positronGraphs, electronGraphs]):
                setPointAndErr(graph["MottPeakL6_Offset"]["p"], energy, mottPeakBinWidth, result.mean, offset=-energy)
                setPointAndErr(graph["MottPeakL6_Mean"]["p"], energy, mottPeakBinWidth, result.mean)
                setPointAndErr(graph["MottPeakL6Res"]["p"], energy, mottPeakBinWidth, result.sigma)

            # MC res mott peak fits
            if (args.MottPeakMCFits):
                # TODO add michels here?
                positronMottPeak_MC_Res_L6_Result = fitCrystalBall(truthResRV, positron_Mott_peak_RDS[energy]["MC_Res_L6"], energy, mc_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Res_L6_pos", "Mott Positrons (L6 tracks)", useBinned=useBinnedData)
                electronMottPeak_MC_Res_L6_Result = fitCrystalBall(truthResRV, electron_Mott_peak_RDS[energy]["MC_Res_L6"], energy, mc_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Res_L6_elec", "Mott Electrons (L6 tracks)", useBinned=useBinnedData)

                positronMottPeak_MC_Res_L8_Result = fitCrystalBall(truthResRV, positron_Mott_peak_RDS[energy]["MC_Res_L8"], energy, mc_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Res_L8_pos", "Mott Positrons (L8 tracks)", useBinned=useBinnedData)
                electronMottPeak_MC_Res_L8_Result = fitCrystalBall(truthResRV, electron_Mott_peak_RDS[energy]["MC_Res_L8"], energy, mc_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Res_L8_elec", "Mott Electrons (L8 tracks)", useBinned=useBinnedData)

                for result, graph in zip([positronMottPeak_MC_Res_L8_Result, electronMottPeak_MC_Res_L8_Result], [positronGraphs, electronGraphs]):
                    setPointAndErr(graph["TruthBiasL8"]["p"], energy, mottPeakBinWidth, result.mean)
                    setPointAndErr(graph["TruthResL8"]["p"], energy, mottPeakBinWidth, result.sigma)

                for result, graph in zip([positronMottPeak_MC_Res_L6_Result, electronMottPeak_MC_Res_L6_Result], [positronGraphs, electronGraphs]):
                    setPointAndErr(graph["TruthBiasL6"]["p"], energy, mottPeakBinWidth, result.mean)
                    setPointAndErr(graph["TruthResL6"]["p"], energy, mottPeakBinWidth, result.sigma)

                # MC momentum
                positronMottPeak_MC_Mom_L6_Result = fitCrystalBall(MCmomentumRV, positron_Mott_peak_RDS[energy]["MC_Mom_L6"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Mom_L6_pos", "Mott Positrons (L6 tracks)", useBinned=useBinnedData)
                electronMottPeak_MC_Mom_L6_Result = fitCrystalBall(MCmomentumRV, electron_Mott_peak_RDS[energy]["MC_Mom_L6"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Mom_L6_elec", "Mott Electrons (L6 tracks)", useBinned=useBinnedData)

                positronMottPeak_MC_Mom_L8_Result = fitCrystalBall(MCmomentumRV, positron_Mott_peak_RDS[energy]["MC_Mom_L8"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Mom_L8_pos", "Mott Positrons (L8 tracks)", useBinned=useBinnedData)
                electronMottPeak_MC_Mom_L8_Result = fitCrystalBall(MCmomentumRV, electron_Mott_peak_RDS[energy]["MC_Mom_L8"], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeak_MC_Mom_L8_elec", "Mott Electrons (L8 tracks)", useBinned=useBinnedData)

                for result, graph in zip([positronMottPeak_MC_Mom_L8_Result, electronMottPeak_MC_Mom_L8_Result], [positronGraphs, electronGraphs]):
                    setPointAndErr(graph["MC_MottPeakL8_Offset"]["p"], energy, mottPeakBinWidth, result.mean, offset=-energy)
                    setPointAndErr(graph["MC_MottPeakL8_Mean"]["p"], energy, mottPeakBinWidth, result.mean)
                    setPointAndErr(graph["MC_MottPeakL8Res"]["p"], energy, mottPeakBinWidth, result.sigma)

                for result, graph in zip([positronMottPeak_MC_Mom_L6_Result, electronMottPeak_MC_Mom_L6_Result], [positronGraphs, electronGraphs]):
                    setPointAndErr(graph["MC_MottPeakL6_Offset"]["p"], energy, mottPeakBinWidth, result.mean, offset=-energy)
                    setPointAndErr(graph["MC_MottPeakL6_Mean"]["p"], energy, mottPeakBinWidth, result.mean)
                    setPointAndErr(graph["MC_MottPeakL6Res"]["p"], energy, mottPeakBinWidth, result.sigma)

            # Lambda fits
            if (args.MottPeakVsLambdaFits):
                for lam in L6_lam_points:
                    positronMottPeakL6Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L6"]["lam"][lam], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_pos", "Mott Positrons (L6 tracks)", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L6_lam_binWidth, useBinned=useBinnedData)
                    electronMottPeakL6Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L6"]["lam"][lam], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_elec", "Mott Electrons (L6 tracks)", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L6_lam_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([positronMottPeakL6Result, electronMottPeakL6Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL6Res"]["lam"], lam, L6_lam_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL6_Offset"]["lam"], lam, L6_lam_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL6_Mean"]["lam"], lam, L6_lam_binWidth, result.mean)

                for lam in L8_lam_points:
                    positronMottPeakL8Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L8"]["lam"][lam], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_pos", "Mott Positrons (L8 tracks)", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L8_lam_binWidth, useBinned=useBinnedData)
                    electronMottPeakL8Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L8"]["lam"][lam], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_elec", "Mott Electrons (L8 tracks)", cut_var_name="lam", cut_var_val=lam, cut_binWidth=L8_lam_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([positronMottPeakL8Result, electronMottPeakL8Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL8Res"]["lam"], lam, L8_lam_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL8_Offset"]["lam"], lam, L8_lam_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL8_Mean"]["lam"], lam, L8_lam_binWidth, result.mean)

            if (args.MottPeakVsPhiFits):
                for phi in phi_points:
                    positronMottPeakL6Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L6"]["phi"][phi], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_pos", "Mott Positrons (L6 tracks)", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)
                    electronMottPeakL6Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L6"]["phi"][phi], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_elec", "Mott Electrons (L6 tracks)", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)

                    positronMottPeakL8Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L8"]["phi"][phi], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_pos", "Mott Positrons (L8 tracks)", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)
                    electronMottPeakL8Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L8"]["phi"][phi], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_elec", "Mott Electrons (L8 tracks)", cut_var_name="phi", cut_var_val=phi, cut_binWidth=phi_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([positronMottPeakL8Result, electronMottPeakL8Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL8Res"]["phi"], phi, phi_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL8_Offset"]["phi"], phi, phi_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL8_Mean"]["phi"], phi, phi_binWidth, result.mean)

                    for result, graph in zip([positronMottPeakL6Result, electronMottPeakL6Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL6Res"]["phi"], phi, phi_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL6_Offset"]["phi"], phi, phi_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL6_Mean"]["phi"], phi, phi_binWidth, result.mean)

            if (args.MottPeakVsZPCAFits):
                for zpca in zpca_points:
                    positronMottPeakL6Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L6"]["zpca"][zpca], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_pos", "Mott Positrons (L6 tracks)", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)
                    electronMottPeakL6Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L6"]["zpca"][zpca], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL6_elec", "Mott Electrons (L6 tracks)", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)

                    positronMottPeakL8Result = fitCrystalBall(momentumRV, positron_Mott_peak_RDS_for_cuts[energy]["L8"]["zpca"][zpca], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_pos", "Mott Positrons (L8 tracks)", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)
                    electronMottPeakL8Result = fitCrystalBall(momentumRV, electron_Mott_peak_RDS_for_cuts[energy]["L8"]["zpca"][zpca], energy, mottPeak_shared_mean_init, mottPeakBinWidth, "MottPeakL8_elec", "Mott Electrons (L8 tracks)", cut_var_name="zpca", cut_var_val=zpca, cut_binWidth=zpca_binWidth, useBinned=useBinnedData)

                    for result, graph in zip([positronMottPeakL8Result, electronMottPeakL8Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL8Res"]["zpca"], zpca, zpca_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL8_Offset"]["zpca"], zpca, zpca_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL8_Mean"]["zpca"], zpca, zpca_binWidth, result.mean)

                    for result, graph in zip([positronMottPeakL6Result, electronMottPeakL6Result], [positronGraphsPerEnergy, electronGraphsPerEnergy]):
                        setPointAndErr(graph[energy]["MottPeakL6Res"]["zpca"], zpca, zpca_binWidth, result.sigma)
                        setPointAndErr(graph[energy]["MottPeakL6_Offset"]["zpca"], zpca, zpca_binWidth, result.mean, offset=-energy)
                        setPointAndErr(graph[energy]["MottPeakL6_Mean"]["zpca"], zpca, zpca_binWidth, result.mean)

    if (args.MottPeakFits):
        # TODO: Fit per energy for lambda?
        fitMomentumOffsetFromTGraph(positronGraphs["MottPeakL6_Offset"]["p"], "MottPeakL6_Offset_positron_fit", energies, title="Mott Positron (L6)", ymin=-1, ymax=0.5)
        fitMomentumOffsetFromTGraph(positronGraphs["MottPeakL8_Offset"]["p"], "MottPeakL8_Offset_positron_fit", energies, title="Mott Positron (L8)", ymin=-1, ymax=0.5)
        fitMomentumOffsetFromTGraph(electronGraphs["MottPeakL6_Offset"]["p"], "MottPeakL6_Offset_electron_fit", energies, title="Mott Electron (L6)", ymin=-1, ymax=0.5)
        fitMomentumOffsetFromTGraph(electronGraphs["MottPeakL8_Offset"]["p"], "MottPeakL8_Offset_electron_fit", energies, title="Mott Electron (L8)", ymin=-1, ymax=0.5)

    # Save all graphs
    for graphs in [michelGraphs, positronGraphs, electronGraphs]:
        saveGraphs(graphs)

    for graphList in [positronGraphsPerEnergy, electronGraphsPerEnergy]:
        for energy in graphList:
            saveGraphs(graphList[energy])

    energies.reverse()
    # select energies you want here

    # TODO split up some plots further by e+/e- and L6/L8...
    # TODO turn off xerror bars?

    for energy in energies:
        for track in ["L6", "L8"]:
            for particle in ["positron", "electron"]:
                hScaled = scaledMomentumHistos[particle][str(energy)][track]
                hNominal = nomialMomentumHistos[particle][str(energy)][track]

                # zoom in?
                overlayMultipleHistos([hNominal, hScaled],
                                      ["Nominal", "Scaled"],
                                      (0.20,0.70,0.35,0.85),
                                      ";p_{Reco} [MeV/#it{c}]; Counts",
                                      f"{outputGraphsDir}ScaledAndNominalMomentum_{particle}_{track}_{energy}",
                                      xmin=(energy-3), xmax=(energy+3))

    if (args.DblTurnFits):
        # DeltaL8 resolutions vs momentum
        overlayMultipleTGraphs([michelGraphs["DeltaL8Res"]["p"], positronGraphs["DeltaL8Res"]["p"], electronGraphs["DeltaL8Res"]["p"]],
                            ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                            (0.20,0.65,0.35,0.85),
                            "L8_{Turn 2} - L8_{Turn 1}; p_{Reco} [MeV/#it{c}]; #sigma_{p} [MeV/#it{c}]",
                            outputGraphsDir + "DeltaL8FitMomentumResVsMomentum",
                            ymin=0.0, ymax=0.7, xmin=5.0, xmax=60.0,
                            legStyle="LEP")

        # DeltaL8 (mean) energy loss vs momentum
        overlayMultipleTGraphs([michelGraphs["EnergyLoss"]["p"], positronGraphs["EnergyLoss"]["p"], electronGraphs["EnergyLoss"]["p"]],
                            ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                            (0.20,0.65,0.35,0.85),
                            "L8_{Turn 2} - L8_{Turn 1};p_{Reco} [MeV/#it{c}]; (MPV) Energy Loss [MeV/#it{c}]",
                            outputGraphsDir + "MPVEnergyLossVsMomentum",
                            ymin=0.6, ymax=1.0, xmin=5.0, xmax=60.0,
                            legStyle="LEP")

        # DeltaL8 landau variable vs momentum
        overlayMultipleTGraphs([michelGraphs["LandauWidth"]["p"], positronGraphs["LandauWidth"]["p"], electronGraphs["LandauWidth"]["p"]],
                            ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                            (0.20,0.65,0.35,0.85),
                            "L8_{Turn 2} - L8_{Turn 1};p_{Reco} [MeV/#it{c}]; #sigma_{Landau} [MeV/#it{c}]",
                            outputGraphsDir + "LandauWidthVsMomentum",
                            ymin=0.0, ymax=0.2, xmin=5.0, xmax=60.0,
                            legStyle="LEP")

        # To see results more clearly per type, may need to do this
        # for graphs, particleType in zip([michelGraphsPerEnergy, positronGraphsPerEnergy, electronGraphsPerEnergy], ["Michel", "Positron", "Electron"]):

        # overlay michel/positron/electron per energy vs zpca/lambda/phi
        for energy in energies:
            if (args.DblTurnVsZPCAFits):
                # DeltaL8 resolution vs zpca
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["DeltaL8Res"]["zpca"], positronGraphsPerEnergy[energy]["DeltaL8Res"]["zpca"], electronGraphsPerEnergy[energy]["DeltaL8Res"]["zpca"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #it{z}_{Reco} [mm]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + f"DeltaL8FitMomentumResVsZPCA_{energy}",
                                    ymin=0.0, ymax=0.7, legStyle="LEP", xmin=min(zpca_points), xmax=max(zpca_points))

                # DeltaL8 (mean) energy loss vs zpca
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["EnergyLoss"]["zpca"], positronGraphsPerEnergy[energy]["EnergyLoss"]["zpca"], electronGraphsPerEnergy[energy]["EnergyLoss"]["zpca"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #it{z}_{Reco} [mm]; (MPV) Energy Loss [MeV/#it{c}]",
                                    outputGraphsDir + f"MPVEnergyLossVsZPCA_{energy}",
                                    ymin=0.6, ymax=1.0, legStyle="LEP", xmin=min(zpca_points), xmax=max(zpca_points))

            if (args.DblTurnVsPhiFits):
                # DeltaL8 resolution vs phi
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["DeltaL8Res"]["phi"], positronGraphsPerEnergy[energy]["DeltaL8Res"]["phi"], electronGraphsPerEnergy[energy]["DeltaL8Res"]["phi"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #phi_{Reco} [rad]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + f"DeltaL8FitMomentumResVsPhi_{energy}",
                                    ymin=0.0, ymax=0.7, legStyle="LEP", xmin=min(phi_points), xmax=max(phi_points))

                # DeltaL8 (mean) energy loss vs phi
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["EnergyLoss"]["phi"], positronGraphsPerEnergy[energy]["EnergyLoss"]["phi"], electronGraphsPerEnergy[energy]["EnergyLoss"]["phi"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #phi_{Reco} [rad]; (MPV) Energy Loss [MeV/#it{c}]",
                                    outputGraphsDir + f"MPVEnergyLossVsPhi_{energy}",
                                    ymin=0.6, ymax=1.0, legStyle="LEP", xmin=min(phi_points), xmax=max(phi_points))

            if (args.DblTurnVsLambdaFits):
                # DeltaL8 resolution vs lambda
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["DeltaL8Res"]["lam"], positronGraphsPerEnergy[energy]["DeltaL8Res"]["lam"], electronGraphsPerEnergy[energy]["DeltaL8Res"]["lam"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #lambda_{Reco} [rad]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + f"DeltaL8FitMomentumResVsLambda_{energy}",
                                    ymin=0.0, ymax=0.7, legStyle="LEP", xmin=min(L8_lam_points), xmax=max(L8_lam_points))

                # DeltaL8 (mean) energy loss vs lambda
                overlayMultipleTGraphs([michelGraphsPerEnergy[energy]["EnergyLoss"]["lam"], positronGraphsPerEnergy[energy]["EnergyLoss"]["lam"], electronGraphsPerEnergy[energy]["EnergyLoss"]["lam"]],
                                    ["Michel e^{+}", "Mott e^{+}", "Mott e^{-}"],   
                                    (0.20,0.65,0.35,0.85),
                                    "L8_{Turn 2} - L8_{Turn 1}; #lambda_{Reco} [rad]; (MPV) Energy Loss [MeV/#it{c}]",
                                    outputGraphsDir + f"MPVEnergyLossVsLambda_{energy}",
                                    ymin=0.6, ymax=1.0, legStyle="LEP", xmin=min(L8_lam_points), xmax=max(L8_lam_points))

    if (args.MottPeakFits):
        # Mott peak resolutions (e+/e- and L6/L8) vs momentum
        overlayMultipleTGraphs([positronGraphs["MottPeakL8Res"]["p"], electronGraphs["MottPeakL8Res"]["p"], positronGraphs["MottPeakL6Res"]["p"], electronGraphs["MottPeakL6Res"]["p"]],
                            ["e^{+} (L8)",  "e^{-} (L8)", "e^{+} (L6)",  "e^{-} (L6)"],
                            (0.20,0.65,0.40,0.85),
                            "Mott peak resolution;p_{Reco} [MeV/#it{c}]; #sigma_{p} [MeV/#it{c}]",
                            outputGraphsDir + "MottPeakFitMomentumResVsMomentum",
                            ymin=0.0, ymax=0.7, xmin=5.0, xmax=60.0,
                            legStyle="LEP", ncols=2)

        # Momentum scale/offset for mott peaks (e+/e- and L6/L8) vs momentum
        overlayMultipleTGraphs([positronGraphs["MottPeakL6_Offset"]["p"], electronGraphs["MottPeakL6_Offset"]["p"], positronGraphs["MottPeakL8_Offset"]["p"], electronGraphs["MottPeakL8_Offset"]["p"]],
                            ["e^{+} (L6)", "e^{-} (L6)", "e^{+} (L8)", "e^{-} (L8)"],
                            (0.60,0.70,0.85,0.85),
                            "Mott peak fitting;p_{Reco} [MeV/#it{c}]; Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]",
                            outputGraphsDir + "MomentumOffsetVsMomentum",
                            ymin=-1, ymax=0.2, xmin=5.0, xmax=60.0, ncols=2,
                            legStyle="LEP")

        # Fitted momentum peak
        overlayMultipleTGraphs([positronGraphs["MottPeakL6_Mean"]["p"], electronGraphs["MottPeakL6_Mean"]["p"], positronGraphs["MottPeakL8_Mean"]["p"], electronGraphs["MottPeakL8_Mean"]["p"]],
                            ["e^{+} (L6)", "e^{-} (L6)", "e^{+} (L8)", "e^{-} (L8)"],
                            (0.60,0.20,0.85,0.35),
                            "Mott peak fitting;p_{Reco} [MeV/#it{c}]; Momentum (#mu_{fit}) [MeV/#it{c}]",
                            outputGraphsDir + "FittedMomentumVsOrignalMomentum",
                            ymin=5, ymax=55, xmin=5.0, xmax=60.0, ncols=2,
                            legStyle="LEP")

    if (args.MottPeakMCFits):
        # MC Mott peak resolutions (e+/e- and L6/L8) vs momentum
        overlayMultipleTGraphs([positronGraphs["TruthResL8"]["p"], electronGraphs["TruthResL8"]["p"], positronGraphs["TruthResL6"]["p"], electronGraphs["TruthResL6"]["p"]],
                            ["e^{+} (L8)",  "e^{-} (L8)", "e^{+} (L6)",  "e^{-} (L6)"],
                            (0.20,0.65,0.40,0.85),
                            "Truth MC resolution for Mott peak selection;p_{Reco} [MeV/#it{c}]; #sigma_{p} [MeV/#it{c}]",
                            outputGraphsDir + "MC_ResVsMomForMottPeak",
                            ymin=0.0, ymax=0.7, xmin=5.0, xmax=60.0,
                            legStyle="LEP", ncols=2)

        overlayMultipleTGraphs([positronGraphs["MC_MottPeakL8Res"]["p"], electronGraphs["MC_MottPeakL8Res"]["p"], positronGraphs["MC_MottPeakL6Res"]["p"], electronGraphs["MC_MottPeakL6Res"]["p"]],
                            ["e^{+} (L8)",  "e^{-} (L8)", "e^{+} (L6)",  "e^{-} (L6)"],
                            (0.20,0.65,0.40,0.85),
                            "Mott peak MC resolution;p_{Reco} [MeV/#it{c}]; #sigma_{p} [MeV/#it{c}]",
                            outputGraphsDir + "MC_MottPeakFitMomentumResVsMomentum",
                            ymin=0.0, ymax=0.7, xmin=5.0, xmax=60.0,
                            legStyle="LEP", ncols=2)

        # MC res for mott peaks (e+/e- and L6/L8) vs momentum
        overlayMultipleTGraphs([positronGraphs["TruthBiasL6"]["p"], electronGraphs["TruthBiasL6"]["p"], positronGraphs["TruthBiasL8"]["p"], electronGraphs["TruthBiasL8"]["p"]],
                            ["e^{+} (L6)", "e^{-} (L6)", "e^{+} (L8)", "e^{-} (L8)"],
                            (0.60,0.70,0.85,0.85),
                            "Truth bias for Mott peak selection;p_{Reco} [MeV/#it{c}];#mu_{fit} [MeV/#it{c}]",
                            outputGraphsDir + "MC_ResBiasVsMomentum",
                            ymin=-0.4, ymax=0.2, xmin=5.0, xmax=60.0, ncols=2,
                            legStyle="LEP")

        overlayMultipleTGraphs([positronGraphs["MC_MottPeakL6_Offset"]["p"], electronGraphs["MC_MottPeakL6_Offset"]["p"], positronGraphs["MC_MottPeakL8_Offset"]["p"], electronGraphs["MC_MottPeakL8_Offset"]["p"]],
                            ["e^{+} (L6)", "e^{-} (L6)", "e^{+} (L8)", "e^{-} (L8)"],
                            (0.60,0.70,0.85,0.85),
                            "Mott peak MC momentum offset;p_{Reco} [MeV/#it{c}];Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]",
                            outputGraphsDir + "MC_MomentumOffsetVsMomentum",
                            ymin=-0.4, ymax=0.2, xmin=5.0, xmax=60.0, ncols=2,
                            legStyle="LEP")

        overlayMultipleTGraphs([positronGraphs["MC_MottPeakL6_Mean"]["p"], electronGraphs["MC_MottPeakL6_Mean"]["p"], positronGraphs["MC_MottPeakL8_Mean"]["p"], electronGraphs["MC_MottPeakL8_Mean"]["p"]],
                            ["e^{+} (L6)", "e^{-} (L6)", "e^{+} (L8)", "e^{-} (L8)"],
                            (0.60,0.20,0.85,0.35),
                            "Mott peak MC momentum mean;p_{Reco} [MeV/#it{c}];Momentum (#mu_{fit}) [MeV/#it{c}]",
                            outputGraphsDir + "MC_MomentumMeanVsMomentum",
                            ymin=5, ymax=55, xmin=5.0, xmax=60.0, ncols=2,
                            legStyle="LEP")

    for segType in ["L6", "L8"]:
        l_points = L6_lam_points if segType=="L6" else L8_lam_points
        for graphs, particleType in zip([positronGraphsPerEnergy, electronGraphsPerEnergy], ["Positron", "Electron"]):

            base = "MottPeak" + segType
            offsetIdx = base + "_Offset"
            resIdx = base + "Res"
            meanIdx = base + "_Mean"

            # TODO change xmin/xmax for plots

            if (args.MottPeakVsLambdaFits):
                # Mott peak vars (per input energy) vs lambda
                overlayMultipleTGraphs([graphs[energy][offsetIdx]["lam"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #lambda_{Reco} [rad]; Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]",
                                    outputGraphsDir + offsetIdx + "VsLambdaForMott" + particleType,
                                    ymin=-0.5, ymax=0.5, xmin=min(l_points), xmax=max(l_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][resIdx]["lam"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #lambda_{Reco} [rad]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + resIdx + "VsLambdaForMott" + particleType,
                                    ymin=0.0, ymax=1.1, xmin=min(l_points), xmax=max(l_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][meanIdx]["lam"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.45,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #lambda_{Reco} [rad]; Momentum (#mu_{fit}) [MeV/#it{c}]",
                                    outputGraphsDir + meanIdx + "VsLambdaForMott" + particleType,
                                    ymin=5, ymax=55, xmin=min(l_points), xmax=max(l_points),
                                    legStyle="LEP", ncols=2)

                # TODO plot offset/res/mean per energy?
                for energy in energies:
                    # Plot tgraphs for each energy
                    c = ROOT.TCanvas()
                    gr = graphs[energy][meanIdx]["lam"].Clone()
                    gr.SetTitle("Mott " + particleType + " Peak (" + segType + "); #lambda_{Reco} [rad]; Momentum (#mu_{fit}) [MeV/#it{c}]")
                    gr.SetMarkerColor(ROOT.kC0)
                    gr.SetLineColor(ROOT.kC0)
                    gr.GetXaxis().SetLimits(min(l_points), max(l_points))
                    gr.GetYaxis().SetRangeUser(energy-2, energy+2)
                    gr.Draw("AP")
                    c.SaveAs(outputGraphsDir + meanIdx + "VsLambdaForMott" + particleType + f"_{energy:.1f}MeV.pdf")


            if (args.MottPeakVsPhiFits):
                overlayMultipleTGraphs([graphs[energy][offsetIdx]["phi"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #phi_{Reco} [rad]; Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]",
                                    outputGraphsDir + offsetIdx + "VsPhiForMott" + particleType,
                                    ymin=-0.5, ymax=0.5, xmin=min(phi_points), xmax=max(phi_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][resIdx]["lam"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #phi_{Reco} [rad]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + resIdx + "VsPhiForMott" + particleType,
                                    ymin=0.0, ymax=1.1, xmin=min(phi_points), xmax=max(phi_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][meanIdx]["phi"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.45,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #phi_{Reco} [rad]; Momentum (#mu_{fit}) [MeV/#it{c}]",
                                    outputGraphsDir + meanIdx + "VsPhiForMott" + particleType,
                                    ymin=5, ymax=55, xmin=min(phi_points), xmax=max(phi_points),
                                    legStyle="LEP", ncols=2)

                for energy in energies:
                    # Plot tgraphs for each energy
                    c = ROOT.TCanvas()
                    gr = graphs[energy][meanIdx]["phi"].Clone()
                    gr.SetTitle("Mott " + particleType + " Peak (" + segType + "); #phi_{Reco} [rad]; Momentum (#mu_{fit}) [MeV/#it{c}]")
                    gr.SetMarkerColor(ROOT.kC0)
                    gr.SetLineColor(ROOT.kC0)
                    gr.GetXaxis().SetLimits(min(phi_points), max(phi_points))
                    gr.GetYaxis().SetRangeUser(energy-2, energy+2)
                    gr.Draw("AP")
                    c.SaveAs(outputGraphsDir + meanIdx + "VsPhiForMott" + particleType + f"_{energy:.1f}MeV.pdf")

            if (args.MottPeakVsZPCAFits):
                overlayMultipleTGraphs([graphs[energy][offsetIdx]["zpca"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #it{z}_{Reco} [mm]; Momentum scale (#mu_{fit} - #mu_{beam}) [MeV/#it{c}]",
                                    outputGraphsDir + offsetIdx + "VsZPCAForMott" + particleType,
                                    ymin=-0.5, ymax=0.5, xmin=min(zpca_points), xmax=max(zpca_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][resIdx]["lam"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.55,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #it{z}_{Reco} [mm]; #sigma_{p} [MeV/#it{c}]",
                                    outputGraphsDir + resIdx + "VsZPCAForMott" + particleType,
                                    ymin=0.0, ymax=1.1, xmin=min(zpca_points), xmax=max(zpca_points),
                                    legStyle="LEP", ncols=2)

                overlayMultipleTGraphs([graphs[energy][meanIdx]["zpca"] for energy in energies],
                                    [f"{energy:.1f} MeV" for energy in energies],
                                    (0.20,0.67,0.45,0.87),
                                    "Mott " + particleType + " Peak (" + segType + "); #it{z}_{Reco} [mm]; Momentum (#mu_{fit}) [MeV/#it{c}]",
                                    outputGraphsDir + meanIdx + "VsZPCAForMott" + particleType,
                                    ymin=5, ymax=55, xmin=min(zpca_points), xmax=max(zpca_points),
                                    legStyle="LEP", ncols=2)

                for energy in energies:
                    # Plot tgraphs for each energy
                    c = ROOT.TCanvas()
                    gr = graphs[energy][meanIdx]["phi"].Clone()
                    gr.SetTitle("Mott " + particleType + " Peak (" + segType + "); #it{z}_{Reco} [mm]; Momentum (#mu_{fit}) [MeV/#it{c}]")
                    gr.SetMarkerColor(ROOT.kC0)
                    gr.SetLineColor(ROOT.kC0)
                    gr.GetXaxis().SetLimits(min(zpca_points), max(zpca_points))
                    gr.GetYaxis().SetRangeUser(energy-2, energy+2)
                    gr.Draw("AP")
                    c.SaveAs(outputGraphsDir + meanIdx + "VsZPCAForMott" + particleType + f"_{energy:.1f}MeV.pdf")

    # All resolutions vs momentum on one plot...
    # overlayMultipleTGraphs([michelGraphs["DeltaL8Res"]["p"], positronGraphs["DeltaL8Res"]["p"], positronGraphs["MottPeakL6Res"], positronGraphs["MottPeakL8Res"], electronGraphs["DeltaL8Res"]["p"], electronGraphs["MottPeakL6Res"], electronGraphs["MottPeakL8Res"]],
    #                        ["Michel e^{+} (Delta L8)", "Mott e^{+} (Delta L8)", "Mott e^{+} (L6 - Peak)",  "Mott e^{+} (L8 - Peak)", "Mott e^{-} (Delta L8)", "Mott e^{-} (L6 - Peak)", "Mott e^{-} (L8 - Peak)"],
    #                        (0.20,0.65,0.65,0.85),
    #                        ";p_{Reco} [MeV/#it{c}]; #sigma_{p} [MeV/#it{c}]",
    #                        outputGraphsDir + "FitMomentumResVsMomentum",
    #                        ymin=0.0, ymax=1.2, xmin=5.0, xmax=60.0,
    #                        legStyle="LEP", ncols=2)

    output_file.Write()
    output_file.Close()

    timeTaken = time.time() - start
    print(f"Took {timeTaken:.2f} s...")
