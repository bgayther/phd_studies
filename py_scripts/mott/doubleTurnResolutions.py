import json
import ROOT
import numpy as np
from collections import defaultdict, OrderedDict
import argparse
from math import log10, floor
from tqdm import tqdm

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of framesTree to process; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='doubleTurnRecoRes', help='Set name of output .root file')
parser.add_argument('--outputGraphsDir', type=str, default='', help='output path')
parser.add_argument('--path', type=str, default="", help='path/to/reco_file.root (not including file)')
args = parser.parse_args()

ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h")
ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/cpp_scripts/include/RootColourDefs.hpp")

colours = ROOT.RootColourDefs()
doubleTurnColour = colours.kC4

ROOT.gStyle.SetHistLineColor(doubleTurnColour)
ROOT.gStyle.SetMarkerColor(doubleTurnColour)

ROOT.gROOT.SetBatch(True)
segs = ROOT.TChain("segs")

outputGraphsDir = args.outputGraphsDir + "/"

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    segs.Add(file)

df = ROOT.RDataFrame(segs)
cols = df.Filter("mc==1").AsNumpy(["p", "mc_prime", "mc", "nhit", "hid00", "hid20", "mc_tid", "seq"])

# first pass
L8_mc_prime_tids_and_hids = OrderedDict()
LP_tids_and_hids = defaultdict(list)
ntrip = 6
for seg in range(len(cols['nhit'])):
    # print(seg, cols['nhit'][seg], type(cols['mc_prime'][seg]))
    if (cols['nhit'][seg]==8):
        # print("dss")
        mc_hids = []
        for i in range(ntrip):
            mc_hids.append(cols['hid00'][seg][i])
        mc_hids.append(cols['hid20'][seg][ntrip -2])        
        mc_hids.append(cols['hid20'][seg][ntrip -1])
        if (cols['mc_prime'][seg]==1): L8_mc_prime_tids_and_hids[cols['mc_tid'][seg]] = (mc_hids, seg)
        if (cols['mc'][seg]==1): LP_tids_and_hids[cols['mc_tid'][seg]].append((mc_hids, seg, cols['seq'][seg]))

pdiffL8 = ROOT.TH1D("pdiffL8", "LP: L8_2 - L8_1;#Deltap [MeV/#it{c}];Counts", 100, -10, 15)
pdiff = ROOT.TH1D("pdiff", "L8: S4_2 - S4_1;#Deltap [MeV/#it{c}];Counts", 100, -10, 15)

for L8_tid, list_of_tuples in tqdm(LP_tids_and_hids.items()):
    if (len(list_of_tuples) >= 2):
        # print(L8_tid, list_of_tuples)

        foundSecond = False
        foundFirst = False

        p_turn_2 = 0.0
        p_turn_1 = 0.0

        # print("new track")
        for (L8_hids, seg_idx, seq) in list_of_tuples:
            # print(L8_hids, seg_idx, seq)
            if (seq == 0 and cols["mc_prime"][seg_idx]==1):
                foundFirst = True
                p_turn_1 = abs(cols["p"][seg_idx])
            if (seq == 1): #need to check mc_hids != first turn..
                foundSecond = True
                p_turn_2 = abs(cols["p"][seg_idx])

        if (foundFirst and foundSecond and (p_turn_2-p_turn_1) != 0.0): 
            # print(p_turn_2, p_turn_1, p_turn_2-p_turn_1)
            pdiffL8.Fill((p_turn_2-p_turn_1))

for L8_tid, (L8_hids, seg_idx) in tqdm(L8_mc_prime_tids_and_hids.items()):
    # print(L8_hids)
    foundSecond = False
    foundFirst = False

    min_idx = seg_idx - 10
    max_idx = seg_idx + 10
    if (min_idx <= 0): min_idx = 0
    if (max_idx >= len(cols['nhit'])): max_idx = len(cols['nhit'])-1

    p_turn_2 = 0.0
    p_turn_1 = 0.0
    for seg in range(min_idx, max_idx):
        if (cols['mc_tid'][seg] == L8_tid and cols['nhit'][seg]==4): 
            hid0_diff = abs(L8_hids[7] - cols['hid00'][seg][0])
            hid1_diff = abs(L8_hids[6] - cols['hid00'][seg][1])
            hid2_diff = abs(L8_hids[5] - cols['hid20'][seg][0])
            hid3_diff = abs(L8_hids[4] - cols['hid20'][seg][1])
            if (max([hid0_diff, hid1_diff, hid2_diff, hid3_diff]) <= 1):
                foundSecond = True
                p_turn_2 = abs(cols['p'][seg])
    
    if (foundSecond):
        for seg in range(min_idx, max_idx):
            if (cols['mc_tid'][seg] == L8_tid and cols['mc_prime'][seg]==1 and cols['nhit'][seg]==4): 
                hid0_diff = abs(L8_hids[0] - cols['hid00'][seg][0])
                hid1_diff = abs(L8_hids[1] - cols['hid00'][seg][1])
                hid2_diff = abs(L8_hids[2] - cols['hid20'][seg][0])
                hid3_diff = abs(L8_hids[3] - cols['hid20'][seg][1])
                if (max([hid0_diff, hid1_diff, hid2_diff, hid3_diff]) <= 1):
                    foundFirst = True
                    p_turn_1 = abs(cols['p'][seg])

    if (foundFirst and foundSecond and (p_turn_2-p_turn_1) != 0.0):
        # print(p_turn_2 - p_turn_1)
        pdiff.Fill((p_turn_2 - p_turn_1))


print(pdiff.GetEntries())
name = outputGraphsDir + "h_double_turn_p_diff.pdf"
can = ROOT.TCanvas()
pdiff.Draw()
can.SaveAs(name)

name = outputGraphsDir + "h_double_turn_p_diff_L8_LP.pdf"
pdiffL8.Draw()
ROOT.gPad.Update()
can.SaveAs(name)

# print(L8_mc_prime_tids_and_hids)
print("done!")














