#############################################################
### Base submission script for running mu3e grid jobs  ######
###     03/12/2020   A. Groves                         ######
#############################################################

from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)

from DIRAC.Interfaces.API.Job import Job
from DIRAC.Interfaces.API.Dirac import Dirac
import datetime
import os
import sys
t=datetime.datetime.now()
dirac = Dirac()

#Set the important variables
GroupName='Mu3eAnaICNorm_'+t.strftime("%d")+t.strftime("%m")+t.strftime("%y") #Name
local=False #Run the job locally for testing purposes
frames = 100 #Number of frames per file
start = 102 #Start seed
end = 102 #End seed
# in total 5000 files

softwareLocation="build_4.4.3" #Specify release version
storageLocation='/mu3e.org/user/b/benjamin.gayther/IC_norms_build_4.4.3' #Change this path to your own user '/mu3e.org/user/<initial>/<userid>/<relativepath>
changeDetectorConfig=False
ExecutableCode='./runIC_norm.sh' #Specify simulation script to run other options are <runSim_Signal.sh/runSim_IC.sh/runSim_ICmic.sh/runSim_Bhabba.sh>
certianRuns=[] #Fill with certian seeds to run over <- If filled takes priority over start and end loop

def setup_submit(seed,frames,local,GroupName,softwareLocation,storageLocation,ExecutableCode):
    print("Setting up job for seed: "+str(seed))
    j = Job()
    myenv = { #Sets up enviroment varibles to be used in the grid job
        "MU3EPATH":"/cvmfs/gridpp.egi.eu/Mu3e/"+softwareLocation,
        "StorageLocation":storageLocation,
        "seed":seed,
        "Executable":ExecutableCode
    }
    j.setExecutionEnv(myenv)
    j.setCPUTime(500)
    j.setInputData([f"/mu3e.org/prod/build4.4.3/IC/mu3e_run_{seed:06d}.root"])
    j.setInputSandbox([ExecutableCode])
    j.setExecutable('GRIDRun.sh') #Set the executable that needed to run

    #Uncomment the following line to ban any particular site that can't run your mu3e jobs.
    #j.setBannedSites(['LCG.UKI-SOUTHGRID-BRIS-HEP.uk','LCG.UKI-SOUTHGRID-OX-HEP.uk','LCG.UKI-NORTHGRID-LANCS-HEP.uk','LCG.UKI-LT2-Brunel.uk','LCG.UKI-NORTHGRID-LIV-HEP.uk','LCG.UKI-SCOTGRID-ECDF.uk','LCG.RAL-LCG2.uk'])

    #For debugging porpuses you can use this command to save log file(s) out of the grid node and save it into the outputSE.
    #The path specified by outputPath is a relative path from /mu3e.org/user/initial/userid/relativepath
    j.setOutputData(['Script1_GRIDRun.sh.log'], outputSE='UKI-NORTHGRID-LIV-HEP-disk', outputPath='logs')
    #j.setOutputData(['skim_000001.root'],['UKI-NORTHGRID-LIV-HEP-disk'],'ICmic')
    j.setName(GroupName+'_%d'%seed)
    if(local):
        j.runLocal() #Runs the job locally
    else:
        result = dirac.submitJob(j)
        jobid = result['JobID']
        print (jobid)
        # print job id to file for future reference
        joblog = open(GroupName+".txt", "a")
        joblog.write(str(jobid)+'\n')
        joblog.close()

    return None

# Alerts you to the run id file already existing
if(os.path.exists(GroupName+".txt")):
    exist = str(input("Group Name job id list already exists, Do you want to remove? yes or no: "))
    if exist == "yes" or exist == "Yes":
        print ("Removing ,"+GroupName+".txt")
        os.remove(GroupName+".txt")
    elif exist == "no" or exist == "No" :
        print ("Exiting code, please change group name")
        sys.exit()
    else:
        f=0
        while(f == 0):
            exist1 = str(input("Please say yes or no "))
            if exist1 == "yes" or exist1 == "Yes":
                os.remove(GroupName+".txt")
                f += 1
            elif exist1 == "no" or exist == "No":
                f += 1
                sys.exit()

#Check if a local softare location exists (needed if going to run locally)
if(not(os.path.exists("/cvmfs/gridpp.egi.eu/Mu3e/"+softwareLocation))):
    if(local):
        print ("Software can't find local cvmfs so can not run local, please change")
        sys.exit()
    exist = str(input("Software not found on local CVMFS, please confirm (yes or no) "))
    if exist == "yes" or exist == "Yes":
        print ("No Software locally, contining anyway")
    elif exist == "no" or exist == "No" :
        print ("Exiting code, please check")
        sys.exit()
    else:
        f=0
        while(f == 0):
            exist1 = str(input("Please say yes or no "))
            if exist1 == "yes" or exist1 == "Yes":
                f += 1
            elif exist1 == "no" or exist == "No":
                print ("Exiting code, please check")
                sys.exit()

if(local):
    print ("Running the simulation locally")

if(len(certianRuns) > 0): #If you want certian run id numbers loop over them
    for i in range(len(certianRuns)):
        setup_submit(certianRuns[i],frames,local,GroupName,softwareLocation,storageLocation,ExecutableCode)
else:
    for i in range(start,end+1):
        setup_submit(i,frames,local,GroupName,softwareLocation,storageLocation,ExecutableCode)

print ("List of Job IDs printed to file "+GroupName+".txt")
