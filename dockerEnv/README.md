docker build --progress=plain -t mu3e_docker_env .
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
docker run --rm -it -e DISPLAY=$ip:0 -v /tmp/.X11-unix:/tmp/.X11-unix --name="mu3eDockerContainer" -v /Users/bgayther/Mu3eWorkingArea:/ben_dev/ -v ~/.docker_bash_history:/root/.bash_history mu3e_docker_env bash
