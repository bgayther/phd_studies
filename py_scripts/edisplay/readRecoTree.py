import json
import ROOT
import argparse
import os

parser = argparse.ArgumentParser(description='Read alignment tree from file and produce json for event display',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--input', dest="inputFile", type=str, required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of framesTree to process; -1 = ALL')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e_cosmicRun/mu3e/tests/data", help='path/to/sim_file.root (not including file)')
args = parser.parse_args()

os.chdir(args.path)

inFile = ROOT.TFile.Open(args.inputFile)
cfg = json.loads(str(inFile.Get("config").GetString())) # if needed
alignment = inFile.GetDirectory("alignment")

frames = inFile.Get("frames")
frameData = []

max_events = frames.GetEntries()
if (args.nevents != -1): max_events = args.nevents

# TODO: merge tracks.json and eventSample.json??

trk_ctr = 0
for i in frames:
    trk_ctr+=1
    if (frames.n>0 and trk_ctr<=max_events):
        event = {"nTracks" : frames.n, "frameID" : frames.eventId, "tracks" : []}
        print("ntrks ", frames.n, " len(x_track) ", len(frames.x_track))
        for trk in range(frames.n):
            trackData = {
                "nHits" : frames.nhit[trk],
                "x_track" : [],
                "y_track" : [],
                "z_track" : []
                # "sensorIds" : [],
                # "r" : frames.r[trk],
                # "x0" : frames.x0[trk],
                # "y0" : frames.y0[trk],
                # "z0" : frames.z0[trk],
                # "k" : frames.k[trk],
                # "phi0" : frames.phi0[trk],
                # "phi1" : frames.phi1[trk],
                # "rt" : frames.rt[trk],
                # "c" : {"x" : frames.cx[trk], "y" : frames.cy[trk]},
                # "h0" : {"x" : frames.h0x[trk], "y" : frames.h0y[trk], "z" : frames.h0z[trk]},
                # "h1" : {"x" : frames.h1x[trk], "y" : frames.h1y[trk], "z" : frames.h1z[trk]}
            }
            if (trk==0): offset = frames.n_points[trk] * trk * (frames.nhit[trk]-1)
            else: offset = frames.n_points[trk] * trk * (frames.nhit[trk-1]-1)
            max = frames.n_points[trk] * (frames.nhit[trk]-1) + offset
            print("  Trk {}, nhit {}, offset {}, max {}".format(trk, frames.nhit[trk], offset, max))
            for i in range(offset, max):
                trackData["x_track"].append(frames.x_track[i])
                trackData["y_track"].append(frames.y_track[i])
                trackData["z_track"].append(frames.z_track[i])
            print("  n points in data = ", len(trackData["x_track"]))
            event["tracks"].append(trackData)

        frameData.append(event)


with open('tracks.json', 'w') as outfile:
    json.dump(frameData,  outfile, indent=4)