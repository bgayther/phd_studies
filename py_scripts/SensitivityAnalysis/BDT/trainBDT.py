import pandas as pd
import numpy as np
from prepareData import desired_vars
from testBDT import main 
import argparse
import pickle
import xgboost as xgb
import time
from sklearn.model_selection import train_test_split
import matplotlib as mpl
import mplhep as hep
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score, roc_auc_score, f1_score
import pathlib
from sklearn.preprocessing import StandardScaler

mpl.use('Agg')
hep.style.use("LHCb2")
mpl.rcParams.update({"axes.grid" : False})

selected_vars = ["chi2", "pm", "targetdist", "mep1", "mmass", "cosalpha", "phi_kuno", "theta_kuno", "pcoplanar", "pacoplanar"]
# selected_vars = desired_vars
# TODO: process with timing info as well ["tv_1", "tv_1err", "tv_chi2", "tv_chi2_bhabha", "t0_si1", "t0_fb1", "t0_tl1"]

def norm_data(bhabha_data, IC_data, sig_data, weights, sig_strength=1e-13):
    # Load weights
    IC_weight_sum = weights["IC_weight_sum"]
    n_bhabha_frames_skim = weights["n_bhabha_frames_skim"]
    n_signal_events = weights["n_signal_events"]

    # Scale weights
    # TODO: may need to divide by number of files or number of times I do this normalisation
    NStopsNorm = 2.5e15
    bhabha_frac = 7.7e-5
    timing_suppression = 70
    avg_bhabha_weight_per_frame = 4.502925517058626e-06 # pre skim (adjusting for different framelengths/beam rates...)
    skim_frame_weight = n_bhabha_frames_skim * avg_bhabha_weight_per_frame
    bhabha_weight = (NStopsNorm * bhabha_frac) / (skim_frame_weight * timing_suppression)
    bhabha_data["NormWeight"] = bhabha_data["weight"] * bhabha_weight

    sim_IC_BR = 2.486041e-06
    IC_weight = (NStopsNorm * sim_IC_BR * 3.4e-5) / IC_weight_sum
    IC_data["NormWeight"] = IC_data["weight"] * IC_weight

    sig_weight = (NStopsNorm * sig_strength) / n_signal_events
    sig_data["NormWeight"] = sig_data["weight"] * sig_weight

    return

def join_dataframes(files, columns=None):
    # print("Reading files: ", files)
    df = None
    for file in files:
        if df is None:
            df = pd.read_parquet(file, columns=columns)
        else:
            df = pd.concat([df, pd.read_parquet(file, columns=columns)], ignore_index=True)
            # print("Memory usage (GB): ", df.memory_usage(index=True).sum() / 1e9)
    return df

# def f1_eval(y_pred, dtrain, threshold=0.5):
#     y_true = dtrain.get_label()
#     err = 1-f1_score(y_true, y_pred > threshold)
#     return 'f1_err', err

def train_bdt(param, X_train, y_train, w_train, eval_watchlist, sample_weight_eval_set, use_weights=True):
    clf = xgb.XGBClassifier(**param)
    # print(clf.get_params()) # just to check

    if use_weights:
        bdt = clf.fit(X_train, y_train, eval_set=eval_watchlist, sample_weight_eval_set=sample_weight_eval_set, eval_metric=param['eval_metric'], sample_weight=w_train)
    else:
        bdt = clf.fit(X_train, y_train, eval_set=eval_watchlist, sample_weight_eval_set=sample_weight_eval_set, eval_metric=param['eval_metric'])

    return bdt

def calculate_significance(y_true, w, tpr, fpr):
    N_sig = np.sum(w[y_true==1])
    N_bkg = np.sum(w[y_true==0])
    S = N_sig * tpr
    B = N_bkg * fpr
    significance = S/np.sqrt(S+B)
    return significance

def load_data(args):
    if args.quick:
        print("Quick test!")
        args.sig_files = args.sig_files[:3]
        args.bhabha_files = args.bhabha_files[:10]
        args.IC_files = args.IC_files[:3]

    # Load data in chunks
    weights = pickle.load(open(args.weightsFile, "rb"))

    # print(f"Signal files: {sig_files[i]}, Bhabha files: {bhabha_files[i]}, IC files: {IC_files[i]}")
    sig_data = join_dataframes(args.sig_files, columns=selected_vars+['mc_signal', 'weight'])
    IC_data = join_dataframes(args.IC_files, columns=selected_vars+['mc_signal', 'weight'])
    bhabha_data = join_dataframes(args.bhabha_files, columns=selected_vars+['mc_signal', 'weight'])

    # Create a category column for each data type (per row)
    sig_data["category"] = 0
    IC_data["category"] = 1
    bhabha_data["category"] = 2

    # Prepare data
    norm_data(bhabha_data, IC_data, sig_data, weights)

    # Concatenate data
    data = pd.concat([bhabha_data, IC_data, sig_data], ignore_index=True)

    return data

def split_and_scale_data(data, test_size=0.5, random_state=40, transform=False):
    train_data, test_data = train_test_split(data, test_size=test_size, random_state=random_state, stratify=data["mc_signal"])

    total_w_sum_signal = sum(data["NormWeight"][data["category"] == 0])
    total_w_sum_IC = sum(data["NormWeight"][data["category"] == 1])
    total_w_sum_bhabha = sum(data["NormWeight"][data["category"] == 2])
    print(f"Sum of all signal weights: {total_w_sum_signal}, sum of data IC background weights: {total_w_sum_IC}, bhaha: {total_w_sum_bhabha}")
    
    train_data.loc[train_data["category"] == 0, "NormWeight"] = train_data.loc[train_data["category"] == 0, "NormWeight"] * (total_w_sum_signal / sum(train_data["NormWeight"][train_data["category"] == 0]))
    train_data.loc[train_data["category"] == 1, "NormWeight"] = train_data.loc[train_data["category"] == 1, "NormWeight"] * (total_w_sum_IC / sum(train_data["NormWeight"][train_data["category"] == 1]))
    train_data.loc[train_data["category"] == 2, "NormWeight"] = train_data.loc[train_data["category"] == 2, "NormWeight"] * (total_w_sum_bhabha / sum(train_data["NormWeight"][train_data["category"] == 2]))

    test_data.loc[test_data["category"] == 0, "NormWeight"] = test_data.loc[test_data["category"] == 0, "NormWeight"] * (total_w_sum_signal / sum(test_data["NormWeight"][test_data["category"] == 0]))
    test_data.loc[test_data["category"] == 1, "NormWeight"] = test_data.loc[test_data["category"] == 1, "NormWeight"] * (total_w_sum_IC / sum(test_data["NormWeight"][test_data["category"] == 1]))
    test_data.loc[test_data["category"] == 2, "NormWeight"] = test_data.loc[test_data["category"] == 2, "NormWeight"] * (total_w_sum_bhabha / sum(test_data["NormWeight"][test_data["category"] == 2]))
    
    X_train = train_data[selected_vars]
    y_train = train_data["mc_signal"]
    w_train = train_data["NormWeight"]
    category_train = train_data["category"]

    X_test = test_data[selected_vars]
    y_test = test_data["mc_signal"]
    w_test = test_data["NormWeight"]
    category_test = test_data["category"]

    print(f"lengths of variables: X_train: {len(X_train)}, y_train: {len(y_train)}, w_train: {len(w_train)}, category_train: {len(category_train)}")
    print(f"Shape of X_train: {X_train.shape}, y_train: {y_train.shape}, w_train: {w_train.shape}, category_train: {category_train.shape}")

    print(f"Sum of (training) signal weights {sum(w_train[y_train == 1])}, sum of IC background weights {sum(w_train[category_train == 1])}, sum of bhabha background weights {sum(w_train[category_train == 2])}")
    print(f"Sum of (testing) signal weights {sum(w_test[y_test == 1])}, sum of IC background weights {sum(w_test[category_test == 1])}, sum of bhabha background weights {sum(w_test[category_test == 2])}")
    print(f"Absolute number of (training) signal events: {np.sum(y_train==1)} and IC background events: {np.sum(category_train==1)} and bhabha background events: {np.sum(category_train==2)}")
    print(f"Absolute number of (testing) signal events: {np.sum(y_test==1)} and IC background events: {np.sum(category_test==1)} and bhabha background events: {np.sum(category_test==2)}")

    # Scale data?
    if transform:
        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.fit_transform(X_test)

    return X_train, y_train, w_train, category_train, X_test, y_test, w_test, category_test

def setup_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--sig_files", type=str, nargs="+", help="Signal files")
    parser.add_argument("--bhabha_files", type=str, nargs="+", help="Bhabha files")
    parser.add_argument("--IC_files", type=str, nargs="+", help="IC files")
    parser.add_argument("--weightsFile", type=str, default="./weights.pickle", help="Weights file")
    parser.add_argument("--ncpu", type=int, default=8, help="Number of CPUs to use")
    parser.add_argument("--resultsDir", type=str, default="./results", help="Results directory")
    parser.add_argument("--quick", action="store_true", help="Quick test")
    args = parser.parse_args()

    return args

if __name__ == "__main__":
    args = setup_args()

    print("Reading data...")

    resultsDir = args.resultsDir

    # Remove trailing slash
    if resultsDir[-1] == "/":
        resultsDir = resultsDir[:-1]
        
    pathlib.Path(resultsDir).mkdir(parents=True, exist_ok=True)
    pathlib.Path(resultsDir + "/test").mkdir(parents=True, exist_ok=True)
    pathlib.Path(resultsDir + "/train").mkdir(parents=True, exist_ok=True)
    pathlib.Path(resultsDir + "/train/var_plots").mkdir(parents=True, exist_ok=True)

    # TODO pass this
    param = {'max_depth': 7, 'eta': 0.2, 'objective': 'binary:logistic', 'n_estimators': 250, 'n_jobs': -1, 'nthread': -1, 'scale_pos_weight': 1, 
             'eval_metric': 'aucpr', 'early_stopping_rounds': 200, 'subsample': 0.9, 'tree_method': 'hist'}

    if args.quick:
        param['n_estimators'] = 45

    # Load data
    data = load_data(args)

    # Split data
    X_train, y_train, w_train, category_train, X_test, y_test, w_test, category_test = split_and_scale_data(data, test_size=0.5, random_state=40)
    print("Data loaded and split")

    # Make dict of hist info (range+n_bins) per variable (selected_vars)
    histo_info = {
        "chi2": {
            "bins": 120,
            "range": (0, 30)
        },
        "pm": {
            "bins": 75,
            "range": (0, 70)
        },
        "targetdist": {
            "bins": 80,
            "range": (-20, 20)
        },
        "mep1": {
            "bins": 160,
            "range": (0, 80)
        },
        "mmass": {
            "bins": 175,
            "range": (80, 115)
        },
        "cosalpha": {
            "bins": 40,
            "range": (-1, 1)
        },
        "phi_kuno": {
            "bins": 125,
            "range": (0, 2*np.pi)
        },
        "theta_kuno": {
            "bins": 62,
            "range": (0, np.pi)
        },
        "pcoplanar": {
            "bins": 350,
            "range": (0, 70)
        },
        "pacoplanar": {
            "bins": 600,
            "range": (-60, 60)
        }
    }

    # Loop over vaaariables in X_train and plot them
    print("Plotting variables...")
    for var in X_train.columns:
        plt.figure()
        plt.hist(X_train.loc[category_train == 0][var], label="Signal", histtype="step", weights=w_train[category_train == 0], bins=histo_info[var]["bins"], range=histo_info[var]["range"])
        plt.hist(X_train.loc[category_train == 1][var], label="IC", histtype="step", weights=w_train[category_train == 1], bins=histo_info[var]["bins"], range=histo_info[var]["range"])
        plt.hist(X_train.loc[category_train == 2][var], label="Bhabha", histtype="step", weights=w_train[category_train == 2], bins=histo_info[var]["bins"], range=histo_info[var]["range"])
        plt.xlabel(var)
        plt.ylabel("Weighted events")
        plt.yscale("log")
        plt.legend()
        plt.savefig(resultsDir + "/train/var_plots/" + var + ".pdf")
        plt.close()

    # plot pm vs mmass
    plt.figure()
    plt.hist2d(X_train.loc[category_train == 0]["pm"], X_train.loc[category_train == 0]["mmass"], label="Signal", weights=w_train[category_train == 0], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Greens")
    plt.xlabel("pm")
    plt.ylabel("mmass")
    plt.colorbar()
    plt.savefig(resultsDir + "/train/var_plots/mmass_vs_pm_signal.pdf")
    plt.close()

    plt.figure()
    plt.hist2d(X_train.loc[category_train == 1]["pm"], X_train.loc[category_train == 1]["mmass"], label="IC", weights=w_train[category_train == 1], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Reds")
    plt.xlabel("pm")
    plt.ylabel("mmass")
    plt.colorbar()
    plt.savefig(resultsDir + "/train/var_plots/mmass_vs_pm_IC.pdf")
    plt.close()

    plt.figure()
    plt.hist2d(X_train.loc[category_train == 2]["pm"], X_train.loc[category_train == 2]["mmass"], label="Bhabha", weights=w_train[category_train == 2], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Blues")
    plt.xlabel("pm")
    plt.ylabel("mmass")
    plt.colorbar()
    plt.savefig(resultsDir + "/train/var_plots/mmass_vs_pm_Bhabha.pdf")
    plt.close()

    plt.figure(figsize=(24, 18))
    _, _, _, h1 = plt.hist2d(X_train.loc[category_train == 0]["pm"], X_train.loc[category_train == 0]["mmass"], label="Signal", weights=w_train[category_train == 0], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Greens")
    _, _, _, h2 = plt.hist2d(X_train.loc[category_train == 1]["pm"], X_train.loc[category_train == 1]["mmass"], label="IC", weights=w_train[category_train == 1], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Reds")
    _, _, _, h3 = plt.hist2d(X_train.loc[category_train == 2]["pm"], X_train.loc[category_train == 2]["mmass"], label="Bhabha", weights=w_train[category_train == 2], range=((0, 15), (90, 115)), bins=(75, 125), norm=mpl.colors.LogNorm(), cmap="Blues")
    plt.xlabel("pm")
    plt.ylabel("mmass")
    plt.colorbar(h1, label="Signal")
    plt.colorbar(h2, label="IC")
    plt.colorbar(h3, label="Bhabha")
    plt.savefig(resultsDir + "/train/var_plots/mmass_vs_pm_all.pdf")
    plt.close()

    # Scale training weights to handle class imbalance?
    # sig_weight_sum = sum(w_train[y_train == 1])
    # back_weight_sum = sum(w_train[y_train == 0])
    
    # sig_freq = sum(y_train)
    # back_freq = sum(y_train == 0)
    
    # sig_scale = sig_freq / sig_weight_sum
    # back_scale = back_freq / back_weight_sum
    
    # w_train = w_train * (y_train*sig_scale + (1-y_train)*back_scale)

    # Create bhabha and IC datasets (i.e. remove IC and Bhabha events from training and set)
    X_train_bhabha = X_train[category_train.isin([0, 2])]
    y_train_bhabha = y_train[category_train.isin([0, 2])]
    w_train_bhabha = w_train[category_train.isin([0, 2])]

    X_test_bhabha = X_test[category_test.isin([0, 2])]
    y_test_bhabha = y_test[category_test.isin([0, 2])]
    w_test_bhabha = w_test[category_test.isin([0, 2])]

    X_train_ic = X_train[category_train.isin([0, 1])]
    y_train_ic = y_train[category_train.isin([0, 1])]
    w_train_ic = w_train[category_train.isin([0, 1])]

    X_test_ic = X_test[category_test.isin([0, 1])]
    y_test_ic = y_test[category_test.isin([0, 1])]
    w_test_ic = w_test[category_test.isin([0, 1])]

    print("Training IC/Sig BDT...")
    start = time.time()
    bdt_IC = train_bdt(param, X_train_ic, y_train_ic, w_train_ic, [(X_train_ic, y_train_ic), (X_test_ic, y_test_ic)], [w_train_ic, w_test_ic], use_weights=False) #use_weights=False
    end = time.time()
    print(f"Time to train BDT: {end - start:.2f}")

    print("Training Bhabha/Sig BDT...")
    start = time.time()
    bdt_bhabha = train_bdt(param, X_train_bhabha, y_train_bhabha, w_train_bhabha, [(X_train_bhabha, y_train_bhabha), (X_test_bhabha, y_test_bhabha)], [w_train_bhabha, w_test_bhabha], use_weights=False) #use_weights=False
    end = time.time()
    print(f"Time to train BDT: {end - start:.2f}")

    del X_train_bhabha, y_train_bhabha, w_train_bhabha, X_test_bhabha, y_test_bhabha, w_test_bhabha
    del X_train_ic, y_train_ic, w_train_ic, X_test_ic, y_test_ic, w_test_ic

    # Loss plot
    evals_result = bdt_IC.evals_result()
    plt.figure()
    plt.plot(evals_result['validation_0'][param["eval_metric"]], label='Train')
    plt.plot(evals_result['validation_1'][param["eval_metric"]], label='Test')
    plt.xlabel('Iteration')
    plt.ylabel(param["eval_metric"])
    plt.legend()
    plt.savefig(f'{resultsDir}/train/eval_metric_vs_estimators_IC_model.pdf')
    plt.close()

    evals_result = bdt_bhabha.evals_result()
    plt.figure()
    plt.plot(evals_result['validation_0'][param["eval_metric"]], label='Train')
    plt.plot(evals_result['validation_1'][param["eval_metric"]], label='Test')
    plt.xlabel('Iteration')
    plt.ylabel(param["eval_metric"])
    plt.legend()
    plt.savefig(f'{resultsDir}/train/eval_metric_vs_estimators_Bhabha_model.pdf')
    plt.close()

    # Combine predictions (probability of being signal for both models)
    y_pred_IC = bdt_IC.predict_proba(X_train)[:,1]

    # Find optimal threshold for IC
    fpr, tpr, thresholds = roc_curve(y_train, y_pred_IC, sample_weight=w_train)
    significance = calculate_significance(y_train, w_train, tpr, fpr)

    optimal_cut = thresholds[np.nanargmax(significance)]

    plt.figure()
    plt.plot(thresholds, significance)
    plt.xlabel('BDT cut value (IC model)')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(0, 1.0)
    plt.axvline(optimal_cut, color='black', linestyle='--')
    plt.text(optimal_cut+0.01, np.nanmax(significance)/1.5, f"Optimal cut: {optimal_cut:.3f}", horizontalalignment='left', verticalalignment='top')
    plt.savefig(f'{resultsDir}/train/significance_vs_threshold_IC_model.pdf')
    plt.close()

    print(f"Optimal cut for IC model: {optimal_cut:.3f}")

    # Apply optimal cut
    y_pred_IC = (y_pred_IC > optimal_cut).astype(int)

    # Find optimal threshold for Bhabha
    y_pred_bhabha = bdt_bhabha.predict_proba(X_train)[:,1]
    fpr, tpr, thresholds = roc_curve(y_train, y_pred_bhabha, sample_weight=w_train)
    significance = calculate_significance(y_train, w_train, tpr, fpr)

    optimal_cut = thresholds[np.nanargmax(significance)]

    plt.figure()
    plt.plot(thresholds, significance)
    plt.xlabel('BDT cut value (Bhabha model)')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(0, 1.0)
    plt.axvline(optimal_cut, color='black', linestyle='--')
    plt.text(optimal_cut+0.01, np.nanmax(significance)/1.5, f"Optimal cut: {optimal_cut:.3f}", horizontalalignment='left', verticalalignment='top')
    plt.savefig(f'{resultsDir}/train/significance_vs_threshold_Bhabha_model.pdf')
    plt.close()

    print(f"Optimal cut for Bhabha model: {optimal_cut:.3f}")

    # Apply optimal cut
    y_pred_bhabha = (y_pred_bhabha > optimal_cut).astype(int)

    # Combine predictions 
    y_pred = y_pred_IC + y_pred_bhabha

    # print(f"Min BDT output: {np.min(y_pred):.3f}, Max BDT output: {np.max(y_pred):.3f}, Mean BDT output: {np.mean(y_pred):.3f}, sum: {np.sum(y_pred):.3f}")
    print(f"Min BDT (IC) output: {np.min(y_pred_IC):.3f}, Max BDT (IC) output: {np.max(y_pred_IC):.3f}, Mean BDT (IC) output: {np.mean(y_pred_IC):.3f}, sum: {np.sum(y_pred_IC):.3f}")
    print(f"Min BDT (Bhabha) output: {np.min(y_pred_bhabha):.3f}, Max BDT (Bhabha) output: {np.max(y_pred_bhabha):.3f}, Mean BDT (Bhabha) output: {np.mean(y_pred_bhabha):.3f}, sum: {np.sum(y_pred_bhabha):.3f}")

    fpr, tpr, thresholds = roc_curve(y_train, y_pred, sample_weight=w_train)
    aucpr = auc(fpr, tpr)
    auroc = roc_auc_score(y_train, y_pred, sample_weight=w_train)

    # Find optimal threshold using custom metric
    significance = calculate_significance(y_train, w_train, tpr, fpr)

    optimal_cut = thresholds[np.nanargmax(significance)]

    plt.figure()
    plt.plot(thresholds, significance)
    plt.xlabel('BDT cut value')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(0, 1.0)
    plt.axvline(optimal_cut, color='black', linestyle='--')
    plt.text(optimal_cut+0.01, np.nanmax(significance)/1.5, f"Optimal cut: {optimal_cut:.3f}", horizontalalignment='left', verticalalignment='top')
    plt.savefig(f'{resultsDir}/train/significance_vs_threshold.pdf')
    plt.close()

    # Plot precision,recall and F1 score vs threshold
    precision, recall, thresholds = precision_recall_curve(y_train, y_pred, sample_weight=w_train)
    f1 = 2 * (precision * recall) / (precision + recall)

    plt.figure()
    plt.plot(thresholds, precision[:-1], label='Precision')
    plt.plot(thresholds, recall[:-1], label='Recall')
    plt.plot(thresholds, f1[:-1], label='F1 score')
    plt.xlabel('BDT cut value')
    plt.ylabel('Score')
    plt.xlim(0, 1.0)
    plt.legend()
    plt.axvline(optimal_cut, color='black', linestyle='--')
    # plt.text(optimal_cut+0.01, 0.9, f"Optimal cut: {optimal_cut:.3f}", horizontalalignment='left', verticalalignment='top')
    plt.savefig(f'{resultsDir}/train/precision_recall_f1_vs_threshold.pdf')

    # Plot BDT output (unweighted)
    plt.figure()
    plt.hist(y_pred[y_train==1], bins=100, range=(0,1), label='Signal', histtype='step')
    plt.hist(y_pred[y_train==0], bins=100, range=(0,1), label='Background', histtype='step')
    plt.xlabel('BDT output')
    plt.ylabel('Events')
    plt.legend()
    plt.yscale('log')
    plt.savefig(f'{resultsDir}/train/unweighted_bdt_output.pdf')
    plt.close()

    # Plot BDT output (weighted)
    plt.figure()
    plt.hist(y_pred[y_train==1], bins=100, range=(0,1), weights=w_train[y_train==1], label='Signal', histtype='step')
    plt.hist(y_pred[y_train==0], bins=100, range=(0,1), weights=w_train[y_train==0], label='Background', histtype='step')
    plt.xlabel('BDT output')
    plt.ylabel('Events')
    plt.legend()
    plt.yscale('log')
    plt.savefig(f'{resultsDir}/train/weighted_bdt_output.pdf')
    plt.close()

    # Define search window
    mmass_range = (101, 107.5)
    pm_range = (0, 5)

    search_mask = (X_train["mmass"] > mmass_range[0]) & (X_train["mmass"] < mmass_range[1]) & (X_train["pm"] > pm_range[0]) & (X_train["pm"] < pm_range[1])

    # Plot BDT output (weighted) in search region
    plt.figure()
    plt.hist(y_pred[(y_train==1) & search_mask], bins=100, range=(0,1), 
                    weights=w_train[(y_train==1) & search_mask], label='Signal', histtype='step')
    plt.hist(y_pred[(y_train==0) & search_mask], bins=100, range=(0,1),
                    weights=w_train[(y_train==0) & search_mask], label='Background', histtype='step')
    plt.xlabel('BDT output')
    plt.ylabel('Events')
    plt.legend()
    plt.yscale('log')
    plt.savefig(f'{resultsDir}/train/search_region_weighted_bdt_output.pdf')
    plt.close()

    # Calculate significance in search region
    search_data = X_train[search_mask]
    search_y = y_train[search_mask]
    search_w = w_train[search_mask]
    search_y_pred = (bdt_IC.predict_proba(search_data)[:,1] + bdt_bhabha.predict_proba(search_data)[:,1]) / 2.0

    search_fpr, search_tpr, search_thresholds = roc_curve(search_y, search_y_pred, sample_weight=search_w)
    search_significance = calculate_significance(search_y, search_w, search_tpr, search_fpr)

    optimal_search_cut = search_thresholds[np.nanargmax(search_significance)]
    
    plt.figure()
    plt.plot(search_thresholds, search_significance)
    plt.xlabel('BDT cut value')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(0, 1.0)
    plt.axvline(optimal_search_cut, color='black', linestyle='--')
    plt.savefig(f'{resultsDir}/train/search_region_significance_vs_threshold.pdf')
    plt.close()

    # Plot feature importance
    plt.figure()
    xgb.plot_importance(bdt_IC, max_num_features=20)
    plt.savefig(f"{resultsDir}/train/FeatureImportance_IC_model.pdf")
    plt.close()

    plt.figure()
    xgb.plot_importance(bdt_bhabha, max_num_features=20)
    plt.savefig(f"{resultsDir}/train/FeatureImportance_bhabha_model.pdf")
    plt.close()
    
    # Save optimal cut
    print(f"Optimal cut (in search region): {optimal_search_cut}, optimal cut (overall): {optimal_cut}")
    with open(f'{resultsDir}/optimal_cut.txt', 'w') as f:
        f.write(f"{optimal_cut}")
    
    print("Saving IC BDT model...")
    with open(f'{resultsDir}/bdt_IC.pickle', 'wb') as handle:
        pickle.dump(bdt_IC, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    print("Saving bhabha BDT model...")
    with open(f'{resultsDir}/bdt_bhabha.pickle', 'wb') as handle:
        pickle.dump(bdt_bhabha, handle, protocol=pickle.HIGHEST_PROTOCOL)

    mpl.rcParams.update(mpl.rcParamsDefault)
    main(bdt_IC,  bdt_bhabha, X_test, y_test, w_test, optimal_cut, resultsDir, mmass_range=mmass_range, pm_range=pm_range)

    print("Finished!")