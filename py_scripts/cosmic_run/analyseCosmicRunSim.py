import ROOT
import numpy as np
import argparse
from collections import defaultdict
from tqdm import tqdm
import os
from copy import deepcopy
from math import atan2, atan
import json

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input sim file names')
parser.add_argument('-hole-file','--hole-file', dest='holes', type=str, required=False, help='Input holes conf file')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of frames to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='cosmicSimAna', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e_cosmicRun/mu3e/tests/data", help='path/to/sim_file.root (not including file though)')
args = parser.parse_args()

holes = []
if args.holes:
    with open(args.holes) as file:
        print("Holes file {} given".format(file))
        holesData = json.load(file)
        print(holesData["holes"])
        for chip, v in holesData["holes"].items():
            holes.append(int(chip))

os.chdir(args.path)
ROOT.gROOT.SetBatch(True)
# ROOT.gStyle.SetOptStat(0)

simTree = ROOT.TChain("mu3e")
mchits = ROOT.TChain("mu3e_mchits")

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    simTree.Add(file)
    mchits.Add(file)

simTree.SetBranchStatus("*", 0)
mchits.SetBranchStatus("*", 0)

nevents = simTree.GetEntries()
if args.nevents >= 0: nevents = args.nevents
skip = args.skip

# TODO move this to include file!

class MCHit:
    tid = np.zeros(1, dtype=np.int32)
    hid = np.zeros(1, dtype=np.int32)
    hid_g = np.zeros(1, dtype=np.int32)
    pdg = np.zeros(1, dtype=np.int32)
    edep = np.zeros(1, dtype=float)
    time = np.zeros(1, dtype=float)

    def __init__(self, tree):
        if(tree):
            tree.SetBranchStatus("tid", 1)
            tree.SetBranchStatus("hid", 1)
            tree.SetBranchStatus("hid_g", 1)
            tree.SetBranchStatus("pdg", 1)
            tree.SetBranchStatus("edep", 1)
            tree.SetBranchStatus("time", 1)
            tree.SetBranchAddress("tid", self.tid)
            tree.SetBranchAddress("hid", self.hid)
            tree.SetBranchAddress("hid_g", self.hid_g)
            tree.SetBranchAddress("pdg", self.pdg)
            tree.SetBranchAddress("edep", self.edep)
            tree.SetBranchAddress("time", self.time)
            self.tree = tree

    def load(self, i):
        self.tree.GetEntry(i)

class Frame:
    def __init__(self, tree, mcTree = 0):
        self.traj =  Frame.Traj(tree)
        self.header = Frame.Header(tree)
        self.pixel = Frame.Pixel(tree, mcTree)

    class Header:
        event = np.zeros(1, dtype=np.int32)
        def __init__(self, tree):
            tree.SetBranchStatus("Header", 1)
            tree.SetBranchAddress("Header", self.event)

    class Traj:
        Ntraj = np.zeros(1, dtype=np.int32)
        vx = ROOT.vector('double')()
        vy = ROOT.vector('double')()
        vz = ROOT.vector('double')()
        px = ROOT.vector('double')()
        py = ROOT.vector('double')()
        pz = ROOT.vector('double')()
        id = ROOT.vector('int')()
        mother = ROOT.vector('int')()
        type = ROOT.vector('int')()
        pid = ROOT.vector('int')()

        def __init__(self, tree):
            tree.SetBranchStatus("Ntrajectories", 1)
            tree.SetBranchStatus("traj_vx", 1)
            tree.SetBranchStatus("traj_vy", 1)
            tree.SetBranchStatus("traj_vz", 1)
            tree.SetBranchStatus("traj_px", 1)
            tree.SetBranchStatus("traj_py", 1)
            tree.SetBranchStatus("traj_pz", 1)
            tree.SetBranchStatus("traj_ID", 1)
            tree.SetBranchStatus("traj_type", 1)
            tree.SetBranchStatus("traj_PID", 1)
            tree.SetBranchStatus("traj_mother", 1)
            tree.SetBranchAddress("Ntrajectories", self.Ntraj)
            tree.SetBranchAddress("traj_vx", self.vx)
            tree.SetBranchAddress("traj_vy", self.vy)
            tree.SetBranchAddress("traj_vz", self.vz)
            tree.SetBranchAddress("traj_px", self.px)
            tree.SetBranchAddress("traj_py", self.py)
            tree.SetBranchAddress("traj_pz", self.pz)
            tree.SetBranchAddress("traj_ID", self.id)
            tree.SetBranchAddress("traj_type", self.type)
            tree.SetBranchAddress("traj_PID", self.pid)
            tree.SetBranchAddress("traj_mother", self.mother)

        def getN(self):
            return self.Ntraj[0]

        def getVr2(self, i):
            return self.vx[i]**2.0 + self.vy[i]**2.0

        def getVr(self, i):
            return np.sqrt(self.getVr2(i))

        def getPt2(self, i):
            return self.px[i]**2.0 + self.py[i]**2.0

        def getPt(self, i):
            # return np.sqrt(self.getPt2(i))
            return self.getP(i) * np.sin(self.getPhiAngle(i))

        def getP2(self, i):
            return self.px[i]**2.0 + self.py[i]**2.0 + self.pz[i]**2.0

        def getP(self, i):
            return np.sqrt(self.getP2(i))

        def getTid(self, i):
            return self.id[i]

        def getTids(self):
            tids = np.array([], dtype=int)
            types = np.array([], dtype=int)

            for traj_no in range(self.getN()):
                if (abs(self.pid[traj_no])==13 and (self.type[traj_no] == 4 or self.type[traj_no] == 3) and self.mother[traj_no]==0): #and self.mother[traj_no]!=0
                    tids = np.append(tids, int(self.id[traj_no]))
                    types = np.append(types, int(self.type[traj_no]))

            return [tids, types]

        def getIndex(self, trajs):
            index = defaultdict(int)
            for traj_no, _ in enumerate(trajs):
                    index[self.id[traj_no]] = traj_no
            return index

        def getPhiAngle(self, i):
            return atan2(self.py[i], self.px[i])

        def getThetaAngle(self, i):
            return np.pi/2.0 - atan(self.pz[i] / self.getPt(i))

    class Pixel:
        Nhit = np.zeros(1, dtype=np.int32)
        hit_pixelid = ROOT.vector('int')()
        hit_timestamp = ROOT.vector('int')()
        mci = ROOT.vector('int')()
        mcn = ROOT.vector('int')()

        mchits = False # MCHits present

        def __init__(self, tree, mcTree = 0):
            tree.SetBranchStatus("Nhit", 1)
            tree.SetBranchStatus("hit_pixelid", 1)
            tree.SetBranchStatus("hit_timestamp", 1)
            tree.SetBranchAddress("Nhit", self.Nhit)
            tree.SetBranchAddress("hit_pixelid", self.hit_pixelid)
            tree.SetBranchAddress("hit_timestamp", self.hit_timestamp)

            if tree.GetBranch("hit_mc_i"):
                tree.SetBranchStatus("hit_mc_i", 1)
                tree.SetBranchStatus("hit_mc_n", 1)
                tree.SetBranchAddress("hit_mc_i", self.mci)
                tree.SetBranchAddress("hit_mc_n", self.mcn)
                self.mchits = True

            self.mcHit = MCHit(mcTree)

        def getN(self):
            return self.Nhit[0]

        def getTid(self, i):
            tid = 0
            if self.mchits:
                if self.mcn[i] == 1:
                    self.mcHit.load(self.mci[i])
                    tid = self.mcHit.tid[0]
            return tid

        def getHid(self, i):
            hid = 0
            if self.mchits:
                if self.mcn[i] == 1:
                    self.mcHit.load(self.mci[i])
                    # hid = self.mcHit.hid_g[0]
                    hid = self.mcHit.hid[0]
            return hid

        def getMcTime(self, i):
            time = 0
            if self.mchits:
                self.mcHit.load(self.mci[i])
                time = self.mcHit.time[0]
            return time

        def getIds(self, i): 
            ''' returns tids and hids of hit i '''
            ids = defaultdict(list) #[tid][hids]
            tid = self.getTid(i)
            if(tid != 0):
                ids[tid].append(self.getHid(i))
            elif self.mchits:
                # ids[0] = [self.getHid(i)]
                for k in range(self.mcn[i]): 
                    self.mcHit.load(self.mci[i] + k)
                    tid = self.mcHit.tid[0]
                    hid = self.mcHit.hid[0]
                    if tid not in ids or hid not in ids[tid]:
                        ids[tid].append(hid)
            else:
                ids[0] = [self.getHid(i)]

            return ids

class PixelHit:
    sensorId = -1
    layer = -1
    mcTime = -1
    ids = []

    def __init__(self, pixelId, mcTime, ids):
        self.sensorId = pixelId >> 16
        self.layer = pixelId >> 26
        self.mcTime = mcTime
        self.ids = ids

    def print(self):
        print("sensorId {}, layer {}, mcTime {}, ids {}".format(self.sensorId, self.layer, self.mcTime, self.ids))

# # Counts each type (index corresponds to type, i.e. 11 postitron from Michel decay)
# def countRemainingTrajs(types, counter):
#     for ptype in types:
#         counter[ptype] += 1

# def getAndCountTrajs(frame, counter):
#     tids, types = frame.traj.getTids()
#     countRemainingTrajs(types, counter)
#     return [tids, types]

# Max number of different tids, hids
def getDiffIds(ids):
    ntids = len(ids)
    if ntids == 1 and 0 in ids:
        return [-1, -1]
    nhids = 0
    for tid, hid in ids.items():
        nhids_ = len(hid)
        if nhids_ > nhids:
            nhids = nhids_
    return [ntids, nhids]

def removeIdsFromTraj(ids, traj_tids):
    for tid, _ in ids.items():
        if tid in traj_tids:
            s = np.where(traj_tids == tid)
            traj_tids = np.delete(traj_tids, s)
    return traj_tids

frame = Frame(simTree, mchits)

output_file = ROOT.TFile(args.output+".root", 'RECREATE')

eff_ls = []
theta_acc_to_4hits = ROOT.TEfficiency("theta_acc_to_4hits", "Efficiency/Acceptance to 4 hits;#theta [rad];", 32 , 0.0, 3.2)
phi_acc_to_4hits = ROOT.TEfficiency("phi_acc_to_4hits", "Efficiency/Acceptance to 4 hits;#phi [rad];", 32 , -3.2, 0.0)
theta_acc_to_3hits  = ROOT.TEfficiency("theta_acc_to_3hits", "Efficiency/Acceptance to 3 hits;#theta [rad];", 32 , 0.0, 3.2)
phi_acc_to_3hits  = ROOT.TEfficiency("phi_acc_to_3hits", "Efficiency/Acceptance to 3 hits;#phi [rad];", 32 , -3.2, 0.0)

eff_ls.extend([theta_acc_to_4hits, phi_acc_to_4hits, theta_acc_to_3hits, phi_acc_to_3hits])

h_mc_phi_vs_theta_all = ROOT.TH2D("h_mc_phi_vs_theta_all", ";#theta [rad];#phi [rad]", 32 , 0.0, 3.2, 32, -3.2, 0.0)
h_mc_phi_vs_theta_none = ROOT.TH2D("h_mc_phi_vs_theta_none", ";#theta [rad];#phi [rad]", 32 , 0.0, 3.2, 32, -3.2, 0.0)
h_mc_phi_vs_theta_3hits_or_more = ROOT.TH2D("h_mc_phi_vs_theta_3hits_or_more", ";#theta [rad];#phi [rad]", 32 , 0.0, 3.2, 32, -3.2, 0.0)

h_mc_phi = ROOT.TH1D("h_mc_phi", ";#phi [rad];", 32 , -3.2, 0.0)
h_mc_theta = ROOT.TH1D("h_mc_theta", ";#theta [rad];", 32 , 0.0, 3.2)
h_mc_p = ROOT.TH1D("h_mc_p", ";p [MeV/#it{c}];", 400 , 0, 40000)
h_mc_pt = ROOT.TH1D("h_mc_pt", ";pt [MeV/#it{c}];", 500 , -40000, 10000)

# Need to fill 0 bin?
h_cosmics_per_frame = ROOT.TH1D("h_cosmics_per_frame", ";Number of cosmic muons per frame;", 40, 0, 40)
h_cosmics_per_frame_with_hits = ROOT.TH1D("h_cosmics_per_frame_with_hits", ";Number of cosmic muons per frame (with hits);", 40, 0, 40)
h_cosmics_per_frame_no_hits = ROOT.TH1D("h_cosmics_per_frame_no_hits", ";Number of cosmic muons per frame (with no hits);", 40, 0, 40)

for i in tqdm(range(skip, nevents+skip)):
    simTree.GetEntry(i)

    # print("Frame {}".format(i))
    traj_tids, _ = frame.traj.getTids()
    traj_index = frame.traj.getIndex(traj_tids)

    traj_to_pixels = defaultdict(list)

    for pixel in range(frame.pixel.getN()):
        ids = frame.pixel.getIds(pixel)
        [ntids, nhids] = getDiffIds(ids)
        tid = frame.pixel.getTid(pixel)

        if (ntids==1 and nhids==1 and tid in traj_tids):
            hit = PixelHit(frame.pixel.hit_pixelid[pixel], frame.pixel.getMcTime(pixel), ids)
            if (hit.sensorId not in holes): traj_to_pixels[tid].append(hit)

        elif (tid in traj_tids): # pileup
            print("PILEUP!")

    # print("Number of cosmics {}".format(len(traj_tids)))
    for tid in traj_tids:
        i = traj_index[tid]
    #     h_mc_phi.Fill(frame.traj.getPhiAngle(i))
    #     h_mc_theta.Fill(frame.traj.getThetaAngle(i))
        h_mc_p.Fill(frame.traj.getP(i))
    #     h_mc_pt.Fill(frame.traj.getPt(i))

    trajs_no_hits = removeIdsFromTraj(traj_to_pixels, traj_tids)

    h_cosmics_per_frame.Fill(len(traj_tids))
    h_cosmics_per_frame_with_hits.Fill(len(traj_tids)-len(trajs_no_hits))
    h_cosmics_per_frame_no_hits.Fill(len(trajs_no_hits))

    # for tid in traj_tids:
    #     i = traj_index[tid]
    #     h_mc_phi_vs_theta_all.Fill(frame.traj.getThetaAngle(i), frame.traj.getPhiAngle(i))

    # for tid, hits in traj_to_pixels.items():
    #     # print("--- TID {} ---".format(tid))
    #     hits.sort(key=lambda x: x.mcTime, reverse=False)
        
    #     layers = [hit.layer for hit in hits]
    #     theta = frame.traj.getThetaAngle(traj_index[tid])
    #     phi = frame.traj.getPhiAngle(traj_index[tid])

    #     has_4hits = (len(hits)==4) and layers == [1,0,0,1]
    #     theta_acc_to_4hits.Fill(has_4hits, theta)
    #     phi_acc_to_4hits.Fill(has_4hits, phi)

    #     has_3hits = (len(hits)==3) #and layers == [1,0,0]
    #     theta_acc_to_3hits.Fill(has_3hits, theta)
    #     phi_acc_to_3hits.Fill(has_3hits, phi)

    #     if (len(hits)>=3): h_mc_phi_vs_theta_3hits_or_more.Fill(theta, phi)

    #     # for hit in hits:
    #     #     hit.print()

    # for tid in trajs_no_hits:
    #     theta = frame.traj.getThetaAngle(traj_index[tid])
    #     phi = frame.traj.getPhiAngle(traj_index[tid])

    #     h_mc_phi_vs_theta_none.Fill(theta, phi)

    #     theta_acc_to_3hits.Fill(False, theta)
    #     phi_acc_to_3hits.Fill(False, phi)
    #     theta_acc_to_4hits.Fill(False, theta)
    #     phi_acc_to_4hits.Fill(False, phi)

    # print("No hits in these trajs (tids) {}".format(trajs_no_hits))
    # print("One hit for these tids {}".format(trajs_one_hit))
    # print("Two hit for these tids {}".format(trajs_two_hit))
    # print("Three hit for these tids {}".format(trajs_three_hit))
    # print("Four hit for these tids {}".format(trajs_four_hit))

# pid_and_type_cut = "(traj_type==3||traj_type==4)&&abs(traj_PID)==13"

# sim_df = ROOT.RDataFrame(simTree).Define("mc_phi", "atan2(traj_py, traj_px)").Define("mc_theta", "TMath::Pi()/2.0 - atan(traj_pz / hypot(traj_px, traj_py))").Define("p", "sqrt(pow(traj_px, 2.0) + pow(traj_py, 2.0) + pow(traj_pz, 2.0))").Define("pt", "p*sin(mc_phi)").Filter(pid_and_type_cut)
# h_mc_phi = sim_df.Histo1D(("h_mc_phi", ";#phi [rad];", 32 , -3.2, 0.0), "mc_phi")
# h_mc_theta = sim_df.Histo1D(("h_mc_theta", ";#theta [rad];", 32 , 0.0, 3.2), "mc_theta")
# h_mc_p = sim_df.Histo1D(("h_mc_p", ";p [MeV/#it{c}];", 400 , 0, 40000), "p")
# h_mc_pt = sim_df.Histo1D(("h_mc_pt", ";pt [MeV/#it{c}];", 500 , -40000, 10000), "pt")

# h_mc_phi.GetPtr().Write()
# h_mc_theta.GetPtr().Write()
# h_mc_p.GetPtr().Write()
# h_mc_pt.GetPtr().Write()

c1 = ROOT.TCanvas()
h_cosmics_per_frame.Draw()
c1.SaveAs("h_cosmics_per_frame.pdf")

c1.ResetDrawn()
h_cosmics_per_frame_with_hits.Draw()
c1.SaveAs("h_cosmics_per_frame_with_hits.pdf")

c1.ResetDrawn()
h_cosmics_per_frame_no_hits.Draw()
c1.SaveAs("h_cosmics_per_frame_no_hits.pdf")

output_file.Write()
for eff in eff_ls: eff.Write()
output_file.Close()