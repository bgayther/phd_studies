# Merge root files with hadd

rm ./plots/*.root

# Sync files from GRID
dirac-dms-directory-sync -j 8 /mu3e.org/user/b/benjamin.gayther/SensHists ./plots

# Merge files
hadd -j 8 -f ./plots/IC.root ./plots/IC_*.root
hadd -j 8 -f ./plots/Signal.root ./plots/Sig_*.root
hadd -j 8 -f ./plots/BhabhaMichel.root ./plots/BhabhaMichel_*.root