import ROOT
import numpy as np
import time
from collections import defaultdict
import argparse
import logging
import os
import sys
import copy
import tabulate
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from fibreClasses import FbCluster, Frame, MCHit

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input sim file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of frames to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='timing_res_analysis', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

# os.chdir(args.path)
ROOT.gROOT.SetBatch(True)

# ROOT.gStyle.SetOptStat(0)
fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n
two_point_three_five = 2 * np.sqrt(2 * np.log(2))

############## SIM RES ###############
simTree = ROOT.TChain("mu3e")
mchits = ROOT.TChain("mu3e_mchits")

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    simTree.Add(file)
    mchits.Add(file)

nevents = simTree.GetEntries()
if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.output+".root", 'RECREATE')

simTree.SetBranchStatus("*", 0)
mchits.SetBranchStatus("*", 0)

frame = Frame(simTree, mchits)

# Counters (index represents traj_type, number at this index represents number of missing tids for this type!)
nTrajs = np.zeros([300], dtype=int)
nTrajsNotFound1AND = np.zeros([300], dtype=int)
nTrajsNotFound1OR  = np.zeros([300], dtype=int)
nTrajsNotFound2AND = np.zeros([300], dtype=int)
nTrajsNotFound2OR  = np.zeros([300], dtype=int)

sipmTotal = 0
nClusters_1p = 0
nClusters_2p = 0
sipmDiffTid = 0
sipmDiffHid = 0
Nbins = 100

# Histos
hClusterTimeMean = ROOT.TH1F("hClusterTimeMean", "Time Resolution;(t_{left} + t_{right})/2 - t_{mc} [ns]", Nbins, -5, 5)
hClusterTimeDiff = ROOT.TH1F("hClusterTimeDiff", "Time Resolution;(t_{left} - t_{right}) [ns]", Nbins, -5, 5)
hClusterTimeMin = ROOT.TH1F("hClusterTimeMin", "Time Resolution;min(t_{left}, t_{right}) - t_{mc} [ns]", Nbins, -5, 5)
hClusterTimeLeft = ROOT.TH1F("hClusterTimeLeft", "Time Resolution;(t_{left} - t_{mc}) [ns]", Nbins, -5, 5)
hClusterTimeRight = ROOT.TH1F("hClusterTimeRight", "Time Resolution;(t_{right} - t_{mc}) [ns]", Nbins, -5, 5)
hClusterTimeMeanCorr = ROOT.TH1F("hClusterTimeMeanCorr", "Time Resolution;(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeDiffCorr = ROOT.TH1F("hClusterTimeDiffCorr", "Time Resolution;(t_{left} - t_{right}) (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeMinCorr = ROOT.TH1F("hClusterTimeMinCorr", "Time Resolution;min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeLeftCorr = ROOT.TH1F("hClusterTimeLeftCorr", "Time Resolution;(t_{left} - t_{mc}) (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeRightCorr = ROOT.TH1F("hClusterTimeRightCorr", "Time Resolution;(t_{right} - t_{mc}) (z corrected) [ns]", Nbins, -5, 5)

hClusterTimeMeanNoPU = ROOT.TH1F("hClusterTimeMeanNoPU", "Time Resolution (no pileup);(t_{left} + t_{right})/2 - t_{mc} [ns]", Nbins, -5, 5)
hClusterTimeDiffNoPU = ROOT.TH1F("hClusterTimeDiffNoPU", "Time Resolution (no pileup);(t_{left} - t_{right}) [ns]", Nbins, -5, 5)
hClusterTimeMinNoPU = ROOT.TH1F("hClusterTimeMinNoPU", "Time Resolution;min(t_{left}, t_{right}) - t_{mc} [ns]", Nbins, -5, 5)
hClusterTimeMeanCorrNoPU = ROOT.TH1F("hClusterTimeMeanCorrNoPU", "Time Resolution;(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeDiffCorrNoPU = ROOT.TH1F("hClusterTimeDiffCorrNoPU", "Time Resolution (no pileup);(t_{left} - t_{right}) (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeMinCorrNoPU = ROOT.TH1F("hClusterTimeMinCorrNoPU", "Time Resolution;min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeLeftCorrNoPU = ROOT.TH1F("hClusterTimeLeftCorrNoPU", "Time Resolution;(t_{left} - t_{mc}) (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeRightCorrNoPU = ROOT.TH1F("hClusterTimeRightCorrNoPU", "Time Resolution;(t_{right} - t_{mc}) (z corrected) [ns]", Nbins, -5, 5)
hClusterTimeLeftNoPU = ROOT.TH1F("hClusterTimeLeftNoPU", "Time Resolution;(t_{left} - t_{mc}) [ns]", Nbins, -5, 5)
hClusterTimeRightNoPU = ROOT.TH1F("hClusterTimeRightNoPU", "Time Resolution;(t_{right} - t_{mc}) [ns]", Nbins, -5, 5)

hTimeProds = ROOT.TH1F("hTimeProds",";time produced [ns]", 200, -5, 5)
hPathlengths = ROOT.TH1F("hPathlengths",";photon pathlengths [mm]", 200, -5, 350)
hPathlengthZdiff = ROOT.TH1F("hPathlengthZdiff",";Difference between photon pathlength and z pos [mm]", 200, -200, 550)
hPathlengthZTimeDiff = ROOT.TH1F("hPathlengthZTimeDiff",";Time difference between photon pathlength and z pos [ns]", 200, -0.5, 0.5)
hPathlengthVsZTimeDiff = ROOT.TH2F("hPathlengthVsZTimeDiff","; Z pos [mm];Time difference between photon pathlength and z pos [ns]", 200, -150, 150, 200, -0.5, 0.5)
hPathlengthVsZTimeDiffL = ROOT.TH2F("hPathlengthVsZTimeDiffL","; Z pos [mm];Time difference between photon pathlength and z pos L [ns]", 200, -150, 150, 200, -0.5, 0.5)
hPathlengthVsZTimeDiffR = ROOT.TH2F("hPathlengthVsZTimeDiffR","; Z pos [mm];Time difference between photon pathlength and z pos R [ns]", 200, -150, 150, 200, -0.5, 0.5)

hClusterTimeDiffVsZ = ROOT.TH2F("hClusterTimeDiffVsZ","; Z pos [mm];(t_{left} - t_{right}) [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeMeanVsZ = ROOT.TH2F("hClusterTimeMeanVsZ","; Z pos [mm];(t_{left} + t_{right})/2 - t_{mc} [ns]", 200, -150, 150, Nbins, -5, 5)

hClusterTimeDiffCorrVsZ = ROOT.TH2F("hClusterTimeDiffCorrVsZ","; Z pos [mm];(t_{left} - t_{right}) (z corrected) [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeMeanCorrVsZ = ROOT.TH2F("hClusterTimeMeanCorrVsZ","; Z pos [mm];(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]", 200, -150, 150, Nbins, -5, 5)

hClusterTimeMinVsZ = ROOT.TH2F("hClusterTimeMinVsZ","; Z pos [mm];min(t_{left}, t_{right}) - t_{mc} [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeMinCorrVsZ = ROOT.TH2F("hClusterTimeMinCorrVsZ","; Z pos [mm];min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeMinCorrVsNcols = ROOT.TH2F("hClusterTimeMinCorrVsNcols","; Total cols (L+R);min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", 30, 0, 30, Nbins, -5, 5)

hClusterTimeLeftVsZ = ROOT.TH2F("hClusterTimeLeftVsZ","Cluster time left vs Z; Z pos [mm];t_{left} - t_{mc} [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeRightVsZ = ROOT.TH2F("hClusterTimeRightVsZ","Cluster time right vs Z; Z pos [mm];t_{left} - t_{mc} [ns]", 200, -150, 150, Nbins, -5, 5)

hClusterTimeLeftCorrVsZ = ROOT.TH2F("hClusterTimeLeftCorrVsZ","Cluster time left vs Z; Z pos [mm];t_{left} - t_{mc} (z corrected) [ns]", 200, -150, 150, Nbins, -5, 5)
hClusterTimeRightCorrVsZ = ROOT.TH2F("hClusterTimeRightCorrVsZ","Cluster time right vs Z; Z pos [mm];t_{left} - t_{mc} (z corrected) [ns]", 200, -150, 150, Nbins, -5, 5)

hClusterTimeMinAvgDiffL = ROOT.TH1F("hClusterTimeMinAvgDiffL", ";(t_{min,L} - t_{avg,L}) [ns]", 200, -1, 15)
hClusterTimeMinAvgDiffLVsNcolL = ROOT.TH2F("hClusterTimeMinAvgDiffLVsNcolL","; NcolL;t_{min,L} - t_{avg,R} [ns]", 30, 0, 30, 200, -1, 15) 

hClusterTimeMinAvgDiffR = ROOT.TH1F("hClusterTimeMinAvgDiffR", ";(t_{min,R} - t_{avg,R}) [ns]", 200, -1, 15)
hClusterTimeMinAvgDiffRVsNcolR = ROOT.TH2F("hClusterTimeMinAvgDiffRVsNcolR","; Z NcolR;t_{min,R} - t_{avg,R} [ns]", 30, 0, 30, 200, -1, 15)

h_cols_vs_p_rt = ROOT.TH2D("h_cols_vs_p_rt", ";r [mm];MPPC Column", 60, 0, 60, 128, 0, 128)
h_cols_vs_phi = ROOT.TH2D("h_cols_vs_phi", ";phi [rad];MPPC Column", 35, 0, 3.5, 128, 0, 128)
h_cols_vs_theta = ROOT.TH2D("h_cols_vs_theta", ";theta [rad];MPPC Column", 35, 0, 3.5, 128, 0, 128)

### Helper funcs ###

# Fit two summed gaussians (doesn't seem to handle cases not centred on zero?)
def fitGaus(h):
    gaus = ROOT.TF1("gaus","gaus")
    gaus2 = ROOT.TF1("gaus2", "[0]*exp(-(x-[1])^2/(2*[2]^2)) + [3]*exp(-(x-[1])^2/(2*[4]^2))")
    gaus2.SetParameter(0, hClusterTimeDiff.GetMaximum() * 0.8)
    gaus2.SetParameter(1, 0.)
    gaus2.SetParameter(2, 0.5)
    gaus2.SetParameter(4, 2.)
    gaus2.SetParameter(3, hClusterTimeDiff.GetMaximum() * 0.2)
    h.Fit(gaus, "Q")
    h.Fit(gaus2, "Q")
    tRes = gaus.GetParameter("Sigma")
    tRes2_0 = gaus2.GetParameter(2)
    tRes2_1 = gaus2.GetParameter(4)
    # returns
    # Std dev of one gauss over whole range
    # Std dev of combined gauss, CORE
    # Std dev of combined gauss, WINGS
    return [tRes, tRes2_0, tRes2_1]

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

# Finds MISSING track_ids / types in cluster (compared to trajs present in frame)
def removeClusterIdsFromTraj(clusters, trajs):
    [tids, types] = trajs
    [tidsAND, typesAND] = trajs
    [tidsOR, typesOR] = trajs
    for cl in clusters:
        for tid in cl.ids:
            if tid in tids:
                if (cl.ampL > 0) and (cl.ampR > 0): # AND
                    s = np.where(tidsAND == tid)
                    typesAND = np.delete(typesAND, s)
                    tidsAND  = np.delete(tidsAND,  s)
                if (cl.ampL > 0) or (cl.ampL > 0): # OR
                    s = np.where(tidsOR == tid)
                    typesOR = np.delete(typesOR, s)
                    tidsOR  = np.delete(tidsOR,  s)
    return [tidsAND, typesAND, tidsOR, typesOR]

# Counts each type (index corresponds to type, i.e. 11 postitron from Michel decay)
def countRemainingTrajs(types, counter):
    for ptype in types:
        counter[ptype] += 1

def getAndCountTrajs(frame, counter, lab=False):
    tids, types = frame.traj.getTids(lab=lab)
    countRemainingTrajs(types, counter)
    return [tids, types]

# Max number of different tids, hids
def getDiffIds(ids, verbose = 0):
    ntids = len(ids)
    if ntids == 1 and 0 in ids:
        return [-1, -1]
    nhids = 0
    tid_maxhids = 0
    for tid, hid in ids.items():
        nhids_ = len(hid)
        if nhids_ > nhids:
            nhids = nhids_
            tid_maxhids = tid
    return [ntids, nhids]

def get_cmap(n, name='tab20'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, n)

#### Analysis loop ####
threshold = 0
skip = args.skip
for i in range(skip, nevents+skip):
    simTree.GetEntry(i)
    if i%100 == 0:
        print ("(%6i/%i) %5.1f%% \r" % (i, nevents, 100.*i/nevents))
    traj_tids, traj_types = getAndCountTrajs(frame, nTrajs)
    # print "\n\n"
    # print traj_tids

    clusters = []
    # Loop through all sipm columns
    for sipmno in range(frame.sipm.getN()):
        if frame.sipm.amplitude[sipmno] <= 0:continue
        sipmTotal += 1

        ids = frame.sipm.getIds(sipmno) # [tid][hids]
        [ntids, nhids] = getDiffIds(ids)

        cluster = FbCluster(frame.sipm, sipmno, ids, 0) # Just cluster of size 1 (one hit)
        # print "DEBUG ", frame.sipm.sipm[sipmno], frame.sipm.sipm[sipmno]%2, frame.sipm.col[sipmno], frame.sipm.amplitude[sipmno], frame.sipm.getIds(sipmno), frame.sipm.time[sipmno], cluster.time_mc, frame.sipm.getZPos(sipmno), frame.sipm.crosstalk[sipmno]

        if (frame.sipm.time_prod[sipmno] != 0.0): 
            hTimeProds.Fill(frame.sipm.time_prod[sipmno] - frame.sipm.getMcTime(sipmno))
        if (frame.sipm.pathlength[sipmno] != 0.0): 
            hPathlengths.Fill(frame.sipm.pathlength[sipmno])
            zPos = frame.sipm.getZPos(sipmno)
            hPathlengthZdiff.Fill(frame.sipm.pathlength[sipmno] - zPos)
            side = frame.sipm.sipm[sipmno]%2
            # d_side = fb_len_half - pow(-1, side) * zPos # double check calc
            if side==0:
                d_side = fb_len_half - zPos
            else:
                d_side = fb_len_half + zPos
            t_diff = frame.sipm.pathlength[sipmno]/cn - d_side/cn
            hPathlengthZTimeDiff.Fill(t_diff)
            hPathlengthVsZTimeDiff.Fill(zPos, t_diff)

            # Make clean version (shouldn't be any <0 times, plus rand noise) (Move to cluster loop)
            if (side == 0):
                hPathlengthVsZTimeDiffL.Fill(zPos, t_diff)
            else:
                hPathlengthVsZTimeDiffR.Fill(zPos, t_diff)

        # check if any of the tid/hid pair is already present in a cluster
        # Merges hits into existing clusters or creates new ones....
        merged = 0
        # photon threshold level
        if(frame.sipm.amplitude[sipmno] > threshold):
            for tid, hids in ids.items():
                for hid in hids:
                    for cl in clusters:
                        # Should this just use track id? Or hit id as well?
                        # Or could change it to more closely resemble reconstruction
                        if tid in cl.ids and hid in cl.ids[tid] and cl.side == cluster.side and cl.ribbon == cluster.ribbon:
                            cl.add(cluster) # add hit to cluster
                            merged = 1
                            break
            if not merged:
                # Additional code - only use tids from selected traj_ids
                for tid, hids in cluster.ids.items():
                    if tid in traj_tids:
                        clusters.append(copy.deepcopy(cluster)) # create new cluster
                        nClusters_1p += 1

    # print "New frame, traj_tids ", traj_tids

    # fig = plt.figure()
    # # ax = plt.axes(projection='3d')
    # ax = plt.axes()
    # cmap = get_cmap(len(clusters))

    # sides = [0,1]

    # for i, cl in enumerate(clusters):
    #     print cl.ids, cl.ribbon, cl.side, cl.Nhits, cl.timeL, cl.timeR, cl.time_mc, cl.z, cl.times
    #     for side in sides:
    #         if cl.side == side and cl.ribbon == 5 and cl.ids.keys()[0] == 12418:
    #             for idx, time in enumerate(cl.times):
    #                 # adjusted_time = time / (float(cl.ampls[idx] / cl.nfbs[idx]))**2.0
    #                 # print time - frame.frameLength*frame.header.event, adjusted_time- frame.frameLength*frame.header.event, cl.time_mc- frame.frameLength*frame.header.event
    #                 ax.scatter(time - frame.frameLength*frame.header.event, cl.side, c=cmap(i), label='hid ' + str(cl.ids[cl.ids.keys()[0]]) + ', side ' + str(cl.side))
    #                 # ax.scatter3D(time - frame.frameLength*frame.header.event, cl.nfbs[idx], cl.cols[idx], c=cmap(i), label='tid ' + str(cl.ids.keys()[0]) + ', hid ' + str(cl.ids[cl.ids.keys()[0]]) + ', side ' + str(cl.side))
    #                 # ax.scatter3D(time - frame.frameLength*frame.header.event, cl.phi, cl.z, c=cmap(i), label='tid ' + str(cl.ids.keys()[0]) + ', hid ' + str(cl.ids[cl.ids.keys()[0]]) + ', side ' + str(cl.side))
    
    # handles, labels = plt.gca().get_legend_handles_labels()
    # labels, ids = np.unique(labels, return_index=True)
    # handles = [handles[i] for i in ids]
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # # Put a legend to the right of the current axis
    # ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))    

    # # ax.set_xlim(0, frame.frameLength)
    # ax.set_xlabel("Hit time [ns]")
    # # ax.set_ylim(0, 12)
    # ax.set_ylabel("Side")
    # # ax.set_ylim(-np.pi, +np.pi)
    # # ax.set_zlim(-150, +150)

    # ax.xaxis.set_major_locator(MultipleLocator(1))
    # ax.xaxis.set_minor_locator(AutoMinorLocator(0.2))
    # ax.grid(which='major', axis='x', color='#CCCCCC', linestyle='--')
    # ax.grid(which='minor', axis='x', color='#CCCCCC', linestyle=':')
    # plt.title('Multiple recurler track with hits on one ribbon')

    # fig = plt.gcf()
    # fig.set_size_inches(18.5, 10.5)
    # plt.show()



    # for cl in clusters:
    #     if cl.ribbon == 0:
    #         for col in cl.cols:
    #             h_cols_vs_p_rt.Fill(cl.p_rt, col)
    #             h_cols_vs_phi.Fill(cl.phi,col)
    #             h_cols_vs_theta.Fill(cl.p_theta,col)
        # if not cl.bothSides(): continue
        # # print cl.ids
        # zPos = cl.z
        # # print cl.ids, cl.timeL, cl.timeR, cl.time_mc, zPos, cl.Nhits
        # ntids, nhids = cl.getPileup()

        # # cl.unweightedMeanTimes()
        # # cl.weightedAmplTimes()
        # # cl.maxAmplTimes()

        # tMC   = cl.getTimeMc()
        # tMean = cl.getTimeMean()
        # tMeanCorr = cl.getTimeMeanCorrected()
        # tDiff = cl.getTimeDiff()
        # tDiffCorr = cl.getTimeDiffCorrected()
        # tMin  = cl.getTimeMinAndMc()
        # tMinCorr = cl.getTimeMinMcCorrected()

        # hClusterTimeMean.Fill(tMean)
        # hClusterTimeMeanVsZ.Fill(zPos, tMean)
        # hClusterTimeMeanCorr.Fill(tMeanCorr)
        # hClusterTimeMeanCorrVsZ.Fill(zPos, tMeanCorr)

        # hClusterTimeDiff.Fill(tDiff)
        # hClusterTimeDiffVsZ.Fill(zPos, tDiff)
        # hClusterTimeDiffCorr.Fill(tDiffCorr)
        # hClusterTimeDiffCorrVsZ.Fill(zPos, tDiffCorr)

        # hClusterTimeMin.Fill(tMin)
        # hClusterTimeMinVsZ.Fill(zPos, tMin)
        # hClusterTimeMinCorr.Fill(tMinCorr)
        # hClusterTimeMinCorrVsZ.Fill(zPos, tMinCorr)
        # hClusterTimeMinCorrVsNcols.Fill(cl.NcolL+cl.NcolR, tMinCorr)

        # hClusterTimeLeft.Fill(cl.timeL - tMC)
        # hClusterTimeRight.Fill(cl.timeR - tMC)

        # hClusterTimeLeftVsZ.Fill(zPos, cl.timeL - tMC)
        # hClusterTimeRightVsZ.Fill(zPos, cl.timeR - tMC)

        # [tL, tR] = cl.getCorrectedTimes()

        # hClusterTimeLeftCorr.Fill(tL - tMC)
        # hClusterTimeRightCorr.Fill(tR - tMC)
        # hClusterTimeLeftCorrVsZ.Fill(zPos, tL - tMC)
        # hClusterTimeRightCorrVsZ.Fill(zPos, tR - tMC)
    
        # # [meanTimeL, meanTimeR] = cl.getMeanTimes()
        # # print (cl.timesL), (cl.timesR), cl.timeSumL, cl.timeSumR
        # # print cl.timeL, cl.timeR, cl.getMeanTimes(), cl.NcolL, cl.NcolR,
        # # print "\n"
        # # hClusterTimeMinAvgDiffL.Fill(meanTimeL-cl.timeL)
        # # hClusterTimeMinAvgDiffLVsNcolL.Fill(cl.NcolL, meanTimeL-cl.timeL)
        # # hClusterTimeMinAvgDiffR.Fill(meanTimeR-cl.timeR)
        # # hClusterTimeMinAvgDiffRVsNcolR.Fill(cl.NcolR, meanTimeR-cl.timeR)

        # # no pile up 
        # if (ntids == 1) or (nhids == 1):
        #     hClusterTimeMeanNoPU.Fill(tMean)
        #     hClusterTimeMeanCorrNoPU.Fill(tMeanCorr)
        #     hClusterTimeDiffNoPU.Fill(tDiff)
        #     hClusterTimeMinNoPU.Fill(tMin)
        #     hClusterTimeLeftNoPU.Fill(cl.timeL - tMC)
        #     hClusterTimeRightNoPU.Fill(cl.timeR - tMC)
        #     hClusterTimeDiffCorrNoPU.Fill(tDiffCorr)
        #     hClusterTimeMinCorrNoPU.Fill(tMinCorr)
        #     hClusterTimeLeftCorrNoPU.Fill(tL - tMC)
        #     hClusterTimeRightCorrNoPU.Fill(tR - tMC)


print ("Time Resolution (1 photon threshold): ")
results = []
results.append(["t_left - t_right", (getFWHM(hClusterTimeDiff, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeDiffNoPU, 0.5)/two_point_three_five * 1e3)])
results.append(["t_left - t_right (z corr)", (getFWHM(hClusterTimeDiffCorr, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeDiffCorrNoPU, 0.5)/two_point_three_five * 1e3)])

results.append(["t_left - tMC", (getFWHM(hClusterTimeLeft, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeLeftNoPU, 0.5)/two_point_three_five * 1e3)])
results.append(["t_left - tMC", (getFWHM(hClusterTimeRight, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeRightNoPU, 0.5)/two_point_three_five * 1e3)])

results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeLeftCorr, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeLeftCorrNoPU, 0.5)/two_point_three_five * 1e3)])
results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeRightCorr, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeRightCorrNoPU, 0.5)/two_point_three_five * 1e3)])

results.append(["(t_left + t_right)/2 - tMC", (getFWHM(hClusterTimeMean, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeMeanNoPU, 0.5)/two_point_three_five * 1e3)])
results.append(["(t_left + t_right)/2 - tMC (z corr)", (getFWHM(hClusterTimeMeanCorr, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeMeanCorrNoPU, 0.5)/two_point_three_five * 1e3)])

results.append(["min (t_left, t_right) - tMC", (getFWHM(hClusterTimeMin, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeMinNoPU, 0.5)/two_point_three_five * 1e3)])
results.append(["min (t_left, t_right) - tMC (z corr)", (getFWHM(hClusterTimeMinCorr, 0.5)/two_point_three_five * 1e3), (getFWHM(hClusterTimeMinCorrNoPU, 0.5)/two_point_three_five * 1e3)])
print(tabulate.tabulate(results, ["Function","FWHM/two_point_three_five [ps]", "FWHM/two_point_three_five (No PU) [ps]"], tablefmt='latex_raw',floatfmt="4.0f"))

results.append(["t_left - t_right (z corr)", (getFWHM(hClusterTimeDiffCorr, 0.5)/two_point_three_five * 1e3)])
results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeLeftCorr, 0.5)/two_point_three_five * 1e3)])
results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeRightCorr, 0.5)/two_point_three_five * 1e3)])
results.append(["(t_left + t_right)/2 - tMC (z corr)", (getFWHM(hClusterTimeMeanCorr, 0.5)/two_point_three_five * 1e3)])
results.append(["min (t_left, t_right) - tMC (z corr)", (getFWHM(hClusterTimeMinCorr, 0.5)/two_point_three_five * 1e3)])
print(tabulate.tabulate(results, ["Function","FWHM/two_point_three_five [ps]"], tablefmt='latex_raw',floatfmt="4.0f"))




input("Press Enter to continue...")
print ("Finished...")

output_file.Write()
output_file.Close()