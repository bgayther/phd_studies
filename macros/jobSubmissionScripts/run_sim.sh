
source /unix/muons/users/bgayther/setup.sh
source /unix/muons/users/bgayther/mu3e/build/mu3e-activate.sh

mkdir -p /tmp/job_$PBS_JOBID
cd /tmp/job_$PBS_JOBID
mkdir data
mkdir results

case $SAMPLE in
    0)
	echo "generating signal"
	OUTDIR=/unix/muons/users/bgayther/SignalDecays/batch/sim_signal_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	cp /unix/muons/users/bgayther/phd_studies/macros/jobSubmissionScripts/sim_signal.json sim.json
	;;
    1)
	echo "generating internal conversion"
	OUTDIR=/unix/muons/users/bgayther/IC/batch/sim_IC_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	cp /unix/muons/users/bgayther/phd_studies/macros/jobSubmissionScripts/sim_IC.json sim.json
	;;
    2)
	echo "generating positron beam (Mott/Bhabha)"
	OUTDIR=/unix/muons/users/bgayther/MottScattering/batch/sim_positron_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	cp /unix/muons/users/bgayther/phd_studies/macros/jobSubmissionScripts/sim_positron.json sim.json
	;;
    3)
	echo "generating electron beam (Mott)"
	OUTDIR=/unix/muons/users/bgayther/MottScattering/batch/sim_electron_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	cp /unix/muons/users/bgayther/phd_studies/macros/jobSubmissionScripts/sim_electron.json sim.json
	;;
    4)
	echo "generating michel decays"
	# OUTDIR=/unix/muons/users/bgayther/MichelDecays/batch/sim_michel_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	OUTDIR=/unix/muons/users/bgayther/TimingStudyData/batch/sim_${RUN}_${EVENTS}_${TAG}/job_$PBS_JOBID
	cp /unix/muons/users/bgayther/phd_studies/macros/jobSubmissionScripts/sim_michel.json sim.json
	;;
    *)
	echo "Don't know what to generate! Jobs will fail."
	;;
esac

mkdir -p $OUTDIR

# cp /unix/muons/users/bgayther/mu3e/install/lib/libwatson.so .

echo "{
    \".include\" : \"sim.json\"
    $SIM_CONF_OPT 
     }" > sim_$TAG.json

echo "/mu3e/run/setRunNumber $RUN" > pipeline.mac
echo "/run/beamOn $EVENTS" >> pipeline.mac

echo "******************** GENERATING RUN ${RUN}, NEVENTS = ${EVENTS}, SAMPLE ${SAMPLE}, SIM CONF ${SIM_CONF_OPT}, REC CONF ${REC_CONF_OPT} ********************"
start=$(date +%s.%N)

echo "==================== RUNNING SIM ===================="
mu3eSim pipeline.mac --conf sim_$TAG.json --output data/mu3e_run_${RUN}_${TAG}.root

echo "==================== RUNNING SORT ==================="
mu3eSort data/mu3e_run_${RUN}_${TAG}.root  data/mu3e_sorted_${RUN}_${TAG}.root 
rm data/mu3e_run_${RUN}_${TAG}.root

# echo "================= RUNNING NORM (IF NEEDED) ==============="
# case $SAMPLE in
#     0)
# 	echo "norm for signal"
# 	mu3eAnaSignalnorm data/mu3e_sorted_${RUN}_${TAG}.root > mu3e_sorted_${RUN}_${TAG}_signalnorm.txt
# 	;;
#     1)
# 	echo "norm for internal conversion"
# 	mu3eAnaICnorm data/mu3e_sorted_${RUN}_${TAG}.root > mu3e_sorted_${RUN}_${TAG}_ICnorm.txt
# 	;;
#     *)
# 	echo "No norm needed"
# 	;;
# esac

# cp /unix/muons/users/bgayther/mu3e/run/trirec.conf ./
# echo $REC_CONF_OPT >> trirec.conf

# echo "================= RUNNING TRIREC ==============="
# mu3eTrirec --conf trirec.conf --input data/mu3e_sorted_${RUN}_${TAG}.root --output results/mu3e_trirec_${RUN}_${TAG}.root 
# rm data/mu3e_sorted_${RUN}_${TAG}.root

# cp /unix/muons/users/bgayther/mu3e/run/vertex.conf .
# echo $VERTEX_CONF_OPT >> vertex.conf

# echo "================= RUNNING VERTEX ==============="
# mu3eAnaVertexfit results/mu3e_trirec_${RUN}_${TAG}.root results/mu3e_vertex_${RUN}_${TAG}.root  

end=$(date +%s.%N)
runtime=$(python -c "print(${end} - ${start})")

echo "Runtime was $runtime"

cp -r * $OUTDIR

rm -rf /tmp/job_$PBS_JOBID
