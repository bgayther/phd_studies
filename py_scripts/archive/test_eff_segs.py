from ROOT import *
import numpy as np
import itertools

# Looking at signal events
DataTree = TChain("segs")
DataTree.Add("run000042-reco.root")

DataTree_mc = TChain("segs_mc")
DataTree_mc.Add("run000042-reco.root")

can = TCanvas("can", "can", 900, 700)
output_file = TFile('mc_defs.root', 'RECREATE')
h_mc_phi = TH1F('h_mc_phi','h_mc_phi',10,-3.2,3.2)
h_mc_theta = TH1F('h_mc_theta','h_mc_theta',10,0,2)
# https://www.physi.uni-heidelberg.de/Forschung/he/mu3e/wiki/index.php/ROOT_File_Contents
# Contains 1 entry (row) per segment (track). The frame (to which the segment belongs) can be identified by eventId. 
segs_n, n3, n4, n6, n8 = 0.0, 0.0, 0.0, 0.0, 0.0
fake_n4, fake_n6, fake_n8 = 0.0, 0.0, 0.0
segs_n_mc, n3_mc, n4_mc, n6_mc, n8_mc = 0.0, 0.0, 0.0, 0.0, 0.0

for entry in xrange(DataTree.GetEntries()):
    segs_n += 1
    DataTree.GetEntry(entry)  

    mc = DataTree.mc
    mc_prime = DataTree.mc_prime
    n = DataTree.n # N_triplets, n + 2 = n_hits
    chi2 = DataTree.chi2
    mc_type = DataTree.mc_type 
    mc_phi = DataTree.mc_phi
    mc_theta = DataTree.mc_theta

    # See if matched to MC track (and is of correct type)
    if mc_prime and bool(mc_type == 91 or mc_type == 92):
        if n == 2:
            n4 += 1
        if n == 4:
            n6 += 1
        if n == 6:
            n8 += 1 

    # Find fakes here (i.e. not matched to MC track)
    # If this was purely misreconstructed would you expect any MC information? -> See Ioannis email
    if not mc: # and bool(mc_type == 91 or mc_type == 92)Could use any mc_*
        h_mc_phi.Fill(mc_phi)
        h_mc_theta.Fill(mc_theta)
        if n == 2:
            fake_n4 += 1
        if n == 4:
            fake_n6 += 1
        if n == 6:
            fake_n8 += 1

for entry in xrange(DataTree_mc.GetEntries()):
    segs_n_mc += 1
    DataTree_mc.GetEntry(entry)   

    mc = DataTree_mc.mc
    mc_prime = DataTree_mc.mc_prime
    n = DataTree_mc.n
    chi2 = DataTree_mc.chi2
    mc_type = DataTree_mc.mc_type 

    if mc_prime and bool(mc_type == 91 or mc_type == 92):
        # Default chi2 values used 32 and 48.....
        if n == 2 :
            n4_mc += 1
        if n == 4 :
            n6_mc += 1
        if n == 6 :
            n8_mc += 1


h_mc_phi.Write()
h_mc_theta.Write()
output_file.Close()

print "\nn4 = {}, n6 = {}, n8 = {}".format(n4, n6, n8)
print "fake_n4 = {}, fake_n6 = {}, fake_n8 = {}".format(fake_n4, fake_n6, fake_n8)
print "n4_mc = {}, n6_mc = {}, n8_mc = {}\n".format(n4_mc, n6_mc, n8_mc)

# Eff = N_reconstructed (matched to MC track) / N_MC
# Fake rate = N_reco (not matched to MC track) / N_MC
print "S4 track efficiency: ", 100 * (n4 / n4_mc)**3.0, "%"
print "S4 track fake rate: ", 100 * (fake_n4 / n4_mc)**3.0, "%\n"

print "S6 track efficiency: ", 100 * (n6 / n6_mc)**3.0, "%"
print "S6 track fake rate: ", 100 * (fake_n6 / n6_mc)**3.0, "%\n"

print "S8 track efficiency: ", 100 * (n8 / n8_mc)**3.0, "%"
print "S8 track fake rate: ", 100 * (fake_n8 / n8_mc)**3.0, "%\n"


