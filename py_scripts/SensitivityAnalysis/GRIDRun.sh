#!/bin/bash
# Setup LCG
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc11-opt
echo "Setup LCG"

echo "Files in current directory:"
ls -l

echo $seeds

vertex_files="./run*-ana_vertexfit.root"
gen_files="./signalnorm_*.txt"

# If mode is BhabhaMichel then vertex_files are ./skim_SigAna_*.root
# and gen files are ./skim_SigAna_*.txt
if [ "$mode" == "BhabhaMichel" ]; then
    vertex_files="./skim_SigAna_*.root"
    # gen_files="./skim_SigAna_*.txt"
fi

# Check if mode equal to "IC", "Bhabha" or "Sig"
if [ "$mode" == "IC" ]; then
    echo "Running IC"
    python SensitivityHistos.py -vertexFilesIC $vertex_files -output ${outputFileName} -ncpu ${ncpu}
elif [ "$mode" == "BhabhaMichel" ]; then
    echo "Running BhabhaMichel"
    python SensitivityHistos.py -vertexFilesBhabha $vertex_files -output ${outputFileName} -ncpu ${ncpu}
elif [ "$mode" == "Sig" ]; then
    echo "Running Signal"
    python SensitivityHistos.py -vertexFilesSig $vertex_files -genFilesSig $gen_files -output ${outputFileName} -ncpu ${ncpu}
else
    echo "Error: mode not recognised"
fi

echo "Finished running SensitivityHistos.py, files in current directory:"
ls -l
