import numpy as np
import math
import sys
from collections import defaultdict
import ROOT

n_events = 10000

f = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/run/scripts/fibres/stat.root")
f2 = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/inputForToyModel.root")
output_file = ROOT.TFile("toyModel.root", 'RECREATE')

ROOT.gRandom.SetSeed(0)
np.random.seed(0)

# Import edep  
hEdep = f.Get("hEdep")
# Import z positions of mppc hits
hZpositions = f.Get("hZpositions")
# Import number of fibres hit per particle
hFHits = f.Get("hFHits")
# Import numb of mppc cols per side
hHits = f.Get("hHits")

hPathlengthZresL = f2.Get("hPathlengthVsZTimeDiffL")
hPathlengthZresR = f2.Get("hPathlengthVsZTimeDiffR")

hClusterTimeMean = ROOT.TH1F("hClusterTimeMean", "Time Resolution cls_1p;(t_{left} + t_{right})/2 - t_{mc} [ns]", 2000, -5, 5)
hClusterTimeDiff = ROOT.TH1F("hClusterTimeDiff", "Time Resolution cls_1p;(t_{left} - t_{right}) [ns]", 2000, -5, 5)
hClusterTimeMin = ROOT.TH1F("hClusterTimeMin", "Time Resolution cls_1p;min(t_{left}, t_{right}) - t_{mc} [ns]", 2000, -5, 5)
hClusterTimeLeft = ROOT.TH1F("hClusterTimeLeft", "Time Resolution cls_1p;(t_{left} - t_{mc}) [ns]", 2000, -5, 5)
hClusterTimeRight = ROOT.TH1F("hClusterTimeRight", "Time Resolution cls_1p;(t_{right} - t_{mc}) [ns]", 2000, -5, 5)
hClusterTimeLeftCorr = ROOT.TH1F("hClusterTimeLeftCorr", "Time Resolution cls_1p;(t_{left} - t_{mc}) (z corrected) [ns]", 2000, -5, 5)
hClusterTimeRightCorr = ROOT.TH1F("hClusterTimeRightCorr", "Time Resolution cls_1p;(t_{right} - t_{mc}) (z corrected) [ns]", 2000, -5, 5)
hClusterTimeMeanCorr = ROOT.TH1F("hClusterTimeMeanCorr", "Time Resolution cls_1p;(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]", 2000, -5, 5)
hClusterTimeDiffCorr = ROOT.TH1F("hClusterTimeDiffCorr", "Time Resolution cls_1p;(t_{left} - t_{right}) (z corrected) [ns]", 2000, -5, 5)
hClusterTimeMinCorr = ROOT.TH1F("hClusterTimeMinCorr", "Time Resolution cls_1p;min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", 2000, -5, 5)
hMinHitTimeDist = ROOT.TH1F("hMinHitTimeDist", ";MIN value generated from exp+pl dist [ns]", 1000, -5, 5)
hMinHitAndDelayTimeDist = ROOT.TH1F("hMinHitAndDelayTimeDist", ";MIN value generated from exp+pl dist + DAQ delay[ns]", 1000, -5, 5)
hMinHitAndDelayTimeDistZCorr = ROOT.TH1F("hMinHitAndDelayTimeDistZCorr", ";MIN value generated from exp+pl dist + DAQ delay (Z corrected)[ns]", 1000, -5, 5)
hMinHitTimeDistZCorr = ROOT.TH1F("hMinHitTimeDistZCorr", ";MIN value generated from exp+pl dist (Z Corrected) [ns]", 1000, -5, 5)
hPathlengths = ROOT.TH1F("hPathlengths",";pathlength [mm]", 200, -10, 350)
# Plot pathlength vs z pos...
hTimeProds = ROOT.TH1F("hTimeProds",";time produced [ns]", 200, -5, 5)
hZpos = ROOT.TH1F("hZpos",";z positions [mm]", 200, -150, 150)

Y = 8000
fb_len = 287.75
mu_daq = 1.0
sigma_daq_jitter = 0.300
mppcPileup = 40.0
decayTime = 2.8
plDecrease = 0.84
plUpper = 1.13
plLower = 1.0
n_fb = 1.59
c_light_mm_ns = 299.792
sptr = 0.085
pde = 0.4
capture = 0.054
frameLength = 64.0
attenuationLength = 1395
npt_thr = 0
cn = c_light_mm_ns / n_fb

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

def getNDetPhotons(nScintPhotons, distance):
    epsilon = pde * capture
    attenuation = np.exp( -distance / attenuationLength)
    return np.random.poisson(nScintPhotons * epsilon * attenuation)

def tShiftAnalytical(distance, nPhotons):
    decayTimes = [0.0]*nPhotons
    pathLengths = [0.0]*nPhotons
    arrivalTimes = [0.0]*nPhotons

    for i in range(nPhotons):
        prodTime = np.random.exponential(decayTime)
        decayTimes[i] = prodTime

        pathLength = 0.0
        while True:
            pathLength = np.random.uniform()
            if not np.random.uniform() < (1.0 - pathLength*plDecrease):
                break
        
        pathLength = (plLower + (pathLength * (plUpper-plLower)))
        pathLength = pathLength * distance

        pathLengths[i] = pathLength

        arrivalTime = prodTime + ((n_fb * pathLength)/c_light_mm_ns)
        arrivalTimes[i] = np.random.normal(arrivalTime, sptr)

    return decayTimes, pathLengths, arrivalTimes

# def correctTime(currHit):
#     dCorr = fb_len/2.0 - pow(-1, currHit.side) * currHit.z 
    # dCorr = 0.0
    # if (currHit.side == -1): dCorr = fb_len_half - currHit.z 
    # elif (currHit.side == +1): dCorr = fb_len_half + currHit.z 
#     tCorr = currHit.time - dCorr / cn
#     return tCorr

def correctTimes(timeL, timeR, zPos):
    dL = fb_len/2.0 - zPos
    dR = fb_len/2.0 + zPos
    tL = timeL - dL/cn
    tR = timeR - dR/cn
    return [tL, tR]

class mppcHit:
    side = -1 # 0 is left, 1 is right
    amplitude = 0 # no of photons per col
    time_mc = 0.0
    z = 0.0

    time = 0.0
    time_prod = 0.0 
    pathlength = 0.0

    def __init__(self, side, amp, time_mc, z):
        self.side = side
        self.amplitude = amp
        self.time_mc = time_mc
        self.z = z

    def printHit(self):
        print('side {}, amp {}, time_mc {}, time_prod {}, time_det {}, pathlength {}, z {}, '.format(self.side, \
            self.amplitude, self.time_mc, self.time_prod, self.time, self.pathlength, self.z))



# Essentially just simulating one ribbon
# Simplified mppcs to just sides...

for i in range(n_events):
    if i%100 == 0:
        print "(%6i/%i) %5.1f%% \r" % (i, n_events, 100.*i/n_events),
        sys.stdout.flush()

    allHits = []
    sideMap = defaultdict(list) # [side][hits]

    # Next lines essentially creating fibre "cluster"
    # Note: About 15 fibres hit per particle crossing (from hFHits plot [3 layer scifi sim])
    fibresHit = int(hFHits.GetRandom()) #15

    mc_time = np.random.uniform()*frameLength
    zPos = hZpositions.GetRandom()

    # mppcResponse() func
    for fibre in range(fibresHit):

        # Not sure if energy response should vary fibre to fibre...
        mu_scint = hEdep.GetRandom()/1000.0 * Y
        sigma_scint = 1.0 * np.sqrt(mu_scint) # 1.0 is res scale 
        n_scint = np.random.normal(mu_scint, sigma_scint) # per fibre
        if (n_scint <= 0): continue

        # Loop over sides
        for side in range(2):
            d_side = fb_len/2.0 - pow(-1, side) * zPos

            n_det = getNDetPhotons(n_scint, d_side) # int
            if n_det == 0: continue

            hit = mppcHit(side, n_det, mc_time, zPos)
            sideMap[side].append(hit)

    # elResponse() func
    for side, hits in sideMap.iteritems():
        for hit in hits:
            d_side = fb_len/2.0 - pow(-1, hit.side) * hit.z

            n_pt = int(math.ceil(hit.amplitude*1.0))

            time_prods, pathlens, time_dets = tShiftAnalytical(d_side, n_pt)

            min_photon_index = 0
            for j in range(n_pt):
                if (time_dets[j] < time_dets[min_photon_index]): min_photon_index = j
            
            if (npt_thr == 0): # trigger to 1st photon
                # Adding daq delay too many times?
                hit.time = hit.time_mc + time_dets[min_photon_index] + np.random.normal(mu_daq, sigma_daq_jitter)
                hMinHitTimeDist.Fill(time_dets[min_photon_index])
                hMinHitAndDelayTimeDist.Fill(hit.time)
                min_time_z_corr = time_dets[min_photon_index] - d_side/cn
                time_z_corr = hit.time - d_side/cn
                # print("Hit mc_time {}, min frm exp+pl {}, zPos {}, Zcorr time {}".format(hit.time_mc, time_dets[min_photon_index], hit.z, z_corr))
                hMinHitTimeDistZCorr.Fill(min_time_z_corr)
                hMinHitAndDelayTimeDistZCorr.Fill(time_z_corr)
                hit.time_prod = hit.time_mc + time_prods[min_photon_index]
                hit.pathlength = pathlens[min_photon_index]
          
            elif (npt_thr < n_pt): # sorting required if not triggering to 1st photon
                index = np.array(range(n_pt))
                time_dets = np.array(time_dets)
                inds = time_dets.argsort()
                sortedArr = index[inds] 

                hit.time = hit.time_mc + time_dets[sortedArr[npt_thr]] + np.random.normal(mu_daq, sigma_daq_jitter)
                hit.time_prod = hit.time_mc + time_prods[sortedArr[npt_thr]]
                hit.pathlength = pathlens[sortedArr[npt_thr]]

    # If using min time on either side...
    # tL_min = 10000
    # for hit in sideMap[0]:
    #     if (hit.time < tL_min): tL_min = hit.time
    # tR_min = 10000
    # for hit in sideMap[1]:
    #     if (hit.time < tR_min): tR_min = hit.time

    # [tL_corr, tR_corr] = correctTimes(tL_min, tR_min, zPos)

    # deltaT = tL_min - tR_min
    # deltaTCorr = tL_corr - tR_corr
    # hClusterTimeDiff.Fill(deltaT)
    # hClusterTimeDiffCorr.Fill(deltaTCorr)

    # hClusterTimeMean.Fill((tL_min+tR_min)/2.0 - mc_time)
    # hClusterTimeMeanCorr.Fill((tL_corr+tR_corr)/2.0 - mc_time)

    # tMin = min(tL_min - mc_time, tR_min - mc_time) 
    # tMin_corr = min(tL_corr - mc_time, tR_corr - mc_time) 

    # hClusterTimeMin.Fill(tMin)
    # hClusterTimeMinCorr.Fill(tMin_corr)

    # hClusterTimeLeftCorr.Fill(tL_corr - mc_time)
    # hClusterTimeRightCorr.Fill(tR_corr - mc_time)

    # Unbiased way...?
    # for hitL in sideMap[0]:
    #     tL = hitL.time
    #     for hitR in sideMap[1]:
    #         tR = hitR.time
    #         [tL_corr, tR_corr] = correctTimes(tL, tR, zPos)

    #         deltaT = tL - tR
    #         deltaTCorr = tL_corr - tR_corr
    #         hClusterTimeDiff.Fill(deltaT)
    #         hClusterTimeDiffCorr.Fill(deltaTCorr)

    #         hClusterTimeMean.Fill((tL+tR)/2.0 - mc_time)
    #         hClusterTimeMeanCorr.Fill((tL_corr+tR_corr)/2.0 - mc_time)

    #         tMin = min(tL - mc_time, tR - mc_time) 
    #         tMin_corr = min(tL_corr - mc_time, tR_corr - mc_time) 

    #         hClusterTimeMin.Fill(tMin)
    #         hClusterTimeMinCorr.Fill(tMin_corr)

    #         hClusterTimeLeftCorr.Fill(tL_corr - mc_time)
    #         hClusterTimeRightCorr.Fill(tR_corr - mc_time)

    # if len(allHits) > 0:
    #     for hit in allHits:
    #         # hit.printHit()
    #         # if (abs(hit.z) > 30): continue
    #         hZpos.Fill(hit.z)
    #         hPathlengths.Fill(hit.pathlength)
    #         hTimeProds.Fill(hit.time_prod-hit.time_mc)            


    
print "Time Resolution (1 photon threshold): "
print "(t_left - t_right) \t\t\t\tFWHM/2.355: \t\t       %4.0f ps" % (getFWHM(hClusterTimeDiff, 0.5)/2.355 * 1e3)
print "(t_left - t_right) z-corrected \t\t\tFWHM/2.355: \t\t       %4.0f ps\n" % (getFWHM(hClusterTimeDiffCorr, 0.5)/2.355 * 1e3)

print "(t_left - tMC) \t\t\t\t\tFWHM/2.355: \t\t       %4.0f ps" % (getFWHM(hClusterTimeLeft, 0.5)/2.355 * 1e3)
print "(t_right - tMC) \t\t\t\tFWHM/2.355: \t\t       %4.0f ps\n" % (getFWHM(hClusterTimeRight, 0.5)/2.355 * 1e3)
print "(t_left - tMC) z-corrected \t\t\tFWHM/2.355: \t\t       %4.0f ps" % (getFWHM(hClusterTimeLeftCorr, 0.5)/2.355 * 1e3)
print "(t_right - tMC) z-corrected \t\t\tFWHM/2.355: \t\t       %4.0f ps\n" % (getFWHM(hClusterTimeRightCorr, 0.5)/2.355 * 1e3)

print "(t_left + t_right)/2 - tMC\t\t\tFWHM/2.355: \t\t       %4.0f ps" % (getFWHM(hClusterTimeMean, 0.5)/2.355 * 1e3)
print "(t_left + t_right)/2 - tMC z-corrected\t\tFWHM/2.355: \t\t       %4.0f ps\n" % (getFWHM(hClusterTimeMeanCorr, 0.5)/2.355 * 1e3)

print "min (t_left, t_right) - tMC\t\t\tFWHM/2.355: \t\t       %4.0f ps" % (getFWHM(hClusterTimeMin, 0.5)/2.355 * 1e3)
print "min (t_left, t_right) - tMC z-corrected\t\tFWHM/2.355: \t\t       %4.0f ps\n" % (getFWHM(hClusterTimeMinCorr, 0.5)/2.355 * 1e3)

# raw_input("Press Enter to continue...")
print "Finished..."

output_file.Write()
output_file.Close()