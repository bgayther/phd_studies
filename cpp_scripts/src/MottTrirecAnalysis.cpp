#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RVec.hxx>
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <THStack.h>
#include <TObjString.h>
#include <TString.h>
#include <TChain.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TKey.h>
#include <TStopwatch.h>
#include <TPaveText.h>
#include <TFrame.h>
#include <TError.h>
#include <nlohmann/json.hpp>
#include "RootColourDefs.hpp"
#include "RootHelpers.hpp"
#include "PlotInfo.hpp"
#include "DataFrameHelpers.hpp"
#include "MathHelpers.hpp"
#include "LambdaHelpers.hpp"

using std::cout;
using std::endl;
using std::cin;
using json = nlohmann::json;
// using namespace lambdaHelpers;

Int_t ELECTRON_COL;
Int_t POSITRON_COL;
Int_t MICHEL_COL;

// make script a class, then make these const and init in ctor...
double momentumSpread;
double michelMomentumSpread;
std::string outputGraphsDir;
double energyTag;
std::string energyTagStr;

std::string michelStr = "Michel";
std::string positronStr = "Positrons";
std::string electronStr = "Electrons";
// std::string range = "resolutionRange"

std::string outputFileType = ".pdf";
std::string fileEnding;
std::string dblTurn_p_cut;

// https://root.cern/doc/master/df018__customActions_8C.html for templating histos
struct histo2DTuple {
    DataFrameHelpers::TH2D_ptr elecHist;
    DataFrameHelpers::TH2D_ptr posHist;
};

struct histo1DTuple {
    DataFrameHelpers::TH1D_ptr elecHist;
    DataFrameHelpers::TH1D_ptr posHist;
};

// TODO: make a header file, and define a bunch of stuff I use there...(i.e. beam resolution)

// struct mottDefs {
//     target_A = 12.011;
//     target_Z = 6;
//     // printf("Mott target atom: A %f, Z %f\n", target_A, target_Z);
//     rest_energy = target_A * 931.494028 * 1.0;
//     ZE2 = target_Z * 1 * 1;
//     RU = 3.12 * 1.e-12; // [mm]
//     hbarc = 197.32705e-12; // [MeV*mm]
// } mott;

// double calculateMottDiffCrossSection(double p_scattered, double theta) {
//     double theta_half = theta/2.0;

//     double mott_point_like = pow((mott.ZE2) / 2*p, 2.0) * (pow(std::cos(theta_half), 2.0) / pow(std::sin(theta_half), 4.0)) * (1 / (1 + (2*p / mott.rest_energy) * pow(std::sin(theta_half),2.0)));

//     // double p_scattered = p / (1 + (p/mott.rest_energy)*(1-std::cos(theta)));

//     double q2 = 2 * p * p_scattered * (1 - std::cos(theta));

//     double q = sqrt(q2);

//     double x = (q * mott.RU) / (mott.hbarc * 2 * M_PI);

//     double nuclear_form_factor = 3 * ( (std::sin(x) - x*std::cos(x))/pow(x, 3.0) ); // for carbon

//     double diff_cross_sec = mott_point_like * pow(nuclear_form_factor, 2.0);

//     return diff_cross_sec;
// }

void plotBoth1D(const ROOT::RDF::RNode& elecRDF, const ROOT::RDF::RNode& posRDF, PlotInfo& plot, std::vector<histo1DTuple>& histos) {
    std::string segType = plot.getSegType();
    std::string segTypeSuffix = "";

    if (!segType.empty()) {
        segTypeSuffix = "_" + segType;
        segType = ", " + segType + " tracks";
    }

    ROOT::RDF::RResultPtr<TH1D> hE, hP;

    auto plot_title_base = plot.title;
    auto plot_name_base = plot.name;

    plot.title = (plot_title_base + " Electrons" + segType);
    plot.name = (plot_name_base + "_elec" + segTypeSuffix);
    hE = DataFrameHelpers::plot1D(elecRDF, plot);

    plot.title = (plot_title_base + " Positrons" + segType);
    plot.name = (plot_name_base + "_pos" + segTypeSuffix);
    hP = DataFrameHelpers::plot1D(posRDF, plot);

    plot.name = plot_name_base;
    plot.title = plot_title_base;

    histos.emplace_back(histo1DTuple{hE, hP});
    return;
}

void plotBoth2D(const ROOT::RDF::RNode& elecRDF, const ROOT::RDF::RNode& posRDF, PlotInfo& plot, std::vector<histo2DTuple>& histos) {
    std::string segType = plot.getSegType();
    std::string segTypeSuffix = "";

    if (!segType.empty()) {
        segTypeSuffix = "_" + segType;
        segType = ", " + segType + " tracks";
    }

    ROOT::RDF::RResultPtr<TH2D> hE, hP;

    auto plot_title_base = plot.title;
    auto plot_name_base = plot.name;

    plot.title = (plot_title_base + " Electrons" + segType);
    plot.name = (plot_name_base + "_elec" + segTypeSuffix);
    hE = DataFrameHelpers::plot2D(elecRDF, plot);

    plot.title = (plot_title_base + " Positrons" + segType);
    plot.name = (plot_name_base + "_pos" + segTypeSuffix);
    hP = DataFrameHelpers::plot2D(posRDF, plot);

    plot.name = plot_name_base;
    plot.title = plot_title_base;

    histos.emplace_back(histo2DTuple{hE, hP});
    return;
}

void makeAll2Dplots(std::vector<histo2DTuple>& histos, const std::string& plotStyle, bool savePlot, bool logZ=false) {
    for (auto& hist : histos) {
        auto hE = hist.elecHist;
        auto hP = hist.posHist;
        if (hE->GetEntries() == 0 && hP->GetEntries() == 0) continue; // print something

        std::string segType = PlotInfo::determineSegType(hE->GetName());
        std::string segTypeSuffix = "";
        if (!segType.empty()) segTypeSuffix = "_" + segType;

        // printf("%s entries %.0f, %s entries %.0f\n", hE->GetName(), hE->GetEntries(), hP->GetName(), hP->GetEntries());
        hE->SetMarkerColor(ELECTRON_COL);
        hE->SetLineColor(ELECTRON_COL);

        hP->SetMarkerColor(POSITRON_COL);
        hP->SetLineColor(POSITRON_COL);

        // SetFillStyle(3001...)

        if (savePlot) {
            // TODO: use new legend function
            auto can = TCanvas();

            can.SetLogz(logZ);
            hE->Draw(plotStyle.c_str());
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hE->GetName()) + "_" + fileEnding).c_str());

            hP->Draw(plotStyle.c_str());
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hP->GetName()) + "_" + fileEnding).c_str());

            // if (plotProfiles)
            // TODO: put this into a function...
            can.SetLogz(false);
            auto hP_ProfX = hP->ProfileX("", 1, -1, "do");
            hP_ProfX->SetTitle(hP->GetTitle());
            hP_ProfX->GetYaxis()->SetTitle(TString("Mean ") + hP->GetYaxis()->GetTitle());
            hP_ProfX->GetXaxis()->SetTitleOffset(hP->GetXaxis()->GetTitleOffset());
            hP_ProfX->GetYaxis()->SetTitleOffset(hP->GetYaxis()->GetTitleOffset());
            hP_ProfX->SetMinimum(hP->GetYaxis()->GetXmin());
            hP_ProfX->SetMaximum(hP->GetYaxis()->GetXmax());
            hP_ProfX->Draw();
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hP->GetName()) + "_ProfileX_" + fileEnding).c_str());

            auto hE_ProfX = hE->ProfileX("", 1, -1, "do");
            hE_ProfX->SetTitle(hE->GetTitle());
            hE_ProfX->GetYaxis()->SetTitle(TString("Mean ") + hE->GetYaxis()->GetTitle());
            hE_ProfX->GetXaxis()->SetTitleOffset(hE->GetXaxis()->GetTitleOffset());
            hE_ProfX->GetYaxis()->SetTitleOffset(hE->GetYaxis()->GetTitleOffset());
            hE_ProfX->SetMinimum(hE->GetYaxis()->GetXmin());
            hE_ProfX->SetMaximum(hE->GetYaxis()->GetXmax());
            hE_ProfX->Draw();
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hE->GetName()) + "_ProfileX_" + fileEnding).c_str());

            auto hE_ProfY = hE->ProfileY("", 1, -1, "do");
            hE_ProfY->SetTitle(hE->GetTitle());
            hE_ProfY->GetYaxis()->SetTitle(TString("Mean ") + hE->GetXaxis()->GetTitle());
            hE_ProfY->GetXaxis()->SetTitleOffset(hE->GetXaxis()->GetTitleOffset());
            hE_ProfY->GetYaxis()->SetTitleOffset(hE->GetYaxis()->GetTitleOffset());
            hE_ProfY->SetMinimum(hE->GetXaxis()->GetXmin());
            hE_ProfY->SetMaximum(hE->GetXaxis()->GetXmax());
            hE_ProfY->Draw(); 
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hE->GetName()) + "_ProfileY_" + fileEnding).c_str());

            auto hP_ProfY = hP->ProfileY("", 1, -1, "do");
            hP_ProfY->SetTitle(hP->GetTitle());
            hP_ProfY->GetYaxis()->SetTitle(TString("Mean ") + hP->GetXaxis()->GetTitle());
            hP_ProfY->GetXaxis()->SetTitleOffset(hP->GetXaxis()->GetTitleOffset());
            hP_ProfY->GetYaxis()->SetTitleOffset(hP->GetYaxis()->GetTitleOffset());
            hP_ProfY->SetMinimum(hP->GetXaxis()->GetXmin());
            hP_ProfY->SetMaximum(hP->GetXaxis()->GetXmax());
            hP_ProfY->Draw(); 
            can.Modified();
            can.Update();
            can.SaveAs((outputGraphsDir + std::string(hP->GetName()) + "_ProfileY_" + fileEnding).c_str());

        }

        hE->Write();
        hP->Write();
    }
}

void makeAll1Dplots(std::vector<histo1DTuple>& histos, bool savePlot, bool logY=false) {
    for (auto& hist : histos) {
        auto hE = hist.elecHist;
        auto hP = hist.posHist;
        if (hE->GetEntries() == 0 && hP->GetEntries() == 0) {
            printf("%s entries %.0f, %s entries %.0f\n", hE->GetName(), hE->GetEntries(), hP->GetName(), hP->GetEntries());
            continue; // print something
        }

        std::string segType = PlotInfo::determineSegType(hE->GetName());
        std::string segTypeSuffix = "";
        if (!segType.empty()) segTypeSuffix = "_" + segType;

        // printf("%s entries %.0f, %s entries %.0f\n", hE->GetName(), hE->GetEntries(), hP->GetName(), hP->GetEntries());
        hE->SetMarkerColor(ELECTRON_COL);
        hE->SetLineColor(ELECTRON_COL);
        hP->SetMarkerColor(POSITRON_COL);
        hP->SetLineColor(POSITRON_COL);

        if (savePlot) {
            auto leg = TLegend(0.76,0.76,0.85,0.85); // move to top right corner and use border?
            leg.AddEntry(hP.GetPtr(), " e^{+}", "l");
            leg.AddEntry(hE.GetPtr(), " e^{-}", "l");

            // very ugly...
            auto can = TCanvas();
            can.SetLogy(logY);
            TString colon = ";";
            std::string title_tmp = hE->GetTitle();
            std::string electrons = " Electrons";
            size_t pos = title_tmp.find(electrons);
            if (pos != std::string::npos) title_tmp.erase(pos, electrons.length());

            auto title = TString(title_tmp) + ";" + hE->GetXaxis()->GetTitle() + colon + hE->GetYaxis()->GetTitle();
            auto hs = THStack("hs", title);
            hs.Add(hE.GetPtr());
            hs.Add(hP.GetPtr());

            hs.Draw("nostack");
            leg.Draw();
            std::string name = hE->GetName();
            std::string stub = "_elec";
            pos = name.find(stub);
            if (pos != std::string::npos) name.erase(pos, stub.length());
            auto can_name = outputGraphsDir + name + "_" + fileEnding;
            can.Modified();
            can.Update();
            can.SaveAs(can_name.c_str());
        }

        hE->Write();
        hP->Write();
    }

    auto colours = RootColourDefs();

    auto S4_COL = colours.kC0;
    auto L6_COL = colours.kC1;
    auto L8_COL = colours.kC2;

    for (auto& hist : histos) {
        auto hE = hist.elecHist;
        auto hP = hist.posHist;

        auto hE_name = hE->GetName();
        auto hP_name = hP->GetName();
        if (PlotInfo::determineSegType(hE_name) != "") continue;

        auto can = TCanvas();
        can.SetLogy(logY);

        // Save these coords somewhere
        auto legE = TLegend(0.76,0.76,0.85,0.85);
        auto legP = TLegend(0.76,0.76,0.85,0.85);

        // std::cout << hE->GetTitle() << ";" << hE->GetXaxis()->GetTitle() << ";" << hE->GetYaxis()->GetTitle() << "\n";
        auto hE_title = hE->GetTitle() + TString(";") + hE->GetXaxis()->GetTitle() + TString(";") + hE->GetYaxis()->GetTitle();
        auto hP_title = hP->GetTitle() + TString(";") + hP->GetXaxis()->GetTitle() + TString(";") + hP->GetYaxis()->GetTitle();

        auto hsE = THStack("hsE", hE_title);
        auto hsP = THStack("hsP", hP_title);

        // std::cout << "Looking for hists matching " << hE_name << "\n";
        int ctrE = 0, ctrP = 0;
        for (auto& histTemp : histos) {
            auto hE_tmp = histTemp.elecHist;
            auto hP_tmp = histTemp.posHist;

            auto hE_tmp_name = (std::string)hE_tmp->GetName();
            auto hP_tmp_name = (std::string)hP_tmp->GetName();

            if (hE_tmp_name.find(hE_name) != std::string::npos && hE_tmp->GetEntries()) {
                std::string segType = PlotInfo::determineSegType(hE_tmp->GetName());
                if (segType.empty()) continue;
                if (segType == "S4") hE_tmp->SetLineColor(S4_COL);
                else if (segType == "L6") hE_tmp->SetLineColor(L6_COL);
                else if (segType == "L8") hE_tmp->SetLineColor(L8_COL);
                // std::cout << "  found! segType " << segType << ", name " << hE_tmp_name << '\n';
                // std::cout << "hE entries " << hE_tmp->GetEntries() << "\n";
                if (logY) hE_tmp->SetMinimum(1);
                legE.AddEntry(hE_tmp.GetPtr(), segType.c_str(), "l");
                hsE.Add(hE_tmp.GetPtr());
                ctrE++;
            }

            if (hP_tmp_name.find(hP_name) != std::string::npos && hP_tmp->GetEntries()) {
                std::string segType = PlotInfo::determineSegType(hP_tmp->GetName());
                if (segType.empty()) continue;
                if (segType == "S4") hP_tmp->SetLineColor(S4_COL);
                else if (segType == "L6") hP_tmp->SetLineColor(L6_COL);
                else if (segType == "L8") hP_tmp->SetLineColor(L8_COL);
                // std::cout << "  found! segType " << segType << ", name " << hP_tmp_name << '\n';
                // std::cout << "hP entries " << hP_tmp->GetEntries() << "\n";
                if (logY) hP_tmp->SetMinimum(1);
                legP.AddEntry(hP_tmp.GetPtr(), segType.c_str(), "l");
                hsP.Add(hP_tmp.GetPtr());
                ctrP++;
            }
        }

        std::string can_name = outputGraphsDir + hE_name + "_overlay_" + fileEnding;
        if (ctrE) {
            hsE.Draw("nostack");
            legE.Draw();
            // std::cout << can_name << std::endl;
            can.SaveAs(can_name.c_str());
        }

        if (ctrP) {
            hsP.Draw("nostack");
            legP.Draw();
            can_name = outputGraphsDir + hP_name + "_overlay_" + fileEnding;
            // std::cout << can_name << std::endl;
            can.Modified();
            can.Update();
            can.SaveAs(can_name.c_str());
        }
    }
}

void plotBoth1DForTrackTypes(std::map<int, ROOT::RDF::RNode>& elecTrackFilterMap, std::map<int, ROOT::RDF::RNode>& posTrackFilterMap, PlotInfo& plot, const std::vector<int>& trackTypes, std::vector<histo1DTuple>& histos) {
    for (const auto& i : trackTypes) {
        plot.setSegType(i);
        plotBoth1D(elecTrackFilterMap.at(i), posTrackFilterMap.at(i), plot, histos); // pass which func you want to use??
    }
}

void plotBoth2DForTrackTypes(std::map<int, ROOT::RDF::RNode>& elecTrackFilterMap, std::map<int, ROOT::RDF::RNode>& posTrackFilterMap, PlotInfo& plot, const std::vector<int>& trackTypes, std::vector<histo2DTuple>& histos) {
    for (const auto& i : trackTypes) {
        plot.setSegType(i);
        plotBoth2D(elecTrackFilterMap.at(i), posTrackFilterMap.at(i), plot, histos); // pass which func you want to use??
    }
}

void plotAll(const ROOT::RDF::RNode& elecRDF, const ROOT::RDF::RNode& posRDF, std::map<int, ROOT::RDF::RNode>& elecTrackFilterMap, std::map<int, ROOT::RDF::RNode>& posTrackFilterMap,
             const std::vector<int>& trackTypes, std::vector<PlotInfo>& plots, std::vector<histo1DTuple>& histos1D, std::vector<histo2DTuple>& histos2D) {

    for (auto& plotter : plots) {
        if (plotter.varY != "") {
            plotBoth2D(elecRDF, posRDF, plotter, histos2D);
            if (plotter.perTrack) plotBoth2DForTrackTypes(elecTrackFilterMap,  posTrackFilterMap, plotter, trackTypes, histos2D);
        } else {
            plotBoth1D(elecRDF, posRDF, plotter, histos1D);
            if (plotter.perTrack) plotBoth1DForTrackTypes(elecTrackFilterMap,  posTrackFilterMap, plotter, trackTypes, histos1D);
        }
    }
}

json getConfig(const std::string& inputFile) {
    // Get config details...
    auto file = new TFile(inputFile.c_str(), "READ");
    auto confKey = file->GetKey("config");
    json conf;
    if (confKey) {
        auto confStr = (TObjString*) confKey->ReadObj();
        auto x = confStr->String();
        conf = json::parse(x.Data());
        std::string particle;
        for (auto& i : conf["digi"]["generators"]) {
            if (i["type"] == "beam" && i.contains("particle")) particle = i["particle"];
        }
        cout << "Energy " << conf["digi"]["beam"]["momentum"]["mean"] << ", particle " << particle << \
                ", target " << conf["detector"]["target"]["shape"] << \
                ", thickness " << conf["detector"]["target"]["thickness1"] << ", material " << \
                conf["detector"]["target"]["material"] << \
                ", beam momentum spread " << conf["digi"]["beam"]["momentum"]["spread"] << std::endl;
    }
    file->Close();
    return conf;
}

ROOT::RDF::RNode makeDataFrame(ROOT::RDataFrame& input) {
    return input.Define("lam010", "lam01[0]")
                .Define("pCorr", corrSegsMomCharge, {"p", "mc_pid"})
                .Define("p_res", diff, {"pCorr", "mc_p"})
                .Define("lam_res", dphi, {"lam010", "mc_lam"})
                .Define("tan010", "tan01[0]")
                .Define("deltaS4_dp", deltaS4_dp, {"p", "p_S4_turn2", "p_S4_turn1"})
                .Define("deltaS4_dtan", dphi, {"tan_S4_turn1", "tan_S4_turn2"})
                .Define("deltaS4_dlam", dphi, {"lam_S4_turn1", "lam_S4_turn2"})
                .Define("deltaL8_dp", deltaL8_dp, {"p_next_turn", "p"})
                .Define("deltaL8_dtan", dphi, {"tan010", "tan_next_turn"})
                .Define("deltaL8_dlam", dphi, {"lam010", "lam_next_turn"})
                .Filter(qualityCut, {"zpca_z", "lam010"})
                .Filter(framesCut, {"nhit", "s6n", "s6n_0", "s8n", "s8n_0", "seq"});
}

int main(int argc, char** argv) {
    namespace po = boost::program_options;

    std::vector<std::string> inputElecFiles, inputPosFiles, inputMichelFiles;
    std::string outputFile, histJSONFile;
    int savePlots = 1;
    int nFrames = 0, nSkip = 0;

    // use template
    std::vector<histo1DTuple> histos1D;
    std::vector<histo2DTuple> histos2D;

    po::options_description opts;
    opts.add_options()
        ("inputElec", po::value(&inputElecFiles)->multitoken(), "input electron reco root file(s)")
        ("inputPos", po::value(&inputPosFiles)->multitoken(), "input positron reco root file(s)")
        ("inputMichel", po::value(&inputMichelFiles)->multitoken(), "input michel reco root file(s) [optional]")
        ("histJSON", po::value(&histJSONFile), "input histogram definitons in json file")
        ("output", po::value(&outputFile), "output root file")
        ("outputGraphsDir", po::value(&outputGraphsDir), "output root file")
        ("savePlots", po::value(&savePlots), "save plots as .pdfs")
        ("nFrames", po::value(&nFrames), "Number of frames to process total")
        ("nSkip", po::value(&nSkip), "Number of frames to skip")
        ("help", "help");

    po::positional_options_description opts_p;
    opts_p.add("inputElec", 1);
    opts_p.add("inputPos", 1);
    opts_p.add("output", 1);
    opts_p.add("outputGraphsDir", 1);
    opts_p.add("savePlots", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).positional(opts_p).run(), vm);
    po::notify(vm);

    if(vm.count("help") || inputElecFiles.empty() || inputPosFiles.empty() || outputFile.empty()) {
        printf("Usage: mu3eAnaMott [options] <inputElec> <inputPos> <output>\n");
        opts.print(cout);
        return -1;
    }

    gROOT->ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h"); // TODO: give dir in args
    // gROOT->ForceStyle();

    // move outside of main?
    auto colours = RootColourDefs();

    ELECTRON_COL = colours.kC0;
    POSITRON_COL = colours.kC3;
    MICHEL_COL = colours.kC2;

    gROOT->SetBatch(true);

    TStopwatch watch;
    ROOT::EnableImplicitMT();

    watch.Reset();
    watch.Start();

    bool MICHEL_MODE = false;
    json michelConf;
    if (!inputMichelFiles.empty()) {
        MICHEL_MODE = true;
        michelConf = getConfig(inputMichelFiles[0]);
        michelMomentumSpread = std::stod(michelConf["digi"]["beam"]["momentum"]["spread"].get<json::string_t>());
    }

    auto conf = getConfig(inputElecFiles[0]);
    getConfig(inputPosFiles[0]);
    energyTagStr = conf["digi"]["beam"]["momentum"]["mean"].get<std::string>();
    energyTag = std::stod(conf["digi"]["beam"]["momentum"]["mean"].get<json::string_t>());
    momentumSpread = std::stod(conf["digi"]["beam"]["momentum"]["spread"].get<json::string_t>());

    fileEnding = energyTagStr + "_MeV_sample" + outputFileType;

    if (!outputGraphsDir.empty()) {
        boost::filesystem::create_directory(outputGraphsDir);
        outputGraphsDir.append("/"); // check if / at end of string
    } else {
        boost::filesystem::create_directory("graphs");
        outputGraphsDir = "graphs/";
    }

    TFile* outfile = new TFile((outputFile + "_" + energyTagStr + "_MeV.root").c_str(), "recreate");

    json histoJSON;
    if (!histJSONFile.empty()) {
        std::ifstream histos(histJSONFile);
        if (histos.is_open()) {
            histos >> histoJSON;
            histos.close();
            // std::cout << histoJSON["histograms"] << std::endl;
        }
        else {
            std::cerr << "error, not able to open histogram json file\n";
            exit(EXIT_FAILURE);
        }
    } else std::cout << "No histograms given in json file...\n";

    std::vector<int> trackTypes {4, 6, 8};

    // segs tree
    ROOT::RDataFrame electronSegsDataFrame("segs", inputElecFiles);
    ROOT::RDataFrame positronSegsDataFrame("segs", inputPosFiles);

    ROOT::RDataFrame michelSegsDataFrame(0);
    if (MICHEL_MODE) {
        ROOT::RDataFrame temp("segs", inputMichelFiles);
        michelSegsDataFrame = temp;
    }

    // TODO: checks for dlam/dtan for double turns!

    auto filteredSegsElecDF = makeDataFrame(electronSegsDataFrame);
    filteredSegsElecDF = filteredSegsElecDF.Filter(filterElecTracks, {"p"}).Filter(selectMCElecTrack, {"mc_pid", "mc_type"});

    auto filteredSegsPosDF = makeDataFrame(positronSegsDataFrame);
    filteredSegsPosDF = filteredSegsPosDF.Filter(filterPosTracks, {"p"}).Filter(selectMCPosTrack, {"mc_pid", "mc_type"});

    ROOT::RDF::RNode filteredSegsMichelDF(michelSegsDataFrame);
    if (MICHEL_MODE) {
        filteredSegsMichelDF = makeDataFrame(michelSegsDataFrame);
        filteredSegsMichelDF = filteredSegsMichelDF.Filter(filterPosTracks, {"p"}).Filter(selectMichelTracks, {"mc_type"});
    }

    printf("Registered dataframes\n");

    // use typedefs
    std::map<int, ROOT::RDF::RNode> elecTrackSegFilterMap;
    std::map<int, ROOT::RDF::RNode> posTrackSegFilterMap;
    std::map<int, ROOT::RDF::RNode> michelTrackSegFilterMap;

    for (auto i : trackTypes) {
        auto cut = [i] (const int &nhit) {return (nhit==i);};
        elecTrackSegFilterMap.insert(std::make_pair(i, filteredSegsElecDF.Filter(cut, {"nhit"})));
        posTrackSegFilterMap.insert(std::make_pair(i, filteredSegsPosDF.Filter(cut, {"nhit"})));
        if (MICHEL_MODE) michelTrackSegFilterMap.insert(std::make_pair(i, filteredSegsMichelDF.Filter(cut, {"nhit"})));
    }

    // auto d = elecTrackSegFilterMap.at(6).Display({"nhit", "mc", "nhit", "seq", "s6n", "s6n_0", "s8n", "s8n_0"}, 100);
    // d->Print();

    printf("Registered dataframes by track type\n");

    // define pMin, pMax, pBins...
    auto energyTagTitleStr = energyTagStr + " MeV";

    std::vector<PlotInfo> plotsToMake = PlotInfo::makePlotsVecFromJSON(histoJSON);
    for (auto& p : plotsToMake) p.setTitle(energyTagTitleStr);

    // dynamic plots
    dblTurn_p_cut = "abs(p)<=(" + std::to_string(energyTag+1) + ")&&abs(p)>=(" + std::to_string(energyTag-1) + ")";
    std::cout << "Double turn momentum cut: " << dblTurn_p_cut << "\n";
    plotsToMake.emplace_back("h_mc_p", energyTagTitleStr, "p_{MC} [MeV/#it{c}]", "Counts", 40, energyTag-2, energyTag+2, "mc_p", dblTurn_p_cut, true);
    // plotsToMake.emplace_back("h_L8_deltaS4", energyTagTitleStr, "#Deltap [MeV/#it{c}]", "Counts", 100, -10, 10, "deltaS4_dp", "nhit==8&&p_S4_turn2!=0.0&&p_S4_turn1!=0.0&&" + dblTurn_p_cut, false);
    // plotsToMake.emplace_back("h_L8_deltaS4_vs_mc_p", energyTagTitleStr, "p_{MC} [MeV/#it{c}]", "#Deltap [MeV/#it{c}]", 40, energyTag-1, energyTag+1, 100, -10, 10, "mc_p", "deltaS4_dp", "nhit==8&&p_S4_turn2!=0.0&&p_S4_turn1!=0.0", false);
    // plotsToMake.emplace_back("h_L8_deltaS4_vs_reco_p", energyTagTitleStr, "p_{Reco} [MeV/#it{c}]", "#Deltap [MeV/#it{c}]", 40, energyTag-1, energyTag+1, 100, -10, 10, "pCorr", "deltaS4_dp", "nhit==8&&p_S4_turn2!=0.0&&p_S4_turn1!=0.0", false);
    // plotsToMake.emplace_back("h_L8_deltaS4_vs_reco_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "#Deltap [MeV/#it{c}]", 144, -1.8, 1.8, 100, -10, 10, "lam010", "deltaS4_dp", "nhit==8&&p_S4_turn2!=0.0&&p_S4_turn1!=0.0", false);
    plotsToMake.emplace_back("h_deltaL8_dp", energyTagTitleStr, "#Deltap [MeV/#it{c}]", "Counts", 100, -6, 4, "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dp_vs_mc_p", energyTagTitleStr, "p_{MC} [MeV/#it{c}]", "#Deltap [MeV/#it{c}]", 40, energyTag-1, energyTag+1, 100, -6, 4, "mc_p", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dp_vs_reco_p", energyTagTitleStr, "p_{Reco} [MeV/#it{c}]", "#Deltap [MeV/#it{c}]", 40, energyTag-1, energyTag+1, 100, -6, 4, "pCorr", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dp_vs_reco_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "#Deltap [MeV/#it{c}]", 144, -1.8, 1.8, 100, -6, 4, "lam010", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dp_vs_reco_phi", energyTagTitleStr, "#phi_{Reco} [rad]", "#Deltap [MeV/#it{c}]", 256, -3.2, 3.2, 100, -6, 4, "tan010", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);

    plotsToMake.emplace_back("h_deltaL8_dlam", energyTagTitleStr, "#Delta#lambda [rad]", "Counts", 40, -0.2, 0.2, "deltaL8_dlam", "nhit==8&&lam_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dlam_vs_mc_p", energyTagTitleStr, "p_{MC} [MeV/#it{c}]", "#Delta#lambda [rad]", 40, energyTag-1, energyTag+1, 40, -0.2, 0.2, "mc_p", "deltaL8_dlam", "nhit==8&&lam_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dlam_vs_reco_p", energyTagTitleStr, "p_{Reco} [MeV/#it{c}]", "#Delta#lambda [rad]", 40, energyTag-1, energyTag+1, 40, -0.2, 0.2, "pCorr", "deltaL8_dlam", "nhit==8&&lam_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dlam_vs_reco_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "#Delta#lambda [rad]", 144, -1.8, 1.8, 40, -0.2, 0.2, "lam010", "deltaL8_dlam", "nhit==8&&lam_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dlam_vs_reco_phi", energyTagTitleStr, "#phi_{Reco} [rad]", "#Delta#lambda [rad]", 256, -3.2, 3.2, 40, -0.2, 0.2, "tan010", "deltaL8_dlam", "nhit==8&&lam_next_turn!=0.0&&" + dblTurn_p_cut, false);

    plotsToMake.emplace_back("h_deltaL8_dphi", energyTagTitleStr, "#Delta#phi [rad]", "Counts", 40, -0.2, 0.2, "deltaL8_dtan", "nhit==8&&tan_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dphi_vs_mc_p", energyTagTitleStr, "p_{MC} [MeV/#it{c}]", "#Delta#phi [rad]", 40, energyTag-1, energyTag+1, 40, -0.2, 0.2, "mc_p", "deltaL8_dtan", "nhit==8&&tan_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dphi_vs_reco_p", energyTagTitleStr, "p_{Reco} [MeV/#it{c}]", "#Delta#phi [rad]", 40, energyTag-1, energyTag+1, 40, -0.2, 0.2, "pCorr", "deltaL8_dtan", "nhit==8&&tan_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dphi_vs_reco_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "#Delta#phi [rad]", 144, -1.8, 1.8, 40, -0.2, 0.2, "lam010", "deltaL8_dtan", "nhit==8&&tan_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dphi_vs_reco_phi", energyTagTitleStr, "#phi_{Reco} [rad]", "#Delta#phi [rad]", 256, -3.2, 3.2, 40, -0.2, 0.2, "tan010", "deltaL8_dtan", "nhit==8&&tan_next_turn!=0.0&&" + dblTurn_p_cut, false);

    plotsToMake.emplace_back("h_deltaL8_dp_vs_dlam", energyTagTitleStr, "#Delta#lambda [rad]", "#Deltap [MeV/#it{c}]", 40, -0.2, 0.2, 100, -6, 4, "deltaL8_dlam", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);
    plotsToMake.emplace_back("h_deltaL8_dp_vs_dphi", energyTagTitleStr, "#Delta#phi [rad]", "#Deltap [MeV/#it{c}]", 40, -0.2, 0.2, 100, -6, 4, "deltaL8_dtan", "deltaL8_dp", "nhit==8&&p_next_turn!=0.0&&" + dblTurn_p_cut, false);

    plotsToMake.emplace_back("h_p_vs_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "p_{Reco} [MeV/#it{c}]", 144, -1.8, 1.8, 40, energyTag-1, energyTag+1, "lam010", "pCorr", "", true);
    plotsToMake.emplace_back("h_p_mc_vs_lam", energyTagTitleStr, "#lambda_{Reco} [rad]", "p_{MC} [MeV/#it{c}]", 144, -1.8, 1.8, 40, energyTag-1, energyTag+1, "lam010", "mc_p", "", true);

    // TODO: Add all to hist json
    plotsToMake.emplace_back("h_p_unidentified", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "mc_type==-1", false);
    plotsToMake.emplace_back("h_p_michel", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==10 || (mc_type-2)==10", false);
    plotsToMake.emplace_back("h_p_photon_conv", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==40 || (mc_type-2)==40", false);
    plotsToMake.emplace_back("h_p_bhabha", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==50 || (mc_type-2)==50", false);
    plotsToMake.emplace_back("h_p_bremSS", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==60 || (mc_type-2)==60", false);
    plotsToMake.emplace_back("h_p_annihilation", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==70 || (mc_type-2)==70", false);
    plotsToMake.emplace_back("h_p_compton", "", "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "(mc_type-1)==80 || (mc_type-2)==80", false);

    // plotsToMake.emplace_back("h_p_res_michel", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==10 || (mc_type-2)==10", false);
    // plotsToMake.emplace_back("h_p_res_photon_conv", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==40 || (mc_type-2)==40", false);
    // plotsToMake.emplace_back("h_p_res_bhabha", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==50 || (mc_type-2)==50", false);
    // plotsToMake.emplace_back("h_p_res_bremSS", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==60 || (mc_type-2)==60", false);
    // plotsToMake.emplace_back("h_p_res_annihilation", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==70 || (mc_type-2)==70", false);
    // plotsToMake.emplace_back("h_p_res_compton", "", "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "(mc_type-1)==80 || (mc_type-2)==80", false);

    // plotsToMake.emplace_back("h_p_fakes", energyTagTitleStr, "p_{Reco} [MeV/#it{c}]", "Counts", 280, -70, 70, "p", "mc==0", true);
    // plotsToMake.emplace_back("h_p_res_fakes", energyTagTitleStr, "p_{Reco} - p_{MC} [MeV/#it{c}]", "Counts", 400, -50, 50, "p_res", "mc==0", true);
    // plotsToMake.emplace_back("h_zpca_z_fakes", energyTagTitleStr, "z0 [mm]", "Counts", 150, -150, 150, "zpca_z", "mc==0", true);
    // plotsToMake.emplace_back("h_zpca_r_fakes", energyTagTitleStr, "DCA [mm]", "Counts", 64, -32, 32, "zpca_r", "mc==0", true);
    // plotsToMake.emplace_back("h_mc_lam_fakes", energyTagTitleStr, "#lambda_{MC} [rad]", "Counts", 144, -1.8, 1.8, "mc_lam", "mc==0", true);
    // plotsToMake.emplace_back("h_reco_lam_fakes", energyTagTitleStr, "#lambda_{Reco} [rad]", "Counts", 144, -1.8, 1.8, "lam010", "mc==0", true);

    // // gErrorIgnoreLevel = kWarning;
    printf("Processed hist json\n");

    bool logAxis = true;
    plotAll(filteredSegsElecDF, filteredSegsPosDF, elecTrackSegFilterMap, posTrackSegFilterMap, trackTypes, plotsToMake, histos1D, histos2D); // TODO: give option to pass michel?
    printf("Registered all histograms to save\n");
    
    makeAll2Dplots(histos2D, "colz", true, logAxis);
    printf("Saved all 2d histograms\n");

    makeAll1Dplots(histos1D, true, logAxis);
    printf("Saved all 1d histograms\n");

    // do {
    //     cout << '\n' << "Press a key to continue...";
    // } while (cin.get() != '\n');

    watch.Stop();
    cout << "The code took " << watch.RealTime() << " seconds." << endl;

    outfile->Write();
    outfile->Close();
    return 0;
}