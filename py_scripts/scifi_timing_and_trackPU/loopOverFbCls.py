import ROOT
import numpy as np
import argparse
import os
import sys
import tabulate
import json

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of frames to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='fbRecoHitTimingFit', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

os.chdir(args.path)

fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n

clusterTree = ROOT.TChain("fb")
cfg = json.loads(str(ROOT.TFile.Open(args.inputs[0]).Get("config").GetString()))

clsMadeWithFit = True if cfg["trirec"]["fb"]["recluster_using_likelihood"].lower() == "true" else False
recluster_singleTrk_clusters = True if cfg["trirec"]["fb"]["recluster_singleTrk_clusters"].lower() == "true" else False
recluster_multiTrk_clusters = True if cfg["trirec"]["fb"]["recluster_multiTrk_clusters"].lower() == "true" else False

singleTrk_dt_min = float(cfg["trirec"]["fb"]["singleTrk_dt_min"])
singleTrk_dt_max = float(cfg["trirec"]["fb"]["singleTrk_dt_max"])
multiTrk_dt_min = float(cfg["trirec"]["fb"]["multiTrk_dt_min"])
fit_max_trackPU_density = int(cfg["trirec"]["fb"]["fit_max_trackPU_density"])
recluster_nh_min = int(cfg["trirec"]["fb"]["recluster_nh_min"])

for file in args.inputs:
    print("Adding {} for analysis".format(file))
    clusterTree.Add(file)

nevents = clusterTree.GetEntries()
print("Entries in tree = {}".format(nevents))
if args.nevents >= 0: nevents = args.nevents

skip = args.skip
# ns_PU_type_ctr = [[0.0]*5 for i in range(6)]

splitCl_ctr = 0
splitCl_incl_one_hit_ctr = 0
splitCl_empty_ctr = 0
num_assignments_ctr = 0

cls_to_split = 0
ns_involved = 0

for i in range(skip, nevents+skip):
    clusterTree.GetEntry(i)

    nh = clusterTree.nh
    ns = clusterTree.ns
    PU_type = clusterTree.PU_type

    if (not clsMadeWithFit):
        t_nh_and_ns_cut = ns<=fit_max_trackPU_density and nh>=recluster_nh_min
        tCut = (ns>1 and clusterTree.dt_max>=multiTrk_dt_min) and t_nh_and_ns_cut and clusterTree.passMultiTrkCut==1#(#and PU_type==2
        # tCut = (ns==1 and (clusterTree.dt_max>=singleTrk_dt_min and clusterTree.dt_max<=singleTrk_dt_max)) and t_nh_and_ns_cut #and PU_type==2
        if (tCut):
            cls_to_split += 1
            ns_involved  += ns
            # print("Event id {}, cl_tid {}, dt {}, nh {}, ns {}, PU type {}, madeWithFit {}, passMultiTrkCut {}".format(clusterTree.eventId, clusterTree.cl_tid, clusterTree.dt_max, nh, ns, PU_type, clusterTree.madeWithFit, clusterTree.passMultiTrkCut))
    else:
        if (clusterTree.madeWithFit==1):
            splitCl_incl_one_hit_ctr+=1
            if (nh>1): 
                splitCl_ctr+=1
                num_assignments_ctr += ns
            elif nh==0:
                splitCl_empty_ctr += 1

    # if (clusterTree.passMultiTrkCut==0):

    # splt_cl = clusterTree.
    # if (ns>0 and ns<=5 and PU_type!=-1): 
    #     # print("ns {}, PU_type {}".format(ns,PU_type))
    #     ns_PU_type_ctr[ns][PU_type] += 1.0


if (clsMadeWithFit): print("Total number of split clusters = {}, number of split clusters (excl one hit cls) = {}, number of segments assigned to these = {}, number of empty split cls {}".format(splitCl_incl_one_hit_ctr, splitCl_ctr, num_assignments_ctr,splitCl_empty_ctr))
elif (not clsMadeWithFit): print("Total number of clusters to be split = {}, total ns involved = {}".format(cls_to_split, ns_involved))

# cl_ns_PU_table = []

# if (sum(ns_PU_type_ctr[1][:])):
#     cl_ns_PU_table.append([1,100.0*ns_PU_type_ctr[1][0]/sum(ns_PU_type_ctr[1][:]),100.0*ns_PU_type_ctr[1][1]/sum(ns_PU_type_ctr[1][:]),100.0*ns_PU_type_ctr[1][2]/sum(ns_PU_type_ctr[1][:]),100.0*ns_PU_type_ctr[1][3]/sum(ns_PU_type_ctr[1][:])])
#     cl_ns_PU_table.append([])
# if (sum(ns_PU_type_ctr[2][:])):
#     cl_ns_PU_table.append([2,100.0*ns_PU_type_ctr[2][0]/sum(ns_PU_type_ctr[2][:]),100.0*ns_PU_type_ctr[2][1]/sum(ns_PU_type_ctr[2][:]),100.0*ns_PU_type_ctr[2][2]/sum(ns_PU_type_ctr[2][:]),100.0*ns_PU_type_ctr[2][3]/sum(ns_PU_type_ctr[2][:])])
#     cl_ns_PU_table.append([])
# if (sum(ns_PU_type_ctr[3][:])):
#     cl_ns_PU_table.append([3,100.0*ns_PU_type_ctr[3][0]/sum(ns_PU_type_ctr[3][:]),100.0*ns_PU_type_ctr[3][1]/sum(ns_PU_type_ctr[3][:]),100.0*ns_PU_type_ctr[3][2]/sum(ns_PU_type_ctr[3][:]),100.0*ns_PU_type_ctr[3][3]/sum(ns_PU_type_ctr[3][:])])
#     cl_ns_PU_table.append([])
# if (sum(ns_PU_type_ctr[4][:])):
#     cl_ns_PU_table.append([4,100.0*ns_PU_type_ctr[4][0]/sum(ns_PU_type_ctr[4][:]),100.0*ns_PU_type_ctr[4][1]/sum(ns_PU_type_ctr[4][:]),100.0*ns_PU_type_ctr[4][2]/sum(ns_PU_type_ctr[4][:]),100.0*ns_PU_type_ctr[4][3]/sum(ns_PU_type_ctr[4][:])])
#     cl_ns_PU_table.append([])
# if (sum(ns_PU_type_ctr[5][:])):
#     cl_ns_PU_table.append([5,100.0*ns_PU_type_ctr[5][0]/sum(ns_PU_type_ctr[5][:]),100.0*ns_PU_type_ctr[5][1]/sum(ns_PU_type_ctr[5][:]),100.0*ns_PU_type_ctr[5][2]/sum(ns_PU_type_ctr[5][:]),100.0*ns_PU_type_ctr[5][3]/sum(ns_PU_type_ctr[5][:])])

# print(tabulate.tabulate(cl_ns_PU_table, ["Numb Segs assigned to fb cluster","% Clean","% RecurlerPU","% TrackPU","% MixedPU"], tablefmt='fancy_grid', floatfmt="3.1f",numalign="right"))
