import json
import ROOT
import numpy as np
# import time
from collections import defaultdict
import argparse
# import logging
import os
import sys
from math import log10, floor
# import copy
import tabulate
from tqdm import tqdm


parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-hole-reco-file','--hole-reco-file', dest='holes', type=str, required=False, action='append', help='Extra reco file with holes')
parser.add_argument('-holeless-reco-file','--holeless-reco-file', dest='no_holes', type=str, required=False, action='append', help='Extra reco file without holes')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of framesTree to process; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='cosmicRecoAna', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e_cosmicRun/mu3e/tests/data", help='path/to/reco_file.root (not including file)')
args = parser.parse_args()

# ROOT.gROOT.ProcessLine(".L SetMu3eRootStyle.h")  
# ROOT.SetMu3eStyle(1)

os.chdir(args.path)
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)

save = True

segs = ROOT.TChain("segs")
segs_mc = ROOT.TChain("segs_mc")

cfg = json.loads(str(ROOT.TFile.Open(args.inputs[0]).Get("config").GetString()))
noise = 0.0
if (not cfg["trirec"].get("expert") is None): noise = float(cfg["trirec"]["expert"]["noise"])

for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    # segs.Add(file)
    segs_mc.Add(file)
    # framesTree.Add(file)
    # framesMCTree.Add(file)

nevents = segs.GetEntries()
if args.nevents >= 0: nevents = args.nevents

if args.holes:
    for file in args.holes: segs.Add(file)
else:
    for file in args.inputs: segs.Add(file)

pid_and_type_cut = "(mc_type==3||mc_type==4)&&abs(mc_pid)==13"

segs_df = ROOT.RDataFrame(segs).Define("pt", "mc_p*sin(mc_phi)").Define("lam010","lam01[0]").Define("abs_p", "abs(p)").Define("abs_zpca_r","abs(zpca_r)").Define("phi010","phi01[0]").Define("perr", "sqrt(perr2)").Filter("mc==1&&"+pid_and_type_cut)
segsFake_df = ROOT.RDataFrame(segs).Define("pt", "mc_p*sin(mc_phi)").Define("lam010","lam01[0]").Define("abs_p", "abs(p)").Define("abs_zpca_r","abs(zpca_r)").Define("phi010","phi01[0]").Filter("mc==0") # should have extra type/pid cut?
segsMC_df = ROOT.RDataFrame(segs_mc).Define("pt", "mc_p*sin(mc_phi)").Define("lam010","lam01[0]").Define("abs_p", "abs(p)").Define("abs_zpca_r","abs(zpca_r)").Define("phi010","phi01[0]").Filter(pid_and_type_cut)

print("All fakes rate {:.2f} %, S4 fake rate {:.2f} %, S3 fake rate {:.2f} %".format(100.0*segsFake_df.Count().GetValue()/segs_df.Count().GetValue(), 100.0*segsFake_df.Filter("nhit==4").Count().GetValue()/segs_df.Filter("nhit==4").Count().GetValue(), 100.0*segsFake_df.Filter("nhit==3").Count().GetValue()/segs_df.Filter("nhit==3").Count().GetValue()))
print("S3 efficiency {:.2f} %, S4 efficiency {:.2f} %".format(100.0*segs_df.Filter("nhit==3").Count().GetValue() / segsMC_df.Filter("nhit==3").Count().GetValue(), 100.0*segs_df.Filter("nhit==4").Count().GetValue() / segsMC_df.Filter("nhit==4").Count().GetValue()))

# TODO: re-use functions for ratio plotting...
# https://root.cern.ch/doc/v608/ratioplot_8py_source.html

# TODO: optimise argument functions (don't give cut to every func...)

# TODO: define global prefix for title/name of plots

# ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h")

def plotVar(var, xbins, xmin, xmax, xtitle, cut):
    can = ROOT.TCanvas("can","")
    if cut != "": df_cut = segs_df.Filter(cut)
    else: df_cut = segs_df
    seg_type = "allSegs_" if cut=="" else ("S4_" if cut=="nhit==4" else "S3_")
    name = seg_type+var
    h = df_cut.Histo1D(("h_"+name, xtitle, xbins, xmin, xmax), var)
    h.Draw()
    can.SetRightMargin(0.12)
    can.SetLeftMargin(0.11)
    can.SetBottomMargin(0.11)
    if (save): can.SaveAs(args.output+"_"+name+".pdf")
    h.Write()
    return h.GetPtr()

def plotVar2d(varX, varY, xbins, xmin, xmax, xtitle, ybins, ymin, ymax, ytitle, cut, useFakes=False, setLogZ=False):
    can = ROOT.TCanvas("can","")
    if cut != "": df_cut = segs_df.Filter(cut)
    else: df_cut = segs_df
    if useFakes: 
        df_cut = segsFake_df.Filter(cut)
    seg_type = "allSegs_" if cut=="" else ("S4_" if "nhit==4" in cut else "S3_") #TODO: make this a func...
    name = seg_type+varY+"_vs_"+varX
    if (useFakes): name += "_fakes"
    h = df_cut.Histo2D(("h_"+name, "", xbins, xmin, xmax, ybins, ymin, ymax), varX, varY)
    h = h.GetPtr()
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.Draw("colz")
    if (setLogZ): can.SetLogz(1)
    can.SetRightMargin(0.12)
    can.SetLeftMargin(0.12)
    can.SetBottomMargin(0.11)
    if (save): can.SaveAs(args.output+"_"+name+".pdf")
    h.Write()
    return

def plotROCCurve(var, xbins, xmin, xmax, xtitle, cut):
    g_roc = ROOT.TGraph()
    seg_type = "allSegs_" if cut=="" else ("S4_" if "nhit==4" in cut else "S3_")
    name = seg_type+var

    h_FPR = segsFake_df.Filter(cut).Histo1D(("h_FPR_"+name, xtitle, xbins, xmin, xmax), var).GetPtr()
    h_FPR.Scale(1./h_FPR.GetEntries())

    h_TPR = segs_df.Filter(cut).Histo1D(("h_TPR_"+name, xtitle, xbins, xmin, xmax), var).GetPtr()
    h_TPR.Scale(1./h_TPR.GetEntries())

    for i in range(1, xbins):
        # print("Bin {}, var_FPR_value {}, var_TPR_value {}, FPR {:.2f}, TPR {:.2f}, h_FPR_integral {:.2f}, h_TPR_integral {:.2f}"\
            # .format(i, h_FPR.GetBinLowEdge(i+1), h_TPR.GetBinLowEdge(i+1), 1.0 * h_FPR.Integral(0, i)/xbins, \
                    # 1.0 * h_TPR.Integral(0, i)/xbins, h_FPR.Integral(0, i),  h_TPR.Integral(0, i)))

        # g_roc.SetPoint(g_roc.GetN(), 1.0 * h_FPR.Integral(0, i)/(xbins*h_FPR.GetEntries()), 1.0 * h_TPR.Integral(0, i)/(xbins*h_TPR.GetEntries()))
        g_roc.SetPoint(g_roc.GetN(), 1.0 * h_FPR.Integral(0, i)/xbins, 1.0 * h_TPR.Integral(0, i)/xbins)

    g_roc.SetName("g_roc_"+name)
    g_roc.SetTitle(name.replace("_", " ")+", ("+str(xmin)+" - "+str(xmax)+")")
    g_roc.GetXaxis().SetTitle("False positive rate")
    g_roc.GetYaxis().SetTitle("True positive rate")
    g_roc.Write()
    can = ROOT.TCanvas("can","")
    g_roc.Draw("ALP*")
    if (save): can.SaveAs(args.output+"_ROC_"+name+".pdf")
    return

# TODO: change to just take h1/h2 instead
def plotFakeRatioPlotForVars(var, xbins, xmin, xmax, xtitle, cut):
    if cut != "":
         df_cut = segs_df.Filter(cut)
         df_fake_cut = segsFake_df.Filter(cut)
    else: 
        df_cut = segs_df
        df_fake_cut = segsFake_df

    c = ROOT.TCanvas("c1", "canvas", 800, 800)
    h_mc_reco = df_cut.Histo1D(("h_mc_reco", xtitle, xbins, xmin, xmax), var)
    h_fake_reco = df_fake_cut.Histo1D(("h_fake_reco", xtitle, xbins, xmin, xmax), var)

    h2 = h_mc_reco.GetPtr()
    h1 = h_fake_reco.GetPtr()

    h1.SetLineColor(ROOT.kRed)
    h1.SetLineWidth(2)
    h1.GetYaxis().SetTitleSize(20)
    h1.GetYaxis().SetTitleFont(43)
    h1.GetYaxis().SetTitleOffset(1.55)
    h1.SetStats(0)

    h2.SetLineColor(ROOT.kBlue+1)
    h2.SetLineWidth(2)

    h3 = h1.Clone("h3")
    h3.SetLineColor(ROOT.kBlack)
    h3.SetMarkerStyle(21)
    h3.SetTitle("")
    h3.SetMinimum(0.0)
    h3.SetMaximum(2.0)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)

    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("ratio fake/reco")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)

    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)

    
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    # draw everything
    pad1.cd()
    h1.SetMinimum(1)
    h2.SetMinimum(1)
    pad1.SetLogy()
    if (h2.GetMaximum()>h1.GetMaximum()):
        h2.Draw("same")
        h1.Draw("same")
        h1.GetYaxis().SetLabelSize(0.0)
    else:
        h1.Draw("same")
        h2.Draw("same")
        h2.GetYaxis().SetLabelSize(0.0)
        
    # to avoid clipping the bottom zero, redraw a small axis
    # axis = ROOT.TGaxis(-5, 20, 5, 200, 20, 220, 510, "")
    # axis.SetLabelFont(43)
    # axis.SetLabelSize(15)
    # axis.Draw()

    seg_type = "_allSegs_" if cut=="" else ("_S4_" if cut=="nhit==4" else "_S3_")

    leg = ROOT.TLegend(0.72,0.72,0.77,0.82)
    leg.AddEntry(h2,"True "+seg_type.strip("_")+" tracks","l")
    leg.AddEntry(h1,"Fake "+seg_type.strip("_")+" tracks","l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.025)
    leg.Draw()
    ROOT.gPad.Update()

    pad2.cd()
    h3.Draw("ep")
    
    if (save): c.SaveAs(args.output + seg_type+"fake_ratio_"+var+".pdf")
    del c
    return
    # input()

def plotRatioPlotForVarsBothSegs(var, xbins, xmin, xmax, xtitle):
    df_cut_S4 = segs_df.Filter("nhit==4")
    df_fake_cut_S4 = segsFake_df.Filter("nhit==4")
    df_cut_S3 = segs_df.Filter("nhit==3")
    df_fake_cut_S3 = segsFake_df.Filter("nhit==3")

    c = ROOT.TCanvas("c1", "canvas", 800, 800)

    h_mc_reco_S4 = df_cut_S4.Histo1D(("h_mc_reco_S4", xtitle, xbins, xmin, xmax), var).GetPtr()
    h_fake_reco_S4 = df_fake_cut_S4.Histo1D(("h_fake_reco_S4", xtitle, xbins, xmin, xmax), var).GetPtr()

    h_mc_reco_S3 = df_cut_S3.Histo1D(("h_mc_reco_S3", xtitle, xbins, xmin, xmax), var).GetPtr()
    h_fake_reco_S3 = df_fake_cut_S3.Histo1D(("h_fake_reco_S3", xtitle, xbins, xmin, xmax), var).GetPtr()

    h_fake_reco_S4.SetLineColor(46)
    h_fake_reco_S4.SetLineWidth(2)
    h_fake_reco_S4.GetYaxis().SetTitleSize(20)
    h_fake_reco_S4.GetYaxis().SetTitleFont(43)
    h_fake_reco_S4.GetYaxis().SetTitleOffset(1.55)
    h_fake_reco_S4.SetStats(0)

    h_fake_reco_S3.SetLineColor(ROOT.kRed)
    h_fake_reco_S3.SetLineWidth(2)
    h_fake_reco_S3.GetYaxis().SetTitleSize(20)
    h_fake_reco_S3.GetYaxis().SetTitleFont(43)
    h_fake_reco_S3.GetYaxis().SetTitleOffset(1.55)
    h_fake_reco_S3.SetStats(0)

    h_mc_reco_S4.SetLineColor(9)
    h_mc_reco_S4.SetLineWidth(2)

    h_mc_reco_S3.SetLineColor(ROOT.kBlue+1)
    h_mc_reco_S3.SetLineWidth(2)

    hRealRatio = h_mc_reco_S3.Clone("hRealRatio")
    hRealRatio.SetLineColor(ROOT.kBlack)
    hRealRatio.SetMarkerColor(ROOT.kBlue+2)
    hRealRatio.SetMarkerStyle(21)
    hRealRatio.SetTitle("")
    hRealRatio.SetMinimum(0.0)
    hRealRatio.SetMaximum(5.0)
    # Set up plot for markers and errors
    hRealRatio.Sumw2()
    hRealRatio.SetStats(0)
    hRealRatio.Divide(h_mc_reco_S4)

    hFakeRatio = h_fake_reco_S3.Clone("h3")
    hFakeRatio.SetLineColor(ROOT.kBlack)
    hFakeRatio.SetMarkerColor(ROOT.kRed+3)
    hFakeRatio.SetMarkerStyle(20)
    hFakeRatio.SetTitle("")
    hFakeRatio.SetMinimum(0.0)
    hFakeRatio.SetMaximum(5.0)
    # Set up plot for markers and errors
    hFakeRatio.Sumw2()
    hFakeRatio.SetStats(0)
    hFakeRatio.Divide(h_mc_reco_S4)

    # hFakeRatio.SetMaximum(hFakeRatio.GetMaximum())
    # hRealRatio.SetMaximum(hFakeRatio.GetMaximum())

    # Adjust y-axis settings
    y = hFakeRatio.GetYaxis()
    y.SetTitle("Ratio")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)

    # Adjust x-axis settings
    x = hFakeRatio.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)

    hRealRatio.GetXaxis().SetLabelSize(0.0)
    hRealRatio.GetYaxis().SetLabelSize(0.0)

    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.3)
    pad2.SetGridx()
    pad2.Draw()

    # draw everything
    pad1.cd()
    pad1.SetLogy()
    list_of_hists = [h_mc_reco_S4, h_fake_reco_S4, h_mc_reco_S3, h_fake_reco_S3]
    list_of_hists = sorted(list_of_hists, key=lambda x: x.GetMaximum(), reverse=True)

    for idx, hist in enumerate(list_of_hists):
        hist.SetMinimum(1)
        hist.Draw("same")
        if (idx!=0): hist.GetYaxis().SetLabelSize(0.0)

    leg = ROOT.TLegend(0.62,0.72,0.67,0.86)
    leg.AddEntry(h_mc_reco_S4," True S4 tracks","l")
    leg.AddEntry(h_fake_reco_S4," Fake S4 tracks","l")
    leg.AddEntry(h_mc_reco_S3," True S3 tracks","l")
    leg.AddEntry(h_fake_reco_S3," Fake S3 tracks","l")
    leg.AddEntry(hRealRatio, " True S3 / True S4 ratio", "p")
    leg.AddEntry(hFakeRatio, " Fake S3 / True S4 ratio", "p")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.025)
    leg.Draw()
    ROOT.gPad.Update()

    pad2.cd()
    hFakeRatio.Draw("ep")
    hRealRatio.Draw("ep SAME")
    
    if (save): c.SaveAs(args.output+"_combined_fake_ratio_"+var+".pdf")
    del c
    return
    # input()

def plotPtEffVsVar(var, xbins, xmin, xmax, xtitle, \
            ybins, ymin, ymax, ytitle, \
            segs_mc_cut, segs_cut):

    can = ROOT.TCanvas("can","")

    dfMC_cut = segsMC_df.Filter(segs_mc_cut)
    df_cut = segs_df.Filter(segs_cut)

    h_ = dfMC_cut.Histo2D(("heff_", "", xbins, xmin, xmax, ybins, ymin, ymax), var, "pt")
    h  = df_cut.Histo2D(("heff", "", xbins, xmin, xmax, ybins, ymin, ymax), var, "pt")
    
    h.Divide(h_.GetPtr())
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.GetZaxis().SetTitle("Efficiency")
    h.GetZaxis().SetRangeUser(0,1)
    can.SetRightMargin(0.12)
    can.SetLeftMargin(0.11)
    can.SetBottomMargin(0.11)
    h.Draw("colz")
    ROOT.gPad.Update()
    # ROOT.gPad.SetLogy(1)
    eff = "S4_" if segs_cut=="nhit==4" else "S3_"
    prefix = args.output + "_PtVsVarEff_" + eff
    if (save): can.SaveAs(prefix+var+".pdf")
    return
    # input()

def plotMomEffVsVar(var, xbins, xmin, xmax, xtitle, \
            ybins, ymin, ymax, ytitle, \
            segs_mc_cut, segs_cut):

    can = ROOT.TCanvas("can","")

    dfMC_cut = segsMC_df.Filter(segs_mc_cut)
    df_cut = segs_df.Filter(segs_cut)

    h_ = dfMC_cut.Histo2D(("heff_", "", xbins, xmin, xmax, ybins, ymin, ymax), var, "mc_p")
    h  = df_cut.Histo2D(("heff", "", xbins, xmin, xmax, ybins, ymin, ymax), var, "mc_p")
    
    h.Divide(h_.GetPtr())
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.GetZaxis().SetTitle("Efficiency")
    h.GetZaxis().SetRangeUser(0,1)
    can.SetRightMargin(0.12)
    can.SetLeftMargin(0.11)
    can.SetBottomMargin(0.11)
    h.Draw("colz")
    ROOT.gPad.Update()
    # ROOT.gPad.SetLogy(1)
    eff = "S4_" if segs_cut=="nhit==4" else "S3_"
    prefix = args.output + "_MomVsVarEff_" + eff
    if (save): can.SaveAs(prefix+var+".pdf")
    return
    # input()

def plotPullPlot(var, mc_var, xbins, xmin, xmax, xtitle, cut, mode="resid", err=""):
    seg_type = "allSegs_" if cut=="" else ("S4_" if "nhit==4" in cut else "S3_")

    if (mode == "resid"): df_cut = segs_df.Filter(cut).Define("resid", var + "-" + mc_var)
    else: df_cut = segs_df.Filter(cut).Define("pull", "({} - {})/{}".format(var, mc_var, err))

    min = df_cut.Min(var).GetValue() if xmin=="" else xmin
    max = df_cut.Max(var).GetValue() if xmax=="" else xmax

    residuals = df_cut.AsNumpy(columns=[mode])
    # print(residuals)

    # x = ROOT.RooRealVar("x", var + " - " + mc_var, min, max)

    # sigma = ROOT.RooRealVar("sigma", "sigma", 3, 0.1, 10)
    # mean = ROOT.RooRealVar("mean", "mean", 0, xmin, xmax)
    # gauss = ROOT.RooGaussian("gauss", "gauss", x, mean, sigma)

    # data = ROOT.RooDataSet("ds", "ds", ROOT.RooArgSet(x))

    h = ROOT.TH1D("h_"+mode+"_"+seg_type+var, xtitle, xbins, xmin, xmax)

    for i in residuals[mode]:
        # print(i)
        # x.setVal(i)
        # data.add(ROOT.RooArgSet(x))
        h.Fill(i)

    # gauss.fitTo(data)
    # data = segs_cut.Book(ROOT.std.move(ROOT.RooDataSetHelper("dataset", "ds", ROOT.RooArgSet(x))), (var)) #v26?

    # frame1 = x.frame(ROOT.RooFit.Title(xtitle))
    # data.plotOn(frame1, ROOT.RooFit.Rescale(1./len(residuals["res"])))
    # gauss.plotOn(frame1)
            
    c = ROOT.TCanvas("can", "", 900, 600)
    h.Draw()
    # frame1.Draw()
    # input()
    if (save): c.SaveAs(args.output+"_"+seg_type+var+"_"+mode+".pdf")
    h.Write()

def plotTeffForVar(var, xbins, xmin, xmax, xtitle, segs_mc_cut, segs_cut):
    df_cut = segs_df.Filter(segs_cut)
    dfMC_cut = segsMC_df.Filter(segs_mc_cut)

    can = ROOT.TCanvas("can","")
    hpass = df_cut.Histo1D(("hpass", "", xbins, xmin, xmax), var)
    # hpass.SetLineColor(ROOT.kBlue)
    # hpass.Draw()
    htotal = dfMC_cut.Histo1D(("htotal", "", xbins, xmin, xmax), var)
    # htotal.SetLineColor(ROOT.kRed)
    # htotal.Draw("SAME")
    # input()
    # print(hpass.GetEntries(), htotal.GetEntries())
    # print(hpass.GetNbinsX(), htotal.GetNbinsX())
    # pEff = ROOT.TEfficiency()
    # pEff.SetTotalHistogram(htotal.GetPtr(), "f")
    # pEff.SetPassedHistogram(hpass.GetPtr(), "f")
    if (ROOT.TEfficiency.CheckConsistency(hpass.GetPtr(), htotal.GetPtr())):
        # pEff = ROOT.TGraphAsymmErrors(hpass.GetPtr(), htotal.GetPtr(), "pois")
        pEff = ROOT.TEfficiency(hpass.GetPtr(), htotal.GetPtr())
        pEff.SetTitle(xtitle)
        pEff.Draw()
        # gr = pEff.GetPaintedGraph().GetYaxis().SetRangeUser(0., 1.05)
        eff = "S4_" if segs_cut=="nhit==4" else "S3_"
        prefix = args.output + "_eff_" + eff
        pEff.SetName(prefix+var)
        if (save): can.SaveAs(prefix+var+".pdf")
        pEff.Write()
        # input()
    else: print("Passed and total histos not consistent...")
    return

def stackHistos(hists, name, legs, colours=[], title="", setLog=False, xmin=0.0, xmax=0.0):
    can = ROOT.TCanvas("can", "can", 1200, 600)
    hs = ROOT.THStack ("hs", str(title))
    for h in hists: hs.Add(h)
    # hs.Add(h1)
    # hs.Add(h2)
    hs.Draw("NOSTACK")
    if (xmin!=0.0 and xmax!=0.0): hs.GetXaxis().SetLimits(xmin, xmax)

    for i, h in enumerate(hists):
        if len(colours):
            h.SetLineColor(colours[i])
            h.SetMarkerColor(colours[i])
        else:
            h.SetLineColor(i+2)
            h.SetMarkerColor(i+2)
        h.SetLineWidth(1)

    # h1.SetLineColor(ROOT.kRed)
    # h1.SetMarkerColor(ROOT.kRed)
    # h1.SetLineWidth(1)

    # h2.SetLineColor(ROOT.kBlue)
    # h2.SetMarkerColor(ROOT.kBlue)
    # h2.SetLineWidth(1) 

    if (setLog):
        for i in hists: h.SetMinimum(1)
        # h1.SetMinimum(1)
        # h2.SetMinimum(2)

    leg = ROOT.TLegend(0.80,0.75,0.85,0.85)
    for i, h in enumerate(hists): leg.AddEntry(h, legs[i], "l")
    # leg.AddEntry(h1,leg1,"l")
    # leg.AddEntry(h2,leg2,"l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.Draw()
    
    if (setLog): ROOT.gPad.SetLogy(1) 
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    can.SaveAs(name)
    return

def getLayer(sensor):
    return int(sensor)>>10 & 0x1F

def countS3LayerPatterns(tree):
    L101_pattern = 0
    L001_pattern = 0
    L100_pattern = 0
    comparePattern = lambda layers1, layers2 : len([i for i, j in zip(layers1, layers2) if i == j])==3
    for entry in tree:
        if (tree.nhit==4): continue
        layers = [tree.layers[i] for i in range(len(tree.layers))]
        if (comparePattern(layers, [1,0,1])): L101_pattern += 1
        if (comparePattern(layers, [0,0,1])): L001_pattern += 1
        if (comparePattern(layers, [1,0,0])): L100_pattern += 1
    return L101_pattern, L001_pattern, L100_pattern

# TODO: break down further by L101_1st, L101_2nd...
def getS3EffForLayerPattern():
    L101_pattern_reco, L001_pattern_reco, L100_pattern_reco = countS3LayerPatterns(segs)
    L101_pattern_MC, L001_pattern_MC, L100_pattern_MC = countS3LayerPatterns(segs_mc)
    print(L101_pattern_reco, L001_pattern_reco, L100_pattern_reco)
    print(L101_pattern_MC, L001_pattern_MC, L100_pattern_MC)
    eff = lambda rec, mc : 100.0 * rec / float(mc)
    print("S3 Effs by pattern: L101 {:.2f} %, L001 {:.2f} %, L100 {:.2f} %".format(eff(L101_pattern_reco, L101_pattern_MC), \
                                                                                   eff(L001_pattern_reco, L001_pattern_MC), \
                                                                                   eff(L100_pattern_reco, L100_pattern_MC)))

p_res = 1000
min_p = -40000 #MeV
max_p = 40000
# max_p = ROOT.RDataFrame(segs, {"p"})
# max_p = int(max_p.Max().GetValue())
# min_p = ROOT.RDataFrame(segs, {"p"})
# min_p = int(min_p.Min().GetValue())
print("min p ", min_p, " max_p ", max_p)
p_bins = int((max_p-min_p)/p_res)

ROOT.gSystem.cd("graphs") 
output_file = ROOT.TFile(args.output+".root", 'RECREATE')

# getS3EffForLayerPattern()

# plotFakeRatioPlotForVars("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "")
# plotFakeRatioPlotForVars("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "")
# plotFakeRatioPlotForVars("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "")
# plotFakeRatioPlotForVars("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "")
# plotFakeRatioPlotForVars("p", p_bins, min_p, max_p, ";p [MeV/#it{c}];Counts", "")

# plotFakeRatioPlotForVars("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "nhit==3")
# plotFakeRatioPlotForVars("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "nhit==3")
# plotFakeRatioPlotForVars("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "nhit==3")
# plotFakeRatioPlotForVars("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "nhit==3")
plotFakeRatioPlotForVars("abs_p", 60, 0, 60000, ";abs(p) [MeV/#it{c}];Counts", "nhit==3")
plotFakeRatioPlotForVars("chi2", 32, 0, 32, ";#chi^{2};Counts", "nhit==3")

# plotFakeRatioPlotForVars("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "nhit==4")
# plotFakeRatioPlotForVars("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "nhit==4")
# plotFakeRatioPlotForVars("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "nhit==4")
# plotFakeRatioPlotForVars("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "nhit==4")
plotFakeRatioPlotForVars("abs_p", 60, 0, 60000, ";abs(p) [MeV/#it{c}];Counts", "nhit==4")
plotFakeRatioPlotForVars("chi2", 32, 0, 32, ";#chi^{2};Counts", "nhit==4")

plotROCCurve("abs_p", 60, 0, 60000, "", "nhit==3")
plotROCCurve("chi2", 32, 0, 32, "", "nhit==3")

plotROCCurve("abs_p", 60, 0, 60000, "", "nhit==4")
plotROCCurve("chi2", 32, 0, 32, "", "nhit==4")

# plotRatioPlotForVarsBothSegs("p", p_bins, min_p, max_p, ";p [MeV/#it{c}];Counts")
plotRatioPlotForVarsBothSegs("abs_p", 60, 0, 60000, ";abs(p) [MeV/#it{c}];Counts")
plotRatioPlotForVarsBothSegs("chi2", 32, 0, 32, ";#chi^{2};Counts")

# plotVar2d("p", "mc_p", p_bins, min_p, max_p, "Reco p [MeV/#it{c}]", p_bins, 0, max_p, "MC p [MeV/#it{c}]", "")
# plotVar2d("p", "mc_p", p_bins, min_p, max_p, "Reco p [MeV/#it{c}]", p_bins, 0, max_p, "MC p [MeV/#it{c}]", "nhit==4")
# plotVar2d("p", "mc_p", p_bins, min_p, max_p, "Reco p [MeV/#it{c}]", p_bins, 0, max_p, "MC p [MeV/#it{c}]", "nhit==3")

plotVar2d("chi2", "abs_p", 32, 0, 32, "#chi^{2}", 60, 0, 60000, "Reco abs(p) [MeV/#it{c}]", "nhit==4", setLogZ=True)
plotVar2d("chi2", "abs_p", 32, 0, 32, "#chi^{2}", 60, 0, 60000, "Reco abs(p) [MeV/#it{c}]", "nhit==4", useFakes=True, setLogZ=True)

plotVar2d("chi2", "abs_p", 32, 0, 32, "#chi^{2}", 60, 0, 60000, "Reco abs(p) [MeV/#it{c}]", "nhit==3", setLogZ=True)
plotVar2d("chi2", "abs_p", 32, 0, 32, "#chi^{2}", 60, 0, 60000, "Reco abs(p) [MeV/#it{c}]", "nhit==3", useFakes=True, setLogZ=True)

# plotVar("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "nhit==4")
# plotVar("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "nhit==4")
# plotVar("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "nhit==4")
# plotVar("abs_zpca_r", 32, 0, 32, ";abs(zpca_r) [mm];Counts", "nhit==4")
# plotVar("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "nhit==4")
# plotVar("p", p_bins, min_p, max_p, ";p [MeV/#it{c}];Counts", "nhit==4")
# plotVar("chi2", 32, 0, 32, ";#chi^{2};Counts", "nhit==4")

# plotVar("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "nhit==3")
# plotVar("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "nhit==3")
# plotVar("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "nhit==3")
# plotVar("abs_zpca_r", 32, 0, 32, ";abs(zpca_r) [mm];Counts", "nhit==3")
# plotVar("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "nhit==3")
# plotVar("p", p_bins, min_p, max_p, ";p [MeV/#it{c}];Counts", "nhit==3")
# plotVar("chi2", 32, 0, 32, ";#chi^{2};Counts", "nhit==3")

# plotVar("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Counts", "")
# plotVar("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Counts", "")
# plotVar("zpca_z", 50, -100, 100, ";z0 [mm];Counts", "")
# plotVar("abs_zpca_r", 32, 0, 32, ";abs(zpca_r) [mm];Counts", "")
# plotVar("zpca_r", 64, -32, 32, ";DCA [mm];Counts", "")
# plotVar("mc_p", p_bins, min_p, max_p, ";MC p [MeV/#it{c}];Counts", "")
# plotVar("chi2", 32, 0, 32, ";#chi^{2};Counts", "")

# h_S4_p = plotVar("p", p_bins, min_p, max_p, ";Reco p [MeV/#it{c}];Counts", "nhit==4")
# h_S4_mc_p = plotVar("mc_p", p_bins, min_p, max_p, ";MC p [MeV/#it{c}];Counts", "nhit==4")
# stackHistos(h_S4_p, h_S4_mc_p, args.output+"_mc_p_and_reco_p_overlay.pdf", leg1="S4 reco p", leg2="S4 mc p", title=";p [MeV/#it{c}]; Counts", setLog=True)
# stackHistos([h_S4_p, h_S4_mc_p], args.output+"_S4_mc_p_and_reco_p_overlay.pdf", ["S4 reco p", "S4 mc p"], title=";p [MeV/#it{c}]; Counts", setLog=True)

# h_S3_p = plotVar("p", p_bins, min_p, max_p, ";Reco p [MeV/#it{c}];Counts", "nhit==3")
# h_S3_mc_p = plotVar("mc_p", p_bins, min_p, max_p, ";MC p [MeV/#it{c}];Counts", "nhit==3")
# stackHistos(h_S3_p, h_S3_mc_p, args.output+"_mc_p_and_reco_p_overlay.pdf", leg1="S3 reco p", leg2="S3 mc p", title=";p [MeV/#it{c}]; Counts", setLog=True)
# stackHistos([h_S3_p, h_S3_mc_p], args.output+"_S3_mc_p_and_reco_p_overlay.pdf", ["S3 reco p", "S3 mc p"], title=";p [MeV/#it{c}]; Counts", setLog=True)

# stackHistos([h_S3_p, h_S3_mc_p, h_S4_p, h_S4_mc_p], args.output+"_S3&S4_mc_p_and_reco_p_overlay.pdf", ["S3 reco p", "S3 mc p", "S4 reco p", "S4 mc p"], colours=[ROOT.kOrange, ROOT.kBlue, ROOT.kSpring, ROOT.kCyan], title=";p [MeV/#it{c}]; Counts", setLog=True)

# h_S4_p_zoom = plotVar("p", 200, -10000, 10000, ";Reco p [MeV/#it{c}];Counts", "nhit==4")
# h_S4_mc_p_zoom = plotVar("mc_p", 200, -10000, 10000, ";MC p [MeV/#it{c}];Counts", "nhit==4")

# h_S3_p_zoom = plotVar("p", 200, -10000, 10000, ";Reco p [MeV/#it{c}];Counts", "nhit==3")
# h_S3_mc_p_zoom = plotVar("mc_p", 200, -10000, 10000, ";MC p [MeV/#it{c}];Counts", "nhit==3")
# stackHistos([h_S3_p_zoom, h_S3_mc_p_zoom, h_S4_p_zoom, h_S4_mc_p_zoom], args.output+"_S3&S4_mc_p_and_reco_p_overlay.pdf", ["S3 reco p", "S3 mc p", "S4 reco p", "S4 mc p"], colours=[ROOT.kOrange, ROOT.kBlue, ROOT.kSpring, ROOT.kCyan], title=";p [MeV/#it{c}]; Counts", setLog=True, xmin=-10000, xmax=10000)

# plotPullPlot("p", "mc_p", p_bins, min_p, max_p, "Residual momentum for S3's;p - mc_p [MeV/#it{c}]; Counts", "nhit==3&&mc_p!=0&&p!=0")
# plotPullPlot("p", "mc_p", p_bins, min_p, max_p, "Residual momentum for S4's;p - mc_p [MeV/#it{c}]; Counts", "nhit==4&&mc_p!=0&&p!=0")

# plotPullPlot("p", "mc_p", 200, -1, 1, "Pull momentum for S3's;(p - mc_p)/p_err; Counts", "nhit==3&&mc_p!=0&&p!=0", mode="pull", err="perr")
# plotPullPlot("p", "mc_p", 200, -1, 1, "Pull momentum for S4's;(p - mc_p)/p_err; Counts", "nhit==4&&mc_p!=0&&p!=0", mode="pull", err="perr")

# plotTeffForVar("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Efficiency", "nhit==4", "nhit==4")
# plotTeffForVar("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Efficiency", "nhit==4", "nhit==4")
# plotTeffForVar("zpca_z", 50, -100, 100, ";z0 [mm];Efficiency", "nhit==4", "nhit==4")
# plotTeffForVar("abs_zpca_r", 32, 0, 32, ";abs(zpca_r) [mm];Efficiency", "nhit==4", "nhit==4")
# plotTeffForVar("zpca_r", 64, -32, 32, ";DCA [mm];Efficiency", "nhit==4", "nhit==4")

# plotTeffForVar("mc_phi", 32, -3.2, 0.0, ";#phi [rad];Efficiency", "nhit==3", "nhit==3")
# plotTeffForVar("mc_theta", 32, 0.0, 3.2, ";#theta [rad];Efficiency", "nhit==3", "nhit==3")
# plotTeffForVar("zpca_z", 50, -100, 100, ";z0 [mm];Efficiency", "nhit==3", "nhit==3")
# plotTeffForVar("abs_zpca_r", 32, 0, 32, ";abs(zpca_r) [mm];Efficiency", "nhit==3", "nhit==3")
# plotTeffForVar("zpca_r", 64, -32, 32, ";DCA [mm];Efficiency", "nhit==3", "nhit==3")

# plotMomEffVsVar("mc_theta", 32, 0, 3.2, "#theta [rad]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==4", "nhit==4")
# plotMomEffVsVar("mc_phi", 32, -3.2, 0.0, "#phi [rad]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==4", "nhit==4")
# plotMomEffVsVar("zpca_r", 64, -32, 32, "DCA [mm]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==4", "nhit==4")
# plotMomEffVsVar("zpca_z", 50, -100, 100, "z0 [mm]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==4", "nhit==4")

# plotMomEffVsVar("mc_theta", 32, 0, 3.2, "#theta [rad]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==3", "nhit==3")
# plotMomEffVsVar("mc_phi", 32, -3.2, 0.0, "#phi [rad]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==3", "nhit==3")
# plotMomEffVsVar("zpca_r", 64, -32, 32, "DCA [mm]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==3", "nhit==3")
# plotMomEffVsVar("zpca_z", 50, -100, 100, "z0 [mm]", p_bins, min_p, max_p, "p [MeV/#it{c}]", "nhit==3", "nhit==3")

output_file.Write()
output_file.Close()
