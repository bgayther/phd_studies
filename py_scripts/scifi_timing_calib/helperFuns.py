import numpy as np
import ROOT
from scipy.stats import sem
import tabulate

fb_sids = 12 # number of fb sensors
frame_ns = 50 # frame length used
fb_resolution = 0.01 #10 ps
fbs = xrange(fb_sids)
all_sensors = [sid for sid in fbs]


def selection(event):
    if (event.n != 6 or event.mc_prime != 1 or event.fbc_t2 == 0 or event.fbc_t1 == 0 or event.mc_pid != -11 or -1 * event.mc_p / event.r < 0 \
        or abs(event.fbc_t2 - event.fbc_t1) < fb_resolution): 
        return False
    else:
        return True


def countTracks(events, nevents):
    track_counter = 0
    for entry in xrange(nevents):
        events.GetEntry(entry)
        if selection(events) == False: continue
        track_counter += 1
    return track_counter

# Most up to date python version of algorithm
def findOffsets(events, nevents, mc_offsets_dict, window, t_diff_window_off, t_diff_window_ma, t_diff_adj_factor):
    curAvg = [0.0 for i in fbs]
    ctr = [0.0 for i in fbs]
    offset_data = [[] for i in fbs]
    offsets_list = [0.0 for i in fbs]
    sid_has_been = [False for i in fbs]
    alpha = 1.0/window
    # could create list of offset plots here and return at end of func

    for entry in xrange(nevents):
        events.GetEntry(entry)
        if selection(events) == False: continue

        fbc_dt = events.fbc_t2 - events.fbc_t1 
        path_length = events.s01[5] - events.s20[0] 

        path_length_per_c = path_length / 300.0 # units of mm/ns, ns overall
        t_diff = fbc_dt - path_length_per_c
        t1_sid = events.fbc_t1_sid
        t2_sid = events.fbc_t2_sid

        #### T_DIFF_ADJ CALCULATION ####
        if ctr[t2_sid] <= window:
            t_2 = events.fbc_t2 - offsets_list[t2_sid]
        else:
            t_2 = events.fbc_t2 - curAvg[t2_sid] 

        if ctr[t1_sid] <= window:
            t_1 = events.fbc_t1 - offsets_list[t1_sid]
        else:
            t_1 = events.fbc_t1 - curAvg[t1_sid] 

        t_diff_adj = (t_2 - t_1) - path_length_per_c

        #### FACTOR TO ADJUST T_DIFF_ADJ FOR LATER WHEN UPDATING OFFSET LIST ####
        if ctr[t2_sid] <= window + window/20.0:
            adj_t2 = t_diff_adj / t_diff_adj_factor
        else:
            adj_t2 = t_diff_adj * t_diff_adj_factor
            # adj_t2 = t_diff_adj / t_diff_adj_factor

        if ctr[t1_sid] <= window + window/20.0:
            adj_t1 = t_diff_adj / t_diff_adj_factor
        else:
            adj_t1 = t_diff_adj * t_diff_adj_factor
            # adj_t1 = t_diff_adj / t_diff_adj_factor

        #### OFFSET UPDATING ####

        # T2
        passed_t2 = False
        window_t2_off = offsets_list[t2_sid] - t_diff_window_off < abs(offsets_list[t2_sid] + adj_t2) < offsets_list[t2_sid] + t_diff_window_off
        window_t2_ma = curAvg[t2_sid] - t_diff_window_ma < abs(curAvg[t2_sid] + adj_t2) < curAvg[t2_sid] + t_diff_window_ma

        if ctr[t2_sid] <= window and window_t2_off:
            passed_t2 = True
            offsets_list[t2_sid] = offsets_list[t2_sid] + adj_t2

        if ctr[t2_sid] > window and window_t2_ma:
            passed_t2 = True
            offsets_list[t2_sid] = curAvg[t2_sid] + adj_t2

        # T1
        passed_t1 = False
        window_t1_off = offsets_list[t1_sid] - t_diff_window_off < abs(offsets_list[t1_sid] - adj_t1) < offsets_list[t1_sid] + t_diff_window_off
        window_t1_ma = curAvg[t1_sid] - t_diff_window_ma < abs(curAvg[t1_sid] - adj_t1) < curAvg[t1_sid] + t_diff_window_ma

        if ctr[t1_sid] <= window and window_t1_off:
            passed_t1 = True
            offsets_list[t1_sid] = offsets_list[t1_sid] - adj_t1

        if ctr[t1_sid] > window and window_t1_ma:
            passed_t1 = True
            offsets_list[t1_sid] = curAvg[t1_sid] - adj_t1


        #### MOVING AVERAGE CALCULATIONS ####
        ## T2
        if passed_t2:

            if sid_has_been[t2_sid]:
                # curAvg[t2_sid] = (alpha * offsets_list[t2_sid]) + (1.0 - alpha)*curAvg[t2_sid]
                curAvg[t2_sid] = curAvg[t2_sid] + (offsets_list[t2_sid] - curAvg[t2_sid])/(ctr[t2_sid]-window+1)

            if ctr[t2_sid] == window:
                sid_has_been[t2_sid] = True 
                # print "Cur avg ", curAvg[t2_sid], " \n"
                curAvg[t2_sid] = np.mean(offset_data[t2_sid])
                # print "Cur avg ", curAvg[t2_sid], " sum ", sum(offset_data[t2_sid]), " size ", len(offset_data[t2_sid]), " \n"

            offset_data[t2_sid].append(offsets_list[t2_sid]) 
            ctr[t2_sid]+=1
        
        ## T1
        if passed_t1:

            if sid_has_been[t1_sid]:
                # curAvg[t1_sid] = (alpha * offsets_list[t1_sid]) + (1.0 - alpha)*curAvg[t1_sid]
                curAvg[t1_sid] = curAvg[t1_sid] + (offsets_list[t1_sid] - curAvg[t1_sid])/(ctr[t1_sid]-window+1)

            if ctr[t1_sid] == window:
                sid_has_been[t1_sid] = True 
                curAvg[t1_sid] = np.mean(offset_data[t1_sid])

            offset_data[t1_sid].append(offsets_list[t1_sid]) 
            ctr[t1_sid]+=1
            
        #### END OF CALCULATION ####

    offsets_map, sigmas = printResults(curAvg, offset_data, mc_offsets_dict)
    print "Window size (first N tracks used) ", window
    print "T_diff range accepted (within window) ", t_diff_window_off 
    print "T_diff range accepted (after window) ",t_diff_window_ma 
    print "T_diff adjustment factor ", t_diff_adj_factor
    # print "Raw cmas ", curAvg

    return offsets_map, sigmas


def printResults(curAvg, offset_data, mc_offsets_dict):
    expected_mean_overall = np.array(list(mc_offsets_dict.values())).mean()
    found_mean_overall = np.mean(curAvg)
    offsets_map = {}
    table = []
    sigmas = []
    for sid in all_sensors:
        rel_offset = curAvg[sid] - found_mean_overall
        expected_offset = mc_offsets_dict.get(sid) - expected_mean_overall
        err = sem(offset_data[sid], axis=None, ddof=0) 
        sigma = (rel_offset - expected_offset)/err
        offsets_map[sid] = rel_offset
        sigmas.append(abs(sigma))
        table.append([sid, rel_offset, expected_offset, err, sigma])

    print(tabulate.tabulate(table, ["sensor","CMA offset","expected offset","error","sigma"], tablefmt='fancy_grid',floatfmt=".3f"))
    print "Mean of found offfsets ", found_mean_overall
    print "Mean of expected offsets ", expected_mean_overall
    print "Sum of sigmas ", sum(sigmas)
    return offsets_map, sigmas


def correctForOffsets(events, nevents, offsets_map, found_offset_sensors):
    h_dt_all = ROOT.TH2D("h_dt_all", "All;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
    h_dt_all_corrected = ROOT.TH2D("h_dt_all_corrected", "Corrected;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
 
    h_dt_unadjusted = ROOT.TH2D("h_dt_unadjusted", "Unadjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
    h_dt_both_adjusted = ROOT.TH2D("h_dt_both_adjusted", "Both adjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)
    h_dt_one_adjusted = ROOT.TH2D("h_dt_one_adjusted", "One adjusted;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10)

    h_t_diff = ROOT.TH1D("h_t_diff", ";t_diff;count", 200, -frame_ns/5, frame_ns/5)
    h_t_diff_corrected = ROOT.TH1D("h_t_diff_corrected", ";t_diff;count", 200, -frame_ns/5, frame_ns/5)

    for entry in xrange(nevents):
        events.GetEntry(entry)
        if selection(events) == False: continue

        t2 = events.fbc_t2
        t1 = events.fbc_t1
        t1_sid = events.fbc_t1_sid
        t2_sid = events.fbc_t2_sid

        if t2 < 0.0 or t1 < 0.0: continue # would require some frame fixing I think
        
        fbc_dt = t2 - t1 

        path_length = events.s01[5] - events.s20[0] 
        path_length_per_c = path_length / 300.0 # units of mm/ns, ns overall
        t_diff = fbc_dt - path_length_per_c

        h_t_diff.Fill(t_diff)
        h_dt_all.Fill(path_length, fbc_dt)

        if t1_sid in offsets_map:
            t1 -= offsets_map.get(t1_sid)
        if t2_sid in offsets_map: 
            t2 -= offsets_map.get(t2_sid)

        fbc_dt = t2 - t1 
        if abs(fbc_dt) < fb_resolution: continue 

        t_diff = fbc_dt - path_length_per_c

        h_dt_all_corrected.Fill(path_length, fbc_dt)
        h_t_diff_corrected.Fill(t_diff)

        # if len(found_offset_sensors) >= 1:
        #     if (t1_sid and t2_sid not in found_offset_sensors):
        #         h_dt_unadjusted.Fill(path_length, fbc_dt)

        #     both = False
        #     if (t1_sid and t2_sid in found_offset_sensors):
        #         both = True
        #         h_dt_both_adjusted.Fill(path_length, fbc_dt)

        #     if (t1_sid or t2_sid in found_offset_sensors and not both):
        #         h_dt_one_adjusted.Fill(path_length, fbc_dt)

    can_dt_all = ROOT.TCanvas("can_dt_all", "can_dt_all", 1200, 600)
    can_dt_all.Divide(2)

    can_dt_all.cd(1)

    line_dt_all = ROOT.TF1("line_dt_all","[0]+[1]*x",90,990)
    h_dt_all.Fit("line_dt_all", "Q N")
    h_dt_all.Draw("colz")
    line_dt_all.Draw("same")

    can_dt_all.cd(2)
    line_dt_all_corrected = ROOT.TF1("line_dt_all_corrected","[0]+[1]*x",90,990)
    h_dt_all_corrected.Fit("line_dt_all_corrected", "Q N")
    h_dt_all_corrected.Draw("colz")
    line_dt_all_corrected.Draw("same")

    can_dt_all.SaveAs("dt_all_corrected_line.pdf")

    ROOT.gPad.Update()

    can_t_diff = ROOT.TCanvas("can_t_diff", "can_t_diff", 1300, 600)
    can_t_diff.Divide(2)

    can_t_diff.cd(1)
    gauss_t_diff = ROOT.TF1("gauss_t_diff", "gaus", -10,10)
    h_t_diff.Fit("gauss_t_diff", "Q N")
    h_t_diff.Draw()
    # gauss_t_diff.Draw("same")

    can_t_diff.cd(2)
    gauss_t_diff_corrected = ROOT.TF1("gauss_t_diff_corrected", "gaus", -10,10)
    h_t_diff_corrected.Fit("gauss_t_diff_corrected", "Q N")
    h_t_diff_corrected.Draw()
    # gauss_t_diff_corrected.Draw("same")

    can_t_diff.SaveAs("t_diff_corrected_gauss.pdf")

    ROOT.gPad.Update()
