/*
 *  scifiTimingCalib.h
 *
 *  Created on: July, 2020
 *      Author: Ben Gayther <benjamin.gayther.15@ucl.ac.uk>
 */

#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <TList.h>
#include <TFile.h>
#include <TChain.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

#include <map>

#ifndef MU3E_CALIB_SCIFI_TIMING_H_
#define MU3E_CALIB_SCIFI_TIMING_H_

using std::vector;
using std::map;

class scifiTimingCalib {
public:

    TTreeReader     fReader;
    TList*          fOutput;
    TFile*          out_file;
    std::string     out_name;

    bool  truth_mode; 
    // TODO: Get following from relevant .json's
    int   fb_sids  = 12;   // number of fb sensors
    int   frame_ns = 50;   // frame length used
    // float fb_res   = 0.25; // from trirec_default.conf (not used currently)

    TTreeReaderValue<Int_t>     n = {fReader, "n"};
    TTreeReaderValue<Int_t>     mc = {fReader, "mc"};
    TTreeReaderValue<Int_t>     mc_prime = {fReader, "mc_prime"};
    TTreeReaderValue<Int_t>     mc_pid = {fReader, "mc_pid"};
    TTreeReaderValue<Int_t>     fbc_t1_sid = {fReader, "fbc_t1_sid"};
    TTreeReaderValue<Int_t>     fbc_t2_sid = {fReader, "fbc_t2_sid"};
    TTreeReaderValue<Float_t>   r = {fReader, "r"};
    TTreeReaderValue<Float_t>   mc_p = {fReader, "mc_p"};
    TTreeReaderValue<Float_t>   fbc_t1 = {fReader, "fbc_t1"};
    TTreeReaderValue<Float_t>   fbc_t2 = {fReader, "fbc_t2"};
    TTreeReaderArray<Float_t>   s01 = {fReader, "s01"};
    TTreeReaderArray<Float_t>   s20 = {fReader, "s20"};

    map<int, double> mc_map; // Map of offsets added to each fb sensor in reconstruction
    double expec_mean; 

    vector<double>          MA{vector<double>(fb_sids, 0.0)};
    vector<double>          ctr{vector<double>(fb_sids, 0.0)};
    vector<double>          all_ctr{vector<double>(fb_sids, 0.0)};
    vector<bool>            sid_has_been{vector<bool>(fb_sids, false)};
    vector<double>          offsets_list{vector<double>(fb_sids, 0.0)};
    vector<vector<double>>  offset_data{vector<vector<double>>(fb_sids)};

    TH1D* h_t_diff;
    TH1D* h_t_diff_adj;
    TH2D* h_dt_all;
    TH1D* h_residual;
    TH1D* h_pull;

    vector<TH1D*>   h_ma_offset_vec;
    vector<TGraph*> g_offset_trk_no_vec;
    vector<TGraph*> g_offset_MA_trk_no_vec;

    void    makeHistos();
    void    addHistos();
    bool    cut();
    double  avg (vector<double>& v) const;
    double  range(vector<double>& v) const;
    double  stdDev(vector<double>& v) const;
    void    findOffsets(unsigned int window, double range_ma, double range_off);
    void    plot(int t1_sid, int t2_sid);
    void    plotErrors();
    void    printResults();
    void    plotMAOffsets();
    void    reset();
    void    plotWindowResults(vector<unsigned int>& windows, double range_ma, double range_off); 
    void    plotMARangeResults(unsigned int window, vector<double>& range_mas, double range_off); 
    void    plotOffRangeResults(unsigned int window, double range_ma, vector<double>& range_offs); 

    vector<double>  getOffsets() {return MA;}

    bool    checkTruthMode(){
        if (!truth_mode) {
            printf("Not in truth mode!\n"); 
            return false;
        } else {
            // printf("In truth mode!\n"); 
            return true;
        }
    }

    void    setMCMap(map<int, double> offsets) {
        mc_map = offsets;
        double sum = 0;
        for(auto it_map = mc_map.begin(); it_map != mc_map.end(); ++it_map) sum += it_map->second;
        expec_mean = sum/fb_sids;
    }

    void    intialise(std::string name, TChain* tc) {
        out_name = name;
        auto out = name + ".root";
        printf("Output file %s\n", out.c_str());
        fOutput = new TList();
        fOutput->SetName(out_name.c_str());
        makeHistos();
        addHistos();
        out_file = new TFile(out.c_str(), "RECREATE");
        tc->LoadTree(0);
        fReader.SetTree(tc);
    }

    // Truth mode ctor
    scifiTimingCalib(std::string name, TChain* tc, map<int, double> offsets) {
        truth_mode = true;
        setMCMap(offsets);
        intialise(name, tc);
    }

    // Non truth mode ctor
    scifiTimingCalib(std::string name, TChain* tc) {
        truth_mode = false;
        intialise(name, tc);
    }

    ~scifiTimingCalib() {
        printf("Writing to file...\n");
        if (out_file) out_file->cd();
        if (fOutput) fOutput->Write();
        out_file->Close();
    }
    
};

#endif