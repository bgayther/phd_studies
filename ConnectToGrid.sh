#Sourcing the dirac api 
source /unix/muons/users/bgayther/dirac_ui/diracos/diracosrc

#Linking to proxy - Needs to be done every time connecting to the grid
dirac-proxy-init -x
dirac-configure -F -S GridPP -C dips://dirac01.grid.hep.ph.ic.ac.uk:9135/Configuration/Server -I
dirac-proxy-init -g mu3e.org_user -M

