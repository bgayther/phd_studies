FROM ubuntu:18.04

SHELL ["/bin/bash", "-c"]

ARG ROOT_VERSION=6.26.02
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install dependencies and python
RUN apt-get update && \
    apt-get install -y \
    locales tmux software-properties-common build-essential mesa-common-dev curl vim \
    git subversion dpkg-dev g++ gcc binutils libx11-dev libssl-dev\
    wget libeigen3-dev libexpat-dev libxerces-c-dev libgtkmm-3.0-dev libmysqlclient-dev libcurl4-openssl-dev\
    libxpm-dev libxft-dev libxext-dev libpng-dev libjpeg-dev libtbb-dev libtiff5 x11-apps xauth libxml2-dev libxmu-dev sudo \
    xlibmesa-glu-dev libqt5opengl5-dev \
    gfortran xterm qt5-default libxss-dev libxxf86vm-dev libxkbfile-dev libxv-dev &&\
    rm -rf /var/lib/apt/lists/*

RUN sudo apt update && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | sudo apt-key add - && \
    sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
    sudo apt install -y cmake

RUN sudo apt update && sudo apt-add-repository ppa:deadsnakes/ppa && sudo apt update && \
    sudo apt install -y python3.9 python3.9-dev python3.9-tk python3.9-distutils && curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3.9 get-pip.py && rm get-pip.py && pip3.9 install --upgrade pip setuptools && \
    # make some useful symlinks that are expected to exist
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3.9 /usr/bin/python; fi && \
    if [[ ! -e /usr/bin/python-config ]]; then ln -sf /usr/bin/python3.9-config /usr/bin/python-config; fi && \
    if [[ ! -e /usr/bin/pip ]]; then ln -sf /usr/bin/pip3.9 /usr/bin/pip; fi

RUN TABULATE_INSTALL=lib-only pip3.9 install tabulate
RUN pip3.9 install --upgrade pip &&\
    pip3.9 install numpy scipy argparse matplotlib tqdm Flask flask-cors sympy plotly statsmodels

RUN git clone -b geant4-11.0-release https://gitlab.cern.ch/geant4/geant4.git \
    && cd /geant4 && mkdir geant4-build && cd geant4-build \
    && cmake -DCMAKE_INSTALL_PREFIX=/usr/local/geant4 \
            -DGEANT4_INSTALL_DATA=ON \
            -DGEANT4_USE_OPENGL_X11=ON \
            -DCMAKE_CXX_STANDARD=17 \
            -DGEANT4_USE_QT=ON \
            -DQT_QMAKE_EXECUTABLE=/usr/lib/qt5/bin/qmake \
            -DCMAKE_BUILD_TYPE=Release ../ \ 
    && make -j8 && make install \
    && source /usr/local/geant4/bin/geant4.sh \
    && cd .. && rm -rf geant4-build && cd .. && rm -rf geant4

RUN curl -O https://root.cern.ch/download/root_v${ROOT_VERSION}.source.tar.gz && \
    tar -xzf root_v${ROOT_VERSION}.source.tar.gz && rm root_v${ROOT_VERSION}.source.tar.gz &&\
    mkdir build-root && cd build-root && \
    cmake -DCMAKE_INSTALL_PREFIX="/usr/local/root" \
        -Dcxx17="ON" \
        -Dlibcxx="ON" \
        -Dmysql="OFF" \
        -Doracle="OFF" \
        -Dpgsql="OFF" \
        -Dpythia6="OFF" \
        -Dpythia8="OFF" \
        -Droofit="ON" \
        -Droot7="OFF" \
        -Dsqlite="OFF" \
        -Dtmva="OFF" \
        -Dtmva-cpu="OFF" \
        -Dtmva-gpu="OFF" \
        -Dtmva-pymva="OFF" \
        -Dvc="OFF" \
        -Dvdt="OFF" \
        -Dx11="ON" \
        -Dxrootd="OFF" \
        -Dpython3="ON" \
        -Dxml="ON" \
        -Dbuiltin_vdt="ON" \
        -DPython3_EXECUTABLE="/usr/bin/python3.9" \
        -DCMAKE_BUILD_TYPE=Release ../root-${ROOT_VERSION} && \
    make -j8 && make install &&\
    # cmake --build . --target install &&\
    cd ../ && source /usr/local/root/bin/thisroot.sh && rm -rf root-${ROOT_VERSION} && rm -rf build-root

RUN cd /home && wget http://downloads.sourceforge.net/project/boost/boost/1.77.0/boost_1_77_0.tar.gz \
  && tar xfz boost_1_77_0.tar.gz \
  && rm boost_1_77_0.tar.gz \
  && cd boost_1_77_0 \
  && ./bootstrap.sh --prefix=/usr/local --with-libraries=program_options,system,filesystem,thread,date_time \
  && ./b2 install -j8\
  && cd /home \
  && rm -rf boost_1_77_0

RUN mkdir $HOME/online && cd $HOME/online && echo "Mu3e /root/online uname" > exptab

# RUN curl -fsSL https://deb.nodesource.com/setup_current.x | sudo -E bash - && sudo apt-get install -y nodejs && npm install -g http-server 

RUN mkdir $HOME/packages && cd $HOME/packages && git clone https://bitbucket.org/tmidas/midas --recursive && \
    cd midas && mkdir build && cd build && source /usr/local/root/bin/thisroot.sh && cmake .. && make -j8 && make install -j8 

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && sudo apt install -y ./google-chrome-stable_current_amd64.deb && rm google-chrome-stable_current_amd64.deb

EXPOSE 8080
EXPOSE 5050

COPY ./install.entrypoint.sh /
RUN chmod +x /install.entrypoint.sh
ENTRYPOINT ["/install.entrypoint.sh"]
# CMD     ["/bin/bash"]
