import ROOT

ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h")

class Colour(int):
    """Create a new ROOT.TColour object with an associated index"""
    # __slots__ = ["object", "name"]

    def __new__(cls, r, g, b, name=""):
        self = int.__new__(cls, ROOT.TColor.GetFreeColorIndex())
        self.object = ROOT.TColor(self, r, g, b, name, 1.0)
        self.name = name
        return self

colours = [
    Colour(0.12156862745098039, 0.4666666666666667, 0.7058823529411765, "kC0"),
    Colour(1.0, 0.4980392156862745, 0.054901960784313725, "kC1"),
    Colour(0.17254901960784313, 0.6274509803921569, 0.17254901960784313, "kC2"),
    Colour(0.8392156862745098, 0.15294117647058825, 0.1568627450980392, "kC3"),
    Colour(0.5803921568627451, 0.403921568627451, 0.7411764705882353, "kC4"),
    Colour(0.5490196078431373, 0.33725490196078434, 0.29411764705882354, "kC5"),
    Colour(0.8901960784313725, 0.4666666666666667, 0.7607843137254902, "kC6"),
    Colour(0.4980392156862745, 0.4980392156862745, 0.4980392156862745, "kC7"),
    Colour(0.7372549019607844, 0.7411764705882353, 0.13333333333333333, "kC8"),
    Colour(0.09019607843137255, 0.7450980392156863, 0.8117647058823529, "kC9"),
]

for Colour in colours:
    setattr(ROOT, Colour.name, Colour)

ColourList = [ROOT.kC0, ROOT.kC1, ROOT.kC2, ROOT.kC3, ROOT.kC4, ROOT.kC5, ROOT.kC6, ROOT.kC7, ROOT.kC8, ROOT.kC9]