from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)

from DIRAC.Interfaces.API.Job import Job
from DIRAC.Interfaces.API.Dirac import Dirac

dirac = Dirac()

import datetime
t = datetime.datetime.now()

import pickle
import subprocess

filesByType = pickle.load(open("files.pickle", "rb"))

JobName = f"SensHists_{t.strftime('%d')}{t.strftime('%m')}{t.strftime('%y')}"

def submit_job(mode, seeds, JobName, ncpu):
    # print(f"Setting up job for seeds: {seeds}")
    j = Job()
    outputFileName = f"{mode}" + f"_{seeds[0]}" + f"_{seeds[-1]}"
    myenv = {
        "seeds": seeds,
        "mode": mode,
        "ncpu": ncpu,
        "outputFileName": outputFileName,
    }
    j.setExecutionEnv(myenv)
    # j.setCPUTime(600)

    files = []
    genFiles = []
    for seed in seeds:
        if mode == "IC":
            files.append(f"/mu3e.org/prod/build4.4.3/IC/run{seed:06d}-ana_vertexfit.root")
        elif mode == "Sig":
            files.append(f"/mu3e.org/prod/build4.4.3/Signal/run{seed:06d}-ana_vertexfit.root")
            genFiles.append(f"/mu3e.org/prod/build4.4.3/Signal/signalnorm_{seed:06d}.txt")
        elif mode == "BhabhaMichel":
            files.append(f"/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_{seed:06d}.root")
            # genFiles.append(f"/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_{seed:06d}.txt")

    if mode == "Sig":
        assert(len(files) == len(genFiles))

    # print(files, len(files))
    # print(genFiles, len(genFiles))
    j.setInputData(files if mode != "Sig" else files + genFiles)
    if ncpu > 1: 
        j.setNumberOfProcessors(ncpu)
    j.setInputSandbox(["SensitivityHistos.py"])
    j.setExecutable("GRIDRun.sh")

    FullJobName = JobName + f"_{mode}" + f"_{seeds[0]}" + f"_{seeds[-1]}"
    # use command dirac-dms-remove-files to remove files from SE
    # subprocess.run(["dirac-dms-remove-files", f"{StorageLocation}/{outputFileName}.root"])

    j.setOutputData([f"{FullJobName}.log", outputFileName+".root"], outputSE="UKI-NORTHGRID-LIV-HEP-disk", outputPath="SensHists")    
    j.setName(FullJobName)

    result = dirac.submitJob(j)
    jobid = result["JobID"]
    print(f"Submitted job {jobid} for mode {mode} and seeds {seeds[0]} to {seeds[-1]}")

    joblog = open(f"{JobName}_{mode}.txt", "a")
    joblog.write(str(jobid) + "\n")
    joblog.close()

def submit_skim_job(mode, seeds, JobName, ncpu):
    # print(f"Setting up job for seeds: {seeds}")
    j = Job()
    outputFileName = f"{mode}_skim" + f"_{seeds[0]}" + f"_{seeds[-1]}"
    myenv = {
        "seeds": seeds,
        "mode": mode,
        "ncpu": ncpu,
        "outputFileName": outputFileName,
    }
    j.setExecutionEnv(myenv)
    # j.setCPUTime(600)

    files = []
    genFiles = []
    for seed in seeds:
        if mode == "IC":
            files.append(f"/mu3e.org/prod/build4.4.3/IC/run{seed:06d}-ana_vertexfit.root")
        elif mode == "Sig":
            files.append(f"/mu3e.org/prod/build4.4.3/Signal/run{seed:06d}-ana_vertexfit.root")
            genFiles.append(f"/mu3e.org/prod/build4.4.3/Signal/signalnorm_{seed:06d}.txt")
        elif mode == "BhabhaMichel":
            files.append(f"/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_{seed:06d}.root")
            # genFiles.append(f"/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_{seed:06d}.txt")

    if mode == "Sig":
        assert(len(files) == len(genFiles))

    # print(files, len(files))
    # print(genFiles, len(genFiles))
    j.setInputData(files if mode != "Sig" else files + genFiles)
    if ncpu > 1: 
        j.setNumberOfProcessors(ncpu)
    j.setInputSandbox(["BDT/prepareData.py"])
    j.setExecutable("GRIDSkimRun.sh")

    FullJobName = JobName + f"_{mode}" + f"_{seeds[0]}" + f"_{seeds[-1]}"
    # use command dirac-dms-remove-files to remove files from SE
    # subprocess.run(["dirac-dms-remove-files", f"{StorageLocation}/{outputFileName}.root"])

    j.setOutputData([f"{FullJobName}.log", outputFileName+".parquet"], outputSE="UKI-NORTHGRID-LIV-HEP-disk", outputPath="skim_files")    
    j.setName(FullJobName)

    result = dirac.submitJob(j)
    jobid = result["JobID"]
    print(f"Submitted job {jobid} for mode {mode} and seeds {seeds[0]} to {seeds[-1]}")

    joblog = open(f"{JobName}_{mode}.txt", "a")
    joblog.write(str(jobid) + "\n")
    joblog.close()

InputDataLimit = 100
Sig_seed_start = 1
Sig_seed_end = 1000
IC_seed_start = 1
IC_seed_end = 5000
BhabhaMichel_seed_start = 500000
BhabhaMichel_seed_end = 532000
ncpu = 1

Sig_seeds = list(range(Sig_seed_start, Sig_seed_end+1))
IC_seeds = list(range(IC_seed_start, IC_seed_end+1))
BhabhaMichel_seeds = list(range(BhabhaMichel_seed_start, BhabhaMichel_seed_end+1))

# Check if files are in filesByType (need to check for all modes, and 2 files per Sig seed), and remove them from the list
for mode, seeds in zip(["Sig", "IC", "BhabhaMichel"], [Sig_seeds, IC_seeds, BhabhaMichel_seeds]):
    # print(f"Checking {mode} files")

    seeds_to_remove = []
    for seed in seeds:
        if mode == "Sig":
            if f"/mu3e.org/prod/build4.4.3/Signal/run{seed:06d}-ana_vertexfit.root" not in filesByType[mode] or f"/mu3e.org/prod/build4.4.3/Signal/signalnorm_{seed:06d}.txt" not in filesByType[mode]:
                seeds_to_remove.append(seed)
        elif mode == "IC":
            if f"/mu3e.org/prod/build4.4.3/{mode}/run{seed:06d}-ana_vertexfit.root" not in filesByType[mode]:
                seeds_to_remove.append(seed)
        elif mode == "BhabhaMichel":
            if f"/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_{seed:06d}.root" not in filesByType[mode]:
                seeds_to_remove.append(seed)
    
    for seed in sorted(seeds_to_remove, reverse=True):
        seeds.remove(seed)

IC_seed_groups = [IC_seeds[i:i + InputDataLimit] for i in range(0, len(IC_seeds), InputDataLimit)]
sig_seed_groups = [Sig_seeds[i:i + int(InputDataLimit/2)] for i in range(0, len(Sig_seeds), int(InputDataLimit/2))] 
bhabhaMichel_seed_groups = [BhabhaMichel_seeds[i:i + InputDataLimit] for i in range(0, len(BhabhaMichel_seeds), InputDataLimit)]

# submit_job("BhabhaMichel", bhabhaMichel_seed_groups[0], JobName, ncpu)
# for seeds in bhabhaMichel_seed_groups:
    # submit_job("BhabhaMichel", seeds, JobName, ncpu)

# submit_job("IC", IC_seed_groups[0], JobName, ncpu)
# for seeds in IC_seed_groups:
    # submit_job("IC", seeds, JobName, ncpu)

# submit_job("Sig", sig_seed_groups[0], JobName, ncpu)
# for seeds in sig_seed_groups:
#     submit_job("Sig", seeds, JobName, ncpu)

# For BDT
ncpu = 8
JobName = f"SkimData_{t.strftime('%d')}{t.strftime('%m')}{t.strftime('%y')}"
# submit_skim_job("BhabhaMichel", bhabhaMichel_seed_groups[0], JobName, ncpu)
# submit_skim_job("IC", IC_seed_groups[0], JobName, ncpu)
# submit_skim_job("Sig", sig_seed_groups[0], JobName, ncpu)
# for seeds in sig_seed_groups:
#     submit_skim_job("Sig", seeds, JobName, ncpu)
# for seeds in bhabhaMichel_seed_groups:
#     submit_skim_job("BhabhaMichel", seeds, JobName, ncpu)
# for seeds in IC_seed_groups:
#     submit_skim_job("IC", seeds, JobName, ncpu)

print("Done")