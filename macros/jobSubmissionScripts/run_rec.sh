source /unix/muons/users/bgayther/setup.sh
source /unix/muons/users/bgayther/mu3e/build/mu3e-activate.sh

mkdir -p /tmp/job_$PBS_JOBID
cd /tmp/job_$PBS_JOBID
mkdir results

# OUTDIR_ADJ=$OUTDIR/rec_${SAMPLE}_${RUN}_${SIM_TAG}_${REC_TAG}/job_$PBS_JOBID
if [ -z "$SAMPLE" ]; then
    OUTDIR_ADJ=$OUTDIR/rec_${RUN}_${SIM_TAG}_${REC_TAG}/job_$PBS_JOBID
else
    OUTDIR_ADJ=$OUTDIR/rec_${SAMPLE}_${RUN}_${SIM_TAG}_${REC_TAG}/job_$PBS_JOBID
fi
mkdir -p $OUTDIR_ADJ

echo "******************** RECONSTRUCTING SIM FILE ${SIM_FILE}, RUN ${RUN}, SAMPLE ${SAMPLE}, SIM_TAG ${SIM_TAG}, REC TAG ${REC_TAG}, OUTDIR ${OUTDIR_ADJ}, REC CONF ${REC_CONF_OPT} ********************"
start=$(date +%s.%N)

cp /unix/muons/users/bgayther/mu3e/run/trirec.conf trirec.conf
echo "
.include \"./trirec.conf\"
$REC_CONF_OPT
" > trirec_${REC_TAG}.conf

echo "================= RUNNING TRIREC ==============="
mu3eTrirec --conf trirec_${REC_TAG}.conf --input ${SIM_FILE} --output results/mu3e_trirec_${RUN}_${SIM_TAG}_${REC_TAG}.root

end=$(date +%s.%N)
runtime=$(python -c "print(${end} - ${start})")

echo "Runtime was $runtime"

cp -r * $OUTDIR_ADJ

rm -rf /tmp/job_$PBS_JOBID
