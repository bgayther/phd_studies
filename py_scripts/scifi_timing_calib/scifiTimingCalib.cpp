/*
 *  scifiTimingCalib.cpp
 *  
 *  Class to find relative offsets in fibre detector. 
 *  See collaboration meeting in July 2020, to see my talk on
 *  this method.
 *
 *  Created on: July, 2020
 *      Author: Ben Gayther <benjamin.gayther.15@ucl.ac.uk>
 */

#include "scifiTimingCalib.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLegend.h>

#include <iostream>
#include <stdio.h>
#include <numeric>
#include <string> 
#include <cmath> 
#include <time.h>


void scifiTimingCalib::makeHistos() 
{
    gStyle->SetOptStat(1111);
    gROOT->SetBatch(true);

    h_t_diff = new TH1D("h_t_diff", ";t_diff [ns];count", 200, -frame_ns/5, frame_ns/5); 
    h_t_diff_adj = new TH1D("h_t_diff_adj", ";t_diff [ns];count", 200, -frame_ns/5, frame_ns/5); 
    h_dt_all = new TH2D("h_dt_all", ";(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -10, 10);
    if (checkTruthMode()) {
        // h_residual = new TH1D("h_residual",";residual [ns]; count",200,-0.05,0.05); 
        h_residual = new TH1D("h_residual","",200,-1.01,1.01); 
        h_pull = new TH1D("h_pull","",200,-3.5,3.5);
    }

    for (int sid = 0; sid < fb_sids; sid++) {
        auto sid_name = std::to_string(sid);

        auto offset_gr = new TGraph();
        auto offset_name = "offset_"+sid_name+"_trk_no";
        offset_gr->SetName(offset_name.c_str());
        auto offset_title = "Fb sensor "+sid_name+";track number;Offset [ns]";
        offset_gr->SetTitle(offset_title.c_str());
        g_offset_trk_no_vec.push_back(offset_gr);

        auto ma_offset_gr = new TGraph();
        auto ma_offset_name = "offset_"+sid_name+"_moving_avg_trk_no";
        ma_offset_gr->SetName(ma_offset_name.c_str());
        auto ma_offset_title = "Fb sensor "+sid_name+";track number;MA [ns]";
        ma_offset_gr->SetTitle(ma_offset_title.c_str());
        g_offset_MA_trk_no_vec.push_back(ma_offset_gr);

        auto offset_hist_name = "h_ma_offset_"+sid_name;
        // auto offset_hist = new TH1D(offset_hist_name.c_str(),"",100,-10,10);
        auto offset_hist = new TH1D(offset_hist_name.c_str(),"",100,-1.0,1.0);
        h_ma_offset_vec.push_back(offset_hist);
    }
}

void scifiTimingCalib::addHistos() 
{
    fOutput->AddAll(gDirectory->GetList());
    for (auto& gr : g_offset_trk_no_vec)
        fOutput->Add(gr);
    for (auto& gr : g_offset_MA_trk_no_vec)
        fOutput->Add(gr);
}

bool scifiTimingCalib::cut()
{
    if (*n != 6 || *mc != 1 || *fbc_t2 == 0 || *fbc_t1 == 0 || *mc_pid != -11  || *r > 0 \
        /* || -1.0 * (*mc_p / *r) < 0 */ /* || std::abs(*fbc_t2 - *fbc_t1) < fb_res */) {
        return false;
    }
    else {
        return true;
    }
}

double scifiTimingCalib::avg(vector<double>& v) const 
{    
    return (std::accumulate(v.cbegin(),v.cend(),0.0)/ v.size());
}

double scifiTimingCalib::range(vector<double>& v) const
{    
    auto it_max = std::max_element(v.begin(), v.end());
    auto it_min = std::min_element(v.begin(), v.end());
    return (*it_max - *it_min);
}

double scifiTimingCalib::stdDev(vector<double>& v) const
{
    auto mean = avg(v);
    std::vector<double> diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size());
    return stdev;
}

void scifiTimingCalib::findOffsets(unsigned int window, double range_ma, double range_off) 
{
    double t_diff_div_factor = 2.0;
    // double alpha = 1.0/100000;

    int i = 0;
    while (fReader.Next()){
        if (!cut()) continue;
        if (i%100000==0) printf("%d entries processed\n",i);
        i++;

        auto fbc_dt =  *fbc_t2 - *fbc_t1;
        auto path_length = s01[5] - s20[0]; 
        auto path_length_per_c = path_length / 300.0; // units of mm/ns, ns overall

        h_dt_all->Fill(path_length, fbc_dt);

        auto t_diff = fbc_dt - path_length_per_c;
        h_t_diff->Fill(t_diff);

        auto t1_sid = *fbc_t1_sid; 
        auto t2_sid = *fbc_t2_sid;

        double t_2 = 0.0;
        double t_1 = 0.0; 

        //// T_DIFF_ADJ CALCULATION ////
        if (!sid_has_been[t2_sid]) {
            t_2 = *fbc_t2 - offsets_list[t2_sid];
        } else {
            t_2 = *fbc_t2 - MA[t2_sid]; 
        }   
        if (!sid_has_been[t1_sid]) {
            t_1 = *fbc_t1 - offsets_list[t1_sid];
        } else { 
            t_1 = *fbc_t1 - MA[t1_sid]; 
        }

        //// T_DIFF_ADJ CALCULATION ////
        auto t_diff_adj = (t_2 - t_1) - path_length_per_c;
        h_t_diff_adj->Fill(t_diff_adj);
        
        //// T_DIFF_DIV_FACTOR ////
        double adj_t2 = 0.0;
        double adj_t1 = 0.0;

        if (!sid_has_been[t2_sid]){
            adj_t2 = t_diff_adj / t_diff_div_factor;
        } else {
            adj_t2 = t_diff_adj / t_diff_div_factor*2;
        }
        if (!sid_has_been[t1_sid]){
            adj_t1 = t_diff_adj / t_diff_div_factor;
        } else {
            adj_t1 = t_diff_adj / t_diff_div_factor*2;
        }

        //// T2 RANGE CHECKS ////
        bool passed_t2 = false;
        bool window_t2_off = offsets_list[t2_sid] - range_off < (offsets_list[t2_sid] + adj_t2) && (offsets_list[t2_sid] + adj_t2) < offsets_list[t2_sid] + range_off;
        bool window_t2_ma = MA[t2_sid] - range_ma < (MA[t2_sid] + adj_t2)  &&  (MA[t2_sid] + adj_t2) < MA[t2_sid] + range_ma;

        //// OFFSET UPDATING T2 ////
        if (window_t2_off && !sid_has_been[t2_sid]){
            passed_t2 = true;
            offsets_list[t2_sid] = offsets_list[t2_sid] + adj_t2;
        }
        else if (window_t2_ma){
            passed_t2 = true;
            offsets_list[t2_sid] = MA[t2_sid] + adj_t2;
        }
        
        //// T1 RANGE CHECKS ////
        bool passed_t1 = false;
        bool window_t1_off = offsets_list[t1_sid] - range_off < (offsets_list[t1_sid] - adj_t1) && (offsets_list[t1_sid] - adj_t1) < offsets_list[t1_sid] + range_off;
        bool window_t1_ma = MA[t1_sid] - range_ma < (MA[t1_sid] - adj_t1) && (MA[t1_sid] - adj_t1) < MA[t1_sid] + range_ma;

        //// OFFSET UPDATING T1 ////
        if (window_t1_off && !sid_has_been[t1_sid]){
            passed_t1 = true;
            offsets_list[t1_sid] = offsets_list[t1_sid] - adj_t1;
        }
        else if (window_t1_ma){
            passed_t1 = true;
            offsets_list[t1_sid] = MA[t1_sid] - adj_t1;
        }

        //// MOVING AVERAGE CALCULATIONS ////
        // T2
        if (passed_t2){

            if (sid_has_been[t2_sid]){
                MA[t2_sid] = MA[t2_sid] + (offsets_list[t2_sid] - MA[t2_sid])/(ctr[t2_sid]+1); 
                // MA[t2_sid] = (alpha * offsets_list[t2_sid]) + (1.0 - alpha) * MA[t2_sid]; // Expon weighted MA
            }

            if (ctr[t2_sid] == window && !sid_has_been[t2_sid]){
                sid_has_been[t2_sid] = true;
                auto mean = avg(offset_data[t2_sid]);
                // printf("SID %d, MEAN %.3f \n", t2_sid, mean);
                MA[t2_sid] = mean;
                ctr[t2_sid] = 0;
            }

            if (!sid_has_been[t2_sid]) offset_data[t2_sid].push_back(offsets_list[t2_sid]); 
            ctr[t2_sid]++;
            all_ctr[t2_sid]++;
        }
        // T1
        if (passed_t1){

            if (sid_has_been[t1_sid]){
                MA[t1_sid] = MA[t1_sid] + (offsets_list[t1_sid] - MA[t1_sid])/(ctr[t1_sid]+1);
                // MA[t1_sid] = (alpha * offsets_list[t1_sid]) + (1.0 - alpha) * MA[t1_sid]; // Expon weighted MA
            }

            if (ctr[t1_sid] == window && !sid_has_been[t1_sid]){
                sid_has_been[t1_sid] = true; 
                auto mean = avg(offset_data[t1_sid]);
                // printf("SID %d, MEAN %.3f \n", t1_sid, mean);
                MA[t1_sid] = mean;
                ctr[t1_sid] = 0;
            }

            if (!sid_has_been[t1_sid]) offset_data[t1_sid].push_back(offsets_list[t1_sid]); 
            ctr[t1_sid]++;
            all_ctr[t1_sid]++;
        }
        //// END ////    
        plot(t1_sid, t2_sid);
    }
    printf("Window size %d, range_ma %f, range_off %f, t_diff_div_factor %f \n", window, range_ma, range_off, t_diff_div_factor);
    if (checkTruthMode()) plotErrors();
    printResults();
}

void scifiTimingCalib::plot(int t1_sid, int t2_sid)
{
    auto gr_t1 = g_offset_trk_no_vec[t1_sid];
    gr_t1->SetPoint(gr_t1->GetN(), all_ctr[t1_sid], offsets_list[t1_sid]);

    auto gr_t2 = g_offset_trk_no_vec[t2_sid];
    gr_t2->SetPoint(gr_t2->GetN(), all_ctr[t2_sid], offsets_list[t2_sid]);

    auto ma_mean = avg(MA);

    if (sid_has_been[t1_sid]) {
        auto gr_ma_t1 = g_offset_MA_trk_no_vec[t1_sid];
        gr_ma_t1->SetPoint(gr_ma_t1->GetN(), ctr[t1_sid], MA[t1_sid] - ma_mean);

        auto h = h_ma_offset_vec[t1_sid];
        // h->Fill(offsets_list[t1_sid]);
        h->Fill(MA[t1_sid]);
    }
    if (sid_has_been[t2_sid]) {
        auto gr_ma_t2 = g_offset_MA_trk_no_vec[t2_sid];
        gr_ma_t2->SetPoint(gr_ma_t2->GetN(), ctr[t2_sid], MA[t2_sid] - ma_mean);

        auto h = h_ma_offset_vec[t2_sid];
        // h->Fill(offsets_list[t2_sid]);
        h->Fill(MA[t2_sid]);
    }
}

void scifiTimingCalib::plotErrors()
{
    double mean = avg(MA);
    for(int i = 0; i < fb_sids; i++){
        double res = (MA[i] - mean) - (mc_map[i] - expec_mean);
        h_residual->Fill(res);
    }
    auto err = h_residual->GetStdDev();
    printf("Error %.4f \n", err);
    for(int i = 0; i < fb_sids; i++){
        double res = (MA[i] - mean) - (mc_map[i] - expec_mean);
        h_pull->Fill(res/err);
    }
    // plotMAOffsets();
}

void scifiTimingCalib::printResults()
{
    double mean = avg(MA);
    double R = range(MA);

    printf("Mean of found offsets %.3f \n", mean);
    printf("Range of found offsets %.3f \n", R);

    if (truth_mode) {
        printf("Mean of expected offsets %.3f \n", expec_mean);
        auto err = h_residual->GetStdDev();
        printf("Sensor \t MA offset [ns] \t Expected [ns] \t Error [ns]\n");
        for(int i = 0; i < fb_sids; i++){
            printf("%d \t %+-.3f \t\t %+-.3f \t %.3f \n", i, (MA[i]-mean), (mc_map[i]-expec_mean), err);
        }
    } else {
        printf("Sensor \t MA offset \n");
        for(int i = 0; i < fb_sids; i++){
            printf("%d \t %+-.3f \n", i, (MA[i]-mean));
        }
    }
}

void scifiTimingCalib::plotMAOffsets() {
    if (!checkTruthMode()) return;
    auto ma_mean = avg(MA);

    for (int sid = 0; sid < fb_sids; sid++) {

        auto can = new TCanvas("can", "can", 1200, 600);
        auto gr = g_offset_MA_trk_no_vec[sid];
        auto expec_val = mc_map[sid]-expec_mean;
        auto obs_val = MA[sid] - ma_mean;
        auto zoom = 1.0;

        gr->GetHistogram()->SetMaximum(obs_val+zoom);         
        gr->GetHistogram()->SetMinimum(obs_val-zoom); 
        gr->Draw();

        // figure out how to get it from TGraph
        auto x_min = 0;
        auto x_max = ctr[sid];

        TLine* line = new TLine(x_min, expec_val, x_max, expec_val);
        line->SetLineColor(kRed);
        line->SetLineWidth(1);
        line->SetLineStyle(2);
        line->Draw();

        TLegend* leg=new TLegend(0.70,0.70,0.85,0.85);
        // leg->AddEntry(gr,"hpx","fp");
        auto leg_label = "Expected offset = "+std::to_string(expec_val)+" ns";
        leg->AddEntry(line,leg_label.c_str(),"l");        
        leg->SetBorderSize(0);  
        leg->SetTextFont(42);
        leg->SetTextSize(0.025);
        leg->Draw();

        gPad->Update();
        
        auto name = "Fb_sensor_" + std::to_string(sid) + "_MA_offset.png";
        can->SaveAs(name.c_str());
        delete can;
    }
}

void scifiTimingCalib::reset(){
    fReader.Restart();
    std::fill(MA.begin(), MA.end(), 0);
    std::fill(ctr.begin(), ctr.end(), 0);
    std::fill(all_ctr.begin(), all_ctr.end(), 0);
    std::fill(sid_has_been.begin(), sid_has_been.end(), false);
    std::fill(offsets_list.begin(), offsets_list.end(), 0);
    for(auto& elem : offset_data) std::fill(elem.begin(), elem.end(), 0);
    h_t_diff->Reset();
    h_t_diff_adj->Reset();
    h_dt_all->Reset();
    h_residual->Reset();
    h_pull->Reset();
    for (auto& h : h_ma_offset_vec)
        h->Reset();
    for (auto& gr : g_offset_trk_no_vec)
        gr->Set(0);
    for (auto& gr : g_offset_MA_trk_no_vec)
        gr->Set(0);
    fOutput->Clear();
}

void scifiTimingCalib::plotWindowResults(vector<unsigned int>& windows, double range_ma, double range_off) 
{   
    if (!checkTruthMode()) return;
    auto can_err_window = new TCanvas("can_err_window", "can_err_window", 1200, 600);
    auto gr = new TGraph();
    auto name = "err_vs_window_size";
    gr->SetName(name);
    auto title = "Error vs Window size (range_ma = "+std::to_string(range_ma)+", range_off = "+std::to_string(range_off)+"); Window size; Error [ns]";
    gr->SetTitle(title.c_str());

    for (auto window : windows) {
        findOffsets(window, range_ma, range_off);
        auto err = h_residual->GetStdDev();
        gr->SetPoint(gr->GetN(), window, err);
        reset();
    }
    fOutput->Add(gr);
    gr->Draw("AL*");
    can_err_window->SaveAs("err_vs_window_size.pdf");
}

void scifiTimingCalib::plotMARangeResults(unsigned int window, vector<double>& range_mas, double range_off) 
{   
    if (!checkTruthMode()) return;
    auto can_err_window = new TCanvas("can_err_range_ma", "can_err_range_ma", 1200, 600);
    auto gr = new TGraph();
    auto name = "err_vs_range_ma";
    gr->SetName(name);
    auto title = "Error vs range_ma (window size = "+std::to_string(window)+", range_off = "+std::to_string(range_off)+"); range_ma [ns]; Error [ns]";
    gr->SetTitle(title.c_str());

    for (auto range_ma : range_mas) {
        findOffsets(window, range_ma, range_off);
        auto err = h_residual->GetStdDev();
        gr->SetPoint(gr->GetN(), range_ma, err);
        reset();
    }
    fOutput->Add(gr);
    gr->Draw("AL*");
    can_err_window->SaveAs("err_vs_range_ma.pdf");
}

void scifiTimingCalib::plotOffRangeResults(unsigned int window, double range_ma, vector<double>& range_offs) 
{   
    if (!checkTruthMode()) return;
    auto can_err_window = new TCanvas("can_err_range_off", "can_err_range_off", 1200, 600);
    auto gr = new TGraph();
    auto name = "err_vs_range_off";
    gr->SetName(name);
    auto title = "Error vs range_off (window = "+std::to_string(window)+", range_ma = "+std::to_string(range_ma)+"); range_off [ns]; Error [ns]";
    gr->SetTitle(title.c_str());

    for (auto range_off : range_offs) {
        findOffsets(window, range_ma, range_off);
        auto err = h_residual->GetStdDev();
        gr->SetPoint(gr->GetN(), range_off, err);
        reset();
    }
    fOutput->Add(gr);
    gr->Draw("AL*");
    can_err_window->SaveAs("err_vs_range_off.pdf");
}

// Recreating numpy linspace in C++
template <typename T> std::vector<T> linspace(T start, T end, size_t N_steps) {
    T step_size = (end - start) / static_cast<T>(N_steps-1);
    vector<T> xs(N_steps);
    typename vector<T>::iterator x;
    T val;
    for (x = xs.begin(), val = start; x != xs.end(); ++x, val += step_size)
        *x = val;
    return xs;
}

int main(int argc, char* argv[]){
    clock_t tStart = clock();
    if (argc < 2) {
        std::cout << "Usage: scifiTimingCalib <input.root> <additional inputs.root>" << std::endl;
        return -1;
    }

    // gSystem->ChangeDirectory("/unix/muons/users/bgayther/testing_code/offset_reco_bkg_data/scenario_5");

    TChain* chain = new TChain("segs");
    for (int i = 1; i < argc; i++) {
        // TODO: If file doesnt exist should throw error and exit (Will do in mu3e/ repo)
        chain->Add(argv[i]);
        std::cout << "Added " << argv[i] << std::endl;
    }

    auto status = chain->LoadTree(0);
    if (status < 0){ printf("WARNING TCHAIN CANNOT BE READ...\n"); return -1;} // TODO: Use mu3e log for error message

    // Write this to the output .root file?
    unsigned int window = 1000;
    double range_ma = 100.0;
    double range_off = 100.0;

    // TODO: When moving this into mu3e/ repo, will need to add something for the offset per sensor in the config files...

    // Scenario_0 (no offsets)
    // std::map<int, double> mc_offsets_map = {{0, 0.0}, {1, 0.0}, {2, 0.0}, {3, 0.0}, {4, 0.0}, {5, 0.0}, \
    //                                         {6, 0.0}, {7, 0.0}, {8, 0.0}, {9, 0.0}, {10, 0.0}, {11, 0.0}};

    // Scenario_1
    // std::map<int, double> mc_offsets_map = {{0, 0.0}, {1, 0.1}, {2, 0.2}, {3, 0.3}, {4, 0.4}, {5, 0.5}, \
    //                                         {6, 0.6}, {7, 0.7}, {8, 0.8}, {9, 0.9}, {10, 1.0}, {11, 4.0}};

    // Scenario_2
    // std::map<int, double> mc_offsets_map = {{0, 0.05}, {1, 0.1}, {2, 0.2}, {3, 0.3}, {4, 0.4}, {5, 0.5}, \
    //                                         {6, 0.6}, {7, 0.7}, {8, 0.8}, {9, 0.9}, {10, 1.0}, {11, 4.0}};

    // Scenario_3
    // std::map<int, double> mc_offsets_map = {{0, 0.2}, {1, 0.2}, {2, 0.2}, {3, 0.2}, {4, 0.2}, {5, 0.2}, \
    //                                         {6, 0.2}, {7, 0.2}, {8, 0.2}, {9, 0.2}, {10, 0.2}, {11, 0.2}};

    // Scenario_4
    // std::map<int, double> mc_offsets_map = {{0, 0.03}, {1, 0.05}, {2, 0.1}, {3, 0.09}, {4, 0.01}, {5, 0.06}, \
    //                                         {6, 0.03}, {7, 0.06}, {8, 0.09}, {9, 0.10}, {10, 0.08}, {11, 0.07}};

    // Scenario_5
    std::map<int, double> mc_offsets_map = {{0, 3.53}, {1, 3.55}, {2, 3.80}, {3, 3.79}, {4, 4.01}, {5, 3.56}, \
                                            {6, 3.03}, {7, 3.36}, {8, 3.29}, {9, 3.10}, {10, 3.18}, {11, 3.67}};

    // scifiTimingCalib fbs("test", chain); 
    scifiTimingCalib fbs("test", chain, mc_offsets_map); 
    fbs.findOffsets(1000, 100.0, 100.0);
    
    // scifiTimingCalib fbs1("window_param_scan", chain, mc_offsets_map); 
    // unsigned int w_start = 100;
    // unsigned int w_end = 50000;
    // size_t w_steps = 20;
    // auto windows = linspace(w_start, w_end, w_steps);
    // fbs1.plotWindowResults(windows, range_ma, range_off);

    // scifiTimingCalib fbs2("ma_param_scan",chain, mc_offsets_map); 
    // double ma_start = 0.1;
    // double ma_end = 50.1;
    // size_t ma_steps = 30;
    // auto range_mas = linspace(ma_start, ma_end, ma_steps);
    // fbs2.plotTDiffMARangeResults(window, range_mas, range_off);

    // scifiTimingCalib fbs3("off_param_scan",chain, mc_offsets_map); 
    // double off_start = 0.1;
    // double off_end = 80.1;
    // size_t off_steps = 20;
    // auto range_offs = linspace(off_start, off_end, off_steps);
    // fbs3.plotTDiffOffRangeResults(window, range_ma, range_offs);

    printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
}