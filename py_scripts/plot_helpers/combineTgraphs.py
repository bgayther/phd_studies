import ROOT
import StyleHelpers

def overlayMultipleTGraphs(grList, legList, legPosition, canTitle, saveName, colourList=[], drawStyle="AP", ymin=None, ymax=None, xmin=None, xmax=None, legStyle="L", ncols=None):
    can = ROOT.TCanvas()
    mg = ROOT.TMultiGraph("mg",str(canTitle))
    leg = ROOT.TLegend(*legPosition)

    num = len(grList)
    if (num != len(legList)): 
        print("Num grs != legs")
        return

    if not(len(colourList)): colourList = StyleHelpers.ColourList
    if (num > len(colourList)):
        print("Too many hists and not enough colours...")
        return

    if (ncols is not None): leg.SetNColumns(ncols)

    for i in range(num):
        gr = grList[i].Clone()
        # if not(gr.GetN()): continue
        # print("Number of points in graph = " + str(gr.GetN()))
        gr.SetLineColor(colourList[i])
        gr.SetMarkerColor(colourList[i])
        mg.Add(gr)
        leg.AddEntry(gr, legList[i], legStyle)
    
    mg.Draw(drawStyle)
    leg.Draw()

    if (ymin is not None): mg.SetMinimum(ymin)
    if (ymax is not None): mg.SetMaximum(ymax)
    if (xmin is not None and xmax is not None): mg.GetXaxis().SetLimits(xmin, xmax)

    can.SaveAs(saveName+".pdf")
    return

def makeGraph(gr1, gr2, title, name):
    can = ROOT.TCanvas("can", "can", 1200, 600)
    mg = ROOT.TMultiGraph("mg",str(title))

    gr1.SetLineColor(ROOT.kRed)
    gr1.SetLineWidth(3)
    gr2.SetLineColor(ROOT.kBlue)
    gr2.SetLineWidth(3)
    mg.Add(gr1,"PL")
    mg.Add(gr2, "PL")
    mg.Draw("A LP")

    leg = ROOT.TLegend(0.65,0.65,0.85,0.85)
    leg.AddEntry(gr1,"coinc (with bug fixes)","l")
    leg.AddEntry(gr2,"coinc (w/o bug fixes)","l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.Draw()

    # gPad.Modified()
    # mg.GetXaxis().SetLimits(0.0,10.0)

    # gPad.Update()
    can.SaveAs(name)
    return  

if __name__ == "__main__":
    pass
    # import ROOT

    # ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h")
    # ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/cpp_scripts/include/RootColourDefs.hpp")

    # cols = ROOT.RootColourDefs()

    # colourList = [cols.kC0, cols.kC1, cols.kC2, cols.kC3, cols.kC4, cols.kC5, cols.kC6, cols.kC7, cols.kC8, cols.kC9]

    # ROOT.gROOT.SetBatch(True)

    # old
    # f_wo_bug_fixes = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/vertexTimingAnalysis_bugs.root")
    # f_w_bug_fixes = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/vertexTimingAnalysis.root")
    # ROOT.gSystem.cd("graphs") 
    # g_roc_bug = f_wo_bug_fixes.Get("g_roc_bhabha")
    # g_roc_no_bug = f_w_bug_fixes.Get("g_roc_bhabha")
    # makeGraph(g_roc_no_bug, g_roc_bug, "Bhabha t_{s} cut;TP rate;FP rate", "roc_overlayed.png")

    # g_ts_bug = f_wo_bug_fixes.Get("g_coinc_bhabha")
    # g_ts_no_bug = f_w_bug_fixes.Get("g_coinc_bhabha")
    # makeGraph(g_ts_no_bug, g_ts_bug, ";t_{s}", "ts_overlayed.png")