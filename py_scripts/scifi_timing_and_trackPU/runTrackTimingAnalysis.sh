DEFAULT_RECO_FILE="/unix/muons/users/bgayther/TimingStudyData/batch/rec_1_muon_beam_defaultReco/job_6024366.farm-a00.hep.ucl.ac.uk/results/mu3e_trirec_1_muon_beam_defaultReco.root"
SINGLE_TRK_PU_RECO_FILE="/unix/muons/users/bgayther/TimingStudyData/batch/rec_1_muon_beam_singleTrkPU/job_6024363.farm-a00.hep.ucl.ac.uk/results/mu3e_trirec_1_muon_beam_singleTrkPU.root"
MULTI_TRK_PU_RECO_FILE="/unix/muons/users/bgayther/TimingStudyData/batch/rec_1_muon_beam_multiTrkPU/job_6024364.farm-a00.hep.ucl.ac.uk/results/mu3e_trirec_1_muon_beam_multiTrkPU.root"
SIM_FILE="/unix/muons/users/bgayther/TimingStudyData/batch/sim_1_100000_muon_beam/job_6022351.farm-a00.hep.ucl.ac.uk/data/mu3e_sorted_1_muon_beam.root"

BASE_ANALYSIS_RESULTS_DIR="/unix/muons/users/bgayther/TimingStudyData/analysis_data"

ANALYSIS_RESULTS_DIR="$BASE_ANALYSIS_RESULTS_DIR/default_reco"
# python fitRecoFbClusters.py -i $DEFAULT_RECO_FILE --output default_reco_other_ana --path $ANALYSIS_RESULTS_DIR -n 500
# python fittingFbClusters.py -i $SIM_FILE --output timing_sim_ana --path $ANALYSIS_RESULTS_DIR -n 10000
python makeFbClusters.py -i $DEFAULT_RECO_FILE --output default_reco_fitting_ana --path $ANALYSIS_RESULTS_DIR -n 20
# python track_timing_res.py -i $DEFAULT_RECO_FILE --output default_reco --path $ANALYSIS_RESULTS_DIR

# ANALYSIS_RESULTS_DIR="$BASE_ANALYSIS_RESULTS_DIR/single_trk_pu_reco"
# python track_timing_res.py -i $SINGLE_TRK_PU_RECO_FILE --output singleTrkPU --path $ANALYSIS_RESULTS_DIR

# ANALYSIS_RESULTS_DIR="$BASE_ANALYSIS_RESULTS_DIR/multi_trk_pu_reco"
# python track_timing_res.py -i $MULTI_TRK_PU_RECO_FILE --output multiTrkPU --path $ANALYSIS_RESULTS_DIR
