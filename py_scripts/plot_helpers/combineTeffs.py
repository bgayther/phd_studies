import ROOT

# TODO: if time, make class, make a main (or move stuff below funcs into its own scripts and import class to use funcs there)
#       also, write such that any number of graphs?

# file_effs = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/timing_track_res_analysis.root")
ROOT.gROOT.SetBatch(True)


def getGraphFromTEff(teff):
    teff.Draw()
    teff.Paint("")
    gr = teff.GetPaintedGraph()
    return gr

# def getGraphAndSetBinLabels(teff):
#     gr = getGraphFromTEff(teff)
#     ax = gr.GetXaxis()
#     x1 = ax.GetBinLowEdge(1)
#     x2 = ax.GetBinUpEdge(ax.GetNbins())
#     ax.Set(2,0,1)
#     ax.SetBinLabel(1, "One time")
#     ax.SetBinLabel(2, "Both times")
#     ay = gr.GetYaxis()
#     ay.SetRangeUser(0,1)
#     return gr

def makeGraph3(gr1, gr2, gr3, title, name):
    can = ROOT.TCanvas("can", "can", 1200, 600)
    mg = ROOT.TMultiGraph("mg",str(title))

    gr1.SetLineColor(ROOT.kRed)
    gr1.SetMarkerColor(ROOT.kRed)
    gr1.SetLineWidth(1)

    gr2.SetLineColor(ROOT.kBlue)
    gr2.SetMarkerColor(ROOT.kBlue)
    gr2.SetLineWidth(1) 

    gr3.SetLineColor(8)
    gr3.SetMarkerColor(8)
    gr3.SetLineWidth(1)

    mg.Add(gr1,"P*")
    mg.Add(gr2, "P*")
    mg.Add(gr3, "P*")
    mg.Draw("A P")

    leg = ROOT.TLegend(0.65,0.35,0.85,0.55)
    leg.AddEntry(gr1,"L6 fb","l")
    leg.AddEntry(gr2,"L8 fb","l")
    leg.AddEntry(gr3,"L6 tl","l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.Draw()

    # ROOT.gPad.Modified()
    # mg.GetXaxis().SetLimits(0.0,10.0)

    ROOT.gPad.Update()
    can.SaveAs(name)
    return  

def makeGraph2(gr1, gr2, title, name, min=0.0, max=1.0, leg1="", leg2="", timesPresentGrs=False, versusTrType=False, versusTimeType=False):
    can = ROOT.TCanvas("can", "can", 1200, 600)
    mg = ROOT.TMultiGraph("mg",str(title))

    gr1.SetLineColor(ROOT.kRed)
    gr1.SetMarkerColor(ROOT.kRed)
    gr1.SetLineWidth(1)

    gr2.SetLineColor(ROOT.kBlue)
    gr2.SetMarkerColor(ROOT.kBlue)
    gr2.SetLineWidth(1) 

    mg.Add(gr1,"P*")
    mg.Add(gr2, "P*")
    mg.Draw("A P")

    tex = ROOT.TLatex()
    tex.SetNDC()
    tex.SetTextSize(0.035)
    tex.SetTextFont(42)
    tex.DrawLatex(0.70, 0.86, "#bf{Mu3e} #it{work in progress}")
    # tex.DrawLatex(0.15, 0.92, "#bf{Mu3e} #it{work in progress}") #if no title

    leg = ROOT.TLegend(0.80,0.75,0.85,0.85)
    leg.AddEntry(gr1,leg1,"l")
    leg.AddEntry(gr2,leg2,"l")
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.Draw()

    ROOT.gPad.Modified()
    # get from gr itself...
    # mg.GetXaxis().SetLimits(0.0,65.0)

    ax = mg.GetXaxis()
    if (timesPresentGrs):
        ax.Set(2,0,2)
        ax.SetBinLabel(1, "T_{1} or T_{2} present only")
        ax.SetBinLabel(2, "Both T_{1} and T_{2} present")

    if (versusTrType):
        ax.Set(3,0,3)
        ax.SetBinLabel(1, "S4")
        ax.SetBinLabel(2, "L6")
        ax.SetBinLabel(3, "L8")

    if (versusTimeType):
        ax.Set(3,0,3)
        ax.SetBinLabel(1, "T1 only")
        ax.SetBinLabel(2, "T2 only")
        ax.SetBinLabel(3, "T1 and T2")

    mg.SetMinimum(min)
    mg.SetMaximum(max)

    ROOT.gPad.Update()
    can.SaveAs(name)
    return  

ROOT.gSystem.cd("graphs") 

# cosmic reco
# cosmic_file = ROOT.TFile.Open("/ben_dev/mu3e_cosmicRun/mu3e/tests/data/cosmicRecoAna.root")
# eff_S4_mc_phi = cosmic_file.Get("eff_S4_mc_phi")
# eff_S4_mc_theta = cosmic_file.Get("eff_S4_mc_theta")
# eff_S3_mc_phi = cosmic_file.Get("eff_S3_mc_phi")
# eff_S3_mc_theta = cosmic_file.Get("eff_S3_mc_theta")

# makeGraph2(getGraphFromTEff(eff_S4_mc_phi), getGraphFromTEff(eff_S3_mc_phi), "Efficiency for S3/S4;#phi [rad];", "phi_eff_S4_vs_S3.pdf", leg1="S4", leg2="S3")
# makeGraph2(getGraphFromTEff(eff_S4_mc_theta), getGraphFromTEff(eff_S3_mc_theta), "Efficiency for S3/S4;#theta [rad];", "theta_eff_S4_vs_S3.pdf", leg1="S4", leg2="S3")

# cosmic_holes_file = ROOT.TFile.Open("/ben_dev/mu3e_cosmicRun/mu3e/tests/data/cosmicRecoHolesAna.root")
# holes_eff_S4_mc_phi = cosmic_holes_file.Get("eff_S4_mc_phi")
# holes_eff_S4_mc_theta = cosmic_holes_file.Get("eff_S4_mc_theta")
# holes_eff_S3_mc_phi = cosmic_holes_file.Get("eff_S3_mc_phi")
# holes_eff_S3_mc_theta = cosmic_holes_file.Get("eff_S3_mc_theta")

# makeGraph2(getGraphFromTEff(holes_eff_S4_mc_phi), getGraphFromTEff(eff_S4_mc_phi), "Efficiency with and without holes (S4);#phi [rad];", "phi_eff_S4_holes_diff.pdf", leg1="Holes", leg2="W/O Holes")
# makeGraph2(getGraphFromTEff(holes_eff_S3_mc_phi), getGraphFromTEff(eff_S3_mc_phi), "Efficiency with and without holes (S3);#phi [rad];", "phi_eff_S3_holes_diff.pdf", leg1="Holes", leg2="W/O Holes")

# makeGraph2(getGraphFromTEff(holes_eff_S4_mc_theta), getGraphFromTEff(eff_S4_mc_theta), "Efficiency with and without holes (S4);#theta [rad];", "theta_eff_S4_holes_diff.pdf", leg1="Holes", leg2="W/O Holes")
# makeGraph2(getGraphFromTEff(holes_eff_S3_mc_theta), getGraphFromTEff(eff_S3_mc_theta), "Efficiency with and without holes (S3);#theta [rad];", "theta_eff_S3_holes_diff.pdf", leg1="Holes", leg2="W/O Holes")


# cosmics sims
# cosmic_file = ROOT.TFile.Open("/ben_dev/mu3e_cosmicRun/mu3e/tests/data/cosmicSimAna.root")
# theta_acc_to_4hits = cosmic_file.Get("theta_acc_to_4hits")
# phi_acc_to_4hits = cosmic_file.Get("phi_acc_to_4hits")
# theta_acc_to_3hits = cosmic_file.Get("theta_acc_to_3hits")
# phi_acc_to_3hits = cosmic_file.Get("phi_acc_to_3hits")
# theta_acc_to_4hits_with_holes = cosmic_file.Get("theta_acc_to_4hits_with_holes")
# phi_acc_to_4hits_with_holes = cosmic_file.Get("phi_acc_to_4hits_with_holes")
# theta_acc_to_3hits_with_holes = cosmic_file.Get("theta_acc_to_3hits_with_holes")
# phi_acc_to_3hits_with_holes = cosmic_file.Get("phi_acc_to_3hits_with_holes")

# makeGraph2(getGraphFromTEff(theta_acc_to_4hits_with_holes), getGraphFromTEff(theta_acc_to_4hits), "Efficiency/Acceptance with and without holes (4-hits);#theta [rad];", "theta_acc_4hits_holes_diff.pdf", leg1="Holes", leg2="W/O Holes", max=0.02)
# makeGraph2(getGraphFromTEff(theta_acc_to_3hits_with_holes), getGraphFromTEff(theta_acc_to_3hits), "Efficiency/Acceptance with and without holes (3-hits);#theta [rad];", "theta_acc_3hits_holes_diff.pdf", leg1="Holes", leg2="W/O Holes", max=0.02)

# makeGraph2(getGraphFromTEff(phi_acc_to_4hits_with_holes), getGraphFromTEff(phi_acc_to_4hits), "Efficiency/Acceptance with and without holes (4-hits);#phi [rad];", "phi_acc_4hits_holes_diff.pdf", leg1="Holes", leg2="W/O Holes", max=0.02)
# makeGraph2(getGraphFromTEff(phi_acc_to_3hits_with_holes), getGraphFromTEff(phi_acc_to_3hits), "Efficiency/Acceptance with and without holes (3-hits);#phi [rad];", "phi_acc_3hits_holes_diff.pdf", leg1="Holes", leg2="W/O Holes", max=0.02)

# makeGraph2(getGraphFromTEff(theta_acc_to_4hits), getGraphFromTEff(theta_acc_to_3hits), "Efficiency/Acceptance for 3-hits and 4-hits;#theta [rad];", "theta_acc_4_vs_3_hits.pdf", leg1="4-hits", leg2="3-hits", max=0.02)
# makeGraph2(getGraphFromTEff(phi_acc_to_4hits), getGraphFromTEff(phi_acc_to_3hits), "Efficiency/Acceptance for 3-hits and 4-hits;#phi [rad];", "phi_acc_4_vs_3_hits.pdf", leg1="4-hits", leg2="3-hits", max=0.02)

# fb track PU fitting effs
# defaultMultiTrk_eff_vs_times_all = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_cl_loss_denominator")
# newMultiTrk_eff_vs_times_all = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_cl_loss_denominator")
# makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_times_all), getGraphFromTEff(newMultiTrk_eff_vs_times_all), "All affected clusters [Multi Track Clusters] - Eff before/after taking into account loss of clusters;;Time efficiency", "multiTrkEffsVsTimesPresentDifferentDenominator.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

# defaultSingle_eff_vs_times_all = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_cl_loss_denominator")
# newSingleTrk_eff_vs_times_all = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_cl_loss_denominator")
# makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_times_all), getGraphFromTEff(newSingleTrk_eff_vs_times_all), "All affected clusters [Single Track Clusters] - Eff before/after taking into account loss of clusters;;Time efficiency", "singleTrkEffsVsTimesPresentDifferentDenominator.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

defaultMultiTrk_eff_vs_times_but_not_fitCl = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_but_not_fitCl")
newMultiTrk_eff_vs_times_but_not_fitCl = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_but_not_fitCl")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_times_but_not_fitCl), getGraphFromTEff(newMultiTrk_eff_vs_times_but_not_fitCl), "All affected clusters [Multi Track Clusters] - Eff before/after looking at clusters not assigned;;Time efficiency", "multiTrkEffsVsTimesPresentAssignmentAffectedByFitCl.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

defaultSingle_eff_vs_times_but_not_fitCl = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_but_not_fitCl")
newSingleTrk_eff_vs_times_but_not_fitCl = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_but_not_fitCl")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_times_but_not_fitCl), getGraphFromTEff(newSingleTrk_eff_vs_times_but_not_fitCl), "All affected clusters [Single Track Clusters] - Eff before/after looking at clusters not assigned;;Time efficiency", "singleTrkEffsVsTimesPresentAssignmentAffectedByFitCl.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

defaultMultiTrk_eff_vs_times = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present")
newMultiTrk_eff_vs_times = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_times), getGraphFromTEff(newMultiTrk_eff_vs_times), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "multiTrkEffsVsTimesPresent.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

defaultSingle_eff_vs_times = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present")
newSingleTrk_eff_vs_times = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_times), getGraphFromTEff(newSingleTrk_eff_vs_times), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "singleTrkEffsVsTimesPresent.pdf", leg1="Default", leg2="Fit", timesPresentGrs=True)

defaultMultiTrk_eff_vs_time_tr_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_tr_type")
newMultiTrk_eff_vs_time_tr_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_tr_type")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_time_tr_type), getGraphFromTEff(newMultiTrk_eff_vs_time_tr_type), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "multiTrkEffsVsTrType.pdf", leg1="Default", leg2="Fit", versusTrType=True)

defaultSingle_eff_vs_time_tr_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_tr_type")
newSingleTrk_eff_vs_time_tr_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_tr_type")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_time_tr_type), getGraphFromTEff(newSingleTrk_eff_vs_time_tr_type), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "singleTrkEffsVsTrType.pdf", leg1="Default", leg2="Fit", versusTrType=True)

defaultMultiTrk_eff_vs_time_time_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_time_type")
newMultiTrk_eff_vs_time_time_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_times_present_time_type")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_time_time_type), getGraphFromTEff(newMultiTrk_eff_vs_time_time_type), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "multiTrkEffsVsTimeType.pdf", leg1="Default", leg2="Fit", versusTimeType=True)

defaultSingle_eff_vs_time_time_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_time_type")
newSingleTrk_eff_vs_time_time_type = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_times_present_time_type")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_time_time_type), getGraphFromTEff(newSingleTrk_eff_vs_time_time_type), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;;Time efficiency", "singleTrkEffsVsTimeType.pdf", leg1="Default", leg2="Fit", versusTimeType=True)

defaultMultiTrk_eff_vs_p = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_p")
newMultiTrk_eff_vs_p = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_p")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_p), getGraphFromTEff(newMultiTrk_eff_vs_p), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;p [MeV/#it{c}];Time efficiency", "multiTrkEffsVsMomentum.pdf", leg1="Default", leg2="Fit")

defaultSingle_eff_vs_p = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_p")
newSingleTrk_eff_vs_p = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_p")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_p), getGraphFromTEff(newSingleTrk_eff_vs_p), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;p [MeV/#it{c}];Time efficiency", "singleTrkEffsVsMomentum.pdf", leg1="Default", leg2="Fit")

defaultMultiTrk_eff_vs_lam = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_lam")
newMultiTrk_eff_vs_lam = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_lam")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_lam), getGraphFromTEff(newMultiTrk_eff_vs_lam), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;#lambda [rad];Time efficiency", "multiTrkEffsVsLambda.pdf", leg1="Default", leg2="Fit")

defaultSingle_eff_vs_lam = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_lam")
newSingleTrk_eff_vs_lam = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_lam")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_lam), getGraphFromTEff(newSingleTrk_eff_vs_lam), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;#lambda [rad];Time efficiency", "singleTrkEffsVsLambda.pdf", leg1="Default", leg2="Fit")

defaultMultiTrk_eff_vs_mc_vt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_mc_vt")
newMultiTrk_eff_vs_mc_vt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_mc_vt")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_mc_vt), getGraphFromTEff(newMultiTrk_eff_vs_mc_vt), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;MC vertex time [ns];Time efficiency", "multiTrkEffsVsVertexTime.pdf", leg1="Default", leg2="Fit")

defaultSingle_eff_vs_mc_vt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_mc_vt")
newSingleTrk_eff_vs_mc_vt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_mc_vt")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_mc_vt), getGraphFromTEff(newSingleTrk_eff_vs_mc_vt), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;MC vertex time [ns];Time efficiency", "singleTrkEffsVsVertexTime.pdf", leg1="Default", leg2="Fit")

defaultMultiTrk_eff_vs_z_diff = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_z_diff")
newMultiTrk_eff_vs_z_diff = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_affected_vs_z_diff")
makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_z_diff), getGraphFromTEff(newMultiTrk_eff_vs_z_diff), "All affected clusters [Multi Track Clusters] - Eff before/after only looking at directly affected clusters;difference in Z [mm];Time efficiency", "multiTrkEffsVsZDiff.pdf", leg1="Default", leg2="Fit")

defaultSingle_eff_vs_z_diff = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_z_diff")
newSingleTrk_eff_vs_z_diff = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_affected_vs_z_diff")
makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_z_diff), getGraphFromTEff(newSingleTrk_eff_vs_z_diff), "All affected clusters [Single Track Clusters] - Eff before/after only looking at directly affected clusters;difference in Z [mm];Time efficiency", "singleTrkEffsVsZDiff.pdf", leg1="Default", leg2="Fit")


# These don't make sense
# defaultSingle_eff_vs_dt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl")
# newSingleTrk_eff_vs_dt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl")
# makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_dt), getGraphFromTEff(newSingleTrk_eff_vs_dt), "All track types [Single Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "singleTrkEffsOverlap.pdf", leg1="Default", leg2="Fit")

# defaultMultiTrk_eff_vs_dt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl")
# newMultiTrk_eff_vs_dt = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl")
# makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_dt), getGraphFromTEff(newMultiTrk_eff_vs_dt), "All track types [Multi Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "multiTrkEffsOverlap.pdf", leg1="Default", leg2="Fit")

# defaultSingle_eff_vs_dt_both_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl_both_present")
# newSingleTrk_eff_vs_dt_both_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl_both_present")
# makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_dt_both_present), getGraphFromTEff(newSingleTrk_eff_vs_dt_both_present), "All track types (both times present) [Single Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "singleTrkEffsOverlapBothPresent.pdf", leg1="Default", leg2="Fit")

# defaultMultiTrk_eff_vs_dt_both_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl_both_present")
# newMultiTrk_eff_vs_dt_both_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl_both_present")
# makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_dt_both_present), getGraphFromTEff(newMultiTrk_eff_vs_dt_both_present), "All track types (both times present) [Multi Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "multiTrkEffsOverlapBothPresent.pdf", leg1="Default", leg2="Fit")

# defaultSingle_eff_vs_dt_one_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl_one_present")
# newSingleTrk_eff_vs_dt_one_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentSingleTrk.root").Get("h_fb_eff_vs_dt_cl_one_present")
# makeGraph2(getGraphFromTEff(defaultSingle_eff_vs_dt_one_present), getGraphFromTEff(newSingleTrk_eff_vs_dt_one_present), "All track types (one time present) [Single Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "singleTrkEffsOverlapOnePresent.pdf", leg1="Default", leg2="Fit")

# defaultMultiTrk_eff_vs_dt_one_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/defaultAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl_one_present")
# newMultiTrk_eff_vs_dt_one_present = ROOT.TFile.Open("/ben_dev/mu3e/tests/data/newAssignmentMultiTrk.root").Get("h_fb_eff_vs_dt_cl_one_present")
# makeGraph2(getGraphFromTEff(defaultMultiTrk_eff_vs_dt_one_present), getGraphFromTEff(newMultiTrk_eff_vs_dt_one_present), "All track types (one time present) [Multi Track Clusters];Max - min hit time within fbc [ns];Time efficiency", "multiTrkEffsOverlapOnePresent.pdf", leg1="Default", leg2="Fit")


# Old basic eff vs z
# eff_t2_l6_fb = file_effs.Get("h_fb_l6_t2_eff_vs_z")
# eff_t2_l8_fb = file_effs.Get("h_fb_l8_t2_eff_vs_z")
# eff_t2_l6_tl = file_effs.Get("h_tl_t2_eff_vs_z")
# makeGraph3(getGraphFromTEff(eff_t2_l6_fb), getGraphFromTEff(eff_t2_l8_fb), getGraphFromTEff(eff_t2_l6_tl), "All track types;Z position of T_{2} [mm];T_{2} efficiency", "effsCombined.png")

