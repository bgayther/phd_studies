import ROOT
import argparse

# TODO: use personal rootstyle for all plots

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--input', type=str, required=True, help='Input reco file name')
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)

can = ROOT.TCanvas("can", "can", 1200, 600)

rfile = ROOT.TFile.Open(args.input)
ROOT.gSystem.cd("graphs") 

h = rfile.Get("stat/hBICPrediction")
h.SetLineWidth(2)
h.SetTitle("BIC Residual")
h.GetXaxis().SetTitle("Predicted - true number of tracks in cluster ")
h.Scale(1./h.GetEntries())
h.GetYaxis().SetTitle("Relative proportion")
# h.GetYaxis().SetTitle("Counts")
# h.Draw()
h.Draw("HIST")

tex = ROOT.TLatex()
tex.SetNDC()
tex.SetTextSize(0.035)
tex.SetTextFont(42)
tex.DrawLatex(0.70, 0.86, "#bf{Mu3e} #it{work in progress}")
tex.DrawLatex(0.70, 0.80, "Mean = {:.2f}, RMS = {:.2f}".format(h.GetMean(), h.GetRMS()))

# ROOT.gPad.SetLogy()
can.SaveAs("hBICPrediction.pdf")

# h = rfile.Get("h_PU_density")
# h.SetLineWidth(2)
# h.Scale(1./h.GetEntries())
# h.GetYaxis().SetTitle("Relative proportion")
# h.Draw("HIST")

# tex = ROOT.TLatex()
# tex.SetNDC()
# tex.SetTextSize(0.035)
# tex.SetTextFont(42)
# tex.DrawLatex(0.70, 0.86, "#bf{Mu3e} #it{work in progress}")

# ROOT.gPad.SetLogy()
# can.SaveAs("h_PU_density.pdf")

# h = rfile.Get("h_nhits_per_side")
# h.SetLineWidth(2)
# h.Scale(1./h.GetEntries())
# h.SetTitle("Number of hits in cluster (per side)")
# # h.GetXaxis().SetTitle("Predicted - true number of tracks in cluster ")
# h.GetYaxis().SetTitle("Relative proportion")
# h.Draw("HIST")

# tex = ROOT.TLatex()
# tex.SetNDC()
# tex.SetTextSize(0.035)
# tex.SetTextFont(42)
# tex.DrawLatex(0.70, 0.86, "#bf{Mu3e} #it{work in progress}")

# can.SaveAs("h_nhits_per_side.pdf")

    