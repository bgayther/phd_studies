#ifndef UTIL_PLOTTER_HPP_
#define UTIL_PLOTTER_HPP_

#include <nlohmann/json.hpp>
using json = nlohmann::json;
class PlotInfo {
    public:

        std::string name = "";
        std::string title = "";
        std::string xtitle = "";
        std::string ytitle = "";

        int xbins = 0;
        double xmin = 0.0;
        double xmax = 0.0;

        int ybins = 0;
        double ymin = 0.0;
        double ymax = 0.0;

        std::string varX = "";
        std::string varY = "";

        int segType = -1;

        std::string style = "";

        std::string cut = "";

        bool perTrack = false;

        bool logy = false;

        PlotInfo() {};
        ~PlotInfo() {};

        PlotInfo(std::string name_
        , std::string title_
        , std::string xtitle_
        , std::string ytitle_
        , int xbins_
        , double xmin_
        , double xmax_
        , std::string varX_
        , std::string cut_ = ""
        , bool perTrack_ = false) : 
            name(name_),  
            title(title_),
            xtitle(xtitle_),
            ytitle(ytitle_),
            xbins(xbins_),
            xmin(xmin_),
            xmax(xmax_),
            varX(varX_),
            cut(cut_),
            perTrack(perTrack_) 
        {}

        PlotInfo(std::string name_
        , std::string title_
        , std::string xtitle_
        , std::string ytitle_
        , int xbins_
        , double xmin_
        , double xmax_
        , int ybins_
        , double ymin_
        , double ymax_
        , std::string varX_
        , std::string varY_
        , std::string cut_ = ""
        , bool perTrack_ = false
        ) : 
            name(name_),
            title(title_),
            xtitle(xtitle_),
            ytitle(ytitle_),
            xbins(xbins_),
            xmin(xmin_),
            xmax(xmax_),
            ybins(ybins_),
            ymin(ymin_),
            ymax(ymax_),
            varX(varX_),
            varY(varY_),
            cut(cut_),
            perTrack(perTrack_)
        {}

        void set2DInfo(std::string name_
        , std::string title_
        , std::string xtitle_
        , std::string ytitle_
        , int xbins_
        , double xmin_
        , double xmax_
        , int ybins_
        , double ymin_
        , double ymax_
        , std::string varX_
        , std::string varY_
        ) {
            reset();
            name = name_;
            title = title_;
            xtitle = xtitle_;
            ytitle = ytitle_;
            xbins = xbins_;
            xmin = xmin_;
            xmax = xmax_;
            ybins = ybins_;
            ymin = ymin_;
            ymax = ymax_;
            varX = varX_;
            varY = varY_;
        }

        void set1DInfo(std::string name_
        , std::string title_
        , std::string xtitle_
        , std::string ytitle_
        , int xbins_
        , double xmin_
        , double xmax_
        , std::string varX_
        ) {
            reset();
            name = name_;
            title = title_;
            xtitle = xtitle_;
            ytitle = ytitle_;
            xbins = xbins_;
            xmin = xmin_;
            xmax = xmax_;
            varX = varX_;
        }

        void setName(std::string name_)   {name = name_;}
        void setTitle(std::string title_) {title = title_;}
        void setXTitle(std::string xtitle_) {xtitle = xtitle_;}
        void setYTitle(std::string ytitle_) {ytitle = ytitle_;}
        void setStyle(std::string style_) {style = style_;}
        void setSegType(int segType_) {segType = segType_;}
        void setCut(std::string cut_) {cut = cut_;}
        void setLogy(bool log_) {logy = log_;}

        void addCut(std::string cut_) {
            if (cut.empty()) setCut(cut_);
            else cut += ("&&" + cut_);
        }

        std::string getSegType() const {
            if (segType == 4) return "S4";
            else if (segType == 3) return "S3";
            else if (segType == 6) return "L6";
            else if (segType == 8) return "L8";
            else return "";
        }

        void reset() {
            name = "";
            title = "";
            xtitle = "";
            ytitle = "";
            xbins = 0;
            xmin = 0.0;
            xmax = 0.0;
            ybins = 0;
            ymin = 0.0;
            ymax = 0.0;
            varX = "";
            varY = "";
            cut = "";
            segType = -1;
            style = "";
            // particle = "";
            perTrack = false;
        }

        static std::string determineSegType(const std::string& input) {
            std::string segType = "";
            if (input.find("S4") != std::string::npos) segType = "S4";
            else if (input.find("L6") != std::string::npos) segType = "L6";
            else if (input.find("L8") != std::string::npos) segType = "L8";
            else if (input.find("Long") != std::string::npos) segType = "Long";
            return segType;
        }

        // TODO: add some safety checks!
        static std::vector<PlotInfo> makePlotsVecFromJSON(const json& input) {
            std::vector<PlotInfo> plots;
            if (!input.contains("histograms")) return plots;
            for (auto& j : input["histograms"].items()) {
                if (!j.value().contains("name")) continue;
                PlotInfo pl;
                pl.name = j.value()["name"];
                pl.title = j.value()["title"];
                pl.xtitle = j.value()["xtitle"];
                pl.ytitle = j.value()["ytitle"];
                pl.xbins = j.value()["xbins"];
                pl.xmin = j.value()["xmin"];
                pl.xmax = j.value()["xmax"];
                pl.varX = j.value()["varX"];
                if (j.value().contains("varY")) {
                    pl.ybins = j.value()["ybins"];
                    pl.ymin = j.value()["ymin"];
                    pl.ymax = j.value()["ymax"];
                    pl.varY = j.value()["varY"];
                }
                if (j.value().contains("cut")) pl.cut = j.value()["cut"];
                if (j.value().contains("perTrack")) pl.perTrack = j.value()["perTrack"];
                if (j.value().contains("logy")) pl.logy = j.value()["logy"];
                plots.push_back(pl);
            }
            return plots;
        }
};

#endif /* UTIL_PLOTTER_HPP_ */