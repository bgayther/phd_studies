import numpy as np 
import pandas as pd
import awkward as ak
import uproot
import pickle
import os
import argparse
import time

desired_vars = ["chi2", "pm", "pcoplanar", "pacoplanar", "targetdist", "cosalpha", "mep1", "mep2", "mmass", "theta_kuno", "phi_kuno", "nhit1", "nhit2", "nhit3"] # vertex pos?
mc_vars = ["id1", "id2", "id3", "pid1", "pid2", "pid3", "type1", "type2", "type3", "prime1", "prime2", "prime3", "mid1", "mid2", "mid3", "true_vx", "true_vy", "true_vz", "mc_signal"]

def retrieve_data(filenames, mode, ncpu):
    # Read data from ROOT files
    
    print(f"Loading data for mode {mode}")
    all_data = ak.Array([])

    # for batch in uproot.iterate(filenames, "vertex", num_workers=ncpu):
    for filename in filenames:
        # print(f"Reading file {filename}")
        vertex = uproot.open(f"{filename}:vertex", num_workers=ncpu)

        data = vertex.arrays(desired_vars+mc_vars)
        weight = vertex["weight"].array()
        event = vertex["event"].array()
        run = vertex["run"].array()

        cut = None
        if mode=="BhabhaMichel":
            cut = (data.chi2 < 30) & (data.nhit1 > 4) & (data.nhit2 > 4) & (data.nhit3 > 4) & (data.mmass > 80) & (data.mmass < 115) & (data.type3 == 252) & (data.prime1 == 1) & \
                  (data.prime2 == 1) & (data.prime3 == 1) & (data.pid1 == -11) & (data.pid2 == -11) & (data.pid3 == 11) & (data.id1 != data.id2) & (data.id1 != data.id3) & (data.id2 != data.id3) & ((data.mid1 == data.mid3) | (data.mid2 == data.mid3))
        elif mode=="Sig":
            cut = (data.chi2 < 30) & (data.nhit1 > 4) & (data.nhit2 > 4) & (data.nhit3 > 4) & (data.mmass > 80) & (data.mmass < 115) & (data.mc_signal==1) & (np.hypot(data.true_vx, data.true_vy) <= 19) & (np.abs(data.true_vz) <= 50)
        elif mode=="IC":
            cut = (data.chi2 < 30) & (data.nhit1 > 4) & (data.nhit2 > 4) & (data.nhit3 > 4) & (data.mmass > 80) & (data.mmass < 115) & (data.id1 != data.id2) & (data.id1 != data.id3) & (data.id2 != data.id3) & (data.mid1 == data.mid2) & (data.mid2 == data.mid3) & \
                  (data.prime1 == 1) & (data.prime2 == 1) & (data.prime3 == 1) & (np.hypot(data.true_vx, data.true_vy) <= 19) & (np.abs(data.true_vz) <= 50) & (data.type1==31) & (data.type2==31) & (data.type3==32)
        
        cut_data = data[cut]
        # Apply cut to weight (note that weight is per event, not per vertex)
        # So keep only the events that have at least one vertex passing the cut
        cut_weight = weight[ak.to_numpy(ak.pad_none(cut, 1, axis=1, clip=True)).any()]
        cut_event = event[ak.to_numpy(ak.pad_none(cut, 1, axis=1, clip=True)).any()]
        cut_run = run[ak.to_numpy(ak.pad_none(cut, 1, axis=1, clip=True)).any()]

        cut_data["weight"] = cut_weight
        cut_data["event"] = cut_event
        cut_data["run"] = cut_run

        all_data = ak.concatenate([all_data, cut_data], axis=0)

    # all_data = ak.pad_none(all_data, 1, axis=1, clip=True)
    data = ak.to_pandas(all_data)

    return data

if __name__ == "__main__":
    # test_train_bdt()
    # Pass filenames to retrieve_data
    parser = argparse.ArgumentParser()
    parser.add_argument("-files", type=str, nargs="+", help="Filenames to retrieve data from")
    parser.add_argument("-mode", type=str, help="mode of data (BhabhaMichel, IC, Sig)", required=True)
    parser.add_argument("-ncpu", help="Number of CPUs to use", default=1, type=int)
    parser.add_argument("-output", help="Output filename", type=str, required=True)
    args = parser.parse_args()

    print("Starting data retrieval...")

    # time
    start = time.time()
    data = retrieve_data(args.files, args.mode, args.ncpu)
    print(data)

    # Save the data
    # data.to_pickle(args.output + ".pickle")
    data.to_parquet(args.output + ".parquet")

    # time
    end = time.time()
    print(f"Time taken: {end - start:.2f} seconds")