DIR=/unix/muons/users/bgayther/MottScattering/batch

SAMPLES=$1
TAGS=$2

for SAMPLE in $SAMPLES ; do
for TAG in $TAGS ; do
    # echo ${DIR}/mu3e_sorted_${SAMPLE}_${TAG}.root, ${DIR}/sim_${SAMPLE}_*_${TAG}/*/data/mu3e_sorted_*_${TAG}.root
    # echo ${DIR}/mu3e_trirec_${SAMPLE}_${TAG}.root, ${DIR}/sim_${SAMPLE}_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
    hadd -j 8 ${DIR}/mu3e_sorted_${SAMPLE}_${TAG}.root ${DIR}/sim_${SAMPLE}_*_${TAG}/*/data/mu3e_sorted_*_${TAG}.root
    hadd -j 8 ${DIR}/mu3e_trirec_${SAMPLE}_${TAG}.root ${DIR}/sim_${SAMPLE}_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
done
done
