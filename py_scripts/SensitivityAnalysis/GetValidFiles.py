# Check files on GRID
import subprocess

files = {"Sig": [], "IC": [], "Bhabha": [], "BhabhaMichel": []}
paths = ["/mu3e.org/prod/build4.4.3/Signal", "/mu3e.org/prod/build4.4.3/IC", "/mu3e.org/prod/build4.4.3/Bhabha", "/mu3e.org/prod/build4.4.3/BhabbaMichel"]

for path, mode in zip(paths, files.keys()):
    print(f"Checking {path} files")

    # Capture output of dirac-dms-find-lfns
    data = subprocess.run(["dirac-dms-find-lfns", f"--Path={path}", "Size>0"], stdout=subprocess.PIPE)
    # print(data)

    # get stdout from data
    curr_files = data.stdout.decode("utf-8")

    # Convert this string to list of strings (each string is a file)
    curr_files = curr_files.split()

    # print(type(files), files)

    files[mode] = curr_files

# print(files)
# Save files to .pickle file
import pickle
with open("files.pickle", "wb") as f:
    pickle.dump(files, f)