'''
Train NN on track reconstruction files
'''

# Imports
import argparse
import numpy as np
from ROOT import RDataFrame, gROOT, gErrorIgnoreLevel, kWarning, gInterpreter
# import uproot
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split

def test_clustering(data_jagged):
    # Do clustering for one event based on x, y, z positions (note that xPositions, yPositions, zPositions are arrays of length nhit), cross check with tid (individual truth track id) and mc_mid (mother/origin id of track)
    # with kNN, DBSCAN
    from sklearn.neighbors import NearestNeighbors

    # Make 2D array of x, y, z positions
    pos = np.array([[data_jagged["xPositions"][0][i][j], data_jagged["yPositions"][0][i][j], data_jagged["zPositions"][0][i][j]] for i in range(len(data_jagged["xPositions"][0])) for j in range(len(data_jagged["xPositions"][0][i]))])
    # print(pos)

    # Get truth labels for each hit (need to associate each hit with a truth track), need to check length of each array in xPositions, yPositions, zPositions
    mc_tid = np.array([data_jagged["mc_tid"][0][i][j] for i in range(len(data_jagged["xPositions"][0])) for j in range(len(data_jagged["xPositions"][0][i]))])

    # Plot pos in 2d (y vs x) with truth labels
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(pos[:, 0], pos[:, 1], c=mc_tid, cmap='rainbow')
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    plt.savefig("pos_xy_truth.png")

    # Do clustering (initiate number of clusters with total number of tracks in event also set this as the maximum number of clusters)
    nbrs = NearestNeighbors(n_neighbors=2, algorithm='auto').fit(pos)
    # Get cluster labels
    distances, indices = nbrs.kneighbors(pos)
    labels_knn = np.array([indices[i][1] for i in range(len(indices))])

    # Now compare with mc_tid and mc_mid (aim is to see if the clustering is doing a good job of separating the tracks)
    # print(data_jagged["mc_tid"][0])
    # print(data_jagged["mc_mid"][0])

    # Get number of unique labels
    print(f"Number of unique labels (kNN): {len(np.unique(labels_knn))}")

    # Get number of unique mc_tid and mc_mid
    print(f"Number of unique mc_tid: {len(np.unique(data_jagged['mc_tid'][0]))}")
    print(f"Number of unique mc_mid: {len(np.unique(data_jagged['mc_mid'][0]))}")

    # Get number of unique labels for each mc_tid and mc_mid
    print(f"Number of unique labels for each mc_tid: {[len(np.unique(labels_knn[np.where(data_jagged['mc_tid'][0] == i)])) for i in np.unique(data_jagged['mc_tid'][0])]}")
    print(f"Number of unique labels for each mc_mid: {[len(np.unique(labels_knn[np.where(data_jagged['mc_mid'][0] == i)])) for i in np.unique(data_jagged['mc_mid'][0])]}")

def prepare_data(data_jagged):
    # Separate data into mc_prime == 1 and (mc_prime == 0)
    data_sig = {var: np.array([data_jagged[var][i][np.where(data_jagged["mc_prime"][i] == 1)] for i in range(len(eventId_unique))], dtype=object) for var in all_vars}
    data_bkg = {var: np.array([data_jagged[var][i][np.where(data_jagged["mc_prime"][i] == 0)] for i in range(len(eventId_unique))], dtype=object) for var in all_vars}

    # print(data_sig["chi2"][0:2])
    # print(data_bkg["chi2"][0:2])

    # Make x_sig and x_bkg such that each row is a different event, and each column is a different variable (var)
    x_sig = np.array([np.array([data_sig[var][i][j] for var in desired_vars]) for i in range(len(data_sig["mc_prime"])) for j in range(len(data_sig["mc_prime"][i]))])
    x_bkg = np.array([np.array([data_bkg[var][i][j] for var in desired_vars]) for i in range(len(data_bkg["mc_prime"])) for j in range(len(data_bkg["mc_prime"][i]))])
    x = np.concatenate((x_sig, x_bkg), axis=0)

    num_sig = len(x_sig)
    num_bkg = len(x_bkg)
    y = np.concatenate((np.ones(num_sig), np.zeros(num_bkg)), axis=0)

    num_all = num_sig + num_bkg
    w = np.concatenate((np.ones(num_sig) / num_sig, np.ones(num_bkg) / num_bkg), axis=0)

    print(f"Number of signal events: {num_sig}, Number of background events: {num_bkg}")

    # Split/shuffle into training and testing data
    
    x_train, x_test, y_train, y_test, w_train, w_test = train_test_split(x, y, w, test_size=0.2, random_state=42)

    return x_train, x_test, y_train, y_test, w_train, w_test

gInterpreter.Declare("""
    // Filter out tracks which belong to multiple recurlers?
    //bool filterMultipleRecurlers() {
    //};

    ROOT::RVec<float> getHitVar(const int& nhit, const ROOT::RVec<float> &pos00, const ROOT::RVec<float> &pos20) {
        ROOT::RVec<float> pos(nhit);
        int ntrip = nhit - 2;
        for (int i = 0; i < ntrip; i++) {
            pos[i] = pos00[i];
        }
        pos[nhit-2] = pos20[ntrip-2];
        pos[nhit-1] = pos20[ntrip-1];
        return pos;
    };


""")

if __name__ == "__main__":
    # Get arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-inputFiles", type=str, nargs='+', required=False, help="Input file")
    args = parser.parse_args()

    # ROOT.EnableImplicitMT(8)
    gROOT.SetBatch(True)
    gErrorIgnoreLevel = kWarning

    # Load files
    inputFiles = args.inputFiles
    inputFiles = "/unix/muons/users/bgayther/MichelDecays/batch/sim_michel_1_100000_standard_muon_beam/job_5709285.farm-a00.hep.ucl.ac.uk/results/mu3e_trirec_1_standard_muon_beam.root"

    # Load data
    df = RDataFrame("segs", inputFiles).Filter("mc==1").Define("lam010", "lam01[0]") \
                                       .Define("tan010", "tan01[0]") \
                                       .Define("xPositions", "getHitVar(nhit, x00, x20)") \
                                       .Define("yPositions", "getHitVar(nhit, y00, y20)") \
                                       .Define("zPositions", "getHitVar(nhit, z00, z20)") \
                                       .Range(1000)
    
    # Convert to numpy arrays
    desired_vars = ["nhit", "chi2", "r", "lam010", "tan010", "xPositions", "yPositions", "zPositions"]
    mc_vars = ["mc_mid", "mc_tid", "mc_prime"] # filter contains mc==1 
    all_vars = desired_vars + mc_vars + ["eventId", "seq"]

    data = df.AsNumpy(all_vars)

    # Group all vars by eventId
    eventId_unique = np.unique(data["eventId"])
    eventId_unique = eventId_unique[eventId_unique != -1]
    print(f"Number of events: {len(eventId_unique)}")

    # Get index of each unique event in list comprehension
    event_index = [np.where(data["eventId"] == eventId_unique[i])[0] for i in range(len(eventId_unique))]
                
    # make jagged array of all vars for each event
    data_jagged = {var: np.array([data[var][event_index[i]] for i in range(len(eventId_unique))], dtype=object) for var in all_vars}

    # Normalise these?
    data_jagged["xPositions"] = np.array([[np.array(data_jagged["xPositions"][i][j]) for j in range(len(data_jagged["xPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    data_jagged["yPositions"] = np.array([[np.array(data_jagged["yPositions"][i][j]) for j in range(len(data_jagged["yPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    data_jagged["zPositions"] = np.array([[np.array(data_jagged["zPositions"][i][j]) for j in range(len(data_jagged["zPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    
    # Calculate radius of each hit
    # data_jagged["rHit"] = np.array([[np.sqrt(data_jagged["xPositions"][i][j]**2 + data_jagged["yPositions"][i][j]**2 + data_jagged["zPositions"][i][j]**2) for j in range(len(data_jagged["xPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    # data_jagged["rtHit"] = np.array([[np.sqrt(data_jagged["xPositions"][i][j]**2 + data_jagged["yPositions"][i][j]**2) for j in range(len(data_jagged["xPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    
    # calcuate lambda of each hit
    # data_jagged["lamHit"] = np.array([[np.arctan(data_jagged["zPositions"][i][j] / data_jagged["rtHit"][i][j]) for j in range(len(data_jagged["xPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    
    # calculate phi of each hit
    # data_jagged["phiHit"] = np.array([[np.arctan2(data_jagged["yPositions"][i][j], data_jagged["xPositions"][i][j]) for j in range(len(data_jagged["xPositions"][i]))] for i in range(len(eventId_unique))], dtype=object)
    
    # Get baseline accuracy of using only 'seq' as feature
    # I.e. for 'seq' variable where seq==0 is signal and seq>0 is background (no neural network, just look at seq variable)
    baseline_stats = {"num_seq_0": 0, 
                      "num_correct_seq_0": 0, 
                      "num_incorrect_seq_0": 0, 
                      "num_seq_greater_0": 0, 
                      "num_correct_seq_greater_0": 0, 
                      "num_incorrect_seq_greater_0": 0
                      }
    baseline_stats_per_track_type = {"S4": baseline_stats.copy(), "L6": baseline_stats.copy(), "L8": baseline_stats.copy()}

    for ev in range(len(eventId_unique)):
        for tr in range(len(data_jagged["seq"][ev])):
            # Get track type (S4, L6, L8) from nhit
            if data_jagged["nhit"][ev][tr] == 4:
                track_type = "S4"
            elif data_jagged["nhit"][ev][tr] == 6:
                track_type = "L6"
            elif data_jagged["nhit"][ev][tr] == 8:
                track_type = "L8"

            if data_jagged["seq"][ev][tr] == 0:
                baseline_stats_per_track_type[track_type]["num_seq_0"] += 1
                if data_jagged["mc_prime"][ev][tr] == 1:
                    baseline_stats_per_track_type[track_type]["num_correct_seq_0"] += 1
                else:
                    baseline_stats_per_track_type[track_type]["num_incorrect_seq_0"] += 1
            else:
                baseline_stats_per_track_type[track_type]["num_seq_greater_0"] += 1
                if data_jagged["mc_prime"][ev][tr] == 0:
                    baseline_stats_per_track_type[track_type]["num_correct_seq_greater_0"] += 1
                else:
                    baseline_stats_per_track_type[track_type]["num_incorrect_seq_greater_0"] += 1

    print("Baseline stats:")
    for track_type in baseline_stats_per_track_type:
        print(f"Track type: {track_type}")

        print(f"\tNumber of seq==0: {baseline_stats_per_track_type[track_type]['num_seq_0']}")
        print(f"\tNumber of correct seq==0: {baseline_stats_per_track_type[track_type]['num_correct_seq_0']}")
        print(f"\tNumber of incorrect seq==0: {baseline_stats_per_track_type[track_type]['num_incorrect_seq_0']}")
        print(f"\tAccuracy of seq==0: {baseline_stats_per_track_type[track_type]['num_correct_seq_0'] / baseline_stats_per_track_type[track_type]['num_seq_0'] if baseline_stats_per_track_type[track_type]['num_seq_0'] > 0 else 0}")

        print(f"\tNumber of seq>0: {baseline_stats_per_track_type[track_type]['num_seq_greater_0']}")
        print(f"\tNumber of correct seq>0: {baseline_stats_per_track_type[track_type]['num_correct_seq_greater_0']}")
        print(f"\tNumber of incorrect seq>0: {baseline_stats_per_track_type[track_type]['num_incorrect_seq_greater_0']}")
        print(f"\tAccuracy of seq>0: {baseline_stats_per_track_type[track_type]['num_correct_seq_greater_0'] / baseline_stats_per_track_type[track_type]['num_seq_greater_0'] if baseline_stats_per_track_type[track_type]['num_seq_greater_0'] > 0 else 0}")