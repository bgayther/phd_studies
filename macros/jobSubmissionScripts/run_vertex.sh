source /unix/muons/users/bgayther/setup.sh
source /unix/muons/users/bgayther/mu3e/build/mu3e-activate.sh

mkdir -p /tmp/job_$PBS_JOBID
cd /tmp/job_$PBS_JOBID
mkdir results

OUTDIR_ADJ=$OUTDIR/vertex_${SAMPLE}_${RUN}_${SIM_TAG}_${VERTEX_TAG}/job_$PBS_JOBID
mkdir -p $OUTDIR_ADJ

cp /unix/muons/users/bgayther/mu3e/run/vertex.conf ./
echo $VERTEX_CONF_OPT >> vertex.conf

echo "================= RUNNING VERTEX ==============="
mu3eAnaVertexfit ${REC_FILE} results/mu3e_vertex_${RUN}_${VERTEX_TAG}.root  

cp -r * $OUTDIR_ADJ

rm -rf /tmp/job_$PBS_JOBID
