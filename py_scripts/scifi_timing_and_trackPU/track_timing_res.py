import json
import ROOT
import numpy as np
# import time
from collections import defaultdict
import argparse
# import logging
import os
import sys
from math import log10, floor
# import copy
import tabulate
from tqdm import tqdm
import pathlib
sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of framesTree to process; -1 = ALL')
parser.add_argument('-printTables', dest='printTables', default=1, type=int, help='print tables of efficiencies and etc; default ON')
parser.add_argument('-stackHistos', dest='stackHistos', default=1, type=int, help='save stacked histos of ns/dt by PU type; default ON')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='timing_track_res_analysis', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

# os.chdir(args.path)
ROOT.gROOT.SetBatch(True)
# ROOT.gStyle.SetOptStat(0)


# Create output directory if it doesn't exist
pathlib.Path(args.path).mkdir(parents=True, exist_ok=True)

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

# def fitGaus(h):
#     gaus = ROOT.TF1("gaus","gaus")
#     gaus2 = ROOT.TF1("gaus2", "[0]*exp(-(x-[1])^2/(2*[2]^2)) + [3]*exp(-(x-[1])^2/(2*[4]^2))")
#     gaus2.SetParameter(0, h.GetMaximum() * 0.8)
#     gaus2.SetParameter(1, 0.)
#     gaus2.SetParameter(2, 0.5)
#     gaus2.SetParameter(4, 2.)
#     gaus2.SetParameter(3, h.GetMaximum() * 0.2)
#     h.Fit(gaus, "Q")
#     h.Fit(gaus2, "Q")
#     tRes = gaus.GetParameter("Sigma")
#     tRes2_0 = gaus2.GetParameter(2)
#     tRes2_1 = gaus2.GetParameter(4)
#     return [tRes, tRes2_0, tRes2_1]


############## TRACKER RES ###############
framesTree = ROOT.TChain("frames") 
framesMCTree = ROOT.TChain("frames_mc")

cfg = json.loads(str(ROOT.TFile.Open(args.inputs[0]).Get("config").GetString()))

# for k in cfg["trirec"]["fb"]:
#     print(k, cfg["trirec"]["fb"][k])
# print(cfg)
clsMadeWithFit = True if cfg["trirec"]["fb"]["recluster_using_likelihood"].lower() == "true" else False
recluster_singleTrk_clusters = True if cfg["trirec"]["fb"]["recluster_singleTrk_clusters"].lower() == "true" else False
recluster_multiTrk_clusters = True if cfg["trirec"]["fb"]["recluster_multiTrk_clusters"].lower() == "true" else False

singleTrk_dt_min = float(cfg["trirec"]["fb"]["singleTrk_dt_min"])
singleTrk_dt_max = float(cfg["trirec"]["fb"]["singleTrk_dt_max"])
multiTrk_dt_min = float(cfg["trirec"]["fb"]["multiTrk_dt_min"])
fit_max_trackPU_density = int(cfg["trirec"]["fb"]["fit_max_trackPU_density"])
recluster_nh_min = int(cfg["trirec"]["fb"]["recluster_nh_min"])


for file in args.inputs:
    print ("Adding {} for analysis".format(file))
    framesTree.Add(file)
    framesMCTree.Add(file)

nevents = framesTree.GetEntries()
if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.path + "/" + args.output + ".root", "RECREATE")

framesTree.SetBranchStatus("*", 0)

# Get from detector/digi.json!
fb_sids = 12 # number of fb sensors
frame_ns = 64 # frame length used
# fb_res = 0.250 #250 ps
# tl_res = 0.06 
c = 299.792 # [mm/ns]

min_x = -32.0
max_x = 32.0
res = 0.05
Nbins = int((max_x-min_x)/res)

#### At layer0 res
## S4
h_fb_s4_l0_res = ROOT.TH1D("h_fb_s4_l0_res", "Res", Nbins, min_x, max_x)
## L6
### Fbs
h_fb1_l6_l0_res = ROOT.TH1D("h_fb1_l6_l0_res", "Res", Nbins, min_x, max_x)
h_fb2_l6_l0_res = ROOT.TH1D("h_fb2_l6_l0_res", "Res", Nbins, min_x, max_x)
h_fb_mean_l6_l0_res = ROOT.TH1D("h_fb_mean_l6_l0_res", "Res", Nbins, min_x, max_x)
### Tls
h_tl_l6_l0_res = ROOT.TH1D("h_tl_l6_l0_res", "Res", 800, -0.4, 0.4)
h_tl_fb_l6_l0_res = ROOT.TH1D("h_tl_fb_l6_l0_res", "Res", 800, -0.4, 0.4)
## L8
h_fb1_l8_l0_res = ROOT.TH1D("h_fb1_l8_l0_res", "Res", Nbins, min_x, max_x)
h_fb2_l8_l0_res = ROOT.TH1D("h_fb2_l8_l0_res", "Res", Nbins, min_x, max_x)
h_fb_mean_l8_l0_res = ROOT.TH1D("h_fb_mean_l8_l0_res", "Res", Nbins, min_x, max_x)

#### At hit pos (only fbs)
h_fb_s4_hit_res = ROOT.TH1D("h_fb_s4_hit_res", "Res", Nbins, min_x, max_x)
h_fb1_l6_hit_res = ROOT.TH1D("h_fb1_l6_hit_res", "Res", Nbins, min_x, max_x)
h_fb2_l6_hit_res = ROOT.TH1D("h_fb2_l6_hit_res", "Res", Nbins, min_x, max_x)
h_fb1_l8_hit_res = ROOT.TH1D("h_fb1_l8_hit_res", "Res", Nbins, min_x, max_x)
h_fb2_l8_hit_res = ROOT.TH1D("h_fb2_l8_hit_res", "Res", Nbins, min_x, max_x)


# true time res (only fbs) 
# TODO change names...

h_fb_eff_res = ROOT.TH1D("h_fb_eff_res", "Res", Nbins, min_x, max_x)
h_fb_eff_res_noPU = ROOT.TH1D("h_fb_eff_res_noPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_recurlerPU = ROOT.TH1D("h_fb_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_trackPU = ROOT.TH1D("h_fb_eff_res_trackPU", "Res", Nbins, min_x, max_x)

# All cases
h_fb_s4_eff_res = ROOT.TH1D("h_fb_s4_eff_res", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res = ROOT.TH1D("h_fb1_l6_eff_res", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res = ROOT.TH1D("h_fb2_l6_eff_res", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res = ROOT.TH1D("h_fb1_l8_eff_res", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res = ROOT.TH1D("h_fb2_l8_eff_res", "Res", Nbins, min_x, max_x)

# No PU case
h_fb_s4_eff_res_noPU = ROOT.TH1D("h_fb_s4_eff_res_noPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_noPU = ROOT.TH1D("h_fb1_l6_eff_res_noPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_noPU = ROOT.TH1D("h_fb2_l6_eff_res_noPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_noPU = ROOT.TH1D("h_fb1_l8_eff_res_noPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_noPU = ROOT.TH1D("h_fb2_l8_eff_res_noPU", "Res", Nbins, min_x, max_x)
# Recurler PU case
h_fb_s4_eff_res_recurlerPU = ROOT.TH1D("h_fb_s4_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_recurlerPU = ROOT.TH1D("h_fb1_l6_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_recurlerPU = ROOT.TH1D("h_fb2_l6_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_recurlerPU = ROOT.TH1D("h_fb1_l8_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_recurlerPU = ROOT.TH1D("h_fb2_l8_eff_res_recurlerPU", "Res", Nbins, min_x, max_x)
# Track PU case
h_fb_s4_eff_res_trackPU = ROOT.TH1D("h_fb_s4_eff_res_trackPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_trackPU = ROOT.TH1D("h_fb1_l6_eff_res_trackPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_trackPU = ROOT.TH1D("h_fb2_l6_eff_res_trackPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_trackPU = ROOT.TH1D("h_fb1_l8_eff_res_trackPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_trackPU = ROOT.TH1D("h_fb2_l8_eff_res_trackPU", "Res", Nbins, min_x, max_x)

# Affected
h_fb_eff_res_affected = ROOT.TH1D("h_fb_eff_res_affected", "Resolution of affected fb clusters;t_{fbc} - t_{mc} [ns];", Nbins, min_x, max_x)
h_fb_eff_res_affected_noPU = ROOT.TH1D("h_fb_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_trackPU = ROOT.TH1D("h_fb_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)

# one time present
h_fb_eff_res_affected_one_time = ROOT.TH1D("h_fb_eff_res_affected_one_time", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_one_time_noPU = ROOT.TH1D("h_fb_eff_res_affected_one_time_noPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_one_time_recurlerPU = ROOT.TH1D("h_fb_eff_res_affected_one_time_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_one_time_trackPU = ROOT.TH1D("h_fb_eff_res_affected_one_time_trackPU", "Res", Nbins, min_x, max_x)

# two times present
h_fb_eff_res_affected_both_times = ROOT.TH1D("h_fb_eff_res_affected_both_times", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_noPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_noPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_recurlerPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_trackPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_trackPU", "Res", Nbins, min_x, max_x)

# two times present but other time contains trackPU / choose wrong hit in other time (due to trackPU)
h_fb_eff_res_affected_both_times_otherIsTrkPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_otherIsTrkPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_otherIsTrkPU_noPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_otherIsTrkPU_noPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_otherIsTrkPU_recurlerPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_otherIsTrkPU_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb_eff_res_affected_both_times_otherIsTrkPU_trackPU = ROOT.TH1D("h_fb_eff_res_affected_both_times_otherIsTrkPU_trackPU", "Res", Nbins, min_x, max_x)

# All cases
h_fb_s4_eff_res_affected = ROOT.TH1D("h_fb_s4_eff_res_affected", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_affected = ROOT.TH1D("h_fb1_l6_eff_res_affected", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_affected = ROOT.TH1D("h_fb2_l6_eff_res_affected", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_affected = ROOT.TH1D("h_fb1_l8_eff_res_affected", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_affected = ROOT.TH1D("h_fb2_l8_eff_res_affected", "Res", Nbins, min_x, max_x)

# No PU case
h_fb_s4_eff_res_affected_noPU = ROOT.TH1D("h_fb_s4_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_affected_noPU = ROOT.TH1D("h_fb1_l6_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_affected_noPU = ROOT.TH1D("h_fb2_l6_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_affected_noPU = ROOT.TH1D("h_fb1_l8_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_affected_noPU = ROOT.TH1D("h_fb2_l8_eff_res_affected_noPU", "Res", Nbins, min_x, max_x)
# Recurler PU case
h_fb_s4_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb_s4_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb1_l6_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb2_l6_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb1_l8_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_affected_recurlerPU = ROOT.TH1D("h_fb2_l8_eff_res_affected_recurlerPU", "Res", Nbins, min_x, max_x)
# Track PU case
h_fb_s4_eff_res_affected_trackPU = ROOT.TH1D("h_fb_s4_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)
h_fb1_l6_eff_res_affected_trackPU = ROOT.TH1D("h_fb1_l6_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)
h_fb2_l6_eff_res_affected_trackPU = ROOT.TH1D("h_fb2_l6_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)
h_fb1_l8_eff_res_affected_trackPU = ROOT.TH1D("h_fb1_l8_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)
h_fb2_l8_eff_res_affected_trackPU = ROOT.TH1D("h_fb2_l8_eff_res_affected_trackPU", "Res", Nbins, min_x, max_x)

# Max time span in a cluster

# T1
dt_max_bins = int(frame_ns/1.0)

h_fb_dt = ROOT.TH1D("h_fb_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_dt_clean = ROOT.TH1D("h_fb_dt_clean", "Clean fb cl's;Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_dt_mixedPU = ROOT.TH1D("h_fb_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_dt_recurlerPU = ROOT.TH1D("h_fb_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_dt_trackPU = ROOT.TH1D("h_fb_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb_singleTrk_dt = ROOT.TH1D("h_fb_singleTrk_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_singleTrk_dt_clean = ROOT.TH1D("h_fb_singleTrk_dt_clean", "Clean fb cl's;Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_singleTrk_dt_mixedPU = ROOT.TH1D("h_fb_singleTrk_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_singleTrk_dt_recurlerPU = ROOT.TH1D("h_fb_singleTrk_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_singleTrk_dt_trackPU = ROOT.TH1D("h_fb_singleTrk_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb_multiTrk_dt = ROOT.TH1D("h_fb_multiTrk_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_multiTrk_dt_clean = ROOT.TH1D("h_fb_multiTrk_dt_clean", "Clean fb cl's;Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_multiTrk_dt_mixedPU = ROOT.TH1D("h_fb_multiTrk_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_multiTrk_dt_recurlerPU = ROOT.TH1D("h_fb_multiTrk_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb_multiTrk_dt_trackPU = ROOT.TH1D("h_fb_multiTrk_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb1_dt = ROOT.TH1D("h_fb1_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_dt_clean = ROOT.TH1D("h_fb1_dt_clean", "Clean fb cl's;Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_dt_mixedPU = ROOT.TH1D("h_fb1_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_dt_recurlerPU = ROOT.TH1D("h_fb1_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_dt_trackPU = ROOT.TH1D("h_fb1_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb1_s4_dt = ROOT.TH1D("h_fb1_s4_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_s4_dt_clean = ROOT.TH1D("h_fb1_s4_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_s4_dt_mixedPU = ROOT.TH1D("h_fb1_s4_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_s4_dt_recurlerPU = ROOT.TH1D("h_fb1_s4_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_s4_dt_trackPU = ROOT.TH1D("h_fb1_s4_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb1_l6F_dt = ROOT.TH1D("h_fb1_l6F_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6F_dt_clean = ROOT.TH1D("h_fb1_l6F_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6F_dt_mixedPU = ROOT.TH1D("h_fb1_l6F_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6F_dt_recurlerPU = ROOT.TH1D("h_fb1_l6F_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6F_dt_trackPU = ROOT.TH1D("h_fb1_l6F_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb1_l6T_dt = ROOT.TH1D("h_fb1_l6T_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6T_dt_clean = ROOT.TH1D("h_fb1_l6T_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6T_dt_mixedPU = ROOT.TH1D("h_fb1_l6T_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6T_dt_recurlerPU = ROOT.TH1D("h_fb1_l6T_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l6T_dt_trackPU = ROOT.TH1D("h_fb1_l6T_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb1_l8_dt = ROOT.TH1D("h_fb1_l8_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l8_dt_clean = ROOT.TH1D("h_fb1_l8_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l8_dt_mixedPU = ROOT.TH1D("h_fb1_l8_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l8_dt_recurlerPU = ROOT.TH1D("h_fb1_l8_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb1_l8_dt_trackPU = ROOT.TH1D("h_fb1_l8_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

# T2
h_fb2_dt = ROOT.TH1D("h_fb2_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_dt_clean = ROOT.TH1D("h_fb2_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_dt_mixedPU = ROOT.TH1D("h_fb2_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_dt_recurlerPU = ROOT.TH1D("h_fb2_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_dt_trackPU = ROOT.TH1D("h_fb2_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb2_l6_dt = ROOT.TH1D("h_fb2_l6_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l6_dt_clean = ROOT.TH1D("h_fb2_l6_dt_clean", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l6_dt_mixedPU = ROOT.TH1D("h_fb2_l6_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l6_dt_recurlerPU = ROOT.TH1D("h_fb2_l6_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l6_dt_trackPU = ROOT.TH1D("h_fb2_l6_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

h_fb2_l8_dt = ROOT.TH1D("h_fb2_l8_dt", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l8_dt_clean = ROOT.TH1D("h_fb2_l8_dt_clean", "v", dt_max_bins, 0, frame_ns)
h_fb2_l8_dt_mixedPU = ROOT.TH1D("h_fb2_l8_dt_mixedPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l8_dt_recurlerPU = ROOT.TH1D("h_fb2_l8_dt_recurlerPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)
h_fb2_l8_dt_trackPU = ROOT.TH1D("h_fb2_l8_dt_trackPU", ";Latest - earliest hit in fb cluster [ns];", dt_max_bins, 0, frame_ns)

def fillTimeSpanHistos(fbc_dt, fbc_PU_type, h_dt, h_dt_clean, h_dt_recurlerPU, h_dt_trackPU, h_dt_mixedPU): 
    h_dt.Fill(fbc_dt)

    if (fbc_PU_type == 0): 
        h_dt_clean.Fill(fbc_dt)
    elif (fbc_PU_type == 1): 
        h_dt_recurlerPU.Fill(fbc_dt)
    elif (fbc_PU_type == 2): 
        h_dt_trackPU.Fill(fbc_dt)
    elif (fbc_PU_type == 3): 
        h_dt_mixedPU.Fill(fbc_dt)


h_fbs_dt_vs_pl = ROOT.TH2D("h_fbs_dt_vs_pl", "MC L6/L8 tracks;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)

h_fbs_dt_vs_pl_L6 = ROOT.TH2D("h_fbs_dt_vs_pl_L6", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fbs_dt_vs_pl_L8 = ROOT.TH2D("h_fbs_dt_vs_pl_L8", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fb_tl_dt_vs_pl = ROOT.TH2D("h_fb_tl_dt_vs_pl", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)

h_fbs_dt_vs_pl_L6_wrong_reco_p = ROOT.TH2D("h_fbs_dt_vs_pl_L6_wrong_reco_p", "L6 (wrong reco p);(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fbs_dt_vs_pl_L8_wrong_reco_p = ROOT.TH2D("h_fbs_dt_vs_pl_L8_wrong_reco_p", "L8 (wrong reco p);(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fb_tl_dt_vs_pl_wrong_reco_p = ROOT.TH2D("h_fb_tl_dt_vs_pl_wrong_reco_p", "L6 (wrong reco p);(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)

h_fbs_dt_vs_pl_fake_tracks = ROOT.TH2D("h_fbs_dt_vs_pl_fake_tracks", "Fake L6/L8 tracks;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fbs_dt_vs_pl_long_corr_reco_p = ROOT.TH2D("h_fbs_dt_vs_pl_long_corr_reco_p", "MC L6/L8 tracks (correct reco p);(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_fbs_dt_vs_pl_long_wrong_reco_p = ROOT.TH2D("h_fbs_dt_vs_pl_long_wrong_reco_p", "MC L6/L8 tracks (incorrect reco p);(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)


# Tile dt
h_tl_dt = ROOT.TH1D("h_tl_dt", ";Latest - earliest hit in tl cluster [ns];", dt_max_bins, 0, frame_ns)
h_tl_dt_noPU = ROOT.TH1D("h_tl_dt_noPU", ";Latest - earliest hit in tl cluster [ns];", 20, 0, 2)
h_tl_dt_PU = ROOT.TH1D("h_tl_dt_PU", ";Latest - earliest hit in tl cluster [ns];", dt_max_bins, 0, frame_ns)

# Eff plots
z_range = 150.0
z_width = 10.0
z_bins =  int((2.0 * z_range ) / z_width)
eff_list = []

h_t1_eff_vs_z = ROOT.TEfficiency("h_t1_eff_vs_z", "S4/L6/L8;Z position of fbc T_{1} [mm]; T_{1} efficiency ", z_bins, -z_range, z_range)
h_fb_t2_eff_vs_z = ROOT.TEfficiency("h_fb_t2_eff_vs_z", "Both L6/L8;Z position of fbc T_{2} [mm]; fb T_{2} efficiency ", z_bins, -z_range, z_range)
h_tl_t2_eff_vs_z = ROOT.TEfficiency("h_tl_t2_eff_vs_z", ";Z position of tl T_{2} [mm]; tl T_{2} efficiency", 100, -400, 400)
h_fb_l6_t2_eff_vs_z = ROOT.TEfficiency("h_fb_l6_t2_eff_vs_z", "L6;Z position of fbc T_{2} [mm]; fb T_{2} efficiency ", z_bins, -z_range, z_range)
h_fb_l8_t2_eff_vs_z = ROOT.TEfficiency("h_fb_l8_t2_eff_vs_z", "L8;Z position of fbc T_{2} [mm]; fb T_{2} efficiency ", z_bins, -z_range, z_range)
eff_list.extend([h_t1_eff_vs_z,h_fb_t2_eff_vs_z, h_tl_t2_eff_vs_z, h_fb_l6_t2_eff_vs_z, h_fb_l8_t2_eff_vs_z])

h_t1_t2_eff_vs_z_diff = ROOT.TEfficiency("h_t1_t2_eff_vs_z_diff", "L6/L8;Z position of fbc/tl T_{2} - Z position of fbc T_{1} [mm]; T_{1} && T_{2} efficiency ", z_bins, -200, 200)
h_t1_t2_tl_eff_vs_z_diff = ROOT.TEfficiency("h_t1_t2_tl_eff_vs_z_diff", "L6 (fb + tl);Z position of tl T_{2} - Z position of fbc T_{1} [mm]; T_{1} && T_{2} efficiency ", z_bins, -200, 200)
h_t1_t2_fb_l6_eff_vs_z_diff = ROOT.TEfficiency("h_t1_t2_fb_l6_eff_vs_z_diff", "L6 (fb only);Z position of fbc T_{2} - Z position of fbc T_{1} [mm]; T_{1} && T_{2} efficiency ", z_bins, -200, 200)
h_t1_t2_fb_l8_eff_vs_z_diff = ROOT.TEfficiency("h_t1_t2_fb_l8_eff_vs_z_diff", "L8;Z position of fbc T_{2} - Z position of fbc T_{1} [mm]; T_{1} && T_{2} efficiency ", z_bins, -200, 200)
eff_list.extend([h_t1_t2_eff_vs_z_diff,h_t1_t2_tl_eff_vs_z_diff, h_t1_t2_fb_l6_eff_vs_z_diff, h_t1_t2_fb_l8_eff_vs_z_diff])

h_tl_t2_edep_diff = ROOT.TEfficiency("h_tl_t2_edep_diff","L6 (tl); Edep (max - min); tl T_{2} efficiency", 200, 0, 20)
h_tl_t2_eff_vs_n = ROOT.TEfficiency("h_tl_t2_eff_vs_n", ";Number of hits in cluster; tl T_{2} efficiency", 10, 0, 10)
eff_list.extend([h_tl_t2_edep_diff, h_tl_t2_eff_vs_n])

# Time diff between timing hits
t_range = 5.0
t_width = 0.5
t_bins = int((2.0*t_range) / t_width)
h_t1_t2_fb_l6_eff_vs_t_diff = ROOT.TEfficiency("h_t1_t2_fb_l6_eff_vs_t_diff", "L6;fbc T_{2} - fbc T_{1} [ns]; T_{1} && T_{2} efficiency ", t_bins, -t_range, t_range)
h_t1_t2_fb_l8_eff_vs_t_diff = ROOT.TEfficiency("h_t1_t2_fb_l8_eff_vs_t_diff", "L8;fbc T_{2} - fbc T_{1} [ns]; T_{1} && T_{2} efficiency ", t_bins, -t_range, t_range)
h_t1_t2_fb_eff_vs_t_diff = ROOT.TEfficiency("h_t1_t2_fb_eff_vs_t_diff", "L6/L8;fbc T_{2} - fbc T_{1} [ns]; T_{1} && T_{2} efficiency ", t_bins, -t_range, t_range)
h_t1_t2_tl_eff_vs_t_diff = ROOT.TEfficiency("h_t1_t2_tl_eff_vs_t_diff", "L6 (fb + tl);tl T_{2} - fbc T_{1} [ns]; T_{1} && T_{2} efficiency ", t_bins, -t_range, t_range)
eff_list.extend([h_t1_t2_fb_l6_eff_vs_t_diff,h_t1_t2_fb_l8_eff_vs_t_diff, h_t1_t2_fb_eff_vs_t_diff, h_t1_t2_tl_eff_vs_t_diff])

# Eff for only one/two timing hits (fbs only)
h_fb_eff_vs_times_present = ROOT.TEfficiency("h_fb_eff_vs_times_present", ";; Time efficiency ", 2, 0, 2)

h_fb_eff_affected_vs_times_present = ROOT.TEfficiency("h_fb_eff_affected_vs_times_present", ";; Time efficiency ", 2, 0, 2)
h_fb_eff_affected_vs_times_cl_loss_denominator = ROOT.TEfficiency("h_fb_eff_affected_vs_times_cl_loss_denominator", ";; Time efficiency ", 2, 0, 2)
h_fb_eff_affected_vs_times_present_tr_type = ROOT.TEfficiency("h_fb_eff_affected_vs_times_present_tr_type", ";; Time efficiency ", 3, 0, 3)
h_fb_eff_affected_vs_times_present_time_type = ROOT.TEfficiency("h_fb_eff_affected_vs_times_present_time_type", ";; Time efficiency ", 3, 0, 3)
h_fb_eff_affected_vs_times_present_but_not_fitCl = ROOT.TEfficiency("h_fb_eff_affected_vs_times_present_but_not_fitCl", ";; Time efficiency ", 2, 0, 2)

h_fb_eff_affected_vs_lam = ROOT.TEfficiency("h_fb_eff_affected_vs_lam", ";#lambda [rad]; Time efficiency", 14, -3.5, 3.5)
h_fb_eff_affected_vs_phi = ROOT.TEfficiency("h_fb_eff_affected_vs_phi", "#phi [rad]", 14, -3.5, 3.5)
h_fb_eff_affected_vs_p = ROOT.TEfficiency("h_fb_eff_affected_vs_p", "p [MeV/#it{c}]", 20, 0, 60)
h_fb_eff_affected_vs_z1 = ROOT.TEfficiency("h_fb_eff_affected_vs_z1", "z position of T1 [mm]", 20, -150, 150)
h_fb_eff_affected_vs_z2 = ROOT.TEfficiency("h_fb_eff_affected_vs_z2", "z position of T2 [mm]", 20, -150, 150)
h_fb_eff_affected_vs_z_diff = ROOT.TEfficiency("h_fb_eff_affected_vs_z_diff", "difference in Z [mm]", 40, -200, 200)
h_fb_eff_affected_vs_mc_vt = ROOT.TEfficiency("h_fb_eff_affected_vs_mc_vt", "vertex origin time [ns]", 32, 0, 64)

eff_list.extend([h_fb_eff_vs_times_present,h_fb_eff_affected_vs_times_present,h_fb_eff_affected_vs_times_cl_loss_denominator, h_fb_eff_affected_vs_times_present_but_not_fitCl,\
    h_fb_eff_affected_vs_times_present_tr_type,h_fb_eff_affected_vs_times_present_time_type, h_fb_eff_affected_vs_lam, h_fb_eff_affected_vs_phi, h_fb_eff_affected_vs_p, \
    h_fb_eff_affected_vs_z1, h_fb_eff_affected_vs_z2, h_fb_eff_affected_vs_z_diff, h_fb_eff_affected_vs_mc_vt])

# Time diff within clusters 
h_fb_eff_vs_dt_cl = ROOT.TEfficiency("h_fb_eff_vs_dt_cl", "S4/L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_eff_vs_dt_cl_both_present = ROOT.TEfficiency("h_fb_eff_vs_dt_cl_both_present", "L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_eff_vs_dt_cl_one_present = ROOT.TEfficiency("h_fb_eff_vs_dt_cl_one_present", "S4/L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_eff_affected_vs_dt_cl = ROOT.TEfficiency("h_fb_eff_affected_vs_dt_cl", "S4/L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_eff_affected_vs_dt_cl_both_present = ROOT.TEfficiency("h_fb_eff_affected_vs_dt_cl_both_present", "L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_eff_affected_vs_dt_cl_one_present = ROOT.TEfficiency("h_fb_eff_affected_vs_dt_cl_one_present", "S4/L6/L8 T1/T2;Max - min hit time within fbc [ns]; Time efficiency ", 13, 0, 65)
h_fb_t1_eff_vs_dt_cl = ROOT.TEfficiency("h_fb_t1_eff_vs_dt_cl", "S4/L6/L8;Max - min hit time within fbc T_{1} [ns]; T_{1} efficiency ", 13, 0, 65)
h_fb_t2_eff_vs_dt_cl = ROOT.TEfficiency("h_fb_t2_eff_vs_dt_cl", "L6/L8;Max - min hit time within fbc T_{2} [ns]; T_{2} efficiency ", 13, 0, 65)
h_fb_l6_t2_eff_vs_dt_cl = ROOT.TEfficiency("h_fb_l6_t2_eff_vs_dt_cl", "L6;Max - min hit time within fbc T_{2} [ns]; T_{2} efficiency ", 13, 0, 65)
h_fb_l8_t2_eff_vs_dt_cl = ROOT.TEfficiency("h_fb_l8_t2_eff_vs_dt_cl", "L8;Max - min hit time within fbc T_{2} [ns]; T_{2} efficiency ", 13, 0, 65)
h_tl_t2_eff_vs_dt_cl = ROOT.TEfficiency("h_tl_t2_eff_vs_dt_cl", "L6 (fb + tl);Max - min hit time within tl cluster T_{2} [ns]; T_{2} efficiency ", 20, 0, 2)
h_tl_t1_eff_vs_dt_cl = ROOT.TEfficiency("h_tl_t1_eff_vs_dt_cl", "L6 (fb + tl);Max - min hit time within fb cluster T_{1} [ns]; T_{1} efficiency ", 13, 0, 65)
eff_list.extend([h_fb_eff_vs_dt_cl, h_fb_eff_vs_dt_cl_both_present, h_fb_eff_vs_dt_cl_one_present, h_fb_eff_affected_vs_dt_cl, h_fb_eff_affected_vs_dt_cl_both_present, h_fb_eff_affected_vs_dt_cl_one_present, h_fb_t1_eff_vs_dt_cl,h_fb_t2_eff_vs_dt_cl, h_fb_l6_t2_eff_vs_dt_cl, h_fb_l8_t2_eff_vs_dt_cl, h_tl_t2_eff_vs_dt_cl, h_tl_t1_eff_vs_dt_cl])

# vs mc lam angle
h_t1_t2_fb_l6_eff_vs_lam = ROOT.TEfficiency("h_t1_t2_fb_l6_eff_vs_lam", "L6;MC lambda angle; T_{1} && T_{2} efficiency ", 32, -1.6, 1.6)
h_t1_t2_fb_l8_eff_vs_lam = ROOT.TEfficiency("h_t1_t2_fb_l8_eff_vs_lam", "L8;MC lambda angle; T_{1} && T_{2} efficiency ", 32, -1.6, 1.6)
h_t1_t2_fb_eff_vs_lam = ROOT.TEfficiency("h_t1_t2_fb_eff_vs_lam", "L6/L8;MC lambda angle; T_{1} && T_{2} efficiency ", 32, -1.6, 1.6)
h_t1_t2_tl_eff_vs_lam = ROOT.TEfficiency("h_t1_t2_tl_eff_vs_lam", "L6 (fb + tl);MC lambda angle; T_{1} && T_{2} efficiency ", 32, -1.6, 1.6)
eff_list.extend([h_t1_t2_fb_l6_eff_vs_lam,h_t1_t2_fb_l8_eff_vs_lam, h_t1_t2_fb_eff_vs_lam, h_t1_t2_tl_eff_vs_lam])

# Dt vs ds plots for t1/t2 corr/incorr
h_L6F_dt_vs_ds_both_correct = ROOT.TH2D("h_L6F_dt_vs_ds_both_correct", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_both_incorr = ROOT.TH2D("h_L6F_dt_vs_ds_both_incorr", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_corr_t2_incorr = ROOT.TH2D("h_L6F_dt_vs_ds_t1_corr_t2_incorr", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_incorr_t2_corr = ROOT.TH2D("h_L6F_dt_vs_ds_t1_incorr_t2_corr", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)

h_L8_dt_vs_ds_both_correct = ROOT.TH2D("h_L8_dt_vs_ds_both_correct", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_both_incorr = ROOT.TH2D("h_L8_dt_vs_ds_both_incorr", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_corr_t2_incorr = ROOT.TH2D("h_L8_dt_vs_ds_t1_corr_t2_incorr", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_incorr_t2_corr = ROOT.TH2D("h_L8_dt_vs_ds_t1_incorr_t2_corr", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)

h_L6T_dt_vs_ds_both_correct = ROOT.TH2D("h_L6T_dt_vs_ds_both_correct", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_both_incorr = ROOT.TH2D("h_L6T_dt_vs_ds_both_incorr", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_t1_corr_t2_incorr = ROOT.TH2D("h_L6T_dt_vs_ds_t1_corr_t2_incorr", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_t1_incorr_t2_corr = ROOT.TH2D("h_L6T_dt_vs_ds_t1_incorr_t2_corr", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)

# Dt vs ds plots by source of incorrectness    
h_L6F_dt_vs_ds_both_incorr_recurlerPU = ROOT.TH2D("h_L6F_dt_vs_ds_both_incorr_recurlerPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5) 
h_L6F_dt_vs_ds_both_incorr_recurlerPU_trackPU = ROOT.TH2D("h_L6F_dt_vs_ds_both_incorr_recurlerPU_trackPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_both_incorr_trackPU_recurlerPU = ROOT.TH2D("h_L6F_dt_vs_ds_both_incorr_trackPU_recurlerPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_both_incorr_trackPU = ROOT.TH2D("h_L6F_dt_vs_ds_both_incorr_trackPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_corr_t2_incorr_recurlerPU = ROOT.TH2D("h_L6F_dt_vs_ds_t1_corr_t2_incorr_recurlerPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_corr_t2_incorr_trackPU = ROOT.TH2D("h_L6F_dt_vs_ds_t1_corr_t2_incorr_trackPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_incorr_t2_corr_recurlerPU = ROOT.TH2D("h_L6F_dt_vs_ds_t1_incorr_t2_corr_recurlerPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6F_dt_vs_ds_t1_incorr_t2_corr_trackPU = ROOT.TH2D("h_L6F_dt_vs_ds_t1_incorr_t2_corr_trackPU", "L6;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)

h_L8_dt_vs_ds_both_incorr_recurlerPU = ROOT.TH2D("h_L8_dt_vs_ds_both_incorr_recurlerPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_both_incorr_recurlerPU_trackPU = ROOT.TH2D("h_L8_dt_vs_ds_both_incorr_recurlerPU_trackPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_both_incorr_trackPU_recurlerPU = ROOT.TH2D("h_L8_dt_vs_ds_both_incorr_trackPU_recurlerPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_both_incorr_trackPU = ROOT.TH2D("h_L8_dt_vs_ds_both_incorr_trackPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_corr_t2_incorr_recurlerPU = ROOT.TH2D("h_L8_dt_vs_ds_t1_corr_t2_incorr_recurlerPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_corr_t2_incorr_trackPU = ROOT.TH2D("h_L8_dt_vs_ds_t1_corr_t2_incorr_trackPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_incorr_t2_corr_recurlerPU = ROOT.TH2D("h_L8_dt_vs_ds_t1_incorr_t2_corr_recurlerPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)
h_L8_dt_vs_ds_t1_incorr_t2_corr_trackPU = ROOT.TH2D("h_L8_dt_vs_ds_t1_incorr_t2_corr_trackPU", "L8;(s_{fb,2} #minus  s_{fb,1}) [mm];t_{fb,2} #minus  t_{fb,1} [ns]", 100, 0, 1000, 100, -5, 5)

h_L6T_dt_vs_ds_both_incorr_recurlerPU = ROOT.TH2D("h_L6T_dt_vs_ds_both_incorr_recurlerPU", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_both_incorr_trackPU = ROOT.TH2D("h_L6T_dt_vs_ds_both_incorr_trackPU", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_t1_incorr_t2_corr_recurlerPU = ROOT.TH2D("h_L6T_dt_vs_ds_t1_incorr_t2_corr_recurlerPU", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)
h_L6T_dt_vs_ds_t1_incorr_t2_corr_trackPU = ROOT.TH2D("h_L6T_dt_vs_ds_t1_incorr_t2_corr_trackPU", "L6;(s_{tl} #minus  s_{fb}) [mm];t_{tl} #minus  t_{fb} [ns]", 100, 0, 1000, 100, -5, 5)

# Do this for T1/T2 and for S4/L6F/L6T/L8??
h_fb_ns = ROOT.TH1D("h_fb_ns", "Num fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)
h_fb_ns_clean = ROOT.TH1D("h_fb_ns_clean", "Num clean fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)
h_fb_ns_recurlerPU = ROOT.TH1D("h_fb_ns_recurlerPU", "Num recurlerPU fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)
h_fb_ns_trackPU = ROOT.TH1D("h_fb_ns_trackPU", "Num trackPU fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)
h_fb1_l6T_ns_trackPU = ROOT.TH1D("h_fb1_l6T_ns_trackPU", "Num trackPU fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)
h_fb_ns_mixedPU = ROOT.TH1D("h_fb_ns_mixedPU", "Num Mixed PU fb cl's vs number of segs assigned to cl", 6, 0.5, 6.5)

# dt vs ns (do this for specific time crossing in tracks?)
h_dt_vs_ns = ROOT.TH2D("h_dt_vs_ns", ";numb segs assigned to cl;dt [ns]", 6, 0.6, 6.5, int(frame_ns/0.5), 0, frame_ns)
h_dt_vs_ns_clean = ROOT.TH2D("h_dt_vs_ns_clean", ";numb segs assigned to cl;dt [ns]", 6, 0.6, 6.5, int(frame_ns/0.5), 0, frame_ns)
h_dt_vs_ns_recurlerPU = ROOT.TH2D("h_dt_vs_ns_recurlerPU", ";numb segs assigned to cl;dt [ns]", 6, 0.6, 6.5, int(frame_ns/0.5), 0, frame_ns)
h_dt_vs_ns_trackPU = ROOT.TH2D("h_dt_vs_ns_trackPU", ";numb segs assigned to cl;dt [ns]", 6, 0.6, 6.5, int(frame_ns/0.5), 0, frame_ns)
h_dt_vs_ns_mixedPU = ROOT.TH2D("h_dt_vs_ns_mixedPU", ";numb segs assigned to cl;dt [ns]", 6, 0.6, 6.5, int(frame_ns/0.5), 0, frame_ns)

framesTree.SetBranchStatus("*", 0)
framesTree.SetBranchStatus("n", 1)
framesTree.SetBranchStatus("nhit", 1)
framesTree.SetBranchStatus("n4", 1)
framesTree.SetBranchStatus("n6", 1)
framesTree.SetBranchStatus("n8", 1)
framesTree.SetBranchStatus("r", 1)
framesTree.SetBranchStatus("p", 1)
framesTree.SetBranchStatus("eventId", 1)

framesTree.SetBranchStatus("mc", 1)
framesTree.SetBranchStatus("mc_pid", 1)
framesTree.SetBranchStatus("mc_phi", 1)
framesTree.SetBranchStatus("mc_p", 1)
framesTree.SetBranchStatus("mc_pt", 1)
framesTree.SetBranchStatus("mc_lam", 1)
framesTree.SetBranchStatus("mc_vr", 1)
framesTree.SetBranchStatus("mc_vz", 1)
framesTree.SetBranchStatus("mc_vt", 1)
framesTree.SetBranchStatus("mc_prime", 1)

framesTree.SetBranchStatus("t0", 1)
framesTree.SetBranchStatus("t0_mc", 1)

framesTree.SetBranchStatus("t0_tl", 1)
framesTree.SetBranchStatus("t0_fb", 1)
framesTree.SetBranchStatus("t0_fb1", 1)
framesTree.SetBranchStatus("t0_fb2", 1)

framesTree.SetBranchStatus("tl_t", 1)

framesTree.SetBranchStatus("mc_fb_crossings", 1)

framesTree.SetBranchStatus("fbc_t1_z", 1)
framesTree.SetBranchStatus("fbc_t2_z", 1)

framesTree.SetBranchStatus("fbc_t1_ns", 1)
framesTree.SetBranchStatus("fbc_t2_ns", 1)
framesTree.SetBranchStatus("fbc_t1_nh", 1)
framesTree.SetBranchStatus("fbc_t2_nh", 1)

framesTree.SetBranchStatus("fbc_t1", 1)
framesTree.SetBranchStatus("fbc_t2", 1)

framesTree.SetBranchStatus("fbc_t1_tmc", 1)
framesTree.SetBranchStatus("fbc_t2_tmc", 1)

# framesTree.SetBranchStatus("fbc_t1_no_recurler_PU", 1)
# framesTree.SetBranchStatus("fbc_t2_no_recurler_PU", 1)

# framesTree.SetBranchStatus("fbc_t1_one_tid", 1)
# framesTree.SetBranchStatus("fbc_t2_one_tid", 1)

framesTree.SetBranchStatus("fbc_t1_prev_frame", 1)
framesTree.SetBranchStatus("fbc_t2_prev_frame", 1)

framesTree.SetBranchStatus("fbc_t1_PU_type", 1)
framesTree.SetBranchStatus("fbc_t2_PU_type", 1)

framesTree.SetBranchStatus("fbc_t1_madeWithFit", 1)
framesTree.SetBranchStatus("fbc_t2_madeWithFit", 1)

framesTree.SetBranchStatus("fbc_t1_origClTrPU", 1)
framesTree.SetBranchStatus("fbc_t2_origClTrPU", 1)

framesTree.SetBranchStatus("fbc_t1_passMultiTrkCut", 1)
framesTree.SetBranchStatus("fbc_t2_passMultiTrkCut", 1)

framesTree.SetBranchStatus("t1_wouldBeAffected", 1)
framesTree.SetBranchStatus("t2_wouldBeAffected", 1)

framesTree.SetBranchStatus("t1_wouldBeAffectedMultiTrk", 1)
framesTree.SetBranchStatus("t2_wouldBeAffectedMultiTrk", 1)

framesTree.SetBranchStatus("t1_wouldBeAffectedSingleTrk", 1)
framesTree.SetBranchStatus("t2_wouldBeAffectedSingleTrk", 1)

framesTree.SetBranchStatus("fbc_dt_t1", 1)
framesTree.SetBranchStatus("fbc_dt_t2", 1)

framesTree.SetBranchStatus("fbc_t1_pl", 1)
framesTree.SetBranchStatus("fbc_t2_pl", 1)

framesTree.SetBranchStatus("tl_t", 1)
framesTree.SetBranchStatus("tl_n", 1)
framesTree.SetBranchStatus("tl_dt", 1)
framesTree.SetBranchStatus("tl_s", 1)
framesTree.SetBranchStatus("tl_z", 1)
framesTree.SetBranchStatus("tl_noPU", 1)
framesTree.SetBranchStatus("tl_edep_sum", 1)
framesTree.SetBranchStatus("tl_edep_diff", 1)

framesTree.SetBranchStatus("t1_isCorrect", 1)
framesTree.SetBranchStatus("t2_isCorrect", 1)

framesTree.SetBranchStatus("t1_isBest", 1)
framesTree.SetBranchStatus("t2_isBest", 1)

framesTree.SetBranchStatus("fbc_t1_true", 1)
framesTree.SetBranchStatus("fbc_t2_true", 1)

framesTree.SetBranchStatus("t1_isPossible", 1)
framesTree.SetBranchStatus("t2_tl_isPossible", 1)
framesTree.SetBranchStatus("t2_fb_isPossible", 1)

framesTree.SetBranchStatus("t1_incorrect_origin", 1)
framesTree.SetBranchStatus("t2_incorrect_origin", 1)

ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
# ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
ROOT.RooMsgService.instance().setSilentMode(True)

def fitGaus(h):
    if h.Integral() == 0:
        return 0.999, 0.001
    # Different fits for hit/L0 resolutions?

    x = ROOT.RooRealVar("x", "fbc time - mc time [ns]", min_x, max_x)
    x.setRange("fit", -1, 1)
    data = ROOT.RooDataHist("data","data",x,h)

    mean = ROOT.RooRealVar("#mu","mean1",-0.2, -3,3)
    sigma1 = ROOT.RooRealVar("#sigma_{1}","sigma1",0.25,0.01,1.0)
    gauss1 = ROOT.RooGaussian("g1","g",x,mean,sigma1)

    # make shared mean?
    # mean2 = ROOT.RooRealVar("mean2","mean",0, min_x, max_x)
    sigma2 = ROOT.RooRealVar("#sigma_{2}","sigma",0.5,0.01,2.0)
    gauss2 = ROOT.RooGaussian("g2","g",x,mean,sigma2)

    # alphacb = ROOT.RooRealVar("alphacb","alphacb",-1,-5,5)
    # ncb = ROOT.RooRealVar("ncb","ncb",1,0.01,h.GetEntries())
    # cb = ROOT.RooCBShape("cb2","cb",x,mean,sigma2,alphacb,ncb)
    # model = cb

    frac = ROOT.RooRealVar("f_{1,2}","fraction",0.9,0.0,1.0)
    model = ROOT.RooAddPdf("sum","g1+g2",ROOT.RooArgList(gauss1,gauss2), ROOT.RooArgList(frac))

    model.fitTo(data, ROOT.RooFit.Range("fit"))
    # print("DONE FITTING (Entries in hist {}, name {})".format(h.GetEntries(), h.GetName()))
    # frac.Print()
    # mean1.Print()
    # sigma1.Print()
    # mean2.Print()
    # sigma2.Print()
    # alphacb.Print()
    # ncb.Print()

    frame = x.frame()
    frame.SetYTitle("Events")
    frame.SetXTitle("Fibre Cluster Time - MC Time [ns]")
    frame.SetTitle("")
    data.plotOn(frame, ROOT.RooFit.DataError(0))
    model.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kC0))#ROOT.RooFit.LineStyle(ROOT.kDashed)
    model.paramOn(frame, ROOT.RooFit.Format("NEU", ROOT.RooFit.AutoPrecision(1)), ROOT.RooFit.Layout(0.6,0.83,0.85), ROOT.RooFit.ShowConstants(False))
    frame.getAttText().SetTextSize(0.95*frame.GetYaxis().GetLabelSize())
    # model.plotOn(frame, ROOT.RooFit.Components("g1"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kC1))
    # model.plotOn(frame, ROOT.RooFit.Components("g2"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kC2))
    # model.plotOn(frame, ROOT.RooFit.Components("cb2"), ROOT.RooFit.LineStyle(ROOT.kDashed), ROOT.RooFit.LineColor(ROOT.kC2))
    frame.SetMinimum(2)
    can = ROOT.TCanvas("can", "")
    frame.Draw()
    can.SetLogy()
    frame.GetXaxis().SetRangeUser(-10, 10)
    can.SaveAs(args.path+"/"+h.GetName()+".pdf")
    # input("Press Enter to continue...")

    coreResolutionVal = sigma1.getVal()
    coreResolutionErr = sigma1.getError()
    if frac.getVal()<0.7 and (sigma2.getVal()<sigma1.getVal()):
        coreResolutionVal = sigma2.getVal()
        coreResolutionErr = sigma2.getError()
    
    # print("CORE RESOLUTION {}".format(coreResolutionVal))
    return float(coreResolutionVal), float(coreResolutionErr)

track_counter = 0.0
positron = -11
electron = 11

INCORRECT_CL = 0
RECURLER_PU = 1
TRACK_PU = 2
MIXED_PU = 3

# TODO: use t2_tl_possible...

#### Analysis loop ####
# nevents = 20000
for j in tqdm(range(0, nevents)):
    framesTree.GetEntry(j)
    # if j%100 == 0:
        # print ("(%6i/%i) %5.1f%% \r" % (j, nevents, 100.*j/nevents), sys.stdout.flush())

    for i in range(framesTree.n):
        if (framesTree.mc_prime[i] != 1): continue
        # if (framesTree.mc_prime[i] != 1 or framesTree.t0_mc[i] == 0.0 or framesTree.mc_pt[i] < 10 \
        #     or abs(framesTree.mc_vz[i]) > 50 or framesTree.mc_vr[i] > 19): continue        
        track_counter += 1.0

        nhit = framesTree.nhit[i]
        mc_vz = framesTree.mc_vz[i]

        t0 = framesTree.t0[i]
        t0_mc = framesTree.t0_mc[i]

        t0_tl = framesTree.t0_tl[i]

        t0_fb = framesTree.t0_fb[i]
        t0_fb1 = framesTree.t0_fb1[i]
        t0_fb2 = framesTree.t0_fb2[i]

        fb_l0_res = t0_fb - t0_mc  
        fb1_l0_res = t0_fb1 - t0_mc
        fb2_l0_res = t0_fb2 - t0_mc 

        fb_t1 = framesTree.fbc_t1[i]
        fb_t2 = framesTree.fbc_t2[i]

        fbc_t1_z = framesTree.fbc_t1_z[i]
        fbc_t2_z = framesTree.fbc_t2_z[i]

        fb_mc_t1 = framesTree.fbc_t1_tmc[i]
        fb_mc_t2 = framesTree.fbc_t2_tmc[i]

        fbc_t1_true = framesTree.fbc_t1_true[i]
        fbc_t2_true = framesTree.fbc_t2_true[i]

        fb1_hit_res = fb_t1 - fb_mc_t1 #change to fbc_t1_true?
        fb2_hit_res = fb_t2 - fb_mc_t2

        fb1_eff_res = fb_t1 - fbc_t1_true
        fb2_eff_res = fb_t2 - fbc_t2_true

        fbc_t1_madeWithFit = framesTree.fbc_t1_madeWithFit[i]
        fbc_t2_madeWithFit = framesTree.fbc_t2_madeWithFit[i]

        fbc_t1_prev_frame = framesTree.fbc_t1_prev_frame[i]
        fbc_t2_prev_frame = framesTree.fbc_t2_prev_frame[i]

        fbc_t1_PU_type = framesTree.fbc_t1_PU_type[i]
        fbc_t2_PU_type = framesTree.fbc_t2_PU_type[i]

        fbc_t1_origClTrPU = framesTree.fbc_t1_origClTrPU[i]
        fbc_t2_origClTrPU = framesTree.fbc_t2_origClTrPU[i]

        fbc_t1_passMultiTrkCut = framesTree.fbc_t1_passMultiTrkCut[i]
        fbc_t2_passMultiTrkCut = framesTree.fbc_t2_passMultiTrkCut[i]

        t1_wouldBeAffected = framesTree.t1_wouldBeAffected[i]
        t2_wouldBeAffected = framesTree.t2_wouldBeAffected[i]

        t1_wouldBeAffectedMultiTrk = framesTree.t1_wouldBeAffectedMultiTrk[i]
        t2_wouldBeAffectedMultiTrk = framesTree.t2_wouldBeAffectedMultiTrk[i]

        t1_wouldBeAffectedSingleTrk = framesTree.t1_wouldBeAffectedSingleTrk[i]
        t2_wouldBeAffectedSingleTrk = framesTree.t2_wouldBeAffectedSingleTrk[i]

        fbc_t1_ns = framesTree.fbc_t1_ns[i]
        fbc_t2_ns = framesTree.fbc_t2_ns[i]

        fbc_t1_nh = framesTree.fbc_t1_nh[i]
        fbc_t2_nh = framesTree.fbc_t2_nh[i]

        fbc_dt1 = framesTree.fbc_dt_t1[i]
        fbc_dt2 = framesTree.fbc_dt_t2[i]

        reco_p = -1.0 * framesTree.p[i] # technically p_q
        reco_r = framesTree.r[i]

        mc_pt = framesTree.mc_pt[i]
        mc_p = framesTree.mc_p[i]
        mc_pid = framesTree.mc_pid[i]
        mc_lam = framesTree.mc_lam[i]
        mc_phi = framesTree.mc_phi[i]
        mc_vt = framesTree.mc_vt[i]

        fbc_t2_pl = framesTree.fbc_t2_pl[i]
        fbc_t1_pl = framesTree.fbc_t1_pl[i]

        t1_isCorrect = framesTree.t1_isCorrect[i]
        t2_isCorrect = framesTree.t2_isCorrect[i]

        t1_isPossible = framesTree.t1_isPossible[i]
        t2_tl_isPossible = framesTree.t2_tl_isPossible[i]
        t2_fb_isPossible = framesTree.t2_fb_isPossible[i]

        t1_incorrect_origin = framesTree.t1_incorrect_origin[i]
        t2_incorrect_origin = framesTree.t2_incorrect_origin[i]

        tl_noPU = framesTree.tl_noPU[i]
        tl_dt = framesTree.tl_dt[i]

        fbs_dt = fb_t2 - fb_t1
        fbs_pl_diff = fbc_t2_pl - fbc_t1_pl

        tl_t = framesTree.tl_t[i]
        tl_n = framesTree.tl_n[i]
        tl_pl = framesTree.tl_s[i]
        tl_z = framesTree.tl_z[i]
        tl_edep_sum = framesTree.tl_edep_sum[i]
        tl_edep_avg = tl_edep_sum / float(tl_n)
        tl_edep_diff = framesTree.tl_edep_diff[i]

        fb_tl_dt = tl_t - fb_t1
        fb_tl_pl_diff = tl_pl - fbc_t1_pl

        t_diff_fbs = fbs_dt - fbs_pl_diff/c
        t_diff_fb_tl = fb_tl_dt - fb_tl_pl_diff/c


        # For PL to TOF, resolution check
        # need true tof between fb crossings i.e. recurls...
        # need to calc tof from PL...
        # true_tof = fbc_t2_true - fbc_t1_true
        # true_tof = fb_mc_t2 - fb_mc_t1
        # tof_res = fbs_pl_diff/c - true_tof
        # # if (fb_mc_t2 != -1.0 and fb_mc_t1 != -1.0):
        # if (fb_mc_t2 > 0 and fb_mc_t1 > 0 and fb_mc_t1 < fb_mc_t2):
        # # if (fbc_t2_true != -1.0 and fbc_t1_true != -1.0 and fbc_t1_true < fbc_t2_true):
        #     # print "fbc_t1_true ", fbc_t1_true, ", fbc_t2_true ", fbc_t2_true, ", true tof (T2-T1) ", true_tof
        #     # print "fb_mc_t1 ", fb_mc_t1, ", fb_mc_t2 ", fb_mc_t2, ", true tof (T2-T1) ", true_tof
        #     # print "PL between fb crossings / recurls ", fbs_pl_diff, ", to TOF ", fbs_pl_diff/c
        #     # print "TOF res ", tof_res
        #     h_tof_res.Fill(tof_res)



        # Should conditions on time be != -1 (if intialised in trirec to tht) or just > 0?
        # There may be lots of cases where time is -0.5 e.g....

        correct_charge = (mc_pid == positron and reco_p > 0) or (mc_pid == electron and reco_p < 0)


        if (fb_t1 > 0 and fb_t2 > 0 and (nhit == 8 or nhit ==6) and framesTree.mc[i] != 1):
            h_fbs_dt_vs_pl_fake_tracks.Fill(fbs_pl_diff, fbs_dt)

        if (framesTree.mc[i] != 1 or framesTree.t0_mc[i] == 0.0): continue

        # CORRECTLY RECONSTRUCTED MOMEMNTUM SIGN / CHARGE (p > 0 = positive track)
        if (fb_t1 > 0 and fb_t2 > 0 and (nhit == 8 or nhit ==6) and correct_charge):
            h_fbs_dt_vs_pl_long_corr_reco_p.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and fb_t2 > 0 and nhit == 8 and correct_charge):
            h_fbs_dt_vs_pl_L8.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and fb_t2 > 0 and nhit == 6 and correct_charge):
            h_fbs_dt_vs_pl_L6.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and tl_t > 0 and nhit == 6 and correct_charge):
            h_fb_tl_dt_vs_pl.Fill(fb_tl_pl_diff, fb_tl_dt)

        # INCORRECTLY RECONSTRUCTED MOMEMNTUM SIGN / CHARGE 
        if (fb_t1 > 0 and fb_t2 > 0 and (nhit == 8 or nhit ==6) and not correct_charge):
            h_fbs_dt_vs_pl_long_wrong_reco_p.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and fb_t2 > 0 and nhit == 8 and not correct_charge):
            h_fbs_dt_vs_pl_L8_wrong_reco_p.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and fb_t2 > 0 and nhit == 6 and not correct_charge):
            h_fbs_dt_vs_pl_L6_wrong_reco_p.Fill(fbs_pl_diff, fbs_dt)
        if (fb_t1 > 0 and tl_t > 0 and nhit == 6 and not correct_charge):
            h_fb_tl_dt_vs_pl_wrong_reco_p.Fill(fb_tl_pl_diff, fb_tl_dt)

        if (fb_t1 > 0 and fb_t2 > 0 and (nhit == 8 or nhit ==6)):
            h_fbs_dt_vs_pl.Fill(fbs_pl_diff, fbs_dt)


        # print "t1 corr ", t1_isCorrect, ", t2 corr ", t2_isCorrect, "nhit ", nhit, ", fb_t1 ", fb_t1, ", fb_t2 ", fb_t2, ", tl_t ", tl_t
        # print "fbc_t1_true ", fbc_t1_true, ", fbc_t2_true ", fbc_t2_true
        # print "fbc_t1_one_tid ", fbc_t1_one_tid, ", fbc_t1_noRecurlerPU ", fbc_t1_noRecurlerPU
        # print "fbc_t2_one_tid ", fbc_t2_one_tid, ", fbc_t2_noRecurlerPU ", fbc_t2_noRecurlerPU, "\n"
        # print "tl_noPU", tl_noPU
        # t1_isCorrect = t1_isCorrect and fbc_t1_one_tid and fbc_t1_noRecurlerPU
        # if (nhit == 6 and tl_t != -1.0): t2_isCorrect = t2_isCorrect and tl_noPU
        # elif ((nhit == 6 and fb_t2 != -1.0) or nhit == 8): t2_isCorrect = t2_isCorrect and fbc_t2_one_tid and fbc_t2_noRecurlerPU
        # print "updated t1 corr ", t1_isCorrect, ", t2 corr ", t2_isCorrect

        # Also correct for prevFrame PU??
        t1Cut = False
        t2Cut = False
        if (clsMadeWithFit): 
            t1Cut = fbc_t1_madeWithFit==1 and fbc_t1_origClTrPU==1
            t2Cut = fbc_t2_madeWithFit==1 and fbc_t2_origClTrPU==1
        else:
            t1_nh_and_ns_cut = fbc_t1_ns<=fit_max_trackPU_density and fbc_t1_nh>=recluster_nh_min
            t2_nh_and_ns_cut = fbc_t2_ns<=fit_max_trackPU_density and fbc_t2_nh>=recluster_nh_min
            if (recluster_singleTrk_clusters):
                t1Cut = (fbc_t1_ns==1 and (fbc_dt1>=singleTrk_dt_min and fbc_dt1<=singleTrk_dt_max)) and t1_nh_and_ns_cut and fbc_t1_PU_type==2
                t2Cut = (fbc_t2_ns==1 and (fbc_dt2>=singleTrk_dt_min and fbc_dt2<=singleTrk_dt_max)) and t2_nh_and_ns_cut and fbc_t2_PU_type==2
            elif (recluster_multiTrk_clusters):
                t1Cut = (fbc_t1_ns>1 and fbc_dt1>=multiTrk_dt_min) and t1_nh_and_ns_cut and fbc_t1_passMultiTrkCut==1 and fbc_t1_PU_type==2
                t2Cut = (fbc_t2_ns>1 and fbc_dt2>=multiTrk_dt_min) and t2_nh_and_ns_cut and fbc_t2_passMultiTrkCut==1 and fbc_t2_PU_type==2

        fb_noPU = fbc_t1_PU_type==0 and fbc_t2_PU_type==0

        # TODO: clean up and merge the following if statements...

        
        if (t1_isCorrect != -1 and t1Cut):
            h_fb_eff_affected_vs_z1.Fill(t1_isCorrect, fbc_t1_z)
            h_fb_eff_affected_vs_lam.Fill(t1_isCorrect, mc_lam)
            h_fb_eff_affected_vs_phi.Fill(t1_isCorrect, mc_phi)
            h_fb_eff_affected_vs_p.Fill(t1_isCorrect, mc_p)
            h_fb_eff_affected_vs_mc_vt.Fill(t1_isCorrect, mc_vt)

        if (t2_isCorrect != -1 and t2Cut and fb_t2 != -1.0):
            h_fb_eff_affected_vs_z2.Fill(t2_isCorrect, fbc_t2_z)
            h_fb_eff_affected_vs_lam.Fill(t2_isCorrect, mc_lam)
            h_fb_eff_affected_vs_phi.Fill(t2_isCorrect, mc_phi)
            h_fb_eff_affected_vs_p.Fill(t2_isCorrect, mc_p)
            h_fb_eff_affected_vs_mc_vt.Fill(t2_isCorrect, mc_vt)

        if (nhit==4):
            if (t1_isCorrect != -1 and t1Cut): h_fb_eff_affected_vs_times_present_tr_type.Fill(t1_isCorrect, 0)
        elif (nhit==6):
            if (t1_isCorrect != -1 and t1Cut): h_fb_eff_affected_vs_times_present_tr_type.Fill(t1_isCorrect, 1)
            if (t2_isCorrect != -1 and t2Cut and fb_t2 != -1.0): h_fb_eff_affected_vs_times_present_tr_type.Fill(t2_isCorrect, 1)
        elif (nhit==8):
            if (t1_isCorrect != -1 and t1Cut): h_fb_eff_affected_vs_times_present_tr_type.Fill(t1_isCorrect, 2)
            if (t2_isCorrect != -1 and t2Cut): h_fb_eff_affected_vs_times_present_tr_type.Fill(t2_isCorrect, 2)

        if (t1_isCorrect != -1 and t2_isCorrect != -1 and fb_t2 != -1.0):
            h_fb_eff_vs_dt_cl_both_present.Fill(t1_isCorrect, fbc_dt1)
            h_fb_eff_vs_dt_cl_both_present.Fill(t2_isCorrect, fbc_dt2)

            if (t1Cut or t2Cut): # what if both??
                h_fb_eff_affected_vs_times_present.Fill(t1_isCorrect and t2_isCorrect, 1)
                h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(t1_isCorrect and t2_isCorrect, 1)
                h_fb_eff_affected_vs_times_present_time_type.Fill(t1_isCorrect and t2_isCorrect, 2)

                h_fb_eff_affected_vs_z_diff.Fill(t1_isCorrect and t2_isCorrect, fbc_t2_z - fbc_t1_z)

                h_fb_eff_affected_vs_dt_cl_both_present.Fill(t1_isCorrect, fbc_dt1)
                h_fb_eff_affected_vs_dt_cl_both_present.Fill(t2_isCorrect, fbc_dt2)

            if (not clsMadeWithFit and (t1Cut or t2Cut)) or \
                (clsMadeWithFit and recluster_multiTrk_clusters and ((t1_wouldBeAffectedMultiTrk>=1 and fbc_t1_madeWithFit==0) or (t2_wouldBeAffectedMultiTrk>=1 and fbc_t2_madeWithFit==0))) or \
                (clsMadeWithFit and recluster_singleTrk_clusters and ((t1_wouldBeAffectedSingleTrk>=1 and fbc_t1_madeWithFit==0) or (t2_wouldBeAffectedSingleTrk>=1 and fbc_t2_madeWithFit==0))): 
                h_fb_eff_affected_vs_times_present_but_not_fitCl.Fill(t1_isCorrect and t2_isCorrect, 1)

            if (t1Cut): 
                h_fb_eff_res_affected_both_times.Fill(fb1_eff_res)
                if (fbc_t2_PU_type == 2 and t2_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 0):
                    h_fb_eff_res_affected_both_times_noPU.Fill(fb1_eff_res)
                    if (fbc_t2_PU_type == 2 and t2_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_noPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 1):
                    h_fb_eff_res_affected_both_times_recurlerPU.Fill(fb1_eff_res)
                    if (fbc_t2_PU_type == 2 and t2_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_recurlerPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 2): 
                    h_fb_eff_res_affected_both_times_trackPU.Fill(fb1_eff_res)
                    if (fbc_t2_PU_type == 2 and t2_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_trackPU.Fill(fb1_eff_res)

            if (t2Cut): 
                h_fb_eff_res_affected_both_times.Fill(fb2_eff_res)
                if (fbc_t1_PU_type == 2 and t1_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 0): 
                    h_fb_eff_res_affected_both_times_noPU.Fill(fb2_eff_res)
                    if (fbc_t1_PU_type == 2 and t1_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_noPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 1):
                    h_fb_eff_res_affected_both_times_recurlerPU.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 2 and t1_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_recurlerPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 2): 
                    h_fb_eff_res_affected_both_times_trackPU.Fill(fb2_eff_res)
                    if (fbc_t1_PU_type == 2 and t1_isCorrect==0): h_fb_eff_res_affected_both_times_otherIsTrkPU_trackPU.Fill(fb2_eff_res)
                    
                    
        if (t1_isCorrect != -1 and t2_isCorrect == -1 and t2_fb_isPossible==1):
            h_fb_eff_vs_times_present.Fill(t1_isCorrect, 0)
            h_fb_eff_vs_dt_cl_one_present.Fill(t1_isCorrect, fbc_dt1)
            if (t1Cut):
                h_fb_eff_affected_vs_times_present_time_type.Fill(t1_isCorrect, 0)
                h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(t1_isCorrect, 0)
                h_fb_eff_affected_vs_dt_cl_one_present.Fill(t1_isCorrect, fbc_dt1)
                h_fb_eff_affected_vs_times_present.Fill(t1_isCorrect, 0)
                h_fb_eff_res_affected_one_time.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 0):
                    h_fb_eff_res_affected_one_time_noPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 1):
                    h_fb_eff_res_affected_one_time_recurlerPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 2): 
                    h_fb_eff_res_affected_one_time_trackPU.Fill(fb1_eff_res)
            if (not clsMadeWithFit and t1Cut) or ((clsMadeWithFit and recluster_singleTrk_clusters and (t1_wouldBeAffectedSingleTrk>=1 and fbc_t1_madeWithFit==0)) or \
                (clsMadeWithFit and recluster_multiTrk_clusters and (t1_wouldBeAffectedMultiTrk>=1 and fbc_t1_madeWithFit==0))): h_fb_eff_affected_vs_times_present_but_not_fitCl.Fill(t1_isCorrect, 0)

        if (t1_isCorrect == -1 and t2_isCorrect != -1 and fb_t2 != -1.0):
            h_fb_eff_vs_times_present.Fill(t2_isCorrect, 0)
            h_fb_eff_vs_dt_cl_one_present.Fill(t2_isCorrect, fbc_dt2)
            if (t2Cut): 
                h_fb_eff_affected_vs_times_present_time_type.Fill(t2_isCorrect, 1)
                h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(t2_isCorrect, 0)
                h_fb_eff_affected_vs_dt_cl_one_present.Fill(t2_isCorrect, fbc_dt2)
                h_fb_eff_affected_vs_times_present.Fill(t2_isCorrect, 0)
                h_fb_eff_res_affected_one_time.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 0): 
                    h_fb_eff_res_affected_one_time_noPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 1):
                    h_fb_eff_res_affected_one_time_recurlerPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 2): 
                    h_fb_eff_res_affected_one_time_trackPU.Fill(fb2_eff_res)
            if (not clsMadeWithFit and t2Cut) or (clsMadeWithFit and recluster_singleTrk_clusters and ((t2_wouldBeAffectedSingleTrk>=1 and fbc_t2_madeWithFit==0))) or \
                (clsMadeWithFit and recluster_multiTrk_clusters and ((t2_wouldBeAffectedMultiTrk>=1 and fbc_t2_madeWithFit==0))): h_fb_eff_affected_vs_times_present_but_not_fitCl.Fill(t2_isCorrect, 0)


        # Loss of clusters denominator.....
        if (t1_isCorrect == -1 and t2_isCorrect == -1 and t1_isPossible==1 and t2_fb_isPossible==1 and (\
            (clsMadeWithFit and recluster_singleTrk_clusters and (t1_wouldBeAffectedSingleTrk>=1 and t2_wouldBeAffectedSingleTrk>=1)) or \
            (clsMadeWithFit and recluster_multiTrk_clusters and (t1_wouldBeAffectedMultiTrk>=1 or t2_wouldBeAffectedMultiTrk>=1)))): h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(False, 1) #not sure if correct way to fill

        if (t1_isCorrect != -1 and t2_isCorrect == -1 and t2_fb_isPossible==1 and ((clsMadeWithFit and recluster_singleTrk_clusters and t2_wouldBeAffectedSingleTrk>=1) or \
             (clsMadeWithFit and recluster_multiTrk_clusters and t2_wouldBeAffectedMultiTrk>=1))): h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(False, 0) #not sure if correct way to fill

        if (t1_isCorrect == -1 and t2_isCorrect != -1 and t1_isPossible==1 and ((clsMadeWithFit and recluster_singleTrk_clusters and t1_wouldBeAffectedSingleTrk>=1) or \
             (clsMadeWithFit and recluster_multiTrk_clusters and t1_wouldBeAffectedMultiTrk>=1))): h_fb_eff_affected_vs_times_cl_loss_denominator.Fill(False, 0) #not sure if correct way to fill


        if (t2_isCorrect != -1):
            if (t2Cut): h_fb_eff_affected_vs_dt_cl.Fill(t2_isCorrect, fbc_dt2)

            if ((nhit == 6 and fb_t2 != -1.0) or nhit == 8): # L6/L8 fb only
                h_fb_t2_eff_vs_z.Fill(t2_isCorrect, fbc_t2_z)
                h_fb_eff_vs_dt_cl.Fill(t2_isCorrect, fbc_dt2)
                h_fb_t2_eff_vs_dt_cl.Fill(t2_isCorrect, fbc_dt2)

            if (nhit == 6 and fb_t2 != -1.0): # make sure second hit is fb (L6)
                h_fb_l6_t2_eff_vs_z.Fill(t2_isCorrect, fbc_t2_z)
                h_fb_l6_t2_eff_vs_dt_cl.Fill(t2_isCorrect, fbc_dt2)

            if (nhit == 8): 
                h_fb_l8_t2_eff_vs_z.Fill(t2_isCorrect, fbc_t2_z)
                h_fb_l8_t2_eff_vs_dt_cl.Fill(t2_isCorrect, fbc_dt2)

            if (nhit == 6 and tl_t != -1.0): # make sure second hit is tl
                h_tl_t2_eff_vs_z.Fill(t2_isCorrect, tl_z)
                h_tl_t2_edep_diff.Fill(t2_isCorrect, tl_edep_diff)
                if (correct_charge): h_tl_t2_eff_vs_dt_cl.Fill(t2_isCorrect, tl_dt)

        if (t1_isCorrect != -1):
            # Do eff vs dt for all cls
            h_fb_t1_eff_vs_dt_cl.Fill(t1_isCorrect, fbc_dt1)
            if (t1Cut): h_fb_eff_affected_vs_dt_cl.Fill(t1_isCorrect, fbc_dt1)
            h_fb_eff_vs_dt_cl.Fill(t1_isCorrect, fbc_dt1)
            h_t1_eff_vs_z.Fill(t1_isCorrect, fbc_t1_z)

            if (t2_isCorrect != -1):

                both_correct = t1_isCorrect and t2_isCorrect
                both_incorr = not t1_isCorrect and not t2_isCorrect
                t1_corr_t2_incorr = t1_isCorrect and not t2_isCorrect
                t1_incorr_t2_corr = not t1_isCorrect and t2_isCorrect

                if ((nhit == 6 and fb_t2 != -1.0) or nhit == 8): # L6/L8 fb only
                    h_t1_t2_eff_vs_z_diff.Fill(both_correct, fbc_t2_z - fbc_t1_z)
                    if (correct_charge): h_t1_t2_fb_eff_vs_t_diff.Fill(both_correct, fbs_dt)
                    h_t1_t2_fb_eff_vs_lam.Fill(both_correct, mc_lam)
                    h_fb_eff_vs_times_present.Fill(both_correct, 1)


                if (nhit == 6 and fb_t2 != -1.0): # make sure second hit is fb (L6)
                    h_t1_t2_fb_l6_eff_vs_z_diff.Fill(both_correct, fbc_t2_z - fbc_t1_z)
                    if (correct_charge): h_t1_t2_fb_l6_eff_vs_t_diff.Fill(both_correct, fbs_dt)
                    h_t1_t2_fb_l6_eff_vs_lam.Fill(both_correct, mc_lam)

                    if (correct_charge):
                        if (both_correct): h_L6F_dt_vs_ds_both_correct.Fill(fbs_pl_diff, fbs_dt)

                        elif (both_incorr): 
                            h_L6F_dt_vs_ds_both_incorr.Fill(fbs_pl_diff, fbs_dt)
                            if (t1_incorrect_origin == RECURLER_PU and t2_incorrect_origin == RECURLER_PU): h_L6F_dt_vs_ds_both_incorr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == RECURLER_PU and t2_incorrect_origin == TRACK_PU): h_L6F_dt_vs_ds_both_incorr_recurlerPU_trackPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU and t2_incorrect_origin == RECURLER_PU): h_L6F_dt_vs_ds_both_incorr_trackPU_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU and t2_incorrect_origin == TRACK_PU): h_L6F_dt_vs_ds_both_incorr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                        elif (t1_corr_t2_incorr): 
                            h_L6F_dt_vs_ds_t1_corr_t2_incorr.Fill(fbs_pl_diff, fbs_dt)
                            if (t2_incorrect_origin == RECURLER_PU): h_L6F_dt_vs_ds_t1_corr_t2_incorr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t2_incorrect_origin == TRACK_PU): h_L6F_dt_vs_ds_t1_corr_t2_incorr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                        elif (t1_incorr_t2_corr): 
                            h_L6F_dt_vs_ds_t1_incorr_t2_corr.Fill(fbs_pl_diff, fbs_dt)
                            if (t1_incorrect_origin == RECURLER_PU): h_L6F_dt_vs_ds_t1_incorr_t2_corr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU): h_L6F_dt_vs_ds_t1_incorr_t2_corr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                if (nhit == 8): 
                    h_t1_t2_fb_l8_eff_vs_z_diff.Fill(both_correct, fbc_t2_z - fbc_t1_z)
                    if (correct_charge): h_t1_t2_fb_l8_eff_vs_t_diff.Fill(both_correct, fbs_dt)
                    h_t1_t2_fb_l8_eff_vs_lam.Fill(both_correct, mc_lam)

                    if (correct_charge):
                        if (both_correct): h_L8_dt_vs_ds_both_correct.Fill(fbs_pl_diff, fbs_dt)

                        elif (both_incorr): 
                            h_L8_dt_vs_ds_both_incorr.Fill(fbs_pl_diff, fbs_dt)
                            if (t1_incorrect_origin == RECURLER_PU and t2_incorrect_origin == RECURLER_PU): h_L8_dt_vs_ds_both_incorr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == RECURLER_PU and t2_incorrect_origin == TRACK_PU): h_L8_dt_vs_ds_both_incorr_recurlerPU_trackPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU and t2_incorrect_origin == RECURLER_PU): h_L8_dt_vs_ds_both_incorr_trackPU_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU and t2_incorrect_origin == TRACK_PU): h_L8_dt_vs_ds_both_incorr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                        elif (t1_corr_t2_incorr): 
                            h_L8_dt_vs_ds_t1_corr_t2_incorr.Fill(fbs_pl_diff, fbs_dt)
                            if (t2_incorrect_origin == RECURLER_PU): h_L8_dt_vs_ds_t1_corr_t2_incorr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t2_incorrect_origin == TRACK_PU): h_L8_dt_vs_ds_t1_corr_t2_incorr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                        elif (t1_incorr_t2_corr): 
                            h_L8_dt_vs_ds_t1_incorr_t2_corr.Fill(fbs_pl_diff, fbs_dt)
                            if (t1_incorrect_origin == RECURLER_PU): h_L8_dt_vs_ds_t1_incorr_t2_corr_recurlerPU.Fill(fbs_pl_diff, fbs_dt)
                            elif (t1_incorrect_origin == TRACK_PU): h_L8_dt_vs_ds_t1_incorr_t2_corr_trackPU.Fill(fbs_pl_diff, fbs_dt)

                if (nhit == 6 and tl_t != -1.0): # make sure second hit is tl
                    h_t1_t2_eff_vs_z_diff.Fill(both_correct, tl_z - fbc_t1_z) 
                    h_t1_t2_tl_eff_vs_z_diff.Fill(both_correct, tl_z - fbc_t1_z)
                    h_t1_t2_tl_eff_vs_t_diff.Fill(both_correct, fb_tl_dt)
                    if (correct_charge and t1Cut): h_tl_t1_eff_vs_dt_cl.Fill(t1_isCorrect, fbc_dt1)
                    h_t1_t2_tl_eff_vs_lam.Fill(both_correct, mc_lam)
                    h_tl_t2_eff_vs_n.Fill(t2_isCorrect, tl_n)

                    if (correct_charge):
                        if (both_correct): h_L6T_dt_vs_ds_both_correct.Fill(fb_tl_pl_diff, fb_tl_dt)

                        elif (both_incorr): 
                            h_L6T_dt_vs_ds_both_incorr.Fill(fb_tl_pl_diff, fb_tl_dt)
                            if (t1_incorrect_origin == RECURLER_PU): h_L6T_dt_vs_ds_both_incorr_recurlerPU.Fill(fb_tl_pl_diff, fb_tl_dt)
                            elif (t1_incorrect_origin == TRACK_PU): h_L6T_dt_vs_ds_both_incorr_trackPU.Fill(fb_tl_pl_diff, fb_tl_dt)

                        elif (t1_corr_t2_incorr): 
                            h_L6T_dt_vs_ds_t1_corr_t2_incorr.Fill(fb_tl_pl_diff, fb_tl_dt)

                        elif (t1_incorr_t2_corr): 
                            h_L6T_dt_vs_ds_t1_incorr_t2_corr.Fill(fb_tl_pl_diff, fb_tl_dt)
                            if (t1_incorrect_origin == RECURLER_PU): h_L6T_dt_vs_ds_t1_incorr_t2_corr_recurlerPU.Fill(fb_tl_pl_diff, fb_tl_dt)
                            elif (t1_incorrect_origin == TRACK_PU): h_L6T_dt_vs_ds_t1_incorr_t2_corr_trackPU.Fill(fb_tl_pl_diff, fb_tl_dt)

        # FBC DT 
        if (fbc_dt1 != -1):
            fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb_dt, h_fb_dt_clean, h_fb_dt_recurlerPU, h_fb_dt_trackPU, h_fb_dt_mixedPU)
            if (fbc_t1_ns == 1): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb_singleTrk_dt, h_fb_singleTrk_dt_clean, h_fb_singleTrk_dt_recurlerPU, h_fb_singleTrk_dt_trackPU, h_fb_singleTrk_dt_mixedPU)
            if (fbc_t1_ns > 1): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb_multiTrk_dt, h_fb_multiTrk_dt_clean, h_fb_multiTrk_dt_recurlerPU, h_fb_multiTrk_dt_trackPU, h_fb_multiTrk_dt_mixedPU)
            fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb1_dt, h_fb1_dt_clean, h_fb1_dt_recurlerPU, h_fb1_dt_trackPU, h_fb1_dt_mixedPU)
            if (nhit == 4): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb1_s4_dt, h_fb1_s4_dt_clean, h_fb1_s4_dt_recurlerPU, h_fb1_s4_dt_trackPU, h_fb1_s4_dt_mixedPU)
            if (nhit == 6 and fb_t2 != -1.0): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb1_l6F_dt, h_fb1_l6F_dt_clean, h_fb1_l6F_dt_recurlerPU, h_fb1_l6F_dt_trackPU, h_fb1_l6F_dt_mixedPU)
            if (nhit == 6 and tl_t != -1.0): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb1_l6T_dt, h_fb1_l6T_dt_clean, h_fb1_l6T_dt_recurlerPU, h_fb1_l6T_dt_trackPU, h_fb1_l6T_dt_mixedPU)
            if (nhit == 8): fillTimeSpanHistos(fbc_dt1, fbc_t1_PU_type, h_fb1_l8_dt, h_fb1_l8_dt_clean, h_fb1_l8_dt_recurlerPU, h_fb1_l8_dt_trackPU, h_fb1_l8_dt_mixedPU)

        if (fbc_dt2 != -1):
            fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb_dt, h_fb_dt_clean, h_fb_dt_recurlerPU, h_fb_dt_trackPU, h_fb_dt_mixedPU)
            if (fbc_t2_ns == 1): fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb_singleTrk_dt, h_fb_singleTrk_dt_clean, h_fb_singleTrk_dt_recurlerPU, h_fb_singleTrk_dt_trackPU, h_fb_singleTrk_dt_mixedPU)
            if (fbc_t2_ns > 1): fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb_multiTrk_dt, h_fb_multiTrk_dt_clean, h_fb_multiTrk_dt_recurlerPU, h_fb_multiTrk_dt_trackPU, h_fb_multiTrk_dt_mixedPU)
            fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb2_dt, h_fb2_dt_clean, h_fb2_dt_recurlerPU, h_fb2_dt_trackPU, h_fb2_dt_mixedPU)
            if (nhit == 6): fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb2_l6_dt, h_fb2_l6_dt_clean, h_fb2_l6_dt_recurlerPU, h_fb2_l6_dt_trackPU, h_fb2_l6_dt_mixedPU)
            if (nhit == 8): fillTimeSpanHistos(fbc_dt2, fbc_t2_PU_type, h_fb2_l8_dt, h_fb2_l8_dt_clean, h_fb2_l8_dt_recurlerPU, h_fb2_l8_dt_trackPU, h_fb2_l8_dt_mixedPU)

        # FBC ns
        if (fbc_t1_ns>0):
            h_fb_ns.Fill(fbc_t1_ns)
            if (fbc_t1_PU_type == 0): h_fb_ns_clean.Fill(fbc_t1_ns)
            if (fbc_t1_PU_type == 1): h_fb_ns_recurlerPU.Fill(fbc_t1_ns)
            if (fbc_t1_PU_type == 2): 
                h_fb_ns_trackPU.Fill(fbc_t1_ns)
                h_fb1_l6T_ns_trackPU.Fill(fbc_t1_ns)
            if (fbc_t1_PU_type == 3): h_fb_ns_mixedPU.Fill(fbc_t1_ns)
        if (fbc_t2_ns>0):
            h_fb_ns.Fill(fbc_t2_ns)
            if (fbc_t2_PU_type == 0): h_fb_ns_clean.Fill(fbc_t2_ns)
            if (fbc_t2_PU_type == 1): h_fb_ns_recurlerPU.Fill(fbc_t2_ns)
            if (fbc_t2_PU_type == 2): h_fb_ns_trackPU.Fill(fbc_t2_ns)
            if (fbc_t2_PU_type == 3): h_fb_ns_mixedPU.Fill(fbc_t2_ns)

        # ns vs dt
        if (fbc_dt1 != -1 and fbc_t1_ns>0):
            h_dt_vs_ns.Fill(fbc_t1_ns, fbc_dt1)
            if (fbc_t1_PU_type == 0): h_dt_vs_ns_clean.Fill(fbc_t1_ns, fbc_dt1)
            if (fbc_t1_PU_type == 1): h_dt_vs_ns_recurlerPU.Fill(fbc_t1_ns, fbc_dt1)
            if (fbc_t1_PU_type == 2): h_dt_vs_ns_trackPU.Fill(fbc_t1_ns, fbc_dt1)
            if (fbc_t1_PU_type == 3): h_dt_vs_ns_mixedPU.Fill(fbc_t1_ns, fbc_dt1)
        if (fbc_dt2 != -1 and fbc_t2_ns>0):
            h_dt_vs_ns.Fill(fbc_t2_ns, fbc_dt2)
            if (fbc_t2_PU_type == 0): h_dt_vs_ns_clean.Fill(fbc_t2_ns, fbc_dt2)
            if (fbc_t2_PU_type == 1): h_dt_vs_ns_recurlerPU.Fill(fbc_t2_ns, fbc_dt2)
            if (fbc_t2_PU_type == 2): h_dt_vs_ns_trackPU.Fill(fbc_t2_ns, fbc_dt2)
            if (fbc_t2_PU_type == 3): h_dt_vs_ns_mixedPU.Fill(fbc_t2_ns, fbc_dt2)

        if (tl_dt!= -1):
            h_tl_dt.Fill(tl_dt)
            if (tl_noPU == 1):  
                h_tl_dt_noPU.Fill(tl_dt)
            else:
                h_tl_dt_PU.Fill(tl_dt)

        # TODO: make resolution plots for case where both times present (and if that cluster has trackPU or not) and only one time present

        # "TRUE" TRACK TIMING RESOLUTIONS
        if (nhit == 4):
            if (fb_t1 != -1.0):
                h_fb_eff_res.Fill(fb1_eff_res)
                h_fb_s4_eff_res.Fill(fb1_eff_res) 
                if (fbc_t1_PU_type == 0):
                    h_fb_eff_res_noPU.Fill(fb1_eff_res)
                    h_fb_s4_eff_res_noPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 1):
                    h_fb_eff_res_recurlerPU.Fill(fb1_eff_res)
                    h_fb_s4_eff_res_recurlerPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 2): 
                    h_fb_eff_res_trackPU.Fill(fb1_eff_res)
                    h_fb_s4_eff_res_trackPU.Fill(fb1_eff_res)

                if (t1Cut):
                    h_fb_eff_res_affected.Fill(fb1_eff_res)
                    h_fb_s4_eff_res_affected.Fill(fb1_eff_res) 
                    if (fbc_t1_PU_type == 0):
                        h_fb_eff_res_affected_noPU.Fill(fb1_eff_res)
                        h_fb_s4_eff_res_affected_noPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 1):
                        h_fb_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                        h_fb_s4_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 2): 
                        h_fb_eff_res_affected_trackPU.Fill(fb1_eff_res)
                        h_fb_s4_eff_res_affected_trackPU.Fill(fb1_eff_res)


        if (nhit == 6): 
            if (fb_t1 != -1.0):
                h_fb_eff_res.Fill(fb1_eff_res)
                h_fb1_l6_eff_res.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 0):
                    h_fb_eff_res_noPU.Fill(fb1_eff_res)
                    h_fb1_l6_eff_res_noPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 1): 
                    h_fb_eff_res_recurlerPU.Fill(fb1_eff_res)
                    h_fb1_l6_eff_res_recurlerPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 2): 
                    h_fb_eff_res_trackPU.Fill(fb1_eff_res)
                    h_fb1_l6_eff_res_trackPU.Fill(fb1_eff_res)
                
                if (t1Cut):
                    h_fb_eff_res_affected.Fill(fb1_eff_res)
                    h_fb1_l6_eff_res_affected.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 0):
                        h_fb_eff_res_affected_noPU.Fill(fb1_eff_res)
                        h_fb1_l6_eff_res_affected_noPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 1): 
                        h_fb_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                        h_fb1_l6_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 2): 
                        h_fb_eff_res_affected_trackPU.Fill(fb1_eff_res)
                        h_fb1_l6_eff_res_affected_trackPU.Fill(fb1_eff_res)


            if (fb_t2 != -1.0):
                h_fb_eff_res.Fill(fb2_eff_res)
                h_fb2_l6_eff_res.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 0): 
                    h_fb_eff_res_noPU.Fill(fb2_eff_res)
                    h_fb2_l6_eff_res_noPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 1):
                    h_fb_eff_res_recurlerPU.Fill(fb2_eff_res)
                    h_fb2_l6_eff_res_recurlerPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 2): 
                    h_fb_eff_res_trackPU.Fill(fb2_eff_res)
                    h_fb2_l6_eff_res_trackPU.Fill(fb2_eff_res)

                if (t2Cut):
                    h_fb_eff_res_affected.Fill(fb2_eff_res)
                    h_fb2_l6_eff_res_affected.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 0): 
                        h_fb_eff_res_affected_noPU.Fill(fb2_eff_res)
                        h_fb2_l6_eff_res_affected_noPU.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 1):
                        h_fb_eff_res_affected_recurlerPU.Fill(fb2_eff_res)
                        h_fb2_l6_eff_res_affected_recurlerPU.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 2): 
                        h_fb_eff_res_affected_trackPU.Fill(fb2_eff_res)
                        h_fb2_l6_eff_res_affected_trackPU.Fill(fb2_eff_res)


        if (nhit == 8):
            if (fb_t1 != -1.0):
                h_fb_eff_res.Fill(fb1_eff_res)
                h_fb1_l8_eff_res.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 0):
                    h_fb_eff_res_noPU.Fill(fb1_eff_res)
                    h_fb1_l8_eff_res_noPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 1): 
                    h_fb_eff_res_recurlerPU.Fill(fb1_eff_res)
                    h_fb1_l8_eff_res_recurlerPU.Fill(fb1_eff_res)
                if (fbc_t1_PU_type == 2): 
                    h_fb_eff_res_trackPU.Fill(fb1_eff_res)
                    h_fb1_l8_eff_res_trackPU.Fill(fb1_eff_res)

                if (t1Cut):
                    h_fb_eff_res_affected.Fill(fb1_eff_res)
                    h_fb1_l8_eff_res_affected.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 0):
                        h_fb_eff_res_affected_noPU.Fill(fb1_eff_res)
                        h_fb1_l8_eff_res_affected_noPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 1): 
                        h_fb_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                        h_fb1_l8_eff_res_affected_recurlerPU.Fill(fb1_eff_res)
                    if (fbc_t1_PU_type == 2): 
                        h_fb_eff_res_affected_trackPU.Fill(fb1_eff_res)
                        h_fb1_l8_eff_res_affected_trackPU.Fill(fb1_eff_res)


            if (fb_t2 != -1.0):
                h_fb_eff_res.Fill(fb2_eff_res)
                h_fb2_l8_eff_res.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 0):
                    h_fb_eff_res_noPU.Fill(fb2_eff_res)
                    h_fb2_l8_eff_res_noPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 1): 
                    h_fb_eff_res_recurlerPU.Fill(fb2_eff_res)
                    h_fb2_l8_eff_res_recurlerPU.Fill(fb2_eff_res)
                if (fbc_t2_PU_type == 2): 
                    h_fb_eff_res_trackPU.Fill(fb2_eff_res)
                    h_fb2_l8_eff_res_trackPU.Fill(fb2_eff_res)

                if (t2Cut):
                    h_fb_eff_res_affected.Fill(fb2_eff_res)
                    h_fb2_l8_eff_res_affected.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 0): 
                        h_fb_eff_res_affected_noPU.Fill(fb2_eff_res)
                        h_fb2_l8_eff_res_affected_noPU.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 1):
                        h_fb_eff_res_affected_recurlerPU.Fill(fb2_eff_res)
                        h_fb2_l8_eff_res_affected_recurlerPU.Fill(fb2_eff_res)
                    if (fbc_t2_PU_type == 2): 
                        h_fb_eff_res_affected_trackPU.Fill(fb2_eff_res)
                        h_fb2_l8_eff_res_affected_trackPU.Fill(fb2_eff_res)


        # TRACK TIMING RESOLUTIONS
        if (nhit == 4):
            if (fb_t1 != -1.0): h_fb_s4_hit_res.Fill(fb1_hit_res) 
            if (t0_fb1 != 0): h_fb_s4_l0_res.Fill(fb1_l0_res)

        if (nhit == 6): 
            if (t0_fb1 != 0 and t0_fb2 != 0): h_fb_mean_l6_l0_res.Fill(fb_l0_res)

            if (t0_fb1 != 0): h_fb1_l6_l0_res.Fill(fb1_l0_res)
            if (t0_fb2 != 0): h_fb2_l6_l0_res.Fill(fb2_l0_res)

            if (fb_t1 != -1.0): h_fb1_l6_hit_res.Fill(fb1_hit_res)
            if (fb_t2 != -1.0): h_fb2_l6_hit_res.Fill(fb2_hit_res)

        if (nhit == 8):
            if (t0_fb1 != 0 and t0_fb2 != 0): h_fb_mean_l8_l0_res.Fill(fb_l0_res)

            if (t0_fb1 != 0): h_fb1_l8_l0_res.Fill(fb1_l0_res)
            if (t0_fb2 != 0): h_fb2_l8_l0_res.Fill(fb2_l0_res)

            if (fb_t1 != -1.0): h_fb1_l8_hit_res.Fill(fb1_hit_res)
            if (fb_t2 != -1.0): h_fb2_l8_hit_res.Fill(fb2_hit_res)

        # Tl res
        if (t0_tl != -1):
            tl_res = t0_tl - t0_mc
            combined = t0 - t0_mc # weighted by number of hits in fb and tl and their resolutions...

            if (nhit == 6):
                h_tl_l6_l0_res.Fill(tl_res)
                if (t0_fb != -1):
                    h_tl_fb_l6_l0_res.Fill(combined)


# getNum = lambda nhit, cut : float(framesTree.Draw("","mc_prime&&mc_vt>10&&mc_vt<54&&nhit=="+str(nhit)+"&&"+str(cut)))
getNum = lambda nhit, cut : float(framesTree.Draw("","mc_prime&&nhit=="+str(nhit)+"&&"+str(cut))) #"+"eventId<="+str(nevents+79)+"&&
getNumMC = lambda nhit, cut : float(framesMCTree.Draw("","mc_prime&&nhit=="+str(nhit)+"&&"+str(cut)))

# Save all printouts to a file
sys.stdout = open(args.path + "/results.txt", "w")

if nevents > 1:
    results = []
    print ("tracks used ", track_counter)
    def format_gaus_result(gaus_result):
        val, err = gaus_result
        val *= 1e3
        err *= 1e3
        significant_digits = 10**floor(log10(err))
        # print(val, err, significant_digits)
        val = val // significant_digits * significant_digits
        # print("val now", val)
        return f"{val:.0f}"
    
    results.append(["S4", "first", format_gaus_result(fitGaus(h_fb_s4_hit_res)), format_gaus_result(fitGaus(h_fb_s4_l0_res) )])
    results.append([])
    results.append(["L6", "first", format_gaus_result(fitGaus(h_fb1_l6_hit_res)), format_gaus_result(fitGaus(h_fb1_l6_l0_res) )])
    results.append(["L6", "second", format_gaus_result(fitGaus(h_fb2_l6_hit_res)), format_gaus_result(fitGaus(h_fb2_l6_l0_res) )])
    results.append(["L6", "combined", "",format_gaus_result(fitGaus(h_fb_mean_l6_l0_res))])
    results.append([])
    results.append(["L8", "first", format_gaus_result(fitGaus(h_fb1_l8_hit_res)), format_gaus_result(fitGaus(h_fb1_l8_l0_res) )])
    results.append(["L8", "second", format_gaus_result(fitGaus(h_fb2_l8_hit_res)), format_gaus_result(fitGaus(h_fb2_l8_l0_res) )])
    results.append(["L8", "combined","", format_gaus_result(fitGaus(h_fb_mean_l8_l0_res))])
    results.append([])
    results.append(["L6", "tl only", "",format_gaus_result(fitGaus(h_tl_l6_l0_res))])
    results.append(["L6", "tl and fb","", format_gaus_result(fitGaus(h_tl_fb_l6_l0_res))])
    print(tabulate.tabulate(results, ["Track type","crossing","Time res at hit position [ps]","Time res at L0 [ps]"], tablefmt='latex_raw',numalign="right", floatfmt="4.0f", colalign=("right", "right","right", "right")))

    eff_res_results = []
    eff_res_results.append(["All", "any", format_gaus_result(fitGaus(h_fb_eff_res)), format_gaus_result(fitGaus(h_fb_eff_res_noPU)), format_gaus_result(fitGaus(h_fb_eff_res_recurlerPU)), format_gaus_result(fitGaus(h_fb_eff_res_trackPU))])
    eff_res_results.append([])
    eff_res_results.append(["S4", "first", format_gaus_result(fitGaus(h_fb_s4_eff_res)), format_gaus_result(fitGaus(h_fb_s4_eff_res_noPU)), format_gaus_result(fitGaus(h_fb_s4_eff_res_recurlerPU) ), format_gaus_result(fitGaus(h_fb_s4_eff_res_trackPU) )])
    eff_res_results.append(["L6", "first", format_gaus_result(fitGaus(h_fb1_l6_eff_res)), format_gaus_result(fitGaus(h_fb1_l6_eff_res_noPU)), format_gaus_result(fitGaus(h_fb1_l6_eff_res_recurlerPU) ), format_gaus_result(fitGaus(h_fb1_l6_eff_res_trackPU) )])
    eff_res_results.append(["L6", "second", format_gaus_result(fitGaus(h_fb2_l6_eff_res)), format_gaus_result(fitGaus(h_fb2_l6_eff_res_noPU)), format_gaus_result(fitGaus(h_fb2_l6_eff_res_recurlerPU) ), format_gaus_result(fitGaus(h_fb2_l6_eff_res_trackPU) )])
    eff_res_results.append([])
    eff_res_results.append(["L8", "first", format_gaus_result(fitGaus(h_fb1_l8_eff_res)), format_gaus_result(fitGaus(h_fb1_l8_eff_res_noPU)), format_gaus_result(fitGaus(h_fb1_l8_eff_res_recurlerPU) ), format_gaus_result(fitGaus(h_fb1_l8_eff_res_trackPU) )])
    eff_res_results.append(["L8", "second", format_gaus_result(fitGaus(h_fb2_l8_eff_res)), format_gaus_result(fitGaus(h_fb2_l8_eff_res_noPU)), format_gaus_result(fitGaus(h_fb2_l8_eff_res_recurlerPU) ), format_gaus_result(fitGaus(h_fb2_l8_eff_res_trackPU) )])
    print(tabulate.tabulate(eff_res_results, ["Track type","crossing","Time res at hit position [ps] (all)","Time res at hit position [ps] (noPU)","Time res at hit position [ps] (recurlerPU)", "Time res at hit position [ps] (trackPU)"], tablefmt='latex_raw',numalign="right", colalign=("right", "right", "right","right", "right")))

    # Affected
    eff_res_affected_results = []
    eff_res_affected_results.append(["All", "any", format_gaus_result(fitGaus(h_fb_eff_res_affected)), format_gaus_result(fitGaus(h_fb_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb_eff_res_affected_recurlerPU)), format_gaus_result(fitGaus(h_fb_eff_res_affected_trackPU))])
    eff_res_affected_results.append([])
    eff_res_affected_results.append(["S4", "first", format_gaus_result(fitGaus(h_fb_s4_eff_res_affected)), format_gaus_result(fitGaus(h_fb_s4_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb_s4_eff_res_affected_recurlerPU) ), format_gaus_result(fitGaus(h_fb_s4_eff_res_affected_trackPU) )])
    eff_res_affected_results.append(["L6", "first", format_gaus_result(fitGaus(h_fb1_l6_eff_res_affected)), format_gaus_result(fitGaus(h_fb1_l6_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb1_l6_eff_res_affected_recurlerPU) ), format_gaus_result(fitGaus(h_fb1_l6_eff_res_affected_trackPU) )])
    eff_res_affected_results.append(["L6", "second", format_gaus_result(fitGaus(h_fb2_l6_eff_res_affected)), format_gaus_result(fitGaus(h_fb2_l6_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb2_l6_eff_res_affected_recurlerPU) ), format_gaus_result(fitGaus(h_fb2_l6_eff_res_affected_trackPU) )])
    eff_res_affected_results.append([])
    eff_res_affected_results.append(["L8", "first", format_gaus_result(fitGaus(h_fb1_l8_eff_res_affected)), format_gaus_result(fitGaus(h_fb1_l8_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb1_l8_eff_res_affected_recurlerPU) ), format_gaus_result(fitGaus(h_fb1_l8_eff_res_affected_trackPU) )])
    eff_res_affected_results.append(["L8", "second", format_gaus_result(fitGaus(h_fb2_l8_eff_res_affected)), format_gaus_result(fitGaus(h_fb2_l8_eff_res_affected_noPU)), format_gaus_result(fitGaus(h_fb2_l8_eff_res_affected_recurlerPU) ), (fitGaus(h_fb2_l8_eff_res_affected_trackPU) )])
    print(tabulate.tabulate(eff_res_affected_results, ["Track type","crossing","Time res at hit position [ps] (all)","Time res at hit position [ps] (noPU)","Time res at hit position [ps] (recurlerPU)", "Time res at hit position [ps] (trackPU)"], tablefmt='latex_raw',numalign="right", colalign=("right", "right", "right","right", "right")))


if args.printTables:
    # Might need prevFramePU bool here as well for fb clusters?

    # T1
    t1_dt_str = "mc_prime&&fbc_t1!=-1&&fbc_dt_t1!=-1"
    t1_multi_tid_str = "&&fbc_t1_PU_type==2"
    t1_recurler_PU_str = "&&fbc_t1_PU_type==1"
    t1_prev_frame_PU_str = "&&fbc_t1_prev_frame==1"

    t1_ctrs = {"all_ctr" : float(framesTree.Draw("",t1_dt_str)), 
            "multi_tid_ctr" : float(framesTree.Draw("",t1_dt_str+t1_multi_tid_str)), 
            "recurler_PU_ctr" : float(framesTree.Draw("",t1_dt_str+t1_recurler_PU_str)), 
            "prev_rame_PU_ctr" : float(framesTree.Draw("",t1_dt_str+t1_prev_frame_PU_str))}

    t1_s4_ctrs = {"all_ctr" : getNum(4, t1_dt_str), 
                "multi_tid_ctr" : getNum(4, t1_dt_str+t1_multi_tid_str), 
                "recurler_PU_ctr" : getNum(4, t1_dt_str+t1_recurler_PU_str), 
                "prev_rame_PU_ctr" : getNum(4, t1_dt_str+t1_prev_frame_PU_str)}

    t1_l6_ctrs = {"all_ctr" : getNum(6, t1_dt_str), 
                "multi_tid_ctr" : getNum(6, t1_dt_str+t1_multi_tid_str), 
                "recurler_PU_ctr" : getNum(6, t1_dt_str+t1_recurler_PU_str), 
                "prev_rame_PU_ctr" : getNum(6, t1_dt_str+t1_prev_frame_PU_str)}

    t1_l6_t2_tl_ctrs = {"all_ctr" : getNum(6, t1_dt_str+"&&tl_t!=-1"), 
                        "multi_tid_ctr" : getNum(6, t1_dt_str+t1_multi_tid_str+"&&tl_t!=-1"), 
                        "recurler_PU_ctr" : getNum(6, t1_dt_str+t1_recurler_PU_str+"&&tl_t!=-1"), 
                        "prev_rame_PU_ctr" : getNum(6, t1_dt_str+t1_prev_frame_PU_str+"&&tl_t!=-1")}

    t1_l8_ctrs = {"all_ctr" : getNum(8, t1_dt_str), 
                "multi_tid_ctr" : getNum(8, t1_dt_str+t1_multi_tid_str), 
                "recurler_PU_ctr" : getNum(8, t1_dt_str+t1_recurler_PU_str), 
                "prev_rame_PU_ctr" : getNum(8, t1_dt_str+t1_prev_frame_PU_str)}

    # T2
    t2_dt_str = "mc_prime&&fbc_t2!=-1&&fbc_dt_t2!=-1"
    t2_multi_tid_str = "&&fbc_t2_PU_type==2"
    t2_recurler_PU_str = "&&fbc_t2_PU_type==1"
    t2_prev_frame_PU_str = "&&fbc_t2_prev_frame==1"

    t2_ctrs = {"all_ctr" : float(framesTree.Draw("",t2_dt_str)), 
            "multi_tid_ctr" : float(framesTree.Draw("",t2_dt_str+t2_multi_tid_str)), 
            "recurler_PU_ctr" : float(framesTree.Draw("",t2_dt_str+t2_recurler_PU_str)), 
            "prev_rame_PU_ctr" : float(framesTree.Draw("",t2_dt_str+t2_prev_frame_PU_str))}

    t2_l6_ctrs = {"all_ctr" : getNum(6, t2_dt_str), 
                "multi_tid_ctr" : getNum(6, t2_dt_str+t2_multi_tid_str), 
                "recurler_PU_ctr" : getNum(6, t2_dt_str+t2_recurler_PU_str), 
                "prev_rame_PU_ctr" : getNum(6, t2_dt_str+t2_prev_frame_PU_str)}

    t2_l8_ctrs = {"all_ctr" : getNum(8, t2_dt_str), 
                "multi_tid_ctr" : getNum(8, t2_dt_str+t2_multi_tid_str), 
                "recurler_PU_ctr" : getNum(8, t2_dt_str+t2_recurler_PU_str), 
                "prev_rame_PU_ctr" : getNum(8, t2_dt_str+t2_prev_frame_PU_str)}


    print ("T1: All fbc t1's ", t1_ctrs["all_ctr"], ", multiple tids ", t1_ctrs["multi_tid_ctr"], ", recurler PU ", t1_ctrs["recurler_PU_ctr"], ", prev frame PU ", t1_ctrs["prev_rame_PU_ctr"])
    print ("T1: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} % \n".format(t1_ctrs["multi_tid_ctr"]/t1_ctrs["all_ctr"] * 100.0, t1_ctrs["recurler_PU_ctr"]/t1_ctrs["all_ctr"] * 100.0, t1_ctrs["prev_rame_PU_ctr"]/t1_ctrs["all_ctr"] * 100.0)) 

    print ("T2: All fbc t2s ", t2_ctrs["all_ctr"], ", multiple tids ", t2_ctrs["multi_tid_ctr"], ", recurler PU ", t2_ctrs["recurler_PU_ctr"], ", prev frame PU ", t2_ctrs["prev_rame_PU_ctr"])
    print ("T2: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} % \n".format(t2_ctrs["multi_tid_ctr"]/t2_ctrs["all_ctr"] * 100.0, t2_ctrs["recurler_PU_ctr"]/t2_ctrs["all_ctr"] * 100.0, t2_ctrs["prev_rame_PU_ctr"]/t2_ctrs["all_ctr"] * 100.0)) 

    # By track types
    print ("S4 T1: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} %".format(t1_s4_ctrs["multi_tid_ctr"]/t1_s4_ctrs["all_ctr"] * 100.0, t1_s4_ctrs["recurler_PU_ctr"]/t1_s4_ctrs["all_ctr"] * 100.0, t1_s4_ctrs["prev_rame_PU_ctr"]/t1_s4_ctrs["all_ctr"] * 100.0)) 
    print ("L6 T1: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} %".format(t1_l6_ctrs["multi_tid_ctr"]/t1_l6_ctrs["all_ctr"] * 100.0, t1_l6_ctrs["recurler_PU_ctr"]/t1_l6_ctrs["all_ctr"] * 100.0, t1_l6_ctrs["prev_rame_PU_ctr"]/t1_l6_ctrs["all_ctr"] * 100.0)) 
    print ("L6 T1: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} % (where subsequent T2 is tile hit)".format(t1_l6_t2_tl_ctrs["multi_tid_ctr"]/t1_l6_t2_tl_ctrs["all_ctr"] * 100.0, t1_l6_t2_tl_ctrs["recurler_PU_ctr"]/t1_l6_t2_tl_ctrs["all_ctr"] * 100.0, t1_l6_t2_tl_ctrs["prev_rame_PU_ctr"]/t1_l6_t2_tl_ctrs["all_ctr"] * 100.0)) 
    print ("L8 T1: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} % \n".format(t1_l8_ctrs["multi_tid_ctr"]/t1_l8_ctrs["all_ctr"] * 100.0, t1_l8_ctrs["recurler_PU_ctr"]/t1_l8_ctrs["all_ctr"] * 100.0, t1_l8_ctrs["prev_rame_PU_ctr"]/t1_l8_ctrs["all_ctr"] * 100.0)) 

    print ("L6 T2: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} %".format(t2_l6_ctrs["multi_tid_ctr"]/t2_l6_ctrs["all_ctr"] * 100.0, t2_l6_ctrs["recurler_PU_ctr"]/t2_l6_ctrs["all_ctr"] * 100.0, t2_l6_ctrs["prev_rame_PU_ctr"]/t2_l6_ctrs["all_ctr"] * 100.0))
    print ("L8 T2: Multiple tid = {:.1f} %, recurler PU = {:.1f} %, prev frame PU = {:.1f} % \n\n".format(t2_l8_ctrs["multi_tid_ctr"]/t2_l8_ctrs["all_ctr"] * 100.0, t2_l8_ctrs["recurler_PU_ctr"]/t2_l8_ctrs["all_ctr"] * 100.0, t2_l8_ctrs["prev_rame_PU_ctr"]/t2_l8_ctrs["all_ctr"] * 100.0)) 

    print ("Tl clusters with track PU = {:.1f} % ({})\n".format(100.0 * getNum(6,"tl_noPU==0")/getNum(6,"tl_noPU!=-1"), getNum(6,"tl_noPU==0")))

    # Time to track eff counters
    print ("TRACK TIMING MATCHING EFFS\n")
    s4_ttt_ctrs = {"total" : getNum(4,"t1_isCorrect!=-1"), "correct" : getNum(4,"t1_isCorrect==1"), "incorrect" : getNum(4,"t1_isCorrect==0")}

    l6_fb_ttt_ctrs = {"total" : getNum(6,"t1_isCorrect!=-1&&t2_isCorrect!=-1&&fbc_t2!=-1"), 
                    "all_correct" : getNum(6,"t1_isCorrect==1&&t2_isCorrect==1&&fbc_t2!=-1"), 
                    "all_incorrect" : getNum(6,"t1_isCorrect==0&&t2_isCorrect==0&&fbc_t2!=-1"), 
                    "T1_correct_T2_incorrect" : getNum(6,"t1_isCorrect==1&&t2_isCorrect==0&&fbc_t2!=-1"), 
                    "T1_incorrect_T2_correct" : getNum(6,"t1_isCorrect==0&&t2_isCorrect==1&&fbc_t2!=-1")}

    l6_tl_ttt_ctrs = {"total" : getNum(6,"t1_isCorrect!=-1&&t2_isCorrect!=-1&&tl_t!=-1"), 
                    "all_correct" : getNum(6,"t1_isCorrect==1&&t2_isCorrect==1&&tl_t!=-1"), 
                    "all_incorrect" : getNum(6,"t1_isCorrect==0&&t2_isCorrect==0&&tl_t!=-1"), 
                    "T1_correct_T2_incorrect" : getNum(6,"t1_isCorrect==1&&t2_isCorrect==0&&tl_t!=-1"), 
                    "T1_incorrect_T2_correct" : getNum(6,"t1_isCorrect==0&&t2_isCorrect==1&&tl_t!=-1")}

    l8_ttt_ctrs = {"total" : getNum(8,"t1_isCorrect!=-1&&t2_isCorrect!=-1"), 
                "all_correct" : getNum(8,"t1_isCorrect==1&&t2_isCorrect==1"), 
                "all_incorrect" : getNum(8,"t1_isCorrect==0&&t2_isCorrect==0"), 
                "T1_correct_T2_incorrect" : getNum(8,"t1_isCorrect==1&&t2_isCorrect==0"), 
                "T1_incorrect_T2_correct" : getNum(8,"t1_isCorrect==0&&t2_isCorrect==1")}

    s4_eff_dct = {"T1 correct" : s4_ttt_ctrs["correct"]/s4_ttt_ctrs["total"] * 100.0,
                "T1 incorrect" : s4_ttt_ctrs["incorrect"]/s4_ttt_ctrs["total"] * 100.0}

    l6_fb_eff_dct = {"T1-T2 correct" : l6_fb_ttt_ctrs["all_correct"]/l6_fb_ttt_ctrs["total"] * 100.0,
                    "T1-T2 incorrect" : l6_fb_ttt_ctrs["all_incorrect"]/l6_fb_ttt_ctrs["total"] * 100.0,
                    "T1 incorrect T2 correct" : l6_fb_ttt_ctrs["T1_incorrect_T2_correct"]/l6_fb_ttt_ctrs["total"] * 100.0, 
                    "T1 correct T2 incorrect" : l6_fb_ttt_ctrs["T1_correct_T2_incorrect"]/l6_fb_ttt_ctrs["total"] * 100.0}

    l6_tl_eff_dct = {"T1-T2 correct" : l6_tl_ttt_ctrs["all_correct"]/l6_tl_ttt_ctrs["total"] * 100.0,
                    "T1-T2 incorrect" : l6_tl_ttt_ctrs["all_incorrect"]/l6_tl_ttt_ctrs["total"] * 100.0,
                    "T1 incorrect T2 correct" : l6_tl_ttt_ctrs["T1_incorrect_T2_correct"]/l6_tl_ttt_ctrs["total"] * 100.0, 
                    "T1 correct T2 incorrect" : l6_tl_ttt_ctrs["T1_correct_T2_incorrect"]/l6_tl_ttt_ctrs["total"] * 100.0}

    l8_fb_eff_dct = {"T1-T2 correct" : l8_ttt_ctrs["all_correct"]/l8_ttt_ctrs["total"] * 100.0,
                    "T1-T2 incorrect" : l8_ttt_ctrs["all_incorrect"]/l8_ttt_ctrs["total"] * 100.0,
                    "T1 incorrect T2 correct" : l8_ttt_ctrs["T1_incorrect_T2_correct"]/l8_ttt_ctrs["total"] * 100.0, 
                    "T1 correct T2 incorrect" : l8_ttt_ctrs["T1_correct_T2_incorrect"]/l8_ttt_ctrs["total"] * 100.0}

    def getLowerAndUpperConf(k, N):
        efficiency = k/float(N)
        lower_error = ROOT.TEfficiency.ClopperPearson(N, k,  0.683,  False)#efficiency - ROOT.TEfficiency.ClopperPearson(N, k,  0.683,  False)
        upper_error = ROOT.TEfficiency.ClopperPearson(N, k,  0.683,  True)# - efficiency
        return [lower_error * 100.0, upper_error * 100.0]


    ttt_eff_table = []
    float_formatter = lambda CI : "{:.2f}, {:.2f}".format(*CI)

    suffix_str = lambda num : " ({:.0f})".format(num)

    ttt_eff_table.append([ "S4", "T1 correct"+suffix_str(s4_ttt_ctrs["correct"]), s4_eff_dct["T1 correct"], float_formatter(getLowerAndUpperConf(s4_ttt_ctrs["correct"], s4_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect"+suffix_str(s4_ttt_ctrs["incorrect"]), s4_eff_dct["T1 incorrect"], float_formatter(getLowerAndUpperConf(s4_ttt_ctrs["incorrect"], s4_ttt_ctrs["total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L6 (fb only)", "T1-T2 correct"+suffix_str(l6_fb_ttt_ctrs["all_correct"]), l6_fb_eff_dct["T1-T2 correct"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["all_correct"], l6_fb_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1-T2 incorrect"+suffix_str(l6_fb_ttt_ctrs["all_incorrect"]), l6_fb_eff_dct["T1-T2 incorrect"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["all_incorrect"], l6_fb_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect T2 correct"+suffix_str(l6_fb_ttt_ctrs["T1_incorrect_T2_correct"]), l6_fb_eff_dct["T1 incorrect T2 correct"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T1_incorrect_T2_correct"], l6_fb_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 correct T2 incorrect"+suffix_str(l6_fb_ttt_ctrs["T1_correct_T2_incorrect"]), l6_fb_eff_dct["T1 correct T2 incorrect"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T1_correct_T2_incorrect"], l6_fb_ttt_ctrs["total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L8 (fb only)", "T1-T2 correct"+suffix_str(l8_ttt_ctrs["all_correct"]), l8_fb_eff_dct["T1-T2 correct"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["all_correct"], l8_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1-T2 incorrect"+suffix_str(l8_ttt_ctrs["all_incorrect"]), l8_fb_eff_dct["T1-T2 incorrect"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["all_incorrect"], l8_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect T2 correct"+suffix_str(l8_ttt_ctrs["T1_incorrect_T2_correct"]), l8_fb_eff_dct["T1 incorrect T2 correct"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T1_incorrect_T2_correct"], l8_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 correct T2 incorrect"+suffix_str(l8_ttt_ctrs["T1_correct_T2_incorrect"]), l8_fb_eff_dct["T1 correct T2 incorrect"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T1_correct_T2_incorrect"], l8_ttt_ctrs["total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L6 (fb + tl)", "T1-T2 correct"+suffix_str(l6_tl_ttt_ctrs["all_correct"]), l6_tl_eff_dct["T1-T2 correct"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["all_correct"], l6_tl_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1-T2 incorrect"+suffix_str(l6_tl_ttt_ctrs["all_incorrect"]), l6_tl_eff_dct["T1-T2 incorrect"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["all_incorrect"], l6_tl_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect T2 correct"+suffix_str(l6_tl_ttt_ctrs["T1_incorrect_T2_correct"]), l6_tl_eff_dct["T1 incorrect T2 correct"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["T1_incorrect_T2_correct"], l6_tl_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 correct T2 incorrect"+suffix_str(l6_tl_ttt_ctrs["T1_correct_T2_incorrect"]), l6_tl_eff_dct["T1 correct T2 incorrect"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["T1_correct_T2_incorrect"], l6_tl_ttt_ctrs["total"])) ])

    print(tabulate.tabulate(ttt_eff_table, ["Track type","Time","Efficiency","Lower, Upper 68.3% conf level"], tablefmt='latex_raw', floatfmt="4.2f",numalign="right", colalign=("right", "right","right", "right")))

    ineff_fmt = lambda CI : "{:.2f}%, ({:.1f})".format(*CI)

    s4_incorr = getNum(4,"t1_isCorrect==0")
    l6_fb_t1_incorr = getNum(6,"t1_isCorrect==0&&fbc_t2!=-1")
    l6_fb_t2_incorr = getNum(6,"t2_isCorrect==0&&fbc_t2!=-1")
    l8_fb_t1_incorr = getNum(8,"t1_isCorrect==0")
    l8_fb_t2_incorr = getNum(8,"t2_isCorrect==0")
    l6_tl_t1_incorr = getNum(6,"t1_isCorrect==0&&tl_t!=-1")

    ineff_origin_table = []
    ineff_origin_table.append([ "S4", "T1 incorrect ("+str(s4_incorr)+")", ineff_fmt([100.0 * getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==1") / s4_incorr, getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==1")]), 
                                                                        ineff_fmt([100.0 * getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==2") / s4_incorr, getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==2")]), 
                                                                        ineff_fmt([100.0 * getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==0") / s4_incorr, getNum(4,"t1_isCorrect==0&&t1_incorrect_origin==0")]) ])
    ineff_origin_table.append([])
    ineff_origin_table.append([ "L6 (fb only)", "T1 incorrect ("+str(l6_fb_t1_incorr)+")", ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==1") / l6_fb_t1_incorr, getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==1")]), 
                                                                                        ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==2") / l6_fb_t1_incorr, getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==2")]), 
                                                                                        ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==0") / l6_fb_t1_incorr, getNum(6,"t1_isCorrect==0&&fbc_t2!=-1&&t1_incorrect_origin==0")])  ])
                                                                                        
    ineff_origin_table.append([ "", "T2 incorrect ("+str(l6_fb_t2_incorr)+")", ineff_fmt([100.0 * getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==1") / l6_fb_t2_incorr, getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==1")]), 
                                                                            ineff_fmt([100.0 * getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==2") / l6_fb_t2_incorr, getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==2")]), 
                                                                            ineff_fmt([100.0 * getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==0") / l6_fb_t2_incorr, getNum(6,"t2_isCorrect==0&&fbc_t2!=-1&&t2_incorrect_origin==0")]) ])
    ineff_origin_table.append([])
    ineff_origin_table.append([ "L8 (fb only)", "T1 incorrect ("+str(l8_fb_t1_incorr)+")", ineff_fmt([100.0 * getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==1") / l8_fb_t1_incorr, getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==1")]), 
                                                                                        ineff_fmt([100.0 * getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==2") / l8_fb_t1_incorr, getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==2")]), 
                                                                                        ineff_fmt([100.0 * getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==0") / l8_fb_t1_incorr, getNum(8,"t1_isCorrect==0&&t1_incorrect_origin==0")])  ])

    ineff_origin_table.append([ "", "T2 incorrect ("+str(l8_fb_t2_incorr)+")", ineff_fmt([100.0 * getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==1") / l8_fb_t2_incorr, getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==1")]), 
                                                                            ineff_fmt([100.0 * getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==2") / l8_fb_t2_incorr, getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==2")]), 
                                                                            ineff_fmt([100.0 * getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==0") / l8_fb_t2_incorr, getNum(8,"t2_isCorrect==0&&t2_incorrect_origin==0")])    ])
    ineff_origin_table.append([])
    ineff_origin_table.append([ "L6 (fb + tl)", "T1 incorrect ("+str(l6_tl_t1_incorr)+")", ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==1") / l6_tl_t1_incorr, getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==1")]), 
                                                                                        ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==2") / l6_tl_t1_incorr, getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==2")]), 
                                                                                        ineff_fmt([100.0 * getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==0") / l6_tl_t1_incorr, getNum(6,"t1_isCorrect==0&&tl_t!=-1&&t1_incorrect_origin==0")])   ])

    print(tabulate.tabulate(ineff_origin_table, ["Track type","Time", "Recurler PU", "Track PU", "Correct hit not in cluster"], tablefmt='latex_raw'))

    eff_ctr_fmt = lambda eff, ctr : "{:.2f}%, ({:.1f})".format(eff, ctr)

    # print("NOTE: Clean - just means one tid/hid in cluster (not necessarily correct matched to correct cl)")

    cl_type_trk = {
        "s4_t1" : getNum(4,"fbc_t1_PU_type>=0"),
        "s4_t1_clean" : getNum(4,"fbc_t1_PU_type==0"),
        "s4_t1_recurlerPU" : getNum(4,"fbc_t1_PU_type==1"),
        "s4_t1_trackPU" : getNum(4,"fbc_t1_PU_type==2"),
        "s4_t1_mixedPU" : getNum(4,"fbc_t1_PU_type==3"),

        "l6f_t1" : getNum(6,"fbc_t2!=-1&&fbc_t1_PU_type>=0"),
        "l6f_t1_clean" : getNum(6,"fbc_t2!=-1&&fbc_t1_PU_type==0"),
        "l6f_t1_recurlerPU" : getNum(6,"fbc_t2!=-1&&fbc_t1_PU_type==1"),
        "l6f_t1_trackPU" : getNum(6,"fbc_t2!=-1&&fbc_t1_PU_type==2"),
        "l6f_t1_mixedPU" : getNum(6,"fbc_t2!=-1&&fbc_t1_PU_type==3"),

        "l6f_t2" : getNum(6,"fbc_t2_PU_type>=0"),
        "l6f_t2_clean" : getNum(6,"fbc_t2_PU_type==0"),
        "l6f_t2_recurlerPU" : getNum(6,"fbc_t2_PU_type==1"),
        "l6f_t2_trackPU" : getNum(6,"fbc_t2_PU_type==2"),
        "l6f_t2_mixedPU" : getNum(6,"fbc_t2_PU_type==3"),

        "l8_t1" : getNum(8,"fbc_t1_PU_type>=0"),
        "l8_t1_clean" : getNum(8,"fbc_t1_PU_type==0"),
        "l8_t1_recurlerPU" : getNum(8,"fbc_t1_PU_type==1"),
        "l8_t1_trackPU" : getNum(8,"fbc_t1_PU_type==2"),
        "l8_t1_mixedPU" : getNum(8,"fbc_t1_PU_type==3"),

        "l8_t2" : getNum(8,"fbc_t2_PU_type>=0"),
        "l8_t2_clean" : getNum(8,"fbc_t2_PU_type==0"),
        "l8_t2_recurlerPU" : getNum(8,"fbc_t2_PU_type==1"),
        "l8_t2_trackPU" : getNum(8,"fbc_t2_PU_type==2"),
        "l8_t2_mixedPU" : getNum(8,"fbc_t2_PU_type==3"),

        "l6t_t1" : getNum(6,"tl_t!=-1&&fbc_t1_PU_type>=0"),
        "l6t_t1_clean" : getNum(6,"tl_t!=-1&&fbc_t1_PU_type==0"),
        "l6t_t1_recurlerPU" : getNum(6,"tl_t!=-1&&fbc_t1_PU_type==1"),
        "l6t_t1_trackPU" : getNum(6,"tl_t!=-1&&fbc_t1_PU_type==2"),
        "l6t_t1_mixedPU" : getNum(6,"tl_t!=-1&&fbc_t1_PU_type==3"),
    }

    cl_type_by_track = []
    cl_type_by_track.append(["S4", "T1", eff_ctr_fmt(100.0*cl_type_trk["s4_t1_clean"]/cl_type_trk["s4_t1"], cl_type_trk["s4_t1_clean"]), 
                                        eff_ctr_fmt(100.0*cl_type_trk["s4_t1_recurlerPU"]/cl_type_trk["s4_t1"], cl_type_trk["s4_t1_recurlerPU"]),
                                        eff_ctr_fmt(100.0*cl_type_trk["s4_t1_trackPU"]/cl_type_trk["s4_t1"], cl_type_trk["s4_t1_trackPU"]), 
                                        eff_ctr_fmt(100.0*cl_type_trk["s4_t1_mixedPU"]/cl_type_trk["s4_t1"], cl_type_trk["s4_t1_mixedPU"])])
    cl_type_by_track.append([])
    cl_type_by_track.append(["L6 (fb only)", "T1", eff_ctr_fmt(100.0*cl_type_trk["l6f_t1_clean"]/cl_type_trk["l6f_t1"], cl_type_trk["l6f_t1_clean"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6f_t1_recurlerPU"]/cl_type_trk["l6f_t1"], cl_type_trk["l6f_t1_recurlerPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6f_t1_trackPU"]/cl_type_trk["l6f_t1"], cl_type_trk["l6f_t1_trackPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6f_t1_mixedPU"]/cl_type_trk["l6f_t1"], cl_type_trk["l6f_t1_mixedPU"])])
    cl_type_by_track.append(["", "T2",  eff_ctr_fmt(100.0*cl_type_trk["l6f_t2_clean"]/cl_type_trk["l6f_t2"], cl_type_trk["l6f_t2_clean"]), 
                                        eff_ctr_fmt(100.0*cl_type_trk["l6f_t2_recurlerPU"]/cl_type_trk["l6f_t2"], cl_type_trk["l6f_t2_recurlerPU"]), 
                                        eff_ctr_fmt(100.0*cl_type_trk["l6f_t2_trackPU"]/cl_type_trk["l6f_t2"], cl_type_trk["l6f_t2_trackPU"]), 
                                        eff_ctr_fmt(100.0*cl_type_trk["l6f_t2_mixedPU"]/cl_type_trk["l6f_t2"], cl_type_trk["l6f_t2_mixedPU"])])
    cl_type_by_track.append([])
    cl_type_by_track.append(["L8 (fb only)", "T1", eff_ctr_fmt(100.0*cl_type_trk["l8_t1_clean"]/cl_type_trk["l8_t1"], cl_type_trk["l8_t1_clean"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l8_t1_recurlerPU"]/cl_type_trk["l8_t1"], cl_type_trk["l8_t1_recurlerPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l8_t1_trackPU"]/cl_type_trk["l8_t1"], cl_type_trk["l8_t1_trackPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l8_t1_mixedPU"]/cl_type_trk["l8_t1"], cl_type_trk["l8_t1_mixedPU"])])
    cl_type_by_track.append(["", "T2", eff_ctr_fmt(100.0*cl_type_trk["l8_t2_clean"]/cl_type_trk["l8_t2"], cl_type_trk["l8_t2_clean"]), 
                                    eff_ctr_fmt(100.0*cl_type_trk["l8_t2_recurlerPU"]/cl_type_trk["l8_t2"], cl_type_trk["l8_t2_recurlerPU"]), 
                                    eff_ctr_fmt(100.0*cl_type_trk["l8_t2_trackPU"]/cl_type_trk["l8_t2"], cl_type_trk["l8_t2_trackPU"]), 
                                    eff_ctr_fmt(100.0*cl_type_trk["l8_t2_mixedPU"]/cl_type_trk["l8_t2"], cl_type_trk["l8_t2_mixedPU"])])
    cl_type_by_track.append([])
    cl_type_by_track.append(["L6 (fb + tl)", "T1", eff_ctr_fmt(100.0*cl_type_trk["l6t_t1_clean"]/cl_type_trk["l6t_t1"], cl_type_trk["l6t_t1_clean"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6t_t1_recurlerPU"]/cl_type_trk["l6t_t1"], cl_type_trk["l6t_t1_recurlerPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6t_t1_trackPU"]/cl_type_trk["l6t_t1"], cl_type_trk["l6t_t1_trackPU"]), 
                                                eff_ctr_fmt(100.0*cl_type_trk["l6t_t1_mixedPU"]/cl_type_trk["l6t_t1"], cl_type_trk["l6t_t1_mixedPU"])])

    print(tabulate.tabulate(cl_type_by_track, ["Track type", "Time", "Clean", "Recurler PU", "Track PU", "Mixed PU"], tablefmt='latex_raw'))

    ttt_rel_eff_table = []

    s4_possible = getNum(4,"t1_isPossible==1")
    l6_tl_possible = getNum(6,"t1_isPossible==1&&t2_tl_isPossible==1&&tl_t!=-1.0") #+ getNum(6,"t1_isPossible==1&&t2_tl_isPossible==01&&tl_t!=-1.0")
    l6_fb_possible = getNum(6,"t1_isPossible==1&&t2_fb_isPossible==1&&fbc_t2!=-1.0") #+ getNum(6,"t1_isPossible==1&&t2_fb_isPossible==0&&fbc_t2!=-1.0")
    l8_possible = getNum(8,"t1_isPossible==1&&t2_fb_isPossible==1")

    ttt_rel_eff_table.append([ "S4", "T1", 100.0 * s4_ttt_ctrs["total"] / s4_possible])
    ttt_rel_eff_table.append([ "", "No timing", 100.0 * getNum(4,"t1_isCorrect==-1") / s4_possible])
    ttt_rel_eff_table.append([])
    ttt_rel_eff_table.append([ "L8 (fb only)", "T1-T2 present", 100.0 * l8_ttt_ctrs["total"] / l8_possible])
    ttt_rel_eff_table.append([ "", "T1 only", 100.0 * getNum(8,"t1_isCorrect!=-1&&t2_isCorrect==-1") / l8_possible])
    ttt_rel_eff_table.append([ "", "T2 only", 100.0 * getNum(8,"t1_isCorrect==-1&&t2_isCorrect!=-1") / l8_possible])
    ttt_rel_eff_table.append([ "", "No timing", 100.0 * getNum(8,"t1_isCorrect==-1&&t2_isCorrect==-1") / l8_possible])
    ttt_rel_eff_table.append([])
    ttt_rel_eff_table.append([ "L6 (fb only)", "T1-T2 present", 100.0 * getNum(6,"t1_isCorrect!=-1&&t2_isCorrect!=-1&&fbc_t2!=-1.0") / l6_fb_possible])
    # ttt_rel_eff_table.append([ "", "T1 only", 100.0 * getNum(6,"t1_isCorrect!=-1&&t2_isCorrect==-1")  / l6_fb_possible])
    ttt_rel_eff_table.append([ "", "T2 only", 100.0 * getNum(6,"t1_isCorrect==-1&&t2_isCorrect!=-1&&fbc_t2!=-1.0")  / l6_fb_possible]) 
    # ttt_rel_eff_table.append([ "", "No timing", 100.0 * getNum(6,"t1_isCorrect==-1&&t2_isCorrect==-1")  / l6_fb_possible])
    ttt_rel_eff_table.append([])
    ttt_rel_eff_table.append([ "L6 (fb + tl)", "T1-T2 present", 100.0 * getNum(6,"t1_isCorrect!=-1&&t2_isCorrect!=-1&&tl_t!=-1.0") / l6_tl_possible])
    # ttt_rel_eff_table.append([ "", "T1 only", 100.0 * getNum(6,"t1_isCorrect!=-1&&t2_isCorrect==-1")  / l6_tl_possible])
    ttt_rel_eff_table.append([ "", "T2 only", 100.0 * getNum(6,"t1_isCorrect==-1&&t2_isCorrect!=-1&&tl_t!=-1.0")  / l6_tl_possible]) 
    # ttt_rel_eff_table.append([ "", "No timing", 100.0 * getNum(6,"t1_isCorrect==-1&&t2_isCorrect==-1")  / l6_tl_possible])

    print(tabulate.tabulate(ttt_rel_eff_table, ["Track type","Time","Relative Efficiency (to possible time detector hits)"], tablefmt='latex_raw', floatfmt="4.2f",numalign="right", ))


    s4_corr = getNum(4,"t1_isCorrect==1&&t1_isBest!=-1")
    l6_fb_t1_corr = getNum(6,"t1_isCorrect==1&&fbc_t2!=-1&&t1_isBest!=-1")
    l6_fb_t2_corr = getNum(6,"t2_isCorrect==1&&fbc_t2!=-1&&t2_isBest!=-1")
    l8_fb_t1_corr = getNum(8,"t1_isCorrect==1&&t1_isBest!=-1")
    l8_fb_t2_corr = getNum(8,"t2_isCorrect==1&&t2_isBest!=-1")
    l6_tl_t1_corr = getNum(6,"t1_isCorrect==1&&tl_t!=-1&&t1_isBest!=-1")

    # For correctly assigned clusters
    best_eff_table = []
    best_eff_table.append([ "S4", "T1 best", ineff_fmt([100.0 * getNum(4,"t1_isCorrect==1&&t1_isBest==1") / s4_corr, getNum(4,"t1_isCorrect==1&&t1_isBest==1")]) ])
    best_eff_table.append([])
    best_eff_table.append([ "L6 (fb only)", "T1 best", ineff_fmt([100.0 * getNum(6,"t1_isCorrect==1&&t1_isBest==1&&fbc_t2!=-1") / l6_fb_t1_corr, getNum(6,"t1_isCorrect==1&&t1_isBest==1&&fbc_t2!=-1")]) ])
    best_eff_table.append([ "", "T2 best",  ineff_fmt([100.0 * getNum(6,"t2_isCorrect==1&&t2_isBest==1&&fbc_t2!=-1") / l6_fb_t2_corr, getNum(6,"t2_isCorrect==1&&t2_isBest==1&&fbc_t2!=-1")]) ])
    best_eff_table.append([])
    best_eff_table.append([ "L8 (fb only)", "T1 best", ineff_fmt([100.0 * getNum(8,"t1_isCorrect==1&&t1_isBest==1") / l8_fb_t1_corr, getNum(8,"t1_isCorrect==1&&t1_isBest==1")]) ])
    best_eff_table.append([ "", "T2 best", ineff_fmt([100.0 * getNum(8,"t2_isCorrect==1&&t2_isBest==1") / l8_fb_t2_corr, getNum(8,"t2_isCorrect==1&&t2_isBest==1")]) ])
    best_eff_table.append([])
    best_eff_table.append([ "L6 (fb + tl)", "T1 best", ineff_fmt([100.0 * getNum(6,"t1_isCorrect==1&&t1_isBest==1&&tl_t!=-1") / l6_tl_t1_corr, getNum(6,"t1_isCorrect==1&&t1_isBest==1&&tl_t!=-1")]) ])


    print(tabulate.tabulate(best_eff_table, ["Track type","Time","Efficiency"], tablefmt='latex_raw', floatfmt="4.2f",numalign="right"))

    getNumAllTrks = lambda cut : float(framesTree.Draw("","mc_prime&&"+str(cut)))

    ns_ctr_dct = {
        "ns_1_all" : getNumAllTrks("fbc_t1_ns==1&&fbc_t1_PU_type>=0") + getNumAllTrks("fbc_t2_ns==1&&fbc_t2_PU_type>=0"),
        "ns_1_clean" : getNumAllTrks("fbc_t1_ns==1&&fbc_t1_PU_type==0") + getNumAllTrks("fbc_t2_ns==1&&fbc_t2_PU_type==0"),
        "ns_1_recurlerPU" : getNumAllTrks("fbc_t1_ns==1&&fbc_t1_PU_type==1") + getNumAllTrks("fbc_t2_ns==1&&fbc_t2_PU_type==1"),
        "ns_1_trackPU" : getNumAllTrks("fbc_t1_ns==1&&fbc_t1_PU_type==2") + getNumAllTrks("fbc_t2_ns==1&&fbc_t2_PU_type==2"),
        "ns_1_mixedPU" : getNumAllTrks("fbc_t1_ns==1&&fbc_t1_PU_type==3") + getNumAllTrks("fbc_t2_ns==1&&fbc_t2_PU_type==3"),

        "ns_2_all" : getNumAllTrks("fbc_t1_ns==2&&fbc_t1_PU_type>=0") + getNumAllTrks("fbc_t2_ns==2&&fbc_t2_PU_type>=0"),
        "ns_2_clean" : getNumAllTrks("fbc_t1_ns==2&&fbc_t1_PU_type==0") + getNumAllTrks("fbc_t2_ns==2&&fbc_t2_PU_type==0"),
        "ns_2_recurlerPU" : getNumAllTrks("fbc_t1_ns==2&&fbc_t1_PU_type==1") + getNumAllTrks("fbc_t2_ns==2&&fbc_t2_PU_type==1"),
        "ns_2_trackPU" : getNumAllTrks("fbc_t1_ns==2&&fbc_t1_PU_type==2") + getNumAllTrks("fbc_t2_ns==2&&fbc_t2_PU_type==2"),
        "ns_2_mixedPU" : getNumAllTrks("fbc_t1_ns==2&&fbc_t1_PU_type==3") + getNumAllTrks("fbc_t2_ns==2&&fbc_t2_PU_type==3"),

        "ns_3_all" : getNumAllTrks("fbc_t1_ns==3&&fbc_t1_PU_type>=0") + getNumAllTrks("fbc_t2_ns==3&&fbc_t2_PU_type>=0"),
        "ns_3_clean" : getNumAllTrks("fbc_t1_ns==3&&fbc_t1_PU_type==0") + getNumAllTrks("fbc_t2_ns==3&&fbc_t2_PU_type==0"),
        "ns_3_recurlerPU" : getNumAllTrks("fbc_t1_ns==3&&fbc_t1_PU_type==1") + getNumAllTrks("fbc_t2_ns==3&&fbc_t2_PU_type==1"),
        "ns_3_trackPU" : getNumAllTrks("fbc_t1_ns==3&&fbc_t1_PU_type==2") + getNumAllTrks("fbc_t2_ns==3&&fbc_t2_PU_type==2"),
        "ns_3_mixedPU" : getNumAllTrks("fbc_t1_ns==3&&fbc_t1_PU_type==3") + getNumAllTrks("fbc_t2_ns==3&&fbc_t2_PU_type==3"),

        "ns_4_all" : getNumAllTrks("fbc_t1_ns==4&&fbc_t1_PU_type>=0") + getNumAllTrks("fbc_t1_ns==4&&fbc_t2_PU_type>=0"),
        "ns_4_clean" : getNumAllTrks("fbc_t1_ns==4&&fbc_t1_PU_type==0") + getNumAllTrks("fbc_t1_ns==4&&fbc_t2_PU_type==0"),
        "ns_4_recurlerPU" : getNumAllTrks("fbc_t1_ns==4&&fbc_t1_PU_type==1") + getNumAllTrks("fbc_t1_ns==4&&fbc_t2_PU_type==1"),
        "ns_4_trackPU" : getNumAllTrks("fbc_t1_ns==4&&fbc_t1_PU_type==2") + getNumAllTrks("fbc_t1_ns==4&&fbc_t2_PU_type==2"),
        "ns_4_mixedPU" : getNumAllTrks("fbc_t1_ns==4&&fbc_t1_PU_type==3") + getNumAllTrks("fbc_t1_ns==4&&fbc_t2_PU_type==3"),

        "ns_5_all" : getNumAllTrks("fbc_t1_ns==5&&fbc_t1_PU_type>=0") + getNumAllTrks("fbc_t1_ns==5&&fbc_t2_PU_type>=0"),
        "ns_5_clean" : getNumAllTrks("fbc_t1_ns==5&&fbc_t1_PU_type==0") + getNumAllTrks("fbc_t1_ns==5&&fbc_t2_PU_type==0"),
        "ns_5_recurlerPU" : getNumAllTrks("fbc_t1_ns==5&&fbc_t1_PU_type==1") + getNumAllTrks("fbc_t1_ns==5&&fbc_t2_PU_type==1"),
        "ns_5_trackPU" : getNumAllTrks("fbc_t1_ns==5&&fbc_t1_PU_type==2") + getNumAllTrks("fbc_t1_ns==5&&fbc_t2_PU_type==2"),
        "ns_5_mixedPU" : getNumAllTrks("fbc_t1_ns==5&&fbc_t1_PU_type==3") + getNumAllTrks("fbc_t1_ns==5&&fbc_t2_PU_type==3"),
    }

    cl_ns_PU_table = []
    cl_ns_PU_table.append([1,eff_ctr_fmt(100.0*ns_ctr_dct["ns_1_clean"]/ns_ctr_dct["ns_1_all"], ns_ctr_dct["ns_1_clean"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_1_recurlerPU"]/ns_ctr_dct["ns_1_all"], ns_ctr_dct["ns_1_recurlerPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_1_trackPU"]/ns_ctr_dct["ns_1_all"], ns_ctr_dct["ns_1_trackPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_1_mixedPU"]/ns_ctr_dct["ns_1_all"], ns_ctr_dct["ns_1_mixedPU"])])
    cl_ns_PU_table.append([])
    cl_ns_PU_table.append([2,eff_ctr_fmt(100.0*ns_ctr_dct["ns_2_clean"]/ns_ctr_dct["ns_2_all"], ns_ctr_dct["ns_2_clean"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_2_recurlerPU"]/ns_ctr_dct["ns_2_all"], ns_ctr_dct["ns_2_recurlerPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_2_trackPU"]/ns_ctr_dct["ns_2_all"], ns_ctr_dct["ns_2_trackPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_2_mixedPU"]/ns_ctr_dct["ns_2_all"], ns_ctr_dct["ns_2_mixedPU"])])
    cl_ns_PU_table.append([])
    cl_ns_PU_table.append([3,eff_ctr_fmt(100.0*ns_ctr_dct["ns_3_clean"]/ns_ctr_dct["ns_3_all"], ns_ctr_dct["ns_3_clean"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_3_recurlerPU"]/ns_ctr_dct["ns_3_all"], ns_ctr_dct["ns_3_recurlerPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_3_trackPU"]/ns_ctr_dct["ns_3_all"], ns_ctr_dct["ns_3_trackPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_3_mixedPU"]/ns_ctr_dct["ns_3_all"], ns_ctr_dct["ns_3_mixedPU"])])
    cl_ns_PU_table.append([])
    cl_ns_PU_table.append([4,eff_ctr_fmt(100.0*ns_ctr_dct["ns_4_clean"]/ns_ctr_dct["ns_4_all"], ns_ctr_dct["ns_4_clean"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_4_recurlerPU"]/ns_ctr_dct["ns_4_all"], ns_ctr_dct["ns_4_recurlerPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_4_trackPU"]/ns_ctr_dct["ns_4_all"], ns_ctr_dct["ns_4_trackPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_4_mixedPU"]/ns_ctr_dct["ns_4_all"], ns_ctr_dct["ns_4_mixedPU"])])
    cl_ns_PU_table.append([])
    cl_ns_PU_table.append([5,eff_ctr_fmt(100.0*ns_ctr_dct["ns_5_clean"]/ns_ctr_dct["ns_5_all"], ns_ctr_dct["ns_5_clean"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_5_recurlerPU"]/ns_ctr_dct["ns_5_all"], ns_ctr_dct["ns_5_recurlerPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_5_trackPU"]/ns_ctr_dct["ns_5_all"], ns_ctr_dct["ns_5_trackPU"]),eff_ctr_fmt(100.0*ns_ctr_dct["ns_5_mixedPU"]/ns_ctr_dct["ns_5_all"], ns_ctr_dct["ns_5_mixedPU"])])

    print(tabulate.tabulate(cl_ns_PU_table, ["Numb Segs assigned to fb cluster","Clean","Recurler PU","Track PU","Mixed PU"], tablefmt='latex_raw', floatfmt="3.1f",numalign="right"))


    # # clsMadeWithFit = False
    # if (getNumAllTrks("fbc_t1_madeWithFit==1||fbc_t2_madeWithFit==1")): clsMadeWithFit = True

    t1Cut = ""
    t2Cut = ""
    if (clsMadeWithFit):
        t1_orig = "&&fbc_t1_origClTrPU==1"
        t2_orig = "&&fbc_t2_origClTrPU==1"
        t1Cut = "&&fbc_t1_madeWithFit==1"#+t1_orig
        t2Cut = "&&fbc_t2_madeWithFit==1"#+t2_orig
    else:
        t1_PU = "&&fbc_t1_PU_type==2"
        t2_PU = "&&fbc_t2_PU_type==2"
        # t1Cut = "&&((fbc_t1_ns==1&&(fbc_dt_t1>singleTrk_dt_min&&fbc_dt_t1<singleTrk_dt_max))||(fbc_t1_ns>1&&fbc_dt_t1>multiTrk_dt_min))&&fbc_t1_PU_type==2"
        # t2Cut = "&&((fbc_t2_ns==1&&(fbc_dt_t2>singleTrk_dt_min&&fbc_dt_t2<singleTrk_dt_max))||(fbc_t2_ns>1&&fbc_dt_t2>multiTrk_dt_min))&&fbc_t2_PU_type==2"
        # t1Cut = "&&(fbc_t1_ns>1&&fbc_dt_t1>="+str(multiTrk_dt_min)+")"#+t1_PU
        # t2Cut = "&&(fbc_t2_ns>1&&fbc_dt_t2>="+str(multiTrk_dt_min)+")"#+t2_PU
        t1Cut = "&&(fbc_t1_ns>=1&&(fbc_dt_t1>={}&&fbc_dt_t1<={}))".format(str(singleTrk_dt_min), str(singleTrk_dt_max))
        t2Cut = "&&(fbc_t2_ns>=1&&(fbc_dt_t2>={}&&fbc_dt_t2<={}))".format(str(singleTrk_dt_min), str(singleTrk_dt_max))

    print(t1Cut)
    print(t2Cut)

    def getNumT1(nhit, cut):
        allCut = cut + t1Cut
        # print(allCut)
        return getNum(nhit, allCut)

    def getNumT2(nhit, cut):
        allCut = cut + t2Cut
        # print(allCut)
        return getNum(nhit, allCut)

    s4_ttt_ctrs = {"total" : getNumT1(4,"t1_isCorrect!=-1"), "correct" : getNumT1(4,"t1_isCorrect==1"), "incorrect" : getNumT1(4,"t1_isCorrect==0")}

    l6_fb_ttt_ctrs = {"T1_total" : getNumT1(6,"t1_isCorrect!=-1&&fbc_t2!=-1"), 
                        "T1_correct" : getNumT1(6,"t1_isCorrect==1&&fbc_t2!=-1"), 
                        "T1_incorrect" : getNumT1(6,"t1_isCorrect==0&&fbc_t2!=-1"), 
                        "T2_total" : getNumT2(6,"t2_isCorrect!=-1&&fbc_t2!=-1"), 
                        "T2_correct" : getNumT2(6,"t2_isCorrect==1&&fbc_t2!=-1"), 
                        "T2_incorrect" : getNumT2(6,"t2_isCorrect==0&&fbc_t2!=-1")}

    l6_tl_ttt_ctrs = {"T1_total" : getNumT1(6,"t1_isCorrect!=-1&&tl_t!=-1"), 
                        "T1_correct" : getNumT1(6,"t1_isCorrect==1&&tl_t!=-1"), 
                        "T1_incorrect" : getNumT1(6,"t1_isCorrect==0&&tl_t!=-1")}

    l8_ttt_ctrs = {"T1_total" : getNumT1(8,"t1_isCorrect!=-1"), 
                    "T1_correct" : getNumT1(8,"t1_isCorrect==1"), 
                    "T1_incorrect" : getNumT1(8,"t1_isCorrect==0"), 
                    "T2_total" : getNumT2(8,"t2_isCorrect!=-1"), 
                    "T2_correct" : getNumT2(8,"t2_isCorrect==1"), 
                    "T2_incorrect" : getNumT2(8,"t2_isCorrect==0")}

    s4_eff_dct = {"T1_correct" : s4_ttt_ctrs["correct"]/s4_ttt_ctrs["total"] * 100.0,
                    "T1_incorrect" : s4_ttt_ctrs["incorrect"]/s4_ttt_ctrs["total"] * 100.0}

    l6_fb_eff_dct = {"T1_correct" : l6_fb_ttt_ctrs["T1_correct"]/l6_fb_ttt_ctrs["T1_total"] * 100.0,
                        "T1_incorrect" : l6_fb_ttt_ctrs["T1_incorrect"]/l6_fb_ttt_ctrs["T1_total"] * 100.0,
                        "T2_correct" : l6_fb_ttt_ctrs["T2_correct"]/l6_fb_ttt_ctrs["T2_total"] * 100.0, 
                        "T2_incorrect" : l6_fb_ttt_ctrs["T2_incorrect"]/l6_fb_ttt_ctrs["T2_total"] * 100.0}

    l6_tl_eff_dct = {"T1_correct" : l6_tl_ttt_ctrs["T1_correct"]/l6_tl_ttt_ctrs["T1_total"] * 100.0,
                        "T1_incorrect" : l6_tl_ttt_ctrs["T1_incorrect"]/l6_tl_ttt_ctrs["T1_total"] * 100.0}

    l8_fb_eff_dct = {"T1_correct" : l8_ttt_ctrs["T1_correct"]/l8_ttt_ctrs["T1_total"] * 100.0,
                        "T1_incorrect" : l8_ttt_ctrs["T1_incorrect"]/l8_ttt_ctrs["T1_total"] * 100.0,
                        "T2_correct" : l8_ttt_ctrs["T2_correct"]/l8_ttt_ctrs["T2_total"] * 100.0, 
                        "T2_incorrect" : l8_ttt_ctrs["T2_incorrect"]/l8_ttt_ctrs["T2_total"] * 100.0}

    ttt_eff_table = []
    ttt_eff_table.append([ "S4", "T1 correct"+suffix_str(s4_ttt_ctrs["correct"]), s4_eff_dct["T1_correct"], float_formatter(getLowerAndUpperConf(s4_ttt_ctrs["correct"], s4_ttt_ctrs["total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect"+suffix_str(s4_ttt_ctrs["incorrect"]), s4_eff_dct["T1_incorrect"], float_formatter(getLowerAndUpperConf(s4_ttt_ctrs["incorrect"], s4_ttt_ctrs["total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L6 (fb only)", "T1 correct"+suffix_str(l6_fb_ttt_ctrs["T1_correct"]), l6_fb_eff_dct["T1_correct"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T1_correct"], l6_fb_ttt_ctrs["T1_total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect"+suffix_str(l6_fb_ttt_ctrs["T1_incorrect"]), l6_fb_eff_dct["T1_incorrect"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T1_incorrect"], l6_fb_ttt_ctrs["T1_total"])) ])
    ttt_eff_table.append([ "", "T2 correct"+suffix_str(l6_fb_ttt_ctrs["T2_correct"]), l6_fb_eff_dct["T2_correct"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T2_correct"], l6_fb_ttt_ctrs["T2_total"])) ])
    ttt_eff_table.append([ "", "T2 incorrect"+suffix_str(l6_fb_ttt_ctrs["T2_incorrect"]), l6_fb_eff_dct["T2_incorrect"], float_formatter(getLowerAndUpperConf(l6_fb_ttt_ctrs["T2_incorrect"], l6_fb_ttt_ctrs["T2_total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L8 (fb only)", "T1 correct"+suffix_str(l8_ttt_ctrs["T1_correct"]), l8_fb_eff_dct["T1_correct"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T1_correct"], l8_ttt_ctrs["T1_total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect"+suffix_str(l8_ttt_ctrs["T1_incorrect"]), l8_fb_eff_dct["T1_incorrect"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T1_incorrect"], l8_ttt_ctrs["T1_total"])) ])
    ttt_eff_table.append([ "", "T2 correct"+suffix_str(l8_ttt_ctrs["T2_correct"]), l8_fb_eff_dct["T2_correct"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T2_correct"], l8_ttt_ctrs["T2_total"])) ])
    ttt_eff_table.append([ "", "T2 incorrect"+suffix_str(l8_ttt_ctrs["T2_incorrect"]), l8_fb_eff_dct["T2_incorrect"], float_formatter(getLowerAndUpperConf(l8_ttt_ctrs["T2_incorrect"], l8_ttt_ctrs["T2_total"])) ])
    ttt_eff_table.append([])
    ttt_eff_table.append([ "L6 (fb + tl)", "T1 correct"+suffix_str(l6_tl_ttt_ctrs["T1_correct"]), l6_tl_eff_dct["T1_correct"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["T1_correct"], l6_tl_ttt_ctrs["T1_total"])) ])
    ttt_eff_table.append([ "", "T1 incorrect"+suffix_str(l6_tl_ttt_ctrs["T1_incorrect"]), l6_tl_eff_dct["T1_incorrect"], float_formatter(getLowerAndUpperConf(l6_tl_ttt_ctrs["T1_incorrect"], l6_tl_ttt_ctrs["T1_total"])) ])

    print(tabulate.tabulate(ttt_eff_table, ["Track type","Time","Efficiency","Lower, Upper 68.3% conf level"], tablefmt='latex_raw', floatfmt="4.2f",numalign="right", colalign=("right", "right","right", "right")))

if args.stackHistos:

    dt_can = ROOT.TCanvas("dt_can", "")
    hstack_dt = ROOT.THStack("hstack_dt", ";Time span [ns];Clusters")

    # hstack_dt.Add(h_fb_dt)

    h_fb_dt_clean.SetLineColor(ROOT.kC0)
    hstack_dt.Add(h_fb_dt_clean)

    h_fb_dt_trackPU.SetLineColor(ROOT.kC1)
    hstack_dt.Add(h_fb_dt_trackPU)

    h_fb_dt_recurlerPU.SetLineColor(ROOT.kC2)
    hstack_dt.Add(h_fb_dt_recurlerPU)

    # h_fb_dt_mixedPU.SetLineColor(ROOT.kC3)
    # hstack_dt.Add(h_fb_dt_mixedPU)
    hstack_dt.Draw("nostack")
    # hstack_dt.Write()

    dt_leg = ROOT.TLegend(0.6,0.6,0.85,0.85)
    # dt_leg.AddEntry(h_fb_dt, "All", "l")
    dt_leg.AddEntry(h_fb_dt_clean, "Clean", "l")
    dt_leg.AddEntry(h_fb_dt_recurlerPU, "Recurler PU", "l")
    dt_leg.AddEntry(h_fb_dt_trackPU, "Track PU", "l")
    # dt_leg.AddEntry(h_fb_dt_mixedPU, "Mixed PU", "l")
    dt_leg.Draw()

    ROOT.gPad.Update()
    ROOT.gPad.SetLogy(1)
    dt_can.SaveAs(args.path+"/cluster_dt_stacked.pdf")

    # 
    # SingleTrack dt_max
    # 
    singleTrk_dt_can = ROOT.TCanvas("singleTrk_dt_can", "")
    hstack_singleTrk_dt = ROOT.THStack("hstack_singleTrk_dt", ";Time span [ns];Single track clusters")

    # hstack_singleTrk_dt.Add(h_fb_singleTrk_dt)

    h_fb_singleTrk_dt_clean.SetLineColor(ROOT.kC0)
    hstack_singleTrk_dt.Add(h_fb_singleTrk_dt_clean)

    h_fb_singleTrk_dt_trackPU.SetLineColor(ROOT.kC1)
    hstack_singleTrk_dt.Add(h_fb_singleTrk_dt_trackPU)

    h_fb_singleTrk_dt_recurlerPU.SetLineColor(ROOT.kC2)
    hstack_singleTrk_dt.Add(h_fb_singleTrk_dt_recurlerPU)

    # h_fb_singleTrk_dt_mixedPU.SetLineColor(ROOT.kC3)
    # hstack_singleTrk_dt.Add(h_fb_singleTrk_dt_mixedPU)
    hstack_singleTrk_dt.Draw("nostack")
    # hstack_singleTrk_dt.Write()

    singleTrk_dt_leg = ROOT.TLegend(0.6,0.6,0.85,0.85)
    # singleTrk_dt_leg.AddEntry(h_fb_singleTrk_dt, "All", "l")
    singleTrk_dt_leg.AddEntry(h_fb_singleTrk_dt_clean, "Clean", "l")
    singleTrk_dt_leg.AddEntry(h_fb_singleTrk_dt_recurlerPU, "Recurler PU", "l")
    singleTrk_dt_leg.AddEntry(h_fb_singleTrk_dt_trackPU, "Track PU", "l")
    # singleTrk_dt_leg.AddEntry(h_fb_singleTrk_dt_mixedPU, "Mixed PU", "l")
    singleTrk_dt_leg.Draw()

    ROOT.gPad.Update()
    ROOT.gPad.SetLogy(1)
    singleTrk_dt_can.SaveAs(args.path+"/cluster_singleTrk_dt_stacked.pdf")

    # 
    # MultiTrack dt_max
    # 
    multiTrk_dt_can = ROOT.TCanvas("multiTrk_dt_can", "")
    hstack_multiTrk_dt = ROOT.THStack("hstack_multiTrk_dt", ";Time span [ns];Multi track clusters")

    # hstack_multiTrk_dt.Add(h_fb_multiTrk_dt)

    h_fb_multiTrk_dt_clean.SetLineColor(ROOT.kC0)
    hstack_multiTrk_dt.Add(h_fb_multiTrk_dt_clean)

    h_fb_multiTrk_dt_trackPU.SetLineColor(ROOT.kC1)
    hstack_multiTrk_dt.Add(h_fb_multiTrk_dt_trackPU)

    h_fb_multiTrk_dt_recurlerPU.SetLineColor(ROOT.kC2)
    hstack_multiTrk_dt.Add(h_fb_multiTrk_dt_recurlerPU)

    # h_fb_multiTrk_dt_mixedPU.SetLineColor(ROOT.kC3)
    # hstack_multiTrk_dt.Add(h_fb_multiTrk_dt_mixedPU)
    hstack_multiTrk_dt.Draw("nostack")
    # hstack_multiTrk_dt.Write()

    multiTrk_dt_leg = ROOT.TLegend(0.6,0.6,0.85,0.85)
    # multiTrk_dt_leg.AddEntry(h_fb_multiTrk_dt, "All", "l")
    multiTrk_dt_leg.AddEntry(h_fb_multiTrk_dt_clean, "Clean", "l")
    multiTrk_dt_leg.AddEntry(h_fb_multiTrk_dt_recurlerPU, "Recurler PU", "l")
    multiTrk_dt_leg.AddEntry(h_fb_multiTrk_dt_trackPU, "Track PU", "l")
    # multiTrk_dt_leg.AddEntry(h_fb_multiTrk_dt_mixedPU, "Mixed PU", "l")
    multiTrk_dt_leg.Draw()

    ROOT.gPad.Update()
    ROOT.gPad.SetLogy(1)
    multiTrk_dt_can.SaveAs(args.path+"/cluster_multiTrk_dt_stacked.pdf")

    # Now ns
    ns_can = ROOT.TCanvas("ns_can", "")
    hstack_ns = ROOT.THStack("hstack_ns", ";Number of assigned tracks;Clusters")

    # hstack_ns.Add(h_fb_ns)

    h_fb_ns_clean.SetLineColor(ROOT.kC0)
    hstack_ns.Add(h_fb_ns_clean)

    h_fb_ns_trackPU.SetLineColor(ROOT.kC1)
    hstack_ns.Add(h_fb_ns_trackPU)

    h_fb_ns_recurlerPU.SetLineColor(ROOT.kC2)
    hstack_ns.Add(h_fb_ns_recurlerPU)

    h_fb_ns_mixedPU.SetLineColor(ROOT.kC3)
    hstack_ns.Add(h_fb_ns_mixedPU)
    hstack_ns.Draw("nostack")
    # hstack_ns.Write()

    ns_leg = ROOT.TLegend(0.6,0.6,0.85,0.85)
    # ns_leg.AddEntry(h_fb_ns, "All", "l")
    ns_leg.AddEntry(h_fb_ns_clean, "Clean", "l")
    ns_leg.AddEntry(h_fb_ns_recurlerPU, "Recurler PU", "l")
    ns_leg.AddEntry(h_fb_ns_trackPU, "Track PU", "l")
    ns_leg.AddEntry(h_fb_ns_mixedPU, "Mixed PU", "l")
    ns_leg.Draw()

    ROOT.gPad.Update()
    ROOT.gPad.SetLogy(1)
    ns_can.SaveAs(args.path+"/cluster_ns_stacked.pdf")

print("Finished...")

output_file.Write()
for teff in eff_list: teff.Write()
output_file.Close()