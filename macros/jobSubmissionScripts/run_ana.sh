source /unix/muons/users/bgayther/setup.sh
source /unix/muons/users/bgayther/mu3e/build/mu3e-activate.sh

mkdir -p /tmp/job_$PBS_JOBID
cd /tmp/job_$PBS_JOBID
mkdir results

OUTDIR_ADJ=$OUTDIR/ana_${SAMPLE}_${SIM_TAG}_${VERTEX_TAG}/job_$PBS_JOBID
mkdir -p $OUTDIR_ADJ

echo "================= RUNNING SIGNAL ANA ==============="
mu3eAnaSignalana ${VERTEX_FILES} mu3e_ana_${SAMPLE}_${SIM_TAG}_${VERTEX_TAG} "Phase I" "${NSIGNAL}" 0 > mu3e_ana_log.txt

cp -r * $OUTDIR_ADJ

rm -rf /tmp/job_$PBS_JOBID
