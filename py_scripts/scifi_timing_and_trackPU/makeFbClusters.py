import random
import ROOT
import numpy as np
import time as time_py
import scipy.stats as st
from scipy.optimize import curve_fit
from scipy.signal import argrelextrema, find_peaks
from collections import defaultdict, Counter
import argparse
import logging
import os
import sys
import copy
import matplotlib
import matplotlib.pyplot as plt
import gc
import pathlib
sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of frames to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='fbRecoHitTimingFit', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

pathlib.Path(args.path).mkdir(parents=True, exist_ok=True)

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792458 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n

# ROOT.TProof.Open("")

############## SIM RES ###############
mppcHitsTree = ROOT.TChain("mppc_hits")

for file in args.inputs:
    print("Adding {} for analysis".format(file))
    mppcHitsTree.Add(file)

nevents = mppcHitsTree.GetEntries()
print("Entries in tree = {}".format(nevents))
if args.nevents >= 0: nevents = args.nevents
# mppcHitsTree.SetProof()

sipmTotal = 0
Nbins = 500

# Histos
# hHitTimeZCorrected = ROOT.TH1F("hHitTimeZCorrected", "Time Resolution per hit (Z corrected);t_{hit} - t_{mc} [ns]", Nbins, -5, 15)


class mppcHit:
    ribbon = -1
    side = -1 # 0 is left, +1 is right
    col = -1

    amplitude = 0 # no of photons per col
    time = 0.0

    # Truth info
    time_mc = 0.0
    crosstalk = -1
    tid = 0
    hid = 0
    hid_g = 0
    zPos = 0.0
    tCorr = 0.0 # using mc zpos

    reco_cl_id = -1

    def __init__(self, ribbon, col, side, amp, time, time_mc, crosstalk, tid, hid, hid_g, reco_cl_id, zPos):
        self.ribbon = ribbon
        self.col = col
        self.side = side
        self.amplitude = amp
        self.time_mc = time_mc
        self.time = time
        self.crosstalk = crosstalk
        self.tid = tid
        self.hid = hid
        self.hid_g = hid_g
        self.reco_cl_id = reco_cl_id
        self.zPos = zPos
        if (self.side==0): dCorr = fb_len_half - self.zPos
        else: dCorr = fb_len_half + self.zPos
        self.tCorr = self.time - dCorr / cn
        # self.tCorr_raw = self.raw_time - dCorr / cn

    def printHit(self):
        print('ribbon {}, col {}, side {}, amp {}, time_mc {}, time {}, tCorr {}, tid {}, hid {}, hid_g {}, xtalk_status {}, reco_cl_id {}, zPos {}'.format(
            self.ribbon, self.col, self.side, self.amplitude, self.time_mc, self.time, self.tCorr, self.tid, self.hid, self.hid_g, self.crosstalk, self.reco_cl_id, self.zPos))


class fbCluster:
    cl_id = -1
    side = 0
    ribbon = -1
    tid = 0
    hid = 0
    hid_g = 0
    trackPU = 0
    recurlerPU = 0
    prevFramePU = 0

    def __init__(self, cl_id, tid, hid):
        self.hits = []
        self.cl_id = cl_id
        self.tid = tid
        self.hid = hid
        self.hids = []
        self.hidgs = []
        # self.hid_g = hid_g

    def addHit(self, hit):
        self.ribbon = hit.ribbon
        if (hit.hid not in self.hids): self.hids.append(hit.hid)
        if (hit.hid_g not in self.hidgs): self.hidgs.append(hit.hid_g)
        if (len(self.hits)==0):
            self.side = hit.side 
            self.tid = hit.tid
            self.hid = hit.hid
            self.hid_g = hit.hid_g
        if (self.side != hit.side): self.side = 0
        self.hits.append(hit)
        if (self.tid != hit.tid): self.trackPU = 1
        if (self.hid != hit.hid and self.tid == hit.tid): self.recurlerPU = 1
        if (hit.time_mc < 0): self.prevFramePU = 1

    def meanTime(self):
        times = [hit.time for hit in self.hits]
        return np.mean(times)
        # if (self.side == 0):
        # else:
    def meanTimeMC(self):
        times = [hit.time_mc for hit in self.hits]
        return np.mean(times)
    def minTime(self):
        times = [hit.time for hit in self.hits]
        # times = [hit.tCorr for hit in self.hits]
        return min(times)
    def minTimeCorr(self):
        times_corr = [hit.tCorr for hit in self.hits]
        return min(times_corr)
    def meanTimeCorr(self):
        times_corr = [hit.tCorr for hit in self.hits]
        return np.mean(times_corr)
        



# Get model and params from workspace
f = ROOT.TFile.Open("/unix/muons/users/bgayther/phd_studies/py_scripts/scifi_timing_and_trackPU/model.root")
ws = f.Get("w")
ws.Print()

a_fromFit = ws.var("a")
a_fromFit.setConstant(ROOT.kTRUE)
a_fromFit.Print()

b_fromFit = ws.var("b")
b_fromFit.setConstant(ROOT.kTRUE)
b_fromFit.Print()

loc_fromFit = ws.var("loc")
# loc_fromFit.setConstant(ROOT.kTRUE)
loc_fromFit.Print()

scale_fromFit = ws.var("scale")
scale_fromFit.setConstant(ROOT.kTRUE)
scale_fromFit.Print()

# Y_fromFit = ws.function("Y")
# Y_fromFit.Print()
f.Close()

output_file = ROOT.TFile(args.path + "/" + args.output + ".root", 'RECREATE')

xmax = 64.0*8
xmin = -64.0
x = ROOT.RooRealVar("x", "Hit time",xmin, xmax)
xres = ROOT.RooRealVar("xres", "Hit time resolution",-10, 70)

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

def makeJohnsonPDFtoFit(name, loc_guess, var_list, pdf_list):

    loc_toFit = ROOT.RooRealVar("loc_toFit"+name, "loc_toFit", loc_guess, -1, 65)

    # Y_toFit = ROOT.RooFormulaVar("Y_toFit"+name,"(@0-@1)/@2", ROOT.RooArgList(x,loc_toFit,scale_fromFit))
    # Y2_toFit = ROOT.RooFormulaVar("Y2_toFit"+name,"pow(@0,2.0)", ROOT.RooArgList(Y_toFit))
    # gauss_input_toFit = ROOT.RooFormulaVar("gauss_input_toFit"+name,"@0 + @1 * TMath::Log(@2 + sqrt(@3+1))", ROOT.RooArgList(a_fromFit,b_fromFit,Y_toFit,Y2_toFit))
    # gauss_toFit = ROOT.RooFormulaVar("gauss_toFit"+name,"exp(-0.5*pow(@0,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_toFit))
    # johnsonsu_pdf_toFit = ROOT.RooFormulaVar("johnsonsu_pdf_toFit"+name,"(@0 / sqrt(@1+1))*@2/@3", ROOT.RooArgList(b_fromFit,Y2_toFit,gauss_toFit,scale_fromFit))
    # model_toFit = ROOT.RooGenericPdf("model_toFit"+name,"model_toFit"+name, "@0", ROOT.RooArgList(johnsonsu_pdf_toFit))

    # ROOT.SetOwnership(model_toFit, False)
    # ROOT.SetOwnership(johnsonsu_pdf_toFit, False)
    # ROOT.SetOwnership(gauss_toFit, False)
    # ROOT.SetOwnership(gauss_input_toFit, False)
    # ROOT.SetOwnership(Y2_toFit, False)
    # ROOT.SetOwnership(Y_toFit, False)
    ROOT.SetOwnership(loc_toFit, False)

    model_toFit = ROOT.RooJohnson("model_toFit"+name,"model_toFit"+name, x, loc_toFit, scale_fromFit, a_fromFit, b_fromFit)
    ROOT.SetOwnership(model_toFit, False)

    var_list.add(loc_toFit)
    pdf_list.add(model_toFit)
    # loc_toFit.Print()
    # model_toFit.Print()
    return

def johnsonsu_mean(loc):
    m = loc - scale_fromFit.getValV() * np.exp(0.5 / b_fromFit.getValV()**2) * np.sinh(a_fromFit.getValV()/b_fromFit.getValV())
    return m

ribbons = [i for i in range(12)]

def getHitsOnRibbonAndSide(hits, ribbon, side):
    hitsOnRibbonAndSide = []
    for hit in hits:
        if (hit.side == side and hit.ribbon == ribbon):
            hitsOnRibbonAndSide.append(hit)
    return hitsOnRibbonAndSide

def getHitsOnRibbonAndSideWithinNCols(hits, ncols):
    hitsOnRibbonAndSide = defaultdict(list)

    group_index = 0
    hitsOnRibbonAndSide[group_index].append(hits[0])
    for i in range(len(hits)):
        hit1 = hits[i]
        if i == len(hits)-1: j = i
        else: j = i + 1
        hit2 = hits[j]
        if (hit1==hit2): continue
        if (hit1.ribbon == hit2.ribbon and hit1.side == hit2.side): 
            if (hit2.col - hit1.col <= ncols):
                hitsOnRibbonAndSide[group_index].append(hit2)                      
            else:
                group_index += 1
                hitsOnRibbonAndSide[group_index].append(hit2)

    return hitsOnRibbonAndSide

def getCrossingMap(hits, use_hidg=False):
    hits_tid_hid_map = defaultdict(list)
    for hit in hits:
        if (use_hidg):
            if (hit.tid not in hits_tid_hid_map or hit.hid_g not in hits_tid_hid_map[hit.tid]): hits_tid_hid_map[hit.tid].append(hit.hid_g)
        else:
            if (hit.tid not in hits_tid_hid_map or hit.hid not in hits_tid_hid_map[hit.tid]): hits_tid_hid_map[hit.tid].append(hit.hid)
    return hits_tid_hid_map 

def getExpectedNClusters(hits):
    hits_tid_hid_map = getCrossingMap(hits)
    N = 0
    # for tid,hids in hits_tid_hid_map.iteritems(): 
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids: 
            N+=1
    return N 

def getMeanMCTimeOfCrossings(hits_tid_hid_map, truth_clusters, use_hidg=False):
    hits_tid_hid_to_mtime_map = defaultdict(lambda: defaultdict(float))
    # for tid,hids in hits_tid_hid_map.iteritems(): 
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids:
            for cl in truth_clusters:
                if cl.tid == tid and (cl.hid == hid if not use_hidg else cl.hid_g == hid): #and side check?
                    # hits_tid_hid_to_mtime_map[tid][hid] = cl.meanTime()
                    hits_tid_hid_to_mtime_map[tid][hid] = cl.meanTimeMC()
    # print(hits_tid_hid_map)
    # print(hits_tid_hid_to_mtime_map)
    return hits_tid_hid_to_mtime_map

def getMCTimeOfCrossingsAndRibbon(hits_tid_hid_map, truth_clusters, use_hidg=False):
    hits_ribbon_to_mtime_map = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
    # for tid,hids in hits_tid_hid_map.iteritems(): 
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids:
            for cl in truth_clusters:
                if cl.tid == tid and (cl.hid == hid if not use_hidg else cl.hid_g == hid): #and side check?
                    hits_ribbon_to_mtime_map[tid][hid][cl.ribbon] = cl.meanTimeMC()
    # print(hits_tid_hid_map)
    # print(hits_ribbon_to_mtime_map)
    return hits_ribbon_to_mtime_map

def getEarliestTimeOfCrossings(hits_tid_hid_map, truth_clusters, use_hidg=False):
    hits_tid_hid_to_mtime_map = defaultdict(lambda: defaultdict(float))
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids:
            for cl in truth_clusters:
                if cl.tid == tid and (cl.hid == hid if not use_hidg else cl.hid_g == hid): #and side check?
                    hits_tid_hid_to_mtime_map[tid][hid] = cl.minTimeCorr()
    # print(hits_tid_hid_map)
    # print(hits_tid_hid_to_mtime_map)
    return hits_tid_hid_to_mtime_map

def getMeanTimeOfCrossings(hits_tid_hid_map, truth_clusters, use_hidg=False):
    hits_tid_hid_to_mtime_map = defaultdict(lambda: defaultdict(float))
    # for tid,hids in hits_tid_hid_map.iteritems(): 
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids:
            for cl in truth_clusters:
                if cl.tid == tid and (cl.hid == hid if not use_hidg else cl.hid_g == hid): #and side check?
                    hits_tid_hid_to_mtime_map[tid][hid] = cl.meanTimeCorr()
    # print(hits_tid_hid_map)
    # print(hits_tid_hid_to_mtime_map)
    return hits_tid_hid_to_mtime_map


h_ncls_resid_BIC = ROOT.TH1F("h_ncls_resid_BIC", "BIC Residual;Numb of Clusters expected - predicted;", 9, -4.5, 4.5)
h_ncls_resid_AIC = ROOT.TH1F("h_ncls_resid_AIC", "AIC;Numb of Clusters expected - predicted;", 10, -5, 5)
h_closest_hit_to_pdf_peak_res = ROOT.TH1F("h_closest_hit_to_pdf_peak_res", ";t_{peak} - t_{closest_hit};", 100, -5, 5)
h_earliest_hit_to_pdf_peak_res = ROOT.TH1F("h_earliest_hit_to_pdf_peak_res", ";t_{peak} - t_{earliest_hit};", 100, -5, 5)

min_x = -3.0
max_x = 3.0
Nbins = 120

h_peak_pdf_res = ROOT.TH1D("h_peak_pdf_res", "Res;t_{peak} - t_{mc} (Z-corrected) [ns]", Nbins, min_x+0.5, max_x+0.5)
h_loc_pdf_res = ROOT.TH1D("h_loc_pdf_res", "Res;t_{loc} - t_{mc} (Z-corrected) [ns]", Nbins, min_x, max_x)
h_earliest_hit_res = ROOT.TH1D("h_earliest_hit_res", "Res;t_{earliest} - t_{mc} (Z-corrected) [ns]", Nbins, min_x, max_x)
h_earliest_hit_recurl2_res = ROOT.TH1D("h_earliest_hit_recurl2_res", "Res;t_{earliest, recurl 2} - t_{mc} (Z-corrected) [ns]", Nbins, min_x, max_x)

h_earliest_recurl_diff = ROOT.TH1D("h_earliest_recurl_diff", ";t_{min, recurl 2} - t_{min, recurl 1} (Z-corrected) [ns]", 100, -10, 10)
h_earliest_recurl_diff_true_tof_diff = ROOT.TH1D("h_earliest_recurl_diff_true_tof_diff", ";(t_{min, recurl 2} - t_{min, recurl 1} (Z-corrected)) - true TOF [ns]", 100, -10, 10)
h_earliest_recurl_diff_vs_true_tof = ROOT.TH2D("h_earliest_recurl_diff_vs_true_tof", ";TOF [ns];(t_{min, recurl 2} - t_{min, recurl 1} (Z-corrected)) [ns]", 100, 0.0, 5.0, 100, -0.5, 4.5)

h_mean_recurl_diff = ROOT.TH1D("h_mean_recurl_diff", ";t_{mean, recurl 2} - t_{mean, recurl 1} (Z-corrected) [ns]", 100, -10, 10)
h_mean_recurl_diff_true_tof_diff = ROOT.TH1D("h_mean_recurl_diff_true_tof_diff", ";(t_{mean, recurl 2} - t_{mean, recurl 1} (Z-corrected)) - true TOF [ns]", 100, -10, 10)
h_mean_recurl_diff_vs_true_tof = ROOT.TH2D("h_mean_recurl_diff_vs_true_tof", ";TOF [ns];(t_{mean, recurl 2} - t_{mean, recurl 1} (Z-corrected)) [ns]", 100, 0.0, 5.0, 100, -0.5, 4.5)

h_recurl1_vs_recurl2_loc_res = ROOT.TH2D("h_recurl1_vs_recurl2_loc_res", "Res;Recurl 1, t_{loc} - t_{mc} (Z-corrected) [ns];Recurl 2, t_{loc} - t_{mc} (Z-corrected) [ns]", Nbins, min_x, max_x, Nbins, min_x, max_x)

h_loc_diff_vs_tof = ROOT.TH2D("h_loc_diff_vs_tof", ";TOF [ns]; t_{loc, recurl 2} - t_{loc, recurl 1} (Z-corrected) [ns]", 100, 0.0, 5.0, 100, -0.5, 4.5)
h_loc_diff_minus_tof = ROOT.TH1D("h_loc_diff_minus_tof", ";(t_{loc, recurl 2} - t_{loc, recurl 1} (Z-corrected)) - true TOF [ns]", 100, -10, 10)
h_loc_diff_minus_tof_high_res = ROOT.TH1D("h_loc_diff_minus_tof_high_res", ";(t_{loc, recurl 2} - t_{loc, recurl 1} (Z-corrected)) - true TOF [ns]", 100, -0.5, 0.5)
# h_peak_pdf_resVsN = ROOT.TH2D("h_peak_pdf_resVsN", "ResVsN;t_{peak} - t_{mc} (Z-corrected) [ns];Nhits", Nbins, min_x+0.5, max_x+0.5, 9, 1, 10)
# h_loc_pdf_resVsN = ROOT.TH2D("h_loc_pdf_resVsN", "ResVsN;t_{loc} - t_{mc} (Z-corrected) [ns];Nhits", Nbins, min_x, max_x, 9, 1, 10)
# h_earliest_hit_resVsN = ROOT.TH2D("h_earliest_hit_resVsN", "ResVsN;t_{earliest} - t_{mc} (Z-corrected) [ns];Nhits", Nbins, min_x, max_x, 9, 1, 10)

h_peak_pdf_resVsN = ROOT.TH2D("h_peak_pdf_resVsN", "ResVsN;Nhits;t_{peak} - t_{mc} (Z-corrected) [ns]", 9, 1, 10, Nbins, min_x+0.5, max_x+0.5)
h_loc_pdf_resVsN = ROOT.TH2D("h_loc_pdf_resVsN", "ResVsN;Nhits;t_{loc} - t_{mc} (Z-corrected) [ns]", 9, 1, 10, Nbins, min_x, max_x)
h_earliest_hit_resVsN = ROOT.TH2D("h_earliest_hit_resVsN", "ResVsN;Nhits;t_{earliest} - t_{mc} (Z-corrected) [ns]", 9, 1, 10, Nbins, min_x, max_x)

h_nhits_cl = ROOT.TH1F("h_nhits_cl", ";Number of hits in cluster;Nhits", 14, 1, 15)
h_nhits_cl_side = ROOT.TH1F("h_nhits_cl_side", ";Number of hits in cluster (per side);Nhits", 9, 1, 10)

eff_fit_TOF = ROOT.TEfficiency("eff_fit_TOF", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF = ROOT.TEfficiency("eff_std_TOF", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_TOF_2recurls = ROOT.TEfficiency("eff_fit_TOF_2recurls", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF_2recurls = ROOT.TEfficiency("eff_std_TOF_2recurls", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_TOF_3recurls = ROOT.TEfficiency("eff_fit_TOF_3recurls", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF_3recurls = ROOT.TEfficiency("eff_std_TOF_3recurls", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_TOF_4recurls = ROOT.TEfficiency("eff_fit_TOF_4recurls", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF_4recurls = ROOT.TEfficiency("eff_std_TOF_4recurls", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_TOF_5recurls = ROOT.TEfficiency("eff_fit_TOF_5recurls", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF_5recurls = ROOT.TEfficiency("eff_std_TOF_5recurls", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_TOF_6recurls = ROOT.TEfficiency("eff_fit_TOF_6recurls", ";MC TOF between ribbons [ns]; fit efficiency ", 50, 0.0, 3.5)
eff_std_TOF_6recurls = ROOT.TEfficiency("eff_std_TOF_6recurls", ";MC TOF between ribbons [ns]; std efficiency ", 50, 0.0, 3.5)

eff_fit_t2_t1 = ROOT.TEfficiency("eff_fit_t2_t1", ";Observed T2 - T1 (min, Z corrected) [ns]; fit efficiency ", 50, -5.0, 5.0)
eff_std_t2_t1 = ROOT.TEfficiency("eff_std_t2_t1", ";Observed T2 - T1 (min, Z corrected) [ns]; std efficiency ", 50, -5.0, 5.0)
eff_fit_neg_obs_times = ROOT.TEfficiency("eff_fit_neg_obs_times", ";Observed T2 - T1 (min, Z corrected) [ns]; fit efficiency ", 50, -5.0, 0.0)

eff_fit_dt_vs_ds = ROOT.TEfficiency("eff_fit_dt_vs_ds", ";ds [ns];Observed T2 - T1 (min, Z corrected) [ns]; fit efficiency", 100, 0.0, 1000.0, 100, -5, 5)
eff_std_dt_vs_ds = ROOT.TEfficiency("eff_std_dt_vs_ds", ";ds [ns];Observed T2 - T1 (min, Z corrected) [ns]; std efficiency", 100, 0.0, 1000.0, 100, -5, 5)

eff_fit_recurls = ROOT.TEfficiency("eff_fit_recurls", ";Number of recurls; fit efficiency ", 15, 0.5, 15.5)
eff_std_recurls = ROOT.TEfficiency("eff_std_recurls", ";Number of recurls; std efficiency ", 15, 0.5, 15.5)

eff_fit_recurls_TOF_under_1ns = ROOT.TEfficiency("eff_fit_recurls_TOF_under_1ns", "For MC TOF < 1 ns;Number of recurls; fit efficiency ", 15, 0.5, 15.5)
eff_std_recurls_TOF_under_1ns = ROOT.TEfficiency("eff_std_recurls_TOF_under_1ns", "For MC TOF < 1 ns;Number of recurls; std efficiency ", 15, 0.5, 15.5)

# recurl1_dataset = ROOT.RooDataSet("recurl1_dataset","recurl1_dataset",ROOT.RooArgSet(xres))
# recurl2_dataset = ROOT.RooDataSet("recurl2_dataset","recurl2_dataset",ROOT.RooArgSet(xres))
# recurl3_dataset = ROOT.RooDataSet("recurl3_dataset","recurl3_dataset",ROOT.RooArgSet(xres))
# recurl4_dataset = ROOT.RooDataSet("recurl4_dataset","recurl4_dataset",ROOT.RooArgSet(xres))
# clean_cls_ds = ROOT.RooDataSet("clean_cls_ds","clean_cls_ds",ROOT.RooArgSet(xres))

gr_nll_diff_vs_TOF = ROOT.TGraph()
gr_nll_diff_vs_TOF.SetTitle(";TOF [ns];NLL diff of CW-CCW model")
gr_nll_diff_vs_TOF.SetName("gr_nll_diff_vs_TOF")

gr_nll_diff_under_TOF_1ns_vs_recurls = ROOT.TGraph()
gr_nll_diff_under_TOF_1ns_vs_recurls.SetTitle(";Number of recurls;NLL diff of CW-CCW model")
gr_nll_diff_under_TOF_1ns_vs_recurls.SetName("gr_nll_diff_under_TOF_1ns_vs_recurls")

gr_nll_diff_vs_recurls = ROOT.TGraph()
gr_nll_diff_vs_recurls.SetTitle(";Number of recurls;NLL diff of CW-CCW model")
gr_nll_diff_vs_recurls.SetName("gr_nll_diff_vs_recurls")

skip = args.skip

fracs = ROOT.RooArgList()
fracs_CW = ROOT.RooArgList()
fracs_CCW = ROOT.RooArgList()

variables = ROOT.RooArgList()
variables_CW = ROOT.RooArgList()
variables_CCW = ROOT.RooArgList()

pdfs = ROOT.RooArgList()
pdfs_CW = ROOT.RooArgList()
pdfs_CCW = ROOT.RooArgList()

constraintPdfs = ROOT.RooArgList()
constraintFuncs = ROOT.RooArgList()

start = time_py.process_time()

fits_failed = 0
fits_attempted = 0
res_widths = [1.0, 0.5, 0.16, 0.08, 0.04]

fit_correct_ID = 0
fit_incorrect_ID = 0
dt_correct_ID = 0
dt_incorrect_ID = 0
fit_correct_ID_neg_time = 0
fit_incorrect_ID_neg_time = 0
fit_correct_small_tof = 0
fit_incorrect_small_tof = 0
std_correct_small_tof = 0
std_incorrect_small_tof = 0


dataset = ROOT.RooDataSet("ds","ds",ROOT.RooArgSet(x))

RIBBON_TIME_OFFSET = 64.0

for i in range(skip, nevents+skip):
    mppcHitsTree.GetEntry(i)
    if i%100 == 0:
        print(("(%6i/%i) %5.1f%% \r" % (i, nevents, 100.*i/nevents)))
        gc.collect()
        sys.stdout.flush()

    nh = mppcHitsTree.nh
    # print("### NEW Frame {} ###".format(i))

    tid_hid_map = defaultdict(list)
    tid_hidg_map = defaultdict(list)
    reco_cl_ids = []
    all_hits = []

    for j in range(nh):
        # Truth info
        crosstalk = mppcHitsTree.crosstalk[j]
        time_mc = mppcHitsTree.tmc[j]
        tid = mppcHitsTree.tid[j]
        hid = mppcHitsTree.hid[j]
        hid_g = mppcHitsTree.hid_g[j]

        # Non truth info
        time = mppcHitsTree.t[j]
        r = mppcHitsTree.r[j]
        phi = mppcHitsTree.phi[j]
        col = mppcHitsTree.col[j]
        amp = mppcHitsTree.amp[j]
        ribbon = mppcHitsTree.ribbon[j]
        side = mppcHitsTree.side[j]
        reco_cl_id = mppcHitsTree.cl_id[j]
        zPos = mppcHitsTree.zPos[j]

        if (reco_cl_id not in reco_cl_ids): reco_cl_ids.append(reco_cl_id)
        # print("TID {}, HID {}".format(tid,hid))
        # append hid_g?
        if (tid not in tid_hid_map or hid not in tid_hid_map[tid]): tid_hid_map[tid].append(hid)
        if (tid not in tid_hidg_map or hid_g not in tid_hidg_map[tid]): tid_hidg_map[tid].append(hid_g)

        all_hits.append(mppcHit(ribbon, col, side, amp, time, time_mc, crosstalk, tid, hid, hid_g, reco_cl_id, zPos))

    for tid, hids in tid_hid_map.items():
        hids.sort()
    for tid, hids in tid_hidg_map.items():
        hids.sort()
    # print(tid_hid_map)
    # print(tid_hidg_map)
    reco_cl_ids = list(set(reco_cl_ids))

    # Count number of track/recurler PU instead?
    trackPU_map = [[0,0] for i in range(12)]
    recurlerPU_map = [[0,0] for i in range(12)]

    # For non overlapping cases (i.e. no consecutive columns)
    trackNonPU_map = [[0,0] for i in range(12)]
    recurlerNonPU_map = [[0,0] for i in range(12)]

    # track_to_hid_to_rib_to_side_map = [[defaultdict(list),defaultdict(list)] for i in range(12)]
    # for hit in all_hits:
    #     dct = track_to_hid_to_rib_to_side_map[hit.ribbon][hit.side]
    #     if (hit.tid not in track_to_hid_to_rib_to_side_map[hit.ribbon][hit.side] or hit.hid not in dct[hit.tid]): dct[hit.tid].append(hit.hid)
    # print(track_to_hid_to_rib_to_side_map)

    trackPU_tid_map = [[[],[]] for i in range(12)]
    recurlerPU_tid_map = [[[],[]] for i in range(12)]
    recurlerPU_hid_map = [[[],[]] for i in range(12)]
    recurlerPU_hidg_map = [[[],[]] for i in range(12)]
    for hit1 in all_hits:
        for hit2 in all_hits:
            if (hit1==hit2): continue
            if (hit1.ribbon == hit2.ribbon and hit1.side == hit2.side): 
                side = 0 if hit1.side == 0 else 1
                
                if (hit2.col - hit1.col == 1):
                    if (hit2.tid != hit1.tid): 
                        trackPU_map[hit1.ribbon][side] = 1 
                        dct = trackPU_tid_map[hit1.ribbon][hit1.side]
                        if (hit1.tid not in trackPU_tid_map[hit1.ribbon][side]): trackPU_tid_map[hit1.ribbon][side].append(hit1.tid)
                        if (hit2.tid not in trackPU_tid_map[hit2.ribbon][side]): trackPU_tid_map[hit2.ribbon][side].append(hit2.tid)
                            
                    if (hit2.tid == hit1.tid and hit2.hid != hit1.hid): 
                        recurlerPU_map[hit1.ribbon][side] = 1 
                        if (hit1.tid not in recurlerPU_tid_map[hit1.ribbon][side]): recurlerPU_tid_map[hit1.ribbon][side].append(hit1.tid)
                        if (hit2.tid not in recurlerPU_tid_map[hit2.ribbon][side]): recurlerPU_tid_map[hit2.ribbon][side].append(hit2.tid)
                        if (hit1.hid not in recurlerPU_hid_map[hit1.ribbon][side]): recurlerPU_hid_map[hit1.ribbon][side].append(hit1.hid)
                        if (hit2.hid not in recurlerPU_hid_map[hit2.ribbon][side]): recurlerPU_hid_map[hit2.ribbon][side].append(hit2.hid)
                        if (hit1.hid_g not in recurlerPU_hidg_map[hit1.ribbon][side]): recurlerPU_hidg_map[hit1.ribbon][side].append(hit1.hid_g)
                        if (hit2.hid_g not in recurlerPU_hidg_map[hit2.ribbon][side]): recurlerPU_hidg_map[hit2.ribbon][side].append(hit2.hid_g)
                else:
                    if (hit2.tid != hit1.tid): trackNonPU_map[hit1.ribbon][side] = 1 
                    if (hit2.tid == hit1.tid and hit2.hid != hit1.hid): recurlerNonPU_map[hit1.ribbon][side] = 1 

    # print("PU maps")
    # print(trackPU_map)
    # print(recurlerPU_map)

    # print ("Tids in track PU per ribbon, per side")
    # print (trackPU_tid_map)

    # print("PU with no overlapping columns maps")
    # print(trackNonPU_map)
    # print(recurlerNonPU_map)

    # reco_cls = []
    # for cl_id in reco_cl_ids:
    #     cl = fbCluster(cl_id, None, None)
    #     for hit in all_hits:
    #         if (hit.reco_cl_id == cl_id): cl.addHit(hit)
    #     reco_cls.append(cl)
    
    truth_cls = []
    truth_cls_L = []
    truth_cls_R = []
    # for tid, hids in tid_hid_map.iteritems():
    for tid, hids in tid_hid_map.items():
        for hid in hids:
            cl = fbCluster(None, tid, hid)
            cl_L = fbCluster(None, tid, hid)
            cl_R = fbCluster(None, tid, hid)
            cl.tid = tid
            cl.hid = hid
            cl_L.tid = tid
            cl_L.hid = hid
            cl_R.tid = tid
            cl_R.hid = hid
            for hit in all_hits:
                if (tid == hit.tid and hid == hit.hid): 
                    cl.addHit(hit)
                    if (hit.side == 0): cl_L.addHit(hit)
                    if (hit.side == 1): cl_R.addHit(hit)

            if (len(cl.hits) >= 1) :truth_cls.append(cl)
            if (len(cl_L.hits) >= 1) :truth_cls_L.append(cl_L)
            if (len(cl_R.hits) >= 1) :truth_cls_R.append(cl_R)


    # Fit to truth clusters for one cluster each
    # print(len(truth_cls_L), len(truth_cls_R))
    # for cl in truth_cls_L:
    #     tmp_rds = ROOT.RooDataSet("tmp_rds","tmp_rds",ROOT.RooArgSet(x))
    #     for hit in cl.hits:
    #         x.setVal(hit.time)
    #         tmp_rds.add(ROOT.RooArgSet(x))
    #     # Create RooJohnson
    #     tmp_loc = ROOT.RooRealVar("tmp_loc", "", cl.meanTime(), -1, 65)
    #     tmp_scale = ROOT.RooRealVar("tmp_scale", "", 1, 0, 10)
    #     tmp_model = ROOT.RooJohnson("tmp_model","", x, tmp_loc, tmp_scale, a_fromFit, b_fromFit)
    #     x.setRange("tmpRange", cl.meanTime()-10, cl.meanTime()+10)
    #     tmp_model.fitTo(tmp_rds, ROOT.RooFit.Range("tmpRange"))

    #     # Save pdf of fit and data
    #     frame = x.frame(ROOT.RooFit.Range("tmpRange"))
    #     frame.SetTitle("")
    #     frame.SetXTitle("Hit time in frame [ns]")
    #     tmp_rds.plotOn(frame, ROOT.RooFit.DataError(0))
    #     tmp_model.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.NormRange("tmpRange"), ROOT.RooFit.Range("tmpRange"))
    #     tmp_can = ROOT.TCanvas()
    #     frame.SetMinimum(1e-5)
    #     frame.Draw("E0")
    #     tmp_can.SaveAs(args.path+f"/fit_to_reco_cl_{cl.tid}.pdf")

    # Fit to multiple clusters
    # tmp_rds = ROOT.RooDataSet("tmp_rds","tmp_rds",ROOT.RooArgSet(x))
    # for cl in truth_cls_L[0:2]:
    #     for hit in cl.hits:
    #         x.setVal(hit.time)
    #         tmp_rds.add(ROOT.RooArgSet(x))

    # fracs.removeAll()
    # variables.removeAll()
    # pdfs.removeAll()
    # for j in range(2):
    #     print(j)
    #     loc_guess = random.uniform(0, 64)
    #     frac = ROOT.RooRealVar("frac"+str(j), "frac"+str(j), 0.2, 0.0001, 1.0)
    #     ROOT.SetOwnership(frac, False)
    #     fracs.add(frac)
    #     makeJohnsonPDFtoFit(str(j), loc_guess, variables, pdfs)

    # x.setRange("tmpRange", tmp_rds.mean(x) - 30, tmp_rds.mean(x) + 30)

    # tmp_model = ROOT.RooAddPdf("joint_model", "", pdfs, fracs)
    # tmp_model.fitTo(tmp_rds, ROOT.RooFit.Range("tmpRange"))

    # frame = x.frame(ROOT.RooFit.Range("tmpRange"))
    # frame.SetTitle("")
    # frame.SetXTitle("Hit time in frame [ns]")
    # tmp_rds.plotOn(frame, ROOT.RooFit.DataError(0))
    # tmp_model.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.NormRange("tmpRange"), ROOT.RooFit.Range("tmpRange"))
    # tmp_can = ROOT.TCanvas()
    # frame.SetMinimum(1e-5)
    # frame.Draw("E0")
    # tmp_can.SaveAs(args.path+f"/multi_fit_to_reco_cl_frame_{i}.pdf")


    # # CHARGE IDENTIFICATION

    # # print (tid_hidg_map)
    # # NEED TO THINK ABT DIFF HIDS WITH SAME HIDGS....
    # for tid, hidgs in tid_hidg_map.items():
    #     # if (len(hidgs) == 2 and 0 not in hidgs and len(tid_hid_map[tid])==2):
    #     if (len(hidgs) == len(tid_hid_map[tid]) and len(hidgs) >= 2 and 0 not in hidgs):
    #         # dataset_R1 = ROOT.RooDataSet("dataset_R1","dataset_R1",ROOT.RooArgSet(x))
    #         # dataset_R2 = ROOT.RooDataSet("dataset_R2","dataset_R2",ROOT.RooArgSet(x))
    #         if (hidgs[0]!=2 and hidgs[0]!=3): 
    #             # print (hidgs)
    #             continue
    #         hits_in_ds = []
    #         # ribbon1 = -1
    #         # ribbon2 = -1
    #         # ribbons_to_offset = []
    #         nhits_per_crossing = []
    #         ribbons_in_ds = []
    #         flag = False

    #         for cl in truth_cls:
    #             if (cl.tid == tid):
    #                 nhits_per_crossing.append(len(cl.hits))
    #                 if len(cl.hidgs)>1: flag=True
    #                 for hit in cl.hits:
    #                     if hit.time_mc<0: flag=True
    #                     hits_in_ds.append(hit)
    #                     if (hit.ribbon not in ribbons_in_ds): ribbons_in_ds.append(hit.ribbon)
            
    #         if flag: continue
    #         dataset.reset()

    #         # ribbons_to_offset = [i for i in ribbons_in_ds]
    #         # if (len(ribbons_in_ds)==2):
    #             # for hit in hits_in_ds:
    #                 # if (ribbon1 != -1 and ribbon2 != -1): break
    #                 # if (hit.hid_g == hidgs[0]):  
    #                 #     ribbon1 = cl.ribbon
    #                 # if (hit.hid_g == hidgs[1]):  
    #                 #     ribbon2 = cl.ribbon
    #         # if (len(ribbons_in_ds)>2):
    #         # for hit in hits_in_ds: hit.printHit()
    #         #     print(ribbons_in_ds)
    #         #     print(ribbons_to_offset)
    #         for hit in hits_in_ds:
    #             # hit.printHit()
    #             idx = ribbons_in_ds.index(hit.ribbon)
    #             # print("Offset time is {}".format(hit.tCorr + idx*RIBBON_TIME_OFFSET))
    #             x.setVal(hit.tCorr + idx*RIBBON_TIME_OFFSET)
    #             # if (hit.ribbon in ribbons_to_offset):
    #             # else:
    #             #     x.setVal(hit.tCorr)
    #             dataset.add(ROOT.RooArgSet(x))


    #         # for cl in truth_cls:
    #         #     if (cl.tid == tid):
    #         #         if (cl.hid_g == hidgs[0]):  
    #         #             ribbon1 = cl.ribbon
    #         #         if (cl.hid_g == hidgs[1]):  
    #         #             ribbon2 = cl.ribbon
    #         #         nhits_per_crossing.append(len(cl.hits))
    #         #         for hit in cl.hits:
    #         #             # hits_in_ds.append(hit)
    #         #             if (cl.ribbon == ribbon2):
    #         #                 x.setVal(hit.tCorr+RIBBON_TIME_OFFSET)
    #         #             else:
    #         #                 x.setVal(hit.tCorr)
    #         #             dataset.add(ROOT.RooArgSet(x))
    #                     # if (cl.ribbon == ribbon1):
    #                     #     dataset_R1.add(ROOT.RooArgSet(x))
    #                     # if (cl.ribbon == ribbon2):
    #                     #     dataset_R2.add(ROOT.RooArgSet(x))
    #         # print("Ribbon 1 (T1) - {}, Ribbon 2 (T2) - {}".format(ribbon1,ribbon2))
    #         hits_tid_hid_map = getCrossingMap(hits_in_ds, True)
    #         meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hits_tid_hid_map, truth_cls, True)
    #         meanTimePerCrossing = getMeanTimeOfCrossings(hits_tid_hid_map, truth_cls, True)
    #         minTimePerCrossing = getEarliestTimeOfCrossings(hits_tid_hid_map, truth_cls, True)
    #         minTimePerCrossingAndRibbon = getMCTimeOfCrossingsAndRibbon(hits_tid_hid_map, truth_cls, True)

    #         fracs_CW.removeAll()
    #         pdfs_CW.removeAll()
    #         fracs_CCW.removeAll()
    #         pdfs_CCW.removeAll()

    #         # for tid, map_of_maps in minTimePerCrossingAndRibbon.items():
    #         #     # print(tid, map_of_maps)
    #         #     for hidg, ribbon_to_mtime in map_of_maps.items():
    #         #         # print(hidg,ribbon_to_mtime)
    #         #         for ribbon, mc_time in ribbon_to_mtime.items():
    #         #             print (ribbon, mc_time)

    #         # for idx, (hidg, ribbon_to_mtime) in enumerate(minTimePerCrossingAndRibbon[tid].items()):
    #         #     print(idx, hidg)
    #         #     for ribbon, mc_time in ribbon_to_mtime.items():
    #         #         print (ribbon, mc_time)


    #         # print(minTimePerCrossingAndRibbon)

    #         # print ("MC time per crossing ", meanMCTimePerCrossing)
    #         # print ("Mean time (Z corr) per crossing ", meanTimePerCrossing)
    #         # tof_T2_T1 = (meanMCTimePerCrossing[tid][hidsOnSide[1]] - meanMCTimePerCrossing[tid][hidsOnSide[0]])
    #         # for tid, mt_to_hids in meanMCTimePerCrossing.items():
    #         #     print (tid, mt_to_hids)
    #         #     for mt, hid in mt_to_hids.items():
    #         #         print (mt, hid)
    #         meanMCTimes = []
    #         for hidg in hidgs:
    #             meanMCTimes.append(meanMCTimePerCrossing[tid][hidg])
    #             # print (hidg, meanMCTimePerCrossing[tid][hidg])
    #         meanMCTimesDiff = [x - meanMCTimes[i - 1] for i, x in enumerate(meanMCTimes)][1:]
    #         # print (meanMCTimes, meanMCTimesDiff)

    #         # fracs_CW.removeAll()
    #         variables_CW.removeAll()
    #         # pdfs_CW.removeAll()
    #         # constraintPdfs.removeAll()
    #         # constraintFuncs.removeAll()

    #         # print (nhits_per_crossing)
    #         # for i, tof in enumerate(meanMCTimesDiff): 
    #             # print ("TOF between T{} and T{} is {}".format(i+1, i+2, tof))
    #             # print ("TOF between T{} and T{} is {}".format(i, i+1, tof))

    #         for i in range(len(hidgs)): #extended version 
    #             # print ("Initial frac (extended) ", nhits_per_crossing[i])
    #             frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i], 1.0, nhits_per_crossing[i])
    #             ROOT.SetOwnership(frac, False)

    #             # frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i])
    #             fracs_CW.add(frac)
    #         # for i in range(len(hidgs)-1): #unextended version 
    #         #     frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), 1/len(hidgs), 0.0, 1.0)
    #         #     ROOT.SetOwnership(frac, False)
    #         #     fracs.add(frac)
    #         # for i, hidg in enumerate(hidgs):
    #         #     loc_guess = meanTimePerCrossing[tid][hidg]
    #         #     # print("loc_guess {}".format(loc_guess))
    #         #     makeJohnsonPDFtoFit(str(i), loc_guess, variables, pdfs)

    #         # loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", minTimePerCrossing[tid][hidgs[0]], -1, minTimePerCrossing[tid][hidgs[1]])
    #         loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", minTimePerCrossing[tid][hidgs[0]], 0, 64)
    #         # loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", meanTimePerCrossing[tid][hidgs[0]], meanTimePerCrossing[tid][hidgs[0]]-2, meanTimePerCrossing[tid][hidgs[0]]+2)
    #         ROOT.SetOwnership(loc_toFit0, False)

    #         model_toFit0 = ROOT.RooJohnson("model_toFit0","model_toFit0", x, loc_toFit0, scale_fromFit, a_fromFit, b_fromFit)
    #         ROOT.SetOwnership(model_toFit0, False)

    #         variables_CW.add(loc_toFit0)
    #         pdfs_CW.add(model_toFit0)
            

    #         # for i, tof in enumerate(meanMCTimesDiff): 
    #         #     name = str(i+1)
    #         #     tof_to_0 = np.sum(meanMCTimesDiff[0:i+1])
    #         #     if (i==0 or i%2==0): tof_to_0 += RIBBON_TIME_OFFSET
    #         #     print("Sum from T0 to T{} is {}".format(name, tof_to_0))

    #         # print(hidgs)
    #         for i, (hidg, ribbon_to_mtime) in enumerate(minTimePerCrossingAndRibbon[tid].items()):
    #             # print(i,hidg, len(ribbon_to_mtime))
    #             if (i==0): continue
    #             for ribbon, mc_time in ribbon_to_mtime.items():
    #                 ribb_idx = ribbons_in_ds.index(ribbon)
    #                 name = str(i)
    #                 tof_to_0 = np.sum(meanMCTimesDiff[0:i])
    #                 # if (i==0 or i%2==0): tof_to_0 += RIBBON_TIME_OFFSET
    #                 tof_to_0 += ribb_idx*RIBBON_TIME_OFFSET
    #                 # print("CW: Sum from T0 to T{} is {}, tof {}, ribbon_index {}, ribbon {}".format(name, tof_to_0, np.sum(meanMCTimesDiff[0:i]), ribb_idx, ribbon))

    #                 tof_rv = ROOT.RooRealVar("tof0"+name, "tof0"+name, tof_to_0)
    #                 tof_rv.setConstant(True)
    #                 ROOT.SetOwnership(tof_rv, False)

    #                 loc_toFit = ROOT.RooFormulaVar("loc_toFit"+name, "(@0+@1)", ROOT.RooArgList(loc_toFit0, tof_rv))
    #                 ROOT.SetOwnership(loc_toFit, False)

    #                 model_toFit = ROOT.RooJohnson("model_toFit"+name,"model_toFit"+name, x, loc_toFit, scale_fromFit, a_fromFit, b_fromFit)
    #                 ROOT.SetOwnership(model_toFit, False)

    #                 variables_CW.add(loc_toFit)
    #                 # constraintFuncs.add(tof_rv)
    #                 pdfs_CW.add(model_toFit)

    #         # print("Ncoefs {}, Npdfs {}".format(fracs_CW.getSize(),pdfs_CW.getSize()))
    #         joint_model_CW = ROOT.RooAddPdf("joint_model_CW","joint_model_CW", pdfs_CW, fracs_CW)
            


    #         # nll_CW = joint_model_CW.createNLL(dataset)
    #         # minimizer_CW = ROOT.RooMinimizer(nll_CW)
    #         # minimizer_CW.setEps(1e-8)
    #         # minimizer_CW.migrad()
    #         # nll_CW = nll_CW.getValV()

    #         # rf_CW_result = minimizer_CW.save()
    #         rf_CW_result = joint_model_CW.fitTo(dataset, ROOT.RooFit.Save(True), ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.PrintEvalErrors(-1), ROOT.RooFit.Warnings(False))

    #         # fits_attempted += 1
    #         if (rf_CW_result.status() != 0): 
    #         #     fits_failed += 1
    #             print("CW FIT FAILED")
    #             continue

    #         nll_CW = rf_CW_result.minNll()

    #         # fracs_CCW.removeAll()
    #         variables_CCW.removeAll()
    #         # pdfs_CCW.removeAll()
    #         # constraintPdfs.removeAll()
    #         # constraintFuncs.removeAll()

    #         for i in range(len(hidgs)): #extended version 
    #             # print ("Initial frac (extended) ", nhits_per_crossing[i])
    #             # if (i==0): 
    #             #     frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i+1], 1.0, nhits_per_crossing[i+1])
    #             #     ROOT.SetOwnership(frac, False)
    #             # elif (i==1): 
    #             #     frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i-1], 1.0, nhits_per_crossing[i-1])
    #             #     ROOT.SetOwnership(frac, False)
    #             # else:
    #             #     frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i], 1.0, nhits_per_crossing[i])
    #             #     ROOT.SetOwnership(frac, False)
    #             if (i%2==0 and i+1<len(hidgs)): 
    #                 # print(i,i%2,i+1,len(hidgs))
    #                 frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i+1], 1.0, nhits_per_crossing[i+1])
    #                 ROOT.SetOwnership(frac, False)
    #             else:
    #                 frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), nhits_per_crossing[i-1], 1.0, nhits_per_crossing[i-1])
    #                 ROOT.SetOwnership(frac, False)

    #             fracs_CCW.add(frac)

    #         # for i, hidg in enumerate(hidgs):
    #         # for i, hidg in enumerate(reversed(hidgs)):
    #         #     loc_guess = meanTimePerCrossing[tid][hidg]
    #         #     # print("loc_guess {}".format(loc_guess))
    #         #     makeJohnsonPDFtoFit(str(i), loc_guess, variables, pdfs)

    #         loc_toFit0 = ROOT.RooRealVar("loc_toFit1", "loc_toFit1", meanTimePerCrossing[tid][hidgs[1]]+RIBBON_TIME_OFFSET, RIBBON_TIME_OFFSET, 2*RIBBON_TIME_OFFSET)
    #         # loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", meanTimePerCrossing[tid][hidgs[0]], meanTimePerCrossing[tid][hidgs[0]]-2, meanTimePerCrossing[tid][hidgs[0]]+2)
    #         # loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", minTimePerCrossing[tid][hidgs[0]], -1, minTimePerCrossing[tid][hidgs[1]])
    #         # loc_toFit0 = ROOT.RooRealVar("loc_toFit0", "loc_toFit0", minTimePerCrossing[tid][hidgs[1]], -1, minTimePerCrossing[tid][hidgs[0]])
    #         ROOT.SetOwnership(loc_toFit0, False)
    #         # loc_toFit0.Print()

    #         model_toFit0 = ROOT.RooJohnson("model_toFit1","model_toFit1", x, loc_toFit0, scale_fromFit, a_fromFit, b_fromFit)
    #         ROOT.SetOwnership(model_toFit0, False)

    #         variables_CCW.add(loc_toFit0)
    #         pdfs_CCW.add(model_toFit0)

    #         # for i, tof in enumerate(meanMCTimesDiff):
    #             # tof_to_0 = np.sum(meanMCTimesDiff[0:i])
    #             # if (i==0 or i%2==0): tof_to_0 -= RIBBON_TIME_OFFSET

    #         for i, (hidg, ribbon_to_mtime) in enumerate(minTimePerCrossingAndRibbon[tid].items()):
    #             if (i==1): continue # already initialised time on this ribbon as first time...
    #             # if (i==0): continue
    #             for ribbon, mc_time in ribbon_to_mtime.items(): # will just loop once...
    #                 ribb_idx = ribbons_in_ds.index(ribbon)
    #                 name = str(i) 
    #                 # tof_to_0 = abs(meanMCTimePerCrossing[tid][hidg] - meanMCTimePerCrossing[tid][hidgs[0]]) if i>1 else meanMCTimesDiff[0]
    #                 tof_to_0 = np.sum(meanMCTimesDiff[0:i+1]) if i%2==0 else np.sum(meanMCTimesDiff[0:i-1])
    #                 # tof_to_0 = np.sum(meanMCTimesDiff[0:i])+meanMCTimesDiff[0] if i>1 else meanMCTimesDiff[0]
    #                 tof_to_0_pre = tof_to_0
    #                 if (ribb_idx != 1): tof_to_0 += (ribb_idx-1)*RIBBON_TIME_OFFSET
    #                 # print("CCW: Sum from T1 to T{} is {}, tof {}, ribbon_index {}, ribbon {}".format(name, tof_to_0, tof_to_0_pre, ribb_idx, ribbon))

    #                 tof_rv = ROOT.RooRealVar("tof0"+name, "tof0"+name, tof_to_0)
    #                 tof_rv.setConstant(True)
    #                 ROOT.SetOwnership(tof_rv, False)

    #                 loc_toFit = ROOT.RooFormulaVar("loc_toFit"+name, "(@0+@1)", ROOT.RooArgList(loc_toFit0, tof_rv))
    #                 # loc_toFit.Print()
    #                 # tof_rv.Print()
    #                 ROOT.SetOwnership(loc_toFit, False)

    #                 model_toFit = ROOT.RooJohnson("model_toFit"+name,"model_toFit"+name, x, loc_toFit, scale_fromFit, a_fromFit, b_fromFit)
    #                 ROOT.SetOwnership(model_toFit, False)

    #                 variables_CCW.add(loc_toFit)
    #                 # constraintFuncs.add(tof_rv)
    #                 pdfs_CCW.add(model_toFit)


    #         joint_model_CCW = ROOT.RooAddPdf("joint_model_CCW","joint_model_CCW", pdfs_CCW, fracs_CCW)
    #         # print("Must be extended? {}".format(joint_model_CCW.mustBeExtended()))


    #         # nll_CCW = joint_model_CCW.createNLL(dataset)
    #         # minimizer_CCW = ROOT.RooMinimizer(nll_CCW)
    #         # minimizer_CCW.setEps(1e-8)
    #         # minimizer_CCW.migrad()
    #         # nll_CCW = nll_CCW.getValV()

    #         # rf_CCW_result = minimizer_CCW.save()
    #         rf_CCW_result = joint_model_CCW.fitTo(dataset, ROOT.RooFit.Save(True), ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.PrintEvalErrors(-1), ROOT.RooFit.Warnings(False))

    #         # fits_attempted += 1
    #         if (rf_CCW_result.status() != 0): 
    #         #     fits_failed += 1
    #             print("CCW FIT FAILED")
    #             continue

    #         nll_CCW = rf_CCW_result.minNll()


    #         # print("NLL for CW model {} and CCW model {}".format(nll_CW, nll_CCW))
    #         if (meanMCTimePerCrossing[tid][hidgs[0]] < meanMCTimePerCrossing[tid][hidgs[1]]):
    #             std_result = False
    #             fit_result = False
    #             if nll_CW == min(nll_CW, nll_CCW):
    #                 fit_result = True
    #                 # print("CLOCKWISE SELCTED")
    #                 fit_correct_ID += 1
    #             elif nll_CCW == min(nll_CW, nll_CCW):
    #                 fit_result = False
    #                 # print("COUNTER CLOCKWISE SELECTED")
    #                 fit_incorrect_ID += 1

    #             gr_nll_diff_vs_TOF.SetPoint(gr_nll_diff_vs_TOF.GetN(), meanMCTimesDiff[0], nll_CW-nll_CCW)

    #             if (meanMCTimesDiff[0]<1): gr_nll_diff_under_TOF_1ns_vs_recurls.SetPoint(gr_nll_diff_under_TOF_1ns_vs_recurls.GetN(),len(hidgs),nll_CW-nll_CCW)
    #             gr_nll_diff_vs_recurls.SetPoint(gr_nll_diff_vs_recurls.GetN(),len(hidgs),nll_CW-nll_CCW)

    #             if (minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])>0:
    #                 std_result = True
    #                 dt_correct_ID += 1
    #             elif (minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])<0:
    #                 std_result = False
    #                 dt_incorrect_ID += 1
    #                 eff_fit_neg_obs_times.Fill(fit_result, minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])
    #                 if (fit_result): fit_correct_ID_neg_time += 1
    #                 else: fit_incorrect_ID_neg_time += 1

    #             # Do this for 2 recurls, 4 recurls, 6 recurls
    #             eff_fit_TOF.Fill(fit_result, meanMCTimesDiff[0])
    #             eff_std_TOF.Fill(std_result, meanMCTimesDiff[0])

    #             if (len(hidgs)==2):
    #                 eff_fit_TOF_2recurls.Fill(fit_result, meanMCTimesDiff[0])
    #                 eff_std_TOF_2recurls.Fill(std_result, meanMCTimesDiff[0])
    #             elif(len(hidgs)==3):
    #                 eff_fit_TOF_3recurls.Fill(fit_result, meanMCTimesDiff[0])
    #                 eff_std_TOF_3recurls.Fill(std_result, meanMCTimesDiff[0])
    #             elif(len(hidgs)==4):
    #                 eff_fit_TOF_4recurls.Fill(fit_result, meanMCTimesDiff[0])
    #                 eff_std_TOF_4recurls.Fill(std_result, meanMCTimesDiff[0])
    #             elif(len(hidgs)==5):
    #                 eff_fit_TOF_5recurls.Fill(fit_result, meanMCTimesDiff[0])
    #                 eff_std_TOF_5recurls.Fill(std_result, meanMCTimesDiff[0])
    #             elif(len(hidgs)==6):
    #                 eff_fit_TOF_6recurls.Fill(fit_result, meanMCTimesDiff[0])
    #                 eff_std_TOF_6recurls.Fill(std_result, meanMCTimesDiff[0])

    #             eff_std_t2_t1.Fill(std_result, minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])
    #             eff_fit_t2_t1.Fill(fit_result, minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])

    #             ds = (meanMCTimePerCrossing[tid][hidgs[1]]-meanMCTimePerCrossing[tid][hidgs[0]])*c
    #             # print(ds)
    #             eff_fit_dt_vs_ds.Fill(fit_result, ds, minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])
    #             eff_std_dt_vs_ds.Fill(std_result, ds, minTimePerCrossing[tid][hidgs[1]]-minTimePerCrossing[tid][hidgs[0]])

    #             eff_fit_recurls.Fill(fit_result, len(hidgs))
    #             eff_std_recurls.Fill(std_result, len(hidgs))

    #             if (meanMCTimesDiff[0]<1):
    #                 eff_fit_recurls_TOF_under_1ns.Fill(fit_result, len(hidgs))
    #                 eff_std_recurls_TOF_under_1ns.Fill(std_result, len(hidgs))
    #                 if (fit_result): fit_correct_small_tof += 1
    #                 else: fit_incorrect_small_tof += 1
    #                 if (std_result): std_correct_small_tof += 1
    #                 else: std_incorrect_small_tof += 1




    #         # if (minTimePerCrossing[tid][hidgs[1]]<minTimePerCrossing[tid][hidgs[0]]): print("T1<T2 INVERTED RECO TIMING!")
    #         # if (meanMCTimesDiff[0]>1.0 and nll_CCW == min(nll_CW, nll_CCW) and nhits_per_crossing[0] >= 3 and nhits_per_crossing[1] >= 3):
    #         # if (nll_CCW == min(nll_CW, nll_CCW)):
    #         # # if (fit_result and not std_result):
    #         # if (len(hidgs)==4):
    #         #     # for hit in hits_in_ds:
    #         #     #     hit.printHit()
    #         # # #     # print ("MC time per crossing ", meanMCTimePerCrossing)
    #         # # #     # print ("Mean time (Z corr) per crossing ", meanTimePerCrossing)
    #         # #     print("Min Z corr time T1 {}, min Z corr time T2 {}".format(minTimePerCrossing[tid][hidgs[0]], minTimePerCrossing[tid][hidgs[1]]))
    #         #     for i,hidg in enumerate(hidgs):
    #         #         print("MC time for hidg {}, (T{}) was {} [ns]".format(hidg,i,meanMCTimePerCrossing[tid][hidg]))
    #         #     for i, tof in enumerate(meanMCTimesDiff): 
    #         #         print ("TOF between T{} and T{} is {}".format(i, i+1, tof))
    #             # print("NLL for CW model {} and CCW model {}".format(nll_CW, nll_CCW))

    #         #     print("Fitted CW locs:")
    #         #     for i in range(variables_CW.getSize()): variables_CW.at(i).Print()
    #         #     print("Fitted CW coeffs:")
    #         #     for i in range(fracs_CW.getSize()): fracs_CW.at(i).Print()


    #         #     print("Fitted CCW locs:")
    #         #     for i in range(variables_CCW.getSize()): variables_CCW.at(i).Print()
    #         #     print("Fitted CCW coeffs:")
    #         #     for i in range(fracs_CCW.getSize()): fracs_CCW.at(i).Print()

    #         # # #     # #     # consts = rf_CW_result.constPars()
    #         # # #     # #     # for i in range(consts.getSize()): consts.at(i).Print()
    #             # frame = x.frame()
    #             # # dataset.convertToTreeStore()
    #             # # print("{}".format(dataset.GetMaximum()))
    #             # dataset.plotOn(frame)
    #             # joint_model_CCW.plotOn(frame,ROOT.RooFit.LineColor(ROOT.kRed))
    #             # joint_model_CW.plotOn(frame,ROOT.RooFit.LineColor(ROOT.kBlue))
    #             # # for pdf in range(j): pdfs.at(pdf).plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed))
    #             # frame.SetMinimum(1e-5)
    #             # frame.Draw()
    #             # input("Enter to continue...")
    #         del rf_CCW_result, rf_CW_result, joint_model_CCW, joint_model_CW, tof_rv, loc_toFit0, model_toFit0, loc_toFit, model_toFit, frac



    # # for ribbon in range(12):
    # #     for sideIdx in range(2):
    # # #         # if (trackPU_map[ribbon][sideIdx] and not recurlerNonPU_map[ribbon][sideIdx] and not recurlerPU_map[ribbon][sideIdx]):
    # #         if (not trackNonPU_map[ribbon][sideIdx] and not trackPU_map[ribbon][sideIdx] and recurlerPU_map[ribbon][sideIdx]):
    # #         # if (not trackNonPU_map[ribbon][sideIdx] and not trackPU_map[ribbon][sideIdx] and recurlerNonPU_map[ribbon][sideIdx]):
    # #             side = -1 if sideIdx==0 else 1
    # # #             # print("Ribbon {}, side {}".format(ribbon,side))

    # #             hitsOnRibbonAndSide = getHitsOnRibbonAndSide(all_hits, ribbon, side)
    # #             # for hit in hitsOnRibbonAndSide: hit.printHit()


    # # #             # For track PU cases only
    # # #             # tidsOnSide = trackPU_tid_map[ribbon][sideIdx]
    # # #             # # print ("track PU tids {}".format(tidsOnSide))
    # # #             # hitsOnRibbonAndSide[:] = [hit for hit in hitsOnRibbonAndSide if hit.tid in tidsOnSide]


    # # #             # For recurler PU cases only
    # #             tidsOnSide = recurlerPU_tid_map[ribbon][sideIdx]
    # #             hidsOnSide = recurlerPU_hid_map[ribbon][sideIdx]
    # #             # print("before sort",hidsOnSide)
    # #             hidsOnSide.sort()
    # #             # print("after sort",hidsOnSide)
    # #             # print ("recurler PU tids {}, hids {}".format(tidsOnSide,hidsOnSide))
    # #             hidgsOnSide = recurlerPU_hidg_map[ribbon][sideIdx]
    # #             hidgsOnSide.sort()
    # #             # print(hidgsOnSide)
    # #             # if (hidgsOnSide[1]-hidgsOnSide[0])
    # #             # TODO Plot TOF vs hid diff
    # #             if (len(hidgsOnSide) > 1): 
    # #                 # print("Recurler 2 hid - recurler 1 hid ", hidgsOnSide[1]-hidgsOnSide[0])
    # #                 if (0 in hidgsOnSide): continue
    # #                 # if (hidgsOnSide[1]-hidgsOnSide[0] != 8): continue
    # #             else: continue
    # #             hitsOnRibbonAndSide[:] = [hit for hit in hitsOnRibbonAndSide if hit.tid in tidsOnSide and hit.hid in hidsOnSide]


    # #             flag = False
    # #             for idx, hit in enumerate(hitsOnRibbonAndSide):
    # #                 # if (hitsOnRibbonAndSide[idx+1].col - hit.col != 1): 
    # #                 #     flag = True
    # #                 #     break 
    # #                 if (hit.time_mc < 0 or hit.hid_g==0): 
    # #                     flag = True
    # #                     break
    # #                 # hit.printHit() 
    # #             # print("Selection done!")
    # #             if (flag): continue

    # #             if len(hitsOnRibbonAndSide) > 0:
    # # #                 # print (len(tidsOnSide))
    # # #                 # if len(tidsOnSide) > 2: print ("MORE THAN 2 TRACK PU!")
    # # #                 # if len(hidsOnSide) > 2: print ("MORE THAN 2 RECURLER PU!")
                    
    # #                 dataset = ROOT.RooDataSet("ds","ds",ROOT.RooArgSet(x))

    # #                 for hit in hitsOnRibbonAndSide:
    # #                     # x.setVal(hit.time)
    # #                     x.setVal(hit.tCorr)
    # #                     dataset.add(ROOT.RooArgSet(x))

    # #                 ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
    # #                 # ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
    # #                 ROOT.RooMsgService.instance().setSilentMode(True)
    # #                 # times = [hit.time for hit in hitsOnRibbonAndSide]
    # #                 times = [hit.tCorr for hit in hitsOnRibbonAndSide]
    # # #                 # nlls = []
    # # #                 # aics = []
    # # #                 # aiccs = []
    # # #                 bics = []
    # # #                 # means = [[] for i in range(int(dataset.sumEntries())+1)]
    # # #                 # c = ROOT.TCanvas("test", "test", 900, 600)
    # # #                 # c.Divide(3)
    # # #                 # expec_cls = getExpectedNClusters(hitsOnRibbonAndSide)
    # # #                 # expec_cls = len(tidsOnSide)
    # #                 expec_cls = len(hidsOnSide)
    # #                 if (expec_cls > 2): continue
    # #                 # print("max hit time {}, min hit time {}".format(max(times), min(times)))
                    
    # #                 meanMCTimePerCrossing = defaultdict(lambda: defaultdict(float))
    # #                 if side == 0: meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_L)
    # #                 elif side == 1: meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_R)

    # #                 minTimePerCrossing = defaultdict(lambda: defaultdict(float))
    # #                 if side == 0: minTimePerCrossing = getEarliestTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_L)
    # #                 elif side == 1: minTimePerCrossing = getEarliestTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_R)

    # #                 meanTimePerCrossing = defaultdict(lambda: defaultdict(float))
    # #                 if side == 0: meanTimePerCrossing = getMeanTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_L)
    # #                 elif side == 1: meanTimePerCrossing = getMeanTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_R)

    # #                 tof = (meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]] - meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]])
    # #                 min_recurl_diff = (minTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]] - minTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]])
    # #                 mean_recurl_diff = (meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]] - meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]])
    # #                 # print ("T_min_recurl_2 {:.2f}, T_min_recurl_1 {:.2f}, diff {:.2f}".format(minTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]], minTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]], minTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]]-minTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]))
    # #                 # print ("T_mean_recurl_2 {:.2f}, T_mean_recurl_1 {:.2f}, diff {:.2f}".format(meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]], meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]], meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]]-meanTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]))
    # #                 # print ("T_mc_recurl_2 {:.2f}, T_mc_recurl_1 {:.2f}".format(meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]], meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]))
    # #                 # print ("TOF was {:.2f} [ns]".format(tof))

    # #                 # recurl 1/2/3/4
    # #                 # recurl1_hits = [hit for hit in hitsOnRibbonAndSide if hit.tid == tidsOnSide[0] and hit.hid == hidsOnSide[0]]
    # #                 # for hit in recurl1_hits:
    # #                 #     xres.setVal(hit.tCorr-hit.time_mc)
    # #                 #     recurl1_dataset.add(ROOT.RooArgSet(xres))

    # #                 # recurl2_hits = [hit for hit in hitsOnRibbonAndSide if hit.tid == tidsOnSide[0] and hit.hid == hidsOnSide[1]]
    # #                 # for hit in recurl2_hits:
    # #                 #     xres.setVal(hit.tCorr-hit.time_mc)
    # #                 #     recurl2_dataset.add(ROOT.RooArgSet(xres))

    # #                 # if (expec_cls == 3):
    # #                 #     recurl3_hits = [hit for hit in hitsOnRibbonAndSide if hit.tid == tidsOnSide[0] and hit.hid == hidsOnSide[2]]
    # #                 #     for hit in recurl3_hits:
    # #                 #         xres.setVal(hit.tCorr-hit.time_mc)
    # #                 #         recurl3_dataset.add(ROOT.RooArgSet(xres))

    # #                 # if (expec_cls == 4):
    # #                 #     recurl4_hits = [hit for hit in hitsOnRibbonAndSide if hit.tid == tidsOnSide[0] and hit.hid == hidsOnSide[3]]
    # #                 #     for hit in recurl4_hits:
    # #                 #         xres.setVal(hit.tCorr-hit.time_mc)
    # #                 #         recurl4_dataset.add(ROOT.RooArgSet(xres))



    # #                 # TODO
    # #                 # Change this so that you create a fixed number of pdfs
    # #                 # THEN fit different numbers of them
    # #                 # Rather than creating/destroying loads of objects repeadtedly.

    # #                 # Try a number of pdfs to fit up to number of data points OR?
    # #                 N = min(int(dataset.sumEntries()), 2)
    # #                 for j in range(2, N+1):
    # #                     fracs.removeAll()
    # #                     variables.removeAll()
    # #                     pdfs.removeAll()
    # #                     timeRange = (max(times) - min(times))/float(j)
    # #                 #     # print(timeRange, (max(times) - min(times)), j)
    # #                     # for i in range(j): #extended version 
    # #                     #     frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), 1/float(j), 0.0, dataset.sumEntries())
    # #                     #     ROOT.SetOwnership(frac, False)
    # #                     #     fracs.add(frac)
    # #                     for i in range(j-1): #unextended version 
    # #                         frac = ROOT.RooRealVar("frac"+str(i), "frac"+str(i), 1/dataset.sumEntries(), 0.0, 1.0)
    # #                         ROOT.SetOwnership(frac, False)
    # #                         fracs.add(frac)
    # #                     for i in range(j):
    # #                         # loc_guess = float(i)*64.0/float(j)
    # #                         # loc_guess = (min(times) + i*timeRange) - Y_fromFit.getValV()*scale_fromFit.getValV() # loc_guess = x - Y*scale
    # #                         loc_guess = min(times) + i*timeRange
    # #                         # loc_guess = 32.0
    # #                         # print("loc_guess {}".format(loc_guess))
    # #                         makeJohnsonPDFtoFit(str(i), loc_guess, variables, pdfs)

    # #                     joint_model = ROOT.RooAddPdf("joint_model","joint_model", pdfs, fracs)

    # #                 #     # joint_model.fitTo(dataset, ROOT.RooFit.Extended(), ROOT.RooFit.PrintEvalErrors(-1000))

    # #                     # nll = joint_model.createNLL(dataset)
    # #                     # minimizer = ROOT.RooMinimizer(nll)
    # #                     # minimizer.migrad()


    # #                     # tof_rv = ROOT.RooRealVar("tof", "tof", tof)
    # #                     # tof_res_rv = ROOT.RooRealVar("tof_res", "tof_res", 0.04)
    # #                     # constraint_pdf = ROOT.RooGenericPdf("fconstraint", "fconstraint", "exp(-0.5*pow((@1-@0) - @2,2.0)/pow(@3,2.0))", ROOT.RooArgList(variables.at(0), variables.at(1), ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.04)))
    # #                     # joint_model_constraint = ROOT.RooProdPdf("joint_model","joint_model", ROOT.RooArgList(joint_model, constraint_pdf))
    # #                     # nll = joint_model_constraint.createNLL(dataset)

    # #                     # f = ROOT.RooFormulaVar("f", "(@1-@0)-@2", ROOT.RooArgList(variables.at(0), variables.at(1), ROOT.RooFit.RooConst(tof)))
    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(0), ROOT.RooFit.RooConst(0.04))
    # #                     # f = ROOT.RooFormulaVar("f", "(@1-@0)", variables)
    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.04))
    # #                     # nll = joint_model.createNLL(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))
    # #                     # nll = joint_model.createNLL(dataset, ROOT.RooFit.Constrain(f))
    # #                     # nll_before = joint_model.createNLL(dataset)
    # #                     # penalty = ROOT.RooFormulaVar("penalty", "100*((@1<@0)*((@1-@0)<0))*exp(-0.5*pow((@1-@0) - @2,2.0)/pow(@3,2.0))", ROOT.RooArgList(variables.at(0), variables.at(1), ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.04)))
    # #                     # nll = ROOT.RooAddition("nll","nll",ROOT.RooArgSet(nll_before,penalty))


    # #                     # minimizer = ROOT.RooMinimizer(nll)
    # #                     # minimizer.setPrintLevel(-1)
    # #                     # minimizer.setPrintEvalErrors(-1)
    # #                     # minimizer.setEps(1e-8)
    # #                     # minimizer.migrad()
    # #                     # rf_result = minimizer.save()

    # #                     f = ROOT.RooFormulaVar("f", "(@1-@0)", variables)
    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(1.5))
    # #                     # joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(1.0))
    # #                     # joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.5))
    # #                     joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.25))
    # #                     # joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.125))
    # #                     joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     # fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.0525))
    # #                     # joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint))

    # #                     fconstraint = ROOT.RooGaussian("fconstraint", "fconstraint", f, ROOT.RooFit.RooConst(tof), ROOT.RooFit.RooConst(0.02))
    # #                     rf_result = joint_model.fitTo(dataset, ROOT.RooFit.ExternalConstraints(fconstraint), ROOT.RooFit.Save(True))

    # #                     fits_attempted += 1

    # #                     if (rf_result.status() != 0): 
    # #                         fits_failed += 1
    # #                         # print ("Fit failure")
    # #                         # for hit in hitsOnRibbonAndSide:
    # #                         #     hit.printHit()
    # #                         # print ("T_mc_recurl_2 {:.2f}, T_mc_recurl_1 {:.2f}".format(meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]], meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]))
    # #                         # print ("MC TOF {:.2f}, T_min_recurl_2 - T_min_recurl_1 = {:.2f}, min_recurl_dt - MC_tof = {:.2f}, fitted loc diff {:.2f}".format(tof, min_recurl_diff, min_recurl_diff-tof, abs(variables.at(0).getValV() - variables.at(1).getValV())))
    # #                         continue

    # #                     # print ("MC TOF {:.2f}, T_min_recurl_2 - T_min_recurl_1 = {:.2f}, min_recurl_dt - MC_tof = {:.2f}, fitted loc diff {:.2f}".format(tof, min_recurl_diff, min_recurl_diff-tof, abs(variables.at(0).getValV() - variables.at(1).getValV())))

    # #                 # #     # For case of just 2 recurl cls
    # #                     recurl1_res = variables.at(0).getValV() - meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]
    # #                     recurl2_res = variables.at(1).getValV() - meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]]

    # #                     h_earliest_recurl_diff.Fill(min_recurl_diff)
    # #                     h_earliest_recurl_diff_true_tof_diff.Fill(min_recurl_diff-tof)
    # #                     h_earliest_recurl_diff_vs_true_tof.Fill(tof, min_recurl_diff)
                        
    # #                     h_mean_recurl_diff.Fill(mean_recurl_diff)
    # #                     h_mean_recurl_diff_true_tof_diff.Fill(mean_recurl_diff-tof)
    # #                     h_mean_recurl_diff_vs_true_tof.Fill(tof, mean_recurl_diff)

    # #                     loc_diff = variables.at(1).getValV()-variables.at(0).getValV()
    # #                     # print("LOC diff %.2f [ns]" % loc_diff)
    # #                     # print("LOC diff - TOF = %.2f [ns]" % (loc_diff-tof))
    # #                     # if (abs(loc_diff-tof)>1.5): 
    # #                     #     print("BAD FIT") 

    # #                     h_loc_diff_minus_tof.Fill(loc_diff - tof)
    # #                     h_loc_diff_minus_tof_high_res.Fill(loc_diff - tof)
    # #                     h_loc_diff_vs_tof.Fill(tof, loc_diff)

    # #                     h_earliest_hit_res.Fill(minTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]]-meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[0]])
    # #                     h_earliest_hit_recurl2_res.Fill(minTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]]-meanMCTimePerCrossing[tidsOnSide[0]][hidsOnSide[1]])

    # #                     h_loc_pdf_res.Fill(recurl1_res) 
    # #                 # #     # h_loc_pdf_res.Fill(recurl2_res)

    # #                 # #     # Plot recurl 1 vs recurl 2, (lowest hid vs higher hid)
    # #                     h_recurl1_vs_recurl2_loc_res.Fill(recurl1_res, recurl2_res)


    #                 #     # TODO try using ICL (integrated completed likelihood)

    #                 #     # print("TRYING N = {} PDFS".format(j))
    #                 #     # print("\tNLL VALUE {}".format(nll.getValV()))
    #                 #     # k = 2*j + 100# n_params
    #                 #     # aic = 2*k + 2*nll.getValV()
    #                 #     # aicc = aic + (2*k*(k+1))/(dataset.sumEntries()-k-1)
    #                 #     # bic = k*np.log(dataset.sumEntries()) + 2*nll.getValV()
    #                 #     # bic = 2*2*j + np.log(dataset.sumEntries())*nll.getValV()
    #                 #     # print("\tAIC VALUE {}".format(aic) )
    #                 #     # print("\tBIC VALUE {}".format(bic) )
    #                 #     # nlls.append(nll.getValV())
    #                 #     # aics.append(aic)
    #                 #     # aiccs.append(aicc)
    #                 #     # bics.append(bic)

    #                     # print("Fitted locs:")
    #                     # for i in range(variables.getSize()): variables.at(i).Print()

    #                 #     # print("Fitted means:")
    #                 #     # for i in range(variables.getSize()): 
    #                 #     #     means[j].append(johnsonsu_mean(variables.at(i).getValV()))
    #                 #     #     print("Mean_{} = {}".format(i,johnsonsu_mean(variables.at(i).getValV())))

    #                     # print("Fitted coeffs:")
    #                     # for i in range(fracs.getSize()): fracs.at(i).Print()

    #                 #     # Evaluating each datapoint's probability of belonging to each pdf!
    #                 #     # P(pdf_0|x=45) = pdf_0(x=45) / sum(pdf_0(45) + pdf_1(45) + ... + pdf_n(45))
    #                 #     # P(pdf_1|x=45) = ...
    #                 #     # P(pdf_n|x=45) = ...

    #                 #     # if j == expec_cls:
    #                 #     #     evals = [[] for i in range(j)]
    #                 #     #     for i in range(j):
    #                 #     #         for time in times:
    #                 #     #             # print(time)
    #                 #     #             x.setVal(time)
    #                 #     #             evaluation = variables.at(i).getVal() * pdfs.at(i).getVal(x)
    #                 #     #             evals[i].append(evaluation)
    #                 #     #     #     print(i, evals[i], sum(evals[i]))
    #                 #     #     # print(evals)
    #                 #     #     for i in range(j):
    #                 #     #         print("PDF_{} at mean peak {}".format(i, means[j][i]))
    #                 #     #         for idx, time in enumerate(times):
    #                 #     #             to_eval = [a[idx:idx+1] for a in evals]
    #                 #     #             to_eval = sum(to_eval, [])
    #                 #     #             # print(to_eval)
    #                 #     #             print("\tProbability of x = {}, belonging to pdf_{} is {}".format(time,i,evals[i][idx]/sum(to_eval)))
    #                 #     #     for i in range(j-1):
    #                 #     #         print([p_n / p_nPlus1 for p_n, p_nPlus1 in zip(evals[i], evals[i+1])])

    #                 #     # Evaluating resolution performance...
    #                 #     # if j == expec_cls:
    #                 #     #     # find nearest crossing times to each pdf's peak
    #                 #     #     meanMCTimePerCrossing = defaultdict(lambda: defaultdict(float))
    #                 #     #     if side == 0: meanMCTimePerCrossing = getEarliestTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_L)
    #                 #     #     elif side == 1: meanMCTimePerCrossing = getEarliestTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_R)
    #                 #     #     # print (meanMCTimePerCrossing)
                            
    #                 #     #     for i in range(j):
    #                 #     #         mean_of_pdf = means[j][i]
    #                 #     #         closest_hit_time = 1e6
    #                 #     #         closest_hit = type(mppcHit)
    #                 #     #         for hit in hitsOnRibbonAndSide:
    #                 #     #             if (abs(hit.time - mean_of_pdf) < closest_hit_time):
    #                 #     #                 closest_hit_time = abs(hit.time - mean_of_pdf)
    #                 #     #                 closest_hit = hit
    #                 #     #         # closest_hit.printHit()
    #                 #     #         h_closest_hit_to_pdf_peak_res.Fill(mean_of_pdf - closest_hit.time)
    #                 #     #         # print (meanMCTimePerCrossing[closest_hit.tid][closest_hit.hid])
    #                 #     #         h_earliest_hit_to_pdf_peak_res.Fill(mean_of_pdf - meanMCTimePerCrossing[closest_hit.tid][closest_hit.hid])

    #                             # dCorr = 0.0
    #                             # if (closest_hit.side == 0): dCorr = fb_len_half - closest_hit.side
    #                             # elif (closest_hit.side == +1): dCorr = fb_len_half + closest_hit.side
    #                 #     #         tCorr = dCorr / cn

    #                 #     #         h_peak_pdf_res.Fill((mean_of_pdf-tCorr) - closest_hit.time_mc)
    #                 #     #         #h_earliest_hit_res.Fill(-closest_hit.time_mc)

    #                 #     # Plotting each model component
    #                     # if j == expec_cls:
    #                     #     frame = x.frame()
    #                     #     dataset.plotOn(frame)
    #                     #     joint_model.plotOn(frame)
    #                     #     # for pdf in range(j): pdfs.at(pdf).plotOn(frame, ROOT.RooFit.LineStyle(ROOT.kDashed))
    #                     #     frame.SetMinimum(1e-5)
    #                     #     frame.Draw()
                        
    #                 #     # Plotting model +/- 1 number of expected pdfs
    #                     # if j == expec_cls: 
    #                     #     # c.cd(2)
    #                     #     frame = x.frame()
    #                     #     dataset.plotOn(frame)
    #                     #     joint_model.plotOn(frame)
    #                     #     frame.SetMinimum(1e-5)
    #                     #     frame.Draw()
    #                 #     # elif j - expec_cls == 1: 
    #                 #     #     c.cd(3)
    #                 #     #     frame = x.frame()
    #                 #     #     dataset.plotOn(frame)
    #                 #     #     joint_model.plotOn(frame)
    #                 #     #     frame.SetMinimum(1e-5)
    #                 #     #     frame.Draw()
    #                 #     # elif j - expec_cls == -1: 
    #                 #     #     c.cd(1)
    #                 #     #     frame = x.frame()
    #                 #     #     dataset.plotOn(frame)
    #                 #     #     joint_model.plotOn(frame)
    #                 #     #     frame.SetMinimum(1e-5)
    #                 #     #     frame.Draw()

    #                 # # Testing how to get true min of aic/bic! (3 methods...)

    #                 # # c = ROOT.TCanvas("test", "test", 900, 600)
    #                 # # gr = ROOT.TGraph()
    #                 # # gr.SetTitle(";Number of pdfs fitted;BIC value")
    #                 # # for j in range(1, N+1):
    #                 # #     gr.SetPoint(gr.GetN(), j, bics[j-1])
    #                 # # # gr.Fit("pol6")
    #                 # # gr.Draw("AL")
    #                 # # c.Draw()

    #                 # # from kneed import KneeLocator
    #                 # # kn_aicc = KneeLocator([j for j in range(1,N+1)], aiccs, curve='convex', direction='decreasing',online=True)
    #                 # # kn_bic = KneeLocator([j for j in range(1,N+1)], bics, curve='convex', direction='decreasing',online=True)
    #                 # # kn_aic = KneeLocator([j for j in range(1,N+1)], aics, curve='convex', direction='decreasing',online=True)
    #                 # # print("Knee at aicc {}, bic {}, aic {}".format(kn_aicc.knee,kn_bic.knee,kn_aic.knee))

    #                 # # print("Local minima in aicc {}, bic {}, aic {}".format(argrelextrema(np.array(aiccs), np.less),argrelextrema(np.array(bics), np.less),argrelextrema(np.array(aics), np.less)))

    #                 # # print("nlls ", nlls)
    #                 # # print("AICs ", aics)
    #                 # # print("aiccs ", aiccs)
    #                 # # print("BICs ", bics)

    #                 # # print("expected numb of clusters {}".format(expec_cls))

    #                 # # min_aic_idx = aics.index(min(aics))+1
    #                 # # min_bic_idx = bics.index(min(bics))+1
    #                 # # print("predicted numb of clusters {} by min AIC".format(min_aic_idx))
    #                 # # print("predicted numb of clusters {} by min corr_aic".format(aiccs.index(min(aiccs))+1))
    #                 # # print("predicted numb of clusters {} by min BIC".format(min_bic_idx))

    #                 # # h_ncls_resid_AIC.Fill(expec_cls-min_aic_idx)
    #                 # # h_ncls_resid_BIC.Fill(expec_cls-min_bic_idx)

    #                 # # print("predicted pdf means with BIC {}".format(means[bics.index(min(bics))+1]))
    #                 # # print("predicted pdf means using expec numb of cls {}".format(means[expec_cls]))

    #                 # # if side == 0: meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_L)
    #                 # # elif side == 1: meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hitsOnRibbonAndSide, truth_cls_R)

    #                 # # c.SaveAs("test.pdf")
    #                 # # c.Draw()

    #                 # # raw_input("Press Enter to continue...")
    #                 # input("Press Enter to continue...")


# base_model = ROOT.RooJohnson("base_model","base_model", xres, loc_fromFit, scale_fromFit, a_fromFit, b_fromFit)

# can = ROOT.TCanvas("test", "test", 1200, 900)
# can.Divide(2,2)

# recurl1_frame = xres.frame(ROOT.RooFit.Title("Time resolution per hit (for recurl 1 hits)"),ROOT.RooFit.Range(-2, 9.0))
# recurl1_frame.SetYTitle("Events")
# recurl1_frame.SetXTitle("t_{hit} - t_{mc} [ns]")
# recurl1_dataset.plotOn(recurl1_frame)
# base_model.plotOn(recurl1_frame)
# recurl1_frame.SetMinimum(1e-5)
# can.cd(1)
# recurl1_frame.Draw()
# recurl1_frame.SetName("recurl1_frame")
# recurl1_frame.Write()

# recurl2_frame = xres.frame(ROOT.RooFit.Title("Time resolution per hit (for recurl 2 hits)"),ROOT.RooFit.Range(-2, 9.0))
# recurl2_frame.SetYTitle("Events")
# recurl2_frame.SetXTitle("t_{hit} - t_{mc} [ns]")
# recurl2_dataset.plotOn(recurl2_frame)
# base_model.plotOn(recurl2_frame)
# recurl2_frame.SetMinimum(1e-5)
# can.cd(2)
# recurl2_frame.Draw()
# recurl2_frame.SetName("recurl2_frame")
# recurl2_frame.Write()

# recurl3_frame = xres.frame(ROOT.RooFit.Title("Time resolution per hit (for recurl 3 hits)"),ROOT.RooFit.Range(-2, 9.0))
# recurl3_frame.SetYTitle("Events")
# recurl3_frame.SetXTitle("t_{hit} - t_{mc} [ns]")
# recurl3_dataset.plotOn(recurl3_frame)
# base_model.plotOn(recurl3_frame)
# recurl3_frame.SetMinimum(1e-5)
# can.cd(3)
# recurl3_frame.Draw()
# recurl3_frame.SetName("recurl3_frame")
# recurl3_frame.Write()

# recurl4_frame = xres.frame(ROOT.RooFit.Title("Time resolution per hit (for recurl 4 hits)"),ROOT.RooFit.Range(-2, 9.0))
# recurl4_frame.SetYTitle("Events")
# recurl4_frame.SetXTitle("t_{hit} - t_{mc} [ns]")
# recurl4_dataset.plotOn(recurl4_frame)
# base_model.plotOn(recurl4_frame)
# recurl4_frame.SetMinimum(1e-5)
# can.cd(4)
# recurl4_frame.Draw()
# recurl4_frame.SetName("recurl4_frame")
# recurl4_frame.Write()

# can.Draw()
# can.SaveAs("test.pdf")

# clean_cls_frame = xres.frame(ROOT.RooFit.Title("Time resolution per hit (for clean cls)"),ROOT.RooFit.Range(-2, 9.0))
# clean_cls_frame.SetYTitle("Events")
# clean_cls_frame.SetXTitle("t_{hit} - t_{mc} [ns]")
# clean_cls_ds.plotOn(clean_cls_frame)
# base_model.plotOn(clean_cls_frame)
# clean_cls_frame.SetMinimum(1e-5)
# clean_cls_frame.Draw()
# clean_cls_frame.SetName("clean_cls_frame")
# clean_cls_frame.Write()

# print("FWHM/2.355 for peak pdf - t_mc: {} [ps]".format(getFWHM(h_peak_pdf_res, 0.5)/2.355 * 1e3))
# print("FWHM/2.355 for loc pdf - t_mc: {} [ps]".format(getFWHM(h_loc_pdf_res, 0.5)/2.355 * 1e3))
# print("FWHM/2.355 for earliest hit - t_mc: {} [ps]".format(getFWHM(h_earliest_hit_res, 0.5)/2.355 * 1e3))
# print("FWHM/2.355 for loc_diff - TOF: {} [ps]".format(getFWHM(h_loc_diff_minus_tof_high_res, 0.5)/2.355 * 1e3))
# print("Std dev of hist for loc_diff - TOF: {:.3f} [ns] (high res)".format(h_loc_diff_minus_tof_high_res.GetStdDev()))
# print("Std dev of hist for loc_diff - TOF: {:.3f} [ns] (low res)".format(h_loc_diff_minus_tof.GetStdDev()))
# raw_input("Press Enter to continue...")
# print("RECURLER PU: Fits attempted {}, fits failed {}, fail rate {:.2f}%".format(fits_attempted, fits_failed, 100.0*fits_failed/fits_attempted))
# input("Press Enter to continue...")
print("FROM FIT: Correct identifications {}, incorrect identifications {}, eff {:.2f}".format(fit_correct_ID, fit_incorrect_ID, 100.0*fit_correct_ID/float(fit_correct_ID+fit_incorrect_ID)))
print("FROM T2>T1 (dt>0): Correct identifications {}, incorrect identifications {}, eff {:.2f}".format(dt_correct_ID, dt_incorrect_ID,  100.0*dt_correct_ID/float(dt_correct_ID+dt_incorrect_ID)))
print("FROM FIT (for cases where dt<0): Correct identifications {}, incorrect identifications {}, eff {:.2f}".format(fit_correct_ID_neg_time, fit_incorrect_ID_neg_time,  100.0*fit_correct_ID_neg_time/float(fit_correct_ID_neg_time+fit_incorrect_ID_neg_time)))
print("FROM FIT (for cases where TOF<1 ns): Correct identifications {}, incorrect identifications {}, eff {:.2f}".format(fit_correct_small_tof, fit_incorrect_small_tof,  100.0*fit_correct_small_tof/float(fit_correct_small_tof+fit_incorrect_small_tof)))
print("FROM T2>T1 (dt>0) (for cases where TOF<1 ns): Correct identifications {}, incorrect identifications {}, eff {:.2f}".format(std_correct_small_tof, std_incorrect_small_tof,  100.0*std_correct_small_tof/float(std_correct_small_tof+std_incorrect_small_tof)))
print("Finished...")

eff_fit_TOF.Write()
eff_std_TOF.Write()
eff_fit_neg_obs_times.Write()
eff_fit_t2_t1.Write()
eff_std_t2_t1.Write()
eff_fit_dt_vs_ds.Write()
eff_std_dt_vs_ds.Write()
gr_nll_diff_vs_TOF.Write()
gr_nll_diff_under_TOF_1ns_vs_recurls.Write()
gr_nll_diff_vs_recurls.Write()
eff_fit_recurls.Write()
eff_std_recurls.Write()
eff_fit_TOF_2recurls.Write()
eff_std_TOF_2recurls.Write()
eff_fit_TOF_3recurls.Write()
eff_std_TOF_3recurls.Write()
eff_fit_TOF_4recurls.Write()
eff_std_TOF_4recurls.Write()
eff_fit_TOF_5recurls.Write()
eff_std_TOF_5recurls.Write()
eff_fit_TOF_6recurls.Write()
eff_std_TOF_6recurls.Write()
eff_fit_recurls_TOF_under_1ns.Write()
eff_std_recurls_TOF_under_1ns.Write()
output_file.Write()
output_file.Close()

print("Time taken was ", time_py.process_time() - start)