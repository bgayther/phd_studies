// Set mu3eStyle
{
  TStyle *mu3eStyle = new TStyle("mu3eStyle", "mu3e style");

  Int_t FontStyle = 132;
  Int_t LineWidth = 1;
  Float_t FontSizeLabel = 0.035;
  Float_t FontSizeLegend = 0.03; // 0.025
  Float_t FontSizeTitle = 0.05;
  Float_t YOffsetTitle = 1.6;

  // use plain black on white colors
  mu3eStyle->SetFrameBorderMode(0);
  mu3eStyle->SetFrameFillColor(0);
  mu3eStyle->SetCanvasBorderMode(0);
  mu3eStyle->SetPadBorderMode(0);
  mu3eStyle->SetCanvasBorderSize(0);
  mu3eStyle->SetFrameBorderSize(0);
  mu3eStyle->SetDrawBorder(0);
  mu3eStyle->SetTitleBorderSize(0);

  mu3eStyle->SetPadColor(0);
  mu3eStyle->SetCanvasColor(0);
  mu3eStyle->SetStatColor(0);
  mu3eStyle->SetFillColor(0);
  mu3eStyle->SetFrameLineColor(0);

  mu3eStyle->SetEndErrorSize(4);
  mu3eStyle->SetStripDecimals(kFALSE);

  mu3eStyle->SetLegendBorderSize(0);
  mu3eStyle->SetLegendFont(FontStyle);
  mu3eStyle->SetLegendTextSize(FontSizeLegend);

  // set the paper & margin sizes
  mu3eStyle->SetPaperSize(20, 26);
  mu3eStyle->SetPadTopMargin(0.1);
  mu3eStyle->SetPadBottomMargin(0.15);
  mu3eStyle->SetPadRightMargin(0.13); // 0.075 -> 0.13 for colz option
  mu3eStyle->SetPadLeftMargin(0.16);  // to include both large/small font options

  // Fonts, sizes, offsets
  mu3eStyle->SetTextFont(FontStyle);
  mu3eStyle->SetTextSize(0.08);

  mu3eStyle->SetLabelFont(FontStyle, "x");
  mu3eStyle->SetLabelFont(FontStyle, "y");
  mu3eStyle->SetLabelFont(FontStyle, "z");
  mu3eStyle->SetLabelFont(FontStyle, "t");
  mu3eStyle->SetLabelSize(FontSizeLabel, "x");
  mu3eStyle->SetLabelSize(FontSizeLabel, "y");
  mu3eStyle->SetLabelSize(FontSizeLabel, "z");
  mu3eStyle->SetLabelOffset(0.015, "x");
  mu3eStyle->SetLabelOffset(0.015, "y");
  mu3eStyle->SetLabelOffset(0.015, "z");

  mu3eStyle->SetTitleFont(FontStyle, "x");
  mu3eStyle->SetTitleFont(FontStyle, "y");
  mu3eStyle->SetTitleFont(FontStyle, "z");
  mu3eStyle->SetTitleFont(FontStyle, "t");
  mu3eStyle->SetTitleSize(FontSizeTitle, "y");
  mu3eStyle->SetTitleSize(FontSizeTitle, "x");
  mu3eStyle->SetTitleSize(FontSizeTitle, "z");
  mu3eStyle->SetTitleOffset(1.08, "x");
  mu3eStyle->SetTitleOffset(YOffsetTitle, "y");
  mu3eStyle->SetTitleOffset(1.2, "z");

  mu3eStyle->SetTitleStyle(0);
  mu3eStyle->SetTitleFontSize(0.06); // 0.08
  mu3eStyle->SetTitleFont(FontStyle, "pad");
  mu3eStyle->SetTitleBorderSize(0);
  mu3eStyle->SetTitleX(0.5f);
  mu3eStyle->SetTitleW(0.8f);
  mu3eStyle->SetTitleAlign(23);

  // use bold lines and markers
  mu3eStyle->SetMarkerStyle(20);
  mu3eStyle->SetMarkerSize(0.8);
  mu3eStyle->SetHistLineWidth(LineWidth);
  // mu3eStyle->SetLineWidth(LineWidth);
  // mu3eStyle->SetFrameLineWidth(LineWidth);
  mu3eStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  mu3eStyle->SetErrorX(0.0001);
  mu3eStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations
  // mu3eStyle->SetOptTitle(0);
  mu3eStyle->SetOptStat(0);
  mu3eStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  mu3eStyle->SetPadTickX(1);
  mu3eStyle->SetPadTickY(1);

  // mu3eStyle->SetNdivisions(503,"XYZ");
  // mu3eStyle->SetNdivisions(503,"y");
  // mu3eStyle->SetNdivisions(503,"x");

  // -- color --
  // functions blue
  mu3eStyle->SetFuncColor(600 - 4);

  mu3eStyle->SetFillColor(1); // make color fillings (not white)

  const Int_t NRGBs = 5;
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  
  const Int_t NCont = 255;

  // - kDarkBodyRadiator -
  //  mu3eStyle->SetPalette(53);  // use the kDarkBodyRadiator color set

  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  mu3eStyle->SetNumberContours(NCont);

  auto kC0 = TColor::GetFreeColorIndex();
  auto new_col_c0 = new TColor(kC0, 0.12156862745098039, 0.4666666666666667, 0.7058823529411765, "C0");
  mu3eStyle->SetHistLineColor(kC0);

  // -- axis --
  mu3eStyle->SetStripDecimals(kFALSE); // don't do 1.0 -> 1
  //  TGaxis::SetMaxDigits(3); // doesn't have an effect
  
  // no supressed zeroes!
  mu3eStyle->SetHistMinimumZero(kTRUE); //TODO: check

  gROOT->SetStyle("mu3eStyle");
  //   gROOT->ForceStyle();
}