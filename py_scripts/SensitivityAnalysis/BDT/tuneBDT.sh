#!/bin/sh
#PBS -N TuningBDT
#PBS -l ncpus=16
#PBS -l mem=15gb
#PBS -q medium
#PBS -o /unix/muons/users/bgayther/phd_studies/py_scripts/SensitivityAnalysis/BDT/logs
#PBS -e /unix/muons/users/bgayther/phd_studies/py_scripts/SensitivityAnalysis/BDT/logs
#PBD -m abe
#PBS -M zcembga@ucl.ac.uk

source /unix/muons/users/bgayther/phd_studies/ucl_setup_on_cvmfs.sh
cd /unix/muons/users/bgayther/phd_studies/py_scripts/SensitivityAnalysis/BDT/
/unix/muons/users/bgayther/phd_studies/shap_venv/bin/python tuningBDT.py --sig_files ../skim_files/Sig_skim_* --bhabha_files ../skim_files/BhabhaMichel_skim_* --IC_files ../skim_files/IC_skim_* --weightsFile ../weights.pickle --ncpu 16 --resultsDir tune_results #--quick