#!/bin/bash
#set -eu -x
cd $(dirname `readlink -f $0`)
#
# run the full simulation -> reconstruction -> analysis pipeline

source ../../install/activate.sh

mkdir -p data
mkdir -p results

cp digi_sig.json digi.json
mu3eSim pipeline.mac
mu3eSort \
    data/mu3e_run_000042.root \
    data/run000042-sort.root
mu3eTrirec \
    --input data/run000042-sort.root \
    --output data/run000042-reco.root
mu3eAnaVertexfit \
    data/run000042-reco.root \
    data/run000042-ana_vertexfit.root
mu3eAnaSignalnorm \
    data/run000042-sort.root
nsignal=$(cat signalnorm.txt)
mu3eAnaSignalana \
    data/run000042-ana_vertexfit.root \
    signal42 \
    "I" \
    "$nsignal" \
    0

rm digi.json
