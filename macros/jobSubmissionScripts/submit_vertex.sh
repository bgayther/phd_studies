function submit {
    echo "\n Submitting vertex fit for file ${REC_FILE}, run ${RUN}, sample ${SAMPLE}"
	qsub -q short -v REC_FILE,VERTEX_CONF_OPT,SIM_TAG,SAMPLE,VERTEX_TAG,OUTDIR,RUN -m e -M zcembga@ucl.ac.uk run_vertex.sh
	sleep 0.1
}

rm vertex_logs/*
cd vertex_logs
cp ../run_vertex.sh .

export OUTDIR=/unix/muons/users/bgayther/SignalDecays/batch

export SIM_TAG="sig_and_muon_beam"
export SAMPLE="signal"

export VERTEX_TAG="momentum_scaling"
export VERTEX_CONF_OPT="vertex.cuts.applyMomentumScaling = true"

export REC_FILE=""

DESIRED_RUN=101

FILELIST=${OUTDIR}/sim_${SAMPLE}_${DESIRED_RUN}_*_${SIM_TAG}/*/results/mu3e_trirec_*.root
# echo ${#FILELIST[@]}
echo $FILELIST
if compgen -G "$FILELIST" > /dev/null; then
    for FILENAME in $FILELIST; do
        # echo $FILENAME
        TRUNC=${FILENAME#*mu3e_trirec_*}
        export RUN=${TRUNC%%_*}
        export REC_FILE=${FILENAME}
        submit
    done
fi

cd ..