cmake_minimum_required(VERSION 3.1)

project(scripts C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(nlohmann_json)
# find_package(Vdt)
# include(FetchContent)
# FetchContent_Declare(json URL https://github.com/nlohmann/json/releases/download/v3.10.5/json.tar.xz)
# FetchContent_MakeAvailable(json)

# TODO: make this an option
find_package(ROOT REQUIRED COMPONENTS RooFit RooStats RooFitCore)
find_package(Boost 1.54 REQUIRED COMPONENTS filesystem program_options system)

link_libraries(${ROOT_LIBRARIES} ${Boost_LIBRARIES})
include_directories(${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR}/include)

add_executable(mu3eTrirecAnaMott src/MottTrirecAnalysis.cpp)
add_executable(mu3eTrirecAnaCosmic src/CosRunRecoAnalysis.cpp)
add_executable(CombinedRunHistos src/CombinedRunHistos.cpp)

set(TARGETS
        mu3eTrirecAnaMott
        mu3eTrirecAnaCosmic
        CombinedRunHistos
)

install(TARGETS ${TARGETS}
        DESTINATION bin
)