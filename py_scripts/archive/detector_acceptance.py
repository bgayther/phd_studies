import ROOT
import numpy as np
import itertools
import collections
from array import array

# Looking at signal events, mu->3e
DataTree = ROOT.TChain("mu3e")
DataTree.Add("run000042-sort.root")
DataTree_mchits = ROOT.TChain("mu3e_mchits")
DataTree_mchits.Add("run000042-sort.root")

N_beam_on = 10116 # Number of simulated decaying muons

def toList(vec):
    # Return list of elements from root vector
    return [i for i in vec]

def getLayer(ID):
    # Returns layer number given sensor ID
    if 0 <= ID < 1024:
        return 0
    if 1024 <= ID < 2048:
        return 1
    if 2000 <= ID < 3000 or 10000 <= ID < 11500 or 14000 <= ID < 15200:
        return 2
    if 3000 <= ID < 4000 or 11500 <= ID < 12500 or 15200 <= ID < 16500:
        return 3
    else:
        return None

mu3e_count  = 0.0
accepted = 0.0
accepted_L = 0.0
rejected = 0.0
rejected_L = 0.0
total_target_hits = 0.0
total_trajectories = 0.0
N_mu3e_trajectories = 0.0
cand_mc_ids = []
cand_pt_s = []
cand_p_s = []
cand_lambda_s = []
cand_all = []

output_file = ROOT.TFile("detEffOutput.root", "RECREATE")

S4_acc_tree = ROOT.TTree('S4_acc_tree', 'S4_acc_tree')
L6_acc_tree = ROOT.TTree('L6_acc_tree', 'L6_acc_tree')

S4_rej_tree = ROOT.TTree('S4_rej_tree', 'S4_rej_tree')
L6_rej_tree = ROOT.TTree('L6_rej_tree', 'L6_rej_tree')

br_acc_p_S4 = array('f', [0.0])
S4_acc_tree.Branch('br_acc_p_S4', br_acc_p_S4, 'br_acc_p_S4/F')
br_acc_lam_S4 = array('f', [0.0])
S4_acc_tree.Branch('br_acc_lam_S4', br_acc_lam_S4, 'br_acc_lam_S4/F')

br_rej_p_S4 = array('f', [0.0])
S4_rej_tree.Branch('br_rej_p_S4', br_rej_p_S4, 'br_rej_p_S4/F')
br_rej_lam_S4 = array('f', [0.0])
S4_rej_tree.Branch('br_rej_lam_S4', br_rej_lam_S4, 'br_rej_lam_S4/F')

br_acc_p_L6 = array('f', [0.0])
L6_acc_tree.Branch('br_acc_p_L6', br_acc_p_L6, 'br_acc_p_L6/F')
br_acc_lam_L6 = array('f', [0.0])
L6_acc_tree.Branch('br_acc_lam_L6', br_acc_lam_L6, 'br_acc_lam_L6/F')

br_rej_p_L6 = array('f', [0.0])
L6_rej_tree.Branch('br_rej_p_L6', br_rej_p_L6, 'br_rej_p_L6/F')
br_rej_lam_L6 = array('f', [0.0])
L6_rej_tree.Branch('br_rej_lam_L6', br_rej_lam_L6, 'br_rej_lam_L6/F')

# TH2Ds
NBINS_p = 50
NBINS_lam = 50
MIN_p = 0
MAX_p = 60
MIN_lam = -np.pi/2.0
MAX_lam = np.pi/2.0

h_acc_p_lam_S4 = ROOT.TH2D("h_acc_p_lam_S4", "", NBINS_lam, MIN_lam, MAX_lam, NBINS_p, MIN_p, MAX_p)
h_rej_p_lam_S4 = ROOT.TH2D("h_rej_p_lam_S4", "", NBINS_lam, MIN_lam, MAX_lam, NBINS_p, MIN_p, MAX_p)
h_acc_p_lam_L6 = ROOT.TH2D("h_acc_p_lam_L6", "", NBINS_lam, MIN_lam, MAX_lam, NBINS_p, MIN_p, MAX_p)
h_rej_p_lam_L6 = ROOT.TH2D("h_rej_p_lam_L6", "", NBINS_lam, MIN_lam, MAX_lam, NBINS_p, MIN_p, MAX_p)


N_entries = DataTree.GetEntries() #2

goodids = [] 
repeatedids = []

for entry in xrange(N_entries):
    DataTree.GetEntry(entry)  

    N_target_hits = DataTree.Ntargethits
    N_trajectories = DataTree.Ntrajectories
    traj_ID        = toList(DataTree.traj_ID)

    traj_mother = toList(DataTree.traj_mother)
    traj_PID    = toList(DataTree.traj_PID)
    traj_type   = toList(DataTree.traj_type)

    traj_vx = toList(DataTree.traj_vx)
    traj_vy = toList(DataTree.traj_vy)
    traj_vz = toList(DataTree.traj_vz)

    traj_px = toList(DataTree.traj_px)
    traj_py = toList(DataTree.traj_py)
    traj_pz = toList(DataTree.traj_pz)

    # Pixel info
    N_pixel_hits = DataTree.Nhit
    hit_pixelid = toList(DataTree.hit_pixelid)
    hit_mc_i = toList(DataTree.hit_mc_i)
    hit_mc_n = toList(DataTree.hit_mc_n)

    if traj_type.count(91) == 2 and traj_type.count(92) == 1 : # Count mu -> 3e
        mu3e_count += 1

    mc_ids = [] 
    p_ts = []
    p_s = []
    lambda_s = []
    cands = []
    newid = True
    for i in range(N_trajectories):    
        if (traj_type[i] == 91 or traj_type[i] == 92):
            if not(len(hit_mc_i) > i):
                continue

            # check if new traj id! Might have to move this to the counting loop?
            
            # for _id in goodids:
            #     if traj_ID[i] == _id:
            #         newid = False
            #         repeatedids.append(traj_ID[i])
            
            N_mu3e_trajectories += 1
            
            l_ambda = np.pi/2.0 - np.arctan2((traj_px[i]**2.0 + traj_py[i]**2.0)**0.5 , traj_pz[i])
            mc_ids.append(hit_mc_i[i])  
            p_s.append((traj_px[i]**2.0 + traj_py[i]**2.0 + traj_pz[i]**2.0)**0.5)
            p_ts.append((traj_px[i]**2.0 + traj_py[i]**2.0)**0.5) 
            lambda_s.append(l_ambda)

            sensor = hit_pixelid[i]>>16
            #alt_layer = hit_pixelid[i]>>26 
            layer = getLayer(sensor)
            
            cand = (hit_mc_i[i], l_ambda, (traj_px[i]**2.0 + traj_py[i]**2.0 + traj_pz[i]**2.0)**0.5, traj_vx[i], traj_vy[i], traj_vz[i], layer)
            cands.append(cand)


            # Can just loop over mchits here instead....

            # N.B. in digi.json:
            #   "trajectory" : {
            #     "momentumCutOff" : 5
            #    },



        else:
            continue
 
    total_target_hits += N_target_hits
    total_trajectories += N_trajectories
    cand_mc_ids = np.append(cand_mc_ids, mc_ids)
    cand_pt_s = np.append(cand_pt_s, p_ts)
    cand_p_s = np.append(cand_p_s, p_s)
    cand_lambda_s = np.append(cand_lambda_s, lambda_s)
    cand_all.append(cands)

# print repeatedids


cand_all_matched = [] # New list of tuples for matched candidates
cand_tids = [] # Will contain matched mchits tid
for cand in cand_all: 
    for i in cand:
        DataTree_mchits.GetEntry(i[0]) 

        track_id = DataTree_mchits.tid   
        det_id = DataTree_mchits.det
        hid = DataTree_mchits.hid

        if det_id == 1 and abs(hid) > 0: # Check for Si Layer hits only
            cand_tids.append(track_id)
            cand_all_matched.append(i+(hid,track_id,))

# del cand_all

all_tids = [] # Will contain ALL mchits tid
for entry in xrange(DataTree_mchits.GetEntries()):
    DataTree_mchits.GetEntry(entry)

    track_id = DataTree_mchits.tid
    det_id = DataTree_mchits.det
    hid = DataTree_mchits.hid

    if det_id == 1 and abs(hid) > 0: # Check for Si Layer hits only
        all_tids.append(track_id)

all_tid_counter = collections.Counter(all_tids)

for cand in cand_all_matched:
    for tid in all_tid_counter:
        if cand[-1] == tid:
            # TARGET: LENGTH: 50.0 mm, RADIUS: 19.0 mm from detector.json or config->GetString()
            if not(abs(cand[5]) < 50.0 and (cand[3]**2.0 + cand[4]**2.0)**0.5 < 19.0) :
                #cand_all_matched_S4.append(cand +(False,))
                #cand_all_matched_L6.append(cand +(False,))

                br_rej_lam_S4[0] = cand[1]
                br_rej_p_S4[0] = cand[2]
                h_rej_p_lam_S4.Fill(br_rej_lam_S4[0],br_rej_p_S4[0])
                br_rej_lam_L6[0] = cand[1]
                br_rej_p_L6[0] = cand[2]
                h_rej_p_lam_L6.Fill(br_rej_lam_L6[0],br_rej_p_L6[0])
                S4_rej_tree.Fill()
                L6_rej_tree.Fill()
                rejected += 1
                rejected_L += 1
                continue
            if all_tid_counter[tid] < 4:
                #cand_all_matched_S4.append(cand +(False,))
                br_rej_lam_S4[0] = cand[1]
                br_rej_p_S4[0] = cand[2]
                h_rej_p_lam_S4.Fill(br_rej_lam_S4[0],br_rej_p_S4[0])
                S4_rej_tree.Fill()
                rejected += 1
            if all_tid_counter[tid] >= 4:
                #cand_all_matched_S4.append(cand +(True,))
                br_acc_lam_S4[0] = cand[1]
                br_acc_p_S4[0] = cand[2]
                h_acc_p_lam_S4.Fill(br_acc_lam_S4[0], br_acc_p_S4[0])
                S4_acc_tree.Fill()
                accepted += 1
            if all_tid_counter[tid] < 6:
                #cand_all_matched_L6.append(cand +(False,))
                br_rej_lam_L6[0] = cand[1]
                br_rej_p_L6[0] = cand[2]
                h_rej_p_lam_L6.Fill(br_rej_lam_L6[0],br_rej_p_L6[0])
                L6_rej_tree.Fill()
                rejected_L += 1
            if all_tid_counter[tid] >= 6:
                #cand_all_matched_L6.append(cand +(True,))
                br_acc_lam_L6[0] = cand[1]
                br_acc_p_L6[0] = cand[2]
                h_acc_p_lam_L6.Fill(br_acc_lam_L6[0], br_acc_p_L6[0])
                L6_acc_tree.Fill()
                accepted_L += 1       

# del cand_all_matched

print "Total Mu->3e events: ", mu3e_count
print "Total target hits: ", total_target_hits
print "Total Mu->3e trajectories: ", N_mu3e_trajectories
print "Total no. of trajectories: ", total_trajectories
print "No. of simulated Mu decays (NBeamOn): ", N_beam_on
print "No. of accepted tracks S4: ", accepted
print "No. of rejected tracks S4: ", rejected
print "No. of accepted tracks L6: ", accepted_L
print "No. of rejected tracks L6: ", rejected_L
print "Detector acceptance S4: ", 100 * (accepted/(accepted+rejected)), " %"
print "Detector acceptance L6: ", 100 * (accepted_L/(accepted_L+rejected_L)), " %"



h_acc_p_lam_S4.Write()
h_rej_p_lam_S4.Write()
h_acc_p_lam_L6.Write()
h_rej_p_lam_L6.Write()
output_file.Write()
output_file.Close()