# export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/CMake/3.20.0/x86_64-centos7-gcc11-opt/bin/:$PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/vdt/0.4.3/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/tbb/2020_U2/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/Boost/1.77.0/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH

# source /cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0/x86_64-centos7/setup.sh
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_101/ROOT/6.24.06/x86_64-centos7-gcc11-opt/bin/thisroot.sh

## setup Geant:
# source /unix/muons/mu3e/Software/install/bin/geant4.sh

# python3
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_101/Python/3.9.6/x86_64-centos7-gcc11-opt/Python-env.sh

# cmake -DBOOST_ROOT=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/Boost/1.77.0/x86_64-centos7-gcc11-opt -Dnlohmann_json_DIR=/cvmfs/sft.cern.ch/lcg/releases/LCG_101/jsonmcpp/3.9.1/x86_64-centos7-gcc11-opt/lib64/cmake/nlohmann_json ..

# Updated
# LCG102
# export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/CMake/3.20.0/x86_64-centos7-gcc11-opt/bin/:$PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/vdt/0.4.3/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/tbb/2020_U2/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/Boost/1.78.0/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH

# export XERCESCROOT=/cvmfs/sft.cern.ch/lcg/releases/XercesC/3.2.3-714f6/x86_64-centos7-gcc11-opt
# export LD_LIBRARY_PATH=/cvmfs/sft.cern.ch/lcg/releases/XercesC/3.2.3-714f6/x86_64-centos7-gcc11-opt/lib:${LD_LIBRARY_PATH}

# source /cvmfs/sft.cern.ch/lcg/contrib/gcc/11/x86_64-centos7/setup.sh
# source /cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0/x86_64-centos7/setup.sh
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_102/ROOT/6.26.04/x86_64-centos7-gcc11-opt/bin/thisroot.sh

## setup Geant:
# source /unix/muons/mu3e/Software/install/bin/geant4.sh
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_102/Geant4/11.0.2/x86_64-centos7-gcc11-opt/Geant4-env.sh
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_102/Geant4/11.0.2/x86_64-centos7-gcc11-opt/bin/geant4.sh
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_102/Geant4/11.0.2/x86_64-centos7-gcc11-opt/share/Geant4-11.0.2/geant4make/geant4make.sh

# python3
# source /cvmfs/sft.cern.ch/lcg/releases/LCG_102/Python/3.9.12/x86_64-centos7-gcc11-opt/Python-env.sh

source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc11-opt
# source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_101 x86_64-centos7-gcc11-opt

# cmake -DBOOST_ROOT=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/Boost/1.78.0/x86_64-centos7-gcc11-opt -Dnlohmann_json_DIR=/cvmfs/sft.cern.ch/lcg/releases/LCG_102/jsonmcpp/3.10.5/x86_64-centos7-gcc11-opt/lib64/cmake/nlohmann_json ..