#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TH1D.h>
#include <TH2D.h>
#include <THStack.h>
#include <TObjString.h>
#include <TString.h>
#include <TKey.h>
#include <TList.h>
#include <TStopwatch.h>
#include <nlohmann/json.hpp>
#include <iostream>

using json = nlohmann::json;
using TH2D_ptr = TH2D*;
using TH1D_ptr = TH1D*;

int main(int argc, char** argv) {
    namespace po = boost::program_options;

    std::vector<std::string> inputFiles;
    std::string outputFile, outputGraphsDir;

    std::vector<TH1D_ptr> histos1D;
    std::vector<TH2D_ptr> histos2D;

    po::options_description opts;
    opts.add_options()
        ("inputFiles", po::value(&inputFiles)->multitoken(), "input reco root files")
        ("output", po::value(&outputFile), "output root file")
        ("outputGraphsDir", po::value(&outputGraphsDir), "output root file")
        ("help", "help");
    
    po::positional_options_description opts_p;
    opts_p.add("inputFiles", 1);
    opts_p.add("output", 1);
    opts_p.add("outputGraphsDir", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).positional(opts_p).run(), vm);
    po::notify(vm);

    if(vm.count("help") || inputFiles.empty() || outputFile.empty()) {
        printf("Usage: CombinedRunHistos [options] <inputFiles> <output>\n");
        opts.print(std::cout);
        return -1;
    }

    gROOT->ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h");
    gROOT->ForceStyle();
    
    TFile* outfile = new TFile((outputFile + ".root").c_str(), "recreate");
    if (!outputGraphsDir.empty()) {
        boost::filesystem::create_directory(outputGraphsDir);
        outputGraphsDir.append("/");
    } else boost::filesystem::create_directory("graphs");

    // gStyle->SetOptStat("emr");
    // gStyle->SetPalette(57);
    // gStyle->SetStatY(0.9);
    // gStyle->SetStatX(0.9);
    // gStyle->SetStatW(0.4);
    // gStyle->SetStatH(0.2);
    gStyle->SetOptStat(0);
    gROOT->SetBatch(true);

    TStopwatch watch;
    ROOT::EnableImplicitMT();

    watch.Reset();
    watch.Start();

    std::vector<std::string> histosToSave;
    histosToSave.push_back("chipID");
    histosToSave.push_back("fpgaID");
    histosToSave.push_back("tDiff");
    histosToSave.push_back("tsdiffChipID");
    histosToSave.push_back("pixScintTimeCorr");
    histosToSave.push_back("pixScintTimeDiff");
    histosToSave.push_back("pixScintSubHeaderTimeDiff");
    histosToSave.push_back("ToTVsDelayTime");
    histosToSave.push_back("delayTimeVsRow");
    histosToSave.push_back("hHitTimeDiff41");
    histosToSave.push_back("hHitTimeDiff42");
    histosToSave.push_back("hHitTimeDiff43");
    histosToSave.push_back("hHitTimeDiff31");
    histosToSave.push_back("hHitTimeDiff32");
    histosToSave.push_back("hHitTimeDiff21");
    histosToSave.push_back("deltaT");
    histosToSave.push_back("deltaT2");

    for (int i = 0; i <= 113; i++) {
        auto chipID = std::to_string(i);
        std::string hitmap_name = "chip" + chipID + "_hitmap";
        std::string time_name = "chip" + chipID + "_ts";
        histosToSave.push_back(hitmap_name);
        histosToSave.push_back(time_name);
    }

    for (auto& file : inputFiles) {
        
        TFile* rfile = TFile::Open(file.c_str(), "READ");
        if (!rfile || rfile->IsZombie()) continue;

        for (auto& histName : histosToSave) {

            if (auto key = rfile->GetKey(histName.c_str())) {
                std::string className(key->GetClassName());
                auto h = rfile->Get(histName.c_str());
                if (className.find("TH1") != std::string::npos) {
                    TH1D* h1d = (TH1D*)h;
                    h1d->SetDirectory(0);
                    histos1D.emplace_back(h1d);
                } else if (className.find("TH2") != std::string::npos) {
                    TH2D* h2d = (TH2D*)h;
                    h2d->SetDirectory(0);
                    histos2D.push_back(h2d);
                }

            }
        }
        rfile->Close();
    }

    outfile->cd();
    for (auto iti = histos1D.begin(); iti != histos1D.end(); ++iti) {
        auto h1 = *iti;

        std::string h1Name = (std::string) h1->GetName();
        for (auto itj = std::next(iti); itj != histos1D.end();) {
            auto h2 = *itj;

            std::string h2Name = (std::string) h2->GetName();
            if (h1Name == h2Name) {
                h1->Add(h2);
                itj = histos1D.erase(itj);
            } else ++itj;
        }
        if (h1->GetEntries()==0) continue;

        if (h1Name.find("pixScintTimeDiff") != std::string::npos) {
            h1->GetXaxis()->SetTitle("T_{Pixel} - T_{Trigger} [ns]");
            h1->GetYaxis()->SetTitle("Counts");
        } else if (h1Name.find("deltaT") != std::string::npos) {
            h1->SetTitle("");
            auto ax = h1->GetXaxis();
            ax->SetTitle("T_{Pixel} - T_{SciFi} (with external trigger) [ns]");
            ax->Set(ax->GetNbins(), (ax->GetXmin()-100)*1.6, (ax->GetXmax()-100)*1.6);
            ax->SetRangeUser(-1000, 1000);
            h1->GetYaxis()->SetTitle("Counts");
            h1->GetYaxis()->SetRangeUser(0, 30);
        }

        h1->Write();
        auto can = TCanvas();
        h1->Draw();

        // auto tex = TLatex();
        // tex.SetNDC();
        // tex.SetTextSize(0.035);
        // tex.SetTextAlign(12);
        // tex.SetTextFont(42);
        // tex.DrawLatex(0.45, 0.85, "#bf{Mu3e} Integration Run 2022 #it{Preliminary}");

        std::string name = h1->GetName();
        auto can_name = outputGraphsDir + name + ".pdf";
        can.Modified(); 
        can.Update();
        can.SaveAs(can_name.c_str());
    }

    for (auto iti = histos2D.begin(); iti != histos2D.end(); ++iti) {
        auto h1 = *iti;

        std::string h1Name = (std::string) h1->GetName();
        for (auto itj = std::next(iti); itj != histos2D.end();) {
            auto h2 = *itj;

            std::string h2Name = (std::string) h2->GetName();
            if (h1Name == h2Name) {
                h1->Add(h2);
                itj = histos2D.erase(itj);
            } else ++itj;
        }

        if (h1->GetEntries()==0) continue;
        auto can = TCanvas();
        if (h1Name.find("hitmap") != std::string::npos) {
            h1->GetXaxis()->SetTitle("Column");
            h1->GetYaxis()->SetTitle("Row");
        }
        h1->Draw("colz");

        std::string name = h1->GetName();
        auto can_name = outputGraphsDir + name + ".pdf";
        can.Modified(); 
        can.Update();
        can.SaveAs(can_name.c_str());
        h1->Write();
    }

    watch.Stop();
    std::cout << "The code took " << watch.RealTime() << " seconds." << std::endl;

    outfile->Write();
    outfile->Close();
    return 0;
}