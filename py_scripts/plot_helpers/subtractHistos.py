import ROOT
ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/cpp_scripts/include/RootColourDefs.hpp")
colours = ROOT.RootColourDefs()

def subtractHisto(dir, input1, input2):
    rf1 = ROOT.TFile.Open(input1)
    rf2 = ROOT.TFile.Open(input2)

    keys1 = rf1.GetListOfKeys()
    keys2 = rf1.GetListOfKeys()

    outfile = ROOT.TFile(dir+"diff.root", 'RECREATE')

    for key1 in keys1:
        for key2 in keys2:
            if (key1.GetName() == key2.GetName() and key1.GetClassName() == key2.GetClassName() and key1.GetClassName() == "TH1D"): 
                # print(key1.GetName(), key2.GetName(), key1.GetClassName(), key2.GetClassName(), type(key1))
                h1 = rf1.Get(key1.GetName())
                h2 = rf2.Get(key2.GetName())
                
                h1.Add(h2, -1)
                h1.SetLineColor(colours.kC0)
                h1.Write()

                can = ROOT.TCanvas()
                h1.Draw()
                can.SaveAs(dir+"diff_graphs/"+key1.GetName()+".pdf")

    outfile.Write()
    outfile.Close()

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h")

    dir = "/unix/muons/users/bgayther/MottScattering/anaOutput/"
    subtractHisto(dir, dir+"mottAna_52MeV_mott_gen_Carbon_250keV_mom_spread_52_MeV.root", dir+"mottAna_52MeV_standard_250keV_mom_spread_52_MeV.root")