#!/bin/bash
###################################################
### Shell script sent to the grid to run     ######
###     03/12/2020   A. Groves               ######
###################################################
set -x
echo "What node is the job running on"
hostname
echo " "
echo "What files are put into the sandbox" 
ls 
echo " "
echo "Is the software cvmfs location seen"
ls /cvmfs/gridpp.egi.eu/Mu3e
echo " "
echo "Copying files from $MU3EPATH"
echo "Storage location to be saved to is $StorageLocation"
SEED=$(printf "%06d" ${seed})
echo "Random seed is $SEED"


# Set up links to run/ dir, but treat data/ special
# cp -r  $MU3EPATH/mu3e/run/*.{csv,sh,conf,bin} .
# if [ ! -f "./detector.json" ]; then
#     echo "Copying detector.json from cvmfs build"
#     cp $MU3EPATH/mu3e/run/detector.json .
# fi
# cp $MU3EPATH/mu3e/run/rosim_digi.json .
# find . -name data -type l -delete
# find . -name run1.mac -type l -delete
# mkdir -p data
# mkdir -p results
# mkdir -p skims

#####  Set up singularity weather to use CVMFS or local one 
SINGULARITY_OPTS=""
if [ -x /usr/bin/singularity ]; then
    echo "Using local in /user"
    SINGULARITY=singularity
elif [ `sysctl -n user.max_user_namespaces` -gt 100]; then
    echo "Using CVMFS version of singularity"
   SINGULARITY="/cvmfs/oasis.opensciencegrid.org/mis/singularity/current/bin/singularity"
   SINGULARITY_OPTS="--userns"  
else
    echo "Cant find singularity"
fi

INPUT_FILE=mu3e_run_${SEED}.root
$SINGULARITY exec $SINGULARITY_OPTS $MU3EPATH /bin/sh ${Executable} ${INPUT_FILE} #<----- Running singularity

echo "Files now in the sandbox"
ls

### Linking to a root version and skims on parameters set in SkimTree
# source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.09.02/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
# inFile="./data/run"${SEED}"-ana_vertexfit.root"
# outFile="skim_"${SEED}".root"
# root.exe -b -l -q 'SkimTree.cpp("'${inFile}'","'${outFile}'","'${SEED}'")'

###  Add the files to dirac file catologe
# source /cvmfs/dirac.egi.eu/dirac/bashrc_gridpp #<- Need to link to the dirac-api
#dirac-dms-add-file $StorageLocation/digi.json digi.json UKI-NORTHGRID-LIV-HEP-disk -ddd #<- Copies the digi.json to the storage location (-ddd is debug mode (dont remove))

if [ -f "./signalnorm.txt" ]; then
    echo "IC_norm done"
    dirac-dms-add-file LFN:$StorageLocation/signalnorm_${SEED}.txt signalnorm.txt UKI-NORTHGRID-LIV-HEP-disk
    # dirac-dms-add-file LFN:$StorageLocation/IC_norm_${SEED}.txt IC_norm_${SEED}.txt UKI-NORTHGRID-LIV-HEP-disk -ddd
fi

# Setup LCG
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc11-opt

# Setup dirac
source /cvmfs/dirac.egi.eu/dirac/bashrc