#ifndef UTIL_ROOT_COLOUR_DEFS_HPP_
#define UTIL_ROOT_COLOUR_DEFS_HPP_

#include <TColor.h>

class RootColourDefs {
    public:
        // Matplotlib default colour set
        Int_t kC0;
        TColor* new_col_c0;

        Int_t kC1;
        TColor* new_col_c1;

        Int_t kC2;
        TColor* new_col_c2;

        Int_t kC3;
        TColor* new_col_c3;

        Int_t kC4;
        TColor* new_col_c4;

        Int_t kC5;
        TColor* new_col_c5;

        Int_t kC6;
        TColor* new_col_c6;

        Int_t kC7;
        TColor* new_col_c7;

        Int_t kC8;
        TColor* new_col_c8;

        Int_t kC9;
        TColor* new_col_c9;

        RootColourDefs() {
            kC0 = TColor::GetFreeColorIndex();
            new_col_c0 = new TColor(kC0, 0.12156862745098039, 0.4666666666666667, 0.7058823529411765, "C0");

            kC1 = TColor::GetFreeColorIndex();
            new_col_c1 = new TColor(kC1, 1.0, 0.4980392156862745, 0.054901960784313725, "C1");

            kC2 = TColor::GetFreeColorIndex();
            new_col_c2 = new TColor(kC2, 0.17254901960784313, 0.6274509803921569, 0.17254901960784313, "C2");

            kC3 = TColor::GetFreeColorIndex();
            new_col_c3 = new TColor(kC3, 0.8392156862745098, 0.15294117647058825, 0.1568627450980392, "C3");

            kC4 = TColor::GetFreeColorIndex();
            new_col_c4 = new TColor(kC4, 0.5803921568627451, 0.403921568627451, 0.7411764705882353, "C4");

            kC5 = TColor::GetFreeColorIndex();
            new_col_c5 = new TColor(kC5, 0.5490196078431373, 0.33725490196078434, 0.29411764705882354, "C5");

            kC6 = TColor::GetFreeColorIndex();
            new_col_c6 = new TColor(kC6, 0.8901960784313725, 0.4666666666666667, 0.7607843137254902, "C6");

            kC7 = TColor::GetFreeColorIndex();
            new_col_c7 = new TColor(kC7, 0.4980392156862745, 0.4980392156862745, 0.4980392156862745, "C7");

            kC8 = TColor::GetFreeColorIndex();
            new_col_c8 = new TColor(kC8, 0.7372549019607844, 0.7411764705882353, 0.13333333333333333, "C8");

            kC9 = TColor::GetFreeColorIndex();
            new_col_c9 = new TColor(kC9, 0.09019607843137255, 0.7450980392156863, 0.8117647058823529, "C9");
        };

        ~RootColourDefs() {
            // delete new_col_c0, new_col_c1, new_col_c2, new_col_c3, new_col_c4, new_col_c5, new_col_c6, new_col_c7, new_col_c8, new_col_c9;
            // printf("dtor called\n");
        };
};

#endif /* UTIL_ROOT_COLOUR_DEFS_HPP_ */
