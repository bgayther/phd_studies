#ifndef UTIL_ROOT_HELPERS_HPP_
#define UTIL_ROOT_HELPERS_HPP_

class RootHelpers {
    public:

        static TPaveText* CreatePaveText(double x1, double y1, double x2, double y2, std::vector<TString> text, double textsize){
            TPaveText *tex = new TPaveText();
            tex->SetFillColor(0);
            tex->SetTextSize(0.05);
            tex->SetFillStyle(0);
            tex->SetBorderSize(0);
            int n = text.size();
            // for(int i=n-1;i>=0;i--) tex->AddText(text[i].Data());
            for (int i = 0; i < n; i++)
                tex->AddText(text[i].Data());
            tex->SetX1NDC(x1);
            tex->SetY1NDC(y1);
            tex->SetX2NDC(x2);
            tex->SetY2NDC(y2);
            tex->SetTextSize(textsize);
            return tex;
        }

        static void GetX1Y1X2Y2(TVirtualPad* c, double &x1, double &y1, double &x2, double &y2) {
            x1 = c->GetFrame()->GetX1() + c->GetLeftMargin();
            y1 = c->GetFrame()->GetY1() + c->GetBottomMargin();
            x2 = c->GetFrame()->GetX2() - c->GetRightMargin();
            y2 = c->GetFrame()->GetY2() - c->GetTopMargin();
        }

        static TLegend* FastLegend(double x_low, double y_low, double x_up, double y_up, double textsize) {
            TLegend *legend = new TLegend(x_low, y_low, x_up, y_up);
            // legend->SetBorderSize(0);
            legend->SetTextSize(textsize);
            legend->SetFillStyle(0);
            legend->SetFillColor(0);
            legend->SetLineColor(0);
            return legend;
        }

        static void checkFiles(std::vector<std::string>& inputFiles, const std::string& treeName) {
            auto it = std::begin(inputFiles);
            while (it != std::end(inputFiles)) {
                // std::cout << *it << std::endl;
                auto tf = TFile::Open((*it).c_str());
                auto tree = tf->Get<TTree>(treeName.c_str());
                if (!tf || !tree) {
                    std::cout << "ERASING\n";
                    it = inputFiles.erase(it);
                    tf->Close();
                }
                else {
                    ++it;
                    // printf("Tree entries %d \n", tree->GetEntries());
                    tf->Close();
                }
            }
        }

        RootHelpers() {};
        ~RootHelpers() {};
};

#endif /* UTIL_ROOT_HELPERS_HPP_ */
