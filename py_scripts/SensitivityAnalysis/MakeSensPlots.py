import ROOT
import sys
import os
import argparse
import pickle

sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers
from combineTH1 import overlayMultipleHistos

# Create class to extract histograms from each file, scale them, and add them to a list
class HistExtractor:
    # loop over root file and extract histograms
    def __init__(self, file, scale_factor):
        self.file = file
        self.scale_factor = scale_factor
        self.hists = []

        for key in self.file.GetListOfKeys():
            # Check if key is a histogram
            if not key.GetClassName().startswith("TH"):
                continue
            hist = key.ReadObj()
            hist.Scale(self.scale_factor)
            self.hists.append(hist)
            
    # get hist by name
    def get_hist(self, name):
        for hist in self.hists:
            if hist.GetName() == name:
                return hist
        # if not found return error
        print(f"Error: {name} not found in {self.file.GetName()}")
        return None

def calc_sig_weight(NStopsNorm, SignalBR, NSig):
    return (NStopsNorm * SignalBR) / NSig

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Make Sensitivity Plots')
    # For sig, IC, bhabha
    parser.add_argument('--SigFile', type=str, required=True, help='Signal file')
    parser.add_argument('--ICFile', type=str, required=True, help='IC file')
    parser.add_argument('--BhabhaFile', type=str, required=True, help='Bhabha file')
    parser.add_argument('--nSigFiles', type=int, required=True, help='Number of signal files')
    parser.add_argument('--nICFiles', type=int, required=True, help='Number of IC files')
    parser.add_argument('--nBhabhaFiles', type=int, required=True, help='Number of Bhabha files')
    parser.add_argument('--weightFile', type=str, required=True, help='weights pickle file')
    parser.add_argument('--output', '-o', type=str, default="plots.root", help='Output file')
    parser.add_argument('--outputDir', '-od', type=str, default="plots", help='Output directory')
    parser.add_argument('--NStopsNorm', type=float, default=1e15, required=False, help='Number of muon stops to normalize to')
    args = parser.parse_args()

    ROOT.gROOT.SetBatch(True)

    print("Making Sensitivity Plots")

    print(f"N sig files: {args.nSigFiles}, N IC files: {args.nICFiles}, N Bhabha files: {args.nBhabhaFiles}")

    # Get histograms from input files
    SigFile = ROOT.TFile(args.SigFile)
    ICFile = ROOT.TFile(args.ICFile)
    BhabhaFile = ROOT.TFile(args.BhabhaFile)

    # Read .pickle file from args.weights
    weights = pickle.load(open(args.weightFile, "rb"))

    IC_weight_sum = weights["IC_weight_sum"]
    n_bhabha_frames_skim = weights["n_bhabha_frames_skim"]
    n_signal_events = weights["n_signal_events"]
    n_bhabha_files = weights["n_bhabha_files"]
    n_IC_files = weights["n_IC_files"]
    n_sig_files = weights["n_sig_files"]

    NStopsNorm = args.NStopsNorm

    bhabha_frac = 7.7e-5
    timing_suppression = 70
    avg_bhabha_weight_per_frame = 4.502925517058626e-06 # pre skim (adjusting for different framelengths/beam rates...)
    skim_frame_weight = n_bhabha_frames_skim * avg_bhabha_weight_per_frame
    total_frame_weight = n_bhabha_files * 100000 * avg_bhabha_weight_per_frame
    bhabha_weight = (NStopsNorm * bhabha_frac) / (skim_frame_weight * timing_suppression)
    print(f"Bhabha weight: {bhabha_weight}")

    sim_IC_BR = 2.486041e-06
    IC_weight = (NStopsNorm * sim_IC_BR * 3.4e-5) / IC_weight_sum
    print(f"IC weight: {IC_weight}")

    # print(f"n_frames_skimmed: {n_frames_skimmed:e}, TotalWeight: {TotalWeight:e}")
    NSigFactor = 1.0/args.nSigFiles
    NICFactor = IC_weight/args.nICFiles
    NBhabhaFactor = bhabha_weight/args.nBhabhaFiles
    print(f"NSigFactor: {NSigFactor:e}, NICFactor: {NICFactor:e}, NBhabhaFactor: {NBhabhaFactor:e}")
    
    # Make the output file
    output_file = ROOT.TFile(args.output, 'RECREATE')

    # Extract histograms from each file
    SigExtractor = HistExtractor(SigFile, NSigFactor)
    ICExtractor = HistExtractor(ICFile, NICFactor)
    BhabhaExtractor = HistExtractor(BhabhaFile, NBhabhaFactor)

    # Save histograms to output file
    for sig_hist, IC_hist, bhabha_hist in zip(SigExtractor.hists, ICExtractor.hists, BhabhaExtractor.hists):
        sig_hist.Write()
        IC_hist.Write()
        bhabha_hist.Write()

    strengths = [1e-12, 1e-13, 1e-14, 1e-15]

    def get_list_of_plots(SigExtractor, ICExtractor, var, BhabhaExtractor=None, NoCut=False):
        sig_title = f"h_{var}_Sig"
        IC_title = f"h_{var}_IC"
        bhabha_title = f"h_{var}_BhabhaMichel"
        if NoCut:
            # append "_no_cut" to all titles
            sig_title += "_no_cut"
            IC_title += "_no_cut"
            bhabha_title += "_no_cut"

        # Need to duplicate main sig_hist and scale by different factors
        sig_hist_base = SigExtractor.get_hist(sig_title).Clone()

        sig_hists = []
        for strength in strengths:
            sig_hist = sig_hist_base.Clone()
            sig_hist.Scale(calc_sig_weight(NStopsNorm, strength, n_signal_events))
            # Set title to include strength (remove 1 and - from strength, so 1e-12 becomes e12)
            sig_hist.SetTitle(f"{sig_hist.GetTitle()}_{str(strength).replace('1e', 'e').replace('-', '')}")
            sig_hists.append(sig_hist)        
        
        IC_hist = ICExtractor.get_hist(IC_title)
        bhabha_hist = BhabhaExtractor.get_hist(bhabha_title) if BhabhaExtractor else None
        hists = [IC_hist] + [bhabha_hist] + sig_hists if bhabha_hist else [IC_hist] + sig_hists

        return hists

    # Set fill pattern for all IC histograms
    for hist in ICExtractor.hists:
        hist.SetFillStyle(3002)
        hist.SetFillColor(ROOT.kC0)

    # Set fill pattern for all Bhabha histograms
    for hist in BhabhaExtractor.hists:
        hist.SetFillStyle(3002)
        hist.SetFillColor(ROOT.kC1)

    # Step through h_mmass_BhabhaMichel and get y-value of each bin (with content > 0)
    h_mmass_bhb = BhabhaExtractor.get_hist("h_mmass_BhabhaMichel")
    for i in range(1, h_mmass_bhb.GetNbinsX()+1):
        if h_mmass_bhb.GetBinContent(i) > 0:
            print(f"Bin {i}: {h_mmass_bhb.GetBinContent(i)}")

    IC_legend = "#mu #rightarrow eee#nu#nu"
    Sig_base_legend = "#mu #rightarrow eee"
    Bhabha_legend = "e^{+}e^{-} + e^{+}"

    # Signal strength should be 10^{-12}, 10^{-13}, 10^{-14}, 10^{-15}
    IC_and_Sig_legend = [IC_legend] + [Sig_base_legend + " (10^{{{}}})".format(e) for e in range(-12, -16, -1)]
    All_legend = [IC_legend] + [Bhabha_legend] + [Sig_base_legend + " (10^{{{}}})".format(e) for e in range(-12, -16, -1)]

    # TODO: Draw IC background with a fill color

    # Save plots
    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "pm"),
                          IC_and_Sig_legend,
                          (0.65, 0.65, 0.85, 0.85),
                          ";p_{eee} [MeV/#it{c}];Counts",
                          f"{args.outputDir}/pm", logy=True, drawStyle="NOSTACK HIST E", xmin=-1, xmax=15, legStyle="LE"
                          )

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "chi2"),
                          IC_and_Sig_legend,
                          (0.65, 0.65, 0.85, 0.85),
                          ";#chi^{2};Counts",
                          f"{args.outputDir}/chi2", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", xmin=0, xmax=16
                          )

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "chi2", BhabhaExtractor=BhabhaExtractor),
                          All_legend,
                          (0.65, 0.65, 0.85, 0.85),
                          ";#chi^{2};Counts",
                          f"{args.outputDir}/chi2_all", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", xmin=0, xmax=16
                          )

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "chi2", BhabhaExtractor=BhabhaExtractor, NoCut=True),
                          All_legend,
                          (0.65, 0.65, 0.85, 0.85),
                          ";#chi^{2};Counts",
                          f"{args.outputDir}/chi2_all_no_cut", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", xmin=0, xmax=16
                          )

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "mmass"),
                          IC_and_Sig_legend,
                          (0.75, 0.65, 0.95, 0.85),
                          ";m_{eee} [MeV/#it{c}^{2}];Counts",
                          f"{args.outputDir}/mmass", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", ymin=6e-5, ymax=1e2, openFrame=True
                          ) # ymin=6e-5, ymax=1e2
    
    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "mmass", BhabhaExtractor=BhabhaExtractor),
                          All_legend,
                          (0.75, 0.65, 0.95, 0.85),
                          ";m_{eee} [MeV/#it{c}^{2}];Counts",
                          f"{args.outputDir}/mmass_all", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", ymin=6e-5, ymax=1e2, openFrame=True
                          ) # ymin=6e-5, ymax=1e2

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "mmass", BhabhaExtractor=BhabhaExtractor, NoCut=True),
                          All_legend,
                          (0.75, 0.65, 0.95, 0.85),
                          ";m_{eee} [MeV/#it{c}^{2}];Counts",
                          f"{args.outputDir}/mmass_all_no_cut", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", openFrame=True, ymin=6e-5, ymax=1e2
                          ) # ymin=6e-5, ymax=1e2

    overlayMultipleHistos(get_list_of_plots(SigExtractor, ICExtractor, "targetdist"),
                          IC_and_Sig_legend,
                          (0.65, 0.65, 0.85, 0.85),
                          ";Target distance [mm];Counts",
                          f"{args.outputDir}/targetdist", logy=True, drawStyle="NOSTACK HIST E", legStyle="LE", xmin=0, xmax=20
                          )

    c = ROOT.TCanvas()
    h_pm_mmass_IC = ICExtractor.get_hist("h_pm_mmass_IC")
    h_pm_mmass_IC.SetMarkerColor(ROOT.kC0)
    h_pm_mmass_IC.Draw("")
    c.SaveAs(f"{args.outputDir}/pm_vs_mmass_IC.pdf")

    # c = ROOT.TCanvas()
    # h_pm_mmass_Bhabha = BhabhaExtractor.get_hist("h_pm_mmass_Bhabha_no_cut")
    # h_pm_mmass_Bhabha.Draw("colz")
    # c.SaveAs(f"{args.outputDir}/pm_vs_mmass_Bhabha.pdf")
    # # Need to find bin numbers for x (95, 110) and y (0, 4)
    # binx1 = h_pm_mmass_Bhabha.GetXaxis().FindBin(95) 
    # binx2 = h_pm_mmass_Bhabha.GetXaxis().FindBin(110) 
    # biny1 = h_pm_mmass_Bhabha.GetYaxis().FindBin(0) 
    # biny2 = h_pm_mmass_Bhabha.GetYaxis().FindBin(4)
    # print(f"Number of Bhabha events with p_eee < 4 MeV/c (with no cuts): {h_pm_mmass_Bhabha.Integral(binx1, binx2, biny1, biny2)}")

    # Save output file
    output_file.Write()
    output_file.Close()
