SEED_START=1
NSEEDS=2

vertexFilesIC=""
vertexFilesSig=""
vertexFilesBhabha=""
genFilesSig=""

for ((seed=$SEED_START; seed<$SEED_START+$NSEEDS; seed++))
do
    SEED=$(printf "%06d" $seed)
    vertexFilesIC="$vertexFilesIC root://hepgrid11.ph.liv.ac.uk//dpm/ph.liv.ac.uk/home/mu3e.org/mu3e.org/prod/build4.4.3/IC/run${SEED}-ana_vertexfit.root"
    vertexFilesSig="$vertexFilesSig root://hepgrid11.ph.liv.ac.uk//dpm/ph.liv.ac.uk/home/mu3e.org/mu3e.org/prod/build4.4.3/Signal/run${SEED}-ana_vertexfit.root"
    genFilesSig="$genFilesSig root://hepgrid11.ph.liv.ac.uk//dpm/ph.liv.ac.uk/home/mu3e.org/mu3e.org/prod/build4.4.3/Signal/signalnorm_${SEED}.txt"
done

SEED_START=500000
NSEEDS=5

for ((seed=$SEED_START; seed<$SEED_START+$NSEEDS; seed++))
do
    SEED=$(printf "%06d" $seed)
    vertexFilesBhabha="$vertexFilesBhabha root://hepgrid11.ph.liv.ac.uk//dpm/ph.liv.ac.uk/home/mu3e.org/mu3e.org/prod/build4.4.3/BhabbaMichel/skim_SigAna_${SEED}.root"
done

# echo $vertexFilesIC
# echo $vertexFilesSig
# echo $genFilesSig

echo "Prepared files"
python SensitivityHistos.py -vertexFilesBhabha $vertexFilesBhabha -output "Bhabha"
# python SensitivityHistos.py -vertexFilesIC $vertexFilesIC -output "IC"
# python SensitivityHistos.py -vertexFilesSig $vertexFilesSig -genFilesSig $genFilesSig -output "Signal"