#ifndef UTIL_MATH_HELPERS_HPP_
#define UTIL_MATH_HELPERS_HPP_

class MathHelpers {
    public:

        template <typename T> static std::vector<T> linspace(const T start, const T end, const size_t N_steps) {
            T step_size = (end - start) / static_cast<T>(N_steps-1);
            std::vector<T> xs(N_steps);
            typename std::vector<T>::iterator x;
            T val;
            for (x = xs.begin(), val = start; x != xs.end(); ++x, val += step_size)
                *x = val;
            return xs;
        }

        MathHelpers() {};
        ~MathHelpers() {};
};

#endif /* UTIL_MATH_HELPERS_HPP_ */
