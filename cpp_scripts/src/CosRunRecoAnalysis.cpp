#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RVec.hxx>
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TEfficiency.h>
#include <TGraphAsymmErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <THStack.h>
#include <TObjString.h>
#include <TString.h>
#include <TKey.h>
#include <TStopwatch.h>
#include <nlohmann/json.hpp>
#include "RootColourDefs.hpp"

Int_t CUT_COL;
Int_t NON_CUT_COL;

using std::cout;
using std::endl;
using std::cin;
using json = nlohmann::json;
using TH2D_ptr = ROOT::RDF::RResultPtr<TH2D>;
using TH1D_ptr = ROOT::RDF::RResultPtr<TH1D>;

struct plotInfo {
    std::string name = "";
    std::string title = "";
    std::string xtitle = "";
    std::string ytitle = "";

    int xbins = 0;
    double xmin = 0.0;
    double xmax = 0.0;

    int ybins = 0;
    double ymin = 0.0;
    double ymax = 0.0;

    std::string varX = "";
    std::string varY = "";

    int segType = -1;

    std::string style = "";

    std::string outputGraphsDir = "";

    std::string cut = "";

    void reset() {
        name = "";
        title = "";
        xtitle = "";
        ytitle = "";
        xbins = 0;
        xmin = 0.0;
        xmax = 0.0;
        ybins = 0;
        ymin = 0.0;
        ymax = 0.0;
        varX = "";
        varY = "";
        cut = "";
        style = "";
        segType = -1;
    }

    void set2DInfo(std::string name_
    , std::string title_
    , std::string xtitle_
    , std::string ytitle_
    , int xbins_
    , double xmin_
    , double xmax_
    , int ybins_
    , double ymin_
    , double ymax_
    , std::string varX_
    , std::string varY_
    ) {
        reset();
        name = name_;
        title = title_;
        xtitle = xtitle_;
        ytitle = ytitle_;
        xbins = xbins_;
        xmin = xmin_;
        xmax = xmax_;
        ybins = ybins_;
        ymin = ymin_;
        ymax = ymax_;
        varX = varX_;
        varY = varY_;
    }

    void set1DInfo(std::string name_
    , std::string title_
    , std::string xtitle_
    , std::string ytitle_
    , int xbins_
    , double xmin_
    , double xmax_
    , std::string varX_
    ) {
        reset();
        name = name_;
        title = title_;
        xtitle = xtitle_;
        ytitle = ytitle_;
        xbins = xbins_;
        xmin = xmin_;
        xmax = xmax_;
        varX = varX_;
    }

    void setName(std::string name_)   {name = name_;}
    void setTitle(std::string title_) {title = title_;}
    void setXTitle(std::string xtitle_) {xtitle = xtitle_;}
    void setYTitle(std::string ytitle_) {ytitle = ytitle_;}
    void setStyle(std::string style_) {style = style_;}
    void setSegType(int segType_) {segType = segType_;}
    void setOutputGraphsDir(std::string outputGraphsDir_) {outputGraphsDir = outputGraphsDir_;}
    void setCut(std::string cut_) {cut = cut_;}
    std::string getSegType() const {
        if (segType == 4) return "S4";
        else if (segType == 3) return "S3";
        else if (segType == 6) return "L6";
        else if (segType == 8) return "L8";
        else return "";
    }
};

struct cutPlotInfo {
    std::string cut;
    std::string title;
    std::string name;
    std::vector<TH1D_ptr> histos1D;
    std::vector<TH2D_ptr> histos2D;
    cutPlotInfo(std::string cut_, std::string title_, std::string name_) : 
        cut(cut_), title(title_), name(name_) {};
};

void plot1D(const ROOT::RDF::RNode& RDF, const plotInfo& plot, std::vector<TH1D_ptr>& histos) {
    std::string segType = plot.getSegType();
    std::string segTypeSuffix = "";

    ROOT::RDF::RNode data(RDF);

    if (!segType.empty()) segTypeSuffix = "_" + segType;

    TH1D_ptr hist;
    // TODO: see if nicer way...
    if (plot.cut != "") {
        hist = data.Filter(plot.cut).Histo1D({(plot.name + segTypeSuffix).c_str(), (segType + plot.title + ";" + 
                                plot.xtitle + ";" + plot.ytitle).c_str(), 
                                plot.xbins, plot.xmin, plot.xmax}, plot.varX);
        // cout << hist->GetName() << " " << hist->GetTitle() << endl;
    } else {
        hist = data.Histo1D({(plot.name + segTypeSuffix).c_str(), (segType + plot.title + ";" + 
                                plot.xtitle + ";" + plot.ytitle).c_str(), 
                                plot.xbins, plot.xmin, plot.xmax}, plot.varX);
    }

    histos.push_back(hist);
    return;
}

void plot2D(const ROOT::RDF::RNode& RDF, const plotInfo& plot, std::vector<TH2D_ptr>& histos) {
    std::string segType = plot.getSegType();
    std::string segTypeSuffix = "";
    ROOT::RDF::RNode data(RDF);

    if (!segType.empty()) segTypeSuffix = "_" + segType;

    TH2D_ptr hist;
    
    if (plot.cut != "") {
        hist = data.Filter(plot.cut).Histo2D({(plot.name + segTypeSuffix).c_str(), (segType + plot.title + ";" + 
                                plot.xtitle + ";" + plot.ytitle).c_str(), plot.xbins, plot.xmin, plot.xmax, 
                                plot.ybins, plot.ymin, plot.ymax}, plot.varX, plot.varY);
    } else {
        hist = data.Histo2D({(plot.name + segTypeSuffix).c_str(), (segType + plot.title + ";" + 
                                plot.xtitle + ";" + plot.ytitle).c_str(), plot.xbins, plot.xmin, 
                                plot.xmax, plot.ybins, plot.ymin, plot.ymax}, plot.varX, plot.varY);
    }

    histos.push_back(hist);
    return;
}

std::string determineSegType(const std::string& input) {
    std::string segType = "";
    if (input.find("S4") != std::string::npos) segType = "S4";
    else if (input.find("S3") != std::string::npos) segType = "S3";
    else if (input.find("L6") != std::string::npos) segType = "L6";
    else if (input.find("L8") != std::string::npos) segType = "L8";
    return segType;
}

void makeAll2Dplots(std::vector<TH2D_ptr>& histos, const std::string& outputGraphsDir, const std::string& plotStyle, bool savePlot) {
    for (auto& hist : histos) {
        std::string segType = determineSegType(hist->GetName());
        std::string segTypeSuffix = "";
        if (!segType.empty()) segTypeSuffix = "_" + segType;

        // printf("%s entries %.0f\n", hist->GetName(), hist->GetEntries());
        // hist->SetMarkerColor(kRed);
        // hist->SetLineColor(kRed);
        // hist->SetMarkerColor(kBlue);
        // hist->SetLineColor(kBlue);

        if (savePlot) {
            // auto leg = TLegend(0.80,0.75,0.85,0.85);
            // leg.AddEntry(hist.GetPtr(), "e^{+}", "l");
            // leg.SetBorderSize(0);
            // leg.SetTextFont(42);
            // leg.SetTextSize(0.025);

            auto can = TCanvas();
            hist->Draw(plotStyle.c_str());
            hist->Draw((plotStyle + " SAME").c_str());
            // leg.Draw();
            std::string name = hist->GetName();
            // cout << name << ", segType = " << segTypeSuffix << endl;
            auto can_name = outputGraphsDir + name + ".pdf";
            can.Modified(); 
            can.Update();
            can.SaveAs(can_name.c_str());
        }

        hist->SetTitle((segType + " " + hist->GetTitle()).c_str());
        hist->Write();
    }
}

void makeAll1Dplots(std::vector<TH1D_ptr>& histos, const std::string& outputGraphsDir, bool savePlot) {
    for (auto& hist : histos) {
        std::string segType = determineSegType(hist->GetName());
        std::string segTypeSuffix = "";
        if (!segType.empty()) segTypeSuffix = "_" + segType;

        // printf("%s entries %.0f\n", hist->GetName(), hist->GetEntries());

        if (savePlot) {
            // auto leg = TLegend(0.80,0.75,0.85,0.85);
            // leg.AddEntry(hist.GetPtr(), "e^{+}", "l");
            // leg.SetBorderSize(0);
            // leg.SetTextFont(42);
            // leg.SetTextSize(0.025);

            auto can = TCanvas();
            // TString colon = ";"; 
            // auto title = hist->GetTitle() + colon + hist->GetXaxis()->GetTitle() + colon + hist->GetYaxis()->GetTitle();
            // auto hs = THStack("hs", title);
            // hs.Add(hist.GetPtr());
            // hs.Add(hist.GetPtr());

            // hist->Draw(plotStyle.c_str());
            // hist->Draw((plotStyle + " SAME").c_str());
            // hs.Draw("nostack");
            // leg.Draw();
            hist->Draw();
            std::string name = hist->GetName();
            // cout << name << ", segType = " << segTypeSuffix << endl;
            auto can_name = outputGraphsDir + name + ".pdf";
            can.Modified(); 
            can.Update();
            can.SaveAs(can_name.c_str());
        }

        hist->SetTitle((segType + " " + hist->GetTitle()).c_str());

        hist->Write();
    }
}

void plot1DForTrackTypes(std::map<int, ROOT::RDF::RNode> filterMap, plotInfo& plot, const std::vector<int>& trackTypes, std::vector<TH1D_ptr>& histos) {
    for (const auto& i : trackTypes) {
        plot.setSegType(i);
        plot1D(filterMap.at(i), plot, histos);
    }
    plot.setSegType(0);
}

void plot2DForTrackTypes(std::map<int, ROOT::RDF::RNode> filterMap, plotInfo& plot, const std::vector<int>& trackTypes, std::vector<TH2D_ptr>& histos) {
    for (const auto& i : trackTypes) {
        plot.setSegType(i);
        plot2D(filterMap.at(i), plot, histos);
    }
    plot.setSegType(0);
}

void overlay1Dplots(std::vector<TH1D_ptr>& histosL, std::vector<TH1D_ptr>& histosR, const std::string& outputGraphsDir, const std::string& labelL, const std::string& labelR, bool savePlot, std::string overlay) {
    assert(histosL.size() == histosR.size());
    for (int i = 0; i < histosL.size(); i++) {
        auto& h1 = histosL[i];
        auto& h2 = histosR[i];
        
        std::string segType = determineSegType(h1->GetName());
        std::string segTypeSuffix = "";
        if (!segType.empty()) segTypeSuffix = "_" + segType;

        // printf("%s entries %.0f, %s entries %.0f\n", h1->GetName(), h1->GetEntries(), h1->GetName(), h1->GetEntries());
        h1->SetMarkerColor(CUT_COL);
        h1->SetLineColor(CUT_COL);

        h2->SetMarkerColor(NON_CUT_COL);
        h2->SetLineColor(NON_CUT_COL);

        if (savePlot) {
            auto leg = TLegend(0.60,0.70,0.75,0.85);
            // leg.SetHeader("#bf{Mu3e} #it{Integration Run 2022 Preliminary}","C");
            leg.AddEntry(h1.GetPtr(), (" " +labelL).c_str(), "l");
            leg.AddEntry(h2.GetPtr(), (" " +labelR).c_str(), "l");

            auto can = TCanvas();
            TString colon = ";"; 
            auto title = segType + colon + h1->GetXaxis()->GetTitle() + colon + h1->GetYaxis()->GetTitle();

            auto hs = THStack("hs", title);
            hs.Add(h1.GetPtr());
            hs.Add(h2.GetPtr());
            hs.Draw("nostack");
            leg.Draw();

            // auto tex = TLatex();
            // tex.SetNDC();
            // tex.SetTextSize(0.035);
            // tex.SetTextAlign(12);
            // tex.SetTextFont(42);
            // tex.DrawLatex(0.30, 0.85, "#bf{Mu3e} Integration Run 2022 #it{Preliminary}");

            std::string name = h1->GetName();
            // cout << name << ", segType = " << segTypeSuffix << endl;
            auto can_name = outputGraphsDir + name + "_" + overlay + ".pdf";
            can.Modified(); 
            can.Update();
            can.SaveAs(can_name.c_str());
        }
    }
}

// use on segs/frames tree?
json makeEventDisplayJSON(const ROOT::RDF::RNode& RDF, const std::string& cut, const json& edisplay) {
    ROOT::RDF::RNode data(RDF);

    std::vector<int> events;

    auto filteredDf = data.Filter(cut);
    filteredDf.Foreach([&events] (const int& eventID) {
        events.push_back(eventID);
    }, {"eventId"} );

    auto filteredDisplay = json::array();
    // for (auto& e : events) cout << e << endl;
    for (const auto& j : edisplay.items()) {
        // cout << j.value()["data"]["eventID"] << endl;
        if ( std::find(events.begin(), events.end(), j.value()["data"]["eventID"]) != events.end() ) filteredDisplay += j.value(); 
        // if (j.value().contains("data.eventID")) {
        // }
    }

    return filteredDisplay;
}

void makeEffPlots(const ROOT::RDF::RNode& RDF, const std::string& outputGraphsDir) {
    ROOT::RDF::RNode data(RDF);

    TEfficiency* zEff = new TEfficiency("h_z_eff", ";z [mm]; #epsilon", 6, -42., 63.);
    TEfficiency* phiEff = new TEfficiency("h_phi_eff", ";#phi [rad]; #epsilon", 10, -3.15, 3.15);

    TEfficiency* zEffL0 = new TEfficiency("h_z_eff_L0", ";z [mm]; #epsilon", 6, -42., 63.);
    TEfficiency* phiEffL0 = new TEfficiency("h_phi_eff_L0", ";#phi [rad]; #epsilon", 8, -3.15, 3.15);

    TEfficiency* zEffL1 = new TEfficiency("h_z_eff_L1", ";z [mm]; #epsilon", 6, -42., 63.);
    TEfficiency* phiEffL1 = new TEfficiency("h_phi_eff_L1", ";#phi [rad]; #epsilon", 10, -3.15, 3.15);

    TEfficiency* phiVszEffL0 = new TEfficiency("h_phi_vs_z_eff_L0", "L0; z [mm]; #phi [rad]", 6, -42., 63., 8, -3.15, 3.15);
    TEfficiency* phiVszEffL1 = new TEfficiency("h_phi_vs_z_eff_L1", "L1; z [mm]; #phi [rad]", 6, -42., 63., 10, -3.15, 3.15);

    data.Foreach([&] (const int& passed, const int& dead, const int& chip, const float& dphi, 
                      const float& dz, const float& rt, const float& z, const float& phi, const int& intersectedLinkMask) {
        if (!dead && chip != -1 && !intersectedLinkMask) {
            bool matching = false;
            if (passed && std::abs(dz) <= 1.0 && std::abs(dphi) <= 0.1) matching = true;
            
            zEff->Fill(matching, z);
            phiEff->Fill(matching, phi);

            if (rt > 32) {
                zEffL1->Fill(matching, z);
                phiEffL1->Fill(matching, phi);
                phiVszEffL1->Fill(matching, z, phi);
            } else {
                zEffL0->Fill(matching, z);
                phiEffL0->Fill(matching, phi);
                phiVszEffL0->Fill(matching, z, phi);
            }
        } 
    }, {"intersectedChipHasHit", "intersectedDeadChip", "intersectedChipID", "delta_intersect_phi", 
        "delta_intersect_z", "intersectedChipHitRt", "intersectedChipHitZ", "intersectedChipHitPhi", "intersectedLinkMask"} );
    TGraphAsymmErrors* gr;

    std::string name = outputGraphsDir + "h_z_eff.pdf";
    auto can = TCanvas();
    zEff->Draw();
    gPad->Update();
    gr = zEff->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-42., 63.);
    zEff->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_phi_eff.pdf";
    phiEff->Draw();
    gPad->Update();
    gr = phiEff->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-3.15, 3.15);
    phiEff->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_phi_eff_L0.pdf";
    phiEffL0->Draw();
    gPad->Update();
    gr = phiEffL0->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-3.15, 3.15);
    phiEffL0->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_phi_eff_L1.pdf";
    phiEffL1->Draw();
    gPad->Update();
    gr = phiEffL1->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-3.15, 3.15);
    phiEffL1->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_z_eff_L0.pdf";
    zEffL0->Draw();
    gPad->Update();
    gr = zEffL0->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-42., 63.);
    zEffL0->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_z_eff_L1.pdf";
    zEffL1->Draw();
    gPad->Update();
    gr = zEffL1->GetPaintedGraph(); gr->GetXaxis()->SetLimits(-42., 63.);
    zEffL1->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_phi_vs_z_eff_L0.pdf";
    phiVszEffL0->Draw("colz");
    phiVszEffL0->Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_phi_vs_z_eff_L1.pdf";
    phiVszEffL1->Draw("colz");
    phiVszEffL1->Write();
    can.SaveAs(name.c_str());
}

void plotForTypesAndCuts(const ROOT::RDF::RNode& RDF, std::map<int, ROOT::RDF::RNode> filterMap, plotInfo& plot, const std::vector<int>& trackType, std::vector<cutPlotInfo>& cuts) {
    const std::string name = plot.name;
    for (auto& cutStyle : cuts) {
        plot.setCut(cutStyle.cut);
        plot.setTitle(cutStyle.title);
        plot.setName((name + cutStyle.name));
        if (plot.varY != "") {
            plot2D(RDF, plot, cutStyle.histos2D);
            plot2DForTrackTypes(filterMap, plot, trackType, cutStyle.histos2D);
        } else {
            plot1D(RDF, plot, cutStyle.histos1D);
            plot1DForTrackTypes(filterMap, plot, trackType, cutStyle.histos1D);
        }
    }
}

json getConfig(const std::string& inputFile, const std::string& name) {
    // Get config details...
    auto file = new TFile(inputFile.c_str(), "READ");
    auto confKey = file->GetKey(name.c_str());
    json conf;
    if (confKey) {
        auto confStr = (TObjString*) confKey->ReadObj();
        auto x = confStr->GetString();
        // cout << x << endl;
        if (name == "eDisplayString") x = "[" + x + "]";
        conf = json::parse(x.Data());
    }
    file->Close();
    return conf;
}

void makeDeadChipMap(const json& sensors, const std::vector<int>& deadChips, const std::string& outputGraphsDir) {
    auto hL0 = TH2D("hL0", "Layer 0; z [mm]; #phi [rad]", 6, -42., 63., 8, -3.15, 3.15);
    auto hL1 = TH2D("hL1", "Layer 1; z [mm]; #phi [rad]", 6, -42., 63., 10, -3.15, 3.15);

    for (const auto& j : sensors.items()) {
        auto phi = std::atan2(j.value()["v"]["y"].get<float>(), j.value()["v"]["x"].get<float>());
        auto z = j.value()["v"]["z"].get<float>();

        int dead = 0;
        if (std::find(deadChips.begin(), deadChips.end(), j.value()["runChip"].get<int>()) != deadChips.end()) dead = 1;

        if (j.value()["layer"].get<int>() == 1) hL1.Fill(z, phi, j.value()["runChip"].get<int>());
        else hL0.Fill(z, phi, j.value()["runChip"].get<int>());
    }

    // hL0.SetMinimum(1);
    // hL1.SetMinimum(1);

    hL0.SetMarkerSize(1.7);
    hL1.SetMarkerSize(1.7);

    std::string name = outputGraphsDir + "h_L0_chip_map.pdf";
    auto can = TCanvas();
    hL0.Draw("text col");
    hL0.Write();
    can.SaveAs(name.c_str());

    name = outputGraphsDir + "h_L1_chip_map.pdf";
    hL1.Draw("text col");
    hL1.Write();
    can.SaveAs(name.c_str());
}

void writeJSON(const std::string fileName, const json& j) {
    std::ofstream file(fileName);
    if (file.is_open()) {
        file << std::setw(4) << j << std::endl;
        file.close();
    } else {
        std::cerr << "Failed to open json file" << endl;
    }
}

int main(int argc, char** argv) {
    namespace po = boost::program_options;

    std::vector<std::string> inputFiles;
    std::string outputFile, outputGraphsDir, linkMaskFile;
    int savePlots = 1;
    int nSegs = 0, nSkip = 0;
    double energyTag = 0.0;
    plotInfo plotter;

    po::options_description opts;
    opts.add_options()
        ("inputFiles", po::value(&inputFiles)->multitoken(), "input reco root files")
        ("linkMaskFile", po::value(&linkMaskFile), "List of runs and link masks per (sim) chip")
        ("output", po::value(&outputFile), "output root file")
        ("outputGraphsDir", po::value(&outputGraphsDir), "output root file")
        ("savePlots", po::value(&savePlots), "save plots as .pdfs")
        ("nSegs", po::value(&nSegs), "Number of segs to process total")
        ("nSkip", po::value(&nSkip), "Number of segs to skip")
        ("help", "help");
    
    po::positional_options_description opts_p;
    opts_p.add("inputFiles", 1);
    opts_p.add("linkMaskFile", 1);
    opts_p.add("output", 1);
    opts_p.add("outputGraphsDir", 1);
    opts_p.add("savePlots", 1);
    opts_p.add("nSegs", 1);
    opts_p.add("nSkip", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).positional(opts_p).run(), vm);
    po::notify(vm);

    if(vm.count("help") || inputFiles.empty() || outputFile.empty()) {
        printf("Usage: mu3eTrirecAnaCosmic [options] <inputFiles> <output>\n");
        opts.print(cout);
        return -1;
    }

    gROOT->ProcessLine(".L /unix/muons/users/bgayther/phd_studies/RootStyle.h");
    gROOT->ForceStyle();

    auto colours = RootColourDefs();
    CUT_COL = colours.kC3;
    NON_CUT_COL = colours.kC0;

    auto sensors = getConfig(inputFiles[0], "sensor_alignment");

    auto totalEDisplayJSON = json::array();
    for (auto& file : inputFiles) {
        // cout << file << endl;
        auto tmp = getConfig(file, "eDisplayString");
        totalEDisplayJSON.insert(totalEDisplayJSON.begin(), tmp.begin(), tmp.end());
    }
    // std::cout << totalEDisplayJSON[0] << std::endl;

    std::map<int, int> simChip_to_runChip;
    std::map<int, int> runChip_to_simChip;
    for (const auto &item : sensors.items()) {
        simChip_to_runChip[item.value()["simChip"]] = item.value()["runChip"];
        runChip_to_simChip[item.value()["runChip"]] = item.value()["simChip"];
    }
    // for (auto& [simChip, runChip] : simChip_to_runChip) std::cout << simChip << ", " << runChip << std::endl;

    // std::vector<int> a = {0,1,10,100,101,105,11,111,112,12,13,14,15,16,17,2,20,22,23,26,27,28,29,3,36,37,38,41,45,5,50,52,62,68,69,70,71,78,79,80,81,87,89,95,96,97,98,99};
    // for (auto& i : a) {
    //     cout << runChip_to_simChip[i] << ", ";
    // }

    // TODO: get minimum common set of dead chips over these runs...
    auto deadChips = getConfig(inputFiles[0], "deadChips");
    // std::vector<std::vector<int>> all_dead_chips;

    // for (const auto& file : inputFiles) {
    //     auto deadJson = getConfig(file, "deadChips");
    //     // std::cout << deadJson << std::endl;
    //     all_dead_chips.push_back(deadJson);
    //     // for (const auto& j : deadJson.items()) {
    //     //     std::cout << "key: " << j.key() << ", value:" << j.value() << '\n';
    //     //     std::vector<int> curr = j.value().get<std::vector<int>>();
    //     //     std::sort(curr.begin(), curr.end());
    //     //     all_dead_chips.push_back(curr);
    //     // }
    // }

    // auto last_intersection = all_dead_chips[0];
    // std::vector<int> common_dead_chips;

    // for (int i = 1; i < all_dead_chips.size(); ++i) {
    //     std::set_intersection(last_intersection.begin(), last_intersection.end(),
    //         all_dead_chips[i].begin(), all_dead_chips[i].end(),
    //         std::back_inserter(common_dead_chips));
    //     std::swap(last_intersection, common_dead_chips);
    //     common_dead_chips.clear();
    // }

    if (!outputGraphsDir.empty()) {
        boost::filesystem::create_directory(outputGraphsDir);
        outputGraphsDir.append("/");
        plotter.setOutputGraphsDir(outputGraphsDir);
    } else {
        boost::filesystem::create_directory("graphs");
        plotter.setOutputGraphsDir("graphs/");
    }

    TFile* outfile = new TFile((outputFile + ".root").c_str(), "recreate");

    // gStyle->SetOptStat("emr");
    // gStyle->SetStatY(0.9);
    // gStyle->SetStatX(0.9);
    // gStyle->SetStatW(0.4);
    // gStyle->SetStatH(0.2);
    gStyle->SetOptStat(0);
    gROOT->SetBatch(true);

    TStopwatch watch;
    ROOT::EnableImplicitMT();

    watch.Reset();
    watch.Start();

    std::vector<int> trackTypes {3, 4};

    makeDeadChipMap(sensors, deadChips, outputGraphsDir);
    // std::string overall_cut;

    // TODO: maybe just make this per var instead of per vector
    auto getSensors = [simChip_to_runChip] (const int& nhit, const ROOT::RVec<float> &sid00, const ROOT::RVec<float> &sid10, const ROOT::RVec<float> &sid20) {
        std::vector<int> sens;
        auto h1 = int(sid00[0]); auto h2 = int(sid10[0]); auto h3 = int(sid20[0]); auto h4 = int(sid20[1]); 
        if (nhit == 3) sens = {simChip_to_runChip.at(h1), simChip_to_runChip.at(h2), simChip_to_runChip.at(h3)};
        else if (nhit == 4) sens = {simChip_to_runChip.at(h1), simChip_to_runChip.at(h2), 
                                    simChip_to_runChip.at(h3), simChip_to_runChip.at(h4)};
        return sens;
    };

    auto getHitVar = [] (const int& nhit, const ROOT::RVec<float> &pos00, const ROOT::RVec<float> &pos10, const ROOT::RVec<float> &pos20) {
        std::vector<float> pos;
        if (nhit == 3) pos = {pos00[0], pos10[0], pos20[0]};
        else if (nhit == 4) pos = {pos00[0], pos10[0], pos20[0], pos20[1]};
        return pos;
    };

    // auto getHitIntVar = [] (const int& nhit, const ROOT::RVec<float> &pos00, const ROOT::RVec<float> &pos10, const ROOT::RVec<float> &pos20) {
        // std::vector<float> pos; // to compile for older versions....
    auto getHitIntVar = [] (const int& nhit, const ROOT::RVec<unsigned long long> &T00, const ROOT::RVec<unsigned long long> &T10, const ROOT::RVec<unsigned long long> &T20) {
        std::vector<unsigned long long> t;
        if (nhit == 3) t = {T00[0], T10[0], T20[0]};
        else if (nhit == 4) t = {T00[0], T10[0], T20[0], T20[1]};
        return t;
    };

    auto getDifference = [] (const unsigned long long first, const unsigned long long second) {
        long long res;
        unsigned long long abs_diff = (first > second) ? (first - second): (second - first);
        assert(abs_diff<=INT64_MAX);
        long long result =  (first > second) ? (long long)abs_diff : -(long long)abs_diff;
        return result;
    };

    auto dphi = [] (const float& phi1, const float& phi2) {
        float dphi = phi2 - phi1;
        if(std::abs(dphi) > float_t(M_PI)) dphi -= std::copysign(float(2*M_PI), dphi);
        return dphi;
    };

    json runsToLinkMasks;
    if (!linkMaskFile.empty()) {
        std::ifstream linkMask(linkMaskFile);
        if (linkMask.is_open())
        {
            // std::cout << "Reading link masks from " << linkMaskFile << std::endl;
            linkMask >> runsToLinkMasks;
            linkMask.close();
        }
        else{
            std::cerr << "error, not able to open link mask file\n";
            exit(EXIT_FAILURE);
        }
        // std::cout << runsToLinkMasks["1064"]["1"] << std::endl;
    }

    auto checkLinkMaskIntersection = [&] (const int& runID, const int& chipID, const int& col) {
        if (linkMaskFile.empty()) return 0;
        if (col == -1 || chipID == -1) return 0;

        int min_non_masked_col = 0, max_non_masked_col = 256;
        if (runsToLinkMasks[std::to_string(runID)].contains(std::to_string(chipID))) {
            // return 1;
            auto links = runsToLinkMasks[std::to_string(runID)][std::to_string(chipID)];
            if (links.size() == 3) return 1;
            // else {
            //     std::cout << "runID " << runID << ", chipID " << chipID << std::endl;
            // }

            // dumb way...
            auto A_found = (std::find(links.begin(), links.end(), "C") != links.end());
            auto B_found = (std::find(links.begin(), links.end(), "B") != links.end());
            auto C_found = (std::find(links.begin(), links.end(), "A") != links.end());

            if (A_found && (!B_found || !C_found)) min_non_masked_col = 84;
            if (B_found && (!A_found || !C_found)) min_non_masked_col = 84; max_non_masked_col = 170;
            if (C_found && (!B_found || !A_found)) min_non_masked_col = 170;

            if (!A_found && B_found && C_found) min_non_masked_col = 0; max_non_masked_col = 84;
            if (A_found && !B_found && C_found) min_non_masked_col = 84; max_non_masked_col = 170;
            if (A_found && B_found && !C_found) min_non_masked_col = 170; max_non_masked_col = 256;

            // for (const auto& link : links) {
            //     // std::cout << "runID " << runID << ", chipID " << chipID << ", link " << link << std::endl;
            //     if (link == "A") min_non_masked_col += 84;
            //     else if (link == "B") min_non_masked_col += 86;
            //     else if (link == "C") max_non_masked_col -= 86;
            // }
            if (col > min_non_masked_col && col < max_non_masked_col) return 0;
            else return 1;
        } else return 0;
    };

    ROOT::RDataFrame SegsDataFrame("segs", inputFiles);

    auto segsData = SegsDataFrame.Filter("chi2<=32")
                                 .Define("getSensors", getSensors, {"nhit", "sid00", "sid10", "sid20"})
                                 .Define("h1ChipID", "getSensors[0]")
                                 .Define("h2ChipID", "getSensors[1]")
                                 .Define("h3ChipID", "getSensors[2]")
                                 .Define("h4ChipID", "getSensors[3]")
                                 .Define("getXHitPos", getHitVar, {"nhit", "x00", "x10", "x20"})
                                 .Define("h1Xpos", "getXHitPos[0]")
                                 .Define("h2Xpos", "getXHitPos[1]")
                                 .Define("h3Xpos", "getXHitPos[2]")
                                 .Define("h4Xpos", "getXHitPos[3]")
                                 .Define("getYHitPos", getHitVar, {"nhit", "y00", "y10", "y20"})
                                 .Define("h1Ypos", "getYHitPos[0]")
                                 .Define("h2Ypos", "getYHitPos[1]")
                                 .Define("h3Ypos", "getYHitPos[2]")
                                 .Define("h4Ypos", "getYHitPos[3]")
                                 .Define("getZHitPos", getHitVar, {"nhit", "z00", "z10", "z20"})
                                 .Define("h1Zpos", "getZHitPos[0]")
                                 .Define("h2Zpos", "getZHitPos[1]")
                                 .Define("h3Zpos", "getZHitPos[2]")
                                 .Define("h4Zpos", "getZHitPos[3]")
                                 .Define("getToTHit", getHitIntVar, {"nhit", "ToT00", "ToT10", "ToT20"})
                                 .Define("h1ToT", "getToTHit[0]")
                                 .Define("h2ToT", "getToTHit[1]")
                                 .Define("h3ToT", "getToTHit[2]")
                                 .Define("h4ToT", "getToTHit[3]")
                                 .Define("getTSHit", getHitIntVar, {"nhit", "TS00", "TS10", "TS20"})
                                 .Define("h1TS", "getTSHit[0]")
                                 .Define("h2TS", "getTSHit[1]")
                                 .Define("h3TS", "getTSHit[2]")
                                 .Define("h4TS", "getTSHit[3]")
                                 .Define("maxHitTime", "*std::max_element(getTSHit.begin(), getTSHit.end())")
                                 .Define("minHitTime", "*std::min_element(getTSHit.begin(), getTSHit.end())")
                                 .Define("tSpread", "(maxHitTime - minHitTime) * 8")
                                 .Define("h43TSDiff", getDifference, {"h4TS", "h3TS"})
                                 .Define("h42TSDiff", getDifference, {"h4TS", "h2TS"})
                                 .Define("h41TSDiff", getDifference, {"h4TS", "h1TS"})
                                 .Define("h32TSDiff", getDifference, {"h3TS", "h2TS"})
                                 .Define("h31TSDiff", getDifference, {"h3TS", "h1TS"})
                                 .Define("h21TSDiff", getDifference, {"h2TS", "h1TS"})
                                 .Define("charge_sign", "(p > 0) ? 1 : ((p < 0) ? -1 : 0)")
                                 .Define("lam010","lam01[0]")
                                 .Define("abs_p", "abs(p)")
                                 .Define("abs_r", "abs(r)")
                                 .Define("inv_r", "1.0/abs_r")
                                 .Define("abs_zpca_r","abs(zpca_r)")
                                 .Define("phi010","phi01[0]")
                                 .Define("perr", "sqrt(perr2)")
                                 .Define("zpca_phi", "std::atan2(zpca_y, zpca_x)")
                                 .Define("phi", "tan01[0]")
                                 .Define("theta", "float(M_PI_2) - lam010")
                                 .Define("intersectedPhi", "std::atan2(intersectedPosY, intersectedPosX)")
                                 .Define("intersectedChipHitPhi", "std::atan2(intersectedChipHitY, intersectedChipHitX)")
                                 .Define("intersectedChipHitRt", "std::hypot(intersectedChipHitX, intersectedChipHitY)")
                                 .Define("delta_intersect_phi", dphi, {"intersectedChipHitPhi", "intersectedPhi"})
                                 .Define("delta_intersect_phi_charge", "delta_intersect_phi * charge_sign")
                                 .Define("delta_intersect_z", "intersectedChipHitZ - intersectedPosZ")
                                 .Define("intersectedLinkMask", checkLinkMaskIntersection, {"runId", "intersectedChipID", "intersectedCol"});

    // use typedefs
    std::map<int, ROOT::RDF::RNode> trackFilterMap;
    
    for (auto& i : trackTypes) {
        auto cut = [i] (const int& nhit) {
            return nhit==i;
        };
        trackFilterMap.insert(std::make_pair(i, segsData.Filter(cut, {"nhit"})));
    }

    float pRes = 100, pMin = 0.0, pMax = 10000.0;
    int pBins = (int)((pMax-pMin)/pRes);

    float rRes = 100, rMin = 0.0, rMax = 10000.0;
    int rBins = (int)((rMax-rMin)/rRes);

    float inv_rRes = 0.0004, inv_rMin = 0.0, inv_rMax = 0.022;
    int inv_rBins = (int)((inv_rMax-inv_rMin)/inv_rRes);

    float phiRes = 0.1, phiMin = -3.2, phiMax = 3.2;
    int phiBins = (int)((phiMax-phiMin)/phiRes);

    float thetaRes = 0.1, thetaMin = -3.2, thetaMax = 3.2;
    int thetaBins = (int)((thetaMax-thetaMin)/thetaRes);

    float chi2Res = 5, chi2Min = 0.0, chi2Max = 100.0;
    int chi2Bins = (int)((chi2Max-chi2Min)/chi2Res);

    // give a path!
    // writeJSON("combinedRunsDisplay.json", totalEDisplayJSON);

    // auto cutDisplay = makeEventDisplayJSON(segsData, "nhit==4", totalEDisplayJSON);
    // writeJSON("selectedEventsDisplay.json", cutDisplay);

    std::vector<TH1D_ptr> histos1D;
    std::vector<TH2D_ptr> histos2D;

    // shared_ptr??
    auto noCuts = cutPlotInfo("", "", ""); // no cut
    auto triggCut = cutPlotInfo("cosTrigg==1", " Trigger Coincidence", "_cosTrigg");
    auto noTriggCut = cutPlotInfo("cosTrigg==0", " No trigger Coincidence", "_noCosTrigg");
    auto intersecCut = cutPlotInfo("intersectedDeadChip==1", " with dead chip intersections", "_deadChipIntersec");
    auto noIntersectCut = cutPlotInfo("intersectedDeadChip==0", " with no dead chip intersections", "_noDeadChipIntersec");
    // auto onlyS4Cut = cutPlotInfo("nhit==4", "Only S4 tracks", "_S4_only");
    // auto onlyS3Cut = cutPlotInfo("nhit==3", "Only S3 tracks", "_S3_only");

    std::vector<cutPlotInfo> allCutHistos = {noCuts, triggCut, noTriggCut, intersecCut, noIntersectCut};

    // plotter.set1DInfo("h_p", "", "p [MeV]", "Counts", 100, -500, 500, "p");
    // plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_phi", "", "#phi [rad]", "Counts", phiBins, phiMin, phiMax, "phi");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_chi2", "", "#chi2", "Counts", chi2Bins, chi2Min, chi2Max, "chi2");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_theta", "", "#theta [rad]", "Counts", thetaBins, thetaMin, thetaMax, "theta");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_r", "", "r [mm]", "Counts", rBins, rMin, rMax, "abs_r");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_inv_r", "", "1/r [mm]^{-1}", "Counts", inv_rBins, inv_rMin, inv_rMax, "inv_r");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set2DInfo("h_chi2_vs_inv_r", "", "1/r [mm]^{-1}", "#chi2 ", inv_rBins, inv_rMin, inv_rMax, chi2Bins, chi2Min, chi2Max, "inv_r", "chi2");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set2DInfo("h_theta_vs_inv_r", "", "1/r [mm]^{-1}", "#theta [rad] ", inv_rBins, inv_rMin, inv_rMax, thetaBins, thetaMin, thetaMax, "inv_r", "theta");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set2DInfo("h_phi_vs_inv_r", "", "1/r [mm]^{-1}", "#phi [rad] ", inv_rBins, inv_rMin, inv_rMax, phiBins, phiMin, phiMax, "inv_r", "phi");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_zpca_r", "", "DCA [mm]", "Counts", 64, -32, 32, "zpca_r");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_zpca_z", "", "z0 [mm]", "Counts", 50, -100, 100, "zpca_z");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_zpca_x", "", "x0 [mm]", "Counts", 50, -100, 100, "zpca_x");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_zpca_y", "", "y0 [mm]", "Counts", 50, -100, 100, "zpca_y");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set1DInfo("h_track_tSpread", "", "Latest hit TS - earliest hit TS [ns]", "Counts", 64, 0, 512, "tSpread");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    plotter.set2DInfo("h_theta_vs_phi", "", "#phi [rad]", "#theta [rad]", phiBins, phiMin, phiMax, thetaBins, thetaMin, thetaMax, "phi", "theta");
    plotForTypesAndCuts(segsData, trackFilterMap, plotter, trackTypes, allCutHistos);

    // charge_sign
    // plotter.set1DInfo("h_charge_sign", "", "charge sign", "Counts", 3, -1, 2, "charge_sign");
    // plot1D(segsData, plotter, histos1D);

    // plotter.set2DInfo("h_dphi_vs_charge_sign", "", "Charge sign", "#Delta #phi [rad]", 3, -1, 2, 100, -1, 1, "charge_sign", "delta_intersect_phi");
    // plotter.setCut("nhit==3&&intersectedChipHasHit==1");
    // plot2D(segsData, plotter, histos2D);

    // plotter.set2DInfo("h_dphi_times_charge_vs_phi", "", "#phi [rad]", "Charge * #Delta #phi [rad]", phiBins, phiMin, phiMax, 100, -1, 1, "intersectedChipHitPhi", "delta_intersect_phi_charge");
    // plotter.setCut("nhit==3&&intersectedChipHasHit==1");
    // plot2D(segsData, plotter, histos2D);


    plotter.set2DInfo("h_dz_vs_z", "", "z [mm]", "#Delta z [mm]", 50, -100, 100, 100, -20, 20, "intersectedChipHitZ", "delta_intersect_z");
    plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_phi)<=0.1");
    plot2D(segsData, plotter, histos2D);

    plotter.set2DInfo("h_dphi_vs_phi_L0", "L0", "#phi [rad]", "#Delta #phi [rad]", phiBins, phiMin, phiMax, 100, -1, 1, "intersectedChipHitPhi", "delta_intersect_phi");
    plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_z)<=0.8&&intersectedChipHitRt<32");
    plot2D(segsData, plotter, histos2D);

    plotter.set2DInfo("h_dz_vs_z_L0", "L0", "z [mm]", "#Delta z [mm]", 50, -100, 100, 100, -20, 20, "intersectedChipHitZ", "delta_intersect_z");
    plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_phi)<=0.1&&intersectedChipHitRt<32");
    plot2D(segsData, plotter, histos2D);

    plotter.set2DInfo("h_dphi_vs_phi_L1", "L1", "#phi [rad]", "#Delta #phi [rad]", phiBins, phiMin, phiMax, 100, -1, 1, "intersectedChipHitPhi", "delta_intersect_phi");
    plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_z)<=0.8&&intersectedChipHitRt>32");
    plot2D(segsData, plotter, histos2D);

    plotter.set2DInfo("h_dz_vs_z_L1", "L1", "z [mm]", "#Delta z [mm]", 50, -100, 100, 100, -20, 20, "intersectedChipHitZ", "delta_intersect_z");
    plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_phi)<=0.1&&intersectedChipHitRt>32");
    plot2D(segsData, plotter, histos2D);

    plotter.set1DInfo("h_hit_runChipIDs", "", "run chipID", "Counts", 113, 0, 113, "getSensors");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit1_runChipIDs", "", "Hit 1 - run chipID", "Counts", 113, 0, 113, "h1ChipID");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit2_runChipIDs", "", "Hit 2 - run chipID", "Counts", 113, 0, 113, "h2ChipID");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit3_runChipIDs", "", "Hit 3 - run chipID", "Counts", 113, 0, 113, "h3ChipID");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit4_runChipIDs", "", "Hit 4 - run chipID", "Counts", 113, 0, 113, "h4ChipID");
    plotter.setCut("nhit==4");
    plot1D(segsData, plotter, histos1D);

    plotter.set1DInfo("h_hit1_Xpos", "", "Hit 1 - X position [mm]", "Counts", 50, -100, 100, "h1Xpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit2_Xpos", "", "Hit 2 - X position [mm]", "Counts", 50, -100, 100, "h2Xpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit3_Xpos", "", "Hit 3 - X position [mm]", "Counts", 50, -100, 100, "h3Xpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit4_Xpos", "", "Hit 4 - X position [mm]", "Counts", 50, -100, 100, "h4Xpos");
    plotter.setCut("nhit==4");
    plot1D(segsData, plotter, histos1D);

    plotter.set1DInfo("h_hit1_Ypos", "", "Hit 1 - Y position [mm]", "Counts", 50, -100, 100, "h1Ypos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit2_Ypos", "", "Hit 2 - Y position [mm]", "Counts", 50, -100, 100, "h2Ypos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit3_Ypos", "", "Hit 3 - Y position [mm]", "Counts", 50, -100, 100, "h3Ypos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit4_Ypos", "", "Hit 4 - Y position [mm]", "Counts", 50, -100, 100, "h4Ypos");
    plotter.setCut("nhit==4");
    plot1D(segsData, plotter, histos1D);

    plotter.set1DInfo("h_hit1_Zpos", "", "Hit 1 - Z position [mm]", "Counts", 50, -100, 100, "h1Zpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit2_Zpos", "", "Hit 2 - Z position [mm]", "Counts", 50, -100, 100, "h2Zpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit3_Zpos", "", "Hit 3 - Z position [mm]", "Counts", 50, -100, 100, "h3Zpos");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit4_Zpos", "", "Hit 4 - Z position [mm]", "Counts", 50, -100, 100, "h4Zpos");
    plotter.setCut("nhit==4");
    plot1D(segsData, plotter, histos1D);

    plotter.set1DInfo("h_hit_ToT", "", "Hit - ToT [ns]", "Counts", 32, 0, 256, "getToTHit");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit1_ToT", "", "Hit 1 - ToT [ns]", "Counts", 32, 0, 256, "h1ToT");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit2_ToT", "", "Hit 2 - ToT [ns]", "Counts", 32, 0, 256, "h2ToT");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit3_ToT", "", "Hit 3 - ToT [ns]", "Counts", 32, 0, 256, "h3ToT");
    plot1D(segsData, plotter, histos1D);
    plotter.set1DInfo("h_hit4_ToT", "", "Hit 4 - ToT [ns]", "Counts", 32, 0, 256, "h4ToT");
    plotter.setCut("nhit==4");
    plot1D(segsData, plotter, histos1D);

    plotter.set1DInfo("h_track_hit31_tdiff", "", "Hit 3 - Hit 1 TS [ns]", "Counts", 64, -256, 256, "h31TSDiff");
    plot1D(segsData, plotter, histos1D);

    if (!linkMaskFile.empty()) {
        plotter.set2DInfo("h_dphi_vs_phi_no_cut", "", "#phi [rad]", "#Delta #phi [rad]", phiBins, phiMin, phiMax, 100, -1, 1, "intersectedChipHitPhi", "delta_intersect_phi");
        plotter.setCut("nhit==3&&intersectedChipHasHit==1&&intersectedLinkMask==0");
        plot2D(segsData, plotter, histos2D);

        plotter.set2DInfo("h_dz_vs_z_no_cut", "", "z [mm]", "#Delta z [mm]", 50, -100, 100, 100, -20, 20, "intersectedChipHitZ", "delta_intersect_z");
        plotter.setCut("nhit==3&&intersectedChipHasHit==1&&intersectedLinkMask==0");
        plot2D(segsData, plotter, histos2D);

        plotter.set2DInfo("h_dphi_vs_phi", "", "#phi [rad]", "#Delta #phi [rad]", phiBins, phiMin, phiMax, 100, -1, 1, "intersectedChipHitPhi", "delta_intersect_phi");
        plotter.setCut("nhit==3&&intersectedChipHasHit==1&&abs(delta_intersect_z)<=0.8&&intersectedLinkMask==0");
        plot2D(segsData, plotter, histos2D);
        makeEffPlots(trackFilterMap.at(3), outputGraphsDir);
    }

    // any plots not in cuts vector
    makeAll1Dplots(histos1D, outputGraphsDir, true);
    makeAll2Dplots(histos2D, outputGraphsDir, "colz", true);

    for (auto& cut : allCutHistos) {
        if (!cut.histos1D.empty()) makeAll1Dplots(cut.histos1D, outputGraphsDir, true);
        if (!cut.histos2D.empty()) makeAll2Dplots(cut.histos2D, outputGraphsDir, "colz", true);
    }

    // overlap plots (don't use indices!!!)
    // maybe just python script to do overlays
    overlay1Dplots(allCutHistos[1].histos1D, allCutHistos[2].histos1D, outputGraphsDir, "Trigger", "No Trigger", true, "overlay");
    overlay1Dplots(allCutHistos[3].histos1D, allCutHistos[4].histos1D, outputGraphsDir, "Dead chip intersection", "No dead chip intersection", true, "overlay");
    // overlay1Dplots(allCutHistos[1].histos1D, allCutHistos[3].histos1D, outputGraphsDir, "Dead chip intersection", "Trigger", true, "deadChipOverlay");

    // frame analysis
    std::vector<TH1D_ptr> histos1D_frames;
    ROOT::RDataFrame FramesDataFrame("frames", inputFiles);
    auto framesData = FramesDataFrame.Define("hitsPerFrame", "nGenuineHits");

    plotter.set1DInfo("h_hits_per_frame", "", "Number of hits in frame", "Frame counts", 100, 0, 100, "hitsPerFrame");
    plot1D(framesData, plotter, histos1D_frames);

    plotter.set1DInfo("h_S3_per_frame", "", "Number of S3's in frame", "Frame counts", 100, 0, 100, "n3");
    plot1D(framesData, plotter, histos1D_frames);

    plotter.set1DInfo("h_S4_per_frame", "", "Number of S4's in frame", "Frame counts", 100, 0, 100, "n4");
    plot1D(framesData, plotter, histos1D_frames);

    plotter.set1DInfo("h_tracks_per_frame", "", "Number of S3's in frame", "Frame counts", 100, 0, 100, "n");
    plot1D(framesData, plotter, histos1D_frames);

    makeAll1Dplots(histos1D_frames, plotter.outputGraphsDir, true);

    // do {
    //     cout << '\n' << "Press a key to continue...";
    // } while (cin.get() != '\n');

    watch.Stop();
    cout << "The code took " << watch.RealTime() << " seconds." << endl;

    outfile->Write();
    outfile->Close();
    return 0;
}