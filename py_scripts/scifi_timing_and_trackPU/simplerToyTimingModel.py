import numpy as np
import math
import sys
from collections import defaultdict
import ROOT
import argparse
import tabulate

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-n_mppcs', dest='n_mppcs', default=-1, type=int, help='number of mppcs per side to sim (1 photon per mppc)')
args = parser.parse_args()

n_events = 50000

f = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/run/scripts/fibres/stat.root")
f2 = ROOT.TFile.Open("/unix/muons/users/bgayther/testing_code/git_branches/scifiTiming_git/mu3e/tests/data/timing_res_analysis.root")
n_mppcs = args.n_mppcs
output_file = ROOT.TFile("toyModel_n_mppcs_"+str(n_mppcs)+".root", 'RECREATE')

ROOT.gRandom.SetSeed(0)
np.random.seed(0)

ROOT.gROOT.SetBatch(True)

# Import z positions of mppc hits
hZpositions = f.Get("hZpositions")

# Import res of pl vs Z pos
hPathlengthZresL = f2.Get("hPathlengthVsZTimeDiffL")
hPathlengthZresR = f2.Get("hPathlengthVsZTimeDiffR")

opts = range(8)

hClusterTimeMean_ls = [ROOT.TH1F("hClusterTimeMean_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} + t_{right})/2 - t_{mc} [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeDiff_ls = [ROOT.TH1F("hClusterTimeDiff_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} - t_{right}) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeMin_ls = [ROOT.TH1F("hClusterTimeMin_opt_"+str(opt), "Time Resolution cls_1p;min(t_{left}, t_{right}) - t_{mc} [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeLeft_ls = [ROOT.TH1F("hClusterTimeLeft_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} - t_{mc}) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeRight_ls = [ROOT.TH1F("hClusterTimeRight_opt_"+str(opt), "Time Resolution cls_1p;(t_{right} - t_{mc}) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeLeftCorr_ls = [ROOT.TH1F("hClusterTimeLeftCorr_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} - t_{mc}) (z corrected) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeRightCorr_ls = [ROOT.TH1F("hClusterTimeRightCorr_opt_"+str(opt), "Time Resolution cls_1p;(t_{right} - t_{mc}) (z corrected) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeMeanCorr_ls = [ROOT.TH1F("hClusterTimeMeanCorr_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeDiffCorr_ls = [ROOT.TH1F("hClusterTimeDiffCorr_opt_"+str(opt), "Time Resolution cls_1p;(t_{left} - t_{right}) (z corrected) [ns]", 2000, -5, 5) for opt in opts]
hClusterTimeMinCorr_ls = [ROOT.TH1F("hClusterTimeMinCorr_opt_"+str(opt), "Time Resolution cls_1p;min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]", 2000, -5, 5) for opt in opts]
hPathlenResL_vs_Zcorr_ls = [ROOT.TH2F("hPathlenResL_vs_Zcorr_opt_"+str(opt), "Left side; Z correction [ns]; Pathlen res [ns]", 2000, -0.5, 2.5, 200, 0.0, 0.25) for opt in opts]
hPathlenResR_vs_Zcorr_ls = [ROOT.TH2F("hPathlenResR_vs_Zcorr_opt_"+str(opt), "Right side; Z correction [ns]; Pathlen res [ns]", 2000, -0.5, 2.5, 200, 0.0, 0.25) for opt in opts]
hClusterTimeDiffVsZ_ls = [ROOT.TH2F("hClusterTimeDiffVsZ_opt_"+str(opt),"; Z pos [mm];(t_{left} - t_{right}) [ns]", 200, -150, 150, 2000, -5, 5) for opt in opts]





fb_len = 287.75
mu_daq = 1.0
sigma_daq_jitter = 0.300
decayTime = 2.8
plDecrease = 0.84
plUpper = 1.13
plLower = 1.0
n_fb = 1.59
c_light_mm_ns = 299.792
sptr = 0.085
frameLength = 64.0
cn = c_light_mm_ns / n_fb
fb_len_half = fb_len/2.0

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

def correctTimes(timeL, timeR, zPos):
    dL = fb_len_half - zPos
    dR = fb_len_half + zPos
    cn = c_light_mm_ns / n_fb
    tL = timeL - dL/cn
    tR = timeR - dR/cn
    return [tL, tR]

def genTime(mc_time, pathlength, d_side, opt):
    time = 0.0
    if (opt == 0):
        # TIME = interaction time (mc_time) smeared by 0.250 ns
        time = np.random.normal(mc_time, 0.250)

    elif (opt == 1):
        # TIME = interaction time (mc_time) + DAQ delay/jitter
        time = mc_time + np.random.normal(mu_daq, sigma_daq_jitter)

    elif (opt == 2):
        # TIME = interaction time (mc_time) + photon pathlength propagation time
        time = mc_time + (n_fb * pathlength)/c_light_mm_ns

    elif (opt == 3):
        # TIME = interaction time (mc_time) + photon expon decay time 
        time = mc_time + np.random.exponential(decayTime) 

    elif (opt == 4):
        # TIME = interaction time (mc_time) + photon pathlength propagation time + DAQ delay/jitter
        time = mc_time + (n_fb * pathlength)/c_light_mm_ns + np.random.normal(mu_daq, sigma_daq_jitter)

    elif (opt == 5):
        # TIME = interaction time (mc_time) + photon expon decay time + photon pathlength propagation time 
        time = mc_time + np.random.exponential(decayTime) + (n_fb * pathlength)/c_light_mm_ns 

    elif (opt == 6):
        # TIME = interaction time (mc_time) + photon expon decay time + photon pathlength propagation time + DAQ delay/jitter
        time = mc_time + np.random.exponential(decayTime) + (n_fb * pathlength)/c_light_mm_ns + np.random.normal(mu_daq, sigma_daq_jitter)

    elif (opt == 7):# Full version!
        # TIME = interaction time (mc_time) + photon expon decay time + photon pathlength propagation time  (prev two are smeared by normal sigma = sptr) + DAQ delay/jitter 
        time = mc_time + np.random.normal( np.random.exponential(decayTime) + (n_fb * pathlength)/c_light_mm_ns, sptr) + np.random.normal(mu_daq, sigma_daq_jitter)

    return time


tgraphs = []
for i in range(10): 
    gr = ROOT.TGraph()
    tgraphs.append(gr)

tgraphs[0].SetName("grTimeMean")
tgraphs[1].SetName("grTimeDiff")
tgraphs[2].SetName("grTimeMin")
tgraphs[3].SetName("grTimeLeft")
tgraphs[4].SetName("grTimeRight")
tgraphs[5].SetName("grTimeLeftCorr")
tgraphs[6].SetName("grTimeRightCorr")
tgraphs[7].SetName("grTimeMeanCorr")
tgraphs[8].SetName("grTimeDiffCorr")
tgraphs[9].SetName("grTimeMinCorr")

tgraphs[0].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} + t_{right})/2 - t_{mc} [ns]")
tgraphs[1].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} - t_{right}) [ns]")
tgraphs[2].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;min(t_{left}, t_{right}) - t_{mc} [ns]")
tgraphs[3].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} - t_{mc}) [ns]")
tgraphs[4].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{right} - t_{mc}) [ns]")
tgraphs[5].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} - t_{mc}) (z corrected) [ns]")
tgraphs[6].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{right} - t_{mc}) (z corrected) [ns]")
tgraphs[7].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} + t_{right})/2 - t_{mc} (z corrected) [ns]")
tgraphs[8].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;(t_{left} - t_{right}) (z corrected) [ns]")
tgraphs[9].SetTitle("n_mppcs = "+str(n_mppcs)+";OPT;min(t_{left}, t_{right}) - t_{mc} (z corrected) [ns]")


for OPT in opts:
    for i in range(n_events):
        if i%100 == 0:
            print "(%6i/%i) %5.1f%% \r" % (i, n_events, 100.*i/n_events),
            sys.stdout.flush()

        zPos = hZpositions.GetRandom()
        # if abs(zPos)>10.1:continue
        # zPos = np.random.uniform(-fb_len_half, fb_len_half) 
        # zPos = np.random.uniform(-50.0, 50.0) 
        # zPos = 0.0

        binNo = hPathlengthZresL.GetXaxis().FindBin(zPos)
        pl_res_L = hPathlengthZresL.ProjectionY("", binNo, binNo,"").GetRandom()
        pl_res_R = hPathlengthZresR.ProjectionY("", binNo, binNo,"").GetRandom()

        if (pl_res_L < 0 or pl_res_R < 0): continue

        # Just a reference time..
        mc_time = 30 #np.random.uniform()*frameLength 

        d_side_L = fb_len_half - zPos
        d_side_R = fb_len_half + zPos
        
        # Pathlength calculations
        pathlengths_L = [0.0]*n_mppcs
        for i in range(n_mppcs):
            while True:
                pathlengths_L[i] = np.random.uniform()
                if not np.random.uniform() < (1.0 - pathlengths_L[i]*plDecrease):
                    break
            pathlengths_L[i] = (plLower + (pathlengths_L[i] * (plUpper-plLower))) * d_side_L
        
        pathlengths_R = [0.0]*n_mppcs
        for i in range(n_mppcs):
            while True:
                pathlengths_R[i] = np.random.uniform()
                if not np.random.uniform() < (1.0 - pathlengths_R[i]*plDecrease):
                    break
            pathlengths_R[i] = (plLower + (pathlengths_R[i] * (plUpper-plLower))) * d_side_R
        
        # ASSUMING one photon per mppc! (normally several which you individually calculate times for)
        timesL = [genTime(mc_time, pathlengths_L[i], d_side_L, OPT) for i in range(n_mppcs)]
        timesR = [genTime(mc_time, pathlengths_R[i], d_side_R, OPT) for i in range(n_mppcs)]
        
        tL = np.min(timesL)
        tR = np.min(timesR)

        tL_corr = 0.0
        tR_corr = 0.0

        if (OPT in [0,1,3]): 
            tL_corr = (tL - pl_res_L) + d_side_L/cn
            tR_corr = (tR - pl_res_R) + d_side_R/cn
        else:
            tL_corr = tL - d_side_L/cn
            tR_corr = tR - d_side_R/cn

        hPathlenResL_vs_Zcorr_ls[OPT].Fill(d_side_L/cn, pl_res_L)
        hPathlenResR_vs_Zcorr_ls[OPT].Fill(d_side_R/cn, pl_res_R)

        deltaT = tL - tR
        deltaTCorr = tL_corr - tR_corr
        hClusterTimeDiff_ls[OPT].Fill(deltaT)
        hClusterTimeDiffVsZ_ls[OPT].Fill(zPos, deltaT)

        hClusterTimeDiffCorr_ls[OPT].Fill(deltaTCorr)

        hClusterTimeMean_ls[OPT].Fill((tL+tR)/2.0 - mc_time)
        hClusterTimeMeanCorr_ls[OPT].Fill((tL_corr+tR_corr)/2.0 - mc_time)

        tMin = min(tL, tR) 
        tMin_corr = min(tL_corr, tR_corr) 

        hClusterTimeMin_ls[OPT].Fill(tMin - mc_time)
        hClusterTimeMinCorr_ls[OPT].Fill(tMin_corr - mc_time)

        hClusterTimeLeftCorr_ls[OPT].Fill(tL_corr - mc_time)
        hClusterTimeRightCorr_ls[OPT].Fill(tR_corr - mc_time)
        hClusterTimeLeft_ls[OPT].Fill(tL - mc_time)
        hClusterTimeRight_ls[OPT].Fill(tR - mc_time)

    
    print "Time Resolution (1 photon threshold): "
    print "OPT = ", OPT, " n_mppcs per side = ", n_mppcs

    results = []

    results.append(["t_left - t_right", (getFWHM(hClusterTimeDiff_ls[OPT], 0.5)/2.355 * 1e3)])
    results.append(["t_left - t_right (z corr)", (getFWHM(hClusterTimeDiffCorr_ls[OPT], 0.5)/2.355 * 1e3)])

    results.append(["t_left - tMC", (getFWHM(hClusterTimeLeft_ls[OPT], 0.5)/2.355 * 1e3)])
    results.append(["t_left - tMC", (getFWHM(hClusterTimeRight_ls[OPT], 0.5)/2.355 * 1e3)])

    results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeLeftCorr_ls[OPT], 0.5)/2.355 * 1e3)])
    results.append(["t_left - tMC (z corr)", (getFWHM(hClusterTimeRightCorr_ls[OPT], 0.5)/2.355 * 1e3)])

    results.append(["(t_left + t_right)/2 - tMC", (getFWHM(hClusterTimeMean_ls[OPT], 0.5)/2.355 * 1e3)])
    results.append(["(t_left + t_right)/2 - tMC (z corr)", (getFWHM(hClusterTimeMeanCorr_ls[OPT], 0.5)/2.355 * 1e3)])

    results.append(["min (t_left, t_right) - tMC", (getFWHM(hClusterTimeMin_ls[OPT], 0.5)/2.355 * 1e3)])
    results.append(["min (t_left, t_right) - tMC (z corr)", (getFWHM(hClusterTimeMinCorr_ls[OPT], 0.5)/2.355 * 1e3)])

    print(tabulate.tabulate(results, ["Function","FWHM/2.355 [ps]"], tablefmt='fancy_grid',floatfmt="4.0f"))



    tgraphs[0].SetPoint(tgraphs[0].GetN(), OPT, getFWHM(hClusterTimeMean_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[1].SetPoint(tgraphs[1].GetN(), OPT, getFWHM(hClusterTimeDiff_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[2].SetPoint(tgraphs[2].GetN(), OPT, getFWHM(hClusterTimeMin_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[3].SetPoint(tgraphs[3].GetN(), OPT, getFWHM(hClusterTimeLeft_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[4].SetPoint(tgraphs[4].GetN(), OPT, getFWHM(hClusterTimeRight_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[5].SetPoint(tgraphs[5].GetN(), OPT, getFWHM(hClusterTimeLeftCorr_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[6].SetPoint(tgraphs[6].GetN(), OPT, getFWHM(hClusterTimeRightCorr_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[7].SetPoint(tgraphs[7].GetN(), OPT, getFWHM(hClusterTimeMeanCorr_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[8].SetPoint(tgraphs[8].GetN(), OPT, getFWHM(hClusterTimeDiffCorr_ls[OPT], 0.5)/2.355 * 1e3)
    tgraphs[9].SetPoint(tgraphs[9].GetN(), OPT, getFWHM(hClusterTimeMinCorr_ls[OPT], 0.5)/2.355 * 1e3)

# save = False
# if save:
#     can =  ROOT.TCanvas("can", "can", 1200, 600)
#     gr = tgraphs[1]
#     gr.Draw()
#     name = "grTimeDiff_n_mppcs_"+str(n_mppcs)+".png"
#     can.SaveAs(name)

for gr in tgraphs:
    gr.Write()

print "Finished..."

output_file.Write()
output_file.Close()
