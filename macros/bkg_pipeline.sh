#!/bin/bash
#set -eu -x

# run the full simulation -> reconstruction -> analysis pipeline
export MAIN_DIR=/unix/muons/users/bgayther/testing_code/old_
source ${MAIN_DIR}/install/activate.sh

export OUT_DIR=/unix/muons/users/bgayther/testing_code/offset_reco_bkg_data


cp ${MAIN_DIR}/mu3e/tests/digi_bg.json ${MAIN_DIR}/digi.json
mu3eSim pipeline_bg.mac
mu3eSort \
    data/mu3e_run_000043.root \
    data/run000043-sort.root
# mu3eTrirec \
#     --input data/run000043-sort.root \
#     --output data/run000043-reco.root

rm digi.json
