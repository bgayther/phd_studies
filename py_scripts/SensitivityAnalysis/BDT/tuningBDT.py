from trainBDT import load_data, setup_args, split_and_scale_data
from sklearn.model_selection import GridSearchCV, StratifiedKFold, RandomizedSearchCV, cross_val_score, train_test_split, ParameterSampler, ParameterGrid
import xgboost as xgb
import pickle
from sklearn.metrics import f1_score, roc_curve, make_scorer, roc_auc_score, precision_recall_curve, auc, f1_score
import numpy as np
import pathlib
import time
import pandas as pd
import matplotlib as mpl
import mplhep as hep
import matplotlib.pyplot as plt
import os, psutil

n_evaluations = 1

mpl.use('Agg')
hep.style.use("LHCb2")
mpl.rcParams.update({"axes.grid" : False})

if __name__ == "__main__":
    args = setup_args()
    start_time = time.time()

    resultsDir = args.resultsDir

    # Remove trailing slash
    if resultsDir[-1] == "/":
        resultsDir = resultsDir[:-1]
        
    pathlib.Path(resultsDir).mkdir(parents=True, exist_ok=True)

    # Load data
    data = load_data(args)
    X_train, y_train, w_train, _, _, _ = split_and_scale_data(data)

    w_sum = sum(w_train)
    # Select subset of training sample 
    portion = 0.05
    X_train = X_train[:int(len(X_train)*portion)]
    y_train = y_train[:int(len(y_train)*portion)]
    w_train = w_train[:int(len(w_train)*portion)]

    # scale weights
    w_train *= w_sum / sum(w_train)
    print(f"Training sample size: {len(X_train)}, sum of weights: {sum(w_train)}")

    # Using Xgboost's own CV
    dtrain = xgb.DMatrix(X_train, label=y_train, weight=w_train)

    def fpreproc(dtrain, dtest, param):
        wtrain = dtrain.get_weight()
        wtest = dtest.get_weight()
        sum_weight = sum(wtrain) + sum(wtest)
        wtrain *= sum_weight / sum(wtrain)
        wtest *= sum_weight / sum(wtest)
        dtrain.set_weight(wtrain)
        dtest.set_weight(wtest)
        return (dtrain, dtest, param)

    # Generate a parameter grid
    param_grid = {
        'max_depth': range(2, 11, 1),
        'n_estimators': range(100, 1100, 100),
        'eta': np.linspace(0.1, 1.0, 9),
        'objective': ['binary:logistic'],
        'tree_method': ['hist'],
        'nthread': [args.ncpu],
        'n_jobs': [args.ncpu]
    }
    # 'min_child_weight': range(1, 5, 1),
    # 'gamma': range(0, 5, 1),
    # 'reg_alpha': range(0, 30, 2),
    # 'subsample': np.linspace(0.5, 1.0, 5),
    # 'colsample_bytree': [0.5, 0.75, 1],
    print(f"Param combinations: {len(list(ParameterGrid(param_grid)))}")
    
    # Randomly sample from the parameter grid
    param_combinations = list(ParameterSampler(param_grid, n_iter=n_evaluations, random_state=42))

    def f1_eval(preds, dtrain):
        f1 = f1_score(dtrain.get_label(), preds > 0.5, sample_weight=dtrain.get_weight())
        return 'f1', f1

    # Run CV for each parameter combination
    results = []
    for params in param_combinations:
        print("Current CV params: {}".format(params))
        # params = {'max_depth': 2, 'n_estimators': 10, 'eta': 0.5, 'objective': 'binary:logistic', 'tree_method': 'hist'}
        cv_start_time = time.time()
        cvresult = xgb.cv(params, dtrain, num_boost_round=params['n_estimators'], nfold=2, stratified=True, verbose_eval=10, fpreproc=fpreproc, feval=f1_eval, maximize=True, seed=42, metrics={'aucpr'}, early_stopping_rounds=10)
        process = psutil.Process(os.getpid())
        print("Current memory usage: {:.2f} GB".format(process.memory_info().rss / 1024**3))
        print(f"CV took {time.time() - cv_start_time:.2f} seconds")
        results.append((params, cvresult))

    print("Done with CV")

    # Find the best parameters
    best_params, best_score = None, 0
    for params, cvresult in results:
        score = cvresult['test-f1-mean'].iloc[-1]
        if score > best_score:
            best_params = params
            best_score = score
    
    with open(f'{resultsDir}/xgb_best_params.txt', 'w') as f:
        f.write(str(best_params))

    print("Best params: {}".format(best_params))
    print("Best (f1) score: {}".format(best_score))

    # Make pd dataframe with the results
    results_df = pd.DataFrame(columns=['params', 'cvresult'])
    for params, cvresult in results:
        results_df = results_df.append({'params': params, 'cvresult': cvresult}, ignore_index=True)

    # Save the results
    results_df.to_pickle(f'{resultsDir}/xgb_cv_results.pickle')
    results_df.to_csv(f'{resultsDir}/xgb_cv_results.csv')

    # Plot the results
    plt.figure()
    for params, cvresult in results:
        plt.plot(cvresult['test-f1-mean'], label=f"Max depth: {params['max_depth']}, Eta: {params['eta']}")
    plt.xlabel('Number of estimators')
    plt.ylabel('F1 score')
    plt.legend()
    plt.savefig(f'{resultsDir}/xgb_cv_results.pdf')
    plt.close()

    # Plot parameter distributions explored
    plt.figure()
    for params, cvresult in results:
        plt.hist(params['max_depth'], alpha=0.5, align='left')
    plt.xlabel('Max depth')
    plt.ylabel('Counts')
    plt.savefig(f'{resultsDir}/xgb_cv_max_depth.pdf')
    plt.close()

    plt.figure()
    for params, cvresult in results:
        plt.hist(params['n_estimators'], alpha=0.5, align='left')
    plt.xlabel('Number of estimators')
    plt.ylabel('Counts')
    plt.savefig(f'{resultsDir}/xgb_cv_n_estimators.pdf')
    plt.close()

    plt.figure()
    for params, cvresult in results:
        plt.hist(params['eta'], alpha=0.5, align='left')
    plt.xlabel('Eta')
    plt.ylabel('Counts')
    plt.savefig(f'{resultsDir}/xgb_cv_eta.pdf')
    plt.close()

    # Plot score vs parameter
    plt.figure()
    for params, cvresult in results:
        plt.plot(params['max_depth'], cvresult['test-f1-mean'].iloc[-1], 'o')
    plt.xlabel('Max depth')
    plt.ylabel('F1 score')
    plt.savefig(f'{resultsDir}/xgb_cv_max_depth_vs_score.pdf')
    plt.close()

    plt.figure()
    for params, cvresult in results:
        plt.plot(params['n_estimators'], cvresult['test-f1-mean'].iloc[-1], 'o')
    plt.xlabel('Number of estimators')
    plt.ylabel('F1 score')
    plt.savefig(f'{resultsDir}/xgb_cv_n_estimators_vs_score.pdf')
    plt.close()

    plt.figure()
    for params, cvresult in results:
        plt.plot(params['eta'], cvresult['test-f1-mean'].iloc[-1], 'o')
    plt.xlabel('Eta')
    plt.ylabel('F1 score')
    plt.savefig(f'{resultsDir}/xgb_cv_eta_vs_score.pdf')
    plt.close()

    print("Total time: {:.2f} seconds".format(time.time() - start_time))