#  Convert Mu3e Sim Event to JSON for input into event display...

# Loop over events

# get hit_pixelid
# convert to rows and cols...

import ROOT
import json
import numpy as np

events = ROOT.TChain("mu3e")
events.Add("/ben_dev/mu3e_cosmicRun/mu3e/tests/data/mu3e_run_000042_100k.root")
nevents = 1000 #events.GetEntries()

events.SetBranchStatus("*", 0)
events.SetBranchStatus("Header", 1)
events.SetBranchStatus("hit_pixelid", 1)
events.SetBranchStatus("hit_timestamp", 1)
events.SetBranchStatus("Nhit", 1)

events.SetBranchStatus("fibremppc_mppc", 1)
events.SetBranchStatus("fibremppc_timestamp", 1)
events.SetBranchStatus("fibremppc_col", 1)
events.SetBranchStatus("Nfibremppc", 1)

json_data = []

frameLength = 50.0
siAddTimeBits = 0
fbAddTimeBits = 10
fbDelay = 20

def fraction(data, fineBits): 
    
    mask = (((1) << fineBits) - 1)
    time = ((2.0 * (data & mask) + 1) / ((1) << (fineBits + 1)))

    # print (fineBits)
    # print (hex(mask))
    # print (hex(data))
    # print (time)
    # print ("\n")

    return time


def time(timestamp, fb): 
    return fraction(timestamp, fb) * frameLength



for entry in range(nevents):
    events.LoadTree(entry)
    events.GetEntry(entry)

    item = {"event" : {}} #maybe just change to empty dict, not necessary really...

    pix_id = events.hit_pixelid
    pixel_rows = []
    pixel_cols = []
    pixel_ids = []
    pixel_times = []

    for idx, id in enumerate(pix_id):
        sensorId = id >> 16 # Can match to id in alignment...
        row = id & 0xFF
        col = (id >> 8) & 0xFF
        # print("Pixel id {}, row {}, and col {}".format(sensorId, row, col))
        pixel_rows.append(row)
        pixel_cols.append(col)
        pixel_times.append(time(events.hit_timestamp[idx], siAddTimeBits))
        # pixel_times.append(events.hit_timestamp[idx])
        pixel_ids.append(sensorId)
    
    item["event"]["frameID"] = events.event

    item["event"]["pixel_ids"] = pixel_ids
    item["event"]["pixel_rows"] = pixel_rows
    item["event"]["pixel_cols"] = pixel_cols
    item["event"]["pixel_times"] = pixel_times
    

    fb_ids = events.fibremppc_mppc
    fbs = []
    fb_cols = []
    fb_times = []

    for idx, fb_id in enumerate(fb_ids):    

        mppc_id_before = ((fb_id >> 1) << 8) + ((events.fibremppc_col[idx] & 0x7F) << 1) + (fb_id & 0x01)
        mppc_id = ((mppc_id_before & 0x001F00) >> 8) * 2 + (mppc_id_before & 0x01)
        # print("Fibre id {}".format(x))
        fbs.append(mppc_id)
        fb_times.append(time(events.fibremppc_timestamp[idx] - fbDelay, fbAddTimeBits))
        # fb_times.append(events.fibremppc_timestamp[idx])
        fb_cols.append(events.fibremppc_col[idx])

    item["event"]["fb_ids"] = fbs
    item["event"]["fb_cols"] = fb_cols    
    item["event"]["fb_times"] = fb_times    
    item["event"]["NPixHits"] = events.Nhit
    item["event"]["NFbHits"] = events.Nfibremppc
 
    json_data.append(item)


json_object = json.dumps(json_data, indent = 4) 
with open("sample.json", "w") as outfile: 
    outfile.write(json_object) 