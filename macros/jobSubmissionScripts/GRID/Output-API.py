from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)

from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job
import sys
dirac = Dirac()

if len(sys.argv) != 2:
    print ('All information is not provided, usage:\n       python Output-API.py [jobID]')
    sys.exit(1) 

jobid = sys.argv[1]
print ("Outputing job log file to directory ",jobid)
dirac.getOutputSandbox(jobid)