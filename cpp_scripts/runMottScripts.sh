#!/bin/sh
set -e
cd build && make -j8 && make install
# /home/bgayther/opt/bin/mu3eTrirecAnaMott --inputElec /unix/muons/mu3e/hesketh/batch/mu3e_trirec_electron_20MeV.root --inputPos /unix/muons/mu3e/hesketh/batch/mu3e_trirec_positron_20MeV.root --output /unix/muons/users/bgayther/MottScattering/data/mottAna --outputGraphsDir /unix/muons/users/bgayther/MottScattering/data
# /home/bgayther/opt/bin/mu3eTrirecAnaMott --inputElec /unix/muons/mu3e/hesketh/batch/mu3e_trirec_electron_35MeV.root --inputPos /unix/muons/mu3e/hesketh/batch/mu3e_trirec_positron_35MeV.root --output /unix/muons/users/bgayther/MottScattering/data/mottAna --outputGraphsDir /unix/muons/users/bgayther/MottScattering/data
# /home/bgayther/opt/bin/mu3eTrirecAnaMott --inputElec /unix/muons/mu3e/hesketh/batch/mu3e_trirec_electron_50MeV.root --inputPos /unix/muons/mu3e/hesketh/batch/mu3e_trirec_positron_50MeV.root --output /unix/muons/users/bgayther/MottScattering/data/mottAna --outputGraphsDir /unix/muons/users/bgayther/MottScattering/data

DIR=/unix/muons/users/bgayther/MottScattering/batch
MICHELFILES=/unix/muons/users/bgayther/MichelDecays/batch/*/*/results/mu3e_trirec_*_standard_muon_beam.root

# standard
# TAG="52MeV_standard_250keV_mom_spread"

# Mott generator - Carbon
# TAG="52MeV_mott_gen_Carbon_250keV_mom_spread"

# Mott generator - Oxygen
# TAG="52MeV_mott_gen_Oxygen_250keV_mom_spread"

# 52MeV_standard_no_degrader_250keV_mom_spread
TAGS="12MeV_mott_gen_Carbon_250keV_mom_spread 52MeV_mott_gen_Carbon_250keV_mom_spread 42MeV_mott_gen_Carbon_250keV_mom_spread 32MeV_mott_gen_Carbon_250keV_mom_spread 22MeV_mott_gen_Carbon_250keV_mom_spread"
for TAG in $TAGS; do 
    echo $TAG

    ELECFILES=${DIR}/sim_electron_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
    POSFILES=${DIR}/sim_positron_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
    /home/bgayther/opt/bin/mu3eTrirecAnaMott --inputElec $ELECFILES --inputPos $POSFILES \
                                            --output /unix/muons/users/bgayther/MottScattering/anaOutput/mottAna_$TAG \
                                            --outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/$TAG \
                                            --histJSON "/unix/muons/users/bgayther/phd_studies/cpp_scripts/mottHists.json" 
                                            # --inputMichel $MICHELFILES \
done

# TAG="22MeV_mott_gen_Carbon_250keV_mom_spread"
# ELECFILES=${DIR}/sim_electron_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
# POSFILES=${DIR}/sim_positron_*_${TAG}/*/results/mu3e_trirec_*_${TAG}.root
# /home/bgayther/opt/bin/mu3eTrirecAnaMott --inputElec $ELECFILES --inputPos $POSFILES \
#                                         --output /unix/muons/users/bgayther/MottScattering/anaOutput/mottAna_$TAG \
#                                         --outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/$TAG \
#                                         --histJSON "/unix/muons/users/bgayther/phd_studies/cpp_scripts/mottHists.json" 
