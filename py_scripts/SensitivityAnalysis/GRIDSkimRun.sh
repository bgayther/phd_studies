#!/bin/bash
# Setup LCG
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc11-opt
echo "Setup LCG"

echo "Files in current directory:"
ls -lh

echo $seeds

vertex_files="./run*-ana_vertexfit.root"
gen_files="./signalnorm_*.txt"

# If mode is BhabhaMichel then vertex_files are ./skim_SigAna_*.root
# and gen files are ./skim_SigAna_*.txt
if [ "$mode" == "BhabhaMichel" ]; then
    vertex_files="./skim_SigAna_*.root"
    # gen_files="./skim_SigAna_*.txt"
fi

python prepareData.py -files ${vertex_files} -output ${outputFileName} -ncpu ${ncpu} -mode ${mode}

echo "Finished running prepareData.py, files in current directory:"
ls -lh
