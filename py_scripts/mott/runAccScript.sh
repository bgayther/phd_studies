set -e

DIR=/unix/muons/users/bgayther/MottScattering/batch
# SIM_TAG="22MeV_mott_gen_Carbon_250keV_mom_spread"
# SIM_FILES="/unix/muons/users/bgayther/MottScattering/mu3e/tests/data/mu3e_run_mott_positron_000042.root"
# RECO_FILES="/unix/muons/users/bgayther/MottScattering/mu3e/tests/data/mu3e_run_mott_positron_000042-reco.root"

RUN=*

ENERGIES=(12 17 22 32 42 52)
for i in ${!ENERGIES[@]};
do
    ENERGY=${ENERGIES[$i]}
    SIM_TAG="${ENERGY}MeV_mott_gen_Carbon_250keV_mom_spread"
    SIM_FILES=${DIR}/sim_positron_${RUN}_*_${SIM_TAG}/*/data/mu3e_sorted_*.root 
    RECO_FILES=${DIR}/sim_positron_${RUN}_*_${SIM_TAG}/*/results/mu3e_trirec_*.root 

    echo "Running on ${SIM_FILES}, ${RECO_FILES}"

    python acceptanceCheck.py -simFiles $SIM_FILES -recoFiles $RECO_FILES -outputGraphsDir /unix/muons/users/bgayther/MottScattering/anaOutput/AcceptancePlotsFor${SIM_TAG}
done
