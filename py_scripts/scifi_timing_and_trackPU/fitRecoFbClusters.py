import ROOT
import numpy as np
import time
# import scipy.stats as st
# from scipy.optimize import curve_fit
from collections import defaultdict
import argparse
import logging
import os
import sys
import copy
from tqdm import tqdm
import matplotlib
import matplotlib.pyplot as plt
from itertools import tee
from sklearn.decomposition import PCA
import pathlib
sys.path.append(os.path.join(os.path.dirname(sys.path[0]),'plot_helpers'))
import StyleHelpers

parser = argparse.ArgumentParser(description='Timing Resolution plotter',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, action='append', required=True, help='Input reco file names')
parser.add_argument('-n', dest='nevents', default=-1, type=int, help='# of clusters to process; -1 = ALL')
parser.add_argument('-s', '--skip', dest='skip', default=0, type=int, help='# of frames to skip; -1 = ALL')
parser.add_argument('--log', dest='loglevel', default='INFO', help='Set the log level: DEBUG, INFO, WARNING, ERROR or CRITICAL') 
parser.add_argument('--output', type=str, default='fbRecoHitTimingFit', help='Set name of output .root file')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e/tests/data", help='path/to/reco_file.root (not including file though)')
args = parser.parse_args()

# Create path if it doesn't exist
pathlib.Path(args.path).mkdir(parents=True, exist_ok=True)

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n

############## SIM RES ###############
# clusterTree = ROOT.TChain("fb")

# for file in args.inputs:
#     print ("Adding {} for analysis".format(file))
#     clusterTree.Add(file)

# nevents = clusterTree.GetEntries()
# if args.nevents >= 0: nevents = args.nevents

output_file = ROOT.TFile(args.path + "/" + args.output + ".root", 'RECREATE')

# sipmTotal = 0
# Nbins = 500

# Histos
# hHitTimeZCorrected = ROOT.TH1F("hHitTimeZCorrected", "Time Resolution per hit (Z corrected);t_{hit} - t_{mc} [ns]", Nbins, -5, 15)
h_min_tL_tR_vs_reco_z = ROOT.TH2D("h_min_tL_tR_vs_reco_z", ";Reconstructed Z intersection [mm];t_{min,L} #minus  t_{min,R} [ns]", 100, -140, 140, 100, -5, 5)
h_mean_tL_tR_vs_reco_z = ROOT.TH2D("h_mean_tL_tR_vs_reco_z", ";Reconstructed Z intersection [mm];t_{mean,L} #minus  t_{mean,R} [ns]", 100, -140, 140, 100, -5, 5)
hTrackPUDensity = ROOT.TH1F("hTrackPUDensity", "Track PU density;Number of tracks in cluster", 5, 1.5, 6.5)
hTrackPUDensity_2cols = ROOT.TH1F("hTrackPUDensity_2cols", "Track PU density (within 2 cols);Number of tracks in cluster", 5, 1.5, 6.5)
hSplitClusterIncludingHitRes = ROOT.TH1F("hSplitClusterIncludingHitRes", "Time Resolution including split hit;min(t_{left}, t_{right}) - t_{mc} (MC Z-corrected) [ns]", 200, -5, 5)
hSplitClusterExcludingHitRes = ROOT.TH1F("hSplitClusterExcludingHitRes", "Time Resolution excluding split hit;min(t_{left}, t_{right}) - t_{mc} (MC Z-corrected) [ns]", 200, -5, 5)
hTrackPU_mctime_diff = ROOT.TH1F("hTrackPU_mctime_diff", "Abs MC time diff between 2 track PU cls;abs(t_{Track 1} - t_{Track 2})[ns]", 128, 0, 64)

hClusterRes1Col = ROOT.TH1F("hClusterRes1Col", "Time Resolution adj hits (<=1 col);min(t_{left}, t_{right}) - t_{mc} (MC Z-corrected) [ns]", 200, -5, 5)
hClusterRes2Col = ROOT.TH1F("hClusterRes2Col", "Time Resolution non adj hits (<=2 col);min(t_{left}, t_{right}) - t_{mc} (MC Z-corrected) [ns]", 200, -5, 5)


x = ROOT.RooRealVar("x","t_{hit} - t_{mc} [ns]", -10, 70)
rds = ROOT.RooDataSet("ds","ds",ROOT.RooArgSet(x))
rds_with_xTalk = ROOT.RooDataSet("ds_xTalk","ds_xTalk",ROOT.RooArgSet(x))

# data = []

# skip = args.skip
# for i in range(skip, nevents+skip):
#     clusterTree.GetEntry(i)
#     if i%100 == 0:
#         print ("(%6i/%i) %5.1f%% \r" % (i, nevents, 100.*i/nevents))
#         sys.stdout.flush()


#     nh = clusterTree.nh
#     recurlerPU = clusterTree.recurlerPU
#     trackPU = clusterTree.trackPU
#     zPos = clusterTree.zPos
#     print ("Cl tid {}, side {}, ribbon {}, recurlerPU {}, trackPU {}".format(clusterTree.cl_tid, clusterTree.cl_side, clusterTree.cl_ribbon, recurlerPU, trackPU))

#     if (recurlerPU==0 and trackPU==0):
#         for j in range(nh):
#             crosstalk = clusterTree.crosstalk[j]

#             # if (crosstalk > 0): continue

#             tmc = clusterTree.tmc[j]
#             if tmc<0: continue

#             hit_time = clusterTree.t[j]
#             # t_corr = clusterTree.t_corr[j]
#             mc_zPos = clusterTree.mc_zPos[j]
            # dCorr = 0.0
            # if (clusterTree.side[j] == -1): dCorr = fb_len_half - mc_zPos
            # elif (clusterTree.side[j] == +1): dCorr = fb_len_half + mc_zPos
#             t_corr = hit_time - dCorr / cn
#             print "Hit time {}, col {}, corr time {}, mc time {}, crosstalk {}, zPos {}, tid {}, hid {}".format(hit_time,clusterTree.col[j],t_corr,tmc,crosstalk,mc_zPos,clusterTree.tid[j],clusterTree.hid[j])

#             xres = t_corr - tmc
#             # xres = hit_time - tmc
#             x.setVal(xres)
#             if (crosstalk == 0): rds.add(ROOT.RooArgSet(x))
#             rds_with_xTalk.add(ROOT.RooArgSet(x))
#             # if (hit_time - tmc) >= 5.0 and crosstalk > 0: 
#             #     print "Hit time {}, corr time {}, mc time {}, crosstalk {}, tdiff {}".format(hit_time,t_corr,tmc,crosstalk,t_corr-tmc)
#             # if (xres < 0):
#             #     print "Hit time {}, corr time {}, mc time {}, crosstalk {}, tcorr_diff {}, t_diff {}".format(hit_time,t_corr,tmc,crosstalk,t_corr-tmc, hit_time-tmc)
#             hHitTimeZCorrected.Fill(xres)
#             # data.append(xres)

# NEW WAY
mppcHitsTree = ROOT.TChain("mppc_hits")

for file in args.inputs:
    print("Adding {} for analysis".format(file))
    mppcHitsTree.Add(file)

nevents = mppcHitsTree.GetEntries()
print("Entries in tree = {}".format(nevents))
if args.nevents >= 0: nevents = args.nevents

class mppcHit:
    ribbon = -1
    side = 0 # -1 is left, +1 is right
    col = -1

    amplitude = 0 # no of photons per col
    time = 0.0

    # Truth info
    time_mc = 0.0
    crosstalk = -1
    tid = 0
    hid = 0
    hid_g = 0
    zPos = 0.0
    tCorr = 0.0 # using mc zpos
    raw_time = 0.0
    tCorr_raw = 0.0

    reco_cl_id = -1

    def __init__(self, ribbon, col, side, amp, time, raw_time, time_mc, crosstalk, tid, hid, hid_g, reco_cl_id, zPos):
        self.ribbon = ribbon
        self.col = col
        self.side = side
        self.amplitude = amp
        self.time_mc = time_mc
        self.time = time
        self.raw_time = raw_time
        self.crosstalk = crosstalk
        self.tid = tid
        self.hid = hid
        self.hid_g = hid_g
        self.reco_cl_id = reco_cl_id
        self.zPos = zPos
        if (self.side==-1): dCorr = fb_len_half - self.zPos
        else: dCorr = fb_len_half + self.zPos
        self.tCorr = self.time - dCorr / cn
        self.tCorr_raw = self.raw_time - dCorr / cn

    def printHit(self):
        print('ribbon {}, col {}, side {}, amp {}, time_mc {}, time {}, tCorr {}, tid {}, hid {}, hid_g {}, xtalk_status {}, reco_cl_id {}, zPos {}'.format(
            self.ribbon, self.col, self.side, self.amplitude, self.time_mc, self.time, self.tCorr, self.tid, self.hid, self.hid_g, self.crosstalk, self.reco_cl_id, self.zPos))


def pairwise(iterable):
    # "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def fitGaus(h):
    # Do in range
    gaus = ROOT.TF1("gaus","gaus")
    gaus2 = ROOT.TF1("gaus2", "[0]*exp(-(x-[1])^2/(2*[2]^2)) + [3]*exp(-(x-[1])^2/(2*[4]^2))")
    gaus2.SetParameter(0, h.GetMaximum() * 0.8)
    gaus2.SetParameter(1, 0.)
    gaus2.SetParameter(2, 0.5)
    gaus2.SetParameter(4, 2.)
    gaus2.SetParameter(3, h.GetMaximum() * 0.2)
    h.Fit(gaus, "Q")
    h.Fit(gaus2, "Q")
    tRes = gaus.GetParameter("Sigma")
    tRes2_0 = gaus2.GetParameter(2)
    tRes2_1 = gaus2.GetParameter(4)
    # returns
    # Std dev of one gauss over whole range
    # Std dev of combined gauss, CORE
    # Std dev of combined gauss, WINGS
    return [tRes, tRes2_0, tRes2_1]

def getFWHM(h, fraction = 0.5):
    amp = h.GetMaximum() * fraction
    binStart = h.FindFirstBinAbove(amp)
    binEnd = h.FindLastBinAbove(amp)
    return h.GetXaxis().GetBinUpEdge(binEnd) - h.GetXaxis().GetBinLowEdge(binStart)

def getCrossingMap(hits, use_hidg=False):
    hits_tid_hid_map = defaultdict(list)
    for hit in hits:
        if (use_hidg):
            if (hit.tid not in hits_tid_hid_map or hit.hid_g not in hits_tid_hid_map[hit.tid]): hits_tid_hid_map[hit.tid].append(hit.hid_g)
        else:
            if (hit.tid not in hits_tid_hid_map or hit.hid not in hits_tid_hid_map[hit.tid]): hits_tid_hid_map[hit.tid].append(hit.hid)
    return hits_tid_hid_map 

def getMeanMCTimeOfCrossings(hits_tid_hid_map, truth_clusters, use_hidg=False):
    hits_tid_hid_to_mtime_map = defaultdict(lambda: defaultdict(float))
    # for tid,hids in hits_tid_hid_map.iteritems(): 
    for tid,hids in hits_tid_hid_map.items(): 
        for hid in hids:
            for cl in truth_clusters:
                if cl.tid == tid and (cl.hid == hid if not use_hidg else cl.hid_g == hid): #and side check?
                    # hits_tid_hid_to_mtime_map[tid][hid] = cl.meanTime()
                    hits_tid_hid_to_mtime_map[tid][hid] = cl.meanTimeMC()
    # print(hits_tid_hid_map)
    # print(hits_tid_hid_to_mtime_map)
    return hits_tid_hid_to_mtime_map

class fbCluster:
    cl_id = -1
    side = 0
    ribbon = -1
    tid = 0
    hid = 0
    hid_g = 0
    trackPU = 0
    recurlerPU = 0
    prevFramePU = 0
    ntids = 1
    nhitsL = 0
    nhitsR = 0
    mc_time = 0.0

    def __init__(self, cl_id, tid, hid):
        self.hits = []
        self.tids = []
        self.hids = []
        self.hidgs = []
        self.cl_id = cl_id
        self.tid = tid
        self.hid = hid
        # self.hid_g = hid_g

    def addHit(self, hit):
        self.ribbon = hit.ribbon
        if(hit.side==-1): self.nhitsL+=1
        if(hit.side==+1): self.nhitsR+=1
        if (hit.tid not in self.tids): self.tids.append(hit.tid)        
        if (hit.hid not in self.hids): self.hids.append(hit.hid)        
        if (hit.hid_g not in self.hidgs): self.hidgs.append(hit.hid_g)   
        if (len(self.hits)==0):
            self.side = hit.side 
            self.tid = hit.tid
            self.hid = hit.hid
            self.mc_time = hit.time_mc
        if (hit.time_mc < self.mc_time): self.mc_time = hit.time_mc
        if (self.side != hit.side): self.side = 0
        self.hits.append(hit)
        if (self.tid != hit.tid): self.trackPU = 1
        if (self.tid == hit.tid and self.hid != hit.hid): self.recurlerPU = 1
        if (hit.time_mc < 0): self.prevFramePU = 1

    def bothSides(self):
        if (self.nhitsL and self.nhitsR): return True
        else: return False

    def meanTime(self):
        times = [hit.time for hit in self.hits]
        return np.mean(times)
        # if (self.side == 0):
        # else:
    def meanTimeMC(self):
        times = [hit.time_mc for hit in self.hits]
        return np.mean(times)
    def minTime(self):
        times = [hit.time for hit in self.hits]
        # times = [hit.tCorr for hit in self.hits]
        return min(times)
    def minTimePerSide(self):
        timesL = [hit.time for hit in self.hits if hit.side==-1]
        timesR = [hit.time for hit in self.hits if hit.side==+1]
        return [min(timesL), min(timesR)]
    def meanTimePerSide(self):
        timesL = [hit.time for hit in self.hits if hit.side==-1]
        timesR = [hit.time for hit in self.hits if hit.side==+1]
        return [np.mean(timesL), np.mean(timesR)]
    def minTimeCorr(self):
        times_corr = [hit.tCorr for hit in self.hits]
        return min(times_corr)
    def getIds(self): 
        ids = defaultdict(list) #[tid][hids]
        tid = self.tid
        for hit in self.hits:
            ids[hit.tid].append(hit.hid)
        return ids
    def minTimeCorrRaw(self):
        times_corr = [hit.tCorr_raw for hit in self.hits]
        return min(times_corr)
    def printHits(self):
        for hit in self.hits: hit.printHit()
    def checkAdjCols(self):
        split = [0,0]
        for idx, side in enumerate([-1, +1]):
            for current, next_ in pairwise(self.hits):
                if ((next_.col - current.col) > 1 and current.side==next_.side and current.side==side): 
                    # print("Comparing hits: ")
                    # current.printHit()
                    # next_.printHit()
                    split[idx] += 1
        return split

def getDiffIds(ids):
    ntids = len(ids)
    if ntids == 1 and 0 in ids:
        return [-1, -1]
    nhids = 0
    for tid, hid in ids.items():
        nhids_ = len(hid)
        if nhids_ > nhids:
            nhids = nhids_
    return [ntids, nhids]

def getHitsOnRibbonAndSideWithinNCols(hits, ncols):
    hitsOnRibbonAndSide = defaultdict(list)

    group_index = 0
    hitsOnRibbonAndSide[group_index].append(hits[0])
    for i in range(len(hits)):
        hit1 = hits[i]
        if i == len(hits)-1: j = i
        else: j = i + 1
        hit2 = hits[j]
        if (hit1==hit2): continue
        if (hit1.ribbon == hit2.ribbon and hit1.side == hit2.side and hit2.col - hit1.col <= ncols and hit2.col != hit1.col): 
            hitsOnRibbonAndSide[group_index].append(hit2)                      
        else:
            group_index += 1
            hitsOnRibbonAndSide[group_index].append(hit2)

    return hitsOnRibbonAndSide


ctr_dict = {"n_splitCls" : 0, "total_cls" : 0, "n_splitCls>1" : 0, "n_splitCls>2" : 0}

skip = args.skip

h_PU_density = ROOT.TH1D("h_PU_density", ";Pileup density;Counts", 4, 1.5, 5.5)

for i in tqdm(range(skip, nevents+skip)):
    mppcHitsTree.GetEntry(i)

    nh = mppcHitsTree.nh
    # print("### NEW Frame {} ###".format(i))

    tid_hid_map = defaultdict(list)
    ribbon_to_tid_hid_map = defaultdict(lambda: defaultdict(list))
    tid_hid_to_cl_numb = defaultdict(lambda: defaultdict(int))
    all_hits = []
    reco_cl_ids = []

    for j in range(nh):
        # Truth info
        crosstalk = mppcHitsTree.crosstalk[j]
        time_mc = mppcHitsTree.tmc[j]
        tid = mppcHitsTree.tid[j]
        hid = mppcHitsTree.hid[j]
        hid_g = mppcHitsTree.hid_g[j]

        # Non truth info
        time = mppcHitsTree.t[j]
        r = mppcHitsTree.r[j]
        phi = mppcHitsTree.phi[j]
        col = mppcHitsTree.col[j]
        amp = mppcHitsTree.amp[j]
        ribbon = mppcHitsTree.ribbon[j]
        side = mppcHitsTree.side[j]
        reco_cl_id = mppcHitsTree.cl_id[j]
        if (reco_cl_id not in reco_cl_ids): reco_cl_ids.append(reco_cl_id)
        zPos = mppcHitsTree.zPos[j]
        raw_time = mppcHitsTree.raw_time[j]

        reco_cl_ids.append(reco_cl_id)
        # print("TID {}, HID {}".format(tid,hid))
        if (tid not in tid_hid_map or hid not in tid_hid_map[tid]): tid_hid_map[tid].append(hid)
        if (tid not in ribbon_to_tid_hid_map[ribbon] or hid not in ribbon_to_tid_hid_map[ribbon][tid]): ribbon_to_tid_hid_map[ribbon][tid].append(hid)

        all_hits.append(mppcHit(ribbon, col, side, amp, time, raw_time, time_mc, crosstalk, tid, hid, hid_g, reco_cl_id, zPos))

    if (not len(all_hits)): continue

    reco_cl_ids = list(set(reco_cl_ids))

    reco_cls = []
    truth_cls = []
    truth_cls_L = []
    truth_cls_R = []
    # for tid, hids in tid_hid_map.iteritems():
    cl_num = 0

    for tid, hids in tid_hid_map.items():
        for hid in hids:
            tid_hid_to_cl_numb[tid][hid] = cl_num
            cl_num+=1

            cl = fbCluster(None, tid, hid)
            cl_L = fbCluster(None, tid, hid)
            cl_R = fbCluster(None, tid, hid)

            cl_L.tid = tid
            cl_L.hid = hid
            cl_R.tid = tid
            cl_R.hid = hid
            cl.tid = tid
            cl.hid = hid

            for hit in all_hits:
                if (tid == hit.tid and hid == hit.hid): 
                    cl.addHit(hit)
                    if (hit.side == -1): cl_L.addHit(hit)
                    if (hit.side == +1): cl_R.addHit(hit)

            if (len(cl.hits) >= 1) :truth_cls.append(cl)
            if (len(cl_L.hits) >= 1) :truth_cls_L.append(cl_L)
            if (len(cl_R.hits) >= 1) :truth_cls_R.append(cl_R)

    cleanTids = []
    for i in range(mppcHitsTree.nCleanTids):
        cleanTids.append(mppcHitsTree.cleanTids[i])

    reco_cls = []
    tid_hid_to_reco_cls = defaultdict(lambda: defaultdict(list))
    for cl_id in reco_cl_ids:
        cl = fbCluster(cl_id, None, None)
        for hit in all_hits:
            if (hit.reco_cl_id == cl_id): cl.addHit(hit)
        reco_cls.append(cl)

    for cl in reco_cls:
        [ntids, nhids] = getDiffIds(cl.getIds())
        if (cl.trackPU): h_PU_density.Fill(ntids)


    # truth_cls = []
    # # for tid, hids in tid_hid_map.iteritems():
    # for tid, hids in tid_hid_map.items():
    #     for hid in hids:
    #         cl = fbCluster(None, tid, hid)
    #         cl.tid = tid
    #         cl.hid = hid
    #         for hit in all_hits:
    #             if (tid == hit.tid and hid == hit.hid): 
    #                 cl.addHit(hit)

    #         if (len(cl.hits) >= 1) :truth_cls.append(cl)
    
    # for cl in truth_cls:
    #     if cl.prevFramePU: continue
        if (not cl.trackPU and not cl.recurlerPU and not cl.prevFramePU and cl.tid in cleanTids):
            tid_hid_to_reco_cls[cl.tid][cl.hid].append(cl)

    for reco_cl in reco_cls:
        # if (reco_cl.tid in cleanTids and not reco_cl.trackPU and not reco_cl.recurlerPU and not reco_cl.prevFramePU): 
        #     hClusterRes1Col.Fill(reco_cl.minTimeCorr()-reco_cl.mc_time)
        # print("NEW RECO CLUSTER")
        if reco_cl.trackPU:
            hTrackPUDensity.Fill(len(reco_cl.tids))
            if (len(reco_cl.tids)==2 and len(reco_cl.hids)==len(reco_cl.hidgs)==2): #and 0 not in reco_cl.hidgs? # i.e. ignore particles generated in fb's themselves??
                hits_tid_hid_map = getCrossingMap(reco_cl.hits, False)
                meanMCTimePerCrossing = getMeanMCTimeOfCrossings(hits_tid_hid_map, truth_cls, False)
                # print("\n")
                # reco_cl.printHits()
                # print(meanMCTimePerCrossing)
                # print(reco_cl.tids[0], hits_tid_hid_map[reco_cl.tids[0]][0])
                # print(reco_cl.tids[1], hits_tid_hid_map[reco_cl.tids[1]][0])
                # print(meanMCTimePerCrossing[reco_cl.tids[0]][hits_tid_hid_map[reco_cl.tids[0]][0]])
                # print(meanMCTimePerCrossing[reco_cl.tids[1]][hits_tid_hid_map[reco_cl.tids[1]][0]])
                # print(abs(meanMCTimePerCrossing[reco_cl.tids[1]][hits_tid_hid_map[reco_cl.tids[0]][0]]-meanMCTimePerCrossing[reco_cl.tids[0]][hits_tid_hid_map[reco_cl.tids[1]][0]]))
                hTrackPU_mctime_diff.Fill(abs(meanMCTimePerCrossing[reco_cl.tids[1]][hits_tid_hid_map[reco_cl.tids[1]][0]]-meanMCTimePerCrossing[reco_cl.tids[0]][hits_tid_hid_map[reco_cl.tids[0]][0]]))

            # print("TRACK PU IN RECO CL")
            # print("Number of TIDS {}, HIDS {}, HIDGS {}".format(len(reco_cl.tids), len(reco_cl.hids), len(reco_cl.hidgs)))
            # reco_cl.printHits()
        # if reco_cl.recurlerPU: 
        #     print("RECURLER PU IN RECO CL")
        #     print("Number of TIDS {}, HIDS {}, HIDGS {}".format(len(reco_cl.tids), len(reco_cl.hids), len(reco_cl.hidgs)))
        #     reco_cl.printHits()

    for cl in truth_cls:
        if (cl.tid not in cleanTids): continue
        split = cl.checkAdjCols()
        if ((split[0]==1 or split[1]==1) and cl.bothSides() and len(cl.hids)==len(cl.hidgs)):
            # hitTimesExcludingSplitHit = [[],[]]
            hitTimesExcludingSplitHit = []
            for idx, side in enumerate([-1, +1]):
                for current, next_ in pairwise(cl.hits):
                    if ((next_.col - current.col)==1 and current.side==next_.side and current.side==side): 
                        # hitTimesExcludingSplitHit[idx].append(hit.tCorr)
                        hitTimesExcludingSplitHit.append(current.tCorr)
                        hitTimesExcludingSplitHit.append(next_.tCorr)
                        
            if (not len(hitTimesExcludingSplitHit)): continue
            # cl.printHits()
            # print(hitTimesExcludingSplitHit)
            hSplitClusterIncludingHitRes.Fill(cl.minTimeCorr()-cl.mc_time)
            # if len(hitTimesExcludingSplitHit[0]) and len(hitTimesExcludingSplitHit[1]): hSplitClusterExcludingHitRes.Fill(min(min(hitTimesExcludingSplitHit[0]),min(hitTimesExcludingSplitHit[1]))-cl.hits[0].time_mc)
            # elif len(hitTimesExcludingSplitHit[0]): hSplitClusterExcludingHitRes.Fill(min(hitTimesExcludingSplitHit[0])-cl.hits[0].time_mc)
            # elif len(hitTimesExcludingSplitHit[1]): hSplitClusterExcludingHitRes.Fill(min(hitTimesExcludingSplitHit[1])-cl.hits[0].time_mc)
            hSplitClusterExcludingHitRes.Fill(min(hitTimesExcludingSplitHit)-cl.mc_time)



    reco_cls_w_2_cols = getHitsOnRibbonAndSideWithinNCols(all_hits,2)
    # if (len(reco_cls_w_2_cols)!=len(reco_cls)): print("WARNING")
    # for hit in all_hits: hit.printHit()
    reco_cls_w_2_cols_lst = []
    tid_hid_to_reco_w_2_cols_cls = defaultdict(lambda: defaultdict(list))
    for grp_idx, hits in reco_cls_w_2_cols.items():
    #     print(grp_idx)
        cl = fbCluster(grp_idx, hits[0].tid, hits[0].hid)
        for hit in hits: 
            cl.addHit(hit)
            # hit.printHit()
        reco_cls_w_2_cols_lst.append(cl)
        if (not cl.trackPU and not cl.recurlerPU and not cl.prevFramePU and cl.tid in cleanTids):
            tid_hid_to_reco_w_2_cols_cls[cl.tid][cl.hid].append(cl)


    for reco_cl_w_2_cols in reco_cls_w_2_cols_lst:            
        # if (reco_cl_w_2_cols.tid in cleanTids and not reco_cl_w_2_cols.trackPU and not reco_cl_w_2_cols.recurlerPU and not reco_cl_w_2_cols.prevFramePU): 
            # hClusterRes2Col.Fill(reco_cl_w_2_cols.minTimeCorr()-reco_cl_w_2_cols.mc_time)
        if reco_cl_w_2_cols.trackPU:
            hTrackPUDensity_2cols.Fill(len(reco_cl_w_2_cols.tids))

    # print("New..")
    for tid, clusters in tid_hid_to_reco_w_2_cols_cls.items():
        # print(tid)
        for hid, cl_list in clusters.items():
            # print(hid)
            if (len(cl_list)==2):
                minT = min(cl_list[0].minTimeCorr(), cl_list[1].minTimeCorr())
                minMC = min(cl_list[0].mc_time, cl_list[1].mc_time)
                hClusterRes2Col.Fill(minT-minMC)
                # for cl in cl_list:
                #     print(" --- ")
                #     cl.printHits()

    for tid, clusters in tid_hid_to_reco_cls.items():
        # print(tid)
        for hid, cl_list in clusters.items():
            # print(hid)
            if (len(cl_list)==2):
                minT = min(cl_list[0].minTimeCorr(), cl_list[1].minTimeCorr())
                minMC = min(cl_list[0].mc_time, cl_list[1].mc_time)
                hClusterRes1Col.Fill(minT-minMC)


    # # for hit in all_hits: hit.printHit()
    # # print([len(ribbon_to_tid_hid_map[i]) for i in range(12)])
    # ribbon_to_recurlTrk_count = defaultdict(int)
    # for item1 in ribbon_to_tid_hid_map.items():
    #     # print(item1, type(item1))
    #     ribbon, tid_to_hids = item1
    #     hid_count = 0
    #     for item2 in tid_to_hids.items():
    #         # print(item2)
    #         tid, hids = item2
    #         # print(len(hids))
    #         hid_count+=len(hids)

    #     ribbon_to_recurlTrk_count[ribbon] = hid_count

    # # print(ribbon_to_recurlTrk_count)

    # for trk in range(mppcHitsTree.nTrackIntersections):
    #     zIntersectionOnRibbon = mppcHitsTree.zIntersectionsOnRibbons[trk]
    #     tidOnRibbon = mppcHitsTree.tidsOnRibbons[trk]
    #     ribbonIdx = mppcHitsTree.ribbonIdx[trk]

    #     for cl in truth_cls:
    #         if (cl.ribbon == ribbonIdx and cl.tid == tidOnRibbon and ribbon_to_recurlTrk_count[ribbonIdx] == 1 and cl.bothSides()):
    #             # print("Filling for ribbon {}, tid {}".format(ribbonIdx,tidOnRibbon))
    #             min_tL, min_tR = cl.minTimePerSide()
    #             mean_tL, mean_tR = cl.meanTimePerSide()
    #             h_min_tL_tR_vs_reco_z.Fill(zIntersectionOnRibbon, min_tL-min_tR)
    #             h_mean_tL_tR_vs_reco_z.Fill(zIntersectionOnRibbon, mean_tL-mean_tR)
    
    # if (not len(all_hits)): continue

    # hitDims = []
    # y = []

    # for hit in all_hits:
    #     hitDims.append([hit.ribbon, hit.side, hit.col, hit.amplitude, hit.time])
    #     y.append(tid_hid_to_cl_numb[hit.tid][hit.hid])

    # hitDims=np.array(hitDims)
    # y=np.array(y)
    # pca = PCA(2)  # project from 5 to 2 dimensions
    # projected = pca.fit_transform(hitDims)
    # print(hitDims.shape)
    # print(projected.shape)

    # print("....")
    # print(ribbon_to_tid_hid_map)
    # print(tid_hid_to_cl_numb)

    # plt.scatter(projected[:, 0], projected[:, 1])
    # color = iter(plt.cm.rainbow(np.linspace(0, 1, cl_num)))
    # for label in np.unique(y):
    #     print(label)
    #     c = next(color)
    #     plt.scatter(projected[y==label, 0], projected[y==label, 1], label=label, color=c)
    #     plt.xlabel('component 1')
    #     plt.ylabel('component 2')  
    #     for i in range(len(projected[y==label,0])):
    #         plt.annotate(str(label), (projected[y==label, 0][i], projected[y==label, 1][i]),fontsize=11)

    # plt.legend(bbox_to_anchor=(1.15, 1.1))
    # plt.savefig("test.png")
    # # plt.show()
    # plt.close()
    # input("Press Enter to continue...")


    for cl in truth_cls:
        # ctr_dict["total_cls"] += 1
        # cl.printHits()
        # split = cl.checkAdjCols()
        # print("Is cluster split (>0)? {}".format(split))

        # if (split[0]>0 or split[1]>0): 
        #     # print("NEW CLUSTER, split {}".format(split))
        #     # cl.printHits()
        #     # print("\n")

        # if (split[0]>0 ): ctr_dict["n_splitCls"] += 1
        # if (split[1]>0 ): ctr_dict["n_splitCls"] += 1

        # if (split[0]>1 ): ctr_dict["n_splitCls>1"] += 1
        # if (split[1]>1 ): ctr_dict["n_splitCls>1"] += 1

        # if (split[0]>2 ): ctr_dict["n_splitCls>2"] += 1
        # if (split[1]>2 ): ctr_dict["n_splitCls>2"] += 1

        # print("Cl tid {}, side {}, ribbon {}".format(cl.tid, cl.side, cl.ribbon))
        for hit in cl.hits:
            # hit.printHit()
            x.setVal(hit.tCorr-hit.time_mc)
            rds_with_xTalk.add(ROOT.RooArgSet(x))
            if (hit.crosstalk==0): rds.add(ROOT.RooArgSet(x))

# data = np.array(data)
frame = x.frame(ROOT.RooFit.Title(""),ROOT.RooFit.Range(-2, 9.0))
frame.SetYTitle("Events")

# a = ROOT.RooRealVar("#gamma", "a", -0.9, -5, 5)
# b = ROOT.RooRealVar("#delta", "b", 0.8, 0.01, 5)
# loc = ROOT.RooRealVar("#mu", "loc", -0.4, -5.0, 5.0)
# scale = ROOT.RooRealVar("#lambda", "scale", 0.7, 0.01, 5.0)
# model = ROOT.RooJohnson("model","model",x,loc,scale,a,b)

# # Construct landau(t,ml,sl)
# ml = ROOT.RooRealVar("ml", "mean landau", 0.5, -3, 3)
# sl = ROOT.RooRealVar("sl", "sigma landau", 1, 0.1, 10)
# landau = ROOT.RooLandau("lx", "lx", x, ml, sl)
 
# # Construct gauss(t,mg,sg)
# mg = ROOT.RooRealVar("mg", "mg", 0, -0.1, 1.0)
# sg = ROOT.RooRealVar("sg", "sg", 2, 0.1, 10)
# gauss = ROOT.RooGaussian("gauss", "gauss", x, mg, sg)
# # Set #bins to be used for FFT sampling to 10000
# x.setBins(10000, "cache")
 
# # Construct landau (x) gauss
# model = ROOT.RooFFTConvPdf("lxg", "landau (X) gauss", x, landau, gauss)

# model.fitTo(rds_with_xTalk)
# a = ROOT.RooRealVar("a", "a", -1.5, -5, 5)
# b = ROOT.RooRealVar("b", "b", 0.9, 0.01, 5)
# loc = ROOT.RooRealVar("loc", "loc", -0.4, -5.0, 5.0)
# scale = ROOT.RooRealVar("scale", "scale", 0.3, 0.01, 1)
# nll = model.createNLL(rds_with_xTalk)

model = ROOT.RooJohnson("model","model",x,loc,scale,a,b)
x.setRange("fitRange", -2, 9.0)

model.fitTo(rds, ROOT.RooFit.Range("fitRange"))
# nll = model.createNLL(rds)
# minimizer = ROOT.RooMinimizer(nll)
# minimizer.setEps(1e-8)
# minimizer.migrad()

rds.plotOn(frame, ROOT.RooFit.DataError(0))
# rds_with_xTalk.plotOn(frame)
model.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kC0), ROOT.RooFit.NormRange("fitRange"), ROOT.RooFit.Range("fitRange"))
model.paramOn(frame, ROOT.RooFit.Format("NEU", ROOT.RooFit.AutoPrecision(1)), ROOT.RooFit.Layout(0.6,0.83,0.85), ROOT.RooFit.ShowConstants(False))
frame.getAttText().SetTextSize(0.95*frame.GetYaxis().GetLabelSize())
# print(frame.chiSquare())
# # rds.plotOn(frame)
# rds_with_xTalk.plotOn(frame)
# model.plotOn(frame)
frame.SetMinimum(2)
can = ROOT.TCanvas("can", "")
frame.Draw()
# can.SetLogy()
can.SaveAs(args.path+"/timeRes.pdf")

# a.setConstant(ROOT.kTRUE)
# b.setConstant(ROOT.kTRUE)
# loc.setConstant(ROOT.kTRUE)
# scale.setConstant(ROOT.kTRUE)

# Y_xTalk1 = ROOT.RooFormulaVar("Y_xTalk1","(x-(loc+5))/scale", ROOT.RooArgList(x,loc,scale))
# Y2_xTalk1 = ROOT.RooFormulaVar("Y2_xTalk1","pow(Y_xTalk1,2.0)", ROOT.RooArgList(Y_xTalk1))
# gauss_input_xTalk1 = ROOT.RooFormulaVar("gauss_input_xTalk1","a + b * TMath::Log(Y_xTalk1 + sqrt(Y2_xTalk1+1))", ROOT.RooArgList(Y_xTalk1,Y2_xTalk1,a,b))
# gauss_xTalk1 = ROOT.RooFormulaVar("gauss_xTalk1","exp(-0.5*pow(gauss_input_xTalk1,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_xTalk1))
# johnsonsu_pdf_xTalk1 = ROOT.RooFormulaVar("johnsonsu_pdf_xTalk1","(b / sqrt(Y2_xTalk1+1))*gauss_xTalk1/scale", ROOT.RooArgList(Y2_xTalk1,b,gauss_xTalk1,scale))

# Y_xTalk2 = ROOT.RooFormulaVar("Y_xTalk2","(x-(loc+3))/scale", ROOT.RooArgList(x,loc,scale))
# Y2_xTalk2 = ROOT.RooFormulaVar("Y2_xTalk2","pow(Y_xTalk2,2.0)", ROOT.RooArgList(Y_xTalk2))
# gauss_input_xTalk2 = ROOT.RooFormulaVar("gauss_input_xTalk2","a + b * TMath::Log(Y_xTalk2 + sqrt(Y2_xTalk2+1))", ROOT.RooArgList(Y_xTalk2,Y2_xTalk2,a,b))
# gauss_xTalk2 = ROOT.RooFormulaVar("gauss_xTalk2","exp(-0.5*pow(gauss_input_xTalk2,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_xTalk2))
# johnsonsu_pdf_xTalk2 = ROOT.RooFormulaVar("johnsonsu_pdf_xTalk2","(b / sqrt(Y2_xTalk2+1))*gauss_xTalk2/scale", ROOT.RooArgList(Y2_xTalk2,b,gauss_xTalk2,scale))

# Y_xTalk3 = ROOT.RooFormulaVar("Y_xTalk3","(x-(loc+8))/scale", ROOT.RooArgList(x,loc,scale))
# Y2_xTalk3 = ROOT.RooFormulaVar("Y2_xTalk3","pow(Y_xTalk3,2.0)", ROOT.RooArgList(Y_xTalk3))
# gauss_input_xTalk3 = ROOT.RooFormulaVar("gauss_input_xTalk3","a + b * TMath::Log(Y_xTalk3 + sqrt(Y2_xTalk3+1))", ROOT.RooArgList(Y_xTalk3,Y2_xTalk3,a,b))
# gauss_xTalk3 = ROOT.RooFormulaVar("gauss_xTalk3","exp(-0.5*pow(gauss_input_xTalk3,2.0)) / sqrt(2*TMath::Pi())", ROOT.RooArgList(gauss_input_xTalk3))
# johnsonsu_pdf_xTalk3 = ROOT.RooFormulaVar("johnsonsu_pdf_xTalk3","(b / sqrt(Y2_xTalk3+1))*gauss_xTalk3/scale", ROOT.RooArgList(Y2_xTalk3,b,gauss_xTalk3,scale))

# N = ROOT.RooRealVar("N", "N", 0.95, 0.85, 1.0)
# N_xTalk1 = ROOT.RooRealVar("N_xTalk1", "N_xTalk1", 0.04)
# N_xTalk2 = ROOT.RooRealVar("N_xTalk2", "N_xTalk2", 0.005, 0.0, 0.1)
# N_xTalk3 = ROOT.RooRealVar("N_xTalk3", "N_xTalk3", 0.005, 0.0, 0.1)
# model_xTalk = ROOT.RooAddPdf("model_xTalk","model_xTalk",ROOT.RooArgList(johnsonsu_pdf, johnsonsu_pdf_xTalk1, johnsonsu_pdf_xTalk2, johnsonsu_pdf_xTalk3), ROOT.RooArgList(N, N_xTalk1, N_xTalk2, N_xTalk3))
# model_xTalk.fitTo(rds_with_xTalk)
# TODO: Figure out what to do with xTalk...

# N = ROOT.RooRealVar("N", "N", 0.95, 0.85, 1.0)
# N_xTalk1 = ROOT.RooFormulaVar("N_xTalk1", "0.04*N", ROOT.RooArgList(N))
# N_xTalk2 = ROOT.RooFormulaVar("N_xTalk2", "", ROOT.RooArgList(N))
# N_xTalk3 = ROOT.RooFormulaVar("N_xTalk3", "N_xTalk1*N_xTalk2", ROOT.RooArgList(N_xTalk1,N_xTalk2))
# model = ROOT.RooGenericPdf("model","model", "N*johnsonsu_pdf + N_xTalk1*johnsonsu_pdf_xTalk1 + N_xTalk3*johnsonsu_pdf_xTalk2 + N_xTalk3*johnsonsu_pdf_xTalk3", ROOT.RooArgList(johnsonsu_pdf,scale,johnsonsu_pdf_xTalk1,johnsonsu_pdf_xTalk2,johnsonsu_pdf_xTalk3))
# model_xTalk.fitTo(rds_with_xTalk)


# First step
# For data w/o xTalk, fit N*johnson_pdf, fit 5 variables

# Second step
# Each time, fixed variables are a,b loc, and scale
# Status 1: N_xTalk1 * johnson_pdf_xTalk1(Y-5) [N_xTalk1 = 0.04*N]
# Status 2: N_xTalk2 * johnson_pdf_xTalk1(Y-3) [?]
# Status 3: N_xTalk3 * johnson_pdf_xTalk1(Y-8) [N_xTalk3 = N_xTalk1*N_xTalk2] 

# Fit 3 vars in total, Norm per status


# rds.plotOn(frame, ROOT.RooFit.Binning(100,-1,9))
# model.plotOn(frame,ROOT.RooFit.Normalization(rds.sumEntries(), ROOT.RooAbsReal.NumEvent))

# rds_with_xTalk.plotOn(frame, ROOT.RooFit.Binning(100,-1,9), ROOT.RooFit.MarkerColor(ROOT.kBlue))
# model_xTalk.plotOn(frame,ROOT.RooFit.Normalization(rds_with_xTalk.sumEntries(), ROOT.RooAbsReal.NumEvent))
# model_xTalk.plotOn(frame)

# frame.Draw()

# a.Print()
# b.Print()
# loc.Print()
# scale.Print()


# w = ROOT.RooWorkspace("w")
# getattr(w,'import')(model)
# w.Print()
# w.writeToFile("model.root")


# raw_input("Press Enter to continue...")
# input("Press Enter to continue...")
# print("Total clusters {}, N split clusters (either or both sides, N non-adj cols >= 1) {}, rate {:.2f}%".format(ctr_dict["total_cls"], ctr_dict["n_splitCls"], 100.0*ctr_dict["n_splitCls"]/float(ctr_dict["total_cls"])))
# print("Total clusters {}, N split clusters (either or both sides, N non-adj cols >= 2) {}, rate {:.2f}%".format(ctr_dict["total_cls"], ctr_dict["n_splitCls>1"], 100.0*ctr_dict["n_splitCls>1"]/float(ctr_dict["total_cls"])))
# print("Total clusters {}, N split clusters (either or both sides, N non-adj cols >= 3) {}, rate {:.2f}%".format(ctr_dict["total_cls"], ctr_dict["n_splitCls>2"], 100.0*ctr_dict["n_splitCls>2"]/float(ctr_dict["total_cls"])))

# print("Time resolution for 1 col seperation (min(t_cl) - t_mc (Z-corr)): Single gauss {:.1f} [ps], Double gauss: core {:.1f} [ps], wings {:.1f} [ps]".format(*[i*1e3 for i in fitGaus(hClusterRes1Col)]))
# print("Time resolution for 2 col seperation (min(t_cl) - t_mc (Z-corr)): Single gauss {:.1f} [ps], Double gauss: core {:.1f} [ps], wings {:.1f} [ps]".format(*[i*1e3 for i in fitGaus(hClusterRes2Col)]))
print("Time resolution for 1 col seperation (min(t_left, t_right) - tMC (Z-corr)): Single gauss {:.1f} [ps], Double gauss: core {:.1f} [ps], wings {:.1f} [ps]".format(*[i*1e3 for i in fitGaus(hClusterRes1Col)]))
print("Time resolution for 2 col seperation (min(t_left, t_right) - tMC (Z-corr)): Single gauss {:.1f} [ps], Double gauss: core {:.1f} [ps], wings {:.1f} [ps]".format(*[i*1e3 for i in fitGaus(hClusterRes2Col)]))

print("INCLUDING SPLIT HITS, FWHM/2.355: min (t_left, t_right) - tMC (z corr) = {:.0f} [ps]".format(getFWHM(hSplitClusterIncludingHitRes, 0.5)/2.355 * 1e3))
print("EXCLUDING SPLIT HITS, FWHM/2.355: min (t_left, t_right) - tMC (z corr) = {:.0f} [ps]".format(getFWHM(hSplitClusterExcludingHitRes, 0.5)/2.355 * 1e3))
print ("Finished...")

output_file.Write()
output_file.Close()
