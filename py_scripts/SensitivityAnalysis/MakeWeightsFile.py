import ROOT
import argparse

# Read in input files
parser = argparse.ArgumentParser(description='')
parser.add_argument('--BhabhaFiles', type=str, nargs="+", required=True, help='files')
parser.add_argument('--ICFiles', type=str, nargs="+", required=True, help='files')
parser.add_argument('--SignalFiles', type=str, nargs="+", required=True, help='files')
parser.add_argument('--output', '-o', type=str, default="weights.pickle", help='Output file')
args = parser.parse_args()


n_bhabha_files = 0
n_IC_files = 0
n_sig_files = 0

n_bhabha_frames_skim = 0
for file in args.BhabhaFiles:
    f = ROOT.TFile(file)
    n_bhabha_frames_skim += f.Get("n_bhabha_frames_skim").GetVal()
    n_bhabha_files += f.Get("n_bhabha_files").GetVal()
    f.Close()

print(f"n_bhabha_frames_skim: {n_bhabha_frames_skim}, n_bhabha_files: {n_bhabha_files}")

IC_weight_sum = 0.0
for file in args.ICFiles:
    f = ROOT.TFile(file)
    IC_weight_sum += f.Get("IC_weight_sum").GetVal()
    n_IC_files += f.Get("n_IC_files").GetVal()
    f.Close()

print(f"IC_weight_sum: {IC_weight_sum}, n_IC_files: {n_IC_files}")

n_signal_events = 0
for file in args.SignalFiles:
    f = ROOT.TFile(file)
    n_signal_events += f.Get("n_signal_events").GetVal()
    n_sig_files += f.Get("n_sig_files").GetVal()
    f.Close()

print(f"n_signal_events: {n_signal_events}, n_sig_files: {n_sig_files}")

# Make dictionary of weights/counts
weights = {}
weights["n_bhabha_frames_skim"] = n_bhabha_frames_skim
weights["IC_weight_sum"] = IC_weight_sum
weights["n_signal_events"] = n_signal_events
weights["n_bhabha_files"] = n_bhabha_files
weights["n_IC_files"] = n_IC_files
weights["n_sig_files"] = n_sig_files

# Save dictionary to pickle file
import pickle
with open(args.output, 'wb') as handle:
    pickle.dump(weights, handle, protocol=pickle.HIGHEST_PROTOCOL)
