#!/bin/sh
set -e
cd build && make -j8 && make install
VERSION=V04
DIR=/unix/muons/users/bgayther/CosmicRun2022/data/analyzerOnBE/${VERSION}
FILES=$DIR/reco_output/mu3e_cosmic_run_reco_*.root
echo $FILES
/home/bgayther/opt/bin/mu3eTrirecAnaCosmic --inputFiles $FILES --output /unix/muons/users/bgayther/CosmicRun2022/data/CosRunAnalysis_${VERSION} --outputGraphsDir /unix/muons/users/bgayther/CosmicRun2022/data/graphs${VERSION} --linkMaskFile /unix/muons/users/bgayther/CosmicRun2022/data/analyzerOnBE/runs_to_link_masks_per_simChip.json
FILES=$DIR/root_output_files/OnlineMonitor*.root
/home/bgayther/opt/bin/CombinedRunHistos --inputFiles $FILES --output /unix/muons/users/bgayther/CosmicRun2022/data/CombinedRecoHistos_${VERSION} --outputGraphsDir /unix/muons/users/bgayther/CosmicRun2022/data/graphs${VERSION}

# /home/bgayther/opt/bin/mu3eTrirecAnaCosmic --inputFiles /unix/muons/users/bgayther/CosmicRun2022/data/run00042-reco_customGeom.root --output /unix/muons/users/bgayther/CosmicRun2022/data/CosRunAnalysis_sim --outputGraphsDir /unix/muons/users/bgayther/CosmicRun2022/data/graphs_sim

# FILES=/unix/muons/users/bgayther/CosmicRun2022/data/analyzerOnBE/1259cor.root
# echo $FILES
# /home/bgayther/opt/bin/CombinedRunHistos --inputFiles $FILES --output /unix/muons/users/bgayther/CosmicRun2022/data/CombinedRunHistos_${VERSION} --outputGraphsDir /unix/muons/users/bgayther/CosmicRun2022/data/graphs${VERSION}
