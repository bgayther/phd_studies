import json

simToIntRunFile = "simChipToIntRunChip.json" #Marius json file
with open(simToIntRunFile) as file:
    data = json.load(file)

newFile = "holes.json"
newJsonData = {"holes" : {}}
print("Sim chip id to intRun chip id")
print(data)

# from cosmicRun image
holes_list_intRun_chips = [17,77,6,71,39,40,41,99,28,29,51,23,43,44,104,103,102,74,73,72,0,60,4,65,36,37,38,98,96,25,86,85,110,32,33,35,45,46,47,107,106,105]

for simId, intRunId in data.items():
    if (intRunId in holes_list_intRun_chips):
        newJsonData["holes"][simId] = 0 #value doesn't matter currently

print("Sim chip ids to be masked")
print(newJsonData)
json_object = json.dumps(newJsonData, indent = 4) 
with open(newFile, "w") as outfile: 
    outfile.write(json_object) 
