import ROOT
import argparse
import os
from array import array
import json
import numpy as np
from tqdm import tqdm

parser = argparse.ArgumentParser(description='Timing Resolution',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-i','--inputs', type=str, nargs='+', required=True, help='Input reco file names')
parser.add_argument('--path', type=str, default="/ben_dev/mu3e_cosmicRun/mu3e/tests/data", help='path/to/reco_file.root (not including file)')
parser.add_argument('--rateIndependent', dest='rateIndependent', default=False, action='store_true')
parser.add_argument('--output', type=str, default='fakesVsNoise', help='Set name of output .root file')
args = parser.parse_args()

os.chdir(args.path)
ROOT.gROOT.SetBatch(True)

pid_and_type_cut = "(mc_type==3||mc_type==4)&&abs(mc_pid)==13"

n_bins = len(args.inputs)
# all_fr = [0.0]*n_bins
# S4_fr = [0.0]*n_bins
# S3_fr = [0.0]*n_bins
# noises = [0.0]*n_bins
# avgNoisyHits = [0.0]*n_bins
all_fr = []
S4_fr = []
S3_fr = []
noises = []
avgNoisyHits = []

for i, file in enumerate(args.inputs):
    print ("Adding {} for analysis".format(file))

    try:
        ROOT.TFile.Open(file)
    except:
        continue

    if (not args.rateIndependent):
        segs_df = ROOT.RDataFrame("segs", file).Filter("mc==1&&"+pid_and_type_cut)
        segsFake_df = ROOT.RDataFrame("segs", file).Filter("mc==0")

        all_fake_rate = segsFake_df.Count().GetValue() / segs_df.Count().GetValue()
        S4_fake_rate = segsFake_df.Filter("nhit==4").Count().GetValue() / segs_df.Filter("nhit==4").Count().GetValue()
        S3_fake_rate = segsFake_df.Filter("nhit==3").Count().GetValue() / segs_df.Filter("nhit==3").Count().GetValue()

        all_fr.append(all_fake_rate)
        S4_fr.append(S4_fake_rate)
        S3_fr.append(S3_fake_rate)
    else:
        all_fakes_ctr = 0
        s4_fakes_ctr = 0
        s3_fakes_ctr = 0

        framesTree = ROOT.TChain("frames")
        framesTree.Add(file)
        framesTree.SetBranchStatus("*", 0)
        framesTree.SetBranchStatus("n", 1)
        framesTree.SetBranchStatus("mc", 1)
        framesTree.SetBranchStatus("nhit", 1)
        framesTree.SetBranchStatus("nhit", 1)
        framesTree.SetBranchStatus("nGenuineHits", 1)
        framesTree.SetBranchStatus("nNoisyHits", 1)
        
        # for entry in tqdm(range(framesTree.GetEntries())):
        for entry in range(framesTree.GetEntries()):
            framesTree.GetEntry(entry)
            # if framesTree.nGenuineHits==0 and framesTree.nNoisyHits>0: 
            for i in range(framesTree.n):
                if (not framesTree.mc[i]):
                    all_fakes_ctr += 1
                    if (framesTree.nhit[i]==4): s4_fakes_ctr += 1
                    elif (framesTree.nhit[i]==3): s3_fakes_ctr += 1

        all_fr.append(all_fakes_ctr/float(framesTree.GetEntries()))
        S4_fr.append(s4_fakes_ctr/float(framesTree.GetEntries()))
        S3_fr.append(s3_fakes_ctr/float(framesTree.GetEntries()))

    cfg = json.loads(str(ROOT.TFile.Open(file).Get("config").GetString()))
    if (not cfg["trirec"].get("expert") is None): noises.append(float(cfg["trirec"]["expert"]["noise"]))
    else: noises.append(0.0)

    avgNoisyHits.append(float(ROOT.RDataFrame("frames", file, {"nNoisyHits"}).Mean("nNoisyHits").GetValue()))
    print("Noise rate {:.2f}, avg. noisy hits per frame {:.2f}, fake rate: S4 {}, S3 {}".format(noises[-1], avgNoisyHits[-1], S4_fr[-1], S3_fr[-1]))


# Just in case not given in sorted order
noises = np.array(noises)
avgNoisyHits = np.array(avgNoisyHits)
all_fr = np.array(all_fr)
S4_fr = np.array(S4_fr)
S3_fr = np.array(S3_fr)

inds = noises.argsort()
noises.sort()

all_fr = all_fr[inds]
S4_fr = S4_fr[inds]
S3_fr = S3_fr[inds]
avgNoisyHits = avgNoisyHits[inds]

# print(noises, S4_fr, S3_fr)

output_file = ROOT.TFile(args.output+".root", 'RECREATE')

# Fake rates vs noise per pixel rates
all_fakeRate_vs_noise = ROOT.TGraph(n_bins, array('d', noises), array('d', all_fr))
all_fakeRate_vs_noise.SetName("all_fakeRate_vs_noise")
all_fakeRate_vs_noise.GetXaxis().SetTitle("Noise per pixel [Hz]")
if (not args.rateIndependent): all_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (All tracks)")
else: all_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
all_fakeRate_vs_noise.Write()

S4_fakeRate_vs_noise = ROOT.TGraph(n_bins, array('d', noises), array('d', S4_fr))
S4_fakeRate_vs_noise.SetName("S4_fakeRate_vs_noise")
S4_fakeRate_vs_noise.GetXaxis().SetTitle("Noise per pixel [Hz]")
if (not args.rateIndependent): S4_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (S4 tracks)")
else: S4_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
S4_fakeRate_vs_noise.Write()

S3_fakeRate_vs_noise = ROOT.TGraph(n_bins, array('d', noises), array('d', S3_fr))
S3_fakeRate_vs_noise.SetName("S3_fakeRate_vs_noise")
S3_fakeRate_vs_noise.GetXaxis().SetTitle("Noise per pixel [Hz]")
if (not args.rateIndependent): S3_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (S3 tracks)")
else: S3_fakeRate_vs_noise.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
S3_fakeRate_vs_noise.Write()

# Fake rates vs avg num noisy hits per frame
all_fakeRate_vs_nNoisyHits = ROOT.TGraph(n_bins, array('d', avgNoisyHits), array('d', all_fr))
all_fakeRate_vs_nNoisyHits.SetName("all_fakeRate_vs_nNoisyHits")
all_fakeRate_vs_nNoisyHits.GetXaxis().SetTitle("Avg. Number of noisy hits per frame")
if (not args.rateIndependent): all_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (All tracks)")
else: all_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
all_fakeRate_vs_nNoisyHits.Write()

S4_fakeRate_vs_nNoisyHits = ROOT.TGraph(n_bins, array('d', avgNoisyHits), array('d', S4_fr))
S4_fakeRate_vs_nNoisyHits.SetName("S4_fakeRate_vs_nNoisyHits")
S4_fakeRate_vs_nNoisyHits.GetXaxis().SetTitle("Avg. Number of noisy hits per frame")
if (not args.rateIndependent): S4_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (S4 tracks)")
else: S4_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
S4_fakeRate_vs_nNoisyHits.Write()

S3_fakeRate_vs_nNoisyHits = ROOT.TGraph(n_bins, array('d', avgNoisyHits), array('d', S3_fr))
S3_fakeRate_vs_nNoisyHits.SetName("S3_fakeRate_vs_nNoisyHits")
S3_fakeRate_vs_nNoisyHits.GetXaxis().SetTitle("Avg. Number of noisy hits per frame")
if (not args.rateIndependent): S3_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [fake/true ratio]  (S3 tracks)")
else: S3_fakeRate_vs_nNoisyHits.GetYaxis().SetTitle("Fake rate [avg no. of fakes per frame]  (All tracks)")
S3_fakeRate_vs_nNoisyHits.Write()

min_y = min(S4_fr) if min(S4_fr)!=0 else 1e-6

ROOT.gStyle.SetImageScaling(3.)

# Combined fake rate vs noise rate plots
if (not args.rateIndependent): mg = ROOT.TMultiGraph("mg",";Noise per pixel [Hz]; Fake rate [fake/true ratio]")
else: mg = ROOT.TMultiGraph("mg",";Noise per pixel [Hz]; Fake rate [avg no. of fakes per frame]")
can = ROOT.TCanvas("can", "can", 1200, 600)

S4_fakeRate_vs_noise.SetLineColor(38)
S4_fakeRate_vs_noise.SetMarkerColor(38)
S4_fakeRate_vs_noise.SetLineWidth(3)
S4_fakeRate_vs_noise.SetMarkerStyle(21)
S4_fakeRate_vs_noise.SetMarkerSize(0.8)
S4_fakeRate_vs_noise.SetLineStyle(2)

S3_fakeRate_vs_noise.SetLineStyle(2)
S3_fakeRate_vs_noise.SetMarkerSize(0.8)
S3_fakeRate_vs_noise.SetMarkerStyle(21)
S3_fakeRate_vs_noise.SetLineWidth(3)
S3_fakeRate_vs_noise.SetMarkerColor(46)
S3_fakeRate_vs_noise.SetLineColor(46)

mg.Add(S4_fakeRate_vs_noise,"PL")
mg.Add(S3_fakeRate_vs_noise, "PL")
mg.Draw("A")

ROOT.gPad.Modified()
mg.GetYaxis().SetRangeUser(min_y,max(S3_fr))

leg = ROOT.TLegend(0.80,0.65,0.85,0.75)
leg.AddEntry(S4_fakeRate_vs_noise,"S4","l")
leg.AddEntry(S3_fakeRate_vs_noise,"S3","l")
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.035)
leg.Draw()

ROOT.gPad.Update()
ROOT.gPad.SetLogy()
# ROOT.gPad.SetLogx(1)
can.SaveAs("combined_fakeRate_vs_noise.png")

# Combined fake rate vs num noisy hits plots
if (not args.rateIndependent): mg = ROOT.TMultiGraph("mg",";Avg. Number of noisy hits per frame; Fake rate [fake/true ratio]")
else: mg = ROOT.TMultiGraph("mg",";Avg. Number of noisy hits per frame; Fake rate [avg no. of fakes per frame]")
can = ROOT.TCanvas("can", "can", 1200, 600)

S4_fakeRate_vs_nNoisyHits.SetLineColor(38)
S4_fakeRate_vs_nNoisyHits.SetMarkerColor(38)
S4_fakeRate_vs_nNoisyHits.SetLineWidth(3)
S4_fakeRate_vs_nNoisyHits.SetMarkerStyle(21)
S4_fakeRate_vs_nNoisyHits.SetMarkerSize(0.8)
S4_fakeRate_vs_nNoisyHits.SetLineStyle(2)

S3_fakeRate_vs_nNoisyHits.SetLineStyle(2)
S3_fakeRate_vs_nNoisyHits.SetMarkerSize(0.8)
S3_fakeRate_vs_nNoisyHits.SetMarkerStyle(21)
S3_fakeRate_vs_nNoisyHits.SetLineWidth(3)
S3_fakeRate_vs_nNoisyHits.SetMarkerColor(46)
S3_fakeRate_vs_nNoisyHits.SetLineColor(46)

mg.Add(S4_fakeRate_vs_nNoisyHits,"PL")
mg.Add(S3_fakeRate_vs_nNoisyHits, "PL")
mg.Draw("A")

ROOT.gPad.Modified()
mg.GetYaxis().SetRangeUser(min_y,max(S3_fr))

leg = ROOT.TLegend(0.80,0.65,0.85,0.75)
leg.AddEntry(S4_fakeRate_vs_nNoisyHits,"S4","l")
leg.AddEntry(S3_fakeRate_vs_nNoisyHits,"S3","l")
leg.SetBorderSize(0)
leg.SetTextFont(42)
leg.SetTextSize(0.035)
leg.Draw()

ROOT.gPad.Update()
ROOT.gPad.SetLogy()
# ROOT.gPad.SetLogx(1)
can.SaveAs("combined_fakeRate_vs_noisyHits.png")

output_file.Write()
output_file.Close()
