
import ROOT
import numpy as np
from collections import defaultdict
from array import array

# Have some constants here?...
fb_length = 287.75
fb_len_half = fb_length/2.0
c = 299.792 # [mm/ns]
n = 1.59 # get from digi.json!
cn = c/n

class FbCluster:
    timeMax = 2**31
    def __init__(self, sipm, sipmno, ids, isLab = 0):
        # self.timesL = []
        # self.timesR = []
        if sipm.sipm[sipmno]%2==0:
            self.NcolL = 1
            self.NcolR = 0

            self.ampL = sipm.amplitude[sipmno]
            self.ampR = 0

            self.maxAmpL = self.ampL
            self.maxAmpR = 0


            self.timeL = sipm.time[sipmno]
            self.timeR = self.timeMax
            # self.timesL.append(self.timeL)

            self.maxAmpTimeL = self.timeL
            self.maxAmpTimeR = self.timeMax

            self.timeSumL = self.timeL
            self.timeSumR = 0.0

            self.weightedAmpTimeL = self.timeL*self.ampL
            self.weightedAmpTimeR = 0.0

        else:
            self.NcolL = 0
            self.NcolR = 1

            self.ampL = 0
            self.ampR = sipm.amplitude[sipmno]

            self.maxAmpL = 0
            self.maxAmpR = self.ampR

            self.timeL = self.timeMax
            self.timeR = sipm.time[sipmno]
            # self.timesR.append(self.timeR)

            self.maxAmpTimeL = self.timeMax
            self.maxAmpTimeR = self.timeR

            self.timeSumL = 0.0
            self.timeSumR = self.timeR

            self.weightedAmpTimeL = 0.0
            self.weightedAmpTimeR = self.timeR*self.ampR

        self.ids = ids

        self.times = [] 
        self.time = sipm.time[sipmno]
        self.times.append(self.time)

        self.amp = sipm.amplitude[sipmno]
        self.ampls = []
        self.ampls.append(self.amp)

        self.nfb = sipm.nfibres[sipmno]
        self.nfbs = []
        self.nfbs.append(self.nfb)

        self.cols = []
        self.col = sipm.col[sipmno]
        self.cols.append(self.col)

        self.time_mc = sipm.getMcTime(sipmno)
        self.z = sipm.getZPos(sipmno)
        self.isLab = isLab
        self.Nhits = 1

        self.side = sipm.sipm[sipmno]%2

        mppc_id = ((sipm.sipm[sipmno] >> 1) << 8) + ((sipm.col[sipmno] & 0x7F) << 1) + (sipm.sipm[sipmno] & 0x01)
        self.ribbon = (mppc_id >> 8) & 0x1F

        if sipm.mchits:
            mcHit = sipm.mcHit
            mcHit.load(sipm.mci[sipmno])

            z = mcHit.pos_g_z[0]
            lx = mcHit.pos_l_x[0]
            p_rt = np.hypot(mcHit.p_in_x[0], mcHit.p_in_y[0])

            self.p_rt = p_rt
            self.p_theta = np.arctan2(mcHit.p_in_z[0], p_rt)
            self.p_phi = np.arctan2(mcHit.p_in_y[0], mcHit.p_in_x[0])
            self.phi = np.arctan2(mcHit.pos_g_y[0], mcHit.pos_g_x[0])
            self.dphi = np.fmod((self.p_phi - self.phi + np.pi), 2*np.pi) - np.pi

        else:
            self.p_theta = np.nan
            self.p_phi   = np.nan
            self.phi     = np.nan
            self.dphi    = np.nan

    def setBranch(self,tree):
        tree.SetBranch("")

    def add(self, hit):
        self.NcolL += hit.NcolL
        self.NcolR += hit.NcolR
        self.ampL += hit.ampL
        self.ampR += hit.ampR
        self.Nhits += 1

        self.times.append(hit.time)
        self.ampls.append(hit.amp)
        self.nfbs.append(hit.nfb)
        self.cols.append(hit.col)

        for tid, hids in hit.ids.items():
            if tid not in self.ids:
                self.ids[tid] = hids
            else:
                for hid in hids:
                    if hid not in self.ids[tid]:
                        self.ids[tid].append(hid)

        if hit.ampL > self.maxAmpL:
            self.maxAmpL = hit.ampL
            self.maxAmpTimeL = hit.timeL
        if hit.ampR > self.maxAmpR:
            self.maxAmpR = hit.ampR
            self.maxAmpTimeR = hit.timeR

        if hit.timeL != self.timeMax:
            # self.timesL.append(hit.timeL)
            self.timeSumL += hit.timeL
            self.weightedAmpTimeL += hit.timeL*hit.ampL

        if hit.timeR != self.timeMax: 
            # self.timesR.append(hit.timeR)
            self.timeSumR += hit.timeR
            self.weightedAmpTimeR += hit.timeR*hit.ampR

        if hit.timeL < self.timeL:
            self.timeL = hit.timeL
        if hit.timeR < self.timeR:
            self.timeR = hit.timeR

        if hit.time_mc < self.time_mc:
            self.time_mc = hit.time_mc
        # if hit.time_mc > self.time_mc:
        #     self.time_mc = hit.time_mc

        if (self.isLab == 0) or (hit.isLab == 0):
            self.isLab = 0

    def bothSides(self):
        if (self.ampL > 0) and (self.ampR > 0):
            return True
        else:
            return False
 
    def unweightedMeanTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        if self.bothSides():
            self.timeL = self.timeSumL/float(self.NcolL)
            self.timeR = self.timeSumR/float(self.NcolR)

    def getMeanTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        return [self.timeSumL/float(self.NcolL), self.timeSumR/float(self.NcolR)]

    def maxAmplTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        if self.bothSides():
            self.timeL = self.maxAmpTimeL
            self.timeR = self.maxAmpTimeR

    def getMaxAmplTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        return [self.maxAmpTimeL, self.maxAmpTimeR]

    def weightedAmplTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        if self.bothSides():
            self.timeL = self.weightedAmpTimeL/float(self.ampL)
            self.timeR = self.weightedAmpTimeR/float(self.ampR)

    def getWeightedAmplTimes(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return 
        return [self.weightedAmpTimeL/float(self.ampL), self.weightedAmpTimeR/float(self.ampR)]

    def getPileup(self):
        nhids = 0
        if -1 in self.ids: # pileup and no mchits
            return [-1, -1]
        for tid, hids in self.ids.items():
            nhids_ = len(hids)
            if nhids < nhids_:
                nhids = nhids_
        return [len(self.ids), nhids]

    def getTimeDiff(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return self.timeMax
        return (self.timeL - self.timeR)

    def getTimeMean(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return self.timeMax
        return (self.timeL + self.timeR)/2.0 - self.time_mc

    def getDistanceToSides(self):
        dL = fb_len_half - self.z
        dR = fb_len_half + self.z
        return [dL, dR]

    def getCorrectedTimes(self):
        [dL, dR] = self.getDistanceToSides()
        tL = self.timeL - dL/cn if self.timeL != self.timeMax else self.timeMax
        tR = self.timeR - dR/cn if self.timeR != self.timeMax else self.timeMax
        return [tL, tR]

    def getTimeDiffCorrected(self):
        [tL, tR] = self.getCorrectedTimes()
        if (tL == self.timeMax) or (tR == self.timeMax):
            return self.timeMax
        return (tL - tR)

    def getTimeMeanCorrected(self):
        [tL, tR] = self.getCorrectedTimes()
        if (tL == self.timeMax) or (tR == self.timeMax):
            return self.timeMax
        return (tL + tR)/2. - self.time_mc

    def getTimeMinAndMc(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return self.timeMax
        return min(self.timeL - self.time_mc, self.timeR - self.time_mc)
    
    def getTimeMc(self):
        if (self.timeL==self.timeMax) or (self.timeR==self.timeMax):
            return self.timeMax 
        return self.time_mc

    def getTimeMinMcCorrected(self):
        [tL, tR] = self.getCorrectedTimes()
        if (tL == self.timeMax) or (tR == self.timeMax):
            return self.timeMax
        return min(tL - self.time_mc, tR - self.time_mc)


class Frame:
    frameLength = 64.0
    def __init__(self, tree, mcTree = 0):
        self.traj =  Frame.Traj(tree)
        self.sipm =  Frame.SiPM(tree, mcTree)
        self.header = Frame.Header(tree)


    class Header:
        event = np.zeros(1, dtype=np.int32)
        def __init__(self, tree):
            tree.SetBranchStatus("Header", 1)
            tree.SetBranchAddress("Header", self.event)


    class Traj:
        vx = ROOT.vector('double')()
        vy = ROOT.vector('double')()
        vz = ROOT.vector('double')()
        px = ROOT.vector('double')()
        py = ROOT.vector('double')()
        pz = ROOT.vector('double')()
        id = ROOT.vector('int')()
        type = ROOT.vector('int')()
        fbhid = ROOT.vector('int')()
        pid = ROOT.vector('int')()

        def __init__(self, tree):
            tree.SetBranchStatus("traj_vx", 1)
            tree.SetBranchStatus("traj_vy", 1)
            tree.SetBranchStatus("traj_vz", 1)
            tree.SetBranchStatus("traj_px", 1)
            tree.SetBranchStatus("traj_py", 1)
            tree.SetBranchStatus("traj_pz", 1)
            tree.SetBranchStatus("traj_ID", 1)
            tree.SetBranchStatus("traj_type", 1)
            tree.SetBranchStatus("traj_fbhid", 1)
            tree.SetBranchStatus("traj_PID", 1)
            tree.SetBranchAddress("traj_vx", self.vx)
            tree.SetBranchAddress("traj_vy", self.vy)
            tree.SetBranchAddress("traj_vz", self.vz)
            tree.SetBranchAddress("traj_px", self.px)
            tree.SetBranchAddress("traj_py", self.py)
            tree.SetBranchAddress("traj_pz", self.pz)
            tree.SetBranchAddress("traj_ID", self.id)
            tree.SetBranchAddress("traj_type", self.type)
            tree.SetBranchAddress("traj_fbhid", self.fbhid)
            tree.SetBranchAddress("traj_PID", self.pid)

        def getN(self):
            return self.vx.size()

        def getVr2(self, i):
            return self.vx[i]**2.0 + self.vy[i]**2.0

        def getVr(self, i):
            return np.sqrt(self.getVr2(i))

        def getPt2(self, i):
            return self.px[i]**2.0 + self.py[i]**2.0

        def getPt(self, i):
            return np.sqrt(self.getPt2(i))

        def getTids(self, fb = True, lab=False):
            tids = np.array([], dtype=int)
            types = np.array([], dtype=int)

            for traj_no in range(self.getN()):
                if (fb == False) or (self.fbhid[traj_no] > 0): # mc traj with fb hit
                    # Target cut (get info from internal digi.json)
                    if (abs(self.vz[traj_no])<50) and (self.getVr(traj_no) < 19): # vertex in inner volume
                        if self.getPt(traj_no) > 10: # pt>10
                            if (lab):
                                theta = np.arctan2(self.pz[traj_no], np.sqrt(self.getPt2(traj_no)))
                                if (abs(theta/np.pi*180) < 18) and (self.pid[traj_no]==211):
                                  tids = np.append(tids, int(self.id[traj_no]))
                                  types = np.append(types, int(self.type[traj_no]))
                            else:
                                tids = np.append(tids, int(self.id[traj_no]))
                                types = np.append(types, int(self.type[traj_no]))
            return [tids, types]

    class SiPM:
        sipm = ROOT.vector('int')()
        col = ROOT.vector('int')()
        mci = ROOT.vector('int')()
        mcn = ROOT.vector('int')()
        amplitude = ROOT.vector('int')()
        crosstalk = ROOT.vector('int')()
        time = ROOT.vector('double')()
        time_prod = ROOT.vector('double')()
        pathlength = ROOT.vector('double')()
        nfibres = ROOT.vector('int')()
        mchits = False # MCHits present

        def __init__(self, tree, mcTree = 0):
            tree.SetBranchStatus("fibremppc_mppc", 1)
            tree.SetBranchStatus("fibremppc_col", 1)
            tree.SetBranchStatus("fibremppc_amplitude", 1)
            tree.SetBranchStatus("fibremppc_time", 1)
            tree.SetBranchStatus("fibremppc_crosstalk", 1)
            tree.SetBranchStatus("fibremppc_time_prod", 1)
            tree.SetBranchStatus("fibremppc_pathlength", 1)
            tree.SetBranchStatus("fibremppc_nfibres", 1)
            tree.SetBranchAddress("fibremppc_nfibres", self.nfibres)
            tree.SetBranchAddress("fibremppc_mppc", self.sipm)
            tree.SetBranchAddress("fibremppc_col", self.col)
            tree.SetBranchAddress("fibremppc_amplitude", self.amplitude)
            tree.SetBranchAddress("fibremppc_time", self.time)
            tree.SetBranchAddress("fibremppc_time_prod", self.time_prod)
            tree.SetBranchAddress("fibremppc_pathlength", self.pathlength)
            tree.SetBranchAddress("fibremppc_crosstalk", self.crosstalk)

            if tree.GetBranch("fibrehit_mc_i"):
                tree.SetBranchStatus("fibremppc_mc_i", 1)
                tree.SetBranchStatus("fibremppc_mc_n", 1)
                tree.SetBranchAddress("fibremppc_mc_i", self.mci)
                tree.SetBranchAddress("fibremppc_mc_n", self.mcn)
                self.mchits = True

            self.mcHit = MCHit(mcTree)

        def getN(self):
            return self.sipm.size()

        def getTid(self, i):
            tid = 0
            if self.mchits:
                if self.mcn[i] == 1:
                    self.mcHit.load(self.mci[i])
                    tid = self.mcHit.tid[0]
            return tid

        def getHid(self, i):
            hid = 0
            if self.mchits:
                if self.mcn[i] == 1:
                    self.mcHit.load(self.mci[i])
                    hid = self.mcHit.hid_g[0]
            return hid

        def getMcTime(self, i):
            time = 0
            if self.mchits:
                self.mcHit.load(self.mci[i])
                time = self.mcHit.time[0]
            return time

        def getZPos(self, i):
            z = 0.0
            if self.mchits:
                for k in range(self.mcn[i]): 
                    self.mcHit.load(self.mci[i] + k)
                    z += self.mcHit.pos_l_z[0]
                return z/self.mcn[i]
            return 0

        def getIds(self, i): 
            ''' returns tids and hids of hit i '''
            ids = defaultdict(list) #[tid][hids]
            tid = self.getTid(i)
            if(tid != 0):
                ids[tid].append(self.getHid(i))
            elif self.mchits:
                ids[0] = [self.getHid(i)]
                # for k in range(self.mcn[i]): 
                #     self.mcHit.load(self.mci[i] + k)
                #     tid = self.mcHit.tid[0]
                #     hid = self.mcHit.hid[0]
                #     if tid not in ids or hid not in ids[tid]:
                #         ids[tid].append(hid)
            else:
                ids[0] = [self.getHid(i)]

            return ids


class MCHit:
    tid = np.zeros(1, dtype=np.int32)
    hid = np.zeros(1, dtype=np.int32)
    hid_g = np.zeros(1, dtype=np.int32)
    pdg = np.zeros(1, dtype=np.int32)
    edep = np.zeros(1, dtype=float)
    time = np.zeros(1, dtype=float)

    pos_l_x = np.zeros(1, dtype=float)
    pos_l_z = np.zeros(1, dtype=float)
    pos_g_x = np.zeros(1, dtype=float)
    pos_g_y = np.zeros(1, dtype=float)
    pos_g_z = np.zeros(1, dtype=float)
    p_in_x = np.zeros(1, dtype=float)
    p_in_y = np.zeros(1, dtype=float)
    p_in_z = np.zeros(1, dtype=float)


    def __init__(self, tree):
        if(tree):
            tree.SetBranchStatus("tid", 1)
            tree.SetBranchStatus("hid", 1)
            tree.SetBranchStatus("hid_g", 1)
            tree.SetBranchStatus("pdg", 1)
            tree.SetBranchStatus("edep", 1)
            tree.SetBranchStatus("time", 1)
            tree.SetBranchAddress("tid", self.tid)
            tree.SetBranchAddress("hid", self.hid)
            tree.SetBranchAddress("hid_g", self.hid_g)
            tree.SetBranchAddress("pdg", self.pdg)
            tree.SetBranchAddress("edep", self.edep)
            tree.SetBranchAddress("time", self.time)

            tree.SetBranchStatus("pos_l_x", 1)
            tree.SetBranchStatus("pos_l_z", 1)
            tree.SetBranchStatus("pos_g_x", 1)
            tree.SetBranchStatus("pos_g_y", 1)
            tree.SetBranchStatus("pos_g_z", 1)
            tree.SetBranchStatus("p_in_x", 1)
            tree.SetBranchStatus("p_in_y", 1)
            tree.SetBranchStatus("p_in_z", 1)
            tree.SetBranchAddress("pos_l_x", self.pos_l_x)
            tree.SetBranchAddress("pos_l_z", self.pos_l_z)
            tree.SetBranchAddress("pos_g_x", self.pos_g_x)
            tree.SetBranchAddress("pos_g_y", self.pos_g_y)
            tree.SetBranchAddress("pos_g_z", self.pos_g_z)
            tree.SetBranchAddress("p_in_x", self.p_in_x)
            tree.SetBranchAddress("p_in_y", self.p_in_y)
            tree.SetBranchAddress("p_in_z", self.p_in_z)

            self.tree = tree

    def load(self, i):
        self.tree.GetEntry(i)
