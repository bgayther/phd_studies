#ifndef UTIL_LAMBDA_HELPERS_HPP_
#define UTIL_LAMBDA_HELPERS_HPP_
    
// TODO: checks for dlam/dtan for double turns!
constexpr double AMU = 931.49410242;  // atomic mass unit [MeV/c^2]

auto get_sign = [] (const float &p) {
    auto sign = (p > 0) ? 1 : ((p < 0) ? -1 : 0);
    return sign;
};

auto deltaL8_dp = [] (const float& p_next_turn, const float& p) -> float {
    auto sign = get_sign(p);
    if (p_next_turn != 0.0) return sign * (p_next_turn - p);
    else return -999.0;
};

auto deltaL8_neg_dp = [] (const float& p_next_turn, const float& p) {
    return -deltaL8_dp(p_next_turn, p);
};

auto qualityCut = [] (const float &zpca_z, const float& lam010) {
    return (std::abs(zpca_z)<=55&&std::abs(lam010)>=0.04);
};

auto corrSegsMomCharge = [] (const float &p, const int &mc_pid) {
    // TODO: don't use mc info, just use file config
    auto charge = (mc_pid < 0) ? -1 : +1;
    return (charge * p);
};

auto selectMichelTracks = [] (const int &mc_type) {
    return (mc_type == 11);
};

auto filterPosTracks = [] (const float& p) {
    return (-1 * p > 0);
};

auto filterElecTracks = [] (const float& p) {
    return (-1 * p < 0);
};

auto selectMCElecTrack = [] (const int &mc_pid, const int &mc_type) {
    return (mc_pid == 11); // && mc_type == -1?
};

auto selectMCPosTrack = [] (const int &mc_pid, const int &mc_type) {
    return (mc_pid == -11); // && mc_type == -1?
};

auto diff = [] (const float &var2, const float &var1) {
    return (var2-var1);
};

auto dphi = [] (const float &phi1, const float &phi2) {
    float dphi = phi2 - phi1;
    if(std::abs(dphi) > float_t(M_PI)) dphi -= std::copysign(float(2*M_PI), dphi);
    return dphi;
};

auto framesCut = [] (const int &nhit, const int& s6n, const int& s6n_0, const int& s8n, const int &s8n_0, const int& seq) {
    bool cut = false;
    if (nhit == 4) cut = (s6n > 0 || s8n > 0);
    else if (nhit == 6) cut = (s8n_0 > 0 || seq > 0);
    else if (nhit == 8) cut = (s6n_0 > 0 || seq > 0);
    return !cut;
};

auto mc_prime_cut = [] (const int& mc_prime) {
    return mc_prime;
};

auto long_track_cut = [] (const int& nhit) {
    return nhit>=6;
};

auto L8_track_cut = [] (const int& nhit) {
    return nhit==8;
};

auto L6_track_cut = [] (const int& nhit) {
    return nhit==6;
};

auto double_turn_cut = [] (const float& p_next_turn) {
    return (p_next_turn!=0.0);
};

auto r =  [] (const float& x, const float& y, const float& z) { 
    return std::sqrt(x*x + y*y + z*z); 
};

auto rt =  [] (const float& x, const float& y) { 
    return std::hypot(x, y); 
};

auto phi =  [] (const float& x, const float& y) { 
    return std::atan2(y, x); 
};

auto lam =  []  (const float& x, const float& y, const float& z) { 
    return std::atan(z / rt(x, y)); 
};

auto deltaS4_dp = [] (const float &p, const float &p_S4_turn2, const float &p_S4_turn1) -> float {
    auto sign = get_sign(p);
    if (p_S4_turn2 != 0.0 && p_S4_turn1 != 0.0) return (std::abs(p_S4_turn2) - std::abs(p_S4_turn1));
    else return -999.0;
};

auto mc_target_cut = [] (const float& mc_vx, const float& mc_vy, const float& mc_vz) {
    return rt(mc_vx, mc_vy) <= 19.0 && std::abs(mc_vz) <= 50.0; // TODO: get this from a config 
};

auto min_max_p_cut = [] (const float& p, const float& min_p, const float& max_p) {
    return (std::abs(p) > min_p && std::abs(p) < max_p);
};

auto min_max_var_cut = [] (const float& var, const float& min_var, const float& max_var) {
    return (var > min_var && var < max_var);
};

auto doubleCrossing = [] (const float& mc_vx, const float& mc_vy, const float& mc_vz, const float& mc_lam, const float& mc_phi) {
    auto targetAngle = 0.3630285; // 20.8 [deg] from TDR
    auto majorAngle = M_PI + targetAngle*2; // 360 - (90-20.8)*2 = 180 + 2*20.8 = 221.6 [deg]
    // auto minorAngle = M_PI - majorAngle; // 360 - 221.6 = 138.4 [deg]
    
    // Check if track on target (within some tolerance)
    // if (mc_vr >= 19.1 && std::abs(mc_vz) >= 50.1) return false;

    if (std::abs(mc_vz) >= 55.0) return false;

    if (mc_vz > 50.0) return true; // had to pass through target to get here?

    // If scattered/decayed inside the target, then has to cross target again on way out
    auto insideTarget = rt(mc_vx, mc_vy) <= 18.8 && std::abs(mc_vz) <= 49.8;
    if (insideTarget) return true;

    // check now whether 'surface' target scatterings/decays go back into target
    // auto abs_mc_lam = std::abs(mc_lam);
    // return abs_mc_lam < majorAngle/2.0;

    // need something as a function of the vertex position
    auto mc_v_phi = phi(mc_vx, mc_vy);

    // just using phi
    // return (mc_phi < mc_v_phi + M_PI_2 && mc_phi > mc_v_phi - M_PI_2); // single crossing?
    return (mc_phi > (mc_v_phi + M_PI_2) || mc_phi < (mc_v_phi - M_PI_2));
};

auto singleCrossing = [] (const float& mc_vx, const float& mc_vy, const float& mc_vz, const float& mc_lam, const float& mc_phi) {
    return !doubleCrossing(mc_vx, mc_vy, mc_vz, mc_lam, mc_phi);
};

auto poly = [] (const double& x, const double& coef, const int& exp) {
    return coef*std::pow(x, exp);
};

auto pos_L6_coefs = std::vector<double>{0.5631168807737191, -0.07452359467425182, 0.003098927622988708, -5.798553482653303e-05, 4.007377759994668e-07};
auto pos_L8_coefs = std::vector<double>{0.862052628125641, -0.1165928806103303, 0.00518771654251628, -0.00010155395946594072, 7.322684697505187e-07};
auto elec_L6_coefs = std::vector<double>{0.3880424654306376, -0.054436267951083875, 0.0022052401189501646, -4.058945590369943e-05, 2.772918374420557e-07};
auto elec_L8_coefs = std::vector<double>{0.4969576188324576, -0.07616708768281108, 0.003539693401085537, -7.226557952537285e-05, 5.411295944701584e-07};

auto scaleMomentum = [] (const int& nhit, const int& mc_pid, const float& p) {
    double pScaled = 0.0;
    // auto sign = get_sign(p);
    int degree = 5;
    // auto coefs = std::vector<double>(degree);
   
   if (nhit==6) {
        if (mc_pid == -11) {
            // coefs = {0.6542482617775359, -0.08564882543560673, 0.0034708037826568366, -6.301413272892154e-05, 4.105540234442832e-07};
            for (int i = 0; i < degree; i++) {
                pScaled += poly(p, pos_L6_coefs[i], i);
            }
        } else if (mc_pid == 11) {
            // coefs = {0.5894780238824929, -0.07907760802214334, 0.0031818010148375055, -5.732522834399562e-05, 3.6980877414532137e-07};
            for (int i = 0; i < degree; i++) {
                pScaled += poly(p, elec_L6_coefs[i], i);
            }
        }
    } else if (nhit==8) {
        if (mc_pid == -11) {
            // coefs = {0.8564739028816502, -0.11680693231665806, 0.005128220963106797, -0.00010153142193917311, 7.214913206689332e-07};
            for (int i = 0; i < degree; i++) {
                pScaled += poly(p, pos_L8_coefs[i], i);
            }
        } else if (mc_pid == 11) {
            // coefs = {0.5511006377843769, -0.07968123852334957, 0.003434698221464054, -6.797238286662876e-05, 4.811577471073007e-07};
            for (int i = 0; i < degree; i++) {
                pScaled += poly(p, elec_L8_coefs[i], i);
            }
        }
    }
    return p-pScaled;
};

double applyAngularDependence(const double& mean_beam_energy, const double& p, const double& theta) {
    double scattered_p = mean_beam_energy / (1 + (mean_beam_energy / (12.010736*AMU)) * (1 - std::cos(theta))); // For Carbon!
    double diff = scattered_p - mean_beam_energy; // momentum loss [MeV/c]
    // printf("diff: %f, mean_beam_energy: %f, scattered_p: %f, p: %f, theta: %f\n", diff, mean_beam_energy, scattered_p, p, theta);
    // if (get_sign(p) == -1) return p+diff;
    // else return p-diff;
    return p-diff;
};

#endif /* UTIL_LAMBDA_HELPERS_HPP_ */